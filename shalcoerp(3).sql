-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2016 at 11:08 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shalcoerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sr_banks`
--

CREATE TABLE `sr_banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `swift_code` varchar(50) NOT NULL,
  `ad_code` varchar(50) NOT NULL,
  `ac_no` int(20) NOT NULL,
  `ac_name` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_banks`
--

INSERT INTO `sr_banks` (`id`, `bank_name`, `branch_name`, `swift_code`, `ad_code`, `ac_no`, `ac_name`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'Yes Bank\r\n\r\n\r\n\r\n', 'Lamigton Road', 'YESBINBB', '0411102', 254564455, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(2, 'Axix Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'AXIXBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(3, 'HDFC Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'HDBCBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(4, 'SBI\r\n\r\n\r\n\r\n', 'Georg Road', 'SBI54BINBB', '0411102', 254564455, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(5, 'UCO Bank\r\n\r\n\r\n\r\n', 'Hill Road', 'UCOBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_chemicals`
--

CREATE TABLE `sr_chemicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `c_mn` float NOT NULL,
  `c_mx` float NOT NULL,
  `mn_mn` float NOT NULL,
  `mn_mx` float NOT NULL,
  `p_mn` float NOT NULL,
  `p_mx` float NOT NULL,
  `s_mn` float NOT NULL,
  `s_mx` float NOT NULL,
  `si_mn` float NOT NULL,
  `si_mx` float NOT NULL,
  `cr_mn` float NOT NULL,
  `cr_mx` float NOT NULL,
  `ni_mn` float NOT NULL,
  `ni_mx` float NOT NULL,
  `mo_mn` float NOT NULL,
  `mo_mx` float NOT NULL,
  `n_mx` float NOT NULL,
  `n_mn` float NOT NULL,
  `fe_mn` float NOT NULL,
  `fe_mx` float NOT NULL,
  `nb_mn` float NOT NULL,
  `nb_mx` float NOT NULL,
  `ti_mn` float NOT NULL,
  `ti_mx` float NOT NULL,
  `cu_mn` float NOT NULL,
  `cu_mx` float NOT NULL,
  `v_mn` float NOT NULL,
  `v_mx` float NOT NULL,
  `b_mn` float NOT NULL,
  `b_mx` float NOT NULL,
  `ai_mn` float NOT NULL,
  `ai_mx` float NOT NULL,
  `ce_mn` float NOT NULL,
  `ce_mx` float NOT NULL,
  `co_mn` float NOT NULL,
  `co_mx` float NOT NULL,
  `w_mn` float NOT NULL,
  `w_mx` float NOT NULL,
  `la_mn` float NOT NULL,
  `la_mx` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_chemicals`
--

INSERT INTO `sr_chemicals` (`id`, `grade_id`, `c_mn`, `c_mx`, `mn_mn`, `mn_mx`, `p_mn`, `p_mx`, `s_mn`, `s_mx`, `si_mn`, `si_mx`, `cr_mn`, `cr_mx`, `ni_mn`, `ni_mx`, `mo_mn`, `mo_mx`, `n_mx`, `n_mn`, `fe_mn`, `fe_mx`, `nb_mn`, `nb_mx`, `ti_mn`, `ti_mx`, `cu_mn`, `cu_mx`, `v_mn`, `v_mx`, `b_mn`, `b_mx`, `ai_mn`, `ai_mx`, `ce_mn`, `ce_mx`, `co_mn`, `co_mx`, `w_mn`, `w_mx`, `la_mn`, `la_mx`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 3, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '2016-03-24 20:04:24', '2016-03-24 20:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `sr_clients`
--

CREATE TABLE `sr_clients` (
  `id` int(11) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mob` varchar(20) NOT NULL,
  `contact_person` varchar(20) NOT NULL,
  `contact_num1` varchar(20) NOT NULL,
  `contact_num2` varchar(20) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `coordinator` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_clients`
--

INSERT INTO `sr_clients` (`id`, `company_name`, `email`, `mob`, `contact_person`, `contact_num1`, `contact_num2`, `fax`, `website`, `address1`, `address2`, `coordinator`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 'f', 'test@gmail.com', 'fd', 'dfsd', 'fdf', 'sfdg', 'dfs', 'https://www.google.co.in/search?', 'sd', 'fsd', 0, 1, 5, 5, '2016-03-17 19:54:36', '2016-04-09 13:41:56'),
(3, 'sad', 'fas', 'fsa', 'fsa', 'saf', 'fas', 'fas', 'vdfvds', 'f', 'fsa', 0, 1, 5, 5, '2016-03-18 18:26:08', '2016-03-18 18:26:20'),
(4, 'test', 'test3g@gmail.com', '1234567890', 'test', '1234567890', '65156', 'fds', 'rmsb.com', 'f', 'sda', 0, 1, 5, 5, '2016-03-18 19:15:26', '2016-03-18 19:21:23'),
(5, 'Ureka', 'mnj@gmail.com', '1234567890', 'Manoj Desai', '122343454', '34234554`', 'test', 'test.com', 'test', 'tewst', 0, 1, 5, 5, '2016-04-08 21:03:30', '2016-04-09 13:40:05'),
(6, 'Friends Steel', 'steel@gmail.com', '35435', '334342423', '1234324', '23222432', 'tewrt', 'freinds.com', 'Lamibto Road', 'Hmsailton', 0, 1, 5, 5, '2016-04-08 21:11:01', '2016-04-08 21:11:01'),
(7, 'Tata', 'tata@gmsail.com', '45435435', 'Ratan Tata', '43543545', '534543543', 'Tat', 'https://www.tata.com', 'Tata House,All India Road', 'Tata House All India Road 2', 5, 1, 7, 7, '2016-04-11 01:09:09', '2016-04-11 01:17:28');

-- --------------------------------------------------------

--
-- Table structure for table `sr_grades`
--

CREATE TABLE `sr_grades` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_name` varchar(20) NOT NULL,
  `uns` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_grades`
--

INSERT INTO `sr_grades` (`id`, `standard_id`, `grade_name`, `uns`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 3, 'TP304/304L', '100', 1, 5, 5, '2016-03-24 18:33:28', '2016-04-09 13:14:36'),
(2, 1, 'TP304/305L', '101', 1, 5, 0, '2016-03-24 18:34:15', '2016-03-24 18:34:15'),
(3, 3, 'TP316/316L NI 11%', '102', 1, 5, 5, '2016-03-24 18:35:01', '2016-03-24 18:41:16'),
(4, 1, 'TP304/307L', '105', 1, 5, 0, '2016-03-24 18:35:56', '2016-03-24 18:35:56'),
(5, 5, 'TP304/308L', '109', 1, 5, 5, '2016-03-24 18:39:44', '2016-04-09 13:18:43'),
(6, 7, 'TP304/305L', '567', 1, 5, 5, '2016-04-09 13:18:24', '2016-04-09 13:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `sr_items`
--

CREATE TABLE `sr_items` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `od_nb` float NOT NULL,
  `wt_nb` varchar(100) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `length` int(11) NOT NULL,
  `gdmm` varchar(100) NOT NULL,
  `gdnb` varchar(100) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_items`
--

INSERT INTO `sr_items` (`id`, `standard_id`, `grade_id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `length`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 4, 4, 1, '6', 17.4, 5, 1.5376, 6, '17.4 MM X 1.65 MM X 6000 MM', '1/4" X SCH10S X 6 MTR', 5, 5, '2016-03-20 13:52:52', '2016-03-20 19:50:29'),
(3, 4, 0, 1, 'SCH10', 17.6, 6, 1.72608, 10, '17.6 MM X 1.69 MM X 10000 MM', '1/4" X SCH10 X 10 MTR', 5, 5, '2016-03-20 13:57:20', '2016-03-24 21:21:08'),
(4, 2, 1, 5, '4', 2, 1, 0.0248, 6, '63 MM X 5 MM X 6000 MM', '5" X 4 X 6 MTR', 5, 5, '2016-03-20 14:26:12', '2016-03-20 19:51:23'),
(6, 3, 3, 3, 'SCH40S', 17.1, 2.31, 0.84729, 6, '17.1 MM X 2.31 MM X 6000 MM', '3/8" X SCH40S X 6 MTR', 5, 5, '2016-04-03 22:14:19', '2016-04-03 22:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `sr_mechanicals`
--

CREATE TABLE `sr_mechanicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `tensile_strength` float NOT NULL,
  `yield_strength` float NOT NULL,
  `elolgation` float NOT NULL,
  `hardness` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_mechanicals`
--

INSERT INTO `sr_mechanicals` (`id`, `grade_id`, `tensile_strength`, `yield_strength`, `elolgation`, `hardness`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 2.3, 1.5, 6, 7, 5, 0, '2016-03-24 20:09:45', '2016-03-24 20:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `sr_physicals`
--

CREATE TABLE `sr_physicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `annealing_f` float NOT NULL,
  `annealing_c` float NOT NULL,
  `density` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_physicals`
--

INSERT INTO `sr_physicals` (`id`, `grade_id`, `annealing_f`, `annealing_c`, `density`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 6.3, 4.1, 4.9, 5, 5, '2016-03-24 20:05:26', '2016-03-24 20:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformainvoices`
--

CREATE TABLE `sr_proformainvoices` (
  `id` int(11) NOT NULL,
  `proforma_no` varchar(50) NOT NULL,
  `proforma_date` date NOT NULL,
  `tod` varchar(100) NOT NULL COMMENT 'Terms of Deilivery',
  `top` varchar(100) NOT NULL COMMENT 'Terms of Payment',
  `bill_to` int(11) NOT NULL COMMENT 'Bill to Client',
  `ship_to` varchar(50) NOT NULL COMMENT 'Ship to Client',
  `ship_address` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total` float NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `isveryfied` tinyint(2) NOT NULL DEFAULT '0',
  `approval_date` datetime NOT NULL,
  `verifiedby` int(11) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformainvoices`
--

INSERT INTO `sr_proformainvoices` (`id`, `proforma_no`, `proforma_date`, `tod`, `top`, `bill_to`, `ship_to`, `ship_address`, `bank_id`, `total`, `status`, `isveryfied`, `approval_date`, `verifiedby`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'SHL/001/2016', '2016-02-21', 'test', 'testr', 1, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-26 20:05:53', '2016-03-26 20:54:33'),
(2, 'SHL/2/2016', '2016-02-21', 'dgfd', 'fdgfd', 1, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-26 20:12:45', '2016-03-26 20:12:45'),
(3, 'SHL/3/2016', '0000-00-00', 'Test', 'Test', 3, '4', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-26 20:36:56', '2016-03-26 20:36:56'),
(4, 'SHL/4/2016', '0000-00-00', 'test', 'dsg', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-26 20:55:35', '2016-03-26 20:56:50'),
(5, 'SHL/5/2016', '2016-03-08', 'test123', 'fesfsd', 1, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-26 20:58:13', '2016-03-27 08:36:00'),
(7, 'SHL/6/2016', '0000-00-00', 'Test', 'tres', 1, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-31 19:43:56', '2016-03-31 19:43:56'),
(8, 'SHL/7/2016', '0000-00-00', 'dfg', 'd', 1, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-31 20:01:10', '2016-03-31 20:01:10'),
(9, 'SHL/8/2016', '0000-00-00', 'dfg', 'fdg', 1, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-31 20:05:59', '2016-03-31 20:05:59'),
(11, 'SHL/9/2016', '1970-01-01', 'kaushal  hsha', 'tesfsdf', 3, '3', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-03-31 20:19:47', '2016-04-09 19:12:38'),
(12, 'SHL/10/2016', '0000-00-00', 'Kaushal', 'sHAH', 2, '3', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-03 14:02:39', '2016-04-03 20:59:40'),
(13, 'SHL/11/2016', '0000-00-00', 'dfs', 'dfds', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-03 21:06:50', '2016-04-03 21:06:50'),
(14, 'SHL/12/2016', '0000-00-00', 'TOD', 'TOP', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-03 22:18:55', '2016-04-03 22:18:55'),
(15, 'SHL/13/2016', '0000-00-00', 'Test', 'test', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-05 21:13:11', '2016-04-05 21:13:11'),
(16, 'SHL/14/2016', '0000-00-00', 'fdsfsfdsdfsdf', 'klk', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-05 21:18:21', '2016-04-05 21:18:21'),
(17, 'SHL/15/2016', '0000-00-00', 'Test ', 'Test', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-06 18:11:52', '2016-04-06 18:48:33'),
(18, 'SHL/16/2016', '2016-04-07', 'testd', 'gdre', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-06 18:57:24', '2016-04-06 20:05:29'),
(19, 'SHL/17/2016', '2016-04-06', 'Cash on Hand', 'On time Delivery', 2, '2', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-06 19:29:18', '2016-04-09 23:40:51'),
(20, 'SHL/18/2016', '2016-04-10', 're', 're', 4, '1', '', 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-10 01:41:27', '2016-04-10 01:42:40'),
(21, 'SHL/19/2016', '2016-04-10', '120 DAYS FROM OPENING OF L/C	\r\n', 'L/C AT SIGHT.	\r\n', 6, '2', 'Garden Road', 2, 4214.4, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-10 15:37:08', '2016-04-10 16:45:49'),
(22, 'SHL/20/2016', '2016-04-10', 'tewst', 'est', 4, '2', '', 1, 159, 1, 0, '0000-00-00 00:00:00', 0, 5, 5, '2016-04-10 16:32:22', '2016-04-10 16:40:58'),
(23, 'SHL/21/2016', '2016-04-10', 'Just TEst', 'Just test', 6, 'Friends Steel', 'Lamibto Road', 3, 1392, 1, 0, '0000-00-00 00:00:00', 0, 7, 7, '2016-04-10 23:06:19', '2016-04-10 23:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformaitems`
--

CREATE TABLE `sr_proformaitems` (
  `id` int(11) NOT NULL,
  `proforma_id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformaitems`
--

INSERT INTO `sr_proformaitems` (`id`, `proforma_id`, `standard_id`, `grade_id`, `size_id`, `length`, `qty`, `price`, `amount`, `total`) VALUES
(20, 11, 1, 2, 2, 0, 151, 45, 6795, 0),
(26, 11, 1, 2, 2, 0, 56, 54, 3024, 0),
(27, 12, 1, 2, 0, 0, 36, 20, 720, 0),
(28, 12, 3, 3, 0, 0, 36, 21, 756, 0),
(29, 12, 5, 1, 0, 0, 690, 65, 44850, 0),
(31, 13, 1, 2, 0, 0, 36, 32, 1152, 0),
(32, 13, 5, 1, 0, 0, 23, 32, 736, 0),
(33, 14, 3, 3, 0, 0, 96, 69, 6624, 0),
(34, 15, 3, 3, 0, 6, 4, 2, 8, 0),
(35, 15, 1, 2, 3, 9, 3, 25, 75, 0),
(36, 16, 1, 4, 3, 2, 36, 65, 2340, 0),
(37, 16, 3, 3, 2, 6, 25, 36, 900, 0),
(38, 17, 1, 2, 1, 3, 6, 9, 54, 0),
(39, 17, 3, 3, 2, 6, 4, 3, 12, 0),
(41, 17, 1, 5, 3, 9, 1, 3, 3, 0),
(44, 18, 1, 2, 4, 10, 3, 36, 108, 0),
(45, 19, 1, 2, 4, 10, 35, 12, 420, 0),
(46, 19, 3, 3, 2, 36, 6, 69, 414, 0),
(47, 19, 7, 6, 8, 14, 20, 14, 280, 0),
(48, 20, 1, 2, 2, 20, 3, 12, 36, 0),
(52, 20, 3, 1, 4, 96, 4, 30, 120, 0),
(53, 21, 1, 2, 3, 6, 1500, 2.02, 3030, 4214.4),
(54, 21, 1, 2, 5, 6, 504, 2.35, 1184.4, 0),
(55, 9, 1, 2, 3, 6, 2, 66, 132, 0),
(56, 9, 3, 3, 2, 6, 3, 9, 27, 0),
(57, 23, 1, 2, 1, 6, 20, 12, 240, 0),
(58, 23, 3, 3, 5, 9, 36, 32, 1152, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_settings`
--

CREATE TABLE `sr_settings` (
  `id` int(11) NOT NULL,
  `office_address` varchar(255) NOT NULL,
  `factory_address` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_settings`
--

INSERT INTO `sr_settings` (`id`, `office_address`, `factory_address`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '24, 2nd FLR., 56, DAYABHAI BUILDING 	\r\nGULALWADI, MUMBAI-400004	\r\nMAHARASHTRA ( INDIA )	\r\n', 'B-11, MIDC Mahad, Dist.Raigad,Pin code-402301,	\r\nMaharashtra, India	\r\n', 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_sizes`
--

CREATE TABLE `sr_sizes` (
  `id` int(11) NOT NULL,
  `od_nb` varchar(20) NOT NULL,
  `wt_nb` varchar(20) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `gdmm` varchar(35) NOT NULL,
  `gdnb` varchar(35) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_sizes`
--

INSERT INTO `sr_sizes` (`id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '12', '25', 32, 6, 3.8688, '32 MM X 6 MM', '12 X 25', 5, 5, '2016-04-05 19:02:49', '2016-04-05 19:13:00'),
(2, '1/4', '25', 13.4, 1.67, 0.48581, '13.4 MM X 1.67 MM ', '1/4 X 25', 5, 5, '2016-04-05 20:15:35', '2016-04-05 20:15:35'),
(3, '1/4', '12', 13.7, 1.69, 0.503363, '13.7 MM X 1.69 MM', '1/4 X 12', 5, 5, '2016-04-05 20:16:24', '2016-04-05 20:16:24'),
(4, '21', 'SCH10S', 36, 6, 4.464, '36 MM X 6 MM', '21" X 10', 5, 5, '2016-04-06 19:20:28', '2016-04-06 19:22:01'),
(5, '1/4', 'SCH10S', 13.7, 1.65, 0.493086, '13.7 MM X 1.65 MM', '1/4"" X SCH10S', 5, 5, '2016-04-08 21:14:40', '2016-04-09 14:50:32'),
(6, '3.3', '2', 13.4, 2, 0.56544, '13.4 MM X 2 MM', '3.3 X 2', 5, 5, '2016-04-09 14:25:52', '2016-04-09 14:30:14'),
(7, '3.3', '8', 13.4, 9, 0.98208, '13.4 MM X 9 MM', '3.3 X 8', 5, 5, '2016-04-09 14:28:31', '2016-04-09 14:32:50'),
(8, '1/5"', '6', 12.9, 2, 0.54064, '12.9 MM X 2 MM', '1/5" X 6', 5, 5, '2016-04-09 18:43:20', '2016-04-09 18:43:20');

-- --------------------------------------------------------

--
-- Table structure for table `sr_standards`
--

CREATE TABLE `sr_standards` (
  `id` int(11) NOT NULL,
  `standard_name` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_standards`
--

INSERT INTO `sr_standards` (`id`, `standard_name`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'A312', 1, 5, 5, '2016-03-24 18:28:39', '2016-04-09 11:51:07'),
(2, 'A313', 1, 5, 5, '2016-03-24 18:29:27', '2016-04-09 12:41:32'),
(3, 'A314', 1, 5, 5, '2016-03-24 18:29:53', '2016-03-24 18:29:53'),
(4, 'A315', 1, 5, 5, '2016-03-24 18:30:26', '2016-04-09 12:30:05'),
(5, 'A316', 1, 5, 0, '2016-03-24 18:30:45', '2016-03-24 18:30:45'),
(6, 'terresf', 1, 5, 0, '2016-03-26 14:36:22', '2016-03-26 14:36:22'),
(7, 'A1234', 1, 5, 5, '2016-04-08 21:24:18', '2016-04-09 11:51:24'),
(8, 'test', 1, 6, 6, '2016-04-08 22:56:34', '2016-04-08 22:56:34'),
(9, 'A312', 1, 5, 5, '2016-04-09 11:47:27', '2016-04-09 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `sr_users`
--

CREATE TABLE `sr_users` (
  `id` int(11) NOT NULL COMMENT 'User id ',
  `usertype_id` int(11) NOT NULL COMMENT '1 = admin, 2 = HR, 3 = Supervisor, 4 = Employee, 5 = Secondary Supervisor',
  `first_name` varchar(30) NOT NULL COMMENT 'User First Name',
  `middle_name` varchar(30) NOT NULL COMMENT 'User Middle Name',
  `last_name` varchar(30) NOT NULL COMMENT 'User Last Name',
  `preferred_name` varchar(50) NOT NULL COMMENT 'User Preferred Name',
  `username` varchar(50) NOT NULL COMMENT 'User Name',
  `email` varchar(100) NOT NULL COMMENT 'User Email',
  `mobile` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT 'User Password',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 = Male, 1 = Female',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `supervisor` int(11) DEFAULT NULL COMMENT 'Supervisor ID',
  `signature` varchar(100) NOT NULL COMMENT 'User Signature',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active',
  `token` varchar(50) NOT NULL COMMENT 'User Information token',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_users`
--

INSERT INTO `sr_users` (`id`, `usertype_id`, `first_name`, `middle_name`, `last_name`, `preferred_name`, `username`, `email`, `mobile`, `password`, `gender`, `dob`, `supervisor`, `signature`, `status`, `token`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(5, 2, 'jay', '', 'shah', '', 'jay@gmail.com', 'jay@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(6, 1, 'admin', '', 'admin', '', 'admin@admin.com', 'admin@admin.com', '453543545', '75874572ba20024ef8283f6d7ad6df2f82fe998f', 0, '2016-04-06', NULL, '', 1, '', 0, 0, '2016-04-08 21:15:40', '2016-04-09 13:43:35'),
(7, 1, 'chirag', '', 'shah', '', 'chirag@gmail.com', 'chirag@gmail.com', '4534535545', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2011-07-06', NULL, '', 1, '', 0, 0, '2016-04-08 22:05:30', '2016-04-08 22:05:30');

-- --------------------------------------------------------

--
-- Table structure for table `sr_usertypes`
--

CREATE TABLE `sr_usertypes` (
  `id` int(11) NOT NULL,
  `usertype_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_usertypes`
--

INSERT INTO `sr_usertypes` (`id`, `usertype_name`) VALUES
(1, 'Admin'),
(2, 'HR'),
(3, 'Supervisor'),
(4, 'Employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sr_banks`
--
ALTER TABLE `sr_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_clients`
--
ALTER TABLE `sr_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_grades`
--
ALTER TABLE `sr_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `standard_id` (`standard_id`);

--
-- Indexes for table `sr_items`
--
ALTER TABLE `sr_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_settings`
--
ALTER TABLE `sr_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_standards`
--
ALTER TABLE `sr_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_users`
--
ALTER TABLE `sr_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type` (`usertype_id`);

--
-- Indexes for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sr_banks`
--
ALTER TABLE `sr_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_clients`
--
ALTER TABLE `sr_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sr_grades`
--
ALTER TABLE `sr_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sr_items`
--
ALTER TABLE `sr_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sr_standards`
--
ALTER TABLE `sr_standards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_users`
--
ALTER TABLE `sr_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id ', AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
