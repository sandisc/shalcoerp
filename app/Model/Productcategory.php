<?php
App::uses('Model', 'Model');
class Productcategory extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Productcategory";
   	var $useTable = "productcategories";
    var $primaryKey="id";
    var $belongsTo = array(
        'Producttaxonomy' => array(
            'className' => 'Producttaxonomy',
            'foreignKey' => 'producttaxonomy_id',
        ),
        'Hscode' => array(
            'className' => 'Hscode',
            'foreignKey' => 'hscode_id',
           /* 'foreignKey'=>false,
                        'finderQuery' => 'SELECT Hscode.* FROM sr_hscodes AS Hscode,sr_productcategories WHERE sr_productcategories.id = {$__cakeID__$} AND sr_productcategories.hscode_id = Hscode.id',*/
        ),
         'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        ),
    );

    public $validate = array(
        'productname'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Product Category Name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueProductname'),
                'message'=>'Product Category Name is already used.',
               // 'on'=>'create'
            )
        ),
        'name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Product Category Alias.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueName'),
                'message'=>'Product Category Alias is already used.',
               // 'on'=>'create'
            )
        ),
        'producttaxonomy_id' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please select Product Taxonomy.'
            ),
        ),
        'hscode_id' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please select Hs code.'
            ),
        ),
       
               
    );

    /**
    * Name : uniqueProductname
    * Use : For check duplicate name of Product name.
    * @param array data
    * @return boolean
    */
    public function uniqueProductname($data) {  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('productname' => $data['productname'],'Productcategory.id !=' => $this->id)));  
        }else{
            $count = $this->find('count', array('conditions' => array('productname' => $data['productname'])));            
        }    
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

     /**
    * Name : uniqueName
    * Use : For check duplicate name of Name.
    * @param array data
    * @return boolean
    */
    public function uniqueName($data){  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('Productcategory.name' => $data['name'],'Productcategory.id !=' => $this->id)));
        }else{
            $count = $this->find('count', array('conditions' => array('Productcategory.name' => $data['name'])));           
        }
        
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}