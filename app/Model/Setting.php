<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Setting extends AppModel {

	var $name = "Setting";
    var $useTable = "settings";
    var $primaryKey="id";

    //Var $belongsTo = array('User');
    public $validate = array(
        'vat_tin_no' => array(
            'required'=>array(
                'rule' => array('notBlank'),                
                'message'=>'Please enter Vat tin no.'
            )           
        ),
        'cst_tin_no' => array(
            'required'=>array(
                'rule' => array('notBlank'),                
                'message'=>'Please enter Cst tin no.'
            )           
        ),
        'cin_no'=>array(
            'required'=>array(   
                'rule' => array('notBlank'),                
                'message'=>'Please enter CIN no.'
            )
        ),
        'iso_certificate' => array(
            'required'=>array(
                'rule' => array('notBlank'),                
                'message'=>'Please enter ISO Certificate Number'
            )           
        ),
        'ped_certificate'=>array(
            'required'=>array(   
                'rule' => array('notBlank'),                
                'message'=>'Please enter PED Certificate Number.'
            )
        ),
        'work_phase'=>array(
            'required'=>array(   
                'rule' => array('notBlank'),                
                'message'=>'Please enter Work Phase.'
            )
        ),                               
    );  
}