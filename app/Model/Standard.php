<?php
App::uses('Model', 'Model');
class Standard extends AppModel {
    var $name = "Standard";
   	var $useTable = "standards";
    var $primaryKey="id";
    var $hasMany = array(
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'standard_id'
        )
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );
    
    public $validate = array(
       'standard_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter standard name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueStd'),
                'message'=>'Standard name already exist.'
            )
        ),
       'termsandcondition'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Terms and Condition.'
            )
        ),       
       'procatid'=>array(
            'required'=>array(
                'rule' => array('multiple', array('min' => 1)),                
                'message'=>'Please select atleast one product category.'
            )
        )       
    );
    
   /**
    * Name : uniqueStd
    * Use : For check duplicate name of standard
    * @param array data
    * @return boolean
    */
    public function uniqueStd($data){
        $count = $this->find('count', array('conditions' => array('standard_name' => $data['standard_name'],'Standard.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}