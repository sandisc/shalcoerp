<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Bank extends AppModel {

	var $name = "Bank";
    var $useTable = "banks";
    var $primaryKey="id";

    public $validate = array(
       'bank_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Bank name.'
            ),
        ),
       'bank_alias'=>array(
            'required'=>array(
                    'rule'=>array('notBlank'),
                            'message'=>'Please enter Bank alias.'
                             ),
            'rule3'=>array(
                            'rule'=>array('uniqueAlias'),
                            'message'=>'Bank Alias is already used.'
                            )
        ),
       'branch_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Branch name.'
            ),
        ),
       'swift_code'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Swift code.'
            ),
        ),
       'ad_code'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter AD code.'
            ),
        ),                       
       'ac_no'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter A/C no.'
            )
        ),
        'ac_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter A/C name.'
            )
        ),
    );
     /**
    * Name : uniqueGrade
    * Use : For check duplicate name of grade for particular standard
    * @param array data
    * @return boolean
    */
  public function uniqueAlias($data) 
    {  
      
        $count = $this->find('count', array('conditions' => array('bank_alias' => $data['bank_alias'],'Bank.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

}