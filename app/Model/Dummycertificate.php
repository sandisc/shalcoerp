<?php
App::uses('Model', 'Model');
class Dummycertificate extends AppModel {
  /**
    * Used for fetch data from Db and insert data in Db 
    */
    var $name = "Dummycertificate";
   	var $useTable = "dummycertificates";
    var $hasMany = array(
        'Dummycertificateitem' => array(
            'className' => 'Dummycertificateitem',
            'foreignKey' => 'certificateid'
        ),
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        ),
        'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'clientid',
            'fields' => array('id','company_name'),
        )
    );

    public $validate = array(
       'certificateno'=>array(
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Certificate No is already used.'
            )
        ),
        'clientid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select client.'
            ),
        ),
        'clientpo' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter client p.o.'
            ),
        ),
        'orderdate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter order date.'
            ),
        ),
        'standard_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select standard.'
            ),
        ),
        'std_spec' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter standard specification.'
            ),
        ),        
        /*'grade_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select grade.'
            ),
        ),*/
        'grd_spec' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter grade specification.'
            ),
        ),        
    );


   /**
    * Name : uniqueNo
    * Use : For check duplicate id of certificate no.
    * @param array data
    * @return boolean
    */  
     public function uniqueNo($data){  
        $count = $this->find('count', array('conditions' => array('certificateno' => $data['certificateno'],'Dummycertificate.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}