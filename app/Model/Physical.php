<?php
App::uses('Model', 'Model');
class Physical extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Physical";
   	var $useTable = "physicals";


   	  var $belongsTo = array(
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id'
        )
    );

       /* public $validate = array(
       'density'=>array(
            'required'=>array(
                            'rule'=>array('notBlank'),
                            'message'=>'Please enter Density.'
                             )
          
        ),
    );*/


}