<?php
App::uses('Model', 'Model');
class Licenceitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licenceitem";
   	var $useTable = "licenceitems";
    var $belongsTo = array(
        'Gradetag' => array(
          'className' => 'Gradetag',
            'foreignKey' => 'gradetagid',
            'fields' => array('id','gradetagname'),
            //'finderQuery' => 'SELECT Gradetag.gradetagname ,Licenceitem.* FROM sr_licenceitems AS Licenceitem INNER JOIN sr_gradetags as Gradetag WHERE Licenceitem.id = {$__cakeID__$} AND Licenceitem.gradetagid = Gradetag.id',
        ),
    );
}