<?php
App::uses('Model', 'Model');
class Licenceexportitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licenceexportitem";
   	var $useTable = "licenceexportitems";
   	var $belongsTo = array(
        'Licenceitem' => array(
            'className' => 'Licenceitem',
            'foreignKey' => 'licenceitemid',
            'finderQuery' => 'SELECT Gradetag.gradetagname ,Licenceitem.ex_qty,Licenceitem.gradetagid FROM sr_licenceitems AS Licenceitem INNER JOIN sr_gradetags as Gradetag INNER JOIN sr_licenceexportitems as Licenceexportitem WHERE Licenceexportitem.id = {$__cakeID__$} AND Licenceexportitem.licenceitemid = Licenceitem.id AND Licenceitem.gradetagid = Gradetag.id',
        ),
    );
}