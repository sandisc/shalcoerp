<?php
App::uses('Model', 'Model');
class Producttaxonomy extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Producttaxonomy";
   	var $useTable = "producttaxonomies";
    var $primaryKey="id";
}