<?php
App::uses('Model', 'Model');
class Report extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Report";
   	var $useTable = "reports";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );

}