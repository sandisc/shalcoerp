<?php

App::uses('Model', 'Model');

class Industry extends AppModel {

	var $name = "Industry";
    var $useTable = "industries";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );
   	public $validate = array(
        'industryname'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Industry name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Industry is already used.',
            )            
        ),
    );

   /**
    * Name : uniqueNo
    * Use : For check duplicate name of industry
    * @param array data
    * @return boolean
    */
    public function uniqueNo($data){      
        $count = $this->find('count', array('conditions' => array('industryname' => $data['industryname'],'Industry.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }         
}