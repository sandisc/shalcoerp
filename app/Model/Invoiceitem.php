<?php
App::uses('Model', 'Model');
class Invoiceitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Invoiceitem";
   	var $useTable = "invoiceitems";

    var $belongsTo = array(
       'Licence' => array(
            'className' => 'Licence',
            'foreignKey' => 'licenceid',
            'fields' => array('Licence.advancelicenceno,Licence.hscode_id'),
        ),
        'Orderitem' => array(
            'className' => 'Orderitem',
            'foreignKey' => 'orderitemid',
        ),

    );  
    
}