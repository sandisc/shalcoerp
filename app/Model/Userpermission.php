<?php
/**
* Name : Userpermission
* Use : For validation of Userpermission field
*/

class Userpermission extends AppModel {

	var $name = "Userpermission";

    var $useTable = "userpermissions";    /* Define Table Name */

    public $validate = array(
        'user_id' => array(
            'required'=>array(
               'rule'=>array('notBlank'),
                'message'=>'Please select employee'
            )
        ), 
        'access_modules' => array(
            'required'=>array(
                'rule'=>array('checkmodules'),
                'message'=>'Please give permission'
            )
        ),       
    );

    function checkmodules() {
        if(empty($this->data['Userpermission']['access_modules'])) {
            return false;
        }
        return true;
    }
}