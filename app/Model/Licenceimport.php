<?php
App::uses('Model', 'Model');
class Licenceimport extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licenceimport";
   	var $useTable = "licenceimports";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),
        'Licence' => array(
            'className' => 'Licence',
            'foreignKey' => 'licenceid',
            'fields' => array('Licence.advancelicenceno'),
        ),
        'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'clientid',
            'fields' => array('Client.company_name'),
        ),
    );
    var $hasMany = array(
        'Licenceimportitem' => array(
            'className' => 'Licenceimportitem',
            'foreignKey' => 'licenceimportid'
        ),        
    );
    
    public $validate = array(
       'boeno'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter BOE No.'
            ),            
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'BOE No is already used.',
            )
        ),
        'boedate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter BOE date'
            ),
        ),
        'invoicedate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Invoice date.'
            ),
        ),
        'clientid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select Party name.'
            ),
        ),  
        'licenceid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select Licence No.'
            ),
        ),       
        'invoiceid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please Enter Invoice No.'
            ),
        ),   
    );
/**
    * Name : uniqueNo
    * Use : For check duplicate id of BOE No.
    * @param array data
    * @return boolean
    */  
    public function uniqueNo($data){  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('boeno' => $data['boeno'],'Licenceimport.id !=' => $this->id)));
        }else{
            $count = $this->find('count', array('conditions' => array('boeno' => $data['boeno'])));           
        }      
        //$count = $this->find('count', array('conditions' => array('advancelicenceno' => $data['advancelicenceno'],'Licence.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

}