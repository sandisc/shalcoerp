<?php

App::uses('Model', 'Model');

class Factory extends AppModel {

	var $name = "Factory";
    var $useTable = "factories";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );

    public $validate = array(
        'factory_alias'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Factory name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Factory alias is already used.',
            )            
        ),
        'division_alias'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Divison alias.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueAlias'),
                'message'=>'Division alias is already used.',
            )            
        ),
        'factory_address'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Factory Address.'
            ),
          
        ),
        'division_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Divison name.'
            ),
        ),
        'division_range'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Division range.'
            ),
        ),
        'commissionerate'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Commision rate.'
            ),
        ),
        'excise_regn_no'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Excise Regn No.'
            ),
        ),
    );

   /**
    * Name : uniqueNo
    * Use : For check duplicate name of factory alias
    * @param array data
    * @return boolean
    */
    public function uniqueNo($data){      
        $count = $this->find('count', array('conditions' => array('factory_alias' => $data['factory_alias'],'Factory.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }         

   /**
    * Name : uniqueAlias
    * Use : For check duplicate name of Division alias
    * @param array data
    * @return boolean
    */
    public function uniqueAlias($data){      
        $count = $this->find('count', array('conditions' => array('division_alias' => $data['division_alias'],'Factory.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }         
}