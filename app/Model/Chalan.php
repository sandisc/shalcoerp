<?php
App::uses('Model', 'Model');
class Chalan extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Chalan";
   	var $useTable = "chalans";
    var $hasMany = array(
        'Chalanitem' => array(
            'className' => 'Chalanitem',
            'foreignKey' => 'chalanid'
        ),
    );
        
   	var $belongsTo = array(
       'Order' => array(
            'className' => 'Order',
            'foreignKey' => 'orderid',
        ),
        'Division' => array(
            'className' => 'Division',
            'foreignKey' => 'division_id'
        ), 
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),      
    );
    public $validate = array(
        'division_id'=>array(
           'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select division.'
            ),
        ),
        'chalanno'=>array(
           'rule3'=>array(
                'rule'=>array('uniqueNo'),
                 'message'=>'Chalan No is already used.',
                //'on'=>'create'
            )
        ),
        'chalandate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter date.'
            ),
        ),
    );
     /**
    * Name : uniqueNo
    * Use : For check duplicate id of Chalan no.
    * @param array data
    * @return boolean
    */  
    public function uniqueNo($data){
        $count = $this->find('count', array('conditions' => array('chalanno' => $data['chalanno'],'division_id' => $this->data['Chalan']['division_id'],'Chalan.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }    
}