<?php
App::uses('Model', 'Model');
class Dummycertificateitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Dummycertificateitem";
   	var $useTable = "dummycertificateitems";

    var $belongsTo = array(
       'Productcategory' => array(
            'className' => 'Productcategory',
            'foreignKey' => 'procatid',
        ),
        'Size' => array(
            'className' => 'Size',
            'foreignKey' => 'size_id',
        )
    );
          
}