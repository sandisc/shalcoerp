<?php
App::uses('Model', 'Model');
class Grade extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Grade";
   	var $useTable = "grades";
    var $primaryKey="id";


   	var $belongsTo = array(
        'Standard' => array(
            'className' => 'Standard',
            'foreignKey' => 'standard_id',
            'fields' => array('id','standard_name'),
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        ),
        'Gradetag' => array(
            'className' => 'Gradetag',
            'foreignKey' => 'gradetagid',
            'fields' => array('id','gradetagname')
        ),
    );
    var $hasOne = array(
        'Physical' => array(
            'className' => 'Physical',
            'foreignKey' => 'grade_id'
        ),
        'Mechanical' => array(
            'className' => 'Mechanical',
            'foreignKey' => 'grade_id'
        ),
        'Chemical' => array(
            'className' => 'Chemical',
            'foreignKey' => 'grade_id'
        ),
        
    );
  public $validate = array(
       'grade_name'=>array(
            'required'=>array(
                            'rule'=>array('notBlank'),
                            'message'=>'Please enter Grade name.'
                             ),
            'rule3'=>array(
                            'rule'=>array('uniqueGrade'),
                            'message'=>'Grade name is already used for this standard.'
                            )
        ),
        'uns'=>array(
             'required'=>array(
                            'rule'=>array('notBlank'),
                            'message'=>'Please enter UNS.'
                             ),
        ),
    );
    /**
    * Name : uniqueGrade
    * Use : For check duplicate name of grade for particular standard
    * @param array data
    * @return boolean
    */
  public function uniqueGrade($data) 
    {  
      
        $count = $this->find('count', array('conditions' => array('grade_name' => $data['grade_name'],'Grade.id !=' => $this->id,'Grade.standard_id' => $this->data[$this->alias]['standard_id'])));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}