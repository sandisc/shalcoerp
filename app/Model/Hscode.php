<?php
App::uses('Model', 'Model');
class Hscode extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Hscode";
   	var $useTable = "hscodes";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );
    public $validate = array(
       'hscode'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter HS Code.'
            ),            
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'HS Code is already used.',
            )
        ),
       'hscode_alias'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter HS Code Alias.'
            ),            
            'rule3'=>array(
                'rule'=>array('uniqueAlias'),
                'message'=>'HS Code ALias is already used.',
            )
        ),
        'rate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Value-Rate(%).'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please enter only numeric value.'
            )            
        ),               
    );

   /**
    * Name : uniqueNo
    * Use : For check duplicate name of hscode
    * @param array data
    * @return boolean
    */
    public function uniqueNo($data){      
        $count = $this->find('count', array('conditions' => array('hscode' => $data['hscode'],'Hscode.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }        
   /**
    * Name : uniqueAlias
    * Use : For check duplicate name of hscode
    * @param array data
    * @return boolean
    */
    public function uniqueAlias($data){      
        $count = $this->find('count', array('conditions' => array('hscode_alias' => $data['hscode_alias'],'Hscode.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}