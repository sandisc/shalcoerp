<?php
App::uses('Model', 'Model');
class Licenceexport extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licenceexport";
   	var $useTable = "licenceexports";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),
        'Licence' => array(
            'className' => 'Licence',
            'foreignKey' => 'licenceid',
            //'fields' => array('Licence.advancelicenceno'),
        ),
        'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'clientid',
            'fields' => array('Client.company_name'),
        ),
        'Invoice' => array(
            'className' => 'Invoice',
            'foreignKey' => 'invoiceid',
            'fields' => array('Invoice.invoiceno,Invoice.invoicedate,Invoice.sbdate,Invoice.sbno'),
        ),        
    );
    var $hasMany = array(
        'Licenceexportitem' => array(
            'className' => 'Licenceexportitem',
            'foreignKey' => 'licenceexportid'
        ),        
    );  
}