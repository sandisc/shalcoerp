<?php
App::uses('Model', 'Model');
class Chemical extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Chemical";
   	var $useTable = "chemicals";


   	  var $belongsTo = array(
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id'
        )
    );

}