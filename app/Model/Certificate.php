<?php
App::uses('Model', 'Model');
class Certificate extends AppModel {
  /**
    * Used for fetch data from Db and insert data in Db 
    */
    var $name = "Certificate";
   	var $useTable = "certificates";
    var $hasMany = array(
        'Certificateitem' => array(
            'className' => 'Certificateitem',
            'foreignKey' => 'certificateid'
        ),
    );
  
    var $belongsTo = array(
       'Order' => array(
            'className' => 'Order',
            'foreignKey' => 'orderid',            
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )       
    );
}