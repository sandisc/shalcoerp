<?php
/**
* Name : Activity
*/

class Activity extends AppModel{
    /**
    *  Used for fetch data from Db and insert data in Db 
    */
    var $name="Activity";
    /**
    *  Define Table Name 
    */
    var $useTable="activities";

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'fields' => array('first_name','last_name'),
        ),
    );    
}