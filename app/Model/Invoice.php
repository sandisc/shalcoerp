<?php
App::uses('Model', 'Model');
class Invoice extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Invoice";
   	var $useTable = "invoices";
      var $hasMany = array(
        'Invoiceitem' => array(
            'className' => 'Invoiceitem',
            'foreignKey' => 'invoiceid'
        ),
  );

        
   	var $belongsTo = array(
       'Order' => array(
            'className' => 'Order',
            'foreignKey' => 'orderid',
        ),  
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),        
    );  

    public $validate = array(
       'invoiceno'=>array(
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Invoiceno No is already used.',
               // 'on'=>'create'
            )
        ),
        'invoicedate' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter invoice date.'
            ),
        ),
        'containerno' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter container no.'
            ),
        ),
        'fromcountry' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter country of origin.'
            ),
        ),
        'tocountry' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter country of final.'
            ),
        ),
        /*'ifscno' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter IEC code no.'
            ),
        ),*/
        'flightno' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter flight no.'
            ),
        ),
        'carriageby' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter carriage by.'
            ),
        ),       
        'receiptplace' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter receipt place.'
            ),
        ),
         'dischargeport' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter discharge port.'
            ),
        ),
         'loadingport' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter loading port.'
            ),
        ),
         'deliveryplace' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter delivery place.'
            ),
        ),
        /* 'tod' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of delivery.'
            ),
        ),
          'top' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of payment.'
            ),
        ),*/
          'hscodeid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select HS code.'
            ),
        ),   
        'totalpackage' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter total package.'
            ),
        ),
          'totalnetweight' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter net weight.'
            ),
        ),
          'totalgrossweight' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter gross weight.'
            ),
        ),         
          /*
          'cif' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter cif.'
            ),
        ),   
          'fob' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter fob.'
            ),
        ),   
          'frieght' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter frieght.'
            ),
        ),   
          'insurance' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter insurance.'
            ),
        ),
       'discount' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter discount.'
            ),
        ),   
        */

    );

 /**
    * Name : uniqueNo
    * Use : For check duplicate id of Invoice no.
    * @param array data
    * @return boolean
    */  
     public function uniqueNo($data) 
    {  
      
        $count = $this->find('count', array('conditions' => array('invoiceno' => $data['invoiceno'],'Invoice.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
    
}