<?php
App::uses('Model', 'Model');
class Gradetag extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Gradetag";
   	var $useTable = "gradetags";
    var $primaryKey="id";
    var $hasMany = array(
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'id'
        ),
    );
}