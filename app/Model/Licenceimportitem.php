<?php
App::uses('Model', 'Model');
class Licenceimportitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licenceimportitem";
   	var $useTable = "licenceimportitems";
     var $belongsTo = array(
        'Licenceitem' => array(
            'className' => 'Licenceitem',
            'foreignKey' => 'licenceitemid',
            'finderQuery' => 'SELECT Gradetag.gradetagname ,Licenceitem.im_qty,Licenceitem.gradetagid FROM sr_licenceitems AS Licenceitem INNER JOIN sr_gradetags as Gradetag INNER JOIN sr_licenceimportitems as Licenceimportitem WHERE Licenceimportitem.id = {$__cakeID__$} AND Licenceimportitem.licenceitemid = Licenceitem.id AND Licenceitem.gradetagid = Gradetag.id',
        ),
    );
}