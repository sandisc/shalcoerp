<?php
App::uses('Model', 'Model');
class Item extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Item";
   	var $useTable = "items";


   	 var $belongsTo = array(
        'Standard' => array(
            'className' => 'Standard',
            'foreignKey' => 'standard_id'
        ),
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id',
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );

   	public $validate = array(
        'standard_id' => array(
            'required' => array(
            	'rule'=>array('notBlank'),
                'message' => 'Please Select Standard name'
            ),
        ),
        'grade_id' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please Select Grade name'
            ),
        ),
        'od_nb' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please enter OD.'
            ),
        ),
        'od_mm' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter OB(MM)'
            ),
        ),
        'wt_nb' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter WT'
            ),
        ), 
        'wt_mm' => array(
            'required' => array(
            'rule' =>array('notBlank'),
            'message' => 'Please enter WT(MM)'
            ),
        ),
        'length' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter Length'
            ),
        ),    
               
    );

    
}