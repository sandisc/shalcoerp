<?php
App::uses('Model', 'Model');
class Proformaitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Proformaitem";
   	var $useTable = "proformaitems";
   	var $belongsTo = array(
        'Productcategory' => array(
            'className' => 'Productcategory',
           // 'foreignKey' => 'procatid',
            /* hscode associated with 3rd level relationship with productcategory, so we have to pass recursive 3 for fetch hscode at time of fetch proforma, but if we use recusrive =3 its not only fetch hscode but its also fetch grade's 3rd level related data like physical property etc, so added manuall join to fetch hscode with recursive = 2 */
            'foreignKey' => false,
            'finderQuery' => 'SELECT Productcategory.productname,Producttaxonomy.unit,Hscode.hscode,Hscode.rate FROM sr_productcategories AS Productcategory,sr_hscodes as Hscode,sr_proformaitems,sr_producttaxonomies AS Producttaxonomy WHERE sr_proformaitems.id = {$__cakeID__$} AND sr_proformaitems.procatid = Productcategory.id AND Productcategory.hscode_id = Hscode.id AND Productcategory.producttaxonomy_id = Producttaxonomy.id',
        ),        
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id',
            'fields' => array('id','grade_name','standard_id'),
        ),
        'Standard' => array(
            'className' => 'Standard',
            'foreignKey' => 'standard_id',
            'fields' => array('id','standard_name'),
           // 'foreignKey' => false,
          //  'conditions' => array('Proformaitem.standard_id = Standard.id'),
        ),
        'Size' => array(
            'className' => 'Size',
            'foreignKey' => 'size_id'
        )
    );

   	public $validate = array(
        'standard_id' => array(
            'required' => array(
            	'rule'=>array('notBlank'),
                'message' => 'Please Select Standard name'
            ),
        ),
        'grade_id' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please Select Grade name'
            ),
        ),               
    );    
}