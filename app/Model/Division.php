<?php
App::uses('Model', 'Model');
class Division extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Division";
   	var $useTable = "divisions";
    var $primaryKey="id";

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );
    public $validate = array(
        'division_alias'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Divison alias.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueAlias'),
                'message'=>'Division alias is already used.',
            )            
        ),
        'division_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Divison name.'
            ),
        ),
        'excise_regn_no'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Excise Regn No.'
            ),
        ),
    );       

   /**
    * Name : uniqueAlias
    * Use : For check duplicate name of Division alias
    * @param array data
    * @return boolean
    */
    public function uniqueAlias($data){      
        $count = $this->find('count', array('conditions' => array('division_alias' => $data['division_alias'],'Division.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }         
}