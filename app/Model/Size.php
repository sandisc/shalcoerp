<?php
App::uses('Model', 'Model');
class Size extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Size";
   	var $useTable = "sizes";

     var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        ),
        'Producttaxonomy' => array(
            'className' => 'Producttaxonomy',
            'foreignKey' => 'producttaxonomy_id',
            'fields' => array('name','unit'),
        )
    );
  	
    public $validate = array(        
       'producttaxonomy_id'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Product Taxonomy.'
            ),
        ),
       'od_mm'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter OD (MM).'
            ),
        ),
       'od_nb'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter OD (NB).'
            ),
        ),
       'wt_mm'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter WT (MM).'
            ),
        ),
       'wt_nb'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter WT (NB).'
            ),
        ),      
        'calc_ratio'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Ratio.'
            ),
        ),                       
         'thickness'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Thickness.'
            ),
        ),                       
       
        'width'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Width.'
            ),
        ), 
        'length'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Length.'
            ),
        ), 
         'diameter'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Diameter.'
            ),
        ),                       
       'gdmm'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Goods Description (MM).'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueGdmm'),
                'message'=>'Goods Description (MM) is already used.',
               // 'on'=>'create'
            )
        ),
        'gdnb'=>array(
            /*'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Goods Description (NB).'
            ),*/
            'rule2'=>array(
                'rule'=>array('uniqueGdnb'),
                 'allowEmpty' => true,
                'message'=>'Goods Description (NB) is already used.',
               // 'on'=>'create'
            )
        ),    
    );
    /*Unset field if no need to validate if for certain condition*/
    public function beforeValidate($options = array()) {
 
        parent::beforeValidate($options);
        if($this->data['Size']['producttaxonomy_id'] != 1 && $this->data['Size']['producttaxonomy_id'] != 2){
            unset($this->data['Size']['od_mm']);
            unset($this->data['Size']['wt_mm']);
            unset($this->data['Size']['od_nb']);
            unset($this->data['Size']['wt_nb']);
            //unset($this->data['Size']['gdnb']);
        }
        if($this->data['Size']['producttaxonomy_id'] != 3){
            unset($this->data['Size']['width']);
            unset($this->data['Size']['thickness']);
            unset($this->data['Size']['length']);         
        }
        if($this->data['Size']['producttaxonomy_id'] != 6){
            unset($this->data['Size']['diameter']);     
        }            
        return true; //this is required, otherwise validation will always fail
    }

   /**
    * Name : uniqueGdmm
    * Use : For check duplicate name of Good Description.
    * @param array data
    * @return boolean
    */
    public function uniqueGdmm($data) {  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('gdmm' => $data['gdmm'],'Size.id !=' => $this->id)));  
        }else{
            $count = $this->find('count', array('conditions' => array('gdmm' => $data['gdmm'])));            
        }    
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

     /**
    * Name : uniqueGdnb
    * Use : For check duplicate name of Good Description.
    * @param array data
    * @return boolean
    */
    public function uniqueGdnb($data){  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('gdnb' => $data['gdnb'],'Size.id !=' => $this->id)));
        }else{
            $count = $this->find('count', array('conditions' => array('gdnb' => $data['gdnb'])));           
        }
        
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}