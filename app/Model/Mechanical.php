<?php
App::uses('Model', 'Model');
class Mechanical extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Mechanical";
   	var $useTable = "mechanicals";


   	  var $belongsTo = array(
        'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id'
        )
    );

}