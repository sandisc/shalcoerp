<?php
App::uses('Model', 'Model');
class Orderitem extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Orderitem";
   	var $useTable = "orderitems";
   	var $belongsTo = array(
        'Productcategory' => array(
            'className' => 'Productcategory',
          //  'foreignKey' => 'procatid',
            /* hscode associated with 3rd level relationship with productcategory, so we have to pass recursive 3 for fetch hscode at time of fetch order, but if we use recusrive =3 its not only fetch hscode but its also fetch grade's 3rd level related data like physical property etc, so added manuall join to fetch hscode with recursive = 2 */
            'foreignKey' => false,
            'finderQuery' => 'SELECT Productcategory.productname,Producttaxonomy.id,Producttaxonomy.unit,Hscode.id,Hscode.hscode,Hscode.rate,Standard.id,Standard.standard_name,Grade.id,Grade.grade_name,Grade.standard_id,Grade.gradetagid,Gradetag.gradetagname,Size.gdmm,Size.gdnb,Size.calc_ratio,Physical.density FROM sr_productcategories AS Productcategory,sr_hscodes as Hscode,sr_orderitems AS Orderitem ,sr_producttaxonomies AS Producttaxonomy,sr_standards AS Standard,sr_grades AS Grade LEFT JOIN sr_physicals AS Physical ON Physical.grade_id = Grade.id,sr_gradetags AS Gradetag,sr_sizes As Size WHERE Orderitem.id = {$__cakeID__$} AND Orderitem.procatid = Productcategory.id AND Productcategory.hscode_id = Hscode.id AND Productcategory.producttaxonomy_id = Producttaxonomy.id AND Orderitem.standard_id = Standard.id AND Orderitem.grade_id = Grade.id AND Grade.gradetagid = Gradetag.id AND Orderitem.size_id = Size.id',

        /*    'finderQuery' => 'SELECT Productcategory.productname,Producttaxonomy.unit,Hscode.id,Hscode.hscode,Hscode.rate FROM sr_productcategories AS Productcategory,sr_hscodes as Hscode,sr_orderitems,sr_producttaxonomies AS Producttaxonomy WHERE sr_orderitems.id = {$__cakeID__$} AND sr_orderitems.procatid = Productcategory.id AND Productcategory.hscode_id = Hscode.id AND Productcategory.producttaxonomy_id = Producttaxonomy.id',*/
        ),        
        /*'Grade' => array(
            'className' => 'Grade',
            'foreignKey' => 'grade_id',
            'fields' => array('id','grade_name','standard_id','gradetagid'),
        ),
        'Standard' => array(
            'className' => 'Standard',
            'foreignKey' => 'standard_id',
            'fields' => array('id','standard_name'),
        ),
         'Size' => array(
            'className' => 'Size',
            'foreignKey' => 'size_id'
        )*/
    );

   /*	public $validate = array(
        'standard_id' => array(
            'required' => array(
            	'rule'=>array('notBlank'),
                'message' => 'Please Select Standard name'
            ),
        ),
        'grade_id' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please Select Grade name'
            ),
        ),
        'od_nb' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please enter OD.'
            ),
        ),
        'od_mm' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter OB(MM)'
            ),
        ),
        'wt_nb' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter WT'
            ),
        ), 
        'wt_mm' => array(
            'required' => array(
            'rule' =>array('notBlank'),
            'message' => 'Please enter WT(MM)'
            ),
        ),
        'length' => array(
            'required' => array(
            'rule' => array('notBlank'),
            'message' => 'Please enter Length'
            ),
        ),    
               
    );*/

    
}