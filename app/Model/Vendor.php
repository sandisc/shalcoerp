<?php

App::uses('Model', 'Model');

class Vendor extends AppModel {

	var $name = "Vendor";
    var $useTable = "vendors";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );
   	public $validate = array(
        'vendorname'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Vendor name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Vendor name is already used.',
            )
        ),        
    );   

   /**
    * Name : uniqueNo
    * Use : For check duplicate name of vendor
    * @param array data
    * @return boolean
    */
    public function uniqueNo($data){      
        $count = $this->find('count', array('conditions' => array('vendorname' => $data['vendorname'],'Vendor.id !=' => $this->id)));
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }     
}