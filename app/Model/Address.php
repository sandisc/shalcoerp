<?php
App::uses('Model', 'Model');
class Address extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Address";
   	var $useTable = "addresses";
   	var $virtualFields = array('full_address' => 'CONCAT(Address.streetaddress, ",", Address.city,",",Address.state," , ",Address.country)');
}