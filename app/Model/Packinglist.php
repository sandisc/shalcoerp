<?php
App::uses('Model', 'Model');
class Packinglist extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Packinglist";
   	var $useTable = "packinglists";
      var $hasMany = array(
        'Packingitem' => array(
            'className' => 'Packingitem',
            'foreignKey' => 'packingid'
        ),
  );

        
   	var $belongsTo = array(
       'Invoice' => array(
            'className' => 'Invoice',
            'foreignKey' => 'invoiceid',
        )
    );
}