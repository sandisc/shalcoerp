<?php
App::uses('Model', 'Model');
class Reportcategory extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Reportcategory";
   	var $useTable = "reportcategories";
    var $primaryKey="id";
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        )
    );

}