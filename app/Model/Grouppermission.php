<?php
/**
* Name : Grouppermission
* Use : For validation of Grouppermission field
*/

class Grouppermission extends AppModel {

	var $name = "Grouppermission";

    var $useTable = "grouppermissions";    /* Define Table Name */

    public $validate = array(
        'usertype_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select user type'
            )
        ), 
        'access_modules' => array(
            'required'=>array(
                'rule'=>array('checkmodules'),
                'message'=>'Please give permission'
            )
        ),       
    );

    function checkmodules() {
        if(empty($this->data['Grouppermission']['access_modules'])) {
            return false;
        }
        return true;
    }
}