<?php
App::uses('Model', 'Model');
class Order extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Order";
   	var $useTable = "orders";
    var $hasMany = array(
        'Orderitem' => array(
            'className' => 'Orderitem',
            'foreignKey' => 'orderid'
        ),
    );
    var $belongsTo = array(
       'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'bill_to',
            'fields' => array('Client.id,Client.company_name,Client.vat_tin_no'),
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),
        'Bank' => array(
            'className' => 'Bank',
            'foreignKey' => 'bank_id'
        ),
        'Dispatch' => array(
            'className' => 'Dispatch',
            'foreignKey' => 'dispatch_id'
        ),
        'Price' => array(
            'className' => 'Price',
            'foreignKey' => 'priceid'
        ),


    );
    public $validate = array(
       'orderno'=>array(
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Order No is already used.',
                //'on'=>'create'
            )
        ),
        'tod' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of delivery.'
            ),
        ),
        'top' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of payment.'
            ),
        ),
        'bill_to' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select bill to.'
            ),
        ),
          'ship_to' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter ship to.'
            ),
        ),
        'bill_address' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter bill address.'
            ),
        ),
        /*'ship_address' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter ship address.'
            ),
        ),*/
        'priceid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select price unit.'
            ),
        ),
        'bank_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select bank.'
            ),
        ),         
        'standard_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select Standard.'
            ),
        ),   
               
        
    );

         /**
    * Name : uniqueNo
    * Use : For check duplicate id of  Order no.
    * @param array data
    * @return boolean
    */
     public function uniqueNo($data) 
    {  
      
        $count = $this->find('count', array('conditions' => array('orderno' => $data['orderno'],'Order.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }


}