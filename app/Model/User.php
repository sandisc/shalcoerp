<?php
App::uses('Model', 'Model');
class User extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "User";
   	var $useTable = "users";


   	var $belongsTo = array(
        'Usertype' => array(
            'className' => 'Usertype',
            'foreignKey' => 'usertype_id'
        ),

    );
    var $hasOne = array(
        /*'Users' => array(
                'className' => 'User',
                'foreignKey' => 'modifiedby',
                'fields' => array('email')
            ),*/
         'Userpermission' => array(
            "className" => "Userpermission",
            "foreignKey" => "user_id"
            )
        );
    
   	public $validate = array(
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter password.'
            ),
            'between' => array(
                'rule'    => array('between', 8, 16),
                'message' => 'Password should be minimum 8 characters long.'
            )
        ),
        'newpassword' => array(
            'between' => array(
                'rule'    => array('between', 8, 16),
                'message' => 'Password should be minimum 8 characters long.'
            )
        ),
        'conpassword'=>  array(
            'required'=>array(
                             'rule'=>array('notBlank'),
                             'message'=>'Please enter confirm password.'
            ),
            'minLength' => array(
                            'rule' => array('minLength', 8),
                            'message' => 'Password should be minimum 8 characters long.'
            ),
           'compare'=>array(
                             'rule'=>'matchPasswords',
                             'message'=>"Confirm password does not match."
            )
        ),        
        'email' => array(
            'required'=>array(
                'rule' => array('notBlank'),
                'message'=>'Please enter email.'
            ),
            'valid_email' => array(
                'rule' => '/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,4})$/',
                'message' => 'Please enter a valid email address.',
            ),
            'unique' => array(
                'rule' => array('isunique'),
                'message' => 'Email address is already in use.',
            ),          
        ),
        'first_name' => array(
            'required'=>array(
                'rule' => array('notBlank'),
                'message'=>'Please enter first name.'
            )
        ),       
        'last_name' => array(
            'required'=>array(
                'rule' => array('notBlank'),
                'message'=>'Please enter last name.'
            )
        ),
    );

    /*
     * Name : beforeSave
     * Use : save password using Auth Component
     * @param array data
     * @return boolean
     */
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

     /*
         * Name : matchPasswords
         * Use : match password and confirm password
         * @param array data
         * @return boolean
         */
        public function matchPasswords($data){
          if($data['conpassword'] == $this->data['User']['password'])
          {
              return true;
          }
          return false;
      }        

    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['con_password'];
    }

    
}