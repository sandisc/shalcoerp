<?php
App::uses('Model', 'Model');
class Client extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Client";
   	var $useTable = "clients";
    var $hasMany = array(
        'Address' => array(
            'className' => 'address',
            'foreignKey' => 'clientid'
        ),
        
    );

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('first_name','last_name'),
        ),
        'Coordinator' => array(
            'className' => 'User',
            'foreignKey' => 'coordinator',
            'fields' => array('first_name','last_name','email'),            
        ),
    );
    
   	public $validate = array(
        'company_name'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter company name.'
            ),
            'rule3'=>array(
                'rule'=>array('uniqueCmp'),
                'message'=>'Company name is already used.'
            ),
        ),
        'contact_person' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please enter contact person.'
            ),
        ),
        'email' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter email.'
            ),
            'valid_email' => array(
                'rule' => '/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,4})$/',
                'message' => 'Please enter a valid email address.',
            ),
            /*'unique' => array(
                'rule' => array('isunique'),
                'message' => 'Email address is already in use.',
            ),*/        
        ),
        'contact_num1' => array(
            'required' => array(
                'rule'=>array('notBlank'),
                'message' => 'Please enter contact number.'
            ),
        ),
        'coordinator' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select co-ordinator.'
            ),
        ),
       /* 'address1' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter address.'
            )           
        ),
        'industryid' => array(
            'required'=>array(
                'rule' => array('multiple', array('min' => 1)),                
                'message'=>'Please select atleast one industry.'
            )           
        ),
        'vendorid' => array(
            'required'=>array(
                'rule' => array('notBlank'),                
                'message'=>'Please select vendor.'
            )           
        ),*/
        'procatid'=>array(
            'required'=>array(
                'rule' => array('multiple', array('min' => 1)),                
                'message'=>'Please select atleast one product category.'
            )
        )     
    );
   
   /**
    * Name : uniqueCmp
    * Use : For check duplicate name of Client.
    * @param array data
    * @return boolean
    */
    public function uniqueCmp($data) 
    {        
        $count = $this->find('count', array('conditions' => array('company_name' => $data['company_name'],'Client.id !=' => $this->id)));
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }
}