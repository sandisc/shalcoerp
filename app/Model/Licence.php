<?php
App::uses('Model', 'Model');
class Licence extends AppModel {
   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Licence";
   	var $useTable = "licences";
    var $hasMany = array(
        'Licenceitem' => array(
            'className' => 'Licenceitem',
            'foreignKey' => 'licenceid'
        ),
        
    );

   	var $belongsTo = array(
         'Hscode' => array(
            'className' => 'Hscode',
            'foreignKey' => 'hscode_id',
            'fields' => array('id','hscode','rate'),
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),

    );
    public $validate = array(
       'advancelicenceno'=>array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Licence No.'
            ),            
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Licence No is already used.',
            )
        ),
        'issue_date' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Issue date'
            ),
        ),
        'validity' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter validity.'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please enter only numeric value.'
            )
        ),
        'expiry_date' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter expiry of date.'
            ),
        ),
        'hscode_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select Hscode.'
            ),
        ),       
    );
/**
    * Name : uniqueNo
    * Use : For check duplicate id of Licence No.
    * @param array data
    * @return boolean
    */  
    public function uniqueNo($data){  
        if(!empty($this->id)){
            $count = $this->find('count', array('conditions' => array('advancelicenceno' => $data['advancelicenceno'],'Licence.id !=' => $this->id)));
        }else{
            $count = $this->find('count', array('conditions' => array('advancelicenceno' => $data['advancelicenceno'])));           
        }      
        //$count = $this->find('count', array('conditions' => array('advancelicenceno' => $data['advancelicenceno'],'Licence.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

}