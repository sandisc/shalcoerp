<?php
App::uses('Model', 'Model');
class Proformainvoice extends AppModel {

   	/**
     * Used for fetch data from Db and insert data in Db 
     */
    var $name = "Proformainvoce";
   	var $useTable = "proformainvoices";
     var $hasMany = array(
        'Proformaitem' => array(
            'className' => 'Proformaitem',
            'foreignKey' => 'proforma_id'
        ),
        
    );
   	var $belongsTo = array(
       'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'bill_to',
            'fields' => array('Client.id,Client.company_name,Client.address1,Client.address2'),
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modifiedby',
            'fields' => array('User.first_name,User.last_name,User.email'),
        ),
        'Bank' => array(
            'className' => 'Bank',
            'foreignKey' => 'bank_id'
        ),
        'Dispatch' => array(
            'className' => 'Dispatch',
            'foreignKey' => 'dispatch_id'
        ),
        'Price' => array(
            'className' => 'Price',
            'foreignKey' => 'priceid'
        ),
    );

    public $validate = array(
       'proforma_no'=>array(
            'rule3'=>array(
                'rule'=>array('uniqueNo'),
                'message'=>'Proforma No is already used.',
                //'on'=>'create'
            )
        ),
        'proforma_date' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter date.'
            ),
        ),
        'tod' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of delivery.'
            ),
        ),
        'top' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter terms of payment.'
            ),
        ),
        'bill_to' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select bill to.'
            ),
        ),
        'ship_to' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter ship to.'
            ),
        ),
        'bill_address' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter bill address.'
            ),
        ),
        'ship_address' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter ship address.'
            ),
        ),
        'priceid' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select price unit.'
            ),
        ),
        'bank_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select bank.'
            ),
        ),         
        'standard_id' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please select Standard.'
            ),
        ),   
        'quantity' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Quantity.'
            ),
        ),
        'delivery_period' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Delivery Period.'
            ),
        ),   
        'dimensions' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Dimensions.'
            ),
        ),
        'certifications' => array(
            'required'=>array(
                'rule'=>array('notBlank'),
                'message'=>'Please enter Certifications.'
            ),
        ),
    );

  /**
    * Name : uniqueNo
    * Use : For check duplicate id of Proforma no.
    * @param array data
    * @return boolean
    */  
     public function uniqueNo($data) 
    {  
      
        $count = $this->find('count', array('conditions' => array('proforma_no' => $data['proforma_no'],'Proformainvoice.id !=' => $this->id)));                
        if($count == 0){
            return true;
        }
        else{
           return false; 
        }
    }

    
}