<?php
/*
 * @Controller name: Item Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Item management.
 */

App::uses('AppController', 'Controller');
class ItemsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Item Management');
	}
	/*
   	 * Name: index
   	 * Use: Item view.
	 */
	function index() {
        $this->set('pageTitle',' Item List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','standard_name','grade_name','od_nb','wt_nb','od_mm','wt_mm','calc_ratio','Item.modified','first_name');
    	$order = array('Item.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Item->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$items = $this->Item->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		//$log = $this->User->getDataSource()->getLog(false, false);
		//debug($log); exit;
		$this->set('item',$items);
		$this->set('count',$count);
		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update item.
	 */
	function add($id = null) {
		$this->loadmodel('Standard');
	    $id = base64_decode($id);

		if(!empty($this->request->data)) {  
			$this->Item->set($this->request->data);
			$logged_user = $this->Auth->user('id');
			$this->request->data['Item']['createdby'] = $logged_user;
			if($this->Item->validates($this->request->data)) {
				if(!empty($id)) {
					$this->request->data['Item']['id'] = $id;
					$this->request->data['Item']['modifiedby'] = $logged_user;	
				}
				if($this->Item->save($this->request->data)) {
					if(!empty($id)) { 
						$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Item has been Updated</div>'));
					}
					else { 
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Item has been Added</div>'));
					}
					$this->redirect('/items/index');
				}
			}
			else
				{
					 $errors = $this->Item->validationErrors;
				}
		}
		if(!empty($id)) {
		$this->set('pageTitle','Edit Item');
		$this->request->data = $this->Item->findById($id);
				if(empty($this->request->data))
				{
					$this->redirect('index');
				}
		}
		else{
			$this->set('pageTitle','Add Item');
		
		}
		$this->set('id',$id);
		$options = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
		$this->set('options',$options);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'items/">Items</a><i class="fa fa-circle"></i></li>');		
	}
	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGrades($id) {
		$this->loadmodel('Grade');
		$this->autoRender = false;
   		$grade_data = $this->Grade->find('all',array('fields' => array('Grade.id','Grade.grade_name'),'conditions'=> array('Grade.standard_id'=>$id)));
   		
   		echo '<select>';
		echo '<option value="">Select Grade</option>';
			foreach($grade_data as $grades) 
			{ 
	  		echo '<option value="'.$grades['Grade']['id'].'">'.$grades['Grade']['grade_name'].'</option>'."\n";
	  		}
		echo '</select>';
		exit;
	}

	/**
   	 * Name: delete
   	 * Use: delete item
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Item->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Item has been Deleted successfully</div>'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
}