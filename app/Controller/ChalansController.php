<?php
/*
 * @Controller name: Chalan Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Chalan management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Orders');
App::import('Controller', 'Activities');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));

class ChalansController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Delivery Chalan Management');
	}
	/*
   	 * Name: index
   	 * Use: Chalan listing page.
	*/
	function index() {
        $this->set('pageTitle','Delivery Chalan List');
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','chalanno','chalandate','division_name','orderno','ship_to','totalqty','totalamount','Chalan.modified','first_name');
    	$order = array('Chalan.modified' => 'desc');
		$res = $this->datatable_append($order,$column);	
		$count = $this->Chalan->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$chalans = $this->Chalan->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));

		$this->set('chalan',$chalans);
		$this->set('count',$count);
	}

	/**
   	 * Name: View
   	 * Use: View details of chalan.
     * @param int $id id of record
     * @return detail record of id
	*/
	function view($id = null) {
		$id = base64_decode($id);
		
		if(empty($id)) {
			$this->redirect(array('controller' => 'Chalans', 'action' => 'index'));
		}
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'chalans/">Chalans</a><i class="fa fa-circle"></i></li>');
		$chalan = $this->Chalan->find('first', array(
   			'conditions' => array('Chalan.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		$this->set('view_chalan',$chalan);
		$this->set('pageTitle','Chalan View');
	}
	/*
   	 * Name: Print
   	 * Use: user can Print all Details of Chalan.
	*/
	function prints($id = null) {
		$id = base64_decode($id);
		
		if(empty($id)) {
			$this->redirect(array('controller' => 'Chalans', 'action' => 'index'));
		}
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'chalans/">Chalans</a><i class="fa fa-circle"></i></li>');
		$chalan = $this->Chalan->find('first', array(
   			'conditions' => array('Chalan.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		
		$this->set('view_chalan',$chalan);
		$this->set('pageTitle','Chalan');
	}
	/*
   	 * Name: add
   	 * Use: user can add,update invioce and Unique Chalan id.
	*/
	function add($id = null) {
		$id = base64_decode($id);
		$this->set('id',$id);
		$this->loadmodel('Division');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
		$this->loadmodel('Chalanitem');
		$this->set('pageTitle','Add Delivery Chalan');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'chalans/">Delivery Chalan</a><i class="fa fa-circle"></i></li>');

		$orderitem = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive' => 2));
		$this->set('order_item',$orderitem);
		/*If Dummy Id OR Id not exists than*/
		if(empty($orderitem)){
			$this->Session->setFlash(__('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Record not exists</div>'));        	
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$division = $this->Division->find('list',array('fields' => array('id','division_name')));
		$this->set('division',$division);

		$chalan_id = $this->request->data['Chalan']['chalanno'] = $this->getChalanNO();
		$this->set('chalan',$chalan_id);
	}
	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->loadmodel('Order');
		$this->loadmodel('Client');
		$this->loadmodel('Orderitem');
		$this->loadmodel('Chalanitem');
	    $this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
		if(!empty($this->request->data)) { 
			$chalan = $this->request->data['Chalan'];
			$date = $chalan['chalandate'];
			$chalan['chalandate'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
			$chalan['chalanno'] = $this->getChalanNO('',$chalan['chalandate'],$chalan['division_id']);
			$chalan['fy'] = $this->calculateFinancialYearForDate($chalan['chalandate']);

			$this->Chalan->set($chalan);
			$orderid = $this->request->data['Chalan']['orderid'];
			$response=array();
			if($this->Chalan->validates($chalan)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$chalan['Chalan']['createdby'] = $logged_user;
				$chalan['Chalan']['modifiedby'] = $logged_user;
				
				$this->Chalan->save($chalan); /*save chalan related data in chalan table*/

				$lastid = $this->Chalan->getLastInsertID();
				$chalan_data = $this->Chalan->find('all', array('limit' => 1,'order' => 'Chalan.id DESC'));
				$item = $this->request->data['Chalan']['item'];
	            $sum = 0;
	            $total = 0;

	           	foreach($item as $items){	
	           		$items['chalanid'] = $lastid;
	           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
	           			$this->Chalanitem->saveall($items);/*save chalanitem related data in chalan item table*/		           		
		           	}
	           	}
       			$updatechalanitemdata = array();
	           	foreach($item as $items){	
	           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           		$items['orderitemid'] = $items['orderitemid'];
		           		$updatechalanitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
		           	}
	           	}

				$this->Orderitem->saveall($updatechalanitemdata);
		        /*End Update order item status and delivered quantity*/
		        $orders = new OrdersController; 
		        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its status, change order status*/

				$chalan['item'] = $item;/*updated value so we pass it into activity log*/
	           	$chalan['Chalanitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Chalan','Add',$chalan);/*Add Data in Log*/

	           	$clientdetails = $this->Client->find('first',array('conditions' => array('Client.id' => $this->request->data['client_id']), 'fields'=>array('company_name'),'recursive'=>-1));	           	
		        
	           		/*Send email to account section and QC section to inform check delivery chalan is proper or not*/
                    $log_first = AuthComponent::user('first_name');
                    $log_last = AuthComponent::user('last_name');
                    $Email = new CakeEmail();
                    $Email->to('accounts@shalco.in','qc@shalco.in','sandyross.it@gmail.com');
                    $Email->subject('[SHALCO ERP] Verify Delivery Chalan');
                    $Email->from(array('erp@shalco.in' => 'Shalco Industries Pvt. Ltd.'));
                    $Email->emailFormat('html');
                    $url = WEBSITE_PATH . "chalans/export/" . $lastid;
                    $verify_url = '<a href="' . $url . '">' . $url . '</a>';
                    $Email->send('Respected Sir <br><br><br>' .
                            'As per task assigned to me, I have created a Delivery Chalan For Client :-'.$clientdetails['Client']['company_name'].' of Rupees '.$chalan['totalamount'].'.<br>
					  		Please check its detail view from here :<a href="' . WEBSITE_PATH . "chalans/view/" . base64_encode($lastid) . '" target="_blank">View Delivery Challan </a>' . '<br><br> 
					  		You can download it from here :<a href="' . $url . '" target="_blank">Download Delivery Challan </a>' . '<br><br> Thanks & Regards<br>'. $log_first . ' ' . $log_last);


		        $msg = 'Chalan has been saved successfully';
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'Chalan';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
	        }
	        else{
	        	$Chalan = $this->Chalan->validationErrors;
	        	$response['status'] = 'error';
                $response['message'] = 'The Chalan could not be saved. Please, try again.';
                $response['data'] = compact('Chalan');
                echo json_encode($response);
	        }
		}			
	}

	/**
   	 * Name: edit
   	 * Use: update chalan.
     * @param int $id id of record
     * @return detail record of id   	 
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'chalans', 'action' => 'index'));
		}	
		$this->loadmodel('Client');
		$this->loadmodel('Proformainvoice');
		$this->loadmodel('Chalanitem');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
	    $this->loadmodel('Division');
		$this->Client->unbindModel(array('belongsTo' => array('User','Coordinator')));
		$this->Order->unbindModel(array('belongsTo' => array('Dispatch','Bank','User')));
		$this->Chalan->unbindModel(array('hasMany' => array('Chalanitem')));		
		$this->Orderitem->bindModel(array('hasOne' => array('Chalanitem' => array(
            'className' => 'Chalanitem',
            'foreignKey' => 'orderitemid',
            'conditions'=>array('Chalanitem.chalanid' => $id)
        ))));

	    $chalan_data = $this->Chalan->find('first',array('conditions' => array('Chalan.id' => $id ),'recursive'=>3));
		if(empty($chalan_data)){
			$this->redirect(array('controller' => 'chalans', 'action' => 'index'));
		}	    
        $this->set('chalan_data',$chalan_data);
		
		$division = $this->Division->find('list',array('fields' => array('id','division_name')));
		$this->set('division',$division);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'chalans/">Delivery Chalan</a><i class="fa fa-circle"></i></li>');		
		$this->set('pageTitle','Edit Chalan');
		
	}
	/*Form submit usign ajax*/
	function ajaxeditsubmit(){
		$this->loadmodel('Client');
		$this->loadmodel('Proformainvoice');
		$this->loadmodel('Chalanitem');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
		$this->autoRender = false;
		if(!empty($this->request->data)) {  
			$this->Chalan->set($this->request->data);
			if($this->Chalan->validates($this->request->data)) {
				$Activities = new ActivitiesController;

				$id = $this->request->data['Chalan']['id'];
				$logged_user = $this->Auth->user('id');
				$this->old = $this->Chalan->find('first', array(
		 											'conditions' => array('Chalan.id' => $id ),
													'recursive' => 2));

				$this->request->data['Chalan']['modifiedby'] = $logged_user;
				$chalans = $this->request->data['Chalan'];
				
				$date = $this->request->data['Chalan']['chalandate'];
				$orderid = $this->request->data['Chalan']['orderid'];
				$chalans['chalandate'] = date('Y-m-d', strtotime(str_replace('/', '-', $date)));

				/*check old and new chalans number is same or not. if not than we will calling to get new chalans number again. Reason is to avoid duplicacy if same time 2 or more user calling same function*/
				if($this->old['Chalan']['chalanno'] != $chalans['chalanno']){
					$chalan['chalanno'] = $this->getChalanNO('',$chalans['chalandate'],$chalans['division_id']);
					$chalans['fy'] = $this->calculateFinancialYearForDate($chalans['chalandate']);
				}
				
				$this->Chalan->save($chalans);/*Save chalan data*/
	            $item = $this->request->data['Chalan']['item'];
	               	foreach($item as $items){	
		           		if(isset($items['id']) && !empty($items['id'])){/*Update items info which is already exist in chalan*/
		           			$items['chalanid'] = $id;	
	 	           			$this->Chalanitem->save($items,array('Chalanitem.id' => $items['id']));
		           		}
		           		else{/*Add items info which is already exist in order but not in chalan*/
		           			$items['chalanid']  = $id;
		           			if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           				$this->Chalanitem->saveall($items);
		           			}
		           		}
		          	}

		        /*Start Update order item status and delivered quantity*/
	           	$updateorderitemdata = array();
	           	foreach($item as $items){	
	           		$items['orderitemid'] = $items['orderitemid'];
	           		$updateorderitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
	           	}
	           	$this->loadmodel('Orderitem');
				$this->Orderitem->saveall($updateorderitemdata);
		        /*End Update order item status and delivered quantity*/

		        $orders = new OrdersController; 
		        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its */
		       	$msg = 'Chalan has been Updated successfully';      
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));

	           	//$this->redirect('index');
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);		           	
	        }
	        else{
                $Chalan = $this->Chalan->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Chalan could not be saved. Please, try again.';
                $response['data'] = compact('Chalan');
                echo json_encode($response);
	        }
		}
	}

   /**
	* Name : getChalanNO
	* Use : Get Unique chalan No for chalan
	*/
	function getChalanNO($type=null,$date=null,$division_id=1){
		if($type == 'ajax'){ /*For on change event inside add and edit chalan form*/
			$this->autoRender=false;
		}
		$this->loadmodel('Division');		
		if(isset($_POST['division_id'])){ $division_id = $_POST['division_id'];}

		/*Default current date*/
		if(empty($date)){$date = date('Y-m-d');}
		else{
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
		}  
		/*If user fill date than override default date with user entered date*/
		if(isset($_POST['date']) && !empty($_POST['date'])){ 
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
		}

		$divisiondata = $this->Division->find('first',array('conditions' => array('id'=>$division_id),'fields'=>array('division_alias'),'recursive'=> -1));

		$new_finan_year = $this->calculateFinancialYearForDate($date);	
		$chalan_id = $this->Chalan->find('first',array('conditions' => array('division_id'=>$division_id,'Chalan.fy' => $new_finan_year),'fields'=>array('Chalan.chalanno','Chalan.fy'),'recursive'=> -1,'order' => 'Chalan.id DESC'));
		
		if(!empty($chalan_id) && $chalan_id['Chalan']['fy'] == $new_finan_year){
			$in_id = $chalan_id['Chalan']['chalanno'];
			$exp_id = explode('/',$in_id);
			$inv_id = $exp_id[1] + 1;
			$year = date("Y");
			$pr_id = $divisiondata['Division']['division_alias'].'/'.$inv_id.'/'.$this->calculateFinancialYearForDate($date);			
		}	
		else{
			$year = date("Y");
			$pr_id = $divisiondata['Division']['division_alias'].'/'.'1/'.$this->calculateFinancialYearForDate($date);			
		}
		return $pr_id;
	}

	/**
   	 * Name: countdeliveredqty
   	 * Use: count delivered quantity of particular order item
     * @param int $orderitemid orderitemid of record
     * @param decimal $qty qty of orderitem
     * @return array with orderitem id,delivered qty and status.   	 
	*/
	function countdeliveredqty($orderitemid,$qty){
		$this->loadmodel('Chalanitem');
		$itemdata = $this->Chalanitem->find('all',array('conditions' => array('orderitemid'=>$orderitemid)));
		$dlvr_qty = 0;
		foreach ($itemdata as $key => $value) {
			$dlvr_qty+=$value['Chalanitem']['qty_mtr'];
		}
		if($dlvr_qty > $qty || $dlvr_qty == $qty){
			$status = 2; //close
		}else{
			$status = 1; //process
		}
		return array('id'=>$orderitemid,'dlvr_qty'=>$dlvr_qty,'status'=>$status);
	}

  /**
	* Name : Chalanoforder
	* Use : Show Invoices of particular order
 	* @param int $orderid orderid of record
    * @return array with order and chalan details of particular order.
	*/
	function chalanoforder($orderid){
		$this->loadmodel('Order');
		$this->loadmodel('Client');
		$this->loadmodel('Orderitem');
		$this->set('pageTitle','Chalan of Order');
		
		$id = base64_decode($orderid);
		$this->Orderitem->bindModel(array('hasMany' => array('Chalanitem' => array(
            'className' => 'Chalanitem',
            'foreignKey' => 'orderitemid'
        ))));
		$orders = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive'=>2));
		if(empty($orders)){//if no order than 
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$clients = $this->Client->find('first',array('conditions' =>array('Client.id' => $orders['Client']['id'])));
		$this->set('orders',$orders);
		$this->set('clients',$clients);
		$this->set('ajaxaction','chalanlisting/'.$id);
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function chalanlisting(){
		$id = $this->params['pass'][0];/*Order id*/
    	$column = array('id','chalanno','chalandate','totalqty','totalamount','Chalan.modified','first_name');
    	$order = array('Chalan.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Chalan->find('count',array('conditions'=>array($res['con'],'Chalan.orderid'=>$id),'order'=>$res['order_by']));
		$chalans = $this->Chalan->find('all',array('conditions'=>array($res['con'],'Chalan.orderid'=>$id),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));		
		$this->set('chalan',$chalans);
		$this->set('count',$count);
	}	

	/*
	* Name : export
	* Use : Export the Order data
	*/
	function export($id = null) {
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$setting= $this->Session->read('setting_data');

		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

		$results = $this->Chalan->find('first',array('conditions'=>array('Chalan.id' => $id),'recursive'=>3));

		$clientinfo = $this->requestAction('App/getClientInfo/'.$results['Order']['bill_to']);

		$reportname = 'Chalan-'.$results['Chalan']['chalanno'];
		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

		$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
		$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
		$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
		$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
		$styleArray = array(
		    'font'  => array(
		        'bold'  => true,
		        'size'  => 12,
		        'name'  => 'Calibri'
		    )
		);
		$headerstyleArray = array(
		    'font'  => array(
		        'bold'  => true,
		        'size'  => 16,
		        'name'  => 'Calibri'
		    ),
		    'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		    					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)
		);

			$maxExcelColumn = 'J';
			
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'img/print_logo.png';
			$objDrawing->setPath($path);
			$objDrawing->setCoordinates('A1');
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1','CHALLAN # '.$results['Chalan']['chalanno']."\n".''."".'DATE : '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Chalan']['chalandate'])));
			//$objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G1:'.$maxExcelColumn.'5');
			//$objPHPExcel->getActiveSheet()->getStyle('G1:'.$maxExcelColumn.'5')->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('G1:'.$maxExcelColumn.'5')->applyFromArray($headerstyleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F5');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', "Office: 24, 2nd Floor, Dayabhai Building Gulalwadi, Mumbai - 400004 Maharastra, (INDIA)."."\n"."Tel: + 91-22-40416214/ 40416221/ 25 Fax: +91-22-23452161"."\n"."Email: info@shalco.in Web: www.shalco.in");
			$objPHPExcel->getActiveSheet()->getStyle('A6:'.$maxExcelColumn.'8')->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('A6:'.$maxExcelColumn.'8')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:'.$maxExcelColumn.'8');
			
			$dyn_cell = 9;

	/*start apply boldness in top section */	
		//$objPHPExcel->getActiveSheet()->getStyle('11')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			//$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true)->setSize(8);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,'SUBJECT TO MUMBAI JURISDICTION')->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'DELIVERY CHALLAN')->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'M/s '.$results['Order']['Client']['company_name'])->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$cl_address = str_replace('<br/>', "\n", searchFromAddress($results['Order']['billaddress_id'], $results['Order']['Client']['Address']));
			$count_buyer = substr_count( $cl_address, "\n" );
			//echo $count_buyer;exit;
			if($count_buyer == 0){
				$for_new2 = $dyn_cell + 1;
			}else{
				$for_new2 = $dyn_cell + $count_buyer;	
			}
			$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$dyn_cell,$cl_address)->mergeCells('A'.$dyn_cell.':F'.++$for_new2);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell,'SHALCO ORDER NO.')->mergeCells('G'.$dyn_cell.':H'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$dyn_cell,$results['Order']['orderno']);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':H'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'')->mergeCells('A'.$dyn_cell.':F'.$dyn_cell);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.++$dyn_cell,'SHALCO ORDER DATE')->mergeCells('G'.$dyn_cell.':H'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.++$dyn_cell,'')->mergeCells('G'.$dyn_cell.':H'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->getFont()->setBold(true);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$dyn_cell,$this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Order']['orderdate'])));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$dyn_cell,'');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':H'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('G'.++$dyn_cell.':H'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('G'.++$dyn_cell.':H'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$dyn_cell = $for_new2;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'Client PAN NO. '.$clientinfo['Client']['pan_no'])->mergeCells('A'.$dyn_cell.':C'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$dyn_cell.':E'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$dyn_cell,'Client TIN NO. '.$clientinfo['Client']['vat_tin_no'])->mergeCells('D'.$dyn_cell.':F'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell,'Client GST NO. '.$clientinfo['Client']['gst_no'])->mergeCells('G'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell.':H'.$dyn_cell)->applyFromArray($border_left);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'Please Receive the undermentioned goods per your order '.$results['Order']['clientpo'].' dt '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Order']['clientpodate'])))->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell,'and returns us the duplicate copy duly signed')->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);

			$headerRow = "Sr NO.,Product Description,,,GOOD DESCRIPTION,,PCS,QTY,PRICE (₹),NET AMOUNT(₹)";
			$key= ++$dyn_cell;
			$maxSize = explode(",", $headerRow);
			for($i=0,$j='A';$i<sizeof($maxSize);$i++,$j++) {
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$key, $maxSize[$i]);
					if($maxSize[$i] != 'Product Description'){
							$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$key.':'.'D'.$key);
					}
					if($maxSize[$i] != 'GOOD DESCRIPTION'){
							$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$key.':'.'F'.$key);
					}
					$objPHPExcel->getActiveSheet()->getStyle($j.$key, $maxSize[$i])->applyFromArray($border_left);
					$objPHPExcel->getActiveSheet()->getStyle($j.$key, $maxSize[$i])->applyFromArray($border_top);
					$objPHPExcel->getActiveSheet()->getStyle($j.$key)->applyFromArray($style_vertical_center);
					$objPHPExcel->getActiveSheet()->getStyle($j.$key)->applyFromArray($style);
					$objPHPExcel->getActiveSheet()->getStyle($j.$key)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle($j.$key)->getAlignment()->setWrapText(true);
			}
			$chalanitem = $results['Order']['Orderitem'];
			$chal_items = $results['Chalanitem'];
			$dataArray = array();
			$idx = '1';
			$index = '1';
			$k = 0;
			$i = 0;
			$j=0;			
			foreach($chalanitem as $v) {
				if(isset($chal_items[$i]['orderitemid'])){
	                if($chal_items[$i]['orderitemid'] == $v['id']){ 
					$dataArray[$index][$k] = $chalanitem[$i]['sr_no'];
					$dataArray[$index][++$k] = $chal_items[$i]['label_productname'];
					$dataArray[$index][++$k] = ''; 
					$dataArray[$index][++$k] = ''; 
					$dataArray[$index][++$k] = setGDmmformat($chalanitem[$j]['Productcategory']['Size']['gdmm'],$chalanitem[$j]['length']);
					$dataArray[$index][++$k] = ''; 
					$dataArray[$index][++$k] = $chal_items[$i]['pcs'];
					$dataArray[$index][++$k] = decimalQty($chal_items[$i]['qty_mtr']).' '.$chalanitem[$j]['Productcategory']['Producttaxonomy']['unit'];
					$dataArray[$index][++$k] = decimalPrice($chal_items[$i]['price']);
					$dataArray[$index][++$k] = decimalPrice($chal_items[$i]['netprice']);
					$idx++;
					$index++;
					++$i;
					$k = 0;
			 		} 
                ++$j;
                }
            } 
			$id = ++$dyn_cell;
			$size = count($dataArray);

			foreach ($dataArray as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						if($j != 'B' && $j != 'C' && $j != 'E'){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_right);
							$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$id.':'.'D'.$id);
							$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$id.':'.'F'.$id);
						}
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_bottom);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, $value[$i]);
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(12);

						$ex = explode('.', $value[$i]);
						/*Start logic for add decimal position because excel removed automatically if value has empty zero like its show 5.00 as 5*/
						if(isset($ex[1])){
							if(is_numeric($ex[1])){
								$zero = '';
								if(strlen($ex[1]) == 2){
									$zero = '0.00';
								}
								if(strlen($ex[1]) == 3){
									$zero = '0.000';
								}
								$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left)->getNumberFormat()->setFormatCode($zero);	
							}					
						}
						/*End logic of add decimal position with zeros*/

						/*PHP_Excel default set numberic value to align right, while string value to align left*/
						/*Set quntity align right*/
						if($i == 7){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
						}										
					}
					else {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, '');
					}
				}
				$id++;
			}
			$nxt_val = $id;
			//$total = $size + 7 + 1;
			$num = $this->requestAction('App/numtowords/'.$results['Chalan']['totalamount'].'/'.$results['Order']['Price']['fullform']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'Ruppes(₹) '.$num)->mergeCells('A'.$nxt_val.':H'.$nxt_val);;

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$nxt_val,'Total Amount (₹)');
			$objPHPExcel->getActiveSheet()->getStyle('I'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('J'.$nxt_val.':K'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$nxt_val,$results['Chalan']['totalamount']);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'CIN NO : ');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$nxt_val,$setting['Setting']['cin_no'])->mergeCells('B'.$nxt_val.':C'.$nxt_val);

			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$nxt_val,'VAT : ');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$nxt_val,'GST NO : ');
			$objPHPExcel->getActiveSheet()->getStyle('D'.$nxt_val)->getFont()->setBold(true);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$nxt_val,$setting['Setting']['vat_tin_no'])->mergeCells('E'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$nxt_val,$setting['Setting']['gst_no'])->mergeCells('E'.$nxt_val.':F'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Excise Extra')->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$nxt_val,'GST (18%) Extra')->mergeCells('I'.$nxt_val.':'.$maxExcelColumn.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('I'.$nxt_val)->getFont()->setBold(true);
			

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'PAN NO : ');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$nxt_val,$setting['Setting']['pan_no'])->mergeCells('B'.$nxt_val.':C'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$nxt_val,'CST : ');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$nxt_val,'');
			$objPHPExcel->getActiveSheet()->getStyle('D'.$nxt_val)->getFont()->setBold(true);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$nxt_val,$setting['Setting']['cst_tin_no'])->mergeCells('E'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$nxt_val,'')->mergeCells('E'.$nxt_val.':F'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'VAT/CST Extra')->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'')->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->getFont()->setBold(true);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'Any Mistake in Weight and Quantity must be immediately reported to us failing which no claims will be entertained.')->mergeCells('A'.$nxt_val.':'.$maxExcelColumn.$nxt_val);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Goods once sold will not be taken back.
')->mergeCells('A'.$nxt_val.':'.$maxExcelColumn.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$rw_grd = $nxt_val + 5;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Notes: '.$results['Chalan']['remarks'])->mergeCells('A'.$nxt_val.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			// Bank Detail -->
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.''.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'Bank Details');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':J'.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':J'.++$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle($rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'Bank Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']['Bank']['branch_name'])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':J'.$rw_grd)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':J'.$rw_grd)->applyFromArray($border_bottom);
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'Swift Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']['Bank']['swift_code'])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'DIRECTOR');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			/*$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objDrawing->setCoordinates('G'.$nxt_val);*/
			//$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'AD Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']['Bank']['ad_code'])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'Beneficiary’s A/C No');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']['Bank']['ac_no'])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'Beneficiary’s Name');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']['Bank']['ac_name'])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'IEC Code');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']["Bank"]["iec_code"])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'IFC Code');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rw_grd.':B'.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$rw_grd,$results['Order']["Bank"]["ifc_code"])->mergeCells('C'.$rw_grd.':'.$maxExcelColumn.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_bottom);
			
			


			
			$new_rw_grd = $rw_grd + 5;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_rw_grd.':'.$maxExcelColumn.$new_rw_grd)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$rw_grd,'RECEIVER SIGNATURE')->mergeCells('A'.$rw_grd.':E'.$new_rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd.':'.$maxExcelColumn.$rw_grd)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->applyFromArray($style_vertical);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rw_grd)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$rw_grd.':F'.$new_rw_grd)->applyFromArray($border_left);	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$rw_grd.':'.$maxExcelColumn.''.$new_rw_grd);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$rw_grd,'For SHALCO INDUSTRIES PVT LTD'."\n".'AUTHORISED SIGNATORY');
			$objPHPExcel->getActiveSheet()->getStyle('F'.$rw_grd)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$rw_grd)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$rw_grd)->applyFromArray($style_vertical);	
			$work_phase = str_replace('<br/>', "\n", $setting['Setting']['work_phase']); 
			$count_work_phase = substr_count( $work_phase, "\n" );
			//echo $count_buyer;exit;
			if($count_work_phase == 0){
				$new_phase = $new_rw_grd + 1;
			}else{
				$new_phase = $new_rw_grd + $count_work_phase;	
			}

			$objPHPExcel->getActiveSheet()->getStyle('A'.++$new_rw_grd)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_rw_grd.':'.$maxExcelColumn.$new_rw_grd)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$new_rw_grd,$work_phase)->mergeCells('A'.$new_rw_grd.':'.$maxExcelColumn.++$new_phase);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_rw_grd)->getAlignment()->setWrapText(true);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$new_rw_grd,$work_phase)->mergeCells('A'.$new_rw_grd.':'.$maxExcelColumn.$new_rw_grd);
			$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_phase.':'.$maxExcelColumn.$new_phase)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A9:'.$maxExcelColumn.$new_phase)->applyFromArray($border_style);
			ob_clean();
			$filename = $reportname.".xlsx";
			/*header('Content-Type: application/vnd.ms-excel');*/
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			$objWriter->save('php://output');
			exit();
	}

	/**
   	 * Name: deleteChalanitem
   	 * Use: delete all invoice item of invoice, or particular invoice item of invoice
     * @param array $item_ids array of invoiceitem 
     * @param int $invoiceid id of invoice
     * @param int $type which kind of delete you wants (type=1 paricular invoice item,type=2 invoice with all invoice item)
	*/
	function deleteChalanitem($item_ids,$chalanid,$type){
		$this->loadmodel('Orderitem');
		$this->loadmodel('Chalanitem');
		$this->loadmodel('Order');
		
		if($type == 2){
     	   $delete_chalanitem_array = $this->Chalanitem->find('all',array('conditions'=>array('Chalanitem.chalanid' => $chalanid)));
     	}elseif($type == 1){
			$delete_chalanitem_array = $this->Chalanitem->find('all',array('conditions'=>array('Chalanitem.id IN ('.$item_ids.')')));
     	}
     	else{
     		return true;
     	}
     	
     	foreach ($delete_chalanitem_array as $delitemvalue) {
     		/*Removing invoiceitem's qty from dlvr qty of orderitem*/
     		$rem_dlvr_qty = $delitemvalue['Orderitem']['dlvr_qty'] - $delitemvalue['Chalanitem']['qty_mtr'];
     		if ($rem_dlvr_qty <= 0) { $itemstatus = 0; /*Pending*/}
     		elseif ($rem_dlvr_qty < $delitemvalue['Orderitem']['qty']) { $itemstatus = 1; /*Process*/}
     		elseif ($rem_dlvr_qty > $delitemvalue['Orderitem']['qty']) { $itemstatus = 2; /*close*/}

     		if($itemstatus == 0 || $itemstatus == 1){
     			/*update order status is 1 means process*/
     			$this->Order->save(array('id' => $delitemvalue['Orderitem']['orderid'], 'status' => 1));
     		}
     		$this->Orderitem->save(array('id'=>$delitemvalue['Orderitem']['id'],'dlvr_qty'=>$rem_dlvr_qty,'status'=>$itemstatus));
     		$this->Chalanitem->delete($delitemvalue['Chalanitem']['id']);
     	}	
	}
	/*
	* Name : delete
	* Use : delete the chalan
	*/
	function delete($id = null) {
		$id = base64_decode($id);	
		$this->autoRender=false;	
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
			return true;
		}

		$this->deleteChalanitem('',$id,2);	 
		if ($this->Chalan->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Chalan has been Deleted successfully</div>'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	/**
   	 * Name: generatepdf
   	 * Use: generate pdf file of record
     * @param int $id id of record
     * @return pdf file   	 
	*/
	public function generatepdf($id = null) {
		
		$this->pdfConfig = array(
			'filename' => 'chalan',
			'download' => $this->request->query('download')
		);
		
		$data = $this->Chalan->find('first',array('conditions'=> array('Chalan.id'=> $id),'recursive' => 3));
    	$this->set('data', $data);
    	/* Make sure the controller doesn't auto render. */
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		//$view->set('text', 'Hello World');		
		$view->viewPath = 'Chalans';
		ini_set('memory_limit', '1024M');
		$html = $view->render('generatepdf');
   		$this->set('post',$html);

   		
	}
	


}	