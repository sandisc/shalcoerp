<?php
/*
 * @Controller name: Bank Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Standard management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class BanksController extends AppController {
	var $name = 'Banks';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Bank Management');
	}
	/*
   	 * Name: index
   	 * Use: Bank view.
	*/
	function index() {
		$this->loadmodel('Bank');
        $this->set('pageTitle','Banks List');
        $this->set('ajaxaction','ajaxlisting');
        
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','bank_name','branch_name','swift_code','ad_code','ac_no','ac_name');
    	$order = array('id' => 'desc');  

		$res = $this->datatable_append($order,$column);

		$count = $this->Bank->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$banks = $this->Bank->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		//$log = $this->User->getDataSource()->getLog(false, false);
		//debug($log); exit;
		$this->set('banks',$banks);
		$this->set('count',$count);
	}
	/*
   	 * Name: add
   	 * Use: user can add,update Bank.
	 */
	function add($id = null) {
		$id = base64_decode($id);
		
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'banks/">Bank</a><i class="fa fa-circle"></i></li>');
        $this->Bank->id = $id;		
		$this->old = $this->Bank->findById($id,array('recursive'=>0));

		if(!empty($this->request->data)) {

			$logged_user = $this->Auth->user('id');
			$this->request->data['Bank']['createdby'] = $logged_user;
			$this->request->data['Bank']['modifiedby'] = $logged_user;	
			$this->Bank->set($this->request->data);
			if($this->Bank->validates($this->data)) {
				if(!empty($id)) {
					
					$this->request->data['Bank']['id'] = $id;
					$this->request->data['Bank']['modifiedby'] = $logged_user;
			
				}
				if($this->Bank->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Bank'],$this->request->data['Bank']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Bank','Edit',$result);/*Add Data in Log*/						
						$msg = 'Bank has been Updated successfully';						
						
					}
					else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Bank','Add',$this->request->data['Bank']);/*Add Data in Log*/
						$msg = 'Bank has been Added successfully';						
					
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					
					$this->redirect('/banks/');
				}
			}
			else{
				 $errors = $this->Bank->validationErrors;
			}
		}
			if(!empty($id)){
				/*$Sthis->Content->id = $id;*/
				$this->set('pageTitle','Edit Bank');
				$this->request->data = $this->Bank->findById($id);

				if(empty($this->request->data))
				{
					$this->redirect('index');
				}
			}
			else{
			$this->set('pageTitle','Add Bank');
		}
		$this->set('id',$id);
	}

	/**
   	 * Name: delete
   	 * Use: delete bank
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Bank->id = $id;
		$data = $this->Bank->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Bank','Delete',$data['Bank']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Bank->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Bank has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}
}