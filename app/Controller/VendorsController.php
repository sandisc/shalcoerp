<?php
/*
 * @Controller name: Vendor Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Vendor management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class VendorsController extends AppController {
	var $name = 'Vendors';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Vendor Management');
	}
	/*
   	 * Name: index
   	 * Use: Vendor list.
	*/
	function index() {
        $this->set('pageTitle','Vendor List');
        $this->set('ajaxaction','ajaxlisting');
        
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','Vendor.vendorname','Vendor.modified','first_name');
    	$order = array('id' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Vendor->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$vendors = $this->Vendor->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('vendors',$vendors);
		$this->set('count',$count);
	}
	/*
   	 * Name: add
   	 * Use: user can add,update Dispatch.
	 */
	function add($id = null) {
		$id = base64_decode($id);
        $this->Vendor->id = $id;		
		$this->old = $this->Vendor->findById($id,array('recursive'=>0));
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Vendor->set($this->request->data);
			if($this->Vendor->validates($this->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Vendor']['modifiedby'] = $logged_user;				
				if(!empty($id)) {				
					$this->request->data['Vendor']['id'] = $id;
				}else{
					$this->request->data['Vendor']['createdby'] = $logged_user;
				}
				if($this->Vendor->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Vendor'],$this->request->data['Vendor']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Vendor','Edit',$result);/*Add Data in Log*/						
						$msg = 'Vendor has been Updated successfully';						
					}else { 
						$msg = 'Vendor has been Added successfully';
						$Activities->addlog($this->loggedin_user_info['id'],'Vendor','Add',$this->request->data['Vendor']);/*Add Data in Log*/
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));		
					$this->redirect('/vendors/');
				}
			}
			else{
				$errors = $this->Vendor->validationErrors;
			}
		}
		/* End : Add/Edit Submit Form Event*/

		if(!empty($id)){
			$this->set('pageTitle','Edit Vendor');
			$this->request->data = $this->Vendor->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Vendor');
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'industries/">Vendor</a><i class="fa fa-circle"></i></li>');		
	}
	
	/*
   	 * Name: delete
   	 * Use: admin can delte standard.
	 */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Vendor->id = $id;
		$data = $this->Vendor->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Vendor','Delete',$data['Vendor']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Vendor->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Vendor  has been Deleted successfully</div>'));			
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));		
	}
}