<?php
/*
 * @Controller name: Invoice Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Invoice management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Orders');
App::import('Controller', 'Activities');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
//App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));

App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));
class InvoicesController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Invoice Management');
	}

	/*
   	 * Name: index
   	 * Use: Invoice listing page.
	*/
	function index() {
        $this->set('pageTitle','Invoice List');
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','invoiceno','orderno','invoicedate','ship_to','Invoice.modified','first_name');
    	$order = array('Invoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Invoice->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$invoices = $this->Invoice->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));
		//pr($invoices);exit;
		$this->set('invoice',$invoices);
		$this->set('count',$count);
	}

	/**
   	 * Name: View
   	 * Use: View details of Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function view($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		$report_button = '';
		$productcat_filter = $this->_group_ty($invoice['Invoiceitem'], 'procatid');
		foreach ($productcat_filter as $key => $value) {
			if(isset($value['0'])){
				$product_taxonomy_id = $value['0']['Orderitem']['Productcategory']['Producttaxonomy']['id'];
				if($product_taxonomy_id == 1 || $product_taxonomy_id == 2){
					$report_button.= '<a id="printrow" href="'.WEBSITE_PATH.'invoices/invoicereports_ks/'.base64_encode($id).'/'.$key.'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button btn dark btn-outline" title="Download Report" targer="_blank"><span class="glyphicon"></span>'.$value['0']['Orderitem']['Productcategory']['productname'].'</a>';
				}
			}
		}
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('report_button',$report_button);
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice View');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}


/**
   	 * Name: PrintInvoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function custominvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}


/**
   	 * Name: PrintInvoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function printinvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');
		$this->set('p',$this->params['p']);
	}
   /**
   	 * Name: Party Invoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function partyinvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/**
   	 * Name: Custom Packing
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function custompacking($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Packing');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/**
   	 * Name: Party Packing
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function partypacking($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Packing');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update invioce and Unique Proforma id.
	*/
	function add($id = null) {
		$id = base64_decode($id);
		$this->set('id',$id);
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->set('pageTitle','Add Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');
		
		$orderdata = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive' => 2)); 
		$this->set('orderdata',$orderdata);
		
		/*If Dummy Id OR Id not exists than*/
		if(empty($orderdata)){
			$this->Session->setFlash(__('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Record not exists</div>'));        			
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
				
		$invoices_id = $this->request->data['Invoice']['invoiceno'] = $this->getInvoiceNO();
		$this->set('invoice',$invoices_id);

		$this->loadmodel('Hscode');
		$hscodes = $this->Hscode->find('list',array('fields' => array('Hscode.rate','Hscode.hscode')));
		$this->set('hscodes',$hscodes);		
	}
	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->loadmodel('Licenceexport'); 
		$this->loadmodel('Licenceexportitem'); 
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
		if(!empty($this->request->data)) { 

				$invoice = $this->request->data['Invoice'];
				$date = $invoice['invoicedate'];
				$invoice['invoicedate'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
				$invoice['signdate'] = $invoice['invoicedate'];
				$invoice['invoiceno'] = $this->getInvoiceNO('',$invoice['invoicedate']);
				$invoice['fy'] = $this->calculateFinancialYearForDate($invoice['invoicedate']);

			$this->Invoice->set($invoice);
			$orderid = $this->request->data['Invoice']['orderid'];
			$response=array();

			if($this->Invoice->validates($invoice)) {
				$logged_user = $this->Auth->user('id');
				$invoice['Invoice']['createdby'] = $logged_user;
				$invoice['Invoice']['modifiedby'] = $logged_user;
				
				$this->Invoice->save($invoice);

				/* Start add invoice item data*/
					$lastid = $this->Invoice->getLastInsertID();
					$response['idvalue'] = $lastid;
		            $item = $this->request->data['Invoice']['item'];   
		            $total = 0;
		           	foreach($item as $key=>$items){	
		           		$items['invoiceid'] = $lastid;
		           		$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
		           		$items['price_mt'] = $items['netprice'] / $items['qty_mt'];

		           		$total += $items['exempted'];
		           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           			$this->Invoiceitem->saveall($items);
		           			$item[$key]['id'] = $this->Invoiceitem->getLastInsertID();/*set invoiceitemid, it will be use to save data into licence export item table*/
		           		}
		           	}
				/* End add invoice item data*/
				
				/*Sorting Order based on grade*/
				$grade_filter = $this->_group_by($item,'grade_id');	
				$gradebaseweight = '';
				$grd_name = '';
				$append_grd ='';
				$final_grd_total = '';
				foreach ($grade_filter as $gradekey => $gradevalue) { //pr($gradevalue);
					$grd_name = $gradevalue[0]['grade_name'];
						$append_grd = 'Grade '.$grd_name.' ';
						$net_weight = '';
					foreach ($gradevalue as $gradekeys => $gradevalues) { //pr($gradevalues);
						$net_weight += $gradevalues['netweight'];
					}
						$append_grd .= (($net_weight)/1000).' MT <br/>';	 
						$final_grd_total[] = $append_grd;
				}
				$concat_grd = implode('',$final_grd_total);
				
		           	if($this->Invoice->id){
		           		$invoice['totalexempted'] = $total;				
		           		$invoice['gradebaseweight'] = $concat_grd;				
		           		$invoice['customgradebaseweight'] = $concat_grd;
		           		$this->Invoice->save($invoice);

		           	}
		        /*Start Update order item status and delivered quantity*/
		           	$updateorderitemdata = array();
		           	foreach($item as $items){			   		
		           		if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           			$items['orderitemid'] = $items['orderitemid'];
		           			$updateorderitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
		           		}
		           	}
		           	$this->loadmodel('Orderitem');
					$this->Orderitem->saveall($updateorderitemdata);
		        /*End Update order item status and delivered quantity*/
			        $orders = new OrdersController; 
			        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its status, change order status*/

			        /*Start Save data in licence export */
					foreach($item as $items){	
				   		/*check licenceid*/
						if($items['licenceid'] != 0){
					   		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
					   			/*check for invoice, particular licence id and invoice id exist in licenceexport table , reason is many invoice item has same licence so, we are insert it in only one time in licenceexport and we add multiple item in licence export with based on it reference. */
						   		$licenceexportrecord = $this->Licenceexport->find('first',array('conditions'=>array('invoiceid' => $lastid,'licenceid'=>$items['licenceid']),'fields'=>array('id'),'recursive'=>-1));
						   		if(empty($licenceexportrecord)){
							   		$licenceexport['invoiceid'] = $lastid;
							   		$licenceexport['clientid'] = $invoice['clientid'];
							   		$licenceexport['licenceid'] = $items['licenceid'];
							   		$licenceexport['createdby'] = $logged_user;
							   		$licenceexport['modifiedby'] = $logged_user;   			   		
									$this->Licenceexport->saveall($licenceexport);
									$licenceexportid = $this->Licenceexport->getLastInsertID();
								}else{
									$licenceexportid = $licenceexportrecord['Licenceexport']['id'];
								}
								/*if no licence than no licenceitem*/
								if(isset($items['licenceitemid'])){
									$lastlicenceexportid = $licenceexportid;
					           		$exportitems['licenceexportid'] = $lastlicenceexportid;
					           		$exportitems['refitemid'] = $items['id'];/*Reference of invoice item id*/
				           			$exportitems['exportqty'] = $items['qty_mt'];
				           			$exportitems['dollarvalue'] = $items['netprice'];
				           			$exportitems['licenceitemid'] = $items['licenceitemid'];
				           			$this->Licenceexportitem->saveall($exportitems);
								}
			           		}
		           		}
		           	}
		           	/*End Save data in licence export */

		        	$msg = 'Invoice has been saved successfully';
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'Invoice';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
				
			}
         	else{
	        	$Invoice = $this->Invoice->validationErrors;
	        	$response['status'] = 'error';
                $response['message'] = 'The Invoice could not be saved. Please, try again.';
                $response['data'] = compact('Invoice');
                echo json_encode($response);
	        }	
		}			
	}

	/**
   	 * Name: edit
   	 * Use: update invoice.
     * @param int $id id of record
     * @return detail record of id   	 
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}	
		$this->loadmodel('Client');
		//$this->loadmodel('Proformainvoice');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Hscode');
		$this->loadmodel('Bank');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/"> Invoices</a><i class="fa fa-circle"></i></li>');
	    $this->set('pageTitle','Edit Invoice');

		$this->Client->unbindModel(array('belongsTo' => array('User','Coordinator')));
		$this->Order->unbindModel(array('belongsTo' => array('Dispatch','User')));
		$this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem')));
		
		$this->Orderitem->bindModel(array('hasOne' => array('Invoiceitem' => array(
            'className' => 'Invoiceitem',
            'foreignKey' => 'orderitemid',
            'conditions'=>array('Invoiceitem.invoiceid' => $id)
        ))));

	    $invoice_data = $this->Invoice->find('first',array('conditions' => array('Invoice.id' => $id ),'recursive'=>3));
	    
        $this->set('invoice_data',$invoice_data);
        //pr($invoice_data);exit;

		$this->request->data = $invoice_data;
		if(empty($invoice_data)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
	}

	/*Form submit usign ajax*/
	function ajaxeditsubmit(){
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->loadmodel('Licenceexport');
		$this->loadmodel('Licenceexportitem');
		$this->autoRender = false; 
	    if(!empty($this->request->data)) {
	    	$this->Invoice->set($this->request->data);
	    	if($this->Invoice->validates($this->request->data)) {

				$Activities = new ActivitiesController;
				$id = $this->request->data['Invoice']['id'];
				$this->old = $this->Invoice->find('first', array(
		 											'conditions' => array('Invoice.id' => $id ),
													'recursive' => 2));

				$logged_user = $this->Auth->user('id');
				$orderid = $this->request->data['Invoice']['orderid'];
				$this->request->data['Invoice']['modifiedby'] = $logged_user;
				$invoices = $this->request->data['Invoice'];
				
				$invoices['invoicedate'] = date('Y-m-d',strtotime(str_replace('/', '-',  $this->request->data['Invoice']['invoicedate'])));
				$invoices['signdate'] = $invoices['invoicedate'];

				/*check old and new invoices number is same or not. if not than we will calling to get new invoices number again. Reason is to avoid duplicacy if same time 2 or more user calling same function*/
				if($this->old['Invoice']['invoiceno'] != $invoices['invoiceno']){
					$invoices['invoiceno'] = $this->getInvoiceNO('',$invoices['invoicedate']);
					$invoices['fy'] = $this->calculateFinancialYearForDate($invoices['invoicedate']);
				}

				$this->Invoice->save($invoices);/*Save invoice data*/

				$item = $this->request->data['Invoice']['item'];
				$get_all_item  = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.invoiceid'=>$id)));
				$del_ids = $this->request->data['removal_id'];
	            if(isset($del_ids) && !empty($del_ids)){
	            	$this->deleteInvoiceitem($del_ids,$id,1);	            	
		        }
	            $sum_qty = 0;
	            $total_exempted = 0;
            	/*Save invoice items data*/
            	foreach($item as $key=>$items){
	           		if(isset($items['id']) && !empty($items['id'])){ /*Update items info which is already exist in invoice*/
	           			$items['invoiceid'] = $id;
	           			$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
	           			$items['price_mt'] = $items['netprice'] / $items['qty_mt'];
						$total_exempted += $items['exempted'];
						$sum_qty += $items['qty_mt'];
						$this->Invoiceitem->id = $items['id'];
 	           			$this->Invoiceitem->save($items);
	           		}
	           		else{/*Add items info which is already exist in order but not in invoice*/
	           			$items['invoiceid']  = $id;

	           			if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
							$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
			           		$items['price_mt'] = $items['netprice'] / $items['qty_mt'];
			           		$total_exempted += $items['exempted'];
			           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
			           			$this->Invoiceitem->saveall($items);
								$item[$key]['id'] = $this->Invoiceitem->getLastInsertID();/*set invoiceitemid, it will be use to save data into licence export item table*/			           			
			           		}							
						}
					}
	           	}
	           	$grades_filter = $this->_group_by($item,'grade_id');	
	           	
				$gradebaseweight = '';
				$grd_name = '';
				$append_grd ='';
				$final_grd_total = '';
				foreach ($grades_filter as $gradekey => $gradevalue) { 
					$grd_name = $gradevalue[0]['grade_name'];
					$append_grd = 'Grade '.$grd_name.' ';
					$net_weight = '';
					foreach ($gradevalue as $gradekeys => $gradevalues) { 
						$net_weight += $gradevalues['netweight'];
					}
					$append_grd .= (($net_weight)/1000).' MT <br/>';	 
					$final_grd_total[] = $append_grd;
				}
				$concat_grd = implode('',$final_grd_total);

	           	if($this->Invoice->id){
	           		if($this->request->data['Invoice']['manual'] != '1'){
		           		$invoice['gradebaseweight'] = $concat_grd;				
		           		$invoice['customgradebaseweight'] = $concat_grd;
	           		}
	           		$invoice['totalexempted'] = $total_exempted; 
	           		$this->Invoice->save($invoice);
	           	}

	           	/*Start Update order item status and delivered quantity*/
	           	$updateorderitemdata = array();
	           	foreach($item as $items){	
	           		$items['orderitemid'] = $items['orderitemid'];
	           		$updateorderitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
	           	}
	           	$this->loadmodel('Orderitem');
				$this->Orderitem->saveall($updateorderitemdata);
		        /*End Update order item status and delivered quantity*/

		        $orders = new OrdersController; 
		        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its status, change order status*/

			        /*Start Save data in licence export */
					foreach($item as $items){	
				   		$oldlicencexportitemid = '';
				   		$oldlicenceexportitem = '';
				   		$oldlicenceexport = '';
				   		$oldlicenceitemdta = '';
				        $licenceexportid = '';
				        $licenceexportrecord = array();
				        $exportitems = array();
				        $licenceexport = array();

				        $this->Licenceexport->create();
				        $this->Licenceexportitem->create();
				        if($items['licenceid'] != 0 || $items['oldlicenceid'] != 0){
					   		if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
								
								/*check for invoice, particular licence id and invoice id exist in licenceexport table , reason is many invoice item has same licence so, we are insert it in only one time in licenceexport and we add multiple item in licence export with based on it reference. */
							   	$licenceexportrecord = $this->Licenceexport->find('first',array('conditions'=>array('invoiceid' => $id,'licenceid'=>$items['licenceid']),'fields'=>array('id'),'recursive'=>-1));

					   			/*Check old licence number and current licence number both same than update its information*/
					   			if(!empty($licenceexportrecord) && ($items['licenceid'] === $items['oldlicenceid'])){
					   				/*udpate export qty of particular refitemid*/	
					   				$this->Licenceexportitem->updateAll(
									 	array('exportqty' => $items['qty_mt'],'dollarvalue' => $items['netprice']), 
									 	array('refitemid'=>$items['id'])
									);   		
											   				
					   			}else{
					   				/*Check old licence number and current licence number both not same than add it*/
					   				if($items['licenceid']!= 0){
						   				if(empty($licenceexportrecord)){/*Empty than, insert it*/
								   			//unset($licenceexport['id']);
								   			//$this->Licenceexport->id = '';
									   		$licenceexport['invoiceid'] = $id;
									   		$licenceexport['clientid'] = $invoices['clientid'];
									   		$licenceexport['licenceid'] = $items['licenceid'];
									   		$licenceexport['createdby'] = $logged_user;
									   		$licenceexport['modifiedby'] = $logged_user;   			   		
											$this->Licenceexport->save($licenceexport); 
											$licenceexportid = $this->Licenceexport->getLastInsertID();
										}else{/*Not Empty than, take its id for use licenceexport table entry*/
											$licenceexportid = $licenceexportrecord['Licenceexport']['id'];
										}
									}
									/*fetch old licenceexport item details based on old invoice item */
									$oldlicenceexportitem = $this->Licenceexportitem->find('first',array('conditions'=>array('refitemid' => $items['id']),'fields'=>array('id'),'recursive'=>-1));
										
									if(!empty($oldlicenceexportitem)){
										/*Update licenceexportitem  : If old entry exist in licenceexport item table than update its qty and new licence export id(based on invoice and licence)*/
					   					$this->Licenceexportitem->id = $oldlicenceexportitem['Licenceexportitem']['id'];
					   					$exportitems['licenceexportid'] = $licenceexportid;	
					   					$exportitems['licenceitemid'] = $items['licenceitemid'];
					   					$exportitems['exportqty'] = $items['qty_mt'];
								   		$exportitems['dollarvalue'] = $items['netprice'];	

										if($items['licenceid']!= 0){/*item has licence than save licenceexportitem*/
											$this->Licenceexportitem->save($exportitems);	
										}else{/*item hasnot licence than delete licenceexportitem*/
											$this->Licenceexportitem->delete($oldlicenceexportitem['Licenceexportitem']['id']);
										}								   				   		
										
									}else{ /*Add licenceexportitem s*/
						           		$exportitems['licenceexportid'] = $licenceexportid;		           		
					           			$exportitems['exportqty'] = $items['qty_mt'];
					           			$exportitems['dollarvalue'] = $items['netprice'];
					           			$exportitems['licenceitemid'] = $items['licenceitemid'];
					           			$exportitems['refitemid'] = $items['id'];		           			
					           			$this->Licenceexportitem->save($exportitems); 
									}															           						
					   			}						
			           		}
		           		}
		           	}
		           	/*fetch all licence related invoice and remove licenceexport row if its licencexport item array is empty*/
					$exportofInvoice = $this->Licenceexport->find('all',array('conditions'=>array('invoiceid' => $id),'recursive'=>1));
					foreach ($exportofInvoice as $v) {
						if(empty($v['Licenceexportitem'])){
							$this->Licenceexport->delete($v['Licenceexport']['id']);
						}
					}
		           	/*End Save data in licence export */

	           	$msg = 'Invoice has been Updated successfully';      
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));

	           	//$this->redirect('index');
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);		           	
	        }
	        else{
                $Invoice = $this->Invoice->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Invoice could not be saved. Please, try again.';
                $response['data'] = compact('Invoice');
                echo json_encode($response);
	        }
		}		
	}

	/**
   	 * Name: deleteInvoiceitem
   	 * Use: delete all invoice item of invoice, or particular invoice item of invoice
     * @param array $item_ids array of invoiceitem 
     * @param int $invoiceid id of invoice
     * @param int $type which kind of delete you wants (type=1 paricular invoice item,type=2 invoice with all invoice item)
	*/
	function deleteInvoiceitem($item_ids,$invoiceid,$type){
		$this->loadmodel('Orderitem');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order');
		$this->loadmodel('Licenceexport');
		$this->loadmodel('Licenceexportitem');
		
		if($type == 2){
     	   $delete_invoiceitem_array = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.invoiceid' => $invoiceid)));
     	}elseif($type == 1){
			$delete_invoiceitem_array = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.id IN ('.$item_ids.')')));
     	}
     	else{
     		return true;
     	}
     	
     	foreach ($delete_invoiceitem_array as $delitemvalue) {
     		/*Removing invoiceitem's qty from dlvr qty of orderitem*/
     		$rem_dlvr_qty = $delitemvalue['Orderitem']['dlvr_qty'] - $delitemvalue['Invoiceitem']['qty_mtr'];
     		if ($rem_dlvr_qty <= 0) { $itemstatus = 0; /*Pending*/}
     		elseif ($rem_dlvr_qty < $delitemvalue['Orderitem']['qty']) { $itemstatus = 1; /*Process*/}
     		elseif ($rem_dlvr_qty > $delitemvalue['Orderitem']['qty']) { $itemstatus = 2; /*close*/}

     		if($itemstatus == 0 || $itemstatus == 1){
     			/*update order status is 1 means process*/
     			$this->Order->save(array('id' => $delitemvalue['Orderitem']['orderid'], 'status' => 1));
     		}
     		$this->Orderitem->save(array('id'=>$delitemvalue['Orderitem']['id'],'dlvr_qty'=>$rem_dlvr_qty,'status'=>$itemstatus));
     		$this->Licenceexportitem->deleteAll(array('refitemid' => $delitemvalue['Invoiceitem']['id']));
     		/* we didnt delete its record from licence export table, we delete it before function close */
     		$this->Invoiceitem->delete($delitemvalue['Invoiceitem']['id']);
     	}
		/*fetch all licence related invoice and remove licenceexport row if its licencexport item array is empty*/
		$exportofInvoice = $this->Licenceexport->find('all',array('conditions'=>array('invoiceid' => $invoiceid),'recursive'=>1));
		foreach ($exportofInvoice as $v) {
			if(empty($v['Licenceexportitem'])){
				$this->Licenceexport->delete($v['Licenceexport']['id']);
			}
		}
	}

	/**
   	 * Name: countdeliveredqty
   	 * Use: count delivered quantity of particular order item
     * @param int $orderitemid orderitemid of record
     * @param decimal $qty qty of orderitem
     * @return array with orderitem id,delivered qty and status.   	 
	*/
	function countdeliveredqty($orderitemid,$qty){
		$itemdata = $this->Invoiceitem->find('all',array('conditions' => array('orderitemid'=>$orderitemid)));
		$dlvr_qty = 0;
		foreach ($itemdata as $key => $value) {
			$dlvr_qty+=$value['Invoiceitem']['qty_mtr'];
		}
		if($dlvr_qty > $qty || $dlvr_qty == $qty){
			$status = 2; //close
		}else{
			$status = 1; //process
		}
		return array('id'=>$orderitemid,'dlvr_qty'=>$dlvr_qty,'status'=>$status);
	}

	/**
   	 * Name: generatepdf
   	 * Use: generate pdf file of record
     * @param int $id id of record
     * @param int $p type of file
     * @return pdf file   	 
	*/
	public function generatepdf($p,$id = null) {
		$this->pdfConfig = array(
			'filename' => 'invoice',
			'download' => $this->request->query('download')
		);
		$this->loadmodel('Licence');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		$data = $this->Invoice->find('first',array('conditions'=> array('Invoice.id'=> $id),'recursive' => 3));
    	$this->set('data', $data);
    	$this->set('p', $p);/*for check request*/
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 

		$view->viewPath = 'Invoices';
		ini_set('memory_limit', '1024M');
		$html = $view->render('generatepdf');
   		$this->set('post',$html);   		
	}
	
   /**
	* Name : getInvoiceNO
	* Use : Get Unique Invoice No for Invoice
	*/
	function getInvoiceNO($type=null,$date=null){
		if($type == 'ajax'){ /*For on change event inside add and edit chalan form*/
			$this->autoRender=false;
		}
		/*Default current date*/
		if(empty($date)){$date = date('Y-m-d');}
		else{
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
		}  
		/*If user fill date than override default date with user entered date*/
		if(isset($_POST['date'])){ 
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
		}

		$new_finan_year = $this->calculateFinancialYearForDate($date);
		$invoice_id = $this->Invoice->find('first',array('conditions'=>array('Invoice.fy' => $new_finan_year),'fields'=>array('Invoice.invoiceno','Invoice.fy'),'recursive'=> -1,'order' => 'Invoice.id DESC'));
		
		if(!empty($invoice_id) && $invoice_id['Invoice']['fy'] == $new_finan_year){
			$in_id = $invoice_id['Invoice']['invoiceno'];
			$exp_id = explode('/',$in_id);
			$inv_id = $exp_id[1] + 1;
			$year = date("Y");
			$pr_id = 'SHI/'.$inv_id.'/'.$this->calculateFinancialYearForDate($date);
			return $pr_id;
		}else{
			$year = date("Y");
			$pr_id = 'SHI/'.'1/'.$this->calculateFinancialYearForDate($date);
			return $pr_id;
		}
	}

  /**
	* Name : invoiceoforder
	* Use : Show Invoices of particular order
 	* @param int $orderid orderid of record
    * @return array with order and invoice details of particular order.
	*/
	function invoiceoforder($orderid){
		if (empty($orderid)) { /* If orderid is empty than */
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}		
		$this->loadmodel('Order');
		$this->loadmodel('Client');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Orderitem');
		
		$id = base64_decode($orderid);
		$this->Orderitem->bindModel(array('hasMany' => array('Invoiceitem' => array(
            'className' => 'Invoiceitem',
            'foreignKey' => 'orderitemid'
        ))));		
		$orders = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive'=>2));
		if(empty($orders)){/*if no invoice for particular order than */
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$clients = $this->Client->find('first',array('conditions' =>array('Client.id' => $orders['Client']['id'])));

		$this->set('orders',$orders);
		$this->set('clients',$clients);
		//pr($orders); exit;
		$this->set('pageTitle','Invoices of Order');
		$this->set('ajaxaction','invoiceoforderlisting/'.$id);
	}

	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function invoiceoforderlisting(){
		$id = $this->params['pass'][0];/*Order id*/
    	$column = array('id','invoiceno','invoicedate','Invoice.totalqty','totalgrossweight','finalvalue','Invoice.modified','first_name');
    	$order = array('Invoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Invoice->find('count',array('conditions'=>array($res['con'],'Invoice.orderid'=>$id),'order'=>$res['order_by']));
		$invoices = $this->Invoice->find('all',array('conditions'=>array($res['con'],'Invoice.orderid'=>$id),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));
		$this->set('invoice',$invoices);
		$this->set('count',$count);
	}

	/*
	* Name : delete
	* Use : delete the invoice
	*/
	function delete($id = null) {
		$id = base64_decode($id);	
		$this->autoRender=false;	
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
			return true;
		}

		$this->deleteInvoiceitem('',$id,2);	 
		if ($this->Invoice->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invoice has been Deleted successfully</div>'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	/*
	* Name : export
	* Use : Export the Order data
	*/
	function export($id = null) {
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

        $this->loadmodel('Setting');
        $settings = $this->Setting->find('first');

		$this->loadmodel('Licence');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		$results = $this->Invoice->find('first',array('conditions'=>array('Invoice.id' => $id),'recursive'=>3)); 
		$dyn_name_report = '';
		if($_GET['p'] == '1'){
			$dyn_name_report = 'Invoice';
			$reportname = 'Party-Invoice-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '0'){
			$dyn_name_report = 'Packing List';
			$reportname = 'Party-Packing-List-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '2'){
			$dyn_name_report = 'Invoice';
			$reportname = 'Custom-Invoice-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '3'){
			$dyn_name_report = 'Packing List';
			$reportname = 'Custom-Packing-List-'.$results['Invoice']['invoiceno'];
		}
		
		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

		$title_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

		$style_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_LEFT,));

		$border_style = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_right_thick = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
		
		$style_underline = array('font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));

		if($results['Order']['Price']['sign'] == '&#8377;'){
			$sym = '₹';
		}else if($results['Order']['Price']['sign'] == '&#36'){
			$sym = '$';
		}	else if($results['Order']['Price']['sign'] == '&#8364;'){
			$sym = '€'; 
		}else {
			$sym = '£';
		}
		/* Start Set Item column name For Row 22nd and 23rd */
			/*$itemHeader , to save main table column name*/
			/*$itemSubHeader , to save column name just below main table column*/
		$excelcolumn='A';
		$itemHeader[$excelcolumn] = "Marks & Bundle.";  
		$itemSubHeader[$excelcolumn] = ''; 
		//$itemSubHeader[$excelcolumn] = 'Container No : '.$results['Invoice']['containerno'];

		if($_GET['p'] == 0 || $_GET['p'] == 3){
			$itemHeader[++$excelcolumn] = "Heat No";  $itemSubHeader[$excelcolumn] = ''; 
		}
		$product_column_key = ++$excelcolumn;
		$product_column_nxt_key = ++$excelcolumn;

		$itemHeader[$product_column_key] = "Product Name"; $itemSubHeader[$product_column_key] = ''; 
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.'22:'.$product_column_nxt_key.'22');		

		$itemHeader[$product_column_nxt_key] = ""; $itemSubHeader[$product_column_nxt_key] = ''; 
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.'23:'.$product_column_nxt_key.'23');	

		$standard_column_key = ++$excelcolumn;
		$itemHeader[$standard_column_key] = "Standard"; $itemSubHeader[$excelcolumn] = ''; 

		$grade_column_key = ++$excelcolumn;
		$itemHeader[$grade_column_key] = "Grade"; $itemSubHeader[$excelcolumn] = '';

		$gooddesc_column_key = ++$excelcolumn;
		$itemHeader[$gooddesc_column_key] = "Good Description"; $itemSubHeader[$excelcolumn] = ''; 

		$pcs_column_key = ++$excelcolumn;
		$itemHeader[$pcs_column_key] = "PCS"; $itemSubHeader[$excelcolumn] = ''; 

		$qtymt_column_key = ++$excelcolumn;
		$itemHeader[$qtymt_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = '(UNIT MT)'; 
		
		if($_GET['p'] == 0 || $_GET['p'] == 1){
			$qtymtr_column_key = ++$excelcolumn;
			$itemHeader[$qtymtr_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = '(UNIT MTR)'; 
		}
		if($_GET['p'] == 2 || $_GET['p'] == 3){
			$exempted_column_key = ++$excelcolumn;
			$itemHeader[$exempted_column_key] = "Exempted"; $itemSubHeader[$excelcolumn] = ' MATERIAL '; 
		}
		if($_GET['p'] == 1 || $_GET['p'] == 2){ 
			$price_column_key = ++$excelcolumn;
			$itemHeader[$price_column_key] = "Price"; $itemSubHeader[$excelcolumn] = ' IN ('.$sym.') '; 
			$netamount_column_key = ++$excelcolumn;
			$itemHeader[$netamount_column_key] = "Net Amount";  $itemSubHeader[$excelcolumn] = ' IN '.$sym; 
		}			
		if($_GET['p'] == 0 || $_GET['p'] == 3){
			$netweight_column_key = ++$excelcolumn;
			$itemHeader[$netweight_column_key] = "Net Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
			$grossweight_column_key = ++$excelcolumn;
			$itemHeader[$grossweight_column_key] = "Gross Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
		}
		$maxExcelColumn = $excelcolumn; 
		/* End Set Item column name For Row 22nd and 23rd */
	
	/*if($_GET['type']==1){
		$headstyleArray = array(
	    	'font'  => array(
	        	'bold'  => true,
	        	'name'  => 'Times New Roman'
	    	)
	    );
	}*/
	//if($_GET['type']==2){
		$headstyleArray = array(
	    	'font'  => array(
	        	'bold'  => true,
	        	//'color' => array('rgb' => 'FF0000'),
	        	'size'  => 10,
	        	'name'  => 'Times New Roman'
	    	)
	    );		
		$valuestyleArray = array(
	    	'font'  => array(
	        	//'bold'  => true,
	        	//'color' => array('rgb' => 'FF0000'),
	        	'size'  => 10,
	        	'name'  => 'Verdana'
	    	)
	    );
		$setting= $this->Session->read('setting_data');
		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

			$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'img/print_logo.png';
			$objDrawing->setPath($path);
			$objDrawing->setCoordinates('A1');
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "CIN NO: ". $setting["Setting"]["cin_no"]."\n"."Office: 24, 2nd Floor, Dayabhai Building"."\n"."Gulalwadi, Mumbai - 400004"."\n". "Maharastra, (INDIA)."."\n"."Tel: + 91-22-40416214/ 40416221/ 25"."\n". "Fax: +91-22-23452161"."\n"."Email: info@shalco.in Web: www.shalco.in");
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G1:'.$maxExcelColumn.'7');
			$objPHPExcel->getActiveSheet()->getStyle('G1:'.$maxExcelColumn.'7')->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('G1:'.$maxExcelColumn.'7')->applyFromArray($hori_left);
			$objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F7');

			$dyn_cell = 8;

	/*start apply boldness in top section */	
		//$objPHPExcel->getActiveSheet()->getStyle('11')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

			$styleArray = array(
		      'font'  => array(
		        'bold'  => true,
		        'size'  => 12,
		        'name'  => 'Calibri'
		       )
			);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,strtoupper($dyn_name_report))->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'EXPORTER')->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'Invoice No.')->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, 'Invoice Date')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_right);			

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'SHALCO INDUSTRIES PVT. LTD.')->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $results['Invoice']['invoiceno'])->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, $this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Invoice']['invoicedate'])))->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_right);


			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'Factory Add')->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, 'Office Add')->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'PO No.')->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, 'PO Date')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->getFont()->setBold(true);

			$new_cell = $dyn_cell + 3;
			$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell.':B'.++$new_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$dyn_cell, $setting["Setting"]["factory_address"])->mergeCells('A'.$dyn_cell.':B'.$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':B'.$new_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $setting["Setting"]["office_address"])->mergeCells('C'.$dyn_cell.':E'.$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $results['Order']['clientpo'])->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($hori_right);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, $this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Order']['orderdate'])))->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, 'Country of Origin of Goods')->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, 'Country of Final Destination')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, $results['Invoice']['fromcountry'])->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, $results['Invoice']['tocountry'])->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.++$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell.':I'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
								
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'CONSIGNEE')->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'BUYERS (IF OTHER THAN CONSIGNEE)')->mergeCells('F'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);

			/*fetching number of row for consignee details*/
			$consignee = $results['Order']['Client']['company_name'].'<br/>'.searchFromAddress($results['Order']['billaddress_id'], $results['Order']['Client']['Address']);
			$cl_address = str_replace('<br/>', "\n",$consignee );
			$count_ad = substr_count( $cl_address, "\n" );
			if($count_ad > 5){ /*If address is not small than*/
				$for_new1 = $dyn_cell + $count_ad;
			}else{ /*If address is not small than set default 5 row for better formatting*/
				$for_new1 = 5;
			}
			


			$buyer = str_replace('<br/>', "\n ",$results['Invoice']['buyers']);
			$count_buyer = substr_count( $buyer, "\n" );			
			if($count_buyer == 0){
				$for_new2 = $dyn_cell + 2;
				//$for_new2 = $for_new1;
				$buyer = $results['Invoice']['buyers'];
			}else{
				$for_new2 = $dyn_cell + $count_buyer;	
			}

			$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell)->applyFromArray($style_vertical);
			$cons_len = $for_new2 + 2;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$dyn_cell, $cl_address)->mergeCells('A'.$dyn_cell.':E'.$cons_len);			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':F'.$for_new1)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $buyer)->mergeCells('F'.$dyn_cell.':'.$maxExcelColumn.++$for_new2);			

			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getAlignment()->setWrapText(true);
			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$for_new2.':'.$maxExcelColumn.$for_new2)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('F'.++$for_new2)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$for_new2, 'IEC CODE NO. '.$results['Order']['Bank']['iec_code'])->mergeCells('F'.$for_new2.':'.$maxExcelColumn.$for_new2);			

			$objPHPExcel->getActiveSheet()->getStyle('F'.$for_new2)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$for_new1.':'.$maxExcelColumn.$for_new2)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$for_new1.':'.$maxExcelColumn.$for_new1)->applyFromArray($border_bottom);
			$dyn_cell = $for_new2;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'PRE CARRIAGE BY')->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, 'PLACE OF RECEIPT BY PRE-CARRIER')->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'OTHER REFERENCES(S)')->mergeCells('F'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Invoice']['carriageby'])->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $results['Invoice']['receiptplace'])->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':E'.$dyn_cell)->applyFromArray($border_bottom);


			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $results['Invoice']['otherref'])->mergeCells('F'.$dyn_cell.':J'.$dyn_cell);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'VESSEL/FLIGHT NO.')->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, 'PORT OF LOADING')->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Invoice']['flightno'])->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($hori_right);				
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $results['Invoice']['loadingport'])->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'PORT OF DISCHARGE')->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, 'PLACE OF DELIVERY')->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);

			$merge_shipping = $dyn_cell + 1;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'Shipping Terms')->mergeCells('F'.$dyn_cell.':G'.$merge_shipping);	
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, $this->requestAction('App/get_delivery_type/'.$results['Order']['delivery_type']).' '.$results['Order']['tod'])->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$merge_shipping);	
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Invoice']['dischargeport'])->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $results['Invoice']['loadingport'])->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);			
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($border_left);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'Container No.')->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, 'Total Packages')->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);
			$merge_top = $dyn_cell + 1;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'TERMS OF PAYMENT')->mergeCells('F'.$dyn_cell.':G'.$merge_top);	
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$dyn_cell, $results['Order']['top'])->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$merge_top);	
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Invoice']['containerno'])->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $results['Invoice']['totalpackage'])->mergeCells('C'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($hori_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);

			/* Start Print Item column name For Row 22nd and 23rd */
			/*$item_head_cnt = ++$dyn_cell;
			foreach ($itemHeader as $itemHeaderkey => $itemHeadervalue) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($itemHeaderkey.$item_head_cnt, $itemHeadervalue);
				$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.$item_head_cnt)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.$item_head_cnt)->applyFromArray($border_bottom);
				if(!empty($itemHeadervalue)){
					$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.$item_head_cnt)->applyFromArray($border_right);	
				}
				
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($itemHeaderkey.$item_head_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			$new_dyn_cell = ++$dyn_cell;
			foreach ($itemSubHeader as $itemSubHeaderkey => $itemSubHeadervalue) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($itemSubHeaderkey.$new_dyn_cell, $itemSubHeadervalue);
				$objPHPExcel->getActiveSheet()->getStyle($itemSubHeaderkey.$new_dyn_cell)->applyFromArray($border_right);
				$objPHPExcel->getActiveSheet()->getStyle($itemSubHeaderkey.$new_dyn_cell)->applyFromArray($border_bottom_thick);
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($itemHeaderkey.$new_dyn_cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			/* End Print Item column name For Row 22nd and 23rd */
			// New code by Kaushal Shah

			if($_GET['p'] == 2 || $_GET['p'] == 3){
				$qty_label = 'Exempted MATERIAL';
			}
			else{
				$qty_label = 'QTY';
			}
			if($_GET['p'] == 0 || $_GET['p'] == 3){
				$headerRow = "Marks & Bundle No.,Heat No,Product Name,,Standard,Grade,Good Description,PCS,QTY "."\n"."(UNIT MT),".$qty_label.", Net Weight IN KGS, Gross Weight IN KGS";
			}
			if($_GET['p'] == 1 || $_GET['p'] == 2){
				if($_GET['p'] == 1){$price_in = "Price"; $licence = '';}
				if($_GET['p'] == 2){$price_in = "Price "."\n"."(UNIT MT)"; $licence = 'Licence No';}
				$headerRow = "Marks & Bundle No.,Product Name,". $licence.",Standard,Grade,Good Description,PCS,QTY "."\n"."(UNIT MT),".$qty_label."\n".",".$price_in."\n"."(".$sym."), Net Amount"."\n"."(".$sym.")";
			}
			
			//echo $dyn_cell;exit;
				$k = ++$dyn_cell;
				$objPHPExcel->getActiveSheet()->getStyle($k)->applyFromArray($style);
				$objPHPExcel->getActiveSheet()->getStyle($k)->getFont()->setBold(true);
				$maxSize = explode(",", $headerRow);
				//pr($maxSize);
				for($i=0,$j='A';$i<sizeof($maxSize);$i++,$j++) {
					if($_GET['p'] == 1 || $_GET['p'] == 2){
						if($_GET['p'] == 1){
							if($maxSize[$i] != 'Product Name'){
								$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$k.':'.'C'.$k);
							}	 
						}//echo $maxSize[$i].'=='.'B'.$k.':'.'C'.$k.'<br/>';	
					}else{
						if($maxSize[$i] != 'Product Name'){
							$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$k.':'.'D'.$k);
						}
					}
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$k, $maxSize[$i]);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k, $maxSize[$i])->applyFromArray($border_left);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style_vertical_center);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->getAlignment()->setWrapText(true);

					if($maxSize[$i] == 'Good Description'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}
					elseif($maxSize[$i] == 'Product Name'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}else{
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(12);
					}
				}
//exit;
			$order_items = $results['Order']['Orderitem'];
            $in_items = $results['Invoiceitem'];
            $i = 0; $j=0;
			$dataArray = array();
			$idx = '1';
			$index = '1';						
			$k = 0;
	        foreach($order_items as $v) { 
	            if(isset($in_items[$i]['orderitemid'])){
	                if($in_items[$i]['orderitemid'] == $v['id']){ 
						$dataArray[$index][$k] = $in_items[$i]['bundleno'];
						if($_GET['p'] == 0 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = $this->requestAction('App/getHeatnumber/'.$id.'/'.$in_items[$i]['id']);
							/*if(!empty($in_items[$i]['heatnumber'])){
								
								$dataArray[$index][++$k] = $in_items[$i]['heatnumber'];		
							}else{
								$dataArray[$index][++$k] = "----";
							}*/							
						}
												
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['productname']; 
						
						if($_GET['p'] == 2){
							if(isset($in_items[$i]['Licence']['advancelicenceno'])){
								$dataArray[$index][++$k] = $in_items[$i]['Licence']['advancelicenceno'];	
							}else{
								$dataArray[$index][++$k] = '';
							}							
						}else{
							$dataArray[$index][++$k] = ''; 	
						}				 
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Standard']['standard_name'];
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Grade']['grade_name'];
						$dataArray[$index][++$k] = setGDmmformat($order_items[$j]['Productcategory']['Size']['gdmm'],$order_items[$j]['length']);
						$dataArray[$index][++$k] = $in_items[$i]['pcs'];
						//if($_GET['p'] != 2){
							 $dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mt']);
						//}
						if($_GET['p'] == 0 || $_GET['p'] == 1){
						$dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mtr']).' '.$order_items[$j]['Productcategory']['Producttaxonomy']['unit'];
						}
						if($_GET['p'] == 2 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = decimalQty($in_items[$i]['exempted']);
						}
						if($_GET['p'] == 1){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['price']);
						}
						if($_GET['p'] == 2){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['price_mt']);
						}
						if($_GET['p'] == 1 || $_GET['p'] == 2){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['netprice']);
						}
						if($_GET['p'] == 0 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = decimalWeight($in_items[$i]['netweight']);
							$dataArray[$index][++$k] = decimalWeight($in_items[$i]['grossweight']);	
						}
            		++$i; 
					$k = 0;
					$idx++;
					$index++;            		                  
                   } 
                   ++$j;
                }
            }
            $id = ++$dyn_cell;
			$size = count($dataArray);

			/*Start add Item value in excell*/
			foreach ($dataArray as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if($_GET['p'] != 2){ 
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.$id.':'.$product_column_nxt_key.$id);/*for merge prodctname and its neighbour column value*/
					}				
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_bottom);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_right);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, $value[$i]);						

						/*Start logic for add decimal position because excel removed automatically if value has empty zero like its show 5.00 as 5*/
						$ex = explode('.', $value[$i]);
						if(isset($ex[1])){
							if(is_numeric($ex[1])){
								$zero = '';
								if(strlen($ex[1]) == 2){
									$zero = '0.00';
								}
								if(strlen($ex[1]) == 3){
									$zero = '0.000';
								}
								$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left)->getNumberFormat()->setFormatCode($zero);	
							}					
						}
						/*End logic of add decimal position with zeros*/	

						/*PHP_Excel default set numberic value to align right, while string value to align left*/
						/*Set quntity align right*/
						if($i == 8 || $i == 9){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
						}

					}
					else {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, '');
					}
				}
				$id++;
			}
			$nxt_val = $id;
			$objPHPExcel->getActiveSheet()->getStyle($nxt_val)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_right);
		if($_GET['p'] == 1 || $_GET['p'] == 2){
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_left);
		}
		if($_GET['p'] == 0){
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($qtymtr_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($netweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($grossweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymtr_column_key.$nxt_val,$results['Invoice']['totalqty']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netweight_column_key.$nxt_val,$results['Invoice']['netweight']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($grossweight_column_key.$nxt_val,$results['Invoice']['grossweight']);
		}

		$amount_field  = getdelivery_type($results['Order']['delivery_type']);/*fetch amount related field from invoice table based on delivery type*/

		if($_GET['p'] == 1){	
			$objPHPExcel->getActiveSheet()->getStyle('F'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($qtymtr_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymtr_column_key.$nxt_val,$results['Invoice']['totalqty']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$nxt_val,strtoupper($amount_field).' Value');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$nxt_val,$results['Invoice'][$amount_field]);			
		}

		if($_GET['p'] == 2){
			$objPHPExcel->getActiveSheet()->getStyle('F'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($exempted_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($exempted_column_key.$nxt_val,$results['Invoice']['totalexempted']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$nxt_val,strtoupper($amount_field).' Value');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$nxt_val,$results['Invoice'][$amount_field]);					
		}
		if($_GET['p'] == 3){

			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($exempted_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($netweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($grossweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($exempted_column_key.$nxt_val,$results['Invoice']['totalexempted']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netweight_column_key.$nxt_val,$results['Invoice']['netweight']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($grossweight_column_key.$nxt_val,$results['Invoice']['grossweight']);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'Exempted Materials Stainless Steel Seamless Pipes/Tubes (Hot/Finished)');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'We intend to claim rewards under Merchandise Exports From India Scheme (MEIS)');
	
		}

		if($_GET['p'] == 1 || $_GET['p'] == 2){

			/*$amount_section = $this->requestAction(
                array('controller' => 'invoices', 'action' => 'dependentAmountField'),
                array("invoice" => $results['Invoice'],'delivery_type'=>$results['Order']['delivery_type'])
            );*/
			$amount_section = dependentAmountField($results['Invoice'],$results['Order']['delivery_type']);

			/* Start Print footer amount related section */
			foreach ($amount_section as $amount_sectionvalue) {
				$index = ++$nxt_val;
				/*applying numberformat to display specified decimalpoint, if number is integer */

				$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$index)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$index)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$index)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$index)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$index)->applyFromArray($border_top);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$index,$amount_sectionvalue['label']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$index.':'.$maxExcelColumn.$index)->applyFromArray($border_bottom);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$index,$amount_sectionvalue['value']);
			}
			/* End Print footer amount related section */			
			$num = $this->requestAction('App/numtowords/'.$results['Invoice']['finalvalue'].'/'.$results['Order']['Price']['fullform']);
			$objPHPExcel->getActiveSheet()->getStyle($nxt_val)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,$num)->mergeCells('A'.$nxt_val.':I'.$nxt_val);
		}

		/*bottom border of amount in words value*/
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
		
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL NET WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalnetweight'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$nxt_val.':G'.$nxt_val)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($border_left);	
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Signature & Date');
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top);
			$st1 = $nxt_val;

			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL GROSS WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalgrossweight'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$nxt_val.':G'.$nxt_val)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($border_left);	
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'FOR SHALCO INDUSTRIES PVT. LTD')->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);			

			$gradebaseweight = showAllGradeWeight($results['Invoice']['gradebaseweight'],$results['Invoice']['customgradebaseweight'],$_GET['p']);
			//$gradebaseweight = str_replace('<br/>', "\n", showAllGradeWeight($results['Invoice']['gradebaseweight'],$results['Invoice']['customgradebaseweight']));
			
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$rw_grd = $nxt_val + 5;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($style_vertical);	
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,$gradebaseweight)->mergeCells('A'.$nxt_val.':G'.$rw_grd);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val.':H'.$rw_grd)->applyFromArray($border_left);	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.''.$rw_grd);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'DIRECTOR');
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($style_vertical);	
			
					
			$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);


			$objPHPExcel->getActiveSheet()->getStyle('A8:'.$maxExcelColumn.$rw_grd)->applyFromArray($border_style);
			ob_clean();

			/*header('Content-Type: application/vnd.ms-excel');*/

			if($_GET['type'] == '2'){

				//$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
				
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
				//$objPHPExcel->getDefaultStyle()->getAlignment()->setIndent(0);
				//$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				
				$rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/dompdf';

				$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
				//$rendererLibrary = 'dompdf.php';

				if (!PHPExcel_Settings::setPdfRenderer(
				 	$rendererName,
				 	$rendererLibraryPath
				 //	$rendererLibrary
				)) {
				die(
				 'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
				 '<br />' .
				 'at the top of this script as appropriate for your directory structure'
				 );
				}
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
			
				//$objPHPExcel->getActiveSheet()->setTitle('Orari');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');	
				$objWriter->setPreCalculateFormulas (false);
				$objWriter->save('php://output');			
			}
			if($_GET['type'] == '1'){ 
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename='.$reportname.".xlsx");
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');
			}
			/*if($_GET['type'] == '3'){

				//$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
				$rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/tcpdf';

				$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
				//$rendererLibrary = 'dompdf.php';

				if (!PHPExcel_Settings::setPdfRenderer(
				 	$rendererName,
				 	$rendererLibraryPath
				 //	$rendererLibrary
				)) {
				die(
				 'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
				 '<br />' .
				 'at the top of this script as appropriate for your directory structure'
				 );
				}
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
			
				//$objPHPExcel->getActiveSheet()->setTitle('Orari');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');	
				$objWriter->setPreCalculateFormulas (false);
				$objWriter->save('php://output');			
			}*/		
			exit();
	}

	/* 
	* Name :Shopping info
	* Use : Update Shopping info 
	*/
	function shoppinginfo(){
	    if(empty($_POST['id'])) {
	    	$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
	    }
	    $this->autoRender = false; 
	    $logged_user_id = $this->Auth->user('id');
	    $date = date('Y-m-d', strtotime(str_replace('/','-', $_POST['sb_date'])));
	    $modified_date = date('Y-m-d H:i:s');
	    $status_update  = array('sbdate' => $date, 'sbno'=>$_POST['sb_no'],'shippedby'=>$logged_user_id,'shippeddate'=>$modified_date,'isshipped'=>1); 
	    $this->Invoice->id =$_POST['id'];
	    $this->Invoice->save($status_update);
	}

	/* 
	* Name : shoppingreminder
	* Use : Cron job for Email Reminder about fill shipping data of particular invoice after 10 days
	*/
	function shoppingreminder(){
	  	$this->autoRender = false;
	  	$data = $this->Invoice->find('all',array('conditions'=>array('TIMESTAMPDIFF(DAY,Invoice.created, NOW()) >' => 10,'Invoice.isshipped !='=>1)));
		foreach($data as $user_email) {
	  		$emails[] = $user_email['User']['email'];
	  	}
	  	if(!empty($data)){
	   		foreach($data as $remindemail){
	       		$emails[] = $remindemail['User']['email'];
	       		$Email = new CakeEmail();
			    $Email->to($emails);
			    $Email->subject('Reminder Shopping info update.');
			    $Email->from(array('shalco@gmail.com' => 'My Site'));
			    $Email->emailFormat('html');
			    $Email->send('Hello Sir,
						Please update you Shopping update info ');
	   		}
	  	}
	}

	/** 
	* Name : invoicereports
	* Use : generate invoice based reports
	* @param int $id id of record
	*/
	function invoicereports($id=null){
		$id = base64_decode($id);
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

        $this->loadmodel('Setting');
        $settings = $this->Setting->find('first');

		$this->loadmodel('Licence'); 
		$this->loadmodel('Order');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
  		$this->Order->unbindModel(array('hasMany' => array('Orderitem')));
		$results = $this->Invoice->find('first',array('conditions'=>array('Invoice.id' => $id),'recursive'=>3)); 
		$productcat_filter = $this->_group_ty($results['Invoiceitem'], 'procatid'); 
		$reportname = $results['Order']['Client']['company_name'];

		$shalco_header_style = array(
		    'font' => array('bold' => true,'size' => 36,'name' => 'Calibri'),		    
		);
		$shalco_address_style = array(
		    'font' => array('size' => 14,'name' => 'Calibri'),		    
		);	
		$reportname_style = array(
		    'font' => array('bold' => true,'size' => 26,'name' => 'Calibri'),		    
		);
	

		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
		$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
		$border_right_thick = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));		

		/*Start IN PROCESS INSPECTION REPORT*/
		$dyn_cell = 2;
		$dyn_name_report = 'IN PROCESS INSPECTION REPORT';
		$maxExcelColumn = 'J';

		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($shalco_header_style);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,strtoupper('SHALCO INDUSTRIES PVT.LTD'))->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);	

		$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell)->applyFromArray($shalco_address_style);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,strtoupper('56 KIKA STREET , GULALAWADI 2nd FLOOR OFFICE NO -22 MUMBAI 400004 , INDIA'))->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);		

		$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell)->applyFromArray($reportname_style);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,strtoupper($dyn_name_report))->mergeCells('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);			
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. ++$dyn_cell,'CUSTOMER NAME')->mergeCells('A'.$dyn_cell.':C'.$dyn_cell);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $dyn_cell,'')->mergeCells('D'.$dyn_cell.':F'.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$dyn_cell)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'. $dyn_cell,'DATE & SHIFT')->mergeCells('G'.$dyn_cell.':H'.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'. $dyn_cell,'')->mergeCells('I'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);	
		$objPHPExcel->getActiveSheet()->getStyle('I'.$dyn_cell)->applyFromArray($border_left);	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);			
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. ++$dyn_cell,'P.O.NO')->mergeCells('A'.$dyn_cell.':C'.$dyn_cell);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $dyn_cell,'')->mergeCells('D'.$dyn_cell.':F'.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$dyn_cell)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'. $dyn_cell,'W.O.NO')->mergeCells('G'.$dyn_cell.':H'.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'. $dyn_cell,'')->mergeCells('I'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);	
		$objPHPExcel->getActiveSheet()->getStyle('I'.$dyn_cell)->applyFromArray($border_left);	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. ++$dyn_cell,'PRODUCT')->mergeCells('A'.$dyn_cell.':C'.$dyn_cell);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $dyn_cell,'')->mergeCells('D'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$dyn_cell)->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_bottom);

		/*IN PROCESS INSPECTION REPORT :: Item Header and SubHeader Start*/
		$itemhead = ++$dyn_cell;
		$itemsubhead = ++$dyn_cell;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $itemhead,'WORK CENTER')->mergeCells('A'.$itemhead.':A'.$itemsubhead);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'. $itemhead,'DATE')->mergeCells('B'.$itemhead.':B'.$itemsubhead);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$itemhead.':B'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'. $itemhead,'SR. NO.')->mergeCells('C'.$itemhead.':C'.$itemsubhead);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$itemhead.':C'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $itemhead,'SIZE');
		$objPHPExcel->getActiveSheet()->getStyle('D'.$itemhead)->applyFromArray($border_left);		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'. $itemhead,'GRADE')->mergeCells('E'.$itemhead.':E'.$itemsubhead);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$itemhead.':E'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'. $itemhead,'HEAT NO.')->mergeCells('F'.$itemhead.':F'.$itemsubhead);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$itemhead.':F'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'. $itemhead,'OBSERVATION');
		$objPHPExcel->getActiveSheet()->getStyle('G'.$itemhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'. $itemhead,'REMARKS')->mergeCells('J'.$itemhead.':J'.$itemsubhead);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$itemhead.':J'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$itemhead.':'.$maxExcelColumn.$itemhead)->applyFromArray($border_bottom);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $itemsubhead,'OD/NB x THK/SCH x LENGTH');
		$objPHPExcel->getActiveSheet()->getStyle('D'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'. $itemsubhead,'OD');
		$objPHPExcel->getActiveSheet()->getStyle('G'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'. $itemsubhead,'THK');
		$objPHPExcel->getActiveSheet()->getStyle('H'.$itemsubhead)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'. $itemsubhead,'LENGTH');
		$objPHPExcel->getActiveSheet()->getStyle('I'.$itemhead)->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$itemsubhead.':'.$maxExcelColumn.$itemsubhead)->applyFromArray($border_bottom);

		/*IN PROCESS INSPECTION REPORT :: Item Header and SubHeader End*/
		
		/*IN PROCESS INSPECTION REPORT :: Item Details Start*/
		$start_row = ++$dyn_cell;
		$end_row = $dyn_cell + 3;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $start_row,'')->mergeCells('A'.$start_row.':A'.$end_row);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'. $start_row,'')->mergeCells('B'.$start_row.':B'.$end_row);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$start_row.':B'.$end_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'. $start_row,'')->mergeCells('C'.$start_row.':C'.$end_row);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$start_row.':C'.$end_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'. $start_row,'');
		$objPHPExcel->getActiveSheet()->getStyle('D'.$start_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'. $start_row,'')->mergeCells('E'.$start_row.':E'.$end_row);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$start_row.':E'.$end_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'. $start_row,'')->mergeCells('F'.$start_row.':F'.$end_row);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$start_row.':F'.$end_row)->applyFromArray($border_left);
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'. $start_row,'');
		$objPHPExcel->getActiveSheet()->getStyle('G'.$start_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'. $start_row,'');
		$objPHPExcel->getActiveSheet()->getStyle('H'.$start_row)->applyFromArray($border_left);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'. $start_row,'');
		$objPHPExcel->getActiveSheet()->getStyle('I'.$start_row)->applyFromArray($border_left);	

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'. $start_row,'Acceptable')->mergeCells('J'.$start_row.':J'.$end_row);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$start_row.':J'.$end_row)->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$start_row.':'.$maxExcelColumn.$end_row)->applyFromArray($border_bottom);
		/*End IN PROCESS INSPECTION REPORT*/

		ob_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $reportname . ".xlsx");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
	}

	function _group_ty($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val['Orderitem'][$key]][] = $val;
        }
        return $return;
    }

	function get_random_range($value,$type){ //return '-';
		if(empty($value)){
			return '';
		}else{		
			$arr = array('');					
			if($type == 'length'){
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val + 1;
				$max_val = $original_val + 10;
				//$min_val_exclude = $original_val - 0.04;
				//$max_val_exclude = $original_val + 0.04;
				//$arr = range($min_val_exclude, $max_val_exclude,0.01);				
				return $this->float_rand($min_val,$max_val,2,$arr);
			}
			if($type == 'od'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[0])){ return '';}
				$value = trim($size_explode[0]); 
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val - 0.10;
				$max_val = $original_val + 0.10;
				$min_val_exclude = $original_val - 0.04;
				$max_val_exclude = $original_val + 0.04;
				$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}			
			if($type == 'thickness'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[1])){ return '';}
				$value = trim($size_explode[1]);
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val - 0.10;
				$max_val = $original_val + 0.10;
				$min_val_exclude = $original_val - 0.04;
				$max_val_exclude = $original_val + 0.04;
				$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}
			if($type == 'od_min'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[0])){ return '';}
				$value = trim($size_explode[0]); 
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val - 0.10;
				$max_val = $original_val - 0.04;
				//$min_val_exclude = $original_val - 0.04;
				//$max_val_exclude = $original_val + 0.04;
				//$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}
			if($type == 'od_max'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[0])){ return '';}
				$value = trim($size_explode[0]); 
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val + 0.10;
				$max_val = $original_val + 0.04;
				//$min_val_exclude = $original_val - 0.04;
				//$max_val_exclude = $original_val + 0.04;
				//$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}
			if($type == 'thickness_min'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[1])){ return '';}
				$value = trim($size_explode[1]);
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val - 0.10;
				$max_val = $original_val - 0.04;
				//$min_val_exclude = $original_val - 0.04;
				//$max_val_exclude = $original_val + 0.04;
				//$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}
			if($type == 'thickness_max'){
				$size_explode = explode('X', $value);
				if(empty($size_explode)){ return '';}
				if(!isset($size_explode[1])){ return '';}
				$value = trim($size_explode[1]);
				$original_val = (float) filter_var( $value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
				$min_val = $original_val + 0.10;
				$max_val = $original_val + 0.04;
				//$min_val_exclude = $original_val - 0.04;
				//$max_val_exclude = $original_val + 0.04;
				//$arr = range($min_val_exclude, $max_val_exclude,0.01);
				return $this->float_rand($min_val,$max_val,2,$arr);
			}												
		}	
	}
	/**
	 * Generate Float Random Number
	 * @param float $Min Minimal value
	 * @param float $Max Maximal value
	 * @param int $round The optional number of decimal digits to round to. default 0 means not round
	 * @return float Random float value
	 */
	function float_rand($min, $max, $round=0,$exclude_array){
	    $randomfloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
	    if($round>0)
	        $randomfloat = round($randomfloat,$round);
	    $return_val = in_array($randomfloat, $exclude_array) ? $this->float_rand($min, $max, $round, $exclude_array) : $randomfloat;
	    return $return_val;
	}

    function invoicereports_ks($id=null,$product_taxonomy_id=null){

    	$id = base64_decode($id);
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		ini_set('memory_limit', '2048M');
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		

/*      $this->loadmodel('Setting');
        $settings = $this->Setting->find('first');*/

		$this->loadmodel('Licence'); 
		$this->loadmodel('Order');  
		$this->loadmodel('Client');  
		$this->loadmodel('Invoiceitem');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
  		$this->Order->unbindModel(array('hasMany' => array('Orderitem'),'belongsTo' => array('User','Bank','Dispatch','Price')));
  		$this->Client->unbindModel(array('belongsTo' => array('Coordinator','User'),'hasMany' => array('Address')));
  		$this->Invoiceitem->unbindModel(array('belongsTo' => array('Licence')));
		$results = $this->Invoice->find('first',array('conditions'=>array('Invoice.id' => $id),'recursive'=>3)); 

		$productcat_filter = $this->_group_ty($results['Invoiceitem'], 'procatid'); 
		$reportname = $results['Order']['Client']['company_name'];
		
/*		$shalco_header_style = array(
		    'font' => array('bold' => true,'size' => 36,'name' => 'Calibri'),		    
		);
		$shalco_address_style = array(
		    'font' => array('size' => 14,'name' => 'Calibri'),		    
		);	
		$reportname_style = array(
		    'font' => array('bold' => true,'size' => 26,'name' => 'Calibri'),		    
		);*/
	

		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
		$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
		$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
		$border_right_thick = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));	

		$fileName = WWW_ROOT.'Invoice_Excel_Report.xlsx';
		/** automatically detect the correct reader to load for this file type */
		$objReader = PHPExcel_IOFactory::createReaderForFile($fileName);
		$objPHPExcel = $objReader->load($fileName);
		$loadedSheetNames = $objPHPExcel->getSheetNames();
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			

			$result_array = $productcat_filter[$product_taxonomy_id];
			
			$clientname = 'M/s '.$results['Order']['Client']['company_name'];
			$invoicedate = $this->requestAction('App/addDayswithdate/'.$results['Order']['clientpodate'].'/5');
			$clientpo = $results['Order']['clientpo'].'     DATE:- '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Order']['clientpodate']));
			$productname = $results['Invoiceitem'][0]['Orderitem']['Productcategory']['productname'];

			$dataArray1 = array();
			$dataArray2 = array();
			$dataArray3 = array();
			$dataArray4 = array();
			$dataArray5 = array();
			$dataArray6 = array();
			$dataArray7 = array();
			$dataArray8 = array();
			$dataArray9 = array();
			$dataArray10 = array();
			$dataArray11 = array();			

			$index = '1';
			$k1 = 0;$k2 = 0;$k3 = 0;$k4 = 0;$k5 = 0;$k6 = 0;$k7 = 0;$k8 = 0;$k9 = 0;$k10 = 0;$kl1 = 0;
			foreach($result_array as $v) {

				$heatnumber = $this->requestAction('App/getHeatnumber/'.$v['id'].'/'.$v['invoiceid']);

				$dataArray1[$index][$k1] = $v['Orderitem']['Productcategory']['Size']['gdmm'];
				$dataArray1[$index][++$k1] = '';
				$dataArray1[$index][++$k1] = $v['Orderitem']['length'];
				$dataArray1[$index][++$k1] = $v['Orderitem']['Productcategory']['Grade']['grade_name'];
				$dataArray1[$index][++$k1] = $v['pcs']; 
				$dataArray1[$index][++$k1] = '';
				$dataArray1[$index][++$k1] = '';
				$dataArray1[$index][++$k1] = '';
				$dataArray1[$index][++$k1] = '';
				$dataArray1[$index][++$k1] = '';
				$k1 = 0;

				$dataArray4[$index][$k4] = '';
				$dataArray4[$index][++$k4] = '';
				$dataArray4[$index][++$k4] = $v['Orderitem']['sr_no'];
				$dataArray4[$index][++$k4] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray4[$index][++$k4] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray4[$index][++$k4] = $heatnumber;
				$dataArray4[$index][++$k4] = $v['Orderitem']['Productcategory']['Size']['gdmm'];
				$dataArray4[$index][++$k4] = $v['Orderitem']['Productcategory']['Size']['gdmm'];
				$dataArray4[$index][++$k4] = $v['Orderitem']['length'];
				$dataArray4[$index][++$k4] = 'Acceptable';										
				$k4 = 0;

				$dataArray5[$index][$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = '';
				$dataArray5[$index][++$k5] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray5[$index][++$k5] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray5[$index][++$k5] = $heatnumber;
				$dataArray5[$index][++$k5] = $v['pcs'];
			    $k5 = 0;

				$dataArray6[$index][$k6] = '';
				$dataArray6[$index][++$k6] = $v['Orderitem']['sr_no'];
				$dataArray6[$index][++$k6] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray6[$index][++$k6] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray6[$index][++$k6] = $heatnumber;
				$dataArray6[$index][++$k6] = '';
				$dataArray6[$index][++$k6] = '';
				$dataArray6[$index][++$k6] = '';
				$dataArray6[$index][++$k6] = $v['pcs'];
				$dataArray6[$index][++$k6] = 'Acceptable';
			    $k6 = 0;

				$dataArray7[$index][$k7] = '';
				$dataArray7[$index][++$k7] = $v['Orderitem']['sr_no'];
				$dataArray7[$index][++$k7] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray7[$index][++$k7] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray7[$index][++$k7] = $heatnumber;
				$dataArray7[$index][++$k7] = '';
				$dataArray7[$index][++$k7] = '';
				$dataArray7[$index][++$k7] = '';
				$dataArray7[$index][++$k7] = $v['pcs'];
				$dataArray7[$index][++$k7] = 'Acceptable';
			    $k7 = 0;

				$dataArray8[$index][$k8] = $v['Orderitem']['sr_no'];
				$dataArray8[$index][++$k8] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray8[$index][++$k8] = ''; 
				$dataArray8[$index][++$k8] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray8[$index][++$k8] = $heatnumber;
				$dataArray8[$index][++$k8] = '';
				$dataArray8[$index][++$k8] = $v['pcs'];
				$dataArray8[$index][++$k8] = $v['pcs'];
				$dataArray8[$index][++$k8] = '';
				$dataArray8[$index][++$k8] = 'Acceptable';
			    $k8 = 0;

				$dataArray9[$index][$k9] = $v['Orderitem']['sr_no'];
				$dataArray9[$index][++$k9] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray9[$index][++$k9] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray9[$index][++$k9] = $heatnumber;
				$dataArray9[$index][++$k9] = $v['pcs'];
				$dataArray9[$index][++$k9] = '';
				$dataArray9[$index][++$k9] = 'Acceptable';
			    $k9 = 0;

				$dataArray10[$index][$k10] = $v['Orderitem']['sr_no'];
				$dataArray10[$index][++$k10] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray10[$index][++$k10] = '';
				$dataArray10[$index][++$k10] = '';
				$dataArray10[$index][++$k10] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray10[$index][++$k10] = '';
				$dataArray10[$index][++$k10] = '';
				$dataArray10[$index][++$k10] = $heatnumber;
			    $k10 = 0;

				$dataArray11[$index][$kl1] = $v['Orderitem']['sr_no'];
				$dataArray11[$index][++$kl1] = $v['Orderitem']['Productcategory']['Size']['gdmm'] . ' X  '.$v['Orderitem']['length']; 
				$dataArray11[$index][++$kl1] = ''; 
				$dataArray11[$index][++$kl1] = $v['Orderitem']['Productcategory']['Grade']['grade_name']; 
				$dataArray11[$index][++$kl1] = $heatnumber;
				$dataArray11[$index][++$kl1] = $v['pcs'];
				$dataArray11[$index][++$kl1] = $v['Orderitem']['length'];
				$dataArray11[$index][++$kl1] = $this->get_random_range($v['Orderitem']['Productcategory']['Size']['gdmm'],'od_min');
				$dataArray11[$index][++$kl1] = $this->get_random_range($v['Orderitem']['Productcategory']['Size']['gdmm'],'od_max');
				$dataArray11[$index][++$kl1] = $this->get_random_range($v['Orderitem']['Productcategory']['Size']['gdmm'],'thickness_min');
				$dataArray11[$index][++$kl1] = $this->get_random_range($v['Orderitem']['Productcategory']['Size']['gdmm'],'thickness_max');
				$dataArray11[$index][++$kl1] = 'Acceptable';
			    $kl1 = 0;		

			    $index++;			    
            }

            unset($k1);unset($k2);unset($k3);unset($k4);unset($k5);unset($k6);unset($k7);unset($k8);unset($k9);unset($k10);unset($k11);

		/* Start First worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('B15',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('I5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('I6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('A8',$productname);

			$id = '25';
			$size = count($dataArray1);
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(25, $size);
			foreach ($dataArray1 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						if($j == 'A'){
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':B'.$id);
						}
						if($j == 'F'){
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':G'.$id);
						}
						if($j == 'H'){
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':J'.$id);
						}								
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray1);
        /* Start First worksheet */
            
		/* Start Fourth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(3);
			$objPHPExcel->getActiveSheet()->setCellValue('D5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('I5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('D6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('D7',$productname);
/*			$i = 10;
			$j = 0;
			$k = 'D';*/

            $maxExcelColumn = 'J';
			$id = '10';
			//$new_id = '12';
			$size = count($dataArray4);
			foreach ($dataArray4 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {						
						if($j < 'G'){
							$new_id = $id + 2; 
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':'.$j.$new_id);
							$objPHPExcel->getActiveSheet()->setCellValue($j.$id, $value[$i]);
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
							$objPHPExcel->getActiveSheet()->getStyle($j.$id.':'.$j.$new_id)->applyFromArray($border_left);
						}
						for($m=0;$m<3;$m++){ 
							$id_l = $id + $m;
							if($j == 'G'){
								$objPHPExcel->getActiveSheet()->setCellValue($j.$id_l, $this->get_random_range($value[$i],'od'));
							}
							if($j == 'H'){
								$objPHPExcel->getActiveSheet()->setCellValue($j.$id_l, $this->get_random_range($value[$i],'thickness'));
							}
							if($j == 'I'){
								$objPHPExcel->getActiveSheet()->setCellValue($j.$id_l, $this->get_random_range($value[$i],'length'));
							}
							if($j == 'J'){
								$objPHPExcel->getActiveSheet()->setCellValue($j.$id_l, $value[$i]);
							}
							$objPHPExcel->getActiveSheet()->getStyle($j.$id_l)->applyFromArray($border_top);
							$objPHPExcel->getActiveSheet()->getStyle($j.$id_l)->applyFromArray($border_left);								
						}			
						//$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(12);
					}
				}
				$id = $new_id + 1;
				//++$id;
			} 
			unset($dataArray4);

			$dyn_cell = $id;
			$dyn_cell5 = $id + 5;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'QC.SUPERVISOR')->mergeCells('A'.$dyn_cell.':H'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$dyn_cell, 'HEAD QA / QC	')->mergeCells('I'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);

		/* End Fourth worksheet */

		/* Start Fifth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(4);
			$objPHPExcel->getActiveSheet()->setCellValue('C8',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('J8',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C9',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C10',$productname);
			
            $maxExcelColumn = 'K';
			$id = '12';
			$size = count($dataArray5);
			foreach ($dataArray5 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {						
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);						
						$objPHPExcel->setActiveSheetIndex(4)->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray5);

			$dyn_cell = $id;
			$dyn_cell5 = $id + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'PRODUCTION SUPERVISIOR')->mergeCells('A'.$dyn_cell.':G'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$dyn_cell, 'PRODUCTION INCHARGE	')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);

		/* End Fifth worksheet */

		/* Start Sixth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(5);			
			$objPHPExcel->getActiveSheet()->setCellValue('C5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('I5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C7',$productname);
			
            $maxExcelColumn = 'J';
			$id = '9';
			$size = count($dataArray6);
			foreach ($dataArray6 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {						
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);						
						$objPHPExcel->setActiveSheetIndex(5)->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray6);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$id, 'REMARKS:- ')->mergeCells('A'.$id.':'.$maxExcelColumn.$id);

			$dyn_cell = ++$id;
			$dyn_cell5 = $id + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell,'PRODUCTION SUPERVISIOR')->mergeCells('A'.$dyn_cell.':G'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$dyn_cell,'PRODUCTION MANAGER	')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);

		/* End Sixth worksheet */

		/* Start Seventh worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(6);
			$objPHPExcel->getActiveSheet()->setCellValue('C5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('I5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C7',$productname);
			
            $maxExcelColumn = 'J';
			$id = '10';
			$size = count($dataArray7);
			foreach ($dataArray7 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray7);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$id, 'REMARKS:- ')->mergeCells('A'.$id.':'.$maxExcelColumn.$id);

			$dyn_cell = ++$id;
			$dyn_cell5 = $id + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'PRODUCTION SUPERVISIOR')->mergeCells('A'.$dyn_cell.':G'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$dyn_cell, 'PRODUCTION MANAGER	')->mergeCells('H'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);

		/* End Seventh worksheet */

		/* Start Eighth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(7);
			$objPHPExcel->getActiveSheet()->setCellValue('C5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('h5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C7',$productname);

  	        $maxExcelColumn = 'J';
			$id = '9';
			$size = count($dataArray8);
			foreach ($dataArray8 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						if($j == 'B'){							
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':C'.$id);
						}
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray8);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$id.':'.$maxExcelColumn.$id)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$id, 'REMARKS:- ')->mergeCells('A'.$id.':'.$maxExcelColumn.$id);

			$dyn_cell = ++$id;
			$dyn_cell5 = $id + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'QC Supervisior')->mergeCells('A'.$dyn_cell.':H'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$dyn_cell, 'HEAD QA / QC')->mergeCells('I'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);			
		/* End Eighth worksheet */

		/* Start Ninth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(8);
			$objPHPExcel->getActiveSheet()->setCellValue('B5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('F5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('B6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('B7',$productname);

  	        $maxExcelColumn = 'G';
			$id1 = '9';
			$size = count($dataArray9);

			foreach ($dataArray9 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id1)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id1)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id1, $value[$i]);
					}
				}
				$id1++;
			}
			unset($dataArray9);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$id1.':'.$maxExcelColumn.$id1)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$id1.':'.$maxExcelColumn.$id1)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$id1, 'REMARKS:- ')->mergeCells('A'.$id1.':'.$maxExcelColumn.$id1);

			$dyn_cell = ++$id1;
			$dyn_cell5 = $id1 + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'PRODUCTION SUPERVISIOR')->mergeCells('A'.$dyn_cell.':E'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$dyn_cell, 'PRODUCTION MANAGER')->mergeCells('F'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);

		/* End Ninth worksheet */

		/* Start Tenth worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(9);
			$objPHPExcel->getActiveSheet()->setCellValue('C5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('H5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C7',$productname);

			$id = '9';
			$size = count($dataArray10);
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(9, $size);
			foreach ($dataArray10 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						if($j == 'B'){
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':D'.$id);
						}
						if($j == 'E'){
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id.':F'.$id);
						}												
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id, $value[$i]);
					}
				}
				$id++;
			}
			unset($dataArray10);			
		/* End Tenth worksheet */

		/* Start Eleventh worksheet */
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(10);
			$objPHPExcel->getActiveSheet()->setCellValue('C5',$clientname);
			$objPHPExcel->getActiveSheet()->setCellValue('L5',$invoicedate);
			$objPHPExcel->getActiveSheet()->setCellValue('C6',$clientpo);
			$objPHPExcel->getActiveSheet()->setCellValue('C7',$productname);

            $maxExcelColumn = 'L';
			$id1 = '13';
			$size = count($dataArray11);
			foreach ($dataArray11 as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						if($j == 'B'){							
							$objPHPExcel->getActiveSheet()->mergeCells($j.$id1.':C'.$id1);
						}						
						$objPHPExcel->getActiveSheet()->getStyle($j.$id1)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id1)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->setCellValue($j.$id1, $value[$i]);
					}
				}
				$id1++;
			}
			unset($dataArray11);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$id1.':'.$maxExcelColumn.$id1)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$id1.':'.$maxExcelColumn.$id1)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$id1, 'REMARKS:- ')->mergeCells('A'.$id1.':'.$maxExcelColumn.$id1);

			$dyn_cell = ++$id1;
			$dyn_cell5 = $id1 + 8;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dyn_cell, 'QC Supervisior')->mergeCells('A'.$dyn_cell.':H'.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$dyn_cell, 'HEAD QA / QC')->mergeCells('I'.$dyn_cell.':'.$maxExcelColumn.$dyn_cell5);
			$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.$dyn_cell5)->applyFromArray($border_style);				
		/* End Eleventh worksheet */

			$filename = "Invoice-Report.xlsx";
			/*header('Content-Type: appli cation/vnd.ms-excel');*/
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter->save('php://output');
		//exit;
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    }
}