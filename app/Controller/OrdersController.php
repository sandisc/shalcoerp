<?php
/*
 * @Controller name: Order Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Order management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));
class OrdersController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Order Management');
	}
	/*
   	 * Name: index
   	 * Use: Order listing page.
	*/
	function index() {
        $this->set('pageTitle','Order List');     
     	$this->set('ajaxaction','ajaxlisting');
		$this->loadmodel('Client');
		$this->loadmodel('Orderitem');	
        $this->set('pageTitle',' Order List');
        $userid = AuthComponent::user('id');
        if ($this->request->is('post')) {
	         	$objReader = PHPExcel_IOFactory::createReaderForFile($this->request->data['Order']['uploadfile']['tmp_name']);

				// Set your options.
				$objReader->setReadDataOnly(true);

				// Tell PHPExcel to load this file and make its best guess as to its type.
				$objPHPExcel = $objReader->load($this->request->data['Order']['uploadfile']['tmp_name']);
				$loadedSheetNames = $objPHPExcel->getSheetNames();

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');

				foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
				    $objWriter->setSheetIndex($sheetIndex);
				    $objWriter->save($loadedSheetName.'.csv');				    
				}
				foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				        	$worksheets[] = $worksheet->toArray();
				}
				    	
         
         	$this->Order->create();
         //	$filename = $this->request->data['Order']['uploadfile']['tmp_name'];
            //$filename = WWW_ROOT.$this->request->data['Order']['uploadfile']['name']; 
			//if (($handle = fopen($filename, "r")) !== FALSE) {
				$data = array();
				$item = array();
				$errors = '';
				$i = 0; /*use for fetch excel index*/
				$r = 1; /*pointing excel sheet row number, which is useful while set error on line number*/
				$sum = 0; /*use for calulate total of amount*/
				$clientpo = array();
				foreach($worksheets[0] as $row) {
    			//while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {    				
    				if($i == 1){
	       				$data['orderno'] = $this->getManualID();
	       				if(!empty($data[' '])){
	       					$data['orderdate'] = $row[0];
	       				}
	       				else{
	       					$data['orderdate'] =  date('Y-m-d');
	       				}
	       				if(!empty($row[1])){	       					
	       					if($this->getClientID($row[1])!=''){
	       						$data['bill_to'] = $this->getClientID($row[1]);	
	       					}
	       					else{
	       						$errors .= '<li>Client name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Client name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[2])){
	       						$data['ship_to'] = $row[2];	
	       				}
	       				else{
	       					$errors .= '<li>Shipping client is missing at line no '.$r.'</li>';			
	       				}
	       				
   						if($data['bill_to'] != '' && $data['ship_to'] != ''){
   							$data['billaddress_id'] = $this->getShipAddID($data['bill_to']);
   							$data['shipaddress_id'] = $this->getShipAddID($data['ship_to']);
   						}	       				
	       				if(!empty($row[3])){
       						if($row[3] == 'CIF'){
       							$data['delivery_type'] = 1;		
       						}elseif($row[3] == 'FOB'){
       							$data['delivery_type'] = 2;
       						}elseif($row[3] == 'Ex-work'){
       							$data['delivery_type'] = 3;
       						}elseif($row == 'CNF'){
       							$data['delivery_type'] = 4;
       						}else{
       							$errors .= '<li> This Delivery type is not available..</li>';
       						}
	       				}else{
	       					$errors .= '<li> This Delivery type is missing at line no '.$r.'</li>';
	       				}	       				
	       				if(!empty($row[4])){
	       					$data['tod'] = $row[4];
	       				}
	       				else{
	       					$data['tod'] = '-'; /*$errors = '<li>Terms of delievery is missing at line no '.$r.'</li>';*/	       					
	       				}
	       				if(!empty($row[5])){
	       					$data['top'] = $row[5];
	       				}
	       				else{
	       					$data['top'] = '-'; /*$errors .= '<li>Terms of payment is missing at line no '.$r.'</li>';*/
	       				}	       				
	       				if(!empty($row[6])){	       					
	       					if($this->getBankID($row[6]) != ''){
	       						$data['bank_id'] = $this->getBankID($row[6]);	
	       					}
	       					else{
	       						$errors .= '<li>Bank name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Bank name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[7])){	       					
	       					if($this->getUserID($row[7]) != ''){
	       						$data['approvalby'] = $this->getUserID($row[7]);	
	       					}
	       					else{
	       						$errors .= '<li>Approval name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Approval name is missing at line no '.$r.'</li>';
	       				}
	       				$data['fy'] = $this->getFinancialyear();       				
					}
					if($i == 4) {
	       				if(!empty($row[0])){	       					
	       					if($this->getDispatchID($row[0]) != ''){
	       						$dispa_id = $this->getDispatchID($row[0]);	
	       						if(!empty($dispa_id)){
	       							$data['dispatch_id'] = $dispa_id[1];
	       							$data['dispatchcode'] = $dispa_id[0];	       							
	       						}	       					
	       					}
	       					else{
	       						$errors .= '<li>Dispatch location is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Dispatch location is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[1])){
	       					if($this->getPriceId($row[1]) != ''){
	       						$price_id = $this->getPriceId($row[1]);	
	       						if(!empty($price_id)){
	       						$data['priceid'] = $price_id;
	       						}
	       					}
	       					else{
	       						$errors .= '<li>Price is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	
	       				}

	       				if(!empty($row[2])){
	       					$data['quantity'] = $row[2];
	       				}else {
	       					$data['quantity'] = ''; /*$errors .= '<li> Quantity is missing at line no '.$r.'</li>';*/
	       				}
	       				if(!empty($row[3])){
	       					$data['delivery_period'] = $row[3];
	       				}else{
	       					$data['delivery_period'] = ''; /*$errors .= '<li> Delivery period is missing at line no '.$r.'</li>';*/
	       				}
	       				if(!empty($row[4])){
	       					$data['dimensions'] = $row[4];	       					
	       				}else{
	       					$data['dimensions'] = '';
	       					/*$errors .= '<li> Dimension is missing at line no '.$r.'</li>';*/
	       				}
	       				if(!empty($row[5])){
	       					$data['comment'] = $row[5];
	       					
	       				}else{
	       					$data['comment'] = ''; /*$errors .= '<li> Comment is missing at line no '.$r.'</li>';*/
	       				}
	       				if(!empty($row[6])){
	       					$data['certifications'] = $row[6];
	       					
	       				}else{
	       					$data['certifications'] = ''; /*$errors .= '<li> Certifications is missing at line no '.$r.'</li>';*/
	       				}
	       				/*if(!empty($row[16])){
	       					$data['fy'] = $row[16];
	       					
	       				}else{
	       					$errors .= '<li> Financial Year is missing at line no '.$r.'</li>';
	       				}*/
	       				$data['status'] = 0;
	       				$data['approval_date'] = date('Y-m-d H:i:s');
	       				$data['ismanual'] = 1;
	       			}
	       			
	       			if($i > 6){
	       				$j = 1;/*use for set auto increment serial number*/
		       			if(!empty($row[0])){
							$item[$i]['sr_no'] = $row[0];
						}else{
							$item[$i]['sr_no'] = $j;
						}
	       				if(!empty($row[1])){
							//$item[$i]['clientpo'] = $row[1];
							$clientpo[] = $row[1];
						}
						else{
							$errors .= '<li>Client po is missing at line no '.$r.'</li>';
						}
						if(!empty($row[2])){
							if($this->getProductCat($row[2],$row[3])!='') {
	       						$item[$i]['procatid'] = $this->getProductCat($row[2],$row[3]);	
	       					}
	       					else{
	       						$errors .= '<li>Product Category is not exist with selected standard please check it at line no '.$r.'</li>';	
	       					}
						}
						else{
							$errors .= '<li>Product Category is missing at line no '.$r.'</li>';
						}
	       				if(!empty($row[3])){
	       					if($this->getStandardID($row[3])!='') {
	       						$item[$i]['standard_id'] = $this->getStandardID($row[3]);
			       				if(!empty($row[5])){	       					
			       					if($this->getGradeID($row[4],$item[$i]['standard_id'])!=''){
			       						$item[$i]['grade_id'] = $this->getGradeID($row[4],$item[$i]['standard_id']);	
			       					}
			       					else{
			       						$errors .= '<li>Grade is not exist in master module please insert it at line no '.$r.'</li>';	
			       					}	       					       					
			       				}
			       				else{
			       					$errors .= '<li>Grade is missing at line no '.$r.'</li>';	
			       				}	       							
	       					}
	       					else{
	       						$errors .= '<li>Standard is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}
	       				}	
	       				else{
	       					$errors .= '<li>Standard is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[5])){
	       					$item[$i]['size_id'] = $this->getSizeTableID($row[5]);	
	       				}
	       				else{
	       					$errors .= '<li>Size is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[6])){
	       					$item[$i]['length'] = $row[6];	
	       				}
	       				else{
	       					$errors .= '<li>Length is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[7])){
	       					$item[$i]['qty'] = $row[7];
	       				}
	       				else{
	       					$errors .= '<li>Quantity is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[8])){
	       					$item[$i]['price'] = $row[8];	
	       				}
	       				else{
	       					$errors .= '<li>Price is missing at line no '.$r.'</li>';			
	       				}
						if(!empty($row[9])){
							$item[$i]['amount'] = $row[9];
							$sum += $item[$i]['amount'];
						}
						else{
							$item[$i]['amount'] = $row[7] * $row[8];
							$sum += $item[$i]['amount'];
						}
					$j++;
					}
					$i++;
					$r++;
		    	} 
		    
		    	if(!empty($errors)){
		    		$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> Please try Again.<ul>'.$errors.'</ul></div>'));
		    				return false;
	       		}
	       		
	       		$data['clientpo'] = implode(',', $clientpo);
		    	$data['total'] = $sum;
		    	$data['createdby'] = $userid;
		    	$data['modifiedby'] = $userid;
		    	
				if($this->Order->save($data)){
					//$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Order imported Successfully.</div>'));
				}

				$last_orderid = $this->Order->getLastInsertID();
				if(!empty($last_orderid)){
					foreach($item as $items){
						$items['orderid'] = $last_orderid;
						$this->Orderitem->saveAll($items);
					}
					$this->redirect(array('controller' => 'orders', 'action' => 'edit',base64_encode($last_orderid)));
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Order imported Successfully.</div>'));
				}				
				//move_uploaded_file($this->request->data['Order']['uploadfile']['tmp_name'],WWW_ROOT.'files/orders/'.time().$this->request->data['Order']['uploadfile']['name']);
				//fclose($handle);
			//}		
        }
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','orderno','orderdate','company_name','Order.modified','first_name');
    	$order = array('Order.modified' => 'desc'); 
		$res = $this->datatable_append($order,$column);	
		$this->Order->unbindModel(array('belongsTo' => array('Dispatch','Price','Bank')));
		$count = $this->Order->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$this->Order->unbindModel(array('belongsTo' => array('Dispatch','Price','Bank')));
		$Orders = $this->Order->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>0));
		$this->set('order',$Orders);
		$this->set('count',$count);
	}

	/**
   	 * Name: edit
   	 * Use: update order.
     * @param int $id id of record
     * @return detail record of id   	 
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Standard');
		$this->loadmodel('Price');
		$this->loadmodel('Grade');
		$this->loadmodel('Orderitem');
		$this->loadmodel('Size');
		$this->loadmodel('Bank');
		$this->loadmodel('Client');
		$this->loadmodel('Productcategory');
		$this->loadmodel('Address');
		$this->loadmodel('Dispatch');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'orders/">Orders</a><i class="fa fa-circle"></i></li>');
		$this->set('pageTitle','Edit Order');
	
		/*Fetch data of particular ID*/
		$order = $this->Order->find('first', array('conditions' => array('Order.id' => $id ), 'recursive' => 2));
		$this->request->data = $order;
		/*Redirect if ID not exist in database*/
		if(empty($order)){
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$this->set('order',$order);

		$standard_all = $this->Standard->find('all', array('fields' => array('Standard.id','Standard.standard_name','Standard.procatid')));
		$this->set('standard_all',$standard_all);
/*
		$standards = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
		$this->set('standards',$standards);
*/
		$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
		$this->set('gdmm',$gdmm);

		$gdmm_all = $this->Size->find('all');
		$this->set('gdmm_all',$gdmm_all);

		$bank = $this->Bank->find('list',array('fields' => array('id','bank_alias')));
		$this->set('bank',$bank);	
		
		$client = $this->Client->find('list',array('fields' => array('Client.id', 'Client.company_name'),'recursive' => 0));
		$this->set('clients',$client);	
				
		$procat = $this->Productcategory->find('all',array('fields' => array('Productcategory.id','Productcategory.name')));
		$this->set('procat',$procat);
		
		$price_set = $this->Price->find('list',array('fields' => array('Price.id','Price.sign')));
		$this->set('price_set',$price_set);	

		$address = $this->request->data['Client']['Address'];
		$ad = array();
		foreach($address as $add){
			$ad[$add['id']] = $add['full_address'];
		}
		
		$this->set('options',$ad);
		
		$dispatch_loc = $this->Dispatch->find('list',array('fields' => array('Dispatch.id','Dispatch.location')));
		$this->set('dispatches',$dispatch_loc);	

	}

	/**
   	 * Name: ajaxeditsubmit
   	 * Use: ajax submit of edit order
	*/
	function ajaxeditsubmit(){
		$this->autoRender = false;	
		$this->loadmodel('Orderitem');

		if(!empty($this->request->data)) {  
			if($this->Order->validates($this->request->data)) {	
				$logged_user = $this->Auth->user('id');
				$Activities = new ActivitiesController;
				$id = $this->request->data['Order']['id'];
				$this->old = $this->Order->find('first', array(
		 											'conditions' => array('Order.id' => $id ),
													'recursive' => 2));				
				$this->request->data['Order']['modifiedby'] = $logged_user;
				$order = $this->request->data['Order'];
				$date = $this->request->data['Order']['orderdate'];
				$order['orderdate'] = date('Y-m-d', strtotime(str_replace('/', '-', $date)));
				$this->Order->save($order);

	            $item = $this->request->data['Order']['item'];
	            $del_ids = $this->request->data['removal_id'];
	            if(isset($del_ids) && !empty($del_ids)){
	            	$del=array($del_ids);
		            $condition = array('Orderitem.id IN ('.$del_ids.')');
		            $this->Orderitem->deleteAll($condition,false);
		        }
		        $result = array_diff_assoc($this->old['Order'],$this->request->data['Order']);/*Get only changed fields*/
		        $totalamount = '';
	           	foreach($item as $items){	
	           		if(isset($items['id'])){
						$this->Orderitem->id = $items['id'];
						$items['amount'] = $items['qty'] * $items['price'];
		           		$totalamount+=$items['amount'];					
						$this->Orderitem->save($items);           		 	
	           		}
	           		else{
	           			$items['orderid'] = $id;
						$items['amount'] = $items['qty'] * $items['price'];
		           		$totalamount+=$items['amount'];           			
	           			$this->Orderitem->saveall($items);
	           		}           	 
	            }
	       	    if($this->Order->id){
	           		//$pinvoice['total'] = $totalamount;
	           		//$this->Proformaitem->save($pinvoice);
	           		$this->Order->saveField('total', $totalamount);
	           	}  

	           	$Order['item'] = $item;/*updated value so we pass it into activity log*/
	           	$Order['Orderitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Order','Edit',$result);/*Add Data in Log*/

	           	$msg = 'Order has been Updated successfully';      
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));            
		        //$this->redirect('index');	
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);		                   
			}
			else{
                $Order = $this->Order->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Order could not be saved. Please, try again.';
                $response['data'] = compact('Order');
                echo json_encode($response);	        	
	        }   	
		}		
	}

	/**
   	 * Name: View
   	 * Use: View all Details of Order
     * @param int $id id of record
     * @return detail record of id   	 
	 */
	function view($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		/*Fetch data of particular ID*/
		$orders = $this->Order->find('first', array(
   			'conditions' => array('Order.id' => $id ), 
    		'recursive' => 2
		));	
		/*Redirect if ID not exist in database*/
		if(empty($orders)){
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$this->set('data',$orders);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'orders/">Orders</a><i class="fa fa-circle"></i></li>');		
		$this->set('pageTitle',' Order View');
	}

/**
   	 * Name: Print
   	 * Use: View all Details of Print Order
     * @param int $id id of record
     * @return detail record of id   	 
	 */
	function prints($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		/*Fetch data of particular ID*/
		$orders = $this->Order->find('first', array(
   			'conditions' => array('Order.id' => $id ), 
    		'recursive' => 2
		));		
		/*Redirect if ID not exist in database*/
		if(empty($orders)){
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$this->set('data',$orders);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'orders/">orders</a><i class="fa fa-circle"></i></li>');		
		$this->set('pageTitle',' Order');
	}

	/**
   	 * Name: generatepdf
   	 * Use: generate pdf file of record
     * @param int $id id of record
     * @return pdf file   	 
	*/
	public function generatepdf($id = null) {
	
		$this->pdfConfig = array(
			'filename' => 'invoice',
			'download' => $this->request->query('download')
		);
		$data = $this->Order->find('first',array('conditions'=> array('Order.id'=> 
			$id),'recursive' => 2));
		$this->set('data', $data);

		/* Make sure the controller doesn't auto render. */
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 		
		$view->viewPath = 'Orders';
		ini_set('memory_limit', '1024M');
		$html = $view->render('generatepdf');
   		$this->set('post',$html);

	}
	/**
		Name : getManualID
		Use : Get Unique Order Id in Order for Import
	*/
	function getManualID(){
		$manual_id = $this->Order->find('all',array('conditions'=> array('ismanual' => 1),'limit' => 1,'order' => 'Order.id DESC'));
		$this->request->data['Order']['orderdate'] = date('d/m/Y');
		if(!empty($manual_id)){
			$pro_id = $manual_id[0]['Order']['orderno'];
			$exp_id = explode('/',$pro_id);
			$manual_id = $exp_id[2] + 1;
			$year = date("Y");
			$manual = 'SHO/OL/'.$manual_id.'/'.$this->getFinancialyear();
			return $manual;
		}
		else{
			$year = date("Y");
			$manual = 'SHO/OL/'.'1/'.$this->getFinancialyear();
			return $manual;
		}
	}
	 /**
   	 * Name: getClientID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getClientID($value) {
		$this->loadmodel('Client');
		$clientdata = $this->Client->find('first', array(
   			'conditions' => array('company_name' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($clientdata)){
			return $clientdata['Client']['id'];
		}
		else{
			return;
		}
	}

	 /**
   	 * Name: getStandardID
   	 * Use: get id of data, it will be use for import order
     * @param string $value name of record
     * @return int id of record
     */
	function getStandardID($value) {
		$this->loadmodel('Standard');
		$std = $this->Standard->find('first', array(
   			'conditions' => array('standard_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($std)){
			return $std['Standard']['id'];
		}
		else{
			return;
		}		
	}	

	/**
   	 * Name: getGradeID
   	 * Use: get id of data, it will be use for import order
     * @param string $value name of grade
     * @param int $standard_id id of standard
     * @return int id of record
     */
	function getGradeID($value,$standard_id) {
		$this->loadmodel('Grade');
		$grd = $this->Grade->find('first', array(
   			'conditions' => array('grade_name' => $value,'standard_id'=>$standard_id),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($grd)){
			return $grd['Grade']['id'];
		}
		else{
			return;
		}		
	}		 


	 /**
   	 * Name: getBankID
   	 * Use: get id of data, it will be use for import order
     * @param string $value name of record
     * @return int id of record
     */
	function getBankID($value) {
		$this->loadmodel('Bank');
		$bank = $this->Bank->find('first', array(
   			'conditions' => array('bank_alias' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));		
		if(!empty($bank)){
			return $bank['Bank']['id'];
		}
		else{
			return;
		}		
	}

	/**
   	 * Name: getSizeTableID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getSizeTableID($gdmm) {
		$this->loadmodel('Size');
		$log_id = $this->Auth->user('id');
		$size = $this->Size->find('first', array(
   			'conditions' => array('gdmm' => $gdmm),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($size)){
			return $size['Size']['id'];	
		}
	}	
	/**
   	 * Name: getSizeID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getSizeID($gdmm,$gdnb) {
		$this->loadmodel('Size');
		$log_id = $this->Auth->user('id');
		$size = $this->Size->find('first', array(
   			'conditions' => array('gdmm' => $gdmm,'gdnb' => $gdnb),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($size)){
			return $size['Size']['id'];	
		}
		else{
			$exp_gdmm = explode(' ',$gdmm);
			$datarow['Size']['od_mm'] = $exp_gdmm[0];
			$datarow['Size']['wt_mm'] = $exp_gdmm[3];
			$exp_gdnb = explode(' ',$gdnb);
			$datarow['Size']['od_nb'] = $exp_gdnb[0];
			$datarow['Size']['wt_nb'] = $exp_gdnb[2];	
			$subtract_ratio = $exp_gdmm[0] - $exp_gdmm[3];
			$mult_ratio = $subtract_ratio * $exp_gdmm[3];
			$ratio = $mult_ratio * 0.0248;
			$datarow['Size']['calc_ratio'] = $mult_ratio * 0.0248;
			$datarow['Size']['gdmm'] = $gdmm;
			$datarow['Size']['gdnb'] = $gdnb;
			$datarow['Size']['createdby'] = $log_id;
			$date = date('Y-m-d H:i:s');
			$this->Size->query("Insert into sr_sizes (od_nb,wt_nb,od_mm,wt_mm,calc_ratio,gdmm,gdnb,createdby,modifiedby,created,modified)values('".$exp_gdnb[0]."','".$exp_gdnb[2]."','".$exp_gdmm[0]."','".$exp_gdmm[3]."','".$ratio."','".$gdmm."','".$gdnb."','".$log_id."','".$log_id."','".$date."','".$date."')");
			//$this->Size->create();
			$lastCreated = $this->Size->find('first', array('order' => array('Size.id' =>'desc'),'fields'=>array('Size.id')));

			//$this->Size->save($datarow['Size']);	
			$last_sizeid = $lastCreated['Size']['id'];
			return $last_sizeid;
		}		
	}		

	/**
   	 * Name: getUserID
   	 * Use: get id of data, it will be use for import order
     * @param string $value name of record
     * @return int id of record
     */
	function getUserID($value) {
		$this->loadmodel('User');
		$userdata = $this->User->find('first', array(
   			'conditions' => array('email' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($userdata)){
			return $userdata['User']['id'];
		}
		else{
			return;
		}
	}

	/**
   	 * Name: getDispatchID
   	 * Use: get id of dispatch location as well as its last code, it will be use for import order
     * @param string $value name of record
     * @return get id of dispatch location as well as its last code
     */
	function getDispatchID($value){
		$this->loadmodel('Dispatch');

		$this->loadmodel('Proformainvoice');
		$dispatch = $this->Dispatch->find('first', array(
   			'conditions' => array('location' => $value),
   			'fields' => array('id','code'), 
    		'recursive' => -1
		));
		if(!empty($dispatch)){		
			$cntlocation = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $dispatch['Dispatch']['id'])));
			$count = sizeof($cntlocation);
			if($count >= 1){
				++$count;
				$location = $dispatch['Dispatch']['code'].'/'.$count;
				$dis_id = $dispatch['Dispatch']['id'];
				return  array($location, $dis_id);
			}else{
				$location = $dispatch['Dispatch']['code'].'/1';
				$dis_id = $dispatch['Dispatch']['id'];
				return array($location, $dis_id);
			}
		}	
		if(empty($dispatch)){
			return;
		}	
	}

	/*
	* Name : getProductCat
	* Use : get product category id
	*
	*/
	function getProductCat($value,$stdvalue){
		$this->loadmodel('Productcategory');
		$this->loadmodel('Standard');
		
		$productcat = $this->Productcategory->find('first', array(
   			'conditions' => array('name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($productcat)){
			$productcatid = $productcat['Productcategory']['id'];	
		}else{
			return ;	
		}
		
		$std = $this->Standard->find('first', array(
   			'conditions' => array('standard_name' => $stdvalue),
   			'fields' => array('procatid'), 
    		'recursive' => -1
		));
		if(!empty($std)){
			$product = explode(",",$std['Standard']['procatid']);
	   			if (in_array($productcatid, $product)) {
					return $productcatid;
		  		}else{
		  			return;
		  		} 
		 }else{
		 	return;
		 }
	}

	/**
	* Name : getPriceId
	* Use  : get price Symbol in csv
	*/
	function getPriceId($val){
		$this->loadmodel('Price');
		$pr_id = $this->Price->find('first',array('conditions'=>array('Price.fullform' => $val),'fields' =>array('Price.id')));
		if(!empty($pr_id)){
			return $pr_id['Price']['id'];
		}else{
			return ;
		}
	}

	/**
	* Name : changeitemstatus
	* Use : change order item status Cancel Order
    * @param int $orderitemid id of record
    * @param int $status_id status_id of record
    * @return void
	*/
	function changeitemstatus($orderitem_id,$status_id){
		$this->autoRender = false ;
		$this->loadmodel('Orderitem');
		$this->Orderitem->id = $orderitem_id;
		$this->Orderitem->save(array('status'=>$status_id,'modifiedby'=>$this->Auth->user('id')));
		/*Fetch Orderid of order*/
		$orders = $this->Orderitem->find('first',array('conditions'=>array('Orderitem.id'=>$orderitem_id),'fields'=>array('orderid')));
		/*Fetch Orderitem which has status 1 (Process)*/
		$processorders = $this->Orderitem->find('all',array('conditions'=>array('Orderitem.orderid'=>$orders['Orderitem']['orderid'],'Orderitem.status'=>1)));
		if(empty($processorders)){
			/*If not a single item in process state than check whether any item in pending state or not*/
		    $pendingorders = $this->Orderitem->find('all',array('conditions'=>array('Orderitem.orderid'=>$orders['Orderitem']['orderid'],'Orderitem.status'=>0)));
			if(empty($pendingorders)){ /*If not a singlr orderitem in pending than update order status as closed (status = 2)*/
				$this->Order->save(array('id'=>$orders['Orderitem']['orderid'],'status'=>2,'modifiedby'=>$this->Auth->user('id')));
			}
		}
		return;
	}

	/**
	* Name : changeorderstatus
	* Use : change order status
    * @param int $orderid id of record
    * @param int $status_id status_id of record
    * @return void
	*/
	function changeorderstatus($orderid,$status_id){
		$this->autoRender = false ;
		$this->Order->id = $orderid;
		$this->Order->save(array('status'=>$status_id,'modifiedby'=>$this->Auth->user('id')));
		/*If status_id = 3,means order cancelled than its item will be also cancelled*/
		if($status_id == 3){
			$this->loadmodel('Orderitem');
			$this->Orderitem->saveall(array('orderid'=>$orderid,'status'=>$status_id,'modifiedby'=>$this->Auth->user('id')));		
		}
		if($status_id == 2){
			$this->loadmodel('Orderitem');
			$this->Orderitem->save(array('orderid'=>$orderid,'status'=>$status_id,'modifiedby'=>$this->Auth->user('id')));		
		}		
		/*If status_id = 2,means order closed than its pending item will be also closed*/
		return;
	}

	/**
	* Name : checkorderstatus
	* Use : check order item status and as per its status, change order status
    * @param int $orderid id of record
    * @return void
	*/
	function checkorderstatus($orderid,$modifiedby){
		$this->autoRender = false ;
		$this->loadmodel('Orderitem');
		/*Fetch Orderitem which has status 1 (Process)*/
		//$processorders = $this->Orderitem->find('all',array('conditions'=>array('Orderitem.orderid'=>$orderid,'Orderitem.status'=>1)));
		$processorders = $this->Orderitem->find('all',array('conditions'=>array('Orderitem.orderid'=>$orderid,'Orderitem.status !='=>0)));
		if(!empty($processorders)){ /*If single orderitem in process than update order status as process (status = 1)*/
			$this->Order->save(array('id'=>$orderid,'status'=>1,'modifiedby'=>$modifiedby));
		}else{ /*If not a single item in process state than check whether any item in pending state or not*/
		  $pendingorders = $this->Orderitem->find('all',array('conditions'=>array('Orderitem.orderid'=>$orderid,'Orderitem.status'=>0)));
			if(empty($pendingorders)){ /*If not a singlr orderitem in pending than update order status as closed (status = 2)*/
				$this->Order->save(array('id'=>$orderid,'status'=>2,'modifiedby'=>$modifiedby));
			}			
		}
	}
  
  /**
	* Name : delete
	* Use : delete the order
	* @param int $orderid id of record	
	*/

	function delete($id = null)
	{
		$this->loadmodel('Orderitem');
		$id = base64_decode($id);		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Order->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Order has been Deleted successfully</div>'));
		}
		$this->Orderitem->deleteAll(array('Orderitem.orderid' => $id), true);
		return $this->redirect(array('action' => 'index'));
	}

	
	/*
	*
	* Name : export
	* Use : Export the Order data
	*/
	function export($id = null) {
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

		$results = $this->Order->find('first',array('conditions'=>array('Order.id' => $id),'recursive'=>2));

		if($results['Price']['sign'] == '&#8377;'){
			$sym = '₹';
		}else if($results['Price']['sign'] == '&#36'){
			$sym = '$';
		}else if($results['Price']['sign'] == '&#8364;'){
			$sym = '€'; 
		}else {
			$sym = '£';
		}

		$reportname = 'Order-'.$results['Order']['orderno'];
		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));		
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

			$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

			$setting= $this->Session->read('setting_data');
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'img/print_logo.png';
			$objDrawing->setPath($path);
			$objDrawing->setCoordinates('A1');
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "CIN NO : ". $setting["Setting"]["cin_no"]."\n"."Office: 24, 2nd Floor, Dayabhai Building"."\n"."Gulalwadi, Mumbai - 400004"."\n". "Maharastra, (INDIA)."."\n"."Tel: + 91-22-40416214/ 40416221/ 25"."\n". "Fax: +91-22-23452161"."\n"."Email: info@shalco.in Web: www.shalco.in");
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F1:J6');
			$objPHPExcel->getActiveSheet()->getStyle('F1:J6')->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('F1:F6')->applyFromArray($hori_left);
			$objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E6');

			$dyn_cell = 7;
			$for_next_cell = $dyn_cell + 3;
			$cl_address = str_replace('<br/>', "\n", searchFromAddress($results['Order']['billaddress_id'], $results['Client']['Address']));
			$shp_address = str_replace('<br/>', "\n ", searchFromAddress($results['Order']['shipaddress_id'], $results['Client']['Address']));
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,'Sales Order')->mergeCells('A'.$dyn_cell.':J'.$dyn_cell);

			$styleArray = array(
		      'font'  => array(
		        'bold'  => true,
		        'size'  => 12,
		        'name'  => 'Calibri'
		       )
			);


			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($styleArray);
	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'Office Address');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.++$for_next_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $setting["Setting"]["office_address"])->mergeCells('C'.$dyn_cell.':E'.$for_next_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':B'.$for_next_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':E'.$for_next_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'Order NO.');
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, 'Date')->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, $results['Order']['orderno']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Order']['orderdate'])))->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);

			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);

			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:B5')->applyFromArray($style);			
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, "Shiping Terms");
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $this->requestAction('App/get_delivery_type/'.$results['Order']['delivery_type']).' '.$results['Order']['tod'])->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$dyn_cell, 'Factory Address');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$new_cell = $dyn_cell + 2;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.++$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':B'.$new_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $setting["Setting"]["factory_address"])->mergeCells('C'.$dyn_cell.':E'.$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, "Terms Of Payment");
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $results['Order']['top'])->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, "Bill To")->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			//$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, "Ship To")->mergeCells('F'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.++$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Client']['company_name'])->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $results['Order']['ship_to'])->mergeCells('F'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $cl_address);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $shp_address);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getAlignment()->setWrapText(true);
			$new_row = $dyn_cell + 2;

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':E'.++$new_row);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$dyn_cell.':J'.$new_row);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$new_row)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_row.':J'.$new_row)->applyFromArray($border_bottom);


			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'13', 'Client Po: - '.$results['Order']['clientpo']);
			$headerRow = "Sr"."\n"."No.,Product Name,Hscode,Standard,Grade,Good Description,,QTY,Unit Price"."\n"."(".$sym."),Amount"."\n"."(.".$results['Price']['sign'].")";
			//echo $dyn_cell;exit;
			$k = ++$new_row;

			$objPHPExcel->getActiveSheet()->getStyle($k)->applyFromArray($style);
				$objPHPExcel->getActiveSheet()->getStyle($k)->getFont()->setBold(true);

				$maxSize = explode(",", $headerRow);
				for($i=0,$j='A';$i<sizeof($maxSize);$i++,$j++) {

					if($maxSize[$i] == 'Good Description'){
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$k.':'.'G'.$k);
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}
					elseif($maxSize[$i] == 'Sr No.'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(8);
					}
					elseif($maxSize[$i] == 'Product Name'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}else{
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(12);
					}
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$k, $maxSize[$i]);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k, $maxSize[$i])->applyFromArray($border_left);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(true);
				}
			$dataArray = array();
			$idx = '1';
			$index = '1';
			foreach($results['Orderitem'] as $result) {
				$dataArray[$index]['0'] = $idx;
					$dataArray[$index]['1'] = $result["Productcategory"]['productname'];
					$dataArray[$index]['2'] = $result["Productcategory"]["Hscode"]["hscode"];
					$dataArray[$index]['3'] = $result['Productcategory']['Standard']['standard_name'];
					$dataArray[$index]['4'] = $result['Productcategory']['Grade']['grade_name'];
					$dataArray[$index]['5'] = setGDmmformat($result['Productcategory']['Size']['gdmm'],$result['length']);
					$dataArray[$index]['6'] = setGDnbformat($result['Productcategory']['Size']['gdnb'],$result['length']);
					$dataArray[$index]['7'] = $result['qty'].' '.$result['Productcategory']['Producttaxonomy']['unit'];;
					$dataArray[$index]['8'] = $result['price'];
					$dataArray[$index]['9'] = $result['amount'];
					$idx++;
					$index++;
			}
			$id = ++$new_row;
			$size = count($dataArray);

			foreach ($dataArray as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_right);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_bottom);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, $value[$i]);
						
						/*Start logic for add decimal position because excel removed automatically if value has empty zero like its show 5.00 as 5*/
						$ex = explode('.', $value[$i]);
						if(isset($ex[1])){
							if(is_numeric($ex[1])){
								$zero = '';
								if(strlen($ex[1]) == 2){
									$zero = '0.00';
								}
								if(strlen($ex[1]) == 3){
									$zero = '0.000';
								}
								$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left)->getNumberFormat()->setFormatCode($zero);	
							}					
						}
						
						/*End logic of add decimal position with zeros*/

						/*PHP_Excel default set numberic value to align right, while string value to align left*/
						/*Set quntity align right*/
						if($i == 0){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						}
						if($i == 7){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
						}	
					}
					else {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, '');
					}
				}
				$id++;
			} 
			$nxt_val = $size + $new_row;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Total Amount '.$results['Price']['fullform'].'('.$sym.')');

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom_thick);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$nxt_val,$results['Order']['total']);
			/* Extra Fields */

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Quantity Tolerance')->mergeCells('A'.$nxt_val.':B'.$nxt_val);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Order']['quantity'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Delivery Period ')->mergeCells('A'.$nxt_val.':B'.$nxt_val);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Order']['delivery_period'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Dimensions')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Order']['dimensions'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Certification ')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Order']['certifications'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Comment  ')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Order']['comment'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			//$objPHPExcel->getActiveSheet()->getStyle('F3:F11')->applyFromArray($border_left);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
				//$objPHPExcel->getActiveSheet()->getStyle('A8:I8')->applyFromArray($border_top);

			/* image */

			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'uploads/signature/logo1.png';
			//echo $path;exit;0110
			$objDrawing->setPath($path);-*/
			/* Bank Details */
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.++$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()
			    ->getStyle('G'.$nxt_val)
			    ->getNumberFormat()
			    ->setFormatCode(
			        PHPExcel_Style_NumberFormat::FORMAT_TEXT
			    );
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Bank Details');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($styleArray);		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':J'.++$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle($nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Bank Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['branch_name'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'For, SHALCO IND. PVT. LTD.	');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Swift Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['swift_code'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'DIRECTOR');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			/*$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objDrawing->setCoordinates('G'.$nxt_val);*/
			//$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'DIRECTOR')->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'AD Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ad_code'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Beneficiary’s A/C No');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ac_no'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Beneficiary’s Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ac_name'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'IEC Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results["Bank"]["iec_code"])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'IFC Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results["Bank"]["ifc_code"])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			

			$total = $size + 12 + 13;

			$objPHPExcel->getActiveSheet()->getStyle('A7:J'.$nxt_val)->applyFromArray($border_style);
				//$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$nxt_val)->getFont()->setSize(9);
		
			ob_clean();
			$filename = $reportname.".xlsx";
			/*header('Content-Type: application/vnd.ms-excel');*/
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			$objWriter->save('php://output');
			exit();
	}
	/**
   	 * Name: countorder
   	 * Use: count no of order at dispatch location
     * @param array $this->request->params['conditional_parameter'] condition 
     * @return int number of record   	 
	*/
	function countorder() {
		if(isset($this->request->params['conditional_parameter'])){
			$count_orders = $this->Order->find('count', array(
	        	'conditions' => $this->request->params['conditional_parameter']));
			return $count_orders;
		}
		else{
			return;
		}		
	}
	/* 
	Name : Print 
	Use  : Ms word documrnt for dynamic data 
	*/
	public function printoword($id = null){
		$file = new File(APP.DS.'webroot'.DS.'css'.DS.'custom.css', false); //1
    	$this->set('inlineCss',$file->read()); //2
    	$file->close(); 
    	$bootstrap = new File(APP.DS.'webroot'.DS.'global/plugins/bootstrap/css'.DS.'bootstrap.min.css', false);
    	$this->set('bootstrap',$bootstrap->read()); //2
    	$file->close(); 
    	$component = new File(APP.DS.'webroot'.DS.'global/css'.DS.'components.min.css', false);
    	$this->set('component',$component->read()); //2
    	$file->close(); 
		$this->layout = 'msword';
		$id = base64_decode($id);
		$proforma = $this->Order->find('first', array(
   			'conditions' => array('Order.id' => $id ), // URL to fetch the required page
    		'recursive' => 2
		));		
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$proforma);
		Configure::write('debug',0);
		/*Redirect if ID empty */
		
		/*Fetch data of particular ID*/
		/*Redirect if ID not exist in database*/
		
		/*$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$proforma);
		$this->set('pageTitle',' Proforma Invoice View');
	    */
	    //Configure::write('debug',0);
	}
	/*
	Name : uploadpo
	Use : Upload Client po
	*/
	function uploadpo(){
		$this->autoRender = false;

		if(empty($_POST['id'])) {
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		if(!empty($_FILES['file']['name'])){
                  move_uploaded_file($_FILES['file']['tmp_name'],'uploads/clientpo/'.$_FILES['file']['name']);
                  $po_upload = $_POST['caption_avatar'];
              }else {
              	$po_upload = '';
              }
   		$po_upload_update  = array('id' => $_POST['id'],'po_upload'=>$po_upload);
	   	$this->Order->save($po_upload_update);
	   	return 1;
	}

	function download(){
		$this->autoRender = false;

		$this->loadmodel('Client');
		$ex_client[0] = '';		
		$clients = $this->Client->find('all',array('fields'=>array('Client.company_name'), 'order'=>'company_name asc','recursive'=>0));
		foreach($clients as $clnt){
			$ex_client[] = $clnt['Client']['company_name'];
		}

		$this->loadmodel('Productcategory');
		$procat_array[0] = '';
		$procat = $this->Productcategory->find('all',array('fields'=>array('name'), 'order'=>'name asc','recursive'=>0));
		foreach($procat as $procatdata){
			$procat_array[] = $procatdata['Productcategory']['name'];
		}

		$this->loadmodel('Standard');
		$std_array[0] = '';
		$standard = $this->Standard->find('all',array('fields'=>array('standard_name'),'order'=>'standard_name asc','recursive'=>0));
		foreach($standard as $standard){
			$std_array[] = $standard['Standard']['standard_name'];
		}

		$this->loadmodel('Grade');
		$grd_array[0] = '';
		$grade = $this->Grade->find('all',array('fields'=>array('grade_name'),'order'=>'grade_name asc','recursive'=>0));
		foreach($grade as $grade){
			$grd_array[] = $grade['Grade']['grade_name'];
		}

		$this->loadmodel('Size');
		$gdmm_array[0] = '';
		$gdnb_array[0] = '';
		$size = $this->Size->find('all',array('fields'=>array('gdmm','gdnb'),'order'=>'gdmm asc','recursive'=>0));
		foreach($size as $size){
			$gdmm_array[] = $size['Size']['gdmm'];
			$gdnb_array[] = $size['Size']['gdnb'];
		}

		$this->loadmodel('Bank');
		$bank_array[0] = '';
		$bank = $this->Bank->find('all',array('fields'=>array('bank_alias'),'order'=>'bank_alias asc','recursive'=>0));
		foreach($bank as $bank){
			$bank_array[] = $bank['Bank']['bank_alias'];
		}

		$this->loadmodel('User');
		$user_array[0] = '';
		$user = $this->User->find('all',array('fields'=>array('email'),'order'=>'email asc','recursive'=>0));
		foreach($user as $user){
			$user_array[] = $user['User']['email'];
		}

		$this->loadmodel('Dispatch');
		$dis_array[0] = '';
		$dispatch = $this->Dispatch->find('all',array('fields'=>array('location'),'order'=>'location asc','recursive'=>0));
		foreach($dispatch as $dis){
			$dis_array[] = $dis['Dispatch']['location'];
		}

		$this->loadmodel('Price');
		$prc_array[0] = '';
		$price = $this->Price->find('all',array('fields'=>array('fullform'),'order'=>'fullform asc','recursive'=>0));
		foreach($price as $prc){
			$prc_array[] = $prc['Price']['fullform'];
		}

		$fileName = WWW_ROOT.'test1.xlsx';
		/** automatically detect the correct reader to load for this file type */
		$objReader = PHPExcel_IOFactory::createReaderForFile($fileName);
		$objPHPExcel = $objReader->load($fileName);
		$loadedSheetNames = $objPHPExcel->getSheetNames();

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->setTitle('ClientSheet');
		for($i=1,$j='A';$i<sizeof($ex_client);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $ex_client[$i]);
		}		

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(2);
		$objPHPExcel->getActiveSheet()->setTitle('ProcatSheet');
		for($i=1,$j='A';$i<sizeof($procat_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $procat_array[$i]);
		}

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(3);
		$objPHPExcel->getActiveSheet()->setTitle('Standard'); 
		for($i=1,$j='A';$i<sizeof($std_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $std_array[$i]);
		}

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(4);
		$objPHPExcel->getActiveSheet()->setTitle('Grade'); 
		for($i=1,$j='A';$i<sizeof($grd_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $grd_array[$i]);
		}
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(5);
		$objPHPExcel->getActiveSheet()->setTitle('Size'); 
		for($i=1,$j='A';$i<sizeof($gdmm_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $gdmm_array[$i]);
		}

		for($i=1,$j='C';$i<sizeof($bank_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $bank_array[$i]);
		}
		for($i=1,$j='D';$i<sizeof($user_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $user_array[$i]);
		}
		for($i=1,$j='E';$i<sizeof($dis_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $dis_array[$i]);
		}
		for($i=1,$j='F';$i<sizeof($prc_array);$i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.$i, $prc_array[$i]);
		}
		/*Start to fill all other sheet data into main sheet*/ 
		/*Set Client data into mainsheet*/
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('B2')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('ClientSheet!A1:A'.sizeof($ex_client).$objPHPExcel->getActiveSheet(1)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set ship data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('C2')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('ClientSheet!A1:A'.sizeof($ex_client).$objPHPExcel->getActiveSheet(1)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set delivery type data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('D2')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('"CIF,FOB,Ex-work,CNF"');
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set Bank data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('G2')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('Size!C1:C'.(sizeof($bank_array) - 1));
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set approve by user data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('H2')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('Size!D1:D'.(sizeof($user_array)-1));
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set dispatch location data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('A5')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('Size!E1:E'.(sizeof($dis_array)-1));
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set price data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('B5')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('Size!F1:F'.(sizeof($prc_array)-1));
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set product category data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('C8')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('=\'ProcatSheet\'!$A$1:$A'.sizeof($procat_array).$objPHPExcel->getActiveSheet(2)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set standard data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('D8')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('=\'Standard\'!$A$1:$A'.sizeof($std_array).$objPHPExcel->getActiveSheet(3)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		/*Set Grade data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('E8')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('=\'Grade\'!$A$1:$A'.sizeof($grd_array).$objPHPExcel->getActiveSheet(4)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');
		
		/*Set size (GD MM) data into mainsheet*/
		$objValidation = $objPHPExcel->getActiveSheet(0)->getCell('F8')->getDataValidation();
		$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
		$objValidation->setFormula1('=\'Size\'!$A$1:$A'.sizeof($gdmm_array).$objPHPExcel->getActiveSheet(5)->getHighestRow());
		$objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
		$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
		$objValidation->setAllowBlank(false);
		$objValidation->setShowInputMessage(true);
		$objValidation->setShowErrorMessage(true);
		$objValidation->setShowDropDown(true);
		$objValidation->setPromptTitle('Pick from list');
		$objValidation->setPrompt('Please pick a value from the drop-down list.');
		$objValidation->setErrorTitle('Input error');
		$objValidation->setError('Value is not in list');

		$objPHPExcel->getSheetByName('ClientSheet')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
		$objPHPExcel->getSheetByName('ProcatSheet')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
		$objPHPExcel->getSheetByName('Standard')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
		$objPHPExcel->getSheetByName('Grade')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
		$objPHPExcel->getSheetByName('Size')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);

		ob_clean();
		$filename = "sample-file.xlsx";
		/*header('Content-Type: application/vnd.ms-excel');*/
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename='.$filename);
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit();

	}
	/**
   	 * Name: getShipAddID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getShipAddID($value) {
		$this->loadmodel('Address');
		$address = $this->Address->find('first', array(
   			'conditions' => array('clientid' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));		
		if(!empty($address)){
			return $address['Address']['id'];
		}
		else{
			return 0;
		}		
	}
}	