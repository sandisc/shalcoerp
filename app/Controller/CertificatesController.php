<?php

/*
 * @Controller name: Certificate Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Certificate management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file' => 'dompdf/dompdf_config.inc.php'));
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));

class CertificatesController extends AppController {

    var $helpers = array('Html', 'Form', 'Csv');
    var $components = array('Auth', 'RequestHandler', 'Session', 'Email');
    var $uses = array('Certificate', 'Certificateitem', 'User');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->set('mainTitle', 'Test Certificate Management');
    }

    /*
     * Name: index
     * Use: certificate listing.
     */

    function index() {
        $this->set('pageTitle', ' Test Certificate List');
        $this->set('ajaxaction', 'ajaxlisting');
    }

    function ajaxlisting() {
        $column = array('id', 'certificateno', 'std_spec', 'grd_spec', 'Certificate.modified', 'User.first_name', 'Certificate.isverified');
        $order = array('Certificate.modified' => 'desc');

        $res = $this->datatable_append($order, $column);
        $this->Certificate->unbindModel(array('hasMany' => array('Certificateitem')));
        $count = $this->Certificate->find('count', array('conditions' => array($res['con']), 'order' => $res['order_by']));
        $this->Certificate->unbindModel(array('hasMany' => array('Certificateitem')));
        $certi = $this->Certificate->find('all', array('conditions' => array($res['con']), 'limit' => $_POST['length'], 'offset' => $_POST['start'], 'order' => $res['order_by']));
        $this->set('certi', $certi);
        $this->set('count', $count);
    }

    /**
     * Name: generatetc
     * Use: Generate Test Cerficate based on Order
     * @param int $orderid orderid of record
     * @param int $reftype reference type if invoice than 0, chalan than 1
     * @param int $invoice_chalan_id invoice id or chalan id of record for which you want to generate certificate
     */
    function generatetc($reftype, $orderid,$invoice_chalan_id) {
        $oid = base64_decode($orderid);
        $inv_cha_id = base64_decode($invoice_chalan_id);
        $this->loadModel('Invoice');
        $this->loadModel('Invoiceitem');
        $this->loadModel('Chalan');
        $this->loadModel('Chalanitem');
        $this->loadModel('Order');
        $this->loadModel('Orderitem');
        $this->loadModel('Certificateitem');

        $this->Order->unbindModel(array('hasMany' => array('Orderitem'), 'belongsTo' => array('Dispatch', 'Price', 'Bank', 'User')));
        $this->Orderitem->unbindModel(array('belongsTo' => 'Size'));
        if ($reftype == 0) {
            $this->Invoiceitem->unbindModel(array('belongsTo' => array('Licence')));
            $this->Invoiceitem->bindModel(array('belongsTo' => array('Orderitem' => array(
                        'className' => 'Orderitem',
                        'foreignKey' => 'orderitemid',
                        'conditions' => array('Orderitem.id = Invoiceitem.orderitemid'),
            ))));
            $order = $this->Invoice->find('first', array('conditions' => array('Order.id' => $oid,'Invoice.id'=>$inv_cha_id), 'recursive' => 3));
            $refid = $order['Invoice']['id'];
            $item = $order['Invoiceitem'];
        } else {
            $this->Chalanitem->bindModel(array('belongsTo' => array('Orderitem' => array(
                        'className' => 'Orderitem',
                        'foreignKey' => 'orderitemid',
                        'conditions' => array('Orderitem.id = Chalanitem.orderitemid'),
            ))));
            $order = $this->Chalan->find('first', array('conditions' => array('Order.id' => $oid,'Chalan.id'=>$inv_cha_id), 'recursive' => 3));
            $refid = $order['Chalan']['id'];
            $item = $order['Chalanitem'];
        }
        $logged_user = $this->Auth->user('id');

        /* Sorting Order based on product category */
        $productcat_filter = $this->_group_ty($item, 'procatid');
        $certificateid = '';
        foreach ($productcat_filter as $stdkey => $stdvalue) {
            /* Sorting Order based on standard */
            $standard_filter = $this->_group_ty($stdvalue, 'standard_id');
            foreach ($standard_filter as $grdkey => $grdvalue) {
                /* Sorting Order based on grade */
                $grade_filter = $this->_group_ty($grdvalue, 'grade_id');
                $index = 0;
                foreach ($grade_filter as $key => $gradedata_filter) {
                    $itemcount = '5'; /* set max 5 order item for each test certificate */
                    foreach ($gradedata_filter as $key => $gradedata) {

                        if ($itemcount % 5 == 0) {
                            $cdata = array();
                            $cdata['reftype'] = $reftype;
                            $cdata['refid'] = $refid;
                            $cdata['grd_spec'] = $gradedata_filter['0']['Orderitem']['Productcategory']['Grade']['grade_name'];
                            $cdata['std_spec'] = $gradedata_filter['0']['Orderitem']['Productcategory']['Standard']['standard_name'];
                            $cdata['createdby'] = $logged_user;
                            $cdata['modifiedby'] = $logged_user;
                            $cdata['issuedate'] = date('Y-m-d');
                            $cdata['certificateno'] = $this->getCertificateNO('',$cdata['issuedate']);
                            $cdata['fy'] = $this->calculateFinancialYearForDate($cdata['issuedate']);
                            $cdata['remarks'] = "1)  SOLUTION ANNEALING AT MINIMUM TEMP. 1040° C WITH WATER QUENCHING 
2)  PICKLING & PASSIVATION CARRIED OUT BY M/s. SIPL - FOUND SATISFACTORY.
3)  100% PIPES PMI CHECKED BY M/s. SIPL-FOUND SATISFACTORY.
4)  HYDROTEST CARRIED OUT AT ABOVE MENTIONED PRESSURE FOR 100% QUANTITY  BY M/s. SIPL-FOUND SATISFACTORY.
5)  Length: 
6)  Microstructure consists of Austenite, ASTM Grain size No. 6-7, No carbide precipitation on grain boundaries – found satisfactory.
7)  Dimension as per ASME B 36.19 carried out and found satisfactory
8)  VISUAL AND DIMENSIONAL INSPECTION CARRIED OUT FOR 100% QUANTITY BY M/s. SIPL-FOUND SATISFACTORY.
9)  Mercury Free NACE MR0175. / MR0103
10) All Tubes polished gr. 240,
11) IGC PRACTICE `E` CARRIED OUT AS PER ASMT A 262 - NO  IGC FISSURES OR CRACKS OBSERVED AT 20X & 250X MAGNIFICATION FOUND STATISFACTORY.

We hereby certify that the product were manufactured and tested strictly according to specifications and customers order.";

                            $this->Certificate->saveall($cdata);
                            $certificateid = $this->Certificate->getInsertID();
                        }
                        $cdataitem = array();
                        $cdataitem['certificateid'] = $certificateid;
                        $cdataitem['refitemid'] = $gradedata['id'];
                        $cdataitem['heatno'] = $this->getHeatno($cdata['issuedate']);
                        $cdataitem['printheatno'] = $cdataitem['heatno'];
                        $this->Certificateitem->saveall($cdataitem);
                        $itemcount++;
                    }
                }
            }
        }
        $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>Certificate has been Updated successfully</div>'));
        $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
    }

    function _group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    function _group_ty($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val['Orderitem'][$key]][] = $val;
        }
        return $return;
    }

    /**
     * Name : getCertificateNO
     * Use : Get Unique certificate No for certificate
     */
    function getCertificateNO($type=null,$date=null){ 
        if($type == 'ajax'){ /*For on change event inside add and edit certificate form*/
            $this->autoRender=false;
        }
        /*Default current date*/
        if(empty($date)){$date = date('Y-m-d');}
        else{
            $date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
        }  
        /*If user fill date than override default date with user entered date*/
        if(isset($_POST['date'])){ 
            $date=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
        }

        $new_finan_year = $this->calculateFinancialYearForDate($date);
        $certiid = $this->Certificate->find('first', array('conditions'=>array('Certificate.fy' => $new_finan_year),'fields' => array('Certificate.certificateno', 'Certificate.fy'), 'recursive' => -1, 'order' => 'Certificate.id DESC'));
       
        if (!empty($certiid) && $certiid['Certificate']['fy'] == $new_finan_year) {
            $in_id = $certiid['Certificate']['certificateno'];
            $exp_id = explode('/', $in_id);
            $inv_id = $exp_id[1] + 1;
            $pr_id = 'SHTC/' . $inv_id . '/' . $this->calculateFinancialYearForDate($date);
            return $pr_id;
        } else {
            $pr_id = 'SHTC/' . '1/' . $this->calculateFinancialYearForDate($date);
            return $pr_id;
        }
    }

    /**
     * Name : getHeatno
     * Use : Get Unique heat No for certificate
     */
    function getHeatno($date=null) {

        $date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
        $new_finan_year = $this->calculateFinancialYearForDate($date);

        $current_certificate = $this->Certificate->find('first', array('conditions'=>array('Certificate.fy' => $new_finan_year),'recursive' => 2,'limit' => 1, 'order' => 'Certificate.id DESC')); 

        if(empty($current_certificate['Certificateitem'])){
            /*Find Previous record*/
            $prev_certificate = $this->Certificate->find('first', array('conditions'=>array('Certificate.fy' => $new_finan_year,'Certificate.id !=' => $current_certificate['Certificate']['id']),'recursive' => 2,'limit' => 1, 'order' => 'Certificate.id DESC'));

            if(!empty($prev_certificate)){
                end($prev_certificate['Certificateitem']);
                $lastIndex = key($prev_certificate['Certificateitem']); /*If particular certificate has many items than needs to select last item*/
                $in_id = $prev_certificate['Certificateitem'][$lastIndex]['heatno'];
                $exp_id = explode('/', $in_id);
                $cut_id = substr($exp_id[1], 4); // cut first four character from string.
                $inv_id = $exp_id[1] + 1;
                $pr_id = 'SHHN/' . $inv_id . '/' . $new_finan_year;
                return $pr_id;    
            }else{                
                $pr_id = 'SHHN/1/' . $new_finan_year;
                return $pr_id;            
            }
        }
        else{
            end($current_certificate['Certificateitem']);
            $lastIndex = key($current_certificate['Certificateitem']); /*If particular certificate has many items than needs to select last item*/
            $in_id = $current_certificate['Certificateitem'][$lastIndex]['heatno'];
            $exp_id = explode('/', $in_id);
            $cut_id = substr($exp_id[1], 4); // cut first four character from string.
            $inv_id = $exp_id[1] + 1;
            $pr_id = 'SHHN/' . $inv_id . '/' . $new_finan_year;
            return $pr_id;            
        }      
    }

    /**
     * Name: checkCertiGenerate
     * Use: check test certificate generate or not
     * @param int $refid invoice or chalan id of record
     * @param int $reftype reference type if invoice than 0, chalan than 1
     * @return true or false
     */
    function checkCertiGenerate($reftype, $refid) {
        if ($this->Certificate->find('count', array('conditions' => array('reftype' => $reftype, 'refid' => $refid))) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Name: viewtclist
     * Use: test certificate of order
     * @param int $orderid orderid of record
     */
    function viewtclist($orderid) {
        $id = base64_decode($orderid);
        if (empty($id)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }

        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'certificates/">Test Certificates</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'Test Certificate Of Order');

        $this->loadModel('Order');
        $this->loadModel('Grade');
        $this->loadModel('Standard');
        $this->loadModel('Orderitem');
        $this->loadModel('Client');

        $this->Grade->unbindModel(array('belongsTo' => array('Physical', 'Chemical', 'Mechanical', 'User', 'Standard')));
        $this->Standard->unbindModel(array('hasMany' => array('Grade'), 'belongsTo' => array('User')));
        $this->Client->unbindModel(array('belongsTo' => array('User', 'Coordinator')));
        $this->Orderitem->unbindModel(array('belongsTo' => array('Size')));
        $this->Order->unbindModel(array('belongsTo' => array('Dispatch', 'Price', 'Bank', 'User')));

        $this->Orderitem->bindModel(array('hasOne' => array('Certificateitem' => array(
                    'className' => 'Certificateitem',
                    'foreignKey' => 'orderitemid'
        ))));
        $this->Certificateitem->bindModel(array('belongsTo' => array('Certificate' => array(
                    'className' => 'Certificate',
                    'foreignKey' => 'certificateid',
        ))));
        $this->Certificateitem->unbindModel(array('belongsTo' => array('Orderitem')));

        $orders = $this->Order->find('first', array('conditions' => array('Order.id' => $id), 'recursive' => 3));
        $this->set('orders', $orders);
    }

    function getcertificatedetail($reftype, $id) {
        $this->loadModel('Order');
        $this->loadModel('Grade');
        $this->loadModel('Standard');
        $this->loadModel('Orderitem');
        $this->loadModel('Invoice');
        $this->loadModel('Invoiceitem');

        $this->loadModel('Chalan');
        $this->loadModel('Chalanitem');
        $this->loadModel('Certificateitem');
        $this->loadModel('Client');

        $this->Grade->unbindModel(array('belongsTo' => array('Physical', 'Chemical', 'Mechanical', 'User', 'Standard')));
        $this->Standard->unbindModel(array('hasMany' => array('Grade'), 'belongsTo' => array('User')));
        $this->Client->unbindModel(array('belongsTo' => array('User', 'Coordinator')));

        $this->Order->unbindModel(array('hasMany' => array('Orderitem'), 'belongsTo' => array('Dispatch', 'Price', 'Bank', 'User')));
        if ($reftype == 1) {
            $this->Chalan->unbindModel(array('hasMany' => array('Chalanitem')));

            $this->Chalanitem->bindModel(array('belongsTo' => array('Orderitem' => array('className' => 'Orderitem', 'foreignKey' => 'orderitemid', 'conditions' => 'Orderitem.id = Chalanitem.orderitemid'))));

            $this->Certificateitem->bindModel(array('belongsTo' => array('Chalanitem' => array('className' => 'Chalanitem', 'foreignKey' => 'refitemid', 'conditions' => 'Chalanitem.id = Certificateitem.refitemid'))));

            $this->Certificate->bindModel(array('belongsTo' => array('Chalan' => array('className' => 'Chalan', 'foreignKey' => 'refid', 'conditions' => 'Chalan.id = Certificate.refid'))));
        } else {
            $this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem')));

            $this->Invoiceitem->bindModel(array('belongsTo' => array('Orderitem' => array('className' => 'Orderitem', 'foreignKey' => 'orderitemid', 'conditions' => 'Orderitem.id = Invoiceitem.orderitemid'), 'Licence')));

            $this->Certificateitem->bindModel(array('belongsTo' => array('Invoiceitem' => array('className' => 'Invoiceitem', 'foreignKey' => 'refitemid', 'conditions' => 'Invoiceitem.id = Certificateitem.refitemid'))));

            $this->Certificate->bindModel(array('belongsTo' => array('Invoice' => array('className' => 'Invoice', 'foreignKey' => 'refid', 'conditions' => 'Invoice.id = Certificate.refid'))));
        }
        $certi = $this->Certificate->find('first', array('conditions' => array('Certificate.id' => $id), 'recursive' => 4));
        return $certi;
    }

    /**
     * Name: edit
     * Use: update certificate
     * @param int $id id of record
     * @return detail record of id	 
     */
    function edit($reftype, $id = null) {
        $id = base64_decode($id);
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }

        if (!empty($this->request->data)) {

            if ($this->Certificate->validates($this->request->data)) {
                $Activities = new ActivitiesController;

                $logged_user = $this->Auth->user('id');
                $this->old = $this->Certificate->find('first', array(
                                                    'conditions' => array('Certificate.id' => $id ),
                                                    'recursive' => 2));                
                $this->request->data['Certificate']['modifiedby'] = $logged_user;
                $certificates = $this->request->data['Certificate'];                
                $certificates['issuedate'] = date('Y-m-d',strtotime(str_replace('/', '-',  $this->request->data['Certificate']['issuedate'])));

                /*check old and new certificates number is same or not. if not than we will calling to get new certificates number again. Reason is to avoid duplicacy if same time 2 or more user calling same function*/
                if($this->old['Certificate']['certificateno'] != $certificates['certificateno']){
                    $certificates['certificateno'] = $this->getCertificateNO('',$certificates['issuedate']);
                    $certificates['fy'] = $this->calculateFinancialYearForDate($certificates['issuedate']);
                }                

                $this->Certificate->save($certificates);

                $item = $this->request->data['Certificate']['Certificateitem'];

                foreach ($item as $items) {
                    if (isset($items['id'])) {
                        $this->Certificateitem->id = $items['id'];
                        $this->Certificateitem->save($items);
                    }
                }
                $certi_date = $this->Certificate->find('first', array('conditions' => array('Certificate.id' => $id), 'fields' => array('Certificate.created', 'Certificate.modified', 'Certificate.isverified'), 'recursive' => '-1'));
                if ($certi_date['Certificate']['created'] == $certi_date['Certificate']['modified'] && $certi_date['Certificate']['isverified'] == 0) {
                    $QA = $this->User->find('all', array('conditions' => array('User.usertype_id' => '5'), 'fields' => array('User.first_name', 'User.last_name', 'User.email')));
                    $to_send = array();
                    foreach ($QA as $user) {
                        $to_send[] = $user['User']['email'];
                    }
                    $role_id = AuthComponent::user('usertype_id');
                    $log_email = AuthComponent::user('email');
                    if ($role_id == 5) {
                        $pos = array_search($log_email, $to_send);
                        unset($to_send[$pos]);
                    }
                    $log_first = AuthComponent::user('first_name');
                    $log_last = AuthComponent::user('last_name');
                    $Email = new CakeEmail();
                    $Email->to($to_send);
                    $Email->subject('[SHALCO ERP] Verify Test Certificate');
                    $Email->from(array('erp@shalco.in' => 'Shalco Industries Pvt. Ltd.'));
                    $Email->emailFormat('html');
                    $url = WEBSITE_PATH . "certificates/getVerified/" . $id;
                    $verify_url = '<a href="' . $url . '">' . $url . '</a>';
                    $Email->send('Respected ' . $QA[0]['User']['first_name'] . ' ' . $QA[0]['User']['last_name'] . '<br><br><br>' .
                            'As per task assigned me, I have created Test Certificate<br>
					  Please check detail view either by attachment or from here :<br>
					  Please verify if by click on below link :<a href="' . $url . '">' . $url . '</a>' . '<br><br>' . '
					  Thanks & Regards<br>'
                            . $log_first . ' ' . $log_last);
                }
                $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>Certificate has been Updated successfully</div>'));
                $this->redirect('index');
            } else {
                $errors = $this->Certificate->validationErrors;
            }
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;

        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'certificates/">Test Certificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'Edit Test Certificate');
        //$log = $this->Order->getDataSource()->getLog(false, false);
        //debug($log); exit;	
    }

    /**
     * Name: view
     * Use: view certificate
     * @param int $id id of record
     * @return detail record of id	 
     */
    function view($reftype, $id = null) {
        $id = base64_decode($id);
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'certificates/">Test Certificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'View Test Certificate');
    }

    /*
     *
     * Name : export
     * Use : Export the Order data
     */

    function export($reftype, $id) {
        $this->layout = false;
        $this->autoRender = false;
        ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large		
        if (empty($id)) {
            $this->redirect($this->referer());
        }
        if ($reftype != 0 && $reftype != 1) {
            $this->redirect($this->referer());
        }
        $objPHPExcel = new PHPExcel(); // Create new PHPExcel object
        $objRichText = new PHPExcel_RichText();
        $reportname = 'Test Certificate';

        $setting= $this->Session->read('setting_data');

        $certi = $this->getcertificatedetail($reftype, $id);
        $itemcount = count($certi['Certificateitem']); /* Number of item in certificate */

        if ($certi['Certificate']['reftype'] == 1) {
            $itemmodel = 'Chalanitem';
            $mainmodel = 'Chalan';
        } else {
            $itemmodel = 'Invoiceitem';
            $mainmodel = 'Invoice';
        }

        $grade_id = $certi['Certificateitem'][0][$itemmodel]['Orderitem']['grade_id']; /* Grade id of first item */
        $chem_req = $this->requestAction('Grades/getchemical/' . $grade_id);
        $chem_comp = ''; /* Chemical component */
        $chem_min = ''; /* Chemical component Min value */
        $chem_max = ''; /* Chemical component Max value */
        $chem_comp_name = array();
        $mech_req = $this->requestAction('Grades/getmechanical/' . $grade_id);
        $phy_req = $this->requestAction('Grades/getphysical/' . $grade_id);
        $start_index = 1; /* cell number where content start */
        $info_index = 8; /* cell number where basic information start */
        $info_end = $info_index + $itemcount + 1; /* cell number where basic information end */
        $srno_header = $info_index + $itemcount; // KS
        
        //$chem_index = $srno_end; /* cell number where chemical proparty information start */
        $phy_index = 1; /* cell number where physical proparty information start */
        //$total_column = 4 + count($chem_req); /* total number of column number of excel we needed */
        //$maxExcelColumn = substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $total_column, 1); /* returns Last Column */
        
        $total_column = (4 + count($chem_req)); /* total number of column number of excel we needed */
        if($total_column < 13){
            $total_column = 13;
        }
        $maxExcelColumn = substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $total_column, 1); /* returns Last Column */

        $chemcount = $itemcount + 3;
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

        $style_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_LEFT,));

        $border_style = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $style_underline = array('font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));
        $custom_head = array(
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
       // pr($certi);exit;

        $basic_first[] = array('bindex' => $info_index, 'Client', $certi[$mainmodel]['Order']['Client']['company_name'], 'Contract', $certi['Order']['clientpo'] . 'Dt:' . $this->requestAction('App/date_ymd_to_dmy/'.strtotime($certi['Order']['clientpodate'])));
        $basic_first[] = array('bindex'=> ++$info_index, 'Specification', $certi['Certificate']['std_spec'] , 'Grade' , $certi['Certificate']['grd_spec']);

        
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', 'Certificate No.'.$certi['Certificate']['certificateno'])->mergeCells('A7:C7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D7', 'PED '.$setting['Setting']['ped_certificate'])->mergeCells('D7:F7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I7', 'Certificate No. '.$setting['Setting']['iso_certificate'])->mergeCells('I7:J7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L7','ISO 9001:2008')->mergeCells('L7:M7');
        $objPHPExcel->getActiveSheet()->getStyle('A7:'.$maxExcelColumn.'7')->getFont()->setBold(true);
        foreach ($basic_first as $basicvalue) {
            $index_val = $basicvalue['bindex'];
            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $index_val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($style);

            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val . ':' . $maxExcelColumn . $index_val)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val . ':' . $maxExcelColumn . $index_val)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $index_val)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $index_val)->applyFromArray($border_left);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $index_val, $basicvalue[0])->mergeCells('A' . $index_val . ':B' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $index_val, $basicvalue[1])->mergeCells('C' . $index_val . ':F' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $index_val, $basicvalue[2])->mergeCells('G' . $index_val . ':H' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $index_val, $basicvalue[3])->mergeCells('I' . $index_val . ':' . $maxExcelColumn . $index_val);
        }
        //echo $srno_header;exit;
            
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $srno_header, 'P.O. Sr No')->mergeCells('A'.$srno_header . ':B' . $srno_header);

        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header)->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header)->getFont()->setBold(true);
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, 'Product')->mergeCells('C'.$srno_header . ':F' . $srno_header);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($style);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header, 'Size')->mergeCells('G'.$srno_header . ':M' . $srno_header); 
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->getFont()->setBold(true);
        foreach ($certi['Certificateitem'] as $item) {

            $keyid = $item['id'];
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$srno_header, $item[$itemmodel]['Orderitem']['sr_no'])->mergeCells('A'.$srno_header . ':B' . $srno_header);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, $item[$itemmodel]['Orderitem']['Productcategory']['productname'])->mergeCells('C'.$srno_header . ':F' . $srno_header);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header,$this->requestAction('Sizes/getsizedimension/'.$item[$itemmodel]['Orderitem']['size_id'].'/'.$item[$itemmodel]['Orderitem']['length'].'/2') )->mergeCells('G'.$srno_header . ':M' . $srno_header); 

            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $srno_header)->applyFromArray($border_left);
        }
        
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$srno_header, 'Supply Condition');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $srno_header, 'PO SR NO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, 'Heat No');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $srno_header, 'Test Ref No');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $srno_header, 'Surface Finish');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $srno_header, 'Dimension Check');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header, "Total length")->mergeCells('G' . $srno_header . ':H' . $srno_header);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $srno_header, "Total Quantity")->mergeCells('I' . $srno_header . ':J' . $srno_header);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $srno_header, "Total Net Weight")->mergeCells('K' . $srno_header . ':' . $maxExcelColumn . $srno_header);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($custom_head);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('K' . $srno_header)->applyFromArray($border_left);

        $srno_start = $srno_header + 1; /* Sedtion 4 : cell number where serial number (sr no) start */
        $srno_end = $srno_start + ($itemcount - 1); /* Sedtion 4 : cell number where serial number (sr no) start */
       // echo $srno_end;exit;    

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $srno_start, $certi['Certificate']['supply_condition'])->mergeCells('A' . $srno_start . ':A' . $srno_end);
       // $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_end . ':' . $maxExcelColumn . $srno_end)->applyFromArray($border_bottom_thick);
        foreach ($certi['Certificateitem'] as $item) {

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $srno_start, $item[$itemmodel]['Orderitem']['sr_no']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_start, $item['heatno']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $srno_start, $item['testref_no']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $srno_start, $item['surface_finish']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $srno_start, $item['dimension']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_start, $item[$itemmodel]['Orderitem']['length'])->mergeCells('G' . $srno_start . ':H' . $srno_start);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $srno_start, $item[$itemmodel]['qty_mtr'])->mergeCells('I' . $srno_start . ':J' . $srno_start);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $srno_start, $item[$itemmodel]['netweight'])->mergeCells('K' . $srno_start . ':' . $maxExcelColumn . $srno_start);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_start . ':' . $maxExcelColumn . $srno_start)->applyFromArray($border_bottom);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_start . ':' . $maxExcelColumn . $srno_start)->applyFromArray($style);
             for($k = 'B' ;$k <= $maxExcelColumn;$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$srno_start)->applyFromArray($border_left); 
             }
            
            ++$srno_start;
        }
        $chem_index = $srno_start;
        $chem_end = $srno_end + $chemcount;

//         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $, $item[$itemmodel]['Orderitem']['sr_no']);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$chem_index,'Solution Annalying Temp')->mergeCells('A'.$chem_index.':B'.$chem_index);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$chem_index,'Chemical Composition %')->mergeCells('C'.$chem_index.':'.$maxExcelColumn.$chem_index); 
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index.':'.$maxExcelColumn.$chem_index)->applyFromArray($style);

          if (isset($phy_req['Physical']['annealing_c']) && $phy_req['Physical']['annealing_c'] != ''){ 
                $solu_row  = $phy_req['Physical']['annealing_c'];
            }
            else { 
                $solu_row ='';
            }
             $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);

         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$chem_index, $solu_row)->mergeCells('A' . $chem_index . ':B' . ++$chem_end);
           for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_bottom_thick); 

             }
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, 'Heat No');
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, '');
         $abc = 'E';
         $chem_comp_name= array();
         foreach ($chem_req as $value) {
            $chem_comp_name[] = $value[0];
            $formull[$abc] = $value[0];
            $formull1[$abc] = $value[1];
            $formull2[$abc] = $value[2];
            $abc++;
          //  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index,  ucfirst($value[0]));
        }
        foreach($formull as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val)); 
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left);  

         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom_thick);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,'');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Minimum');
        $objPHPExcel->getActiveSheet()->getStyle('D'.$chem_index)->getFont()->setBold(true);
          for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_top);
        
        foreach($formull1 as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val));   
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left);  
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,'');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Maximum');
        $objPHPExcel->getActiveSheet()->getStyle('D'.$chem_index)->getFont()->setBold(true);
         for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_bottom);
       
        foreach($formull2 as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val));   
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left); 
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        foreach ($certi['Certificateitem'] as $item) {
            //echo $new_abc;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,$item['heatno']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Observed');
             for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
            $new_abc = 'E';
            foreach ($chem_comp_name as $val) {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue($new_abc .$chem_index,$item[$val]);
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_left); 
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_bottom);
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($style);
               $new_abc++;
            }
        }
        $chem_end = $chem_index + 1;

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$chem_index, 'Testing item')->mergeCells('A' . $chem_index . ':B' . ++$chem_end);
         $objPHPExcel->getActiveSheet()->getStyle('C'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
         $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
     
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, 'Tensile Strength');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, 'SIZE');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, 'Yield Strength ');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' .$chem_index, 'Elongation %');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' .$chem_index, 'Flattening Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' .$chem_index, 'Flaring Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, 'Eddy Current Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' .$chem_index, 'Ultrasonic Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' .$chem_index, 'Hardness HRB');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L' .$chem_index, 'Hydro Test (Pressure) PSI');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' .$chem_index, 'IGC - Test');

         $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
         $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);
         $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
        for($k = 'C'; $k <= $maxExcelColumn; $k++){
            $objPHPExcel->getActiveSheet()->getStyle($k.$chem_index)->applyFromArray($border_left); 
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .++$chem_index, '(MPA)');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, 'Coarser');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, 'RP 0.2%');

        $objPHPExcel->getActiveSheet()->getStyle('C' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);

            $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);

         for($k = 'C'; $k <= $maxExcelColumn; $k++){
                        $objPHPExcel->getActiveSheet()->getStyle($k.$chem_index)->applyFromArray($border_left); 
            }

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .++$chem_index, 'Required');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' .$chem_index, $certi['Certificate']['grd_spec']);
         $objPHPExcel->getActiveSheet()->getStyle('B'.$chem_index)->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, $mech_req['Mechanical']['tensile_strength']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, $mech_req['Mechanical']['grain_size']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, $mech_req['Mechanical']['yield_strength']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' .$chem_index, $mech_req['Mechanical']['elongation']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' .$chem_index, $mech_req['Mechanical']['flattening']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' .$chem_index, $mech_req['Mechanical']['flaring']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, $mech_req['Mechanical']['eddy']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' .$chem_index, $mech_req['Mechanical']['ultrasonic']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' .$chem_index, $mech_req['Mechanical']['hardness']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L' .$chem_index, 'text 001');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' .$chem_index, '');

        $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
        $final_end = $chem_index + ($itemcount - 1);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .++$chem_index, 'Testing Data')->mergeCells('A' . $chem_index . ':B' . ++$final_end);
        $final_abc = 'C';
        foreach ($certi['Certificateitem'] as $item) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$chem_index,$item['heatno']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$chem_index,$item['tensile_strength']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,$item['grain_size']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$chem_index,$item['yield_strength']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$chem_index,$item['elongation']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$chem_index,$item['flattening']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$chem_index,$item['flaring']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$chem_index,$item['eddy']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$chem_index,$item['ultrasonic']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$chem_index,$item['hardness']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$chem_index,$item['hydro']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$chem_index,'');
           // $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_left); 
            //   $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_bottom);
                    for($j = $final_abc; $j <= $maxExcelColumn; $j++){
                        $objPHPExcel->getActiveSheet()->getStyle($j.$chem_index)->applyFromArray($border_left); 
                    }
               $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom);
               $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
            $chem_index++;
            // $final_abc++;
        }
        $final_end = $final_end + 7;
        $remarks = str_replace('<br/>', "\n ", $certi['Certificate']['remarks']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .$chem_index, 'Remarks'."\n" .$remarks)->mergeCells('A' .$chem_index.':H'.$final_end);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index)->getAlignment()->setWrapText(true);
//exit;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, 'Head QA.QC')->mergeCells('I' .$chem_index.':'.$maxExcelColumn.$final_end);
        //echo $final_end;exit;
        $objPHPExcel->getActiveSheet()->getStyle('I' .$chem_index.':I'.$final_end)->applyFromArray($border_left); 
               $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $final_end)->applyFromArray($border_bottom_thick);
       $objPHPExcel->getActiveSheet()->getStyle('A2'.':'.$maxExcelColumn.$final_end)->applyFromArray($border_style);
        //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$info_index, $this->requestAction('Sizes/getsizedimension/'.$item[$itemmodel]['Orderitem']['size_id'].'/'.$item[$itemmodel]['Orderitem']['length'].'/2'));
        //$maxExcelColumn = $excelcolumn; 
        /* End Set Item column name For Row 22nd and 23rd */

        ob_clean();

        /* header('Content-Type: application/vnd.ms-excel'); */

        /* if($_GET['type'] == '2'){

          //$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');

          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
          //$objPHPExcel->getDefaultStyle()->getAlignment()->setIndent(0);
          //$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

          $rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/dompdf';

          $rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
          //$rendererLibrary = 'dompdf.php';

          if (!PHPExcel_Settings::setPdfRenderer(
          $rendererName,
          $rendererLibraryPath
          //	$rendererLibrary
          )) {
          die(
          'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
          '<br />' .
          'at the top of this script as appropriate for your directory structure'
          );
          }
          header('Content-Type: application/pdf');
          header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          //$objPHPExcel->getActiveSheet()->setTitle('Orari');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
          $objWriter->setPreCalculateFormulas (false);
          $objWriter->save('php://output');
          }
          if($_GET['type'] == '1'){ */
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $reportname . ".xlsx");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        /* }
          if($_GET['type'] == '3'){

          //$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
          $rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/tcpdf';

          $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
          //$rendererLibrary = 'dompdf.php';

          if (!PHPExcel_Settings::setPdfRenderer(
          $rendererName,
          $rendererLibraryPath
          //	$rendererLibrary
          )) {
          die(
          'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
          '<br />' .
          'at the top of this script as appropriate for your directory structure'
          );
          }
          header('Content-Type: application/pdf');
          header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          //$objPHPExcel->getActiveSheet()->setTitle('Orari');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
          $objWriter->setPreCalculateFormulas (false);
          $objWriter->save('php://output');
          } */
        exit();
    }

    /**
     * Name : getVerified
     * Use : Verify Test Certificates
     * @param int $id id of record
     * @return void
     */
    function getVerified($id) {
        $this->autoRender = false;
        $data = $this->Certificate->find('first', array('conditions' => array('Certificate.id' => $id), 'fields' => array('Certificate.isverified,Certificate.id'), 'recursive' => -1));
        //pr($data);exit;
        $verified = $data['Certificate']['isverified'];
        $verified_user = $this->Auth->user('id');
        /* Check if proforma is already verified or not, if not verified than we send email and update info */
        if ($verified == 0) {
            $update = array('isverified' => 1, 'verifiedby' => $verified_user, 'verified_date' => date('Y-m-d H:i:s'));
            $this->Certificate->id = $id;
            $this->Certificate->save($update);
        }
        else{$this->redirect(array('controller' => 'certificates', 'action' => 'index'));} 
    }
    
    /**
     * Name : generatecertificatepdf
     * Use : generate certificate pdf using tcPDF library
     * @param int $id id of record
     * @param int $reftype reftype of record
     * @return void
     */
    function generatecertificatepdf($reftype, $id = null){
        $id = base64_decode($id);
        ini_set('memory_limit', '1024M');
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'certificates/">Test Certificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'View Test Certificate');
    }


    /**
     * Name: generatepdf
     * Use: generate pdf file of record
     * @param int $id id of record
     * @param int $reftype reftype of record
     * @return pdf file      
    */
    public function generatepdf($reftype, $id = null) {
        
        $this->pdfConfig = array(
            'filename' => 'invoice',
            'download' => $this->request->query('download')
        );
        
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'certificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);

        /* Make sure the controller doesn't auto render. */
        $this->autoRender = true;
        /* Set up new view that won't enter the ClassRegistry */
        $view = new View($this, false);
        $view->layout = null;       
        $view->viewPath = 'Certificates';
        ini_set('memory_limit', '1024M');
        $html = $view->render('generatepdf');
        $this->set('post',$html);
    }
}