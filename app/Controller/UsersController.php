<?php
/*
 * @Controller name: User Controller
 * @Version: 1.1.0
 * @Author: Shalco
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to User management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class UsersController extends AppController {
	var $name = 'Users';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login','logout','forgotpass','replacepass');
		$this->set('mainTitle','User Management');
	}

   /**
   	* Name: index
   	* Use: User view.
	*/
	function index() {
        $this->set('pageTitle',' User List');
        $this->set('ajaxaction','ajaxlisting');
	}

   /**
   	* Name: Listing with Ajax
   	* Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('User.id','first_name','email','mobile','usertype_name','User.modified','User.modifiedby','User.status');
    	$order = array('User.modified' => 'desc');
		$res = $this->datatable_append($order,$column);
		$count = $this->User->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$users = $this->User->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('users',$users);
		$this->set('count',$count);
	}
   
   /**
   	* Name: Login For User
   	* Use: User Authantication for System.
	*/
	function login() {
		$this->layout = "login";
		$this->set('pageTitle','Login');
		if ($this->Auth->user('id')) {
			$this->redirect(WEBSITE_PATH.'dashboards/index');
		}
		
	    if(!empty($this->request->data)) {    
	        $this->User->set($this->request->data); 
	        $this->User->validator()->remove('email', 'unique');
	        $this->User->validator()->remove('password', 'minLength');
	        $this->User->validator()->remove('password', 'between');
	        if($this->User->validates($this->request->data)) { 
	          if($this->Auth->login()) { 
	          	$userdata = $this->User->findById($this->Auth->user('id')); 
				$this->Session->write('login_usertype_id',$userdata['User']['usertype_id']);

	          	     if($this->request->data['User']['remember'] == 1){
              				setcookie('email',$this->request->data['User']['email'],time() + (86400 * 7));
              				setcookie('password',$this->request->data['User']['password'],time() + (86400 * 7));
              				setcookie('remember','Y',time() + (86400 * 7));
            			}

            	/*redirect on reference (previous) page if logout by session by system*/		
            	if($userdata['User']['usertype_id'] == 1){
       				return $this->redirect($this->Auth->redirectUrl());
       			}
	          	$this->redirect(WEBSITE_PATH.'dashboards/index');
	          }
	          else {
	            $this->Session->setFlash(__('<label class="errormsg" for="error">Email or password incorrect.</label>'));
	          }
	        }
	    }
	}

   /**
   	* Name: Add 
   	* Use: Add New User.
	*/
	function add($id = null) {
		$this->loadmodel('Usertype');
        $id = base64_decode($id);
        $this->User->id = $id;		
		$this->old = $this->User->findById($id,array('recursive'=>0));
        /* Start : Add/Edit Submit Form Event*/
		if(!empty($this->request->data)) {
			$Activities = new ActivitiesController;
			$this->request->data['User']['username'] = $this->request->data['User']['email'];
			$logged_user = $this->Auth->user('id');
			$this->request->data['User']['modifiedby'] = $logged_user; 			
			if(!$id){
				$this->User->validator()->remove('newpassword', 'between');
				$this->request->data['User']['createdby'] = $logged_user;				
				//$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], null, true); 
			}
			else{ 
				if(!empty($this->request->data['User']['newpassword'])) { 
					
				}
				else{
					$this->User->validator()->remove('newpassword', 'between');
					$this->User->validator()->remove('password', 'required');
				}
			}
			$this->User->set($this->request->data);
			if($this->User->validates($this->request->data)) {			
				if(!empty($id)) {
					$this->request->data['User']['id'] = $id;
					if(!empty($this->request->data['User']['newpassword'])){ 
						$this->request->data['User']['password'] = $this->request->data['User']['newpassword']; 
					}
				}
				if($this->User->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = 'User has been Updated successfully';
						$result = array_diff_assoc($this->old['User'],$this->request->data['User']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'User','Edit',$result);/*Add Data in Log*/												
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'User','Add',$this->request->data['User']);/*Add Data in Log*/
						$msg = 'User has been Added successfully';
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					$this->redirect('/users/index');
				}
			}			
		}
		/* End : Add/Edit Submit Form Event*/
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit User');
			$this->request->data = $this->User->findById($id);
			if(empty($this->request->data)){	/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add User');		
		}
        $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'users/">User</a><i class="fa fa-circle"></i></li>');		
		$this->set('id',$id);
		$user_role = $this->Usertype->find('list', array(
        	'fields' => array('id','usertype_name'),
        	'recursive' => 0
    	));
		$this->set('roles',$user_role);
	}

	/**
   	 * Name: delete
   	 * Use: delete user
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null){
		$id = base64_decode($id);
		$this->User->id = $id;
		$data = $this->User->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'User','Delete',$data['User']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> User has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}

   /**
	* Name : Changepassword 
	* Use : Login user can change thier password.
	*/
	function changepass() {
		$this->set('pageTitle','Change Password');
		
		$this->Session->write('title','Change password');
		$loginId = AuthComponent::user('id');

		if(!empty($this->data)) {
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$loginId)));
			$old_password = Security::hash($this->request->data['User']['old_password'], null, true);
			if($old_password == $user['User']['password']) {
				$this->User->set($this->data);
				if($this->User->validates($this->data)) {
					$data = array('id' => $loginId,'password' => $this->data['User']['password']);
					$this->User->save($data);
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Password has been changed successfully</div>'));
					$this->redirect(array('action'=>'changepass','controller'=>'users'));
				}
				else {
					$errors = $this->User->validationErrors;
				}
			}
			else {
				$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> Current Password is incorrect!</div>'));
			}
		}
	}

   /**
	* Name :  Personalinfo
	* Use  :  Logged user can update his/her Profile Details
	*/
	function personalinfo() {
		$this->set('pageTitle','Personal Information');
		$this->set('mainTitle','Personal Information');

		$loginId = AuthComponent::user('id');
		$loginUser = $this->Auth->User();

		if(!empty($this->data)) {
			$this->User->set($this->data);
			if($this->User->validates($this->data)) {
				$this->User->id = $loginId;
				$userdata = array(
					'first_name' => $this->data['User']['first_name'],
					'last_name' => $this->data['User']['last_name'],
					'gender' => $this->data['User']['gender'],
					'mob' => $this->data['User']['mobile'],
					'dob' => date('Y-m-d',strtotime($this->data['User']['dob'])),
				);
				$this->User->save($userdata);
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Personal Information has been Updated</div>'));
					$this->redirect(array('controller'=>'users','action'=>'personalinfo'));
			}
			else {
				$errors = $this->User->validationErrors;
			}
		}
				
		$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$loginId)));
		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$loginId)));
		$this->set('user',$user);
	}

  /**
   	* Name: Logout 
   	* Use: User Logout
	*/
	function logout(){

		$this->Session->delete('Auth.User');
		$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> You are successfully logged out</div>'));
		$this->Session->delete('accesscontrollers_actions');
		$this->Session->delete('controller_access');
		$this->Session->delete('login_usertype_id');

		$this->redirect('login');
	}
	/*
    Name : getUsername();
    use: get user name from id
    */
    function getUsername($id = null){
        $username = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'fields'=>array('User.first_name','User.last_name','User.email')));
        return $username['User']['first_name']." ".$username['User']['last_name'];
    }
    /*
   	 * Name: forgotpass
   	 * Use: User can forgot own password
   	 * Parameters:
	 */
	function forgotpass() {
		$this->layout = 'login';
		$this->set('pageTitle','Forgot password');
		if(!empty($this->request->data)) {
			$this->User->set($this->data);
			$this->User->validator()->remove('email', 'unique');
			$this->User->validator()->remove('email', 'role');
			/*$this->User->validator()->remove('password', 'between');
			$this->User->validator()->remove('password', 'compare');*/

			if($this->User->validates($this->data)) {
				$this->User->unBindModel(array("belongsTo" => array('Usertype')));
				$fields = array('User.id','User.first_name','User.last_name','User.email','User.status');
				$user = $this->User->find('first',array('conditions'=>array('User.email' => $this->request->data['User']['email']),'fields'=>$fields));
				$token = md5(uniqid(mt_rand(), true));
				if($user) {
					if($user['User']['status'] == 1) {
						$this->User->unBindModel(array("belongsTo" => array('Usertype')));
						$this->User->updateAll(array('token' => "'".$token."'"),array('id' => $user['User']['id']));

						$temp = $this->getEmailtemplate('forgot_pass_email');
						$templateBody = $temp['Emailtemplate']['body'];
						$templateSub = $temp['Emailtemplate']['subject'];
						$url = WEBSITE_PATH."users/replacepass/".$token;
						$user['User']['url'] = '<a href="'.$url.'">'.$url.'</a>';
						$Email = new CakeEmail();
						$Email->to($user['User']['email']);
						$Email->subject($templateSub);
						$Email->from(array('shalco@gmail.com' => 'My Site'));
						$Email->emailFormat('html');
						$Email->send($user['User']['url']);
		
						/*$employee_info = array(
							'request_id' => $user['User']['id'],
							'to' => $user['User']['email'],
							'subject' => 'This is subjet',
							'body' => '',
							'type' => '4'
							);
						*/
						$moduleName = 'users/forgotpass';
						$MSG = 'this is msg';
						$userdata = array(
							'loingName' => $user['User']['first_name']." ".$user['User']['last_name']
							);
					//	$activity = $this->replaceTemplate($MSG,$userdata);
					//	$this->addlog($this->loggedin_user_info['id'],$moduleName,$activity,$user['User']['id']);

						$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>Reset password link successfully Sent.</div>'));
						$this->redirect(array('controller' => 'users', 'action' => 'login',$token));
					}
					else {
						$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> You are not authorized to access</div>'));
					}
				}
				else {
					$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> You are not authorized to access</div>'));
				}
			}
			else {
				$errors = $this->User->validationErrors;
				/*debug($errors);*/
			}
		}
	}

	/*
   	 * Name: replacepass
   	 * Use: Reset password for user
   	 * Parameters:
	 */
	function replacepass($token = '') {
		$this->set('pageTitle','Reset password');
		$this->layout = "login";
		$this->Session->write('title','Reset your password');

		$fields = array('User.id','User.first_name','User.last_name','User.email');
		$user = $this->User->find('first',array('conditions'=>array('User.token'=>$token)));

		if(!empty($this->request->data)) {
			$this->User->set($this->request->data);
			if($this->User->validates($this->request->data)) {
				$data = array(
					'id' => $user['User']['id'],
					'password' => $this->data['User']['password'],
					'token' => ''
					);
				$this->User->save($data);

				/* Add log */
				$moduleName = 'users/replacepass';

				$MSG = 'replacemsg';
				$userdata = array(
					'loingName' => $user['User']['first_name'].' '.$user['User']['last_name']
					);
			//	$activity = 'recentactivity';
			//	$this->addlog($this->loggedin_user_info['id'],$moduleName,	$activity,$user['User']['id']);

				//$noti = $this->getNotificationtemplate('change_pass_success');
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Password changed successfully.</div>'));
				$this->redirect(array('controller' => 'users', 'action' => 'login'));
			} else {
				$errors = $this->User->validationErrors;
			}
		}

		if(empty($user)) {
			$noti = $this->getNotificationtemplate('not_authorized_to_access');
			$this->set('notauthorized','notauthorized');
			$this->set('noti',$noti);
		}

		if(isset($token)) {
			$this->set('token',$token);
		}
	}

    
}