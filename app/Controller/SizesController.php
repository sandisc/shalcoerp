<?php
/*
 * @Controller name: Size Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Size management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class SizesController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Size Management');
	}
	/*
   	 * Name: index
   	 * Use: Size view.
	 */
	function index() {
        $this->set('pageTitle',' Size List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','Producttaxonomy.name','gdmm','gdnb','calc_ratio','Size.modified','first_name');
    	$order = array('Size.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Size->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$sizes = $this->Size->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));

		$this->set('size',$sizes);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Size.
	 */
	function add($id = null) {
		
		$this->loadmodel('Producttaxonomy');
		$id = base64_decode($id);
		$this->Size->id = $id;
		$this->old = $this->Size->findById($id,array('recursive'=>0));
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit Size');
			$this->request->data = $this->Size->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Size');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'sizes/">Sizes</a><i class="fa fa-circle"></i></li>');
		$product_cat = $this->Producttaxonomy->find('all',array('fields'=>array('Producttaxonomy.id','Producttaxonomy.name','Producttaxonomy.unit')));
	
		$list = array();
		foreach($product_cat as $cat){
			if($cat['Producttaxonomy']['id']!=2){
				$list[$cat['Producttaxonomy']['id']] = $cat['Producttaxonomy']['name'].' ( '.$cat['Producttaxonomy']['unit'].' ) ';
			}
		}	
		$this->set('taxonomy',$list);
	}	

	/*Form add/edit submit using ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Size->set($this->request->data);	
			$response=array();
			/* set reference location if add size form open in modal dialog means popup */
			if(isset($this->request->data['Size']['targetid'])){
				$response['targetid'] = $this->request->data['Size']['targetid'];
				unset($this->request->data['Size']['targetid']);	
			}
			
			if($this->Size->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Size']['modifiedby'] = $logged_user;				
				$id = '';
				if(isset($this->request->data['Size']['id'])){
					$id = $this->request->data['Size']['id'];
					$this->old = $this->Size->findById($id,array('recursive'=>0));
					/*if($this->request->data['Size']['producttaxonomy_id'] == 1){
						$this->request->data['Size']['thickness'] = '';
						$this->request->data['Size']['width'] = '';
						$this->request->data['Size']['length'] = '';
						$this->request->data['Size']['diameter'] = '';
					}elseif($this->request->data['Size']['producttaxonomy_id'] == 3){
						$this->request->data['Size']['od_nb'] = '';$this->request->data['Size']['wt_nb'] = '';
						$this->request->data['Size']['od_mm'] = '';$this->request->data['Size']['wt_mm'] = '';
						$this->request->data['Size']['diameter'] = '';
					}elseif($this->request->data['Size']['producttaxonomy_id'] == 6){
						$this->request->data['Size']['od_nb'] = '';$this->request->data['Size']['wt_nb'] = '';
						$this->request->data['Size']['od_mm'] = '';$this->request->data['Size']['wt_mm'] = '';
						$this->request->data['Size']['thickness'] = '';$this->request->data['Size']['width'] = '';
						$this->request->data['Size']['length'] = '';
					}
					else{
						$this->request->data['Size']['od_nb'] = '';
						$this->request->data['Size']['wt_nb'] = '';
						$this->request->data['Size']['od_mm'] = '';
						$this->request->data['Size']['wt_mm'] = '';
						$this->request->data['Size']['thickness'] = '';
						$this->request->data['Size']['width'] = '';
						$this->request->data['Size']['length'] = '';
						$this->request->data['Size']['diameter'] = '';
					}*/
				}				
				if(empty($id)) {
					$this->request->data['Size']['createdby'] = $logged_user;
				}
				if($this->Size->save($this->request->data)) { 
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = 'Size has been Updated successfully';
						$result = array_diff_assoc($this->old['Size'],$this->request->data['Size']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Size','Edit',$result);/*Add Data in Log*/												
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Size','Add',$this->request->data['Size']);/*Add Data in Log*/	
						$msg = 'Size has been Added successfully';
						$response['idvalue'] = $this->Size->getLastInsertID();
						$response['text'] = $this->request->data['Size']['gdmm'];
					}
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						//$this->redirect('/sizes/index');
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'size';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
				}
			}
			else{
                $Size = $this->Size->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Size could not be saved. Please, try again.';
                $response['data'] = compact('Size');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGrades($id) {
		$this->loadmodel('Grade');
		$this->autoRender = false;
   		$grade_data = $this->Grade->find('all',array('fields' => array('Grade.id','Grade.grade_name'),'conditions'=> array('Grade.standard_id'=>$id)));
   		
   		echo '<select>';
		echo '<option value="">Select Grade</option>';
			foreach($grade_data as $grades) 
			{ 
	  		echo '<option value="'.$grades['Grade']['id'].'">'.$grades['Grade']['grade_name'].'</option>'."\n";
	  		}
		echo '</select>';
		exit;
	}
	/**
   	 * Name: delete
   	 * Use: delete size
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Size->id = $id;
		$data = $this->Size->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Size','Delete',$data['Size']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Size->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Size has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}

	/**
	* Name : getsizedimension
   	* Use : get Size dimension in different format
    * @param int $size_id id of size table
    * @param int $length length in meter
    * @param int $type means which format you need,0 for mm, 1 for nb and 2 for both
    * @return full formatted size
   	*/
	function getsizedimension($size_id,$length,$type=0) { 
		$sizedata = $this->Size->find('first',array('conditions'=>array('Size.id'=>$size_id)));
		$output = '';
		if($type == 0){
			if(!empty($sizedata['Size']['od_mm'])){
				$output .= $sizedata['Size']['od_mm'].'MM X ';
			}
			if(!empty($sizedata['Size']['od_mm'])){
				$output .= $sizedata['Size']['wt_mm'].'MM X ';
			}
			if(!empty($length)){
				$output .= $length;
			}
			//$output = $sizedata['Size']['od_mm'].'MM x '.$sizedata['Size']['wt_mm'].'MM x '.$length;
		}
		elseif($type == 1){
			if(!empty($sizedata['Size']['od_nb'])){
				$output .= $sizedata['Size']['od_nb'].'MM X ';
			}
			if(!empty($sizedata['Size']['wt_nb'])){
				$output .= $sizedata['Size']['wt_nb'].'MM X ';
			}
			//$output = $sizedata['Size']['od_nb'].' X '.$sizedata['Size']['wt_nb'];
		}
		else{
			if(!empty($sizedata['Size']['od_mm'])){
				$output .= $sizedata['Size']['od_mm'].'MM X ';
			}
			if(!empty($sizedata['Size']['wt_mm'])){
				$output .= $sizedata['Size']['wt_mm'].'MM X ';
			}
			if(!empty($length)){
				$output .= $length;
			}
			if(!empty($output)){
				$output .= ' /';
			}						
			if(!empty($sizedata['Size']['od_nb'])){
				$output .= $sizedata['Size']['od_nb'].'MM X ';
			}
			if(!empty($sizedata['Size']['wt_nb'])){
				$output .= $sizedata['Size']['wt_nb'].'MM';
			}
			//$output = $sizedata['Size']['od_mm'].'MM X '.$sizedata['Size']['wt_mm'].'MM X '.$length .'  / '.$sizedata['Size']['od_nb'].' X '.$sizedata['Size']['wt_nb'];
		}
		return $output;
	}	
}