<?php

/*
 * @Controller name: Dummycertificate Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Dummycertificate management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file' => 'dompdf/dompdf_config.inc.php'));
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));

class DummycertificatesController extends AppController {

    var $helpers = array('Html', 'Form', 'Csv');
    var $components = array('Auth', 'RequestHandler', 'Session', 'Email');
    var $uses = array('Dummycertificate', 'Dummycertificateitem', 'User');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->set('mainTitle', 'Dummy Test Certificate Management');
    }

    /*
     * Name: index
     * Use: certificate listing.
     */

    function index() {
        $this->set('pageTitle', ' Dummy Test Certificate List');
        $this->set('ajaxaction', 'ajaxlisting');
    }

    function ajaxlisting() {
        $column = array('id', 'certificateno', 'print_certificateno', 'std_spec', 'grd_spec', 'Dummycertificate.modified', 'User.first_name', 'Dummycertificate.isverified');
        $order = array('Dummycertificate.modified' => 'desc');

        $res = $this->datatable_append($order, $column);
        $this->Dummycertificate->unbindModel(array('hasMany' => array('Dummycertificateitem')));
        $count = $this->Dummycertificate->find('count', array('conditions' => array($res['con']), 'order' => $res['order_by']));
        $this->Dummycertificate->unbindModel(array('hasMany' => array('Dummycertificateitem')));
        $certi = $this->Dummycertificate->find('all', array('conditions' => array($res['con']), 'limit' => $_POST['length'], 'offset' => $_POST['start'], 'order' => $res['order_by']));
        $this->set('certi', $certi);
        $this->set('count', $count);
    }

    /**
     * Name: add
     * Use: user can add dummy certificate
    */
    function add() {
        $this->loadmodel('Standard');
        $this->loadmodel('Grade');
        $this->loadmodel('Size');
        $this->loadmodel('Productcategory');
        $this->loadmodel('Client');
        $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'dummycertificates/">Certificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle','Add Dummy Certificate');

        $this->request->data['Dummycertificate']['certificateno'] = $this->getCertificateNO();

        $client = $this->Client->find('list',array('fields' => array('Client.id', 'Client.company_name'),'recursive' => 0));
        $this->set('clients',$client);

        $standards = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name'),'recursive' => 0));
        $this->set('standards',$standards);

        $procat = $this->Productcategory->find('list',array('fields' => array('Productcategory.id','Productcategory.name'),'recursive' => 0));
        $this->set('procat',$procat);  

        $sizes = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm'),'recursive' => 0));
        $this->set('sizes',$sizes);
    }

    /**
     * Name: ajaxaddsubmit
     * Use: ajax submit of add dummy certificate
    */
    function ajaxaddsubmit(){
        $this->autoRender = false;  
        $this->loadmodel('Dummycertificateitem');
        if(!empty($this->request->data)) { 

                $dummycertificate = $this->request->data['Dummycertificate'];
                $dummycertificate['orderdate'] = date('Y-m-d', strtotime(str_replace('/','-', $dummycertificate['orderdate'])));
                $dummycertificate['issuedate'] = date('Y-m-d', strtotime(str_replace('/','-', $dummycertificate['issuedate'])));
                $dummycertificate['certificateno'] = $this->getCertificateNO('',$dummycertificate['issuedate']);
                $dummycertificate['fy'] = $this->calculateFinancialYearForDate($dummycertificate['issuedate']);
                            
            $this->Dummycertificate->set($dummycertificate); 
            if($this->Dummycertificate->validates($dummycertificate)) {
                $Activities = new ActivitiesController;
                $logged_user = $this->Auth->user('id');
                $dummycertificate['Dummycertificate']['createdby'] = $logged_user;
                $dummycertificate['Dummycertificate']['modifiedby'] = $logged_user;

                
                $this->Dummycertificate->save($dummycertificate);

                $lastid = $this->Dummycertificate->getLastInsertID(); 
                $item = $this->request->data['Dummycertificate']['item'];
               
                foreach($item as $keyitem => $items){   
                    $item[$keyitem]['certificateid'] = $lastid;
                    $this->Dummycertificateitem->saveall($item[$keyitem]);
                }
                $dummycertificate['item'] = $item;/*updated value so we pass it into activity log*/
                $dummycertificate['Dummycertificateitem'] = $item;
                $Activities->addlog($this->loggedin_user_info['id'],'Dummycertificate','Add',$dummycertificate);/*Add Data in Log*/
               
                $msg = 'The Dummy Certificate has been Added';
                $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
                $redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);                
            }
            else{
                $Dummycertificate = $this->Dummycertificate->invalidFields();
                $response['status'] = 'error';
                $response['message'] = 'The Dummycertificate could not be saved. Please, try again.';
                $response['data'] = compact('Dummycertificate');
                echo json_encode($response);                
            }
        }       
    }
    function _group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    function _group_ty($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val['Orderitem'][$key]][] = $val;
        }
        return $return;
    }

    /**
     * Name : getCertificateNO
     * Use : Get Unique certificate No for dummy certificate
     */
    function getCertificateNO($type=null,$date=null){ 
        if($type == 'ajax'){ /*For on change event inside add and edit certificate form*/
            $this->autoRender=false;
        }
        /*Default current date*/
        if(empty($date)){$date = date('Y-m-d');}
        else{
            $date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
        }  
        /*If user fill date than override default date with user entered date*/
        if(isset($_POST['date'])){ 
            $date=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
        }

        $new_finan_year = $this->calculateFinancialYearForDate($date);
        $certiid = $this->Dummycertificate->find('first', array('conditions'=>array('Dummycertificate.fy' => $new_finan_year),'fields' => array('Dummycertificate.certificateno', 'Dummycertificate.fy'), 'recursive' => -1, 'order' => 'Dummycertificate.id DESC'));

        if (!empty($certiid) && $certiid['Dummycertificate']['fy'] == $new_finan_year) {
            $in_id = $certiid['Dummycertificate']['certificateno'];
            $exp_id = explode('/', $in_id);
            $inv_id = $exp_id[1] + 1;
            $pr_id = 'SHDTC/' . $inv_id . '/' . $this->calculateFinancialYearForDate($date);
            return $pr_id;
        } else {
            $pr_id = 'SHDTC/' . '1/' . $this->calculateFinancialYearForDate($date);
            return $pr_id;
        }
    }

    /**
     * Name : getHeatno
     * Use : Get Unique heat No for certificate
     */
    function getHeatno() {
        $certiid = $this->Dummycertificate->find('first', array('limit' => 1, 'order' => 'Dummycertificate.id DESC'));
        $rand = $this->rand_str(1, 1) . $this->rand_str(2, 2) . $this->rand_str(1, 1);
        if (!empty($certiid)) {
            $in_id = $certiid['Dummycertificate']['certificateno'];
            $exp_id = explode('/', $in_id);
            $cut_id = substr($exp_id[1], 4); // cut first four character from string.
            $inv_id = $cut_id + 1;
            $year = date("Y");
            $pr_id = 'SHHN/' . $inv_id . '/' . $this->getFinancialyear();
            return $pr_id;
        } else {
            $year = date("Y");
            $pr_id = 'SHHN/1/' . $this->getFinancialyear();
            return $pr_id;
        }
    }

    /**
     * Name: checkCertiGenerate
     * Use: check test certificate generate or not
     * @param int $refid invoice or chalan id of record
     * @param int $reftype reference type if invoice than 0, chalan than 1
     * @return true or false
     */
    function checkCertiGenerate($reftype, $refid) {
        if ($this->Dummycertificate->find('count', array('conditions' => array('reftype' => $reftype, 'refid' => $refid))) > 0) {
            return true;
        }
        return false;
    }

    function getcertificatedetail($reftype=null, $id=null) { 
        $this->loadModel('Dummycertificateitem'); 
        $certi = $this->Dummycertificate->find('first', array('conditions' => array('Dummycertificate.id' => $id), 'recursive' => 2));
        return $certi;
    }

    /**
     * Name: edit
     * Use: update certificate
     * @param int $id id of record
     * @return detail record of id	 
     */
    function edit($reftype, $id = null) {
        $id = base64_decode($id);
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }

        if (!empty($this->request->data)) {

            if ($this->Dummycertificate->validates($this->request->data)) {
                $Activities = new ActivitiesController;

                $logged_user = $this->Auth->user('id');
                $this->request->data['Dummycertificate']['modifiedby'] = $logged_user;
            
                $this->old = $this->Dummycertificate->find('first', array(
                                                    'conditions' => array('Dummycertificate.id' => $id ),
                                                    'recursive' => 2));                
                $this->request->data['Dummycertificate']['modifiedby'] = $logged_user;
                $dummycertificates = $this->request->data['Dummycertificate'];              
                $dummycertificates['issuedate'] = date('Y-m-d',strtotime(str_replace('/', '-',  $this->request->data['Dummycertificate']['issuedate'])));

                /*check old and new certificates number is same or not. if not than we will calling to get new certificates number again. Reason is to avoid duplicacy if same time 2 or more user calling same function*/
                if($this->old['Dummycertificate']['certificateno'] != $dummycertificates['certificateno']){
                    $dummycertificates['certificateno'] = $this->getCertificateNO('',$dummycertificates['issuedate']);
                    $dummycertificates['fy'] = $this->calculateFinancialYearForDate($dummycertificates['issuedate']);
                }                

                $this->Dummycertificate->save($dummycertificates);

                //$invoice_data = $this->Order->find('first',  array('conditions' => array('Order.id' => $id )));

                $item = $this->request->data['Dummycertificate']['Dummycertificateitem'];

                foreach ($item as $items) {
                    if (isset($items['id'])) {
                        $this->Dummycertificateitem->id = $items['id'];
                        $this->Dummycertificateitem->save($items);
                    }
                }
                $certi_date = $this->Dummycertificate->find('first', array('conditions' => array('Dummycertificate.id' => $id), 'fields' => array('Dummycertificate.created', 'Dummycertificate.modified', 'Dummycertificate.isverified'), 'recursive' => '-1'));
                if ($certi_date['Dummycertificate']['created'] == $certi_date['Dummycertificate']['modified'] && $certi_date['Dummycertificate']['isverified'] == 0) {
                    $QA = $this->User->find('all', array('conditions' => array('User.usertype_id' => '5'), 'fields' => array('User.first_name', 'User.last_name', 'User.email')));
                    $to_send = array();
                    foreach ($QA as $user) {
                        $to_send[] = $user['User']['email'];
                    }
                    $role_id = AuthComponent::user('usertype_id');
                    $log_email = AuthComponent::user('email');
                    if ($role_id == 5) {
                        $pos = array_search($log_email, $to_send);
                        unset($to_send[$pos]);
                    }
                    $log_first = AuthComponent::user('first_name');
                    $log_last = AuthComponent::user('last_name');
                    $Email = new CakeEmail();
                    $Email->to($to_send);
                    $Email->subject('Verify Test Dummycertificate');
                    $Email->from(array('shalco@gmail.com' => 'Shalco Industry'));
                    $Email->emailFormat('html');
                    $url = WEBSITE_PATH . "dummycertificates/getVerified/" . $id;
                    $verify_url = '<a href="' . $url . '">' . $url . '</a>';
                    $Email->send('Respected ' . $QA[0]['User']['first_name'] . ' ' . $QA[0]['User']['last_name'] . '<br><br><br>' .
                            'As per task assigned me, I have created proforma invoice<br>
					  Please check detail view either by attachment or from here :<br>
					  Please verify if by click on below link :<a href="' . $url . '">' . $url . '</a>' . '<br><br>' . '
					  Thanks & Regards<br>'
                            . $log_first . ' ' . $log_last);
                }
                $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>Dummycertificate has been Updated successfully</div>'));
                $this->redirect('index');
            } else {
                $errors = $this->Dummycertificate->validationErrors;
            }
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;

        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'dummycertificates/">Test Dummycertificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'Edit Dummy Test Certificate');
        //$log = $this->Order->getDataSource()->getLog(false, false);
        //debug($log); exit;	
    }

    /**
     * Name: view
     * Use: view certificate
     * @param int $id id of record
     * @return detail record of id	 
     */
    function view($reftype, $id = null) {
        $id = base64_decode($id);
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'dummycertificates/">Test Dummycertificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'View Dummy Test Certificate');
    }

    /*
     *
     * Name : export
     * Use : Export the Order data
     */

    function export($reftype, $id) {
        $this->layout = false;
        $this->autoRender = false;
        ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large		
        if (empty($id)) {
            $this->redirect($this->referer());
        }
        if ($reftype != 0 && $reftype != 1) {
            $this->redirect($this->referer());
        }
        $objPHPExcel = new PHPExcel(); // Create new PHPExcel object
        $objRichText = new PHPExcel_RichText();
        $reportname = 'Dummy Test Certificate';

        $certi = $this->getcertificatedetail($reftype, $id);
        $itemcount = count($certi['Dummycertificateitem']); /* Number of item in certificate */

        if ($certi['Dummycertificate']['reftype'] == 1) {
            $itemmodel = 'Chalanitem';
            $mainmodel = 'Chalan';
        } else {
            $itemmodel = 'Invoiceitem';
            $mainmodel = 'Invoice';
        }

        $grade_id = $certi['Dummycertificate']['grade_id']; /* Grade id of first item */
        $chem_req = $this->requestAction('Grades/getchemical/' . $grade_id);
        $chem_comp = ''; /* Chemical component */
        $chem_min = ''; /* Chemical component Min value */
        $chem_max = ''; /* Chemical component Max value */
        $chem_comp_name = array();
        $mech_req = $this->requestAction('Grades/getmechanical/' . $grade_id);
        $phy_req = $this->requestAction('Grades/getphysical/' . $grade_id);
        $start_index = 1; /* cell number where content start */
        $info_index = 8; /* cell number where basic information start */
        $info_end = $info_index + $itemcount + 1; /* cell number where basic information end */
        $srno_header = $info_index + $itemcount; // KS
        
        //$chem_index = $srno_end; /* cell number where chemical proparty information start */
        $phy_index = 1; /* cell number where physical proparty information start */
        //$total_column = 4 + count($chem_req); /* total number of column number of excel we needed */
        //$maxExcelColumn = substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $total_column, 1); /* returns Last Column */
        
        $total_column = (4 + count($chem_req)); /* total number of column number of excel we needed */
        if($total_column < 13){
            $total_column = 13;
        }
        $maxExcelColumn = substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $total_column, 1); /* returns Last Column */

        $chemcount = $itemcount + 3;
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

        $style_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_LEFT,));

        $border_style = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

        $style_underline = array('font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));
        $custom_head = array(
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
       // pr($certi);exit;

        $basic_first[] = array('bindex' => $info_index, 'Client',$certi['Client']['company_name'], 'Contract', $certi['Dummycertificate']['clientpo'] . 'Dt:' . $certi['Dummycertificate']['orderdate']);
        $basic_first[] = array('bindex'=> ++$info_index, 'Specification', $certi['Dummycertificate']['std_spec'] , 'Grade' , $certi['Dummycertificate']['grd_spec']);

        
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', 'Dummycertificate no.'.$certi['Dummycertificate']['certificateno'])->mergeCells('A7:C7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D7', 'PED '.$setting["Setting"]['ped_certificate'])->mergeCells('D7:F7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I7', 'Dummycertificate No. '.$setting["Setting"]['iso_certificate'])->mergeCells('I7:J7');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L7','ISO 9001:2008')->mergeCells('L7:M7');
        $objPHPExcel->getActiveSheet()->getStyle('A7:'.$maxExcelColumn.'7')->getFont()->setBold(true);
        foreach ($basic_first as $basicvalue) {
            $index_val = $basicvalue['bindex'];
            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $index_val)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($style);

            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val . ':' . $maxExcelColumn . $index_val)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $index_val . ':' . $maxExcelColumn . $index_val)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $index_val)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $index_val)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $index_val)->applyFromArray($border_left);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $index_val, $basicvalue[0])->mergeCells('A' . $index_val . ':B' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $index_val, $basicvalue[1])->mergeCells('C' . $index_val . ':F' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $index_val, $basicvalue[2])->mergeCells('G' . $index_val . ':H' . $index_val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $index_val, $basicvalue[3])->mergeCells('I' . $index_val . ':' . $maxExcelColumn . $index_val);
        }
        //echo $srno_header;exit;
            
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $srno_header, 'P.O. Sr No')->mergeCells('A'.$srno_header . ':B' . $srno_header);

        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header)->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header)->getFont()->setBold(true);
       $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, 'Product')->mergeCells('C'.$srno_header . ':F' . $srno_header);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($style);
       $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header, 'Size')->mergeCells('G'.$srno_header . ':M' . $srno_header); 
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->getFont()->setBold(true);
        foreach ($certi['Dummycertificateitem'] as $item) {

            $keyid = $item['id'];
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$srno_header, $item['sr_no'])->mergeCells('A'.$srno_header . ':B' . $srno_header);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, $item['Productcategory']['productname'])->mergeCells('C'.$srno_header . ':F' . $srno_header);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header,$this->requestAction('Sizes/getsizedimension/'.$item['size_id'].'/'.$item['length'].'/2') )->mergeCells('G'.$srno_header . ':M' . $srno_header); 

            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $srno_header)->applyFromArray($border_left);
        }
        
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$srno_header, 'Supply Condition');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $srno_header, 'PO SR NO');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_header, 'Heat No');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $srno_header, 'Test Ref No');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $srno_header, 'Surface Finish');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $srno_header, 'Dimension Check');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_header, "Total length")->mergeCells('G' . $srno_header . ':H' . $srno_header);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $srno_header, "Total Quantity")->mergeCells('I' . $srno_header . ':J' . $srno_header);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $srno_header, "Total Net Weight")->mergeCells('K' . $srno_header . ':' . $maxExcelColumn . $srno_header);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_top_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($custom_head);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_header . ':' . $maxExcelColumn . $srno_header)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $srno_header)->applyFromArray($border_left);
            $objPHPExcel->getActiveSheet()->getStyle('K' . $srno_header)->applyFromArray($border_left);

        $srno_start = $srno_header + 1; /* Sedtion 4 : cell number where serial number (sr no) start */
        $srno_end = $srno_start + ($itemcount - 1); /* Sedtion 4 : cell number where serial number (sr no) start */
       // echo $srno_end;exit;    

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $srno_start, $certi['Dummycertificate']['supply_condition'])->mergeCells('A' . $srno_start . ':A' . $srno_end);
       // $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_end . ':' . $maxExcelColumn . $srno_end)->applyFromArray($border_bottom_thick);
        foreach ($certi['Dummycertificateitem'] as $item) {

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $srno_start, $item['sr_no']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $srno_start, $item['heatno']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $srno_start, $item['testref_no']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $srno_start, $item['surface_finish']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $srno_start, $item['dimension']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $srno_start, $item['length'])->mergeCells('G' . $srno_start . ':H' . $srno_start);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $srno_start, $item['qty_mtr'])->mergeCells('I' . $srno_start . ':J' . $srno_start);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $srno_start, $item['netweight'])->mergeCells('K' . $srno_start . ':' . $maxExcelColumn . $srno_start);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_start . ':' . $maxExcelColumn . $srno_start)->applyFromArray($border_bottom);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $srno_start . ':' . $maxExcelColumn . $srno_start)->applyFromArray($style);
             for($k = 'B' ;$k <= $maxExcelColumn;$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$srno_start)->applyFromArray($border_left); 
             }
            
            ++$srno_start;
        }
        $chem_index = $srno_start;
        $chem_end = $srno_end + $chemcount;

//         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $, $item[$itemmodel]['Orderitem']['sr_no']);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$chem_index,'Solution Annalying Temp')->mergeCells('A'.$chem_index.':B'.$chem_index);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$chem_index,'Chemical Composition %')->mergeCells('C'.$chem_index.':'.$maxExcelColumn.$chem_index); 
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index.':'.$maxExcelColumn.$chem_index)->applyFromArray($style);

          if (isset($phy_req['Physical']['annealing_c']) && $phy_req['Physical']['annealing_c'] != ''){ 
                $solu_row  = $phy_req['Physical']['annealing_c'];
            }
            else { 
                $solu_row ='';
            }
             $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);

         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$chem_index, $solu_row)->mergeCells('A' . $chem_index . ':B' . ++$chem_end);
           for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_bottom_thick); 

             }
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, 'Heat No');
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, '');
         $abc = 'E';
         $chem_comp_name= array();
         foreach ($chem_req as $value) {
            $chem_comp_name[] = $value[0];
            $formull[$abc] = $value[0];
            $formull1[$abc] = $value[1];
            $formull2[$abc] = $value[2];
            $abc++;
          //  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index,  ucfirst($value[0]));
        }
        foreach($formull as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val)); 
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left);  

         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom_thick);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,'');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Minimum');
        $objPHPExcel->getActiveSheet()->getStyle('D'.$chem_index)->getFont()->setBold(true);
          for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_top);
        
        foreach($formull1 as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val));   
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left);  
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,'');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Maximum');
        $objPHPExcel->getActiveSheet()->getStyle('D'.$chem_index)->getFont()->setBold(true);
         for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
         $objPHPExcel->getActiveSheet()->getStyle('C' .$chem_index.':D'.$chem_index)->applyFromArray($border_bottom);
       
        foreach($formull2 as $key => $val){
         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($key .$chem_index,  ucfirst($val));   
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_left); 
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle($key .$chem_index)->applyFromArray($style);
        }
        foreach ($certi['Dummycertificateitem'] as $item) {
            //echo $new_abc;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.++$chem_index,$item['heatno']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,'Observed');
             for($k = 'C' ;$k <= 'D';$k++){
                $objPHPExcel->getActiveSheet()->getStyle($k .$chem_index)->applyFromArray($border_left); 
             }
            $new_abc = 'E';
            foreach ($chem_comp_name as $val) {
               $objPHPExcel->setActiveSheetIndex(0)->setCellValue($new_abc .$chem_index,$item[$val]);
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_left); 
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_bottom);
               $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($style);
               $new_abc++;
            }
        }
        $chem_end = $chem_index + 1;

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . ++$chem_index, 'Testing item')->mergeCells('A' . $chem_index . ':B' . ++$chem_end);
         $objPHPExcel->getActiveSheet()->getStyle('C'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
         $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
     
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, 'Tensile Strength');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, 'SIZE');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, 'Yield Strength ');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' .$chem_index, 'Elongation %');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' .$chem_index, 'Flattening Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' .$chem_index, 'Flaring Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, 'Eddy Current Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' .$chem_index, 'Ultrasonic Test');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' .$chem_index, 'Hardness HRB');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L' .$chem_index, 'Hydro test (Pressure) PSI');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' .$chem_index, 'IGC - Test');

         $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom);
         $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index. ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_top_thick);
         $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);
         $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
        for($k = 'C'; $k <= $maxExcelColumn; $k++){
            $objPHPExcel->getActiveSheet()->getStyle($k.$chem_index)->applyFromArray($border_left); 
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .++$chem_index, '(MPA)');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, 'Coarser');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, 'RP 0.2%');

        $objPHPExcel->getActiveSheet()->getStyle('C' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_left);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);

            $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($custom_head);

         for($k = 'C'; $k <= $maxExcelColumn; $k++){
                        $objPHPExcel->getActiveSheet()->getStyle($k.$chem_index)->applyFromArray($border_left); 
            }

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .++$chem_index, 'Required');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' .$chem_index, $certi['Dummycertificate']['grd_spec']);
         $objPHPExcel->getActiveSheet()->getStyle('B'.$chem_index)->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' .$chem_index, $mech_req['Mechanical']['tensile_strength']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' .$chem_index, $mech_req['Mechanical']['grain_size']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' .$chem_index, $mech_req['Mechanical']['yield_strength']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' .$chem_index, $mech_req['Mechanical']['elongation']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' .$chem_index, $mech_req['Mechanical']['flattening']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' .$chem_index, $mech_req['Mechanical']['flaring']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, $mech_req['Mechanical']['eddy']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' .$chem_index, $mech_req['Mechanical']['ultrasonic']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' .$chem_index, $mech_req['Mechanical']['hardness']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L' .$chem_index, 'text 001');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' .$chem_index, $mech_req['Mechanical']['igc']);

        $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom_thick);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
        $final_end = $chem_index + ($itemcount - 1);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .++$chem_index, 'Testing Data')->mergeCells('A' . $chem_index . ':B' . ++$final_end);
        $final_abc = 'C';
        foreach ($certi['Dummycertificateitem'] as $item) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$chem_index,$item['heatno']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$chem_index,$item['tensile_strength']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$chem_index,$item['grain_size']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$chem_index,$item['yield_strength']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$chem_index,$item['elongation']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$chem_index,$item['flattening']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$chem_index,$item['flaring']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$chem_index,$item['eddy']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$chem_index,$item['ultrasonic']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$chem_index,$item['hardness']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$chem_index,$item['hydro']);    
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$chem_index,$item['igc']);
           // $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_left); 
            //   $objPHPExcel->getActiveSheet()->getStyle($new_abc .$chem_index)->applyFromArray($border_bottom);
                    for($j = $final_abc; $j <= $maxExcelColumn; $j++){
                        $objPHPExcel->getActiveSheet()->getStyle($j.$chem_index)->applyFromArray($border_left); 
                    }
               $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $chem_index)->applyFromArray($border_bottom);
               $objPHPExcel->getActiveSheet()->getStyle('A' . $chem_index . ':' . $maxExcelColumn . $chem_index)->applyFromArray($style);
            $chem_index++;
            // $final_abc++;
        }
        $final_end = $final_end + 7;
        $remarks = str_replace('<br/>', "\n ", $certi['Dummycertificate']['remarks']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' .$chem_index, 'Remarks'."\n" .$remarks)->mergeCells('A' .$chem_index.':H'.$final_end);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chem_index)->getAlignment()->setWrapText(true);
//exit;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' .$chem_index, 'Head QA.QC')->mergeCells('I' .$chem_index.':'.$maxExcelColumn.$final_end);
        //echo $final_end;exit;
        $objPHPExcel->getActiveSheet()->getStyle('I' .$chem_index.':I'.$final_end)->applyFromArray($border_left); 
               $objPHPExcel->getActiveSheet()->getStyle('A' .$chem_index.':'.$maxExcelColumn . $final_end)->applyFromArray($border_bottom_thick);
       $objPHPExcel->getActiveSheet()->getStyle('A2'.':'.$maxExcelColumn.$final_end)->applyFromArray($border_style);
        //$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$info_index, $this->requestAction('Sizes/getsizedimension/'.$item[$itemmodel]['Orderitem']['size_id'].'/'.$item[$itemmodel]['Orderitem']['length'].'/2'));
        //$maxExcelColumn = $excelcolumn; 
        /* End Set Item column name For Row 22nd and 23rd */

        ob_clean();

        /* header('Content-Type: application/vnd.ms-excel'); */

        /* if($_GET['type'] == '2'){

          //$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');

          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
          $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
          //$objPHPExcel->getDefaultStyle()->getAlignment()->setIndent(0);
          //$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

          $rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/dompdf';

          $rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
          //$rendererLibrary = 'dompdf.php';

          if (!PHPExcel_Settings::setPdfRenderer(
          $rendererName,
          $rendererLibraryPath
          //	$rendererLibrary
          )) {
          die(
          'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
          '<br />' .
          'at the top of this script as appropriate for your directory structure'
          );
          }
          header('Content-Type: application/pdf');
          header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          //$objPHPExcel->getActiveSheet()->setTitle('Orari');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
          $objWriter->setPreCalculateFormulas (false);
          $objWriter->save('php://output');
          }
          if($_GET['type'] == '1'){ */
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $reportname . ".xlsx");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        /* }
          if($_GET['type'] == '3'){

          //$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
          $rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/tcpdf';

          $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
          //$rendererLibrary = 'dompdf.php';

          if (!PHPExcel_Settings::setPdfRenderer(
          $rendererName,
          $rendererLibraryPath
          //	$rendererLibrary
          )) {
          die(
          'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
          '<br />' .
          'at the top of this script as appropriate for your directory structure'
          );
          }
          header('Content-Type: application/pdf');
          header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

          //$objPHPExcel->getActiveSheet()->setTitle('Orari');
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
          $objWriter->setPreCalculateFormulas (false);
          $objWriter->save('php://output');
          } */
        exit();
    }

    /**
     * Name : getVerified
     * Use : Verify Test Dummycertificates
     * @param int $id id of record
     * @return void
     */
    function getVerified($id) {
        $this->autoRender = false;
        $data = $this->Dummycertificate->find('first', array('conditions' => array('Dummycertificate.id' => $id), 'fields' => array('Dummycertificate.isverified,Dummycertificate.id'), 'recursive' => -1));
        //pr($data);exit;
        $verified = $data['Dummycertificate']['isverified'];
        $verified_user = $this->Auth->user('id');
        /* Check if proforma is already verified or not, if not verified than we send email and update info */
        if ($verified == 0) {
            $update = array('isverified' => 1, 'verifiedby' => $verified_user, 'verified_date' => date('Y-m-d H:i:s'));
            $this->Dummycertificate->id = $id;
            $this->Dummycertificate->save($update);
        }
        else{$this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));} 
    }
    
    /**
     * Name : generatecertificatepdf
     * Use : generate certificate pdf using tcPDF library
     * @param int $id id of record
     * @param int $reftype reftype of record
     * @return void
     */
    function generatecertificatepdf($reftype, $id = null){
        $id = base64_decode($id);
        ini_set('memory_limit', '1024M');
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);
        $this->set('middle_breadcrumb', '<li><a href="' . WEBSITE_PATH . 'dummycertificates/">Test Dummycertificate</a><i class="fa fa-circle"></i></li>');
        $this->set('pageTitle', 'View Dummy Test Certificate');
    }


    /**
     * Name: generatepdf
     * Use: generate pdf file of record
     * @param int $id id of record
     * @param int $reftype reftype of record
     * @return pdf file      
    */
    public function generatepdf($reftype, $id = null) {
        
        $this->pdfConfig = array(
            'filename' => 'invoice',
            'download' => $this->request->query('download')
        );
        
        /* Redirect if ID empty */
        if (empty($id)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $certi = $this->getcertificatedetail($reftype, $id);

        $this->request->data = $certi;
        /* Redirect if ID not exist in database */
        if (empty($certi)) {
            $this->redirect(array('controller' => 'dummycertificates', 'action' => 'index'));
        }
        $this->set('certi', $certi);

        /* Make sure the controller doesn't auto render. */
        $this->autoRender = true;
        /* Set up new view that won't enter the ClassRegistry */
        $view = new View($this, false);
        $view->layout = null;       
        $view->viewPath = 'Dummycertificates';
        ini_set('memory_limit', '1024M');
        $html = $view->render('generatepdf');
        $this->set('post',$html);
    }
}