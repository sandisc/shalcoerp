<?php
/*
 * @Controller name: Factory Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Factory management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));
class FactoriesController extends AppController {
	var $name = 'Factories';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Factory Management');
	}
	/*
   	 * Name: index
   	 * Use: Factory view.
	 */
	function index() {
        $this->set('pageTitle',' Factory List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: View
   	 * Use: Details Of Advance Factory no.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Factoryitem');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'Factorys/">Factorys</a><i class="fa fa-circle"></i></li>');
		$Factory = $this->Factory->find('first', array(
   			'conditions' => array('Factory.id' => $id ),'recursive'=> 2
		));
		$this->set('data',$Factory);
		$this->set('pageTitle',' View Factory');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','factory_alias','factory_address','division_name','division_alias','division_range','first_name','Factory.modified');
    	$order = array('Factory.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Factory->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$factories = $this->Factory->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('factories',$factories);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Factory.
	 */
	function add($id = null) {
		$id = base64_decode($id);
		$this->Factory->id = $id;
		$this->old = $this->Factory->findById($id,array('recursive'=>0));
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit Factory');
			$this->request->data = $this->Factory->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Factory');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'factories/">Factorys</a><i class="fa fa-circle"></i></li>');
		
	}	

	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Factory->set($this->request->data);	
			$response=array();
			/* set reference location if add Factory form open in modal dialog means popup */
			if(isset($this->request->data['Factory']['targetid'])){
				$response['targetid'] = $this->request->data['Factory']['targetid'];
				unset($this->request->data['Factory']['targetid']);	
			}

			if($this->Factory->validates($this->request->data)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Factory']['modifiedby'] = $logged_user;
				$id = $this->request->data['Factory']['id'];
				if(!empty($id)){
					$this->old = $this->Factory->findById($id,array('recursive'=>0));
					$factory = $this->request->data['Factory'];
					$this->Factory->save($factory);
					$Activities->addlog($this->loggedin_user_info['id'],'Factory','Add',$factory);	
				}			

				if(empty($id)) {
					$this->request->data['Factory']['createdby'] = $logged_user;
					$this->Factory->save($this->request->data); 
					$factory = $this->request->data['Factory'];
		          	$Activities->addlog($this->loggedin_user_info['id'],'Factory','Add',$factory);
				}
				if(!empty($id)) { 
					$msg = 'Factory has been Updated successfully';
					$result = array_diff_assoc($this->old['Factory'],$this->request->data['Factory']);/*Get only changed fields*/
					$Activities->addlog($this->loggedin_user_info['id'],'Factory','Edit',$result);/*Add Data in Log*/												
				}else { 
					$Activities->addlog($this->loggedin_user_info['id'],'Factory','Add',$this->request->data['Factory']);/*Add Data in Log*/	
					$msg = 'Factory has been Added successfully';
					$response['idvalue'] = $this->Factory->getLastInsertID();
					//$response['text'] = $this->request->data['Factory']['gdmm'];
				}
				$redirect = '';
				$currentpage_controller = $this->request->params['controller'];
				$refer_url = $this->referer('/', true); /*reference page url*/
				$parse_url_params = Router::parse($refer_url);
				$referencepage_controller = $parse_url_params['controller'];
				/*if(Router::url(null, true) == $this->referer()){*/
				if($currentpage_controller == $referencepage_controller){
					//$this->redirect('/Factorys/index');
					$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
				    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
				}
				$response['topic'] = 'Factory';
	            $response['status'] = 'success';
	            $response['message'] = $msg;
	            $response['redirect'] = $redirect;
	            echo json_encode($response);				
			}
			else{
                $Factory = $this->Factory->invalidFields(); 
                $response['status'] = 'error';
                $response['message'] = 'The Factory could not be saved. Please, try again.';
                $response['data'] = compact('Factory');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	
	/**
   	 * Name: delete
   	 * Use: delete Factory
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Factory->id = $id;
		$this->autoRender = false;
		$this->loadmodel('Factoryitem');		
		$data = $this->Factory->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Factory','Delete',$data['Factory']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Factory->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Factory has been Deleted successfully</div>'));
		}
		$this->Factoryitem->deleteAll(array('Factoryitem.Factoryid' => $id), true);		
		return $this->redirect(array('action' => 'index'));
	}
		
}