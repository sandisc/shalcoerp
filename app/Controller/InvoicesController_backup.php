<?php
/*
 * @Controller name: Invoice Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Invoice management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Orders');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
//App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));

App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));
class InvoicesController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Invoice Management');
	}

	/*
   	 * Name: index
   	 * Use: Invoice listing page.
	*/
	function index() {
        $this->set('pageTitle','Invoice List');
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','invoiceno','orderno','invoicedate','Client.company_name','Invoice.modified','first_name');
    	$order = array('Invoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Invoice->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$invoices = $this->Invoice->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));
		//pr($invoices);exit;
		$this->set('invoice',$invoices);
		$this->set('count',$count);
	}

	/**
   	 * Name: View
   	 * Use: View details of Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function view($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice View');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}


/**
   	 * Name: PrintInvoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function custominvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}


/**
   	 * Name: PrintInvoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function printinvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');
		$this->set('p',$this->params['p']);
	}
   /**
   	 * Name: Party Invoice
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function partyinvoice($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/**
   	 * Name: Custom Packing
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function custompacking($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Packing');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/**
   	 * Name: Party Packing
   	 * Use: View details of Print Invoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function partypacking($id = null) {
		$id = base64_decode($id);

		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}
		$this->loadmodel('Licence');		
		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		/*Fetch data of particular ID*/
		$invoice = $this->Invoice->find('first', array(
   			'conditions' => array('Invoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 3
		));
		/*Redirect if ID not exist in database*/
		if(empty($invoice)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
		$this->set('data',$invoice);
		$this->set('pageTitle','Packing');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update invioce and Unique Proforma id.
	*/
	function add($id = null) {
		$id = base64_decode($id);
		$this->set('id',$id);
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->set('pageTitle','Add Invoice');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/">Invoices</a><i class="fa fa-circle"></i></li>');
		
		$orderdata = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive' => 2)); 
		$this->set('orderdata',$orderdata);
		
		/*If Dummy Id OR Id not exists than*/
		if(empty($orderdata)){
			$this->Session->setFlash(__('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Record not exists</div>'));        			
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
				
		$invoices_id = $this->request->data['Invoice']['invoiceno'] = $this->getInvoiceNO();
		$this->set('invoice',$invoices_id);

		$this->loadmodel('Hscode');
		$hscodes = $this->Hscode->find('list',array('fields' => array('Hscode.rate','Hscode.hscode')));
		$this->set('hscodes',$hscodes);		
	}
	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->loadmodel('Licenceexport'); 
		$this->loadmodel('Licenceexportitem'); 
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
		if(!empty($this->request->data)) { 
			$this->Invoice->set($this->request->data);
			$orderid = $this->request->data['Invoice']['orderid'];
			$response=array();

			if($this->Invoice->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Invoice']['createdby'] = $logged_user;
				$this->request->data['Invoice']['modifiedby'] = $logged_user;
				$invoice = $this->request->data['Invoice'];
				$date = $this->request->data['Invoice']['invoicedate'];

				$invoice['invoicedate'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
				$invoice['signdate'] = $invoice['invoicedate'];
				$invoice['invoiceno'] = $this->getInvoiceNO();
				$invoice['fy'] = $this->getFinancialyear();
				$this->Invoice->save($invoice);

				/* Start add invoice item data*/
					$lastid = $this->Invoice->getLastInsertID();
					$response['idvalue'] = $lastid;
		            $item = $this->request->data['Invoice']['item'];   
		            $total = 0;
		           	foreach($item as $key=>$items){	
		           		$items['invoiceid'] = $lastid;
		           		$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
		           		$items['price_mt'] = $items['netprice'] / $items['qty_mt'];

		           		$total += $items['exempted'];
		           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           			$this->Invoiceitem->saveall($items);
		           			$item[$key]['id'] = $this->Invoiceitem->getLastInsertID();/*set invoiceitemid, it will be use to save data into licence export item table*/
		           		}
		           	}
				/* End add invoice item data*/
				
				/*Sorting Order based on grade*/
				$grade_filter = $this->_group_by($item,'grade_id');	
				$gradebaseweight = '';
				$grd_name = '';
				$append_grd ='';
				$final_grd_total = '';
				foreach ($grade_filter as $gradekey => $gradevalue) { //pr($gradevalue);
					$grd_name = $gradevalue[0]['grade_name'];
						$append_grd = 'Grade '.$grd_name;
						$net_weight = '';
					foreach ($gradevalue as $gradekeys => $gradevalues) { //pr($gradevalues);
						$net_weight += $gradevalues['netweight'];
					}
						$append_grd .= $net_weight.' MT <br/>';	 
						$final_grd_total[] = $append_grd;
				}
				$concat_grd = implode('',$final_grd_total);
				
		           	if($this->Invoice->id){
		           		$invoice['totalexempted'] = $total;				
		           		$invoice['gradebaseweight'] = $concat_grd;				
		           		$invoice['customgradebaseweight'] = $concat_grd;
		           		$this->Invoice->save($invoice);

		           	}
		        /*Start Update order item status and delivered quantity*/
		           	$updateorderitemdata = array();
		           	foreach($item as $items){			   		
		           		if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
		           			$items['orderitemid'] = $items['orderitemid'];
		           			$updateorderitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
		           		}
		           	}
		           	$this->loadmodel('Orderitem');
					$this->Orderitem->saveall($updateorderitemdata);
		        /*End Update order item status and delivered quantity*/
			        $orders = new OrdersController; 
			        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its status, change order status*/

			        /*Start Save data in licence export */
					foreach($item as $items){	
				   		/*check licenceid*/
						if($items['licenceid'] != 0){
					   		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
					   			/*check for invoice, particular licence id and invoice id exist in licenceexport table , reason is many invoice item has same licence so, we are insert it in only one time in licenceexport and we add multiple item in licence export with based on it reference. */
						   		$licenceexportrecord = $this->Licenceexport->find('first',array('conditions'=>array('invoiceid' => $lastid,'licenceid'=>$items['licenceid']),'fields'=>array('id'),'recursive'=>-1));
						   		if(empty($licenceexportrecord)){
							   		$licenceexport['invoiceid'] = $lastid;
							   		$licenceexport['clientid'] = $invoice['clientid'];
							   		$licenceexport['licenceid'] = $items['licenceid'];
							   		$licenceexport['createdby'] = $logged_user;
							   		$licenceexport['modifiedby'] = $logged_user;   			   		
									$this->Licenceexport->saveall($licenceexport);
									$licenceexportid = $this->Licenceexport->getLastInsertID();
								}else{
									$licenceexportid = $licenceexportrecord['Licenceexport']['id'];
								}
								/*if no licence than no licenceitem*/
								if(isset($items['licenceitemid'])){
									$lastlicenceexportid = $licenceexportid;
					           		$exportitems['licenceexportid'] = $lastlicenceexportid;
					           		$exportitems['refitemid'] = $items['id'];/*Reference of invoice item id*/
				           			$exportitems['exportqty'] = $items['qty_mt'];
				           			$exportitems['dollarvalue'] = $items['netprice'];
				           			$exportitems['licenceitemid'] = $items['licenceitemid'];
				           			$this->Licenceexportitem->saveall($exportitems);
								}
			           		}
		           		}
		           	}
		           	/*End Save data in licence export */

		        	$msg = 'Invoice has been saved successfully';
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'Invoice';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
				
			}
         	else{
	        	$Invoice = $this->Invoice->validationErrors;
	        	$response['status'] = 'error';
                $response['message'] = 'The Invoice could not be saved. Please, try again.';
                $response['data'] = compact('Invoice');
                echo json_encode($response);
	        }	
		}			
	}

	/**
   	 * Name: edit
   	 * Use: update invoice.
     * @param int $id id of record
     * @return detail record of id   	 
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}	
		$this->loadmodel('Client');
		//$this->loadmodel('Proformainvoice');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Hscode');
		$this->loadmodel('Bank');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'invoices/"> Invoices</a><i class="fa fa-circle"></i></li>');
	    $this->set('pageTitle','Edit Invoice');

		$this->Client->unbindModel(array('belongsTo' => array('User','Coordinator')));
		$this->Order->unbindModel(array('belongsTo' => array('Dispatch','User')));
		$this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem')));
		
		$this->Orderitem->bindModel(array('hasOne' => array('Invoiceitem' => array(
            'className' => 'Invoiceitem',
            'foreignKey' => 'orderitemid',
            'conditions'=>array('Invoiceitem.invoiceid' => $id)
        ))));

	    $invoice_data = $this->Invoice->find('first',array('conditions' => array('Invoice.id' => $id ),'recursive'=>3));
	    
        $this->set('invoice_data',$invoice_data);
        //pr($invoice_data);exit;

		$this->request->data = $invoice_data;
		if(empty($invoice_data)){
			$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
		}		
	}

	/*Form submit usign ajax*/
	function ajaxeditsubmit(){
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order'); 
		$this->loadmodel('Licenceexport');
		$this->loadmodel('Licenceexportitem');
		$this->autoRender = false; 
	    if(!empty($this->request->data)) {
	    	$this->Invoice->set($this->request->data);
	    	if($this->Invoice->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$id = $this->request->data['Invoice']['id'];
				$orderid = $this->request->data['Invoice']['orderid'];
				$this->request->data['Invoice']['modifiedby'] = $logged_user;
				$invoices = $this->request->data['Invoice'];
				$date = $this->request->data['Invoice']['invoicedate'];
				$invoices['invoicedate'] = date('Y-m-d',strtotime(str_replace('/', '-', $date)));
				$invoices['signdate'] = $invoices['invoicedate'];
				$this->Invoice->save($invoices);/*Save invoice data*/

				$item = $this->request->data['Invoice']['item'];
				$get_all_item  = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.invoiceid'=>$id)));
				$del_ids = $this->request->data['removal_id'];
	            if(isset($del_ids) && !empty($del_ids)){
	            	$this->deleteInvoiceitem($del_ids,$id,1);	            	
		        }
	            $sum_qty = 0;
	            $total_exempted = 0;
            	/*Save invoice items data*/
            	foreach($item as $key=>$items){
	           		if(isset($items['id']) && !empty($items['id'])){ /*Update items info which is already exist in invoice*/
	           			$items['invoiceid'] = $id;
	           			$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
	           			$items['price_mt'] = $items['netprice'] / $items['qty_mt'];
						$total_exempted += $items['exempted'];
						$sum_qty += $items['qty_mt'];
						$this->Invoiceitem->id = $items['id'];
 	           			$this->Invoiceitem->save($items);
	           		}
	           		else{/*Add items info which is already exist in order but not in invoice*/
	           			$items['invoiceid']  = $id;

	           			if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
							$items['exempted'] = $items['qty_mt'] + ($items['qty_mt'] * $items['hscode_rate']/100);
			           		$items['price_mt'] = $items['netprice'] / $items['qty_mt'];
			           		$total_exempted += $items['exempted'];
			           		if($items['qty_mtr'] != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
			           			$this->Invoiceitem->saveall($items);
								$item[$key]['id'] = $this->Invoiceitem->getLastInsertID();/*set invoiceitemid, it will be use to save data into licence export item table*/			           			
			           		}							
						}
					}
	           	}
	           	$grades_filter = $this->_group_by($item,'grade_id');	
	           	
				$gradebaseweight = '';
				$grd_name = '';
				$append_grd ='';
				$final_grd_total = '';
				foreach ($grades_filter as $gradekey => $gradevalue) { 
					$grd_name = $gradevalue[0]['grade_name'];
					$append_grd = 'Grade '.$grd_name;
					$net_weight = '';
					foreach ($gradevalue as $gradekeys => $gradevalues) { 
						$net_weight += $gradevalues['netweight'];
					}
					$append_grd .= $net_weight.' MT <br/>';	 
					$final_grd_total[] = $append_grd;
				}
				$concat_grd = implode('',$final_grd_total);

	           	if($this->Invoice->id){
	           		if($this->request->data['Invoice']['manual'] != '1'){
		           		$invoice['gradebaseweight'] = $concat_grd;				
		           		$invoice['customgradebaseweight'] = $concat_grd;
	           		}
	           		$invoice['totalexempted'] = $total_exempted; 
	           		$this->Invoice->save($invoice);
	           	}

	           	/*Start Update order item status and delivered quantity*/
	           	$updateorderitemdata = array();
	           	foreach($item as $items){	
	           		$items['orderitemid'] = $items['orderitemid'];
	           		$updateorderitemdata[] = $this->countdeliveredqty($items['orderitemid'],$items['qty']);
	           	}
	           	$this->loadmodel('Orderitem');
				$this->Orderitem->saveall($updateorderitemdata);
		        /*End Update order item status and delivered quantity*/

		        $orders = new OrdersController; 
		        $orders->checkorderstatus($orderid,$this->Auth->user('id')); /*check order item status and as per its status, change order status*/

			        /*Start Save data in licence export */
					foreach($item as $items){	
				   		$oldlicencexportitemid = '';
				   		$oldlicenceexportitem = '';
				   		$oldlicenceexport = '';
				   		$oldlicenceitemdta = '';
				        $licenceexportid = '';
				        $licenceexportrecord = array();
				        $exportitems = array();
				        $licenceexport = array();

				        $this->Licenceexport->create();
				        $this->Licenceexportitem->create();
				        if($items['licenceid'] != 0 || $items['oldlicenceid'] != 0){
					   		if(round($items['qty_mtr']) != 0 && $items['qty_mtr'] != ''){ /* ignore those items if it has zero quantity*/
								
								/*check for invoice, particular licence id and invoice id exist in licenceexport table , reason is many invoice item has same licence so, we are insert it in only one time in licenceexport and we add multiple item in licence export with based on it reference. */
							   	$licenceexportrecord = $this->Licenceexport->find('first',array('conditions'=>array('invoiceid' => $id,'licenceid'=>$items['licenceid']),'fields'=>array('id'),'recursive'=>-1));

					   			/*Check old licence number and current licence number both same than update its information*/
					   			if(!empty($licenceexportrecord) && ($items['licenceid'] === $items['oldlicenceid'])){
					   				/*udpate export qty of particular refitemid*/	
					   				$this->Licenceexportitem->updateAll(
									 	array('exportqty' => $items['qty_mt'],'dollarvalue' => $items['netprice']), 
									 	array('refitemid'=>$items['id'])
									);   		
											   				
					   			}else{
					   				/*Check old licence number and current licence number both not same than add it*/
					   				if($items['licenceid']!= 0){
						   				if(empty($licenceexportrecord)){/*Empty than, insert it*/
								   			//unset($licenceexport['id']);
								   			//$this->Licenceexport->id = '';
									   		$licenceexport['invoiceid'] = $id;
									   		$licenceexport['clientid'] = $invoices['clientid'];
									   		$licenceexport['licenceid'] = $items['licenceid'];
									   		$licenceexport['createdby'] = $logged_user;
									   		$licenceexport['modifiedby'] = $logged_user;   			   		
											$this->Licenceexport->save($licenceexport); 
											$licenceexportid = $this->Licenceexport->getLastInsertID();
										}else{/*Not Empty than, take its id for use licenceexport table entry*/
											$licenceexportid = $licenceexportrecord['Licenceexport']['id'];
										}
									}
									/*fetch old licenceexport item details based on old invoice item */
									$oldlicenceexportitem = $this->Licenceexportitem->find('first',array('conditions'=>array('refitemid' => $items['id']),'fields'=>array('id'),'recursive'=>-1));
										
									if(!empty($oldlicenceexportitem)){
										/*Update licenceexportitem  : If old entry exist in licenceexport item table than update its qty and new licence export id(based on invoice and licence)*/
					   					$this->Licenceexportitem->id = $oldlicenceexportitem['Licenceexportitem']['id'];
					   					$exportitems['licenceexportid'] = $licenceexportid;	
					   					$exportitems['licenceitemid'] = $items['licenceitemid'];
					   					$exportitems['exportqty'] = $items['qty_mt'];
								   		$exportitems['dollarvalue'] = $items['netprice'];	

										if($items['licenceid']!= 0){/*item has licence than save licenceexportitem*/
											$this->Licenceexportitem->save($exportitems);	
										}else{/*item hasnot licence than delete licenceexportitem*/
											$this->Licenceexportitem->delete($oldlicenceexportitem['Licenceexportitem']['id']);
										}								   				   		
										
									}else{ /*Add licenceexportitem s*/
						           		$exportitems['licenceexportid'] = $licenceexportid;		           		
					           			$exportitems['exportqty'] = $items['qty_mt'];
					           			$exportitems['dollarvalue'] = $items['netprice'];
					           			$exportitems['licenceitemid'] = $items['licenceitemid'];
					           			$exportitems['refitemid'] = $items['id'];		           			
					           			$this->Licenceexportitem->save($exportitems); 
									}															           						
					   			}						
			           		}
		           		}
		           	}
		           	/*fetch all licence related invoice and remove licenceexport row if its licencexport item array is empty*/
					$exportofInvoice = $this->Licenceexport->find('all',array('conditions'=>array('invoiceid' => $id),'recursive'=>1));
					foreach ($exportofInvoice as $v) {
						if(empty($v['Licenceexportitem'])){
							$this->Licenceexport->delete($v['Licenceexport']['id']);
						}
					}
		           	/*End Save data in licence export */

	           	$msg = 'Invoice has been Updated successfully';      
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));

	           	//$this->redirect('index');
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);		           	
	        }
	        else{
                $Invoice = $this->Invoice->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Invoice could not be saved. Please, try again.';
                $response['data'] = compact('Invoice');
                echo json_encode($response);
	        }
		}		
	}

	/**
   	 * Name: deleteInvoiceitem
   	 * Use: delete all invoice item of invoice, or particular invoice item of invoice
     * @param array $item_ids array of invoiceitem 
     * @param int $invoiceid id of invoice
     * @param int $type which kind of delete you wants (type=1 paricular invoice item,type=2 invoice with all invoice item)
	*/
	function deleteInvoiceitem($item_ids,$invoiceid,$type){
		$this->loadmodel('Orderitem');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Order');
		$this->loadmodel('Licenceexport');
		$this->loadmodel('Licenceexportitem');
		
		if($type == 2){
     	   $delete_invoiceitem_array = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.invoiceid' => $invoiceid)));
     	}elseif($type == 1){
			$delete_invoiceitem_array = $this->Invoiceitem->find('all',array('conditions'=>array('Invoiceitem.id IN ('.$item_ids.')')));
     	}
     	else{
     		return true;
     	}
     	
     	foreach ($delete_invoiceitem_array as $delitemvalue) {
     		/*Removing invoiceitem's qty from dlvr qty of orderitem*/
     		$rem_dlvr_qty = $delitemvalue['Orderitem']['dlvr_qty'] - $delitemvalue['Invoiceitem']['qty_mtr'];
     		if ($rem_dlvr_qty <= 0) { $itemstatus = 0; /*Pending*/}
     		elseif ($rem_dlvr_qty < $delitemvalue['Orderitem']['qty']) { $itemstatus = 1; /*Process*/}
     		elseif ($rem_dlvr_qty > $delitemvalue['Orderitem']['qty']) { $itemstatus = 2; /*close*/}

     		if($itemstatus == 0 || $itemstatus == 1){
     			/*update order status is 1 means process*/
     			$this->Order->save(array('id' => $delitemvalue['Orderitem']['orderid'], 'status' => 1));
     		}
     		$this->Orderitem->save(array('id'=>$delitemvalue['Orderitem']['id'],'dlvr_qty'=>$rem_dlvr_qty,'status'=>$itemstatus));
     		$this->Licenceexportitem->deleteAll(array('refitemid' => $delitemvalue['Invoiceitem']['id']));
     		/* we didnt delete its record from licence export table, we delete it before function close */
     		$this->Invoiceitem->delete($delitemvalue['Invoiceitem']['id']);
     	}
		/*fetch all licence related invoice and remove licenceexport row if its licencexport item array is empty*/
		$exportofInvoice = $this->Licenceexport->find('all',array('conditions'=>array('invoiceid' => $invoiceid),'recursive'=>1));
		foreach ($exportofInvoice as $v) {
			if(empty($v['Licenceexportitem'])){
				$this->Licenceexport->delete($v['Licenceexport']['id']);
			}
		}
	}

	/**
   	 * Name: countdeliveredqty
   	 * Use: count delivered quantity of particular order item
     * @param int $orderitemid orderitemid of record
     * @param decimal $qty qty of orderitem
     * @return array with orderitem id,delivered qty and status.   	 
	*/
	function countdeliveredqty($orderitemid,$qty){
		$itemdata = $this->Invoiceitem->find('all',array('conditions' => array('orderitemid'=>$orderitemid)));
		$dlvr_qty = 0;
		foreach ($itemdata as $key => $value) {
			$dlvr_qty+=$value['Invoiceitem']['qty_mtr'];
		}
		if($dlvr_qty > $qty || $dlvr_qty == $qty){
			$status = 2; //close
		}else{
			$status = 1; //process
		}
		return array('id'=>$orderitemid,'dlvr_qty'=>$dlvr_qty,'status'=>$status);
	}

	/**
   	 * Name: generatepdf
   	 * Use: generate pdf file of record
     * @param int $id id of record
     * @param int $p type of file
     * @return pdf file   	 
	*/
	public function generatepdf($p,$id = null) {
		$this->pdfConfig = array(
			'filename' => 'invoice',
			'download' => $this->request->query('download')
		);
		$this->loadmodel('Licence');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		
		$data = $this->Invoice->find('first',array('conditions'=> array('Invoice.id'=> $id),'recursive' => 3));
    	$this->set('data', $data);
    	$this->set('p', $p);/*for check request*/
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 

		$view->viewPath = 'Invoices';
		ini_set('memory_limit', '1024M');
		$html = $view->render('generatepdf');
   		$this->set('post',$html);   		
	}
	
   /**
	* Name : getInvoiceNO
	* Use : Get Unique Invoice No for Invoice
	*/
	function getInvoiceNO(){
		$invoice_id = $this->Invoice->find('first',array('fields'=>array('Invoice.invoiceno','Invoice.fy'),'recursive'=> -1,'order' => 'Invoice.id DESC'));
		$new_finan_year = $this->getFinancialyear();
		if(!empty($invoice_id) && $invoice_id['Invoice']['fy'] == $new_finan_year){
			$in_id = $invoice_id['Invoice']['invoiceno'];
			$exp_id = explode('/',$in_id);
			$inv_id = $exp_id[1] + 1;
			$year = date("Y");
			$pr_id = 'SHI/'.$inv_id.'/'.$this->getFinancialyear();
			return $pr_id;
		}else{
			$year = date("Y");
			$pr_id = 'SHI/'.'1/'.$this->getFinancialyear();
			return $pr_id;
		}
	}

  /**
	* Name : invoiceoforder
	* Use : Show Invoices of particular order
 	* @param int $orderid orderid of record
    * @return array with order and invoice details of particular order.
	*/
	function invoiceoforder($orderid){
		if (empty($orderid)) { /* If orderid is empty than */
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}		
		$this->loadmodel('Order');
		$this->loadmodel('Client');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Orderitem');
		
		$id = base64_decode($orderid);
		$this->Orderitem->bindModel(array('hasMany' => array('Invoiceitem' => array(
            'className' => 'Invoiceitem',
            'foreignKey' => 'orderitemid'
        ))));		
		$orders = $this->Order->find('first',array('conditions' => array('Order.id' => $id),'recursive'=>2));
		if(empty($orders)){/*if no invoice for particular order than */
			$this->redirect(array('controller' => 'orders', 'action' => 'index'));
		}
		$clients = $this->Client->find('first',array('conditions' =>array('Client.id' => $orders['Client']['id'])));

		$this->set('orders',$orders);
		$this->set('clients',$clients);
		//pr($orders); exit;
		$this->set('pageTitle','Invoices of Order');
		$this->set('ajaxaction','invoiceoforderlisting/'.$id);
	}

	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function invoiceoforderlisting(){
		$id = $this->params['pass'][0];/*Order id*/
    	$column = array('id','invoiceno','invoicedate','Invoice.totalqty','totalgrossweight','finalvalue','Invoice.modified','first_name');
    	$order = array('Invoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Invoice->find('count',array('conditions'=>array($res['con'],'Invoice.orderid'=>$id),'order'=>$res['order_by']));
		$invoices = $this->Invoice->find('all',array('conditions'=>array($res['con'],'Invoice.orderid'=>$id),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>2));
		$this->set('invoice',$invoices);
		$this->set('count',$count);
	}

	/*
	* Name : delete
	* Use : delete the invoice
	*/
	function delete($id = null) {
		$id = base64_decode($id);	
		$this->autoRender=false;	
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
			return true;
		}

		$this->deleteInvoiceitem('',$id,2);	 
		if ($this->Invoice->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invoice has been Deleted successfully</div>'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	/*
	* Name : export
	* Use : Export the Order data
	*/
	function export($id = null) {
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

        $this->loadmodel('Setting');
        $settings = $this->Setting->find('first');

		$this->loadmodel('Licence');  
  		$this->Licence->unbindModel(array('belongsTo' => array('User','Hscode'),'hasMany'=>array('Licenceitem')));
		$results = $this->Invoice->find('first',array('conditions'=>array('Invoice.id' => $id),'recursive'=>3));
		if($_GET['p'] == '1'){
			$reportname = 'Party-Invoice-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '0'){
			$reportname = 'Party-Packing-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '2'){
			$reportname = 'Custom-Invoice-'.$results['Invoice']['invoiceno'];
		}
		if($_GET['p'] == '3'){
			$reportname = 'Custom-Packing-'.$results['Invoice']['invoiceno'];
		}
		
		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

		$title_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_CENTER,));

		$style_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment:: HORIZONTAL_LEFT,));

		$border_style = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

		$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_right_thick = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));

		$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
		
		$style_underline = array('font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));

		if($results['Order']['Price']['sign'] == '&#8377;'){
			$sym = '₹';
		}else if($results['Order']['Price']['sign'] == '&#36'){
			$sym = '$';
		}	else if($results['Order']['Price']['sign'] == '&#8364;'){
			$sym = '€'; 
		}else {
			$sym = '£';
		}
		/* Start Set Item column name For Row 22nd and 23rd */
			/*$itemHeader , to save main table column name*/
			/*$itemSubHeader , to save column name just below main table column*/
		$excelcolumn='A';
		$itemHeader[$excelcolumn] = "Marks & Bundle.";  
		$itemSubHeader[$excelcolumn] = ''; 
		//$itemSubHeader[$excelcolumn] = 'Container No : '.$results['Invoice']['containerno'];

		if($_GET['p'] == 0 || $_GET['p'] == 3){
			$itemHeader[++$excelcolumn] = "Heat No";  $itemSubHeader[$excelcolumn] = ''; 
		}
		$product_column_key = ++$excelcolumn;
		$product_column_nxt_key = ++$excelcolumn;

		$itemHeader[$product_column_key] = "Product Name"; $itemSubHeader[$product_column_key] = ''; 
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.'22:'.$product_column_nxt_key.'22');		

		$itemHeader[$product_column_nxt_key] = ""; $itemSubHeader[$product_column_nxt_key] = ''; 
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.'23:'.$product_column_nxt_key.'23');	

		$standard_column_key = ++$excelcolumn;
		$itemHeader[$standard_column_key] = "Standard"; $itemSubHeader[$excelcolumn] = ''; 

		$grade_column_key = ++$excelcolumn;
		$itemHeader[$grade_column_key] = "Grade"; $itemSubHeader[$excelcolumn] = '';

		$gooddesc_column_key = ++$excelcolumn;
		$itemHeader[$gooddesc_column_key] = "Good Description"; $itemSubHeader[$excelcolumn] = ''; 

		$pcs_column_key = ++$excelcolumn;
		$itemHeader[$pcs_column_key] = "PCS"; $itemSubHeader[$excelcolumn] = ''; 

		$qtymt_column_key = ++$excelcolumn;
		$itemHeader[$qtymt_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = '(UNIT MT)'; 
		
		if($_GET['p'] == 0 || $_GET['p'] == 1){
			$qtymtr_column_key = ++$excelcolumn;
			$itemHeader[$qtymtr_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = '(UNIT MTR)'; 
		}
		if($_GET['p'] == 2 || $_GET['p'] == 3){
			$exempted_column_key = ++$excelcolumn;
			$itemHeader[$exempted_column_key] = "Exempted"; $itemSubHeader[$excelcolumn] = ' MATERIAL '; 
		}
		if($_GET['p'] == 1 || $_GET['p'] == 2){ 
			$price_column_key = ++$excelcolumn;
			$itemHeader[$price_column_key] = "Price"; $itemSubHeader[$excelcolumn] = ' IN ('.$sym.') '; 
			$netamount_column_key = ++$excelcolumn;
			$itemHeader[$netamount_column_key] = "Net Amount";  $itemSubHeader[$excelcolumn] = ' IN '.$sym; 
		}			
		if($_GET['p'] == 0 || $_GET['p'] == 3){
			$netweight_column_key = ++$excelcolumn;
			$itemHeader[$netweight_column_key] = "Net Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
			$grossweight_column_key = ++$excelcolumn;
			$itemHeader[$grossweight_column_key] = "Gross Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
		}
		$maxExcelColumn = $excelcolumn; 
		/* End Set Item column name For Row 22nd and 23rd */
	
	/*if($_GET['type']==1){
		$headstyleArray = array(
	    	'font'  => array(
	        	'bold'  => true,
	        	'name'  => 'Times New Roman'
	    	)
	    );
	}*/
	//if($_GET['type']==2){
		$headstyleArray = array(
	    	'font'  => array(
	        	'bold'  => true,
	        	//'color' => array('rgb' => 'FF0000'),
	        	'size'  => 6,
	        	'name'  => 'Times New Roman'
	    	)
	    );		
		$valuestyleArray = array(
	    	'font'  => array(
	        	//'bold'  => true,
	        	//'color' => array('rgb' => 'FF0000'),
	        	'size'  => 6,
	        	'name'  => 'Verdana'
	    	)
	    );
		$objPHPExcel->getDefaultStyle()->applyFromArray($valuestyleArray);
	//}
	
	/*start apply boldness in top section */	
		//$objPHPExcel->getActiveSheet()->getStyle('11')->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->getStyle('2')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('3')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('4')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('5')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E8')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('G8')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('H8')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('10')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('16')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('18')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A20:E20')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('E19')->applyFromArray($headstyleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A11')->applyFromArray($headstyleArray);
	/*end apply boldness in top section */			

	/*start apply width in top section */	
		/*$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);		
		$objPHPExcel->getActiveSheet()->getColumnDimension($product_column_key)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension($gooddesc_column_key)->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension($standard_column_key)->setWidth(18);
		if($_GET['p'] == '1' || $_GET['p'] == '2'){
			$objPHPExcel->getActiveSheet()->getColumnDimension($netamount_column_key)->setWidth(13);
		}
		if($_GET['p'] == '0' || $_GET['p'] == '3'){
			$objPHPExcel->getActiveSheet()->getColumnDimension($grossweight_column_key)->setWidth(13);	
		}*/
	/*end apply width in top section */

	/*start apply border in top section */
		
		//$objPHPExcel->getActiveSheet()->getStyle('2')->applyFromArray($style);
		$objPHPExcel->getActiveSheet()->getStyle('E3:E24')->applyFromArray($border_left); /*verticle line-spearet top portion in two part left and right*/
		$objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($style_underline);/*office add, factory add underline*/
		
		$objPHPExcel->getActiveSheet()->getStyle('E6:'.$maxExcelColumn.'6')->applyFromArray($border_top);		
		$objPHPExcel->getActiveSheet()->getStyle('E7:'.$maxExcelColumn.'7')->applyFromArray($border_bottom);
		$objPHPExcel->getActiveSheet()->getStyle('G8:G9')->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('E13:'.$maxExcelColumn.'13')->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('E19:'.$maxExcelColumn.'19')->applyFromArray($border_top);

		$objPHPExcel->getActiveSheet()->getStyle('A5:B5')->applyFromArray($style_underline);
		$objPHPExcel->getActiveSheet()->getStyle('C16:C21')->applyFromArray($border_left);
		$objPHPExcel->getActiveSheet()->getStyle('A17:D17')->applyFromArray($border_bottom);
		$objPHPExcel->getActiveSheet()->getStyle('A19:D19')->applyFromArray($border_bottom);

		$objPHPExcel->getActiveSheet()->getStyle('A10:'.$maxExcelColumn.'10')->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A16:'.$maxExcelColumn.'16')->applyFromArray($border_top);
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.$maxExcelColumn.'9')->applyFromArray($border_bottom);

		$objPHPExcel->getActiveSheet()->getStyle('A14:'.$maxExcelColumn.'14')->applyFromArray($border_top);		
		$objPHPExcel->getActiveSheet()->getStyle('A21:'.$maxExcelColumn.'21')->applyFromArray($border_bottom_thick);		
		$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.'2')->applyFromArray($title_style);			
	/*end apply border in top section */
	/*start merge cell and column in top section */
		/*start section 1*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:'.$maxExcelColumn.'2');/*top horizontal line*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E3:F3');/*invoice no*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:F4');/*invoice no, Data*/

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G3:'.$maxExcelColumn.'3');/* invoice date*/			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G4:'.$maxExcelColumn.'4');/* invoice date, Data*/			

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:B5');/*Factory address*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:B9');/*Factory address, Data*/
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A8:B8');
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:B9');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:D5');/*office address*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C6:D9');/*office address,Data*/
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C7:D7');
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C8:D8');
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C9:D9');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:'.$maxExcelColumn.'5');/*Blank , right side of office address*/

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:'.$maxExcelColumn.'6');/*P.O. No*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E7:'.$maxExcelColumn.'7');/*P.O. No, Data*/
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E7:'.$maxExcelColumn.'7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E8:G8');/*country of origin*/			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E9:G9');/*country of origin, Data*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H8:'.$maxExcelColumn.'8');/*country of final*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H9:'.$maxExcelColumn.'9');/*country of final, data*/
	/*end section 1*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A10:D10');/*consignee*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A11:D11');/*consignee name, Data*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A12:D15');/*consignee address, Data*/
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A13:D13');
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A14:D14');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E10:'.$maxExcelColumn.'10');/*Buyer address*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E11:'.$maxExcelColumn.'13');/*Buyer address,Data*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E14:'.$maxExcelColumn.'14');/*IEC Code*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E15:'.$maxExcelColumn.'15');/*IEC Code, Data*/
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E15:'.$maxExcelColumn.'15')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A16:B16');/*Pre Carriage*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A17:B17');/*Pre Carriage,Data*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A18:B18');/*Vessel No*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A19:B19');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A20:B20');/*Port of Discharge */
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A21:B21');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C16:D16');/*Receipt by pre carrrier*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C17:D17');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C18:D18');/*Port of Loading*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C19:D19');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C20:D20');/*Place of Delivery*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C21:D21');
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E16:'.$maxExcelColumn.'16');/*Other Refs*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E17:'.$maxExcelColumn.'18');/*Other Refs,Data*/

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E19:F19');/*TODelivery*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G19:'.$maxExcelColumn.'19');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E20:F20');/* TOPayment*/
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G20:'.$maxExcelColumn.'20');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E21:'.$maxExcelColumn.'21');/*blank after Terms of Payment data*/

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'2', 'INVOICE');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'3', 'EXPORTER');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'4', 'SHALCO INDUSTRIES PVT. LTD.');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'5', 'Factory Add');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'6', $settings['Setting']['factory_address']);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'5', 'Office Add');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'6', $settings['Setting']['office_address']);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'3', 'INVOICE NO');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.'3', 'DATE');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'4', $results['Invoice']['invoiceno']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.'4', $this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Invoice']['invoicedate'])));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'6', 'P.O. NO. & DATE:');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'7', $results['Order']['clientpo']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'8', 'COUNTRY OF ORIGIN OF GOODS');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'9', $results['Invoice']['fromcountry']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.'8', 'COUNTRY OF FINAL DESTINATION');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.'9', $results['Invoice']['tocountry']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'10', 'CONSIGNEE');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'11', $results['Order']['Client']['company_name']);
			/*$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'12', str_replace(array('<br/>','</br>'),"\n ",$results['Order']['Client']['address1']));*/
			

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'12', str_replace(array('<br/>','</br>'),"\n ",
			searchFromAddress($results['Order']['billaddress_id'], $results['Order']['Client']['Address'])));

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'10', "BUYER'S (IF OTHER THAN CONSIGNEE)");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'11', str_replace(array('<br/>','</br>'),"\n ",$results['Invoice']['buyers']));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'14', 'IEC CODE NO');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'15', $results['Order']['Bank']['iec_code']);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'16', 'PRE CARRIAGE BY');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'17', $results['Invoice']['carriageby']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'16', 'PLACE OF RECEIPT BY PRE-CARRIER');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'17', $results['Invoice']['receiptplace']);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'18', 'VESSEL/FLIGHT NO.');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'19', $results['Invoice']['flightno']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'18', 'PORT OF LOADING');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'19', $results['Invoice']['loadingport']);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'20', 'PORT OF DISCHARGE:');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'21', $results['Invoice']['dischargeport']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'20', 'PLACE OF DELIVERY:');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.'21', $results['Invoice']['loadingport']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'16', 'OTHER REFERENCE(S)');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'17', $results['Invoice']['otherref']);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'19', 'TERMS OF DELIVERY :');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.'19', $results['Order']['tod']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.'20', 'TERMS OF PAYMENT :');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.'20', $results['Order']['top']);
		
			/* Start Print Item column name For Row 22nd and 23rd */
			foreach ($itemHeader as $itemHeaderkey => $itemHeadervalue) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($itemHeaderkey.'22', $itemHeadervalue);
				$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.'22')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.'22')->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle($itemHeaderkey.'22')->applyFromArray($border_right);
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($itemHeaderkey.'22')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			foreach ($itemSubHeader as $itemSubHeaderkey => $itemSubHeadervalue) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($itemSubHeaderkey.'23', $itemSubHeadervalue);
				$objPHPExcel->getActiveSheet()->getStyle($itemSubHeaderkey.'23')->applyFromArray($border_right);
				$objPHPExcel->getActiveSheet()->getStyle($itemSubHeaderkey.'23')->applyFromArray($border_bottom_thick);
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($itemHeaderkey.'23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			/* End Print Item column name For Row 22nd and 23rd */

			$order_items = $results['Order']['Orderitem'];
            $in_items = $results['Invoiceitem'];
            $i = 0; $j=0;
			$dataArray = array();
			$idx = '1';
			$index = '1';						
			$k = 0;
	        foreach($order_items as $v) { 
	            if(isset($in_items[$i]['orderitemid'])){
	                if($in_items[$i]['orderitemid'] == $v['id']){ 
            			
						$dataArray[$index][$k] = $in_items[$i]['bundleno'];
						if($_GET['p'] == 0 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = $in_items[$i]['heatnumber']; 						
						}						
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['productname']; 
						$dataArray[$index][++$k] = ''; 
						 						 
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Standard']['standard_name'];
						$dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Grade']['grade_name'];
						$dataArray[$index][++$k] = setGDmmformat($order_items[$j]['Productcategory']['Size']['gdmm'],$order_items[$j]['length']);
						$dataArray[$index][++$k] = $in_items[$i]['pcs'];
						//if($_GET['p'] != 2){
							 $dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mt']);
						//}
						if($_GET['p'] == 0 || $_GET['p'] == 1){
						$dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mtr']);
						}
						if($_GET['p'] == 2 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = decimalQty($in_items[$i]['exempted']);
						}

						if($_GET['p'] == 1){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['price']);
						}
						if($_GET['p'] == 2){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['price_mt']);
						}
						if($_GET['p'] == 1 || $_GET['p'] == 2){							
							$dataArray[$index][++$k] = decimalPrice($in_items[$i]['netprice']);
						}
						if($_GET['p'] == 0 || $_GET['p'] == 3){
							$dataArray[$index][++$k] = decimalWeight($in_items[$i]['netweight']);
							$dataArray[$index][++$k] = decimalWeight($in_items[$i]['grossweight']);	
						}	               

            		++$i; 
					$k = 0;
					$idx++;
					$index++;            		                  
                   } 
                   ++$j;
                }
            }
			$id = '24';
			$size = count($dataArray);

			/*Start add Item value in excell*/
			foreach ($dataArray as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells($product_column_key.$id.':'.$product_column_nxt_key.$id);/*for merge prodctname and its neighbour column value*/				
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_bottom);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_right);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, $value[$i]);
					}
					else {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, '');
					}
				}
				$id++;
			}
			/*End add Item value in excell*/
		
		/*END Middle Item part */
			
			/*START Footer price related section and total of item like total pcs,qty etc  */
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'uploads/signature/logo1.png';
			$objDrawing->setPath($path);
			
			$nxt_val = $size + 23;
			$sum = 14;
			/*if($_GET['p'] == '1'){
				if($results['Invoice']['fob'] == 0){
					$sum = $sum - 1;
				}
				if(empty($results['Invoice']['exwork'])){
					$sum = $sum - 1;
				}
				if($results['Invoice']['frieght'] == 0){
					$sum = $sum - 1;	
				}				
				if(empty($results['Invoice']['insurance'])){
					$sum = $sum - 1;
				}
				if(empty($results['Invoice']['discount'])){
					$sum = $sum - 1;
				}
				if($results['Invoice']['advancereceive'] == 0){
					$sum = $sum - 1;
				}
				$total = $size + 23 + $sum;		
			}
			if($_GET['p'] == '0'){
				$total = $size + 23 + 5;	
			}
			if($_GET['p'] == '2'){
				$total = $size + 23 + 10;	
			}
			if($_GET['p'] == '3'){
				$total = $size + 23 + 8;	
			}*/
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top_thick);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom_thick);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_right);
		if($_GET['p'] == 1 || $_GET['p'] == 2){
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_left);
		}
		if($_GET['p'] == 0){
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($qtymtr_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($netweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($grossweight_column_key.$nxt_val)->getNumberFormat()->setFormatCode('0.000');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymtr_column_key.$nxt_val,$results['Invoice']['totalqty']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netweight_column_key.$nxt_val,$results['Invoice']['netweight']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($grossweight_column_key.$nxt_val,$results['Invoice']['grossweight']);
		}

		$amount_field  = getdelivery_type($results['Order']['delivery_type']);/*fetch amount related field from invoice table based on delivery type*/

		if($_GET['p'] == 1){	
			$objPHPExcel->getActiveSheet()->getStyle('F'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($qtymtr_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymtr_column_key.$nxt_val,$results['Invoice']['totalqty']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$nxt_val,strtoupper($amount_field).' Value');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$nxt_val,$results['Invoice'][$amount_field]);			
		}

		if($_GET['p'] == 2){
			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($exempted_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($exempted_column_key.$nxt_val,$results['Invoice']['totalexempted']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$nxt_val,strtoupper($amount_field).' Value');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$nxt_val,$results['Invoice'][$amount_field]);					
		}
		if($_GET['p'] == 3){

			$objPHPExcel->getActiveSheet()->getStyle($pcs_column_key.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle($qtymt_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($exempted_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($netweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->getStyle($grossweight_column_key.$nxt_val)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.000');
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$nxt_val,'Total');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($pcs_column_key.$nxt_val,$results['Invoice']['totalpcs']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($qtymt_column_key.$nxt_val,$results['Invoice']['totalqtymt']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($exempted_column_key.$nxt_val,$results['Invoice']['totalexempted']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netweight_column_key.$nxt_val,$results['Invoice']['netweight']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($grossweight_column_key.$nxt_val,$results['Invoice']['grossweight']);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'Exempted Materials Stainless Steel Seamless Pipes/Tubes (Hot/Finished)');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'We intend to claim rewards under Merchandise Exports From India Scheme (MEIS)"');
	
		}

		if($_GET['p'] == 1 || $_GET['p'] == 2){

			/*$amount_section = $this->requestAction(
                array('controller' => 'invoices', 'action' => 'dependentAmountField'),
                array("invoice" => $results['Invoice'],'delivery_type'=>$results['Order']['delivery_type'])
            );*/
			$amount_section = dependentAmountField($results['Invoice'],$results['Order']['delivery_type']);

			/* Start Print footer amount related section */
			foreach ($amount_section as $amount_sectionvalue) {
				$index = ++$nxt_val;
				/*applying numberformat to display specified decimalpoint, if number is integer */

				$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$index)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$index)->applyFromArray($border_left)->getNumberFormat()->setFormatCode('0.00');
				$objPHPExcel->getActiveSheet()->getStyle($price_column_key.$index)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle($netamount_column_key.$index)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$index)->applyFromArray($border_top);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($price_column_key.$index,$amount_sectionvalue['label']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$index.':'.$maxExcelColumn.$index)->applyFromArray($border_bottom);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($netamount_column_key.$index,$amount_sectionvalue['value']);
			}
			/* End Print footer amount related section */			
			$num = $this->requestAction('App/numtowords/'.$results['Invoice']['finalvalue'].'/'.$results['Order']['Price']['fullform']);
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top_thick);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,$num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
		}

		/*bottom border of amount in words value*/
		$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom_thick);
		
			/*blank cell*/
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'')->mergeCells('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			
			

			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'Container No.')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['containerno'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Signature & Date '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Invoice']['signdate'])))->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);
			
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_top_thick);
			$st1 = $nxt_val;

			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL PACKAGES')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalpackage'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'FOR SHALCO INDUSTRIES PVT. LTD')->mergeCells('H'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val);			

			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL NET WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalnetweight'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$nxt_val.':G'.$nxt_val)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$st2 = $nxt_val;

			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL GROSS WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalgrossweight'])->mergeCells('C'.$nxt_val.':G'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$nxt_val.':G'.$nxt_val)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$gradebaseweight = showAllGradeWeight($results['Invoice']['gradebaseweight'],$results['Invoice']['customgradebaseweight']);
			$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
			$rw_grd = $nxt_val + 3;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':G'.$rw_grd);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,$gradebaseweight);
			$st3 = $nxt_val;
		
			$objPHPExcel->getActiveSheet()->getStyle('H'.$st1.':H'.$rw_grd)->applyFromArray($border_left_thick);	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$st2.':'.$maxExcelColumn.''.$rw_grd);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$st2,'DIRECTOR');
					
			$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getActiveSheet()->getStyle('A2:A'.$nxt_val)->applyFromArray($border_left_thick);
$objPHPExcel->getActiveSheet()->getStyle($maxExcelColumn.'2:'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_right_thick);
$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_bottom_thick);

//$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.''.$nxt_val)->applyFromArray($styleArray);			
			//$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.''.$nxt_val)->applyFromArray($border_style);
			//$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.''.$total)->applyFromArray($border_style);
			//$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($style_left);
		/*if($_GET['p'] == 1 || $_GET['p'] == 2){
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($border_left_thick);
		}
		if($_GET['p'] == 0 || $_GET['p'] == 3){
			//$objPHPExcel->getActiveSheet()->getStyle('I'.$nxt_val.':K'.$nxt_val)->applyFromArray($border_bottom_thick);
		}
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Signature & Date');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$nxt_val,$results['Invoice']['signdate']);

		$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($border_left_thick);

		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL NET WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalnetweight']);
		$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($style_left);

		$objDrawing->setCoordinates('H'.$nxt_val);
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getStyle(++$nxt_val)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->applyFromArray($border_left_thick);

		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$nxt_val,'TOTAL GROSS WEIGHT')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['totalgrossweight']);

		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Party Gradebase Total')->mergeCells('A'.$nxt_val.':B'.$nxt_val);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Invoice']['gradebaseweight']);
		
		//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$nxt_val,$results['Invoice']['totalgrossweight']);
		$objPHPExcel->getActiveSheet()->getStyle('A2:'.$maxExcelColumn.''.$total)->applyFromArray($border_style);
			*/
	
			ob_clean();

			/*header('Content-Type: application/vnd.ms-excel');*/

			if($_GET['type'] == '2'){

				//$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
				
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
				$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
				//$objPHPExcel->getDefaultStyle()->getAlignment()->setIndent(0);
				//$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				
				$rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/dompdf';

				$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
				//$rendererLibrary = 'dompdf.php';

				if (!PHPExcel_Settings::setPdfRenderer(
				 	$rendererName,
				 	$rendererLibraryPath
				 //	$rendererLibrary
				)) {
				die(
				 'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
				 '<br />' .
				 'at the top of this script as appropriate for your directory structure'
				 );
				}
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
			
				//$objPHPExcel->getActiveSheet()->setTitle('Orari');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');	
				$objWriter->setPreCalculateFormulas (false);
				$objWriter->save('php://output');			
			}
			if($_GET['type'] == '1'){ 
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename='.$reportname.".xlsx");
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');
			}
			/*if($_GET['type'] == '3'){

				//$objPHPexcel = PHPExcel_IOFactory::load('C:\Users\Nikunj\AppData\Local\Temp\Party-Invoice-SHI_1_16-17.xlsx');
				$rendererLibraryPath = dirname(dirname(__FILE__)).'/Vendor/tcpdf';

				$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
				//$rendererLibrary = 'dompdf.php';

				if (!PHPExcel_Settings::setPdfRenderer(
				 	$rendererName,
				 	$rendererLibraryPath
				 //	$rendererLibrary
				)) {
				die(
				 'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
				 '<br />' .
				 'at the top of this script as appropriate for your directory structure'
				 );
				}
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment;filename="'.$reportname.'.pdf"'); //tell browser what's the file name
				header('Cache-Control: max-age=0'); //no cache
			
				//$objPHPExcel->getActiveSheet()->setTitle('Orari');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');	
				$objWriter->setPreCalculateFormulas (false);
				$objWriter->save('php://output');			
			}*/		
			exit();
	}

	/* 
	* Name :Shopping info
	* Use : Update Shopping info 
	*/
	function shoppinginfo(){
	    if(empty($_POST['id'])) {
	    	$this->redirect(array('controller' => 'invoices', 'action' => 'index'));
	    }
	    $this->autoRender = false; 
	    $logged_user_id = $this->Auth->user('id');
	    $date = date('Y-m-d', strtotime(str_replace('/','-', $_POST['sb_date'])));
	    $modified_date = date('Y-m-d H:i:s');
	    $status_update  = array('sbdate' => $date, 'sbno'=>$_POST['sb_no'],'shippedby'=>$logged_user_id,'shippeddate'=>$modified_date,'isshipped'=>1); 
	    $this->Invoice->id =$_POST['id'];
	    $this->Invoice->save($status_update);
	}

	/* 
	* Name : shoppingreminder
	* Use : Cron job for Email Reminder about fill shipping data of particular invoice after 10 days
	*/
	function shoppingreminder(){
	  	$this->autoRender = false;
	  	$data = $this->Invoice->find('all',array('conditions'=>array('TIMESTAMPDIFF(DAY,Invoice.created, NOW()) >' => 10,'Invoice.isshipped !='=>1)));
		foreach($data as $user_email) {
	  		$emails[] = $user_email['User']['email'];
	  	}
	  	if(!empty($data)){
	   		foreach($data as $remindemail){
	       		$emails[] = $remindemail['User']['email'];
	       		$Email = new CakeEmail();
			    $Email->to($emails);
			    $Email->subject('Reminder Shopping info update.');
			    $Email->from(array('shalco@gmail.com' => 'My Site'));
			    $Email->emailFormat('html');
			    $Email->send('Hello Sir,
						Please update you Shopping update info ');
	   		}
	  	}
	}	
}