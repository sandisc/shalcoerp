<?php
/*
 * @Controller name: Client Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to client management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class ReportsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login','logout');
		$this->set('mainTitle','Report Management');
	}
	function index() {
        $this->set('pageTitle',' Report List');
        $this->set('report',$this->Report->find('all',array('fields' => array('id','reportname'))));
        $this->set('ajaxaction','ajaxlisting');

	}
    /**
     * Name: Listing with Ajax
     * Use: View,Multiple search,Multiple delete From All Records.
    */
    function ajaxlisting(){
        $column = array('id','reportname','Report.modified','User.first_name',);
        $order = array('Report.modified' => 'desc');  
        $res = $this->datatable_append($order,$column); 
        $count = $this->Report->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
        $reports = $this->Report->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
        $this->set('report',$reports);
        $this->set('count',$count);
    }
	function countgradebasesale(){
		$this->set('pageTitle',' Gradebase Sale');
		$this->loadmodel('Gradetag');
		$this->loadmodel('Client');
		$gradetag = $this->Gradetag->find('list',array('fields'=>array('Gradetag.id','Gradetag.gradetagname')));
		$client = $this->Client->find('list',array('fields'=>array('Client.id','Client.company_name')));
		$this->set('gradetag',$gradetag);
		$this->set('client',$client);

	}
	function gradebasesale(){
		$this->autoRender = false;
		$this->loadmodel('Invoice');
		$this->loadmodel('Grade');
		$this->loadmodel('Gradetag');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
		$this->loadmodel('Invoiceitem');
		//pr($_POST);
		$grade_tag_id = implode(",",$_POST['data']['Gradebasesale']['gradetag_id']);
		$year = $_POST['data']['Gradebasesale']['year'];
        $query = '';
       
        if(!empty($_POST['data']['Gradebasesale']['client'])){
            $im_client = implode(",",$_POST['data']['Gradebasesale']['client']);
            $query .= ' AND (so1.bill_to IN ('.$im_client.'))';
        }
         if(!empty($_POST['data']['Gradebasesale']['year'])){
            $im_year = implode(",",$_POST['data']['Gradebasesale']['year']);
            if($_POST['data']['Gradebasesale']['type'][0] == 1 && !isset($_POST['data']['Gradebasesale']['type'][1]))
            {            
                $query .= ' AND (si1.fy IN ('.$im_year.'))';
            }
            if($_POST['data']['Gradebasesale']['type'][0] == 2){            
                $query .= ' AND (sc1.fy IN ('.$im_year.'))';
            }
            
        }
       // echo $query;exit;
		$client = $_POST['data']['Gradebasesale']['client'];

		if($_POST['data']['Gradebasesale']['type'][0] == 1 && !isset($_POST['data']['Gradebasesale']['type'][1])){
                $sql = $this->Gradetag->query('select sg.gradetagname as Gradetag,SUM(si.qty_mtr) as qty_mtr from sr_gradetags as sg INNER JOIN sr_grades as sga on sg.id = sga.gradetagid INNER JOIN sr_orderitems as so on so.grade_id = sga.id INNER JOIN sr_orders as so1 on so1.id = so.orderid LEFT JOIN sr_invoiceitems as si on si.orderitemid = so.id INNER JOIN sr_invoices as si1 on si1.orderid = so1.id INNER JOIN sr_invoices as si2 on si2.id = si.invoiceid WHERE (( sg.id IN ('.$grade_tag_id.')) '.$query.') GROUP BY sg.id');
		}else if($_POST['data']['Gradebasesale']['type'][0] == 2){
                $sql = $this->Gradetag->query('select sg.gradetagname as Gradetag,so1.id as order_id,SUM(sc.qty_mtr) as qty_mtr from sr_gradetags as sg INNER JOIN sr_grades as sga on sg.id = sga.gradetagid INNER JOIN sr_orderitems as so on so.grade_id = sga.id INNER JOIN sr_orders as so1 on so1.id = so.orderid LEFT JOIN sr_chalanitems as sc on sc.orderitemid = so.id INNER JOIN sr_chalans as sc1 on sc1.orderid = so1.id INNER JOIN sr_chalans as sc2 on sc2.id = sc.chalanid WHERE (( sg.id IN ('.$grade_tag_id.')) '.$query.') GROUP BY sg.id');
    	} else{
        //    echo "dfs";exit;
                $sql = $this->Gradetag->query('select gradetagname as Gradetag,SUM(si.qty_mtr) as qty_mtr from sr_gradetags as sg INNER JOIN sr_grades as sga on sg.id = sga.gradetagid INNER JOIN sr_orderitems as so on so.grade_id = sga.id INNER JOIN sr_orders as so1 on so1.id = so.orderid LEFT JOIN sr_invoiceitems as si on si.orderitemid = so.id INNER JOIN sr_invoices as si1 on si1.orderid = so1.id INNER JOIN sr_invoices as si2 on si2.id = si.invoiceid WHERE (( sg.id IN ('.$grade_tag_id.')) '.$query.') GROUP BY sg.id
                    UNION 
                    select gradetagname as Gradetag,SUM(sc.qty_mtr) as qty_mtr from sr_gradetags as sg INNER JOIN sr_grades as sga on sg.id = sga.gradetagid INNER JOIN sr_orderitems as so on so.grade_id = sga.id INNER JOIN sr_orders as so1 on so1.id = so.orderid LEFT JOIN sr_chalanitems as sc on sc.orderitemid = so.id INNER JOIN sr_chalans as sc1 on sc1.orderid = so1.id INNER JOIN sr_chalans as sc2 on sc2.id = sc.chalanid WHERE (( sg.id IN ('.$grade_tag_id.')) '.$query.') GROUP BY sg.id');

        }

		/*$grade_tag = $this->Gradetag->find('all', array(
            'joins' => array(
                array(
                    'table' => 'grades',
                    'prefix' => 'sr',
                    'alias' => 'Grade',
                    'type' => 'INNER',
                    'conditions' =>
                     array(
                        ' Gradetag.id = Grade.gradetagid',
                    )
                ),
             
                array(
                    'table' => 'standards',
                    'prefix' => 'sr',
                    'alias' => 'Standard',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Standard.id = Grade.standard_id',
                    )
                ),
                array(
                    'table' => 'productcategories',
                    'prefix' => 'sr',
                    'alias' => 'Productcategory',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Productcategory.id = Standard.procatid',
                    )
                ),
                  array(
                    'table' => 'producttaxonomies',
                    'prefix' => 'sr',
                    'alias' => 'Producttaxonomy',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Producttaxonomy.id = Productcategory.producttaxonomy_id',
                    )
                ),
            'fields' => array('Gradetag.gradetagname'),
            'group' => 'Gradetag.id',
            'conditions' => array('Producttaxonomy.id IN ('.$fra.')'),
            
        ));		*/
    
        if($_POST['data']['Gradebasesale']['save'] == 1){
        	$reports = $this->request->data['Gradebasesale'];
        	$reports['reportname'] = $this->request->data['Gradebasesale']['reportname'];
        	$reports['created'] = date('Y-m-d H:i:s');
   			$reports['createdby'] = $this->Auth->user('id');   			
   			$this->Report->save($reports);

        }
        $html = '<table class="table table-bordered table-striped table-condensed flip-content" id="report_id">
                <thead class="flip-content portlet box green">
                      <tr>
		              	<th width="25%">#</th>
		                <th width="25%">Gradetag Name</th>
		                <th width="25%">Total Qty MTR</th>
		                <th width="25%">Action</th>
		            </tr>
                  </thead>';
         $i = 1;
        foreach($sql as $grade_row => $val){
            
        	$html .= '<tr>';
        	$html .= '<td>'.$i.'</td>';
        	$html .= '<td>'.$val[0]['Gradetag'].'</td>';
        	$html .= '<td>'.$val[0]['qty_mtr'].'</td>';
        	$html .= '<td><a href="" class="">View</td>';
        	$html .= '</tr>';
        	$i++;
        }
         $html .='</tbody>
            </table>';
            echo $html;die;
	}
    public function gradesizesalesdetails(){
        $this->set('pageTitle',' Gradesize Details');
        $this->loadmodel('Gradetag');
        $this->loadmodel('Producttaxonomy');
        $this->loadmodel('Client');
        $producttaxonomy = $this->Producttaxonomy->find('list',array('fields'=>array('Producttaxonomy.id','Producttaxonomy.name')));
        $this->set('producttaxonomy',$producttaxonomy);
        $client = $this->Client->find('list',array('fields'=>array('Client.id','Client.company_name')));
        $this->set('client',$client);
    }
    public function GetGrades(){
        $this->autoRender = false;
        $this->loadmodel('Product');
        $this->loadmodel('Standard');
        $this->loadmodel('Grade');
        $this->loadmodel('Gradetag');
       
    //    pr($_POST['final']);exit;
        if(!empty($_POST['final'])){
            $procat_id = $_POST['final'];
        }else{

            $procat_id = 0;
        }
        $options = array();
        $options['joins'] = array(
                array(
                    'table' => 'grades',
                    'prefix' => 'sr',
                    'alias' => 'Grade',
                    'type' => 'INNER',
                    'conditions' =>
                     array(
                        ' Gradetag.id = Grade.gradetagid',
                    )
                ),
            
                array(
                    'table' => 'standards',
                    'prefix' => 'sr',
                    'alias' => 'Standard',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Standard.id = Grade.standard_id',
                    )
                ),
                array(
                    'table' => 'productcategories',
                    'prefix' => 'sr',
                    'alias' => 'Productcategory',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Productcategory.id = Standard.procatid',
                    )
                ),
                array(
                    'table' => 'producttaxonomies',
                    'prefix' => 'sr',
                    'alias' => 'Producttaxonomy',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Producttaxonomy.id = Productcategory.producttaxonomy_id',
                    )
                )
            );
        $options['conditions'] = array(
                  'Producttaxonomy.id IN ('.$procat_id.')',

        );
        $options['group'] = 'Gradetag.id';
        $options['fields'] = 'Gradetag.id,Gradetag.gradetagname';
        $grade_tag = $this->Gradetag->find('all', $options);
        $html = '';
        $html .= '<label class="control-label col-md-4">Select Gradetag<span class="required" aria-required="true"> * </span></label>';
        $html .= '<div class="col-md-8 checkbo_container"><ul>';
        foreach($grade_tag as $grades){
            $html .= '<li><input type="checkbox" name="data[Gradebasesalesdetails][gradetag][]" value="'.$grades['Gradetag']['id'].'" id="GradebasesalesdetailsYear" type="checkbox">'.$grades['Gradetag']['gradetagname'].'</li>';
                }
        $html .='</ul><span class="help-block"><br></span>';
        echo $html;die;
    }
    public function gradebasesales(){
        $this->loadmodel('Product');
        $this->loadmodel('Standard');
        $this->loadmodel('Grade');
        $this->loadmodel('Gradetag');
        $this->loadmodel('Invoice');
        $this->loadmodel('Invoiceitem');
        $this->loadmodel('Order');
        $this->loadmodel('Orderitem');
        $this->loadmodel('Chalan');
        $this->loadmodel('Chalanitem');
        $this->loadmodel('Size');
        if(isset($_POST['data']['Gradebasesalesdetails']['year']) && !empty($_POST['data']['Gradebasesalesdetails']['year'])){
            $finalcial_year = implode("','", $_POST['data']['Gradebasesalesdetails']['year']);    
        }
        $producttaxonomy_row = implode(",",$_POST['data']['Gradebasesalesdetails']['producttaxonomy']);
        if(isset($_POST['data']['Gradebasesalesdetails']['gradetag']) && !empty($_POST['data']['Gradebasesalesdetails']['gradetag'])){
          
            $gradetag_row = implode(",",$_POST['data']['Gradebasesalesdetails']['gradetag']);    
        }
        if(isset($_POST['data']['Gradebasesalesdetails']['from_date']) && !empty($_POST['data']['Gradebasesalesdetails']['from_date']) && isset($_POST['data']['Gradebasesalesdetails']['to_date']) && !empty($_POST['data']['Gradebasesalesdetails']['to_date']))
        {
            $date_range_from = date('Y-m-d',strtotime($_POST['data']['Gradebasesalesdetails']['from_date']));
            $date_range_to = date('Y-m-d',strtotime($_POST['data']['Gradebasesalesdetails']['to_date']));
        }
        if(isset($_POST['data']['Gradebasesalesdetails']['client']) && !empty($_POST['data']['Gradebasesalesdetails']['client'])){
            $client_id = implode(",",$_POST['data']['Gradebasesalesdetails']['client']);
        }
        if(isset($_POST['data']['Gradebasesalesdetails']['from_od']) && !empty($_POST['data']['Gradebasesalesdetails']['from_od']) && isset($_POST['data']['Gradebasesalesdetails']['to_od']) && !empty($_POST['data']['Gradebasesalesdetails']['to_od'])){
            $from_od = $_POST['data']['Gradebasesalesdetails']['from_od'];
            $to_od = $_POST['data']['Gradebasesalesdetails']['to_od'];
        }

        if(isset($_POST['data']['Gradebasesalesdetails']['from_wt']) && !empty($_POST['data']['Gradebasesalesdetails']['from_wt']) && isset($_POST['data']['Gradebasesalesdetails']['to_wt']) && !empty($_POST['data']['Gradebasesalesdetails']['to_wt'])){
            $from_wt = $_POST['data']['Gradebasesalesdetails']['from_wt'];
            $to_wt = $_POST['data']['Gradebasesalesdetails']['to_wt'];
        }
        if($_POST['data']['Gradebasesalesdetails']['bill_type'] == 1){
            $table = 'invoices';
            $sub_table = 'invoiceitems';
            $alias = 'Invoice';
            $subalias = 'Invoiceitem';
            $invoiceno = 'invoiceno';
            $invoiceid = 'invoiceid';
            $invoicedate = 'invoicedate';
        }else{
            $table = 'chalans';
            $sub_table = 'chalanitems';
            $alias = 'Chalan';
            $subalias = 'Chalanitem';
            $invoiceno = 'chalanno';
            $invoiceid = 'chalanid';
            $invoicedate = 'chalandate';
        }    

        $options = array();
        $options['joins'] = array(
                array(
                    'table' => 'grades',
                    'prefix' => 'sr',
                    'alias' => 'Grade',
                    'type' => 'INNER',
                    'conditions' => array(
                        ' Gradetag.id = Grade.gradetagid',
                    )
                ),
                array(
                    'table' => 'orderitems',
                    'prefix' => 'sr',
                    'alias' => 'Orderitem',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Orderitem.grade_id = Grade.id',
                    )
                ),
                array(
                    'table' => 'orders',
                    'prefix' => 'sr',
                    'alias' => 'Order',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Order.id = Orderitem.orderid',
                    )
                ),                
                array(
                    'table' => 'sizes',
                    'prefix' => 'sr',
                    'alias' => 'Size',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Size.id = Orderitem.size_id',
                    )
                ),
                array(
                    'table' => 'clients',
                    'prefix' => 'sr',
                    'alias' => 'Client',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Client.id = Order.bill_to',
                    )
                ),
                array(
                    'table' => $sub_table,
                    'prefix' => 'sr',
                    'alias' => $subalias,
                    'type' => 'INNER',
                    'conditions' => array(
                        $subalias.'.orderitemid = Orderitem.id',
                    )
                ),
                array(
                    'table' => $table,
                    'prefix' => 'sr',
                    'alias' => $alias,
                    'type' => 'INNER',
                    'conditions' => array(
                        $alias.'.orderid = Order.id',
                    )
                ),
                array(
                    'table' => $table,
                    'prefix' => 'sr',
                    'alias' => $alias.'2',
                    'type' => 'INNER',
                    'conditions' => array(
                        $alias.'2.id = '.$subalias.'.'.$invoiceid,
                    )
                ),
                array(
                    'table' => 'standards',
                    'prefix' => 'sr',
                    'alias' => 'Standard',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Standard.id = Grade.standard_id',
                    )
                ),
                array(
                    'table' => 'productcategories',
                    'prefix' => 'sr',
                    'alias' => 'Productcategory',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Productcategory.id = Standard.procatid',
                    )
                ),
                array(
                    'table' => 'producttaxonomies',
                    'prefix' => 'sr',
                    'alias' => 'Producttaxonomy',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Producttaxonomy.id = Productcategory.producttaxonomy_id',
                    )
                ),
        /*        array(
                    'table' => 'sizes',
                    'prefix' => 'sr',
                    'alias' => 'Size',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Size.producttaxonomy_id = Producttaxonomy.id',
                    )
                )*/
            );
         $options['conditions'] = 'Producttaxonomy.id IN ('.$producttaxonomy_row.')';
        if(isset($finalcial_year) && !empty($finalcial_year)){
          $options['conditions'] .= " AND ".$alias.".fy IN ('".$finalcial_year."')";
        
        }
        if(isset($date_range_to) && !empty($date_range_to) && isset($date_range_from) && !empty($date_range_from)){
           $options['conditions'] .= ' AND '.$alias.'.'.$invoicedate.'>= "'.$date_range_from.'"
            AND '.$alias.'.'.$invoicedate.'<= "'.$date_range_to.'"';           
        }
        if(!empty($client_id)){
            $options['conditions'] .= ' AND Order.bill_to IN ('.$client_id.')';   
        }
        if(!empty($gradetag_row)){
            $options['conditions'] .= ' AND Gradetag.id IN ('.$gradetag_row.')';   
        }
        if(!empty($from_od) && !empty($to_od)){
            $size_id = $this->Size->find('list',array('conditions' => array('Size.od_mm >=' => $from_od,'Size.od_mm <=' => $to_od),'fields'=>array('Size.id')));
            $impl_size = implode(",",$size_id);
        }
        $options['fields'] = $alias.'.'.$invoiceno.',Client.company_name,Grade.grade_name,Gradetag.gradetagname,Size.gdmm,'.$subalias.'.qty_mtr,'.$subalias.'.price,'.$subalias.'.netprice';
       
        $grade_tag = $this->Gradetag->find('all', $options);
        /* $log = $this->Gradetag->getDataSource()->getLog(false, false);
        debug($log);*/
        //pr($grade_tag);exit;
          $html = '<table class="table table-bordered table-striped table-condensed flip-content" id="report_id">
                <thead class="flip-content portlet box green">
                      <tr>
                        <th width="5%">#</th>
                        <th width="10%">Invoice / Chalan No.</th>
                        <th width="10%">Client Name</th>
                        <th width="10%">Gradetag Name</th>
                        <th width="10%">Grade Name</th>
                        <th width="10%">Size</th>
                        <th width="20%">Qty mtr</th>
                        <th width="20%">Price</th>
                        <th width="15%">Net Price</th>
                    </tr>
                  </thead>';
         $i = 1;
         
$groups = array();
 $key = 0;
        foreach($grade_tag as $grade_row){
           //  $summaryTotals[] = Array('netprice'=>array_sum($grade_row));
            $html .= '<tr>';
            $html .= '<td>'.$i.'</td>';
            $html .= '<td>'.$grade_row[$alias][$invoiceno].'</td>';
            $html .= '<td>'.$grade_row['Client']['company_name'].'</td>';
            $html .= '<td>'.$grade_row['Grade']['grade_name'].'</td>';
            $html .= '<td>'.$grade_row['Gradetag']['gradetagname'].'</td>';
            $html .= '<td>'.$grade_row['Size']['gdmm'].'</td>';
          //  $html .= '<td>'.$grade_row['Invoiceitem']['qty_mt'].'</td>';
            $html .= '<td>'.$grade_row[$subalias]['qty_mtr'].'</td>';
            $html .= '<td>'.$grade_row[$subalias]['price'].'</td>';
            $html .= '<td>'.$grade_row[$subalias]['netprice'].'</td>';
           
              $key = $grade_row['Gradetag']['gradetagname'];
            if (!array_key_exists($key, $groups)) {

               $groups[$key] = array(
                     'qty' => $grade_row[$subalias]['qty_mtr'],
                     'price' => $grade_row[$subalias]['price'],
                     'netprice' => $grade_row[$subalias]['netprice'],

                );
            }else{
            $groups[$key]['total'] = $groups[$key]['qty'] + $grade_row[$subalias]['qty_mtr'];
            $groups[$key]['totalprice'] = $groups[$key]['price'] + $grade_row[$subalias]['price'];
            $groups[$key]['totalnetprice'] = $groups[$key]['netprice'] + $grade_row[$subalias]['netprice'];
            $html .= '<tr>';
            $html .= '<td colspan="6">Total</td>';
            $html .= '<td>'.$groups[$key]['total'].'</td>';
            $html .= '<td>'.$groups[$key]['totalprice'].'</td>';
            $html .= '<td>'.$groups[$key]['totalnetprice'].'</td>';
            $html .= '</tr>';
            }
             $html .= '</tr>';
            //$result[$grade_row['Gradetag']['gradetagname']] = $result[$grade_row['Gradetag']['gradetagname']] + 
            $i++;
        }
      
      //  array_walk($result, create_function('&$v,$k', '$v = array_sum($v) / count($v);'));
     //   var_dump($summaryTotals);exit;
         $html .='</tbody>
            </table>';
            echo $html;die;
    }


}