<?php
/**
 * Name : ErrorsController
 * Use : For error message
 */
class ErrorsController extends AppController {
    public $name = 'Errors';

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('unauthorize');
    }

	/* Unauthorize Page */
    public function unauthorize()
    {    
        $this->layout = 'accessdenied';        
        $this->set('mainTitle','Unauthorize');              
    }
    /* Unauthorize Page */
    public function accessdenied()
    {   
        $this->layout = 'accessdenied';      
        $this->Session->write('title','Access Denied');         
    }
    public function error404() {
        //$this->layout = 'default';
    }
}