<?php
/*
 * @Controller name: Licence Import Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Licence Import management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('CakeEmail', 'Network/Email');
class LicenceimportsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Licence Import Management');
	}
	/*
   	 * Name: index
   	 * Use: Licence view.
	 */
	function index() {
        $this->set('pageTitle',' Licence Import List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: View
   	 * Use: Details Of Advance licence no.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Licenceimportitem');
		$this->loadmodel('Invoice');
		$this->loadmodel('Licence');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'licenceimports/">Licence Import</a><i class="fa fa-circle"></i></li>');
		//$this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem'),'belongsTo' => array('Hscode','Order')));
		$this->Licence->unbindModel(array('hasMany' => array('Licenceitem'),'belongsTo' => array('Hscode')));
		$licence = $this->Licenceimport->find('first', array(
   			'conditions' => array('Licenceimport.id' => $id ),'recursive'=>2
		));
		//pr($licence);exit;
		$this->set('data',$licence);
		$this->set('pageTitle',' View Licence Import');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','advancelicenceno','boeno','boedate','invoiceno','invoicedate','company_name','first_name','Licenceimport.modified',);
    	$order = array('Licenceimport.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Licenceimport->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$Licences = $this->Licenceimport->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('licence',$Licences);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Licence.
	 */
	function add($id = null) {
			$this->loadmodel('Client');
			$this->loadmodel('Licence');
			$this->loadmodel('Licenceitem');
			$this->loadmodel('Licenceimportitem');
			$this->loadmodel('Gradetag');
			$this->loadmodel('Price');
			$id = base64_decode($id);
			$this->set('pageTitle','Add Licence Import');
		//	$this->request->data = $this->Licenceimport->findById($id);
			if(!empty($id)) {
			$this->set('pageTitle','Edit Licence Import');
			$this->Licence->unbindModel(array('hasMany' => array('Licenceitem'),'belongsTo' => array('Hscode')));
			$this->request->data = $this->Licenceimport->find('first',array('conditions'=>array('Licenceimport.id' => $id),'recursive'=>2));
			$licenitem = $this->Licenceimportitem->find('all',array('conditions'=>array('Licenceimportitem.licenceimportid' => $id),'recursive'=>1));
			$this->set('licenitem',$licenitem);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Licence Import');		
		}
		$this->set('id',$id);
			$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'licenceimports/">Licence Import</a><i class="fa fa-circle"></i></li>');
		$client_list = $this->Client->find('list',array('fields'=>array('Client.id','Client.company_name')));
		$this->set('client_list',$client_list);
		$licencelist = $this->Licence->find('list',array('fields'=>array('Licence.id','Licence.advancelicenceno')));
		$this->set('licencelist',$licencelist);
		$pricelist = $this->Price->find('list',array('fields'=>array('Price.id','Price.sign')));
		$this->set('price_set',$pricelist);
	}	

	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->loadmodel('Licenceimportitem');
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Licenceimport->set($this->request->data);	
			$response=array();
			/* set reference location if add Licence form open in modal dialog means popup */
			if(isset($this->request->data['Licenceimport']['targetid'])){
				$response['targetid'] = $this->request->data['Licenceimport']['targetid'];
				unset($this->request->data['Licenceimport']['targetid']);	
			}
			if($this->Licenceimport->validates($this->request->data)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Licenceimport']['modifiedby'] = $logged_user;
					$id = $this->request->data['Licenceimport']['id'];
					if(!empty($id)){
					$this->old = $this->Licenceimport->findById($id,array('recursive'=>0));
					$licenseitems = $this->request->data['Licenceimport'];
					$boedate = $this->request->data['Licenceimport']['boedate'];
					$in_date = $this->request->data['Licenceimport']['invoicedate'];
					$licenseitems['boedate'] = date('Y-m-d', strtotime(str_replace('/','-', $boedate)));
					$licenseitems['invoicedate'] = date('Y-m-d', strtotime(str_replace('/','-', $in_date)));
						if($this->Licenceimport->save($licenseitems)) {
		            		$item = $this->request->data['Licenceimport']['item'];
		           			foreach($item as $keyitem => $items){
			           			if(!empty($items['id'])){
			           				$this->Licenceimport->id = $items['id'];
			           				if(!empty($items['importqty'])){
			           					//pr($items);
				           				$this->Licenceimportitem->save($item[$keyitem]);
				           			}
				           		}
			        		}
						}
				$licenseitems['item'] = $item;/*updated value so we pass it into activity log*/
	           	$licenseitems['Licenceitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Licenceimport','Add',$licenseitems);	
					}			

			if(empty($id)) {
					$this->request->data['Licenceimport']['createdby'] = $logged_user;
				$this->request->data['Licenceimport']['boedate'] = date('Y-m-d', strtotime(str_replace('/','-', $this->request->data['Licenceimport']['boedate'])));
				$this->request->data['Licenceimport']['invoicedate'] = date('Y-m-d', strtotime(str_replace('/','-', $this->request->data['Licenceimport']['invoicedate'])));
				if($this->Licenceimport->save($this->request->data)) {
					$lastid = $this->Licenceimport->getLastInsertID(); 
		            $item = $this->request->data['Licenceimport']['item'];
		            $totalamount = 0;
		           	foreach($item as $keyitem => $items){
			           		$item[$keyitem]['licenceimportid'] = $lastid;
			           		if(!empty($items['importqty'])){
			           		$this->Licenceimportitem->saveall($item[$keyitem]);
			           		}
			          	}
	          	}
	          	$licenseitems['item'] = $item;/*updated value so we pass it into activity log*/
	           	$licenseitems['Licenceimportitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Licenceimport','Add',$licenseitems);

			}
					if(!empty($id)) { 
						$msg = 'Licence Import has been Updated successfully';
						$result = array_diff_assoc($this->old['Licenceimport'],$this->request->data['Licenceimport']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Licenceimport','Edit',$result);/*Add Data in Log*/												
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Licenceimport','Add',$this->request->data['Licenceimport']);/*Add Data in Log*/	
						$msg = 'Licence Import has been Added successfully';
						$response['idvalue'] = $this->Licenceimport->getLastInsertID();
						//$response['text'] = $this->request->data['Licence']['gdmm'];
					}
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						//$this->redirect('/Licences/index');
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'Licenceimport';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
				
			}
			else{
                $Licenceimport = $this->Licenceimport->invalidFields(); 
                $response['status'] = 'error';
                $response['message'] = 'The Licence Import could not be saved. Please, try again.';
                $response['data'] = compact('Licenceimport');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGradetag($id = null) {
		$this->loadmodel('Licenceitem');
		$this->autoRender = false;
		
   		$all_grade_data = $this->Licenceitem->find('all',array('conditions'=> array('Licenceitem.licenceid'=>$id)));
   		$i = 0;
   		foreach($all_grade_data as $grade){
   			$balance_qty = $this->requestAction('Licenceimports/ImportGradetagbalanceqty/'.$id.'/'.$grade['Gradetag']['id']);
	   		echo '<tr><input type="hidden" name="data[Licenceimport][item]['.$i.'][licenceitemid]" value="'.$grade['Licenceitem']['id'].'">';
	   		echo '<tr><input type="hidden" name="data[Licenceimport][item]['.$i.'][gradetagid]" value="'.$grade['Gradetag']['id'].'">';
	   		echo '<td>'.$grade['Gradetag']['gradetagname'].'</td>';

	   		echo '<td>'.$grade['Licenceitem']['im_qty'].'</td>';

	   		echo '<td><div class="input text"><input type="text" class="form-control" name="data[Licenceimport][item]['.$i.'][importqty]"></div></td>';

	   		echo '<td><div class="input text"><input type="text" class="form-control" name="data[Licenceimport][item]['.$i.'][dollarvalue]"></div></td>';

	   		echo '<td><div class="input text">'.$balance_qty.'</div></td>';
	   		
	   		echo '</tr>';
	   		$i++;
   		}
   		exit;
	}
	/**
   	 * Name: delete
   	 * Use: delete Licence import
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->autoRender = false;
		$this->loadmodel('Licenceimportitem');
		$this->Licenceimport->id = $id;
		$data = $this->Licenceimport->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Licenceimport','Delete',$data['Licenceimport']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Licenceimport->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Licence Import has been Deleted successfully</div>'));
		}		
		$this->Licenceimportitem->deleteAll(array('Licenceimportitem.licenceimportid' => $id), true);
		return $this->redirect(array('action' => 'index'));
	}

	/**
	* Name : ImportGradetagbalanceQty
	* Use : Count balance value as per Gradetag and Licence item
	*/
	function ImportGradetagbalanceQty($licenceid = null,$gradetag_id = null){
		$this->autoRender = false;
		$this->loadmodel('Licence');
		$this->Licence->unbindModel(array('hasMany' => array('Licenceitem'),'belongsTo' => array('Hscode')));
		$data = $this->Licenceimport->find('all',array('conditions'=>array('Licenceimport.licenceid' => $licenceid),'recursive'=>2));
		$used_qty = 0;
		$allow_qty = 0;
		/*If licenceitem and grade tag not used in licence import than*/
		if(empty($data)){
			$this->loadmodel('Licenceitem');
			$data = $this->Licenceitem->find('first',array('conditions'=>array('licenceid' => $licenceid,'gradetagid' => $gradetag_id),'recursive'=>-1));
			$allow_qty = $data['Licenceitem']['im_qty'];
		}
		/*If licenceitem and grade tag used in in licence import than*/
		else{
			foreach($data as $all_value){
					foreach($all_value['Licenceimportitem'] as $import_value){
					  	if(!empty($gradetag_id)){
							if($import_value['Licenceitem']['gradetagid'] == $gradetag_id){
								$allow_qty = $import_value['Licenceitem']['im_qty'];
								$used_qty += $import_value['importqty'];	
							}
						}	
					}
			}
		}
		$balance_qty = $allow_qty - $used_qty;
		return number_format((float)$balance_qty,3, '.', '');
	}
}