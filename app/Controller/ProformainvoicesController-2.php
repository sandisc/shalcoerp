<?php
/*
 * @Controller name: Proformainvoice Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Proformainvoice management.
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class ProformainvoicesController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Proforma Invoice Management');
	}
	
	/*
   	 * Name: index
   	 * Use: Proformainvoice listing page.
	*/
	function index() {
        $this->set('pageTitle',' Proforma Invoice List');
        $this->loadmodel('Dispatch');
        $status = $this->Proformainvoice->find('all',array('conditions'=> array('Proformainvoice.isverified'=>1),'recursive' => -1));
 		$this->set('status_info',$status);
        $this->set('dispatch',$this->Dispatch->find('list',array('fields' => array('id','location'))));
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','proforma_no','proforma_date','company_name','Proformainvoice.modified','first_name','status','isverified');
    	$order = array('Proformainvoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Proformainvoice->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$proformainvoices = $this->Proformainvoice->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('proformainvoice',$proformainvoices);
		$this->set('count',$count);
	}
	/*
	 * Name : Order listing
	 * Use  : Order listing of Approval Proforma Invoice.
	*/

	function orderlisting(){
    	$column = array('Proformainvoice.id','proforma_no','proforma_date','company_name','Proformainvoice.modified','first_name','status');
    	$order = array('Proformainvoice.id' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Proformainvoice->find('count',array('conditions'=>array($res['con'],'Proformainvoice.status'=>1),'order'=>$res['order_by']));
		$orderlisting = $this->Proformainvoice->find('all',array('conditions'=>array($res['con'],'Proformainvoice.status'=>1),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('orderlisting',$orderlisting);
		$this->set('count',$count);
	}

	/*
   	 * Name: View
   	 * Use: user can View all Details of Proformainvoice.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Setting');
		if(empty($id)) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma invoices</a><i class="fa fa-circle"></i></li>');
		$proforma = $this->Proformainvoice->find('all', array(
   		'conditions' => array('Proformainvoice.id' => $id ), // URL to fetch the required page
    	'recursive' => 2
		));
		$this->set('data',$proforma);
		$setting = $this->Setting->find('all');
		$this->set('setting',$setting);
		$this->set('pageTitle',' Proforma Invoice View');
	}
	/*
   	 * Name: add
   	 * Use: user can add,update invioce and Unique Proforma id.
	*/
	function add() {
		$this->loadmodel('Standard');
		$this->loadmodel('Size');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Bank');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma invoices</a><i class="fa fa-circle"></i></li>');

		if(!empty($this->request->data)) {  
			$logged_user = $this->Auth->user('id');
			$this->request->data['Proformainvoice']['createdby'] = $logged_user;
			$this->request->data['Proformainvoice']['modifiedby'] = $logged_user;
			
			$invoice = $this->request->data['Proformainvoice'];
			$date = $this->request->data['Proformainvoice']['proforma_date'];
			$invoice['proforma_date'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
			$this->Proformainvoice->save($invoice);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Proformainvoice has been Added</div>'));
			$invoice_data = $this->Proformainvoice->find('all', array('limit' => 1,'order' => 'Proformainvoice.id DESC'));
            $item = $this->request->data['Proformainvoice']['item'];
          //  pr($item);exit;
           	foreach($item as $items)
           	{	
           		//pr($items);exit;
           		$items['proforma_id'] = $invoice_data[0]['Proformainvoice']['id'];
           		$this->Proformaitem->saveall($items);
           	}
           		
            	$this->redirect('index');
           		 $lastid = $this->Proformainvoice->getLastInsertID();
           		 $pro_data = $this->Proformainvoice->find('all',array('conditions'=> array('Proformainvoice.id'=>$lastid),'recursive' => 1));
           		 $record = $this->email($lastid);
           		 $Email = new CakeEmail();
				 $Email->from('shalco@gmail.com');
				 $Email->to($pro_data[0]['User']['email']);
				 $Email->subject('Proforma Invoice');
				// $Email->send($record);
				 $Email->emailFormat('html');
	           	 
		}
		
			$this->set('pageTitle','Add Proforma Invoice');
			//$this->set('id',$id);
			//pr($id);
			if(empty($id)){
			$pr_id = $this->request->data['Proformainvoice']['proforma_no'] = $this->getProformaID();
			}else{
		   		$this->set('pr',$this->request->data['Proformainvoice']['proforma_no']);
			}
		$standard_all = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
		$this->set('standard_all',$standard_all);
		//pr($standard_all);exit;
		$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
		$this->set('gdmm',$gdmm);
		//pr($gdmm);exit;
		$bank = $this->Bank->find('list',array('fields' => array('id','bank_name')));
		$this->set('bank',$bank);	

	}
	/*
   	 * Name: Edit
   	 * Use: user can update invioce.
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Standard');
		$this->loadmodel('Grade');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Size');
		$this->loadmodel('Bank');
		$this->loadmodel('Client');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma invoices</a><i class="fa fa-circle"></i></li>');
		if(!empty($this->request->data)) {  
			$logged_user = $this->Auth->user('id');
			$this->request->data['Proformainvoice']['createdby'] = $logged_user;
			$this->request->data['Proformainvoice']['modifiedby'] = $logged_user;
			$invoice = $this->request->data['Proformainvoice'];
			$date = $this->request->data['Proformainvoice']['proforma_date'];
			$invoice['proforma_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $date)));
			$this->Proformainvoice->save($invoice);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Proformainvoice has been Added</div>'));
			$invoice_data = $this->Proformainvoice->find('first',  array('conditions' => array('Proformainvoice.id' => $id )));
			//pr($invoice_data);exit;
            $item = $this->request->data['Proformainvoice']['item'];
            $del_ids = $this->request->data['removal_id'];
            if(isset($del_ids) && !empty($del_ids)){
            	$del=array($del_ids);
	            $condition = array('Proformaitem.id IN ('.$del_ids.')');
	            $this->Proformaitem->deleteAll($condition,false);
	        }
               	foreach($item as $items){	
	           		if(isset($items['id'])){
	           		 	$this->Proformaitem->updateAll($items,array('Proformaitem.id' => $items['id']));
	           		}
	           		else{
	           			$items['proforma_id'] = $invoice_data['Proformainvoice']['id'];
	           			$this->Proformaitem->saveall($items);
	           		//	$log = $this->Proformaitem->getDataSource()->getLog(false, false);
					//	debug($log);
					}           	 
	           }
	           $this->redirect('index');
	           
		}
		$this->set('pageTitle','Edit Proforma Invoice');
		//$this->set('id',$id);
			if(!empty($id)){
					$year = date("Y");
					$pr_id = $this->request->data['Proformainvoice']['proforma_no'] = 'SHL/000/'.$year;
					$this->set('pr',$pr_id);
			}
			$this->request->data = $this->Proformainvoice->findById($id);
			$standard_all = $this->Standard->find('all', array('fields' => array('Standard.id','Standard.standard_name')));
			$standards = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
			$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
			$gdmm_all = $this->Size->find('all');
			$proformainvoice = $this->Proformainvoice->find('all', array(
   		 	'conditions' => array('Proformainvoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 2));
    		//pr($proformainvoice);exit;
			$bank = $this->Bank->find('list',array('fields' => array('id','bank_name')));
			$this->set('bank',$bank);	
			$this->set('proformainvoice',$proformainvoice);
			$this->set('standard_all',$standard_all);
			$this->set('standards',$standards);
			$this->set('gdmm_all',$gdmm_all);
			$this->set('gdmm',$gdmm);
			$clients = $this->set('clients', $this->Client->find('list', array(
        				'fields' => array('Client.id', 'Client.company_name'),
        				'recursive' => 0
    		)));
		}
	/*
   	 * Name: getDispatchcode via Ajax
   	 * Use: Fetch Dispatch Location for Approval Proformainvoice .
	 */
	public function getDispatchcode($id,$proforma_id) {
		$this->loadmodel('Dispatch');

		$location = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $id,'id'=>$proforma_id)));
		$cntdata = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $id)));
		$count = sizeof($cntdata);
		$this->autoRender = false;
   		$dispatch_data = $this->Dispatch->find('all',array('conditions'=> array('Dispatch.id'=>$id)));
   		$i = 1;
   		//echo $count;exit;
   		if(!empty($location)){
   			echo $data = $location[0]['Proformainvoice']['dispatchcode'];
   		}else{
   			if($count == 0)
   			{
   				++$count;
   				echo $data = $dispatch_data[0]['Dispatch']['code'].'-'.$count;
   			}else{
   				++$count;
   				echo $data = $dispatch_data[0]['Dispatch']['code'].'-'.$count;
   			}
   		}
   		
		exit;
	}

	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGrades($id) {
		$this->loadmodel('Grade');
		$this->autoRender = false;
   		$grade_data = $this->Grade->find('all',array('conditions'=> array('Grade.standard_id'=>$id)));
   		echo '<select>';
		echo '<option value="">Select Grade</option>';
		foreach($grade_data as $grades) { 
	  		echo '<option value="'.$grades['Grade']['id'].'">'.$grades['Grade']['grade_name'].'</option>'."\n";
	  	}
		echo '</select>';
		exit;
	}
	/*
	* Name : 
	*
	*/
	function getnb($id) {
		$this->loadmodel('Size');
		$this->autoRender = false;
   		$gd_data = $this->Size->find('all',array('conditions'=> array('Size.id'=>$id)));
   		echo $data = $gd_data[0]['Size']['gdnb'];
   		exit;
	}
	function delete($id = null)
	{
		 $id = base64_decode($id);
		if (!$id) {
		$this->Session->setFlash(__('<div class="alert alert-danger"><strong>Invalid!</strong> Invalid Proformainvoice. </div>'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Proformainvoice->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-sucess"><strong>Deleted!</strong> Proformainvoice Deleted. </div>'));
			return $this->redirect(array('action' => 'index'));
		} 
		else {
			$this->Session->setFlash(__('<div class="alert alert-danger"><strong>Not Deleted!</strong> Proformainvoice Not Deleted. </div>'));
			return $this->redirect(array('action' => 'index'));
		}
	}
	/**
	* Name : getclients
   	* Use : Getting clients based on suggestion
   	*/
	public function getclients()
	{
		$this->loadmodel('client');
		$this->autoRender = false;
		$resp = $this->client->find('all',array('conditions'=>array('company_name LIKE' => "%".$_REQUEST['q']."%")));
		$final_resp = array();
		foreach($resp as $key => $value){
			$final_resp[] = array('text'=>$value['client']['company_name'],'id'=>$value['client']['id']);
		}
		echo json_encode($final_resp);
	}

	/**
	* Name : getclientdetail
   	* Use : Fetch client details
   	*/
	public function getclientdetail(){
		$this->loadmodel('client');
		$this->autoRender = false;
		$recursive = -1;
		$resp = $this->client->find('first',array('recursive'=>-1,'conditions'=>array('id' => $_POST['id'])));
		echo json_encode($resp);
	}

	/*
	Use : Send email of Custom function of Proforma invoce
	Name : email
	*/
	public function email($id = null){
		$this->loadmodel('Setting');
		$data = $this->Proformainvoice->find('all',array('conditions'=> array('Proformainvoice.id'=>$id),'recursive' => 2));
		$setting = $this->Setting->find('all');
        $html = '<style>
			table {
					  float: none;
					  margin: 0 auto;
					  width: 100%;
				   }
			table {
				  border-collapse: collapse;
				  border-spacing: 0;
				}
			.flip-scroll table, .table td .img-responsive {
			  width: 100%;
			}
			.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
		  		border: 1px solid #e7ecf1
				}
		  	.table {
		  			margin-bottom: 20px;
		  			width: 100%;
			}
				</style>';
	$html .= '<html>
			  <head>
			  		<title>Proforma Email</title>
			  </head>
			  <body>
				<table id="proforma_table" width="100%" border="1" class="table table-bordered table-striped 		  table-condensed flip-content view_invoice">
				<tbody> 
		 			<tr>
                        <td colspan="8"><b>PROFORMA INVOICE</b> </td>
                    </tr>
                    <tr>
                    	<td colspan="8"><b>SHALCO INDUSTRIES PVT LTD</b> </td>
                    </tr>
                    <tr>
                        <td colspan="2"> Office Address :</td>
                        <td colspan="2">'.$setting[0]["Setting"]["office_address"].'</td>
						<td colspan="2"> PROFORMA NO. <br>'.$data[0]["Proformainvoice"]["proforma_no"].'</td>
						<td colspan="2">Date <br>'.$data[0]["Proformainvoice"]["proforma_date"].'</td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="2"> Factory Address :</td>
                        <td width="20%" rowspan="2" colspan="2" >B-11, MIDC Mahad, Dist.Raigad,Pin code-402301, Maharashtra, India</td>
                        <td colspan="2"><b>Terms of Delivey</b></td>
                        <td colspan="2">'.$data[0]["Proformainvoice"]["tod"].'</td>
                    		<tr>
			                    <td colspan="2"><b>Terms Of Payment</b></td>
			                    <td colspan="2">'.$data[0]["Proformainvoice"]["top"].'</td>
	                 		</tr>
                    	</td>
                     </tr>
                    <tr>
                        <td colspan="4"><b>Bill To </b></td>
                        <td colspan="4"><b>Ship To</b></td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>'.$data[0]["Client"]["company_name"].'</b><br>'.$data[0]["Client"]["address1"].",".$data[0]["Client"]["address2"].'</td>
                        <td colspan="4"><b>'.$data[0]["Client"]["company_name"].'</b><br>'.$data[0]["Proformainvoice"]["ship_address"].'</td>
                    </tr>
                    <tr>
                    	<td width="10%" align="center"><b>SR No</b></th>
                        <td width="10%" align="center"><b>STANDARD</b></th>
                        <td width="10%" align="center"><b>MATERIAL<br>GRADE</b></th>
                        <td width="30%" colspan="2" align="center"><b>GOOD DESCRIPTION</b></th>
                        <td width="10%" align="center"><b>QTY<br>MTR.</b></th>
                        <td width="10%" align="center"><b>PRICE<br>USD/MTR </b></th>
                        <td width="10%" align="center"><b>AMOUNT<br>USD</b></th>
                    </tr>';
               				$i = 1;
                            foreach($data[0]["Proformaitem"] as $value) {  
                            	$html .= '<tr class="addmore">';
                                $length = $value["length"]/1000;
                               	$html .= '<td>'.$i.'</td>
                                <td>'.$value["Standard"]["standard_name"].'</td>
                                <td>'.$value["Grade"]["grade_name"].'</td>
                                <td style="white-space:nowrap;">'.$value["Size"]["gdmm"]." X ".$value["length"]." MM".'</td>
                                <td style="white-space:nowrap;">'.$value["Size"]["gdnb"]." X ".$length." MTR".'</td>
                                <td>'.$value["qty"].'</td>
                                <td>'.$value["price"].'</td>
                                <td>'.$value["amount"].'</td>
                                </tr>';$i++;
                            } 
                            $html .= '<tr>    
                            			<td colspan="6"></td>
                                        <td><b>Total</b></td>
                                        <td><b>'.$data[0]["Proformainvoice"]["total"].'</b></td>
                                      </tr>          
                                      <tr>
                                         <td colspan="7"><b><u>Bank Details</u></b></td>
                                      </tr>
                                      <tr>
                                         <td colspan="2">Bank Name:</td>
                                         <td colspan="3">'.$data[0]["Bank"]["bank_name"].'</td>
                                         <td colspan="3">For, SHALCO IND. PVT. LTD. </td>
                                       </tr>
                                       <tr>
                                          <td colspan="2">Branch Name:</td>
                                          |<td colspan="3">'.$data[0]["Bank"]["branch_name"].'</td>
                                        </tr>
                                        <tr>
                                           	<td colspan="2">Swift Code:</td>
                                            <td colspan="3">'.$data[0]["Bank"]["swift_code"].'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">AD Code: </td>
                                            <td colspan="3">'.$data[0]["Bank"]["ad_code"].'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Beneficiary’s EEFC A/C No:
                                            <td colspan="3">'.$data[0]["Bank"]["ac_no"].'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Beneficiary’s Name:
                                            <td colspan="3">'.$data[0]["Bank"]["ac_name"].'</td>
                                        </tr>
                                        <tr>
 	                                        <td colspan="8"><b>All Terms & Conditions:-</b>
                                            	<tr>
                                                	<td colspan="8"><b>All the term & condition as per the mentions on Purchase Order.</b></td>
                                                </tr>
                                            </td>
                                        </tr>
                 </tbody>
             </table>
             </body>
             </html>';
             echo $html;
	  	/*	$Email = new CakeEmail();
			$Email->from('shalco@gmail.com');
			$Email->to($data[0]['Client']['email']);
			$Email->subject('Verified Proforma Invoice');
			$Email->send($html);
			$Email->emailFormat('html');*/

	}
	/**
	* Name : getStatus
   	* Use : Set a Status approval,rejected and pending via ajax
   	*/
   	function getStatus($id,$status_id) {	
   		$this->autoRender = false;
   		$current_time = date('Y-m-d H:i:s');
   		$approved_user = $this->Auth->user('id');
   		$status_update  = array('status' => $status_id, 'approval_date'=> $current_time,'approvalby' => $approved_user);
   		$rejectcomment  = array('status' => $status_id, 'approval_date'=> $current_time,'approvalby' => $approved_user,'rejectcomment');
	   		$this->Proformainvoice->id = $id;
	   			$this->Proformainvoice->save($status_update);
	   
   	}	

   	/*
	* Name : rejectproformainvoice
	* Use : reject proforma invoice
   	*/
   	function rejectproformainvoice()
   	{	$this->autoRender = false;
   		$rejectdata  = array('rejectcomment' => $_POST['comment']);
   	   	$this->Proformainvoice->id = $_POST['id'];
	   	$this->Proformainvoice->save($rejectdata);
	   
   	}

   	/*
	* Name : approveproformainvoice
	* Use : approve proforma invoice
   	*/
   	function approveproformainvoice($id,$dispatch,$dispatchcode)
   	{
   		$this->autoRender = false;
   		$approvedata  = array('dispatch_id'=>$_POST['dispatch'],'dispatchcode' => $_POST['dispatchcode']);
   	   	$this->Proformainvoice->id = $_POST['id'];
	   	$this->Proformainvoice->save($approvedata);
	   
   	}
	/**
	* Name : getVerified
   	* Use : Set a value Verified or not via Ajax
   	*/
   	function getVerified($id) {
   	$this->autoRender = false;
   	$this->loadmodel('Setting');
   	$data = $this->Proformainvoice->find('all',array('conditions'=> array('Proformainvoice.id'=>$id),'recursive' => 2));
	//exit;
   //	$verify_id = $data[0]['Proformainvoice']['isverified'];
   	$this->email($id);
   	$current_time = date('Y-m-d H:i:s');
   	$verified_user = $this->Auth->user('id');
	   	if($verify_id == 0){
	   		$update = array('isverified' => 1, 'verifiedby'=> $verified_user,'verified_date' => $current_time);
	   		$this->Proformainvoice->id = $id;
	   		$this->Proformainvoice->save($update);
			$setting = $this->Setting->find('all');
            $Email = new CakeEmail();
			$Email->from('shalco@gmail.com');
			$Email->to($data[0]['User']['email']);
			$Email->subject('Verified Proforma Invoice');
			//$Email->send($this->email);
			$Email->emailFormat('html');
		}
	}
	/*
		Name : getProformaID
		Use : Get Unique Proforma Id for Invoicd

	*/
	function getProformaID(){
		$proforma_id = $this->Proformainvoice->find('all',array('limit' => 1,'order' => 'Proformainvoice.id DESC'));
			//pr($proforma_id);
			$this->request->data['Proformainvoice']['proforma_date'] = date('d/m/Y');
			if(!empty($proforma_id)){
				$pro_id = $proforma_id[0]['Proformainvoice']['proforma_no'];
				$import_id = '/OL/'; 
				$pos = strpos($pro_id, $import_id);
				$year = date("Y");
				if ($pos === false){
					$exp_id = explode('/',$pro_id);
					$cut_id = substr($exp_id[1], 4); // cut first four character from string.
					$proforma_id = $cut_id + 1;
					$pr_id = 'SHL/'.$this->rand_str().$proforma_id.'/'.$year;
					return $pr_id;
				}
				else{
					$exp_id = explode('/',$pro_id);
					$cut_id = substr($exp_id[2], 4);
					$proforma_id = $cut_id + 1;
					$pr_id = 'SHL/'.$this->rand_str().$proforma_id.'/'.$year;
					return $pr_id;
				}
			}	
			else
			{
				$year = date("Y");
				$pr_id = 'SHL/'.$this->rand_str().'1/'.$year;
				return $pr_id;
			}
	}
	/*
		Name : getManualID
		Use : Get Unique Proforma Id for Invoice for Import
	*/
	function getManualID(){
		$manual_id = $this->Proformainvoice->find('all',array('limit' => 1,'order' => 'Proformainvoice.id DESC'));
			$this->request->data['Proformainvoice']['proforma_date'] = date('d/m/Y');
			if(!empty($manual_id)){
				$pro_id = $manual_id[0]['Proformainvoice']['proforma_no'];
				$import_id = '/OL/'; 
				$pos = strpos($pro_id, $import_id);
				$year = date("Y");
				if ($pos === false){
					$exp_id = explode('/',$pro_id);
					$cut_id = substr($exp_id[1], 4); // cut first four character from string.
					$proforma_id = $cut_id + 1;
					$pr_id = 'SHL/OL/'.$this->rand_str().$proforma_id.'/'.$year;
					return $pr_id;
				}
				else{
					$exp_id = explode('/',$pro_id);
					$cut_id = substr($exp_id[2], 4);
					$proforma_id = $cut_id + 1;
					$pr_id = 'SHL/OL/'.$this->rand_str().$proforma_id.'/'.$year;
					return $pr_id;
				}
			}
			else
			{
				$year = date("Y");
				$pr_id = 'SHL/OL/'.$this->rand_str().'1/'.$year;
				return $pr_id;
			}	
	}
	/*
   	 * Name: order
   	 * Use: Approval Order listing page.
	*/
	function order() {
		$this->loadmodel('Client');
		$this->loadmodel('Proformaitem');	
        $this->set('pageTitle',' Order List');
        $this->set('ajaxaction','orderlisting');
         if ($this->request->is('post')) {
         $this->Proformainvoice->create();
            $filename = WWW_ROOT.$this->request->data['Proformainvoice']['uploadfile']['name']; 
            move_uploaded_file($this->request->data['Proformainvoice']['uploadfile']['tmp_name'],$filename);
			if (($handle = fopen($filename, "r")) !== FALSE) {
				$data = array();
				$item = array();
				$errors = '';
				$i = 0;
				$r = 1; /*pointing excel sheet row number, which is useful while set error on line number*/
				$sum = 0;
    			while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
    				if($i == 1)
    				{
	       				$data['proforma_no'] = $this->getManualID();
	       				if(!empty($data['proform_date'])){
	       					$data['proforma_date'] = $row[0];
	       				}
	       				else{
	       					$data['proforma_date'] =  date('Y-m-d');
	       				}
	       				if(!empty($row[4])){
	       					$data['tod'] = $row[4];
	       				}
	       				else{
	       					$errors = '<li>Terms of delievery is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[5])){
	       					$data['top'] = $row[5];
	       				}
	       				else{
	       					$errors .= '<li>Terms of payment is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[1])){	       					
	       					if($this->getClientID($row[1])!=''){
	       						$data['bill_to'] = $this->getClientID($row[1]);	
	       					}
	       					else{
	       						$errors .= '<li>Client name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Client name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[2])){
	       					$data['ship_to'] = $row[2];	
	       				}
	       				else{
	       					$errors .= '<li>Shipping client is missing at line no '.$r.'</li>';			
	       				}
	       				if(!empty($row[3])){
	       					$data['ship_address'] = $row[3];	
	       				}
	       				else{
	       					$errors .= '<li>Shipping address is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[6])){	       					
	       					if($this->getBankID($row[6]) != ''){
	       						$data['bank_id'] = $this->getBankID($row[6]);	
	       					}
	       					else{
	       						$errors .= '<li>Bank name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Bank name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[7])){	       					
	       					if($this->getUserID($row[7]) != ''){
	       						$data['approvalby'] = $this->getUserID($row[7]);	
	       					}
	       					else{
	       						$errors .= '<li>Approval name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Approval name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[8])){	       					
	       					if($this->getDispatchID($row[8]) != ''){
	       						$data['dispatch_id'] = $this->getDispatchID($row[8]);	
	       					}
	       					else{
	       						$errors .= '<li>Dispatch location is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Dispatch location is missing at line no '.$r.'</li>';
	       				}
	       				$data['ismanual'] = 1;
	       			}

	       			if($i > 3){
					//	$item[$i]['proforma_id'] = $last_proformaid;
	       				if(!empty($row[0])){
	       					if($this->getStandardID($row[0])!='') {
	       						$item[$i]['standard_id'] = $this->getStandardID($row[0]);	
	       					}
	       					else{
	       						$errors .= '<li>Standard is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}
	       				}
	       				else{
	       					$errors .= '<li>Standard is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[1])){	       					
	       					if($this->getGradeID($row[1])!=''){
	       						$item[$i]['grade_id'] = $this->getGradeID($row[1]);	
	       					}
	       					else{
	       						$errors .= '<li>Grade is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					       					
	       				}
	       				else{
	       					$errors .= '<li>Grade is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[2]) && $row[3]){
	       					$item[$i]['size_id'] = $this->getSizeID($row[2],$row[3]);	
	       				}
	       				else{
	       					$errors .= '<li>Size is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[4])){
	       					$item[$i]['length'] = $row[4];	
	       				}
	       				else{
	       					$errors .= '<li>Length is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[5])){
	       					$item[$i]['qty'] = $row[5];
	       				}
	       				else{
	       					$errors .= '<li>Quantity is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[6])){
	       					$item[$i]['price'] = $row[6];	
	       				}
	       				else{
	       					$errors .= '<li>Price is missing at line no '.$r.'</li>';			
	       				}
						if(!empty($row[7])){
							$item[$i]['amount'] = $row[7];
							$sum += $item[$i]['amount'];
						}
						else{
							$item[$i]['amount'] = $row[5] * $row[6];
							$sum += $item[$i]['amount'];
						}
					}
					$i++;
					$r++;
		    	}
		    	if(!empty($errors)){
		    		$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> Please try Again.<ul>'.$errors.'</ul></div>'));
		    			return false;
	       		}
	       		
		    	$data['total'] = $sum;
				if($this->Proformainvoice->save($data)){
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Order imported Successfully.</div>'));
				}
				$last_proformaid = $this->Proformainvoice->getLastInsertID();
				if(!empty($last_proformaid)){
					foreach($item as $items){
						$items['proforma_id'] = $last_proformaid;
						$this->Proformaitem->saveall($items);
					}
				}
				fclose($handle);
			}	
			
        }
	}

	 /**
   	 * Name: getClientID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getClientID($value) {
		$this->loadmodel('Client');
		$clientdata = $this->Client->find('first', array(
   			'conditions' => array('company_name' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($clientdata)){
			return $clientdata['Client']['id'];
		}
		else{
			return;
		}
	}

	/**
   	 * Name: getUserID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getUserID($value) {
		$this->loadmodel('User');
		$userdata = $this->User->find('first', array(
   			'conditions' => array('email' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($userdata)){
			return $userdata['User']['id'];
		}
		else{
			return;
		}
	}

	 /**
   	 * Name: getBankID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getBankID($value) {
		$this->loadmodel('Bank');
		$bank = $this->Bank->find('first', array(
   			'conditions' => array('bank_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));		
		if(!empty($bank)){
			return $bank['Bank']['id'];
		}
		else{
			return;
		}		
	}


	 /**
   	 * Name: getDispatchID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getDispatchID($value) {
		$this->loadmodel('Dispatch');
		$dispatch = $this->Dispatch->find('first', array(
   			'conditions' => array('location' => $value),
   			'fields' => array('id','code'), 
    		'recursive' => -1
		));		
		$cntlocation = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $dispatch['Dispatch']['id'])));
			$count = sizeof($cntlocation);
		if($count >= 1){
			++$count;
			$location = $dispatch['Dispatch']['code'].'/'.$this->rand_str().'/'.$count;
			//pr($location);exit;
			return $location;
		}else{
			return $dispatch['Dispatch']['code'].'/'.$this->rand_str().'/1';
		}	
		if(!empty($dispatch)){
			return $dispatch['Dispatch']['code'].'/'.$this->rand_str().'/1';
		}else{
			return;
		}	
	}			

	 /**
   	 * Name: getStandardID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getStandardID($value) {
		$this->loadmodel('Standard');
		$std = $this->Standard->find('first', array(
   			'conditions' => array('standard_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($std)){
			return $std['Standard']['id'];
		}
		else{
			return;
		}		
	}	

	 /**
   	 * Name: getGradeID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getGradeID($value) {
		$this->loadmodel('Grade');
		$grd = $this->Grade->find('first', array(
   			'conditions' => array('grade_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($grd)){
			return $grd['Grade']['id'];
		}
		else{
			return;
		}		
	}		 

	 /**
   	 * Name: getSizeID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getSizeID($gdmm,$gdnb) {
		$this->loadmodel('Size');
		$log_id = $this->Auth->user('id');
		$size = $this->Size->find('first', array(
   			'conditions' => array('gdmm' => $gdmm,'gdnb' => $gdnb),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($size)){
			return $size['Size']['id'];	
		}
		else{
			$exp_gdmm = explode(' ',$gdmm);
			$datarow['od_mm'] = $exp_gdmm[0];
			$datarow['wt_mm'] = $exp_gdmm[3];
			$exp_gdnb = explode(' ',$gdnb);
			$datarow['od_nb'] = $exp_gdnb[0];
			$datarow['wt_nb'] = $exp_gdnb[2];
			$subtract_ratio = $exp_gdmm[0] - $exp_gdmm[3];
			$mult_ratio = $subtract_ratio * $exp_gdmm[3];
			$datarow['calc_ratio'] = $mult_ratio * 0.0248;
			$datarow['gdmm'] = $gdmm;
			$datarow['gdnb'] = $gdnb;
			$datarow['createdby'] = $log_id;
		//	pr($datarow);
			$this->Size->create();
			$this->Size->save($datarow);
		//	$log = $this->Size->getDataSource()->getLog(false, false);
		//	debug($log);
		
			$last_sizeid = $this->Size->getLastInsertID();
			//pr($last_sizeid);
			//exit;
			return $last_sizeid;

		}
		
	}		 		  

}	