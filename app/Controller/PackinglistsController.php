<?php
/*
 * @Controller name: Invoice Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Invoice management.
 */

App::uses('AppController', 'Controller');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));

class PackinglistsController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Packing Management');
	}
	/*
   	 * Name: index
   	 * Use: Invoice listing page.
	*/
	function index() {
        $this->set('pageTitle','Packing List');
        $this->loadmodel('Invoice');
        $this->loadmodel('Dispatch');
        $status = $this->Packinglist->find('all',array('recursive' => -1));
 		$this->set('status_info',$status);
        $this->set('dispatch',$this->Dispatch->find('list',array('fields' => array('id','location'))));
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','packingno','company_name','Packinglist.modified','first_name');
    	$order = array('Packinglist.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Packinglist->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$Packinglists = $this->Packinglist->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by'],'recursive'=>3));
		//pr($Packinglists);exit;
		$this->set('Packinglist',$Packinglists);
		$this->set('count',$count);
	}
	/*
   	 * Name: View
   	 * Use: user can View all Details of Packinglist.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Setting');
		$this->loadmodel('Productcategory');
		if(empty($id)) {
			$this->redirect(array('controller' => 'Packinglists', 'action' => 'index'));
		}
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'packinglists/">Packinglists</a><i class="fa fa-circle"></i></li>');
		$packinglist = $this->Packinglist->find('all', array(
   		'conditions' => array('Packinglist.id' => $id ), // URL to fetch the required page
    	'recursive' => 4
		));
		//pr($Packinglist);exit;
		$this->set('data',$packinglist);

		$setting = $this->Setting->find('all');
		$this->set('settings',$setting);
		$this->set('pageTitle','Packinglist View');
	}

	/*
   	 * Name: add
   	 * Use: user can add,update invioce and Unique Proforma id.
	*/
	function add($id = null) {
		$id = base64_decode($id);
		$this->set('id',$id);
		$this->loadmodel('Standard');
		$this->loadmodel('Invoice');
		$this->loadmodel('Proformainvoice');
		$this->loadmodel('Size');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Productcategory');
		$this->loadmodel('Bank');
		$this->loadmodel('Price');
		$this->loadmodel('Setting');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Hscode');
		$this->loadmodel('Packingitem');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'packinglists/">Invoices</a><i class="fa fa-circle"></i></li>');

		if(!empty($this->request->data)) {  
			//pr($this->request->data);exit;
			$logged_user = $this->Auth->user('id');
			$this->request->data['Packinglist']['createdby'] = $logged_user;
			//$this->request->data['Packinglist']['modifiedby'] = $logged_user;			
			$this->Packinglist->save($this->request->data);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Packing list has been Added</div>'));
			$invoice_data = $this->Packinglist->find('all', array('limit' => 1,'order' => 'Packinglist.id DESC'));
			//$invoice_data = $this->Invoice->find('all', array('limit' => 1,'order' => 'Proformainvoice.id DESC'));
            $item = $this->request->data['Packinglist']['item'];
          //  pr($item);exit;
            $sum = 0;
            $total = 0;
           	foreach($item as $items)
           	{	
           		$items['packingid'] = $invoice_data[0]['Packinglist']['id'];
           		$this->Packingitem->saveall($items);
           	}
           //	pr($invoice);
       /*    	$this->Packinglist->id;
           	if($this->Packinglist->id){
				$this->Packinglist->saveField('totalqty', $invoice['totalqty']);
           		$this->Packinglist->saveField('totalexempted', $invoice['totalexempted']);
           	}*/
          	$this->redirect('index');
		}
			$packing_id = $this->request->data['Packinglist']['packingno'] = $this->getPackingNO();
			$this->set('packing_id',$packing_id);
			$this->set('pageTitle','Add Packinglist');
			$this->set('order_item',$this->Invoice->find('all',array('conditions' => array('Invoice.id' => $id),'recursive' => 3)));
			$this->set('settings',$this->Setting->find('all'));
			
			//$invoices_id = $this->request->data['Packinglist']['packingno'];
		$hscodes = $this->Hscode->find('list',array('fields' => array('Hscode.rate','Hscode.hscode')));
		$this->set('hscodes',$hscodes);
		
		$standard_all = $this->Standard->find('all', array('fields' => array('Standard.id','Standard.standard_name')));
		$this->set('standard_all',$standard_all);
		//pr($standard_all);exit;
		$gdmm = $this->Size->find('all');
		$this->set('gdmm',$gdmm);
		//pr($gdmm);exit;
		$bank = $this->Bank->find('list',array('fields' => array('id','bank_name')));
		$this->set('bank',$bank);

		$procat = $this->Productcategory->find('all',array('fields' => array('Productcategory.id','Productcategory.name')));
		$this->set('procat',$procat);	

		$price_set = $this->Price->find('list',array('fields' => array('Price.id','Price.sign')));
		$this->set('price',$price_set);

	}
	/*
   	 * Name: Edit
   	 * Use: user can update packinglist.
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'packinglists', 'action' => 'index'));
		}
		$this->loadmodel('Standard');
		$this->loadmodel('Price');
		$this->loadmodel('Grade');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Size');
		$this->loadmodel('Bank');
		$this->loadmodel('Client');
		$this->loadmodel('Productcategory');
		$this->loadmodel('Proformainvoice');
		$this->loadmodel('Invoiceitem');
		$this->loadmodel('Hscode');
		$this->loadmodel('Packingitem');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'packinglists/"> Packinglist</a><i class="fa fa-circle"></i></li>');
	    $packing_data = $this->Packinglist->find('all',  array('conditions' => array('Packinglist.id' => $id ),'recursive'=>4));
		$this->set('packing_data',$packing_data);
		if(!empty($this->request->data)) {  
			$logged_user = $this->Auth->user('id');
			$this->request->data['Packinglist']['modifiedby'] = $logged_user;
			$pack = $this->request->data['Packinglist'];
			$date = $this->request->data['Packinglist']['invoicedate'];
			$invoices['invoicedate'] = date('Y-m-d', strtotime(str_replace('/', '-', $date)));
			$this->Packinglist->save($pack);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Packing has been Updated</div>'));
			
            $item = $this->request->data['Packinglist']['item'];
            $sum_qty = 0;
            $total_exempted = 0;
               	foreach($item as $items){	
	           		if(isset($items['id']) && !empty($items['id'])){
	           			$items['packingid'] = $packing_data[0]['Packinglist']['id'];
	           			$items['invoiceitemid'] = $packing_data[0]['Packinglist']['id'];	
	           			$this->Packingitem->id = $items['id'];
 	           			$this->Packingitem->save($items);
	           		}
	           		else{
	           			$items['packingid']  = $packing_data[0]['Packinglist']['id'];
	           			$this->Packingitem->saveall($items);
					}           	 
	           	}
	           	$invoice['totalqty'] = $sum_qty;
           		$invoice['totalexempted'] = $total_exempted;
           		$this->redirect('index');
		}
			$this->set('pageTitle','Edit Packinglist');
			$standard_all = $this->Standard->find('all', array('fields' => array('Standard.id','Standard.standard_name')));
			$this->set('standard_all',$standard_all);
			
			$standards = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
			$this->set('standards',$standards);
			
			$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
			$this->set('gdmm',$gdmm);

			$gdmm_all = $this->Size->find('all');
			$this->set('gdmm_all',$gdmm_all);
			
			$bank = $this->Bank->find('list',array('fields' => array('id','bank_name')));
			$this->set('bank',$bank);

			$this->set('id',$id);
			$procat = $this->Productcategory->find('all',array('fields' => array('Productcategory.id','Productcategory.name')));
			$this->set('procat',$procat);
			
			$price_set = $this->Price->find('list',array('fields' => array('Price.id','Price.sign')));
			$this->set('price',$price_set);	
			$hscodes = $this->Hscode->find('list',array('fields' => array('Hscode.rate','Hscode.hscode')));
			$this->set('hscodes',$hscodes);

		}
		public function pdf_index($id = null) {
    // increase memory limit in PHP 
	
		//pr($id);exit;
		$this->loadmodel('Setting');
		$this->pdfConfig = array(
		'filename' => 'Packinglist',
		'download' => (bool)$this->request->query('download')
	);
		//pr($this->pdfConfig);exit;
		//$id = $this->Proformainvoice->getLastInsertID();
		$data = $this->Invoice->find('all',array('conditions'=> array('Invoice.id'=> $id),'recursive' => 3));
		//pr($data);exit;
		$settings = $this->Setting->find('all');
       $html = '<style>
       		 .separatordiv {
 			 border: 1px solid;
  			 margin-left: 3px !important;
  			 margin-right: 3px !important;
  			 padding-bottom: 3px !important;
  			padding-top: 3px !important;
			}
			.col-md-6 {
 				 width: 50%;
			}
			.col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {
  float: left;
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
  min-height: 1px;
  padding-left: 15px;
  padding-right: 15px;
  position: relative;
}
				</style>';
	$html .= '<html>
			  <head>
			  		<title>Proforma Email</title>
			  </head>
			  <body>
			<div class="portlet-body form">
        <div class="form-body"> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                 <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"></span>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">Factory address</label>
                      <div class="input textarea">'.$settings[0]["Setting"]["office_address"].'</div>
                      <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea">'.$settings[0]["Setting"]["factory_address"].'</div>
                      <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Invoice No</label>
                        <div class="input text">'.$data[0]["Invoice"]["invoiceno"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">

                        <label class="control-label">Date</label>
                      <div class="input-group">
                      <div class="input text">'.$data[0]["Invoice"]["invoicedate"].'</div>
                      </div>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">P.O no</label>
                        <div class="input text">'.$data[0]["Proformainvoice"]["po"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">

                      <label class="control-label">Date</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <div class="input text">'.$data[0]["Proformainvoice"]["approval_date"].'</div>
                      </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Country Of Origin Goods</label>
                      <div class="input text">'.$data[0]["Invoice"]["fromcountry"].'</div>      <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Country Of final Destination</label>
                       <div class="input text">'.$data[0]["Invoice"]["fromcountry"].'</div>
                       <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Consignee</label>
                       <div class="input text">'.$data[0]["Proformainvoice"]["Client"]["company_name"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Consignee Address</label>
                       <div class="input text">'.$data[0]["Proformainvoice"]["Client"]["address1"].'</div>
                                <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Buyers</label>
                       <div class="inpux`1t text"></div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-12">
                        <label class="control-label">IFCS CODE NO.</label>
                        <div class="input text">'.$data[0]["Invoice"]["ifscno"].'</div>
                                <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIEGE BY</label>
                        <div class="input text">'.$data[0]["Invoice"]["carriageby"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Place of Receipt By Pre-Carrier</label>
                        <div class="input text">'.$data[0]["Invoice"]["receiptplace"].'</div>
                                <span class="help-block"></span>
                    </div>
                </div>

                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Other Reference</label>
                        <div class="input text">'.$data[0]["Invoice"]["otherref"].'</div>
                            <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Vesse Flight no</label>
                        <div class="input text">'.$data[0]["Invoice"]["flightno"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Port of Loading</label>
                        <div class="input text">'.$data[0]["Invoice"]["loadingport"].'</div>
                                <span class="help-block"></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Delievery</label>
                        <div class="input text">'.$data[0]["Invoice"]["top"].'</div>
                            <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Port of Discharge</label>
                        <div class="input text">'.$data[0]["Invoice"]["dischargeport"].'</div>
                            <span class="help-block"></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Place of Delievery</label>
                        <div class="input text">'.$data[0]["Invoice"]["deliveryplace"].'</div>
                                <span class="help-block"></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Payment</label>
                        <div class="input text">'.$data[0]["Invoice"]["top"].'</div>
                            <span class="help-block"></span>
                    </div>
                </div>
            </div>
               <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Container no</label>
                        <div class="input text">'.$data[0]["Invoice"]["containerno"].'</div>
                            <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
            </div>
        </div>  <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                <thead class="flip-content portlet box green">
                    <tr>
                        <th width="12%">No of Kind Packages</th>
                        <th width="15%">Product</th>
                        <th width="10%">STANDARD</th>
                        <th width="10%">MATERIAL<br>GRADE</th>
                        <th width="20%" colspan="1">GOOD DESCRIPTION</th>
                        <th width="10%">HEAT NO</th>
                        <th width="4%">Pcs</th>
                        <th width="6%"> Length<br>(MTR)</th>
                        <th width="6%" id="qty_lbl"> QTY<br>MTR.'.$data[0]["Proformainvoice"]["Price"]["sign"].'</th>
                        <th width="6%" id="qty_lbl"> Exemted</th>
                        <th width="10%" id="prc_lbl">UNIT PRICE<br>MTR '.$data[0]["Proformainvoice"]["Price"]["sign"].'</th>
                        <th width="10%" id="amount_lbl">Net Amount '.$data[0]["Proformainvoice"]["Price"]["sign"].'
                        <br></th>
                    </tr>                                          
                </thead>
                <tbody>';
            $in_items = $data[0]['Invoiceitem'];
                    $i = 0;
                       foreach($data[0]['Proformainvoice']['Proformaitem'] as $key => $v) { ++$key;
                    $html .= '<tr class="addmore">
                    <td>'.$in_items[$i]["bundleno"].'</td>
                        <td>'.$v["productname"].'
                        </td>
                        <td>'.$v["Standard"]["standard_name"].'</td>
                        <td>'.$v["Grade"]["grade_name"].'
                        </td>'.$total_length = $v["length"] * 1000;'
                        <td>'.$v["Size"]["gdmm"].' X '.$total_length;'
                        </td>
                        <td style="white-space:nowrap;">'.$in_items[$i]["heatnumber"].'
                        </td>
                        <td style="white-space:nowrap;">'.$in_items[$i]["pcs"].'</td>
                        <td style="white-space:nowrap;">'.$v["length"].'
                        </td>
                        <td> '.$in_items[$i]["qty_mtr"].'
                        </td>
                        <td> 
                          '.$in_items[$i]["exempted"].'
                        </td>
                        <td> 
                          '.$in_items[$i]["price"].'
                        </td>
                        <td>'.$in_items[$i]["netprice"];++$i;}'.</td>
                     
                    </tr>
                    <tr id="1">    
                       
                       <td colspan="7"></td>
                       <td id="total_lbl"> Total </td>
                       <td>'.$data[0]["Invoice"]["totalqty"].'</td>
                        <td>'.$data[0]["Invoice"]["totalexempted"].'</td> 
                        <td>CIF Value</td>  
                        <td>'.$data[0]["Invoice"]["cif"].'</td> 
                     </tr>
                     <tr id="1">    
                       <td colspan="10"></td>
                       <td id="total_lbl"> FOB Value </td>
                       <td></td>
                     </tr>
                     <tr id="1">    
                       
                       <td colspan="10"></td>
                       <td id="total_lbl">Frieght</td>
                       <td></td>
                     </tr>

                     <tr id="1">    
                       
                       <td colspan="10">Export Under Advance License No. 0310801619/3/03/00 DT .07.01.2015</td>
                       <td id="total_lbl">Insurance</td>
                       <td></td>
                     </tr>
                     <tr id="1">    
                       
                       <td colspan="10">Exempted Materials Stainless Steel Seamless Pipes/Tubes(HOT Finished)</td>
                       <td id="total_lbl">Discount Value</td>
                       <td></td>
                     </tr>                      
                      <tr id="1">    
                       <td colspan="11">We Intended to claim rewards under Merchandise Exports From india Scheme(MEIS)</td>
                       </tr>
                      <tr id="1">    
                       <td colspan="10">ONE LACS EIGHTY THREE THOUSANDS FIVE HUNDERS ONLY</td>
                       <td>Final CIF Value</td>
                       <td></td>
                       </tr>        
                </tbody>
            </table>
              </div>
        <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Packages<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                             <div class="input text">'.$data[0]["Invoice"]["totalpackage"].'</div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Net Wieght<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <div class="input text">'.$data[0]["Invoice"]["netweight"].'</div><span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Gross Weight<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                             <div class="input text">'.$data[0]["Invoice"]["grossweight"].'</div><span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"> Date<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                             <div class="input text">'.$data[0]["Invoice"]["invoicedate"].'</div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"> Signature<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <img src="'.ROOT_PATH.'uploads/signature/'.$settings[0]["Setting"]["signature"].'" width="200">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</body>
</html>';
    ini_set('memory_limit', '1024M');
   // echo $html;
   		$this->set('post',$html);
   		//$this->redirect(array('action'=>'pdf_index', 'ext'=>'pdf', 39));
/*require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
spl_autoload_register('DOMPDF_autoload'); 
$dompdf = new DOMPDF(); 
$dompdf->set_paper = 'A4';
$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
$dompdf->render();
echo $dompdf->output();
*/
}
/*
		Name : getPackingID
		Use : Get Unique Proforma Id for Invoicd

	*/
	function getPackingNO(){
		$pack_id = $this->Packinglist->find('all',array('limit' => 1,'order' => 'Packinglist.id DESC'));
			$this->request->data['Packinglist']['packingdate'] = date('d-m-Y');
			if(!empty($pack_id)){
				$in_id = $pack_id[0]['Packinglist']['packingno'];
				$exp_id = explode('/',$in_id);
				$cut_id = substr($exp_id[1], 4); // cut first four character from string.
				$inv_id = $cut_id + 1;
				$year = date("Y");
				$pr_id = 'SHPL/'.$this->rand_str().$inv_id.'/'.$year;
				return $pr_id;
			}	
			else{
					$year = date("Y");
					$pr_id = 'SHL/'.$this->rand_str().'1/'.$year;
					return $pr_id;
			}
	}

}	