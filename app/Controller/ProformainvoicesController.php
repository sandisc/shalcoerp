<?php
/*
 * @Controller name: Proformainvoice Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Proformainvoice management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel.php'));
class ProformainvoicesController extends AppController {
	
	var $helpers  =  array('Html','Form','Csv');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set('mainTitle','Proforma Invoice Management');
	}
	
	/**
   	 * Name: index
   	 * Use: Proformainvoice listing page.
	*/
	function index() {
        $this->set('pageTitle',' Proforma Invoice List');
        $this->loadmodel('Dispatch');
        $this->set('dispatch',$this->Dispatch->find('list',array('fields' => array('id','location'))));
        $this->set('ajaxaction','ajaxlisting');
	}
	
	/**
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
		$this->loadmodel('Order');
    	$column = array('id','proforma_no','proforma_date','company_name','Proformainvoice.modified','first_name','Proformainvoice.status','isverified');
      	$order = array('Proformainvoice.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);	
		$count = $this->Proformainvoice->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$proformainvoices = $this->Proformainvoice->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$orders_find = $this->Order->find('first',array('conditions'=>array('Order.proformaid')));

		$this->set('proformainvoice',$orders_find);
		$this->set('proformainvoice',$proformainvoices);
		$this->set('count',$count);
	}

	/**
   	 * Name: View
   	 * Use: View details of Proformainvoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function view($id = null) {
		$id = base64_decode($id);
		/*Redirect if ID empty */
		if(empty($id)) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}

		/*Fetch data of particular ID*/
		$proforma = $this->Proformainvoice->find('first', array(
   			'conditions' => array('Proformainvoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 2
		));	
		/*Redirect if ID not exist in database*/
		if(empty($proforma)){
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}

		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$proforma);
		$this->set('pageTitle',' Proforma Invoice View');
	}

	/**
   	 * Name: Print
   	 * Use: View details of Print of Proformainvoice.
     * @param int $id id of record
     * @return detail record of id
	*/
	function prints($id = null) {
		$id = base64_decode($id);
		/*Redirect if ID empty */
		if(empty($id)) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}

		/*Fetch data of particular ID*/
		$print = $this->Proformainvoice->find('first', array(
   			'conditions' => array('Proformainvoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 2
		));		
		/*Redirect if ID not exist in database*/
		if(empty($print)){
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}

		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$print);
		$this->set('pageTitle',' Proforma Invoice');
	}

	/**
   	 * Name: add
   	 * Use: user can add proforma invioce
	*/
	function add() {
		$this->loadmodel('Standard');
		$this->loadmodel('Size');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Productcategory');
		$this->loadmodel('Bank');
		$this->loadmodel('Price');
		$this->loadmodel('Client');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');
		
		$this->set('pageTitle','Add Proforma Invoice');
		
		if(empty($id)){
			$pr_id = $this->request->data['Proformainvoice']['proforma_no'] = $this->getProformaNO();
		}else{
	   		$this->set('pr',$this->request->data['Proformainvoice']['proforma_no']);
			$standard_all = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
			$this->set('standard_all',$standard_all);		
		}
		
		/*$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
		$this->set('gdmm',$gdmm);*/

		$bank = $this->Bank->find('list',array('fields' => array('id','bank_alias')));
		$this->set('bank',$bank);

		$procat = $this->Productcategory->find('list',array('fields' => array('Productcategory.id','Productcategory.name')));
		$this->set('procat',$procat);	

		$price_set = $this->Price->find('list',array('fields' => array('Price.id','Price.sign')));
		$this->set('price',$price_set);

		$client = $this->Client->find('list',array('fields' => array('Client.id', 'Client.company_name'),'recursive' => 0));
		$this->set('clients',$client);

	}	

	/**
   	 * Name: ajaxaddsubmit
   	 * Use: ajax submit of add proforma invioce
	*/
	function ajaxaddsubmit(){
		$this->autoRender = false;	
		$this->loadmodel('Proformaitem');
		if(!empty($this->request->data)) {
				$proformainvoice = $this->request->data['Proformainvoice'];
				$date = $proformainvoice['proforma_date'];
				$proformainvoice['proforma_date'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
				$proformainvoice['proforma_no'] = $this->getProformaNO('',$proformainvoice['proforma_date']);
				$proformainvoice['fy'] = $this->calculateFinancialYearForDate($proformainvoice['proforma_date']);	
						
			$this->Proformainvoice->set($proformainvoice);
			if($this->Proformainvoice->validates($proformainvoice)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$proformainvoice['Proformainvoice']['createdby'] = $logged_user;
				$proformainvoice['Proformainvoice']['modifiedby'] = $logged_user;

				$this->Proformainvoice->save($proformainvoice);
				$lastid = $this->Proformainvoice->getLastInsertID(); 
	            $item = $this->request->data['Proformainvoice']['item'];
	            $totalamount = 0;
	           	foreach($item as $keyitem => $items){	
	           		$item[$keyitem]['proforma_id'] = $lastid;
	           		$item[$keyitem]['amount'] = $items['qty'] * $items['price'];
	           		$totalamount+=$item[$keyitem]['amount'];
	           		$this->Proformaitem->saveall($item[$keyitem]);
	           	}

           	    if($this->Proformainvoice->id){
           	    	$proformainvoice['total'] = $totalamount;
	           		$this->Proformainvoice->saveField('total', $totalamount);
	           	}
				$proformainvoice['item'] = $item;/*updated value so we pass it into activity log*/
	           	$proformainvoice['Proformaitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Proformainvoice','Add',$proformainvoice);/*Add Data in Log*/
	           	$this->sendemailcoordinator();           		 
				$msg = 'The Proformainvoice has been Added';
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));

				//$this->redirect('/Proformainvoices/index');
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);				
	        }
	        else{
                $Proformainvoice = $this->Proformainvoice->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Proformainvoice could not be saved. Please, try again.';
                $response['data'] = compact('Proformainvoice');
                echo json_encode($response);	        	
	        }   	
        }		
	}
	/**
   	 * Name: edit
   	 * Use: update profroma invoice.
     * @param int $id id of record
     * @return detail record of id   	 
	*/
	function edit($id = null) {
		$id = base64_decode($id);
		/*Redirect if ID empty */
		if(empty($id)) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}						
		$this->loadmodel('Standard');
		$this->loadmodel('Price');
		$this->loadmodel('Grade');
		$this->loadmodel('Proformaitem');
		$this->loadmodel('Size');
		$this->loadmodel('Bank');
		$this->loadmodel('Client');
		$this->loadmodel('Productcategory');
		$this->loadmodel('Address');
	    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');
		$this->set('pageTitle','Edit Proforma Invoice');

		/*Fetch data of particular ID*/
		$proformainvoice = $this->Proformainvoice->find('first', array(
		 											'conditions' => array('Proformainvoice.id' => $id ),
													'recursive' => 2));
		//$this->old = $proformainvoice;
		
		$this->request->data = $proformainvoice;
		/*Redirect if ID not exist in database*/
		if(empty($proformainvoice)){
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}
		
		$this->set('proformainvoice',$proformainvoice);

		$standard_all = $this->Standard->find('all', array('fields' => array('Standard.id','Standard.standard_name','Standard.procatid')));
		$this->set('standard_all',$standard_all);

/*		$standards = $this->Standard->find('list', array('fields' => array('Standard.id','Standard.standard_name')));
		$this->set('standards',$standards);
*/			
		$gdmm = $this->Size->find('list', array('fields' => array('Size.id','Size.gdmm')));
		$this->set('gdmm',$gdmm);

		$gdmm_all = $this->Size->find('all');
		$this->set('gdmm_all',$gdmm_all);

		$bank = $this->Bank->find('list',array('fields' => array('id','bank_alias')));
		$this->set('bank',$bank);	

		$client = $this->Client->find('list',array('fields' => array('Client.id', 'Client.company_name'),'recursive' => 0));
		$this->set('clients',$client);
		
		$procat = $this->Productcategory->find('all',array('fields' => array('Productcategory.id','Productcategory.name')));
		$this->set('procat',$procat);

		$price_set = $this->Price->find('list',array('fields' => array('Price.id','Price.sign')));
		$this->set('price',$price_set);
		$address = $this->request->data['Client']['Address'];
		$ad = array();
		foreach($address as $add){
			$ad[$add['id']] = $add['full_address'];
		}
		$this->set('options',$ad);
	}

	/**
   	 * Name: ajaxeditsubmit
   	 * Use: ajax submit of edit proforma invioce
	*/
	function ajaxeditsubmit(){
		$this->autoRender = false;	
		$this->loadmodel('Proformaitem');

		if(!empty($this->request->data)) { 
			$this->Proformainvoice->set($this->request->data);
			if($this->Proformainvoice->validates($this->request->data)) {			
				$Activities = new ActivitiesController;
				$id = $this->request->data['Proformainvoice']['id'];
				$this->old = $this->Proformainvoice->find('first', array(
		 											'conditions' => array('Proformainvoice.id' => $id ),
													'recursive' => 2));
				$logged_user = $this->Auth->user('id');
				$this->request->data['Proformainvoice']['modifiedby'] = $logged_user;
				$proformainvoice = $this->request->data['Proformainvoice'];
				$proformainvoice['proforma_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['Proformainvoice']['proforma_date'])));

				/*check old and new proforma number is same or not. if not than we will calling to get new proforma number again. Reason is to avoid duplicacy if same time 2 or more user calling same function*/
				if($this->old['Proformainvoice']['proforma_no'] != $proformainvoice['proforma_no']){
					$proformainvoice['proforma_no'] = $this->getProformaNO('',$proformainvoice['proforma_date']);
					$proformainvoice['fy'] = $this->calculateFinancialYearForDate($proformainvoice['proforma_date']);
				}
				
				$this->Proformainvoice->save($proformainvoice);

	            $item = $this->request->data['Proformainvoice']['item'];
	            $del_ids = $this->request->data['removal_id'];
	            if(isset($del_ids) && !empty($del_ids)){
	            	$del=array($del_ids);
		         	$condition = array('Proformaitem.id IN ('.$del_ids.')');
		        	$this->Proformaitem->deleteAll($condition,false);
		        }
		        
		        $result = array_diff_assoc($this->old['Proformainvoice'],$this->request->data['Proformainvoice']);/*Get only changed fields*/
		        
		        $totalamount = 0;
		        foreach($item  as $keyitem => $items){	
	           		if(isset($items['id'])){
	           		 	//$this->Proformaitem->updateAll($items,array('Proformaitem.id' => $items['id']));
						$this->Proformaitem->id = $items['id'];					
	           			$item[$keyitem]['amount'] = $items['qty'] * $items['price'];
		           		$totalamount+=$item[$keyitem]['amount'];
						$this->Proformaitem->save($item[$keyitem]);
	           		}
	           		else{
	           			$item[$keyitem]['proforma_id'] = $id;
	           			$item[$keyitem]['amount'] = $items['qty'] * $items['price'];
		           		$totalamount+=$item[$keyitem]['amount'];
	           			$this->Proformaitem->saveall($item[$keyitem]);
					}
	            }
	       	    if($this->Proformainvoice->id){
	       	    	$proformainvoice['total'] = $totalamount;
	           		$this->Proformaitem->saveField('total', $totalamount);
	           	}
				$proformainvoice['item'] = $item;/*updated value so we pass it into activity log*/
	           	$proformainvoice['Proformaitem'] = $item;
	           	$Activities->addlog($this->loggedin_user_info['id'],'Proformainvoice','Edit',$result);/*Add Data in Log*/
				$msg = 'Proformainvoice has been Updated';
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));           
	            //$this->redirect('index');
				
				$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
                $response['status'] = 'success';
                $response['message'] = $msg;
                $response['redirect'] = $redirect;
                echo json_encode($response);	            
        	}
	        else{
                $Proformainvoice = $this->Proformainvoice->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Proformainvoice could not be saved. Please, try again.';
                $response['data'] = compact('Proformainvoice');
                echo json_encode($response);	        	
	        }	           
		}		
	}

	/*
   	 * Name: getDispatchcode via Ajax
   	 * Use: Fetch Dispatch Location for Approval Proformainvoice .
	 */
	public function getDispatchcode($id = null) {
		$this->loadmodel('Dispatch');
		$cntdata = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $id)));
		$count = sizeof($cntdata);
		
		$this->autoRender = false;
   		$dispatch_data = $this->Dispatch->find('all',array('conditions'=> array('Dispatch.id'=>$id),'fields'=>array('Dispatch.code')));
   		$i = 1;
   		if(isset($id) && !empty($id)){
   			$dc = $dispatch_data[0]['Dispatch']['code'].'-';
   			//echo $dc;exit;
   		}else {
   			$dc = '';
   		}
   		
   		$rand = $this->rand_str(1,1).$this->rand_str(2,2).$this->rand_str(1,1);
   		//echo $rand;
   			if($count == 0 && !empty($id))
   			{
   				++$count;
				echo $data = $dc.$count;
   			}else if($count > 0 && !empty($id)){
   				$total = $count + 1;
   				echo $data = $dc.$total;
   			}else if(!isset($dc)){
   				echo $data = '';
   			}else {
   				echo $data = '';
   			}
		exit;
	}


	function getDispatchID($value) {
		$this->loadmodel('Dispatch');
		$dispatch = $this->Dispatch->find('first', array(
   			'conditions' => array('location' => $value),
   			'fields' => array('id','code'), 
    		'recursive' => -1
		));		
		$cntlocation = $this->Proformainvoice->find('all', array('recursive' => -1, 'fields' => array('dispatch_id','Proformainvoice.dispatchcode'), 'conditions' => array('dispatch_id' => $dispatch['Dispatch']['id'])));
			$count = sizeof($cntlocation);
		if($count >= 1){
			++$count;
			$location = $dispatch['Dispatch']['code'].'/'.$count;
			$dis_id = $dispatch['Dispatch']['id'];
			return  array($location, $dis_id);
		}else{
			$location = $dispatch['Dispatch']['code'].'/1';
			$dis_id = $dispatch['Dispatch']['id'];
			return array($location, $dis_id);
		}	
		if(empty($dispatch)){
			return;
		}	
	}		



	/**
   	 * Name: getUserID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getUserID($value) {
		$this->loadmodel('User');
		$userdata = $this->User->find('first', array(
   			'conditions' => array('email' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($userdata)){
			return $userdata['User']['id'];
		}
		else{
			return;
		}
	}
	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGrades($id = null) {
		$this->loadmodel('Grade');
		$this->autoRender = false;
		$grade_data = $this->Grade->find('all',array('conditions'=> array('Grade.standard_id'=>$id),'fields'=>array('Grade.id','Grade.grade_name'),'recursive'=>-1));
   	//	pr($grade_data);exit;
   		echo '<select>';
		echo '<option value="">Select Grade</option>';
		foreach($grade_data as $grades) { 
	  		echo '<option value="'.$grades['Grade']['id'].'">'.$grades['Grade']['grade_name'].'</option>'."\n";
	  	}
		echo '</select>';
		exit;
	}

	/*
   	 * Name: change_procat via Ajax
   	 * Use: Fetch product name,unit,standards,size for particular category.
	 */
	function change_procat($productcategoryid) {
		
		$this->autoRender = false;
		$returndata = array();
   		
		$this->loadmodel('Productcategory');
   		$category_data = $this->Productcategory->findById($productcategoryid,array('Productcategory.producttaxonomy_id','Productcategory.productname','Producttaxonomy.unit'));   		
   		$producttaxonomy_id = $category_data['Productcategory']['producttaxonomy_id'];
   		if($category_data['Productcategory']['producttaxonomy_id'] == 2){$producttaxonomy_id = 1;}
		$returndata['productname'] = $category_data['Productcategory']['productname'];
		$returndata['unit'] = $category_data['Producttaxonomy']['unit'];
		/*fetch standard associate with particular product category */
   		$this->loadmodel('Standard');
   		$standard_data = $this->Standard->find('all',array('fields'=>array('Standard.id,standard_name','Standard.procatid'),'recursive'=>-1));
   		
   		$standard = '<select>';
   		$standard .= '<option value="">Select Standard</option>';
   		if(!empty($standard_data)){
	   		foreach($standard_data as $std_data){
	   			$product = explode(",",$std_data['Standard']['procatid']);

	   			if (in_array($productcategoryid, $product)) {
					$standard .= '<option value="'.$std_data['Standard']['id'].'">'.$std_data['Standard']['standard_name'].'</option>'."\n";
		  		} 
			}
		}
		$standard .= '</select>';
		$returndata['standard'] = $standard;

   		$this->loadmodel('Size');
   		$size_datas = $this->Size->find('all',array('conditions'=>array('producttaxonomy_id'=>$producttaxonomy_id),'recursive'=>-1));
   		
   		$size = '<select>';
   		$size .= '<option value="">Select Size</option>';
   		if(!empty($size_datas)){
   			foreach($size_datas as $size_data){
				$size .= '<option value="'.$size_data['Size']['id'].'">'.$size_data['Size']['gdmm'].'</option>'."\n";
			}
		}
		$size .= '</select>';
		$returndata['size'] = $size;
		echo json_encode($returndata);
		exit;
	}

	/*
   	 * Name: getStandards via Ajax
   	 * Use: Fetch Standards for particular Standard .
	 */
	function getStandards($id) {
		$this->loadmodel('Standard');
		$this->autoRender = false;
		
   		$standard_data = $this->Standard->find('all',array('fields'=>array('Standard.id,standard_name','Standard.procatid'),'recursive'=>-1));
   		
   		echo '<select>';
   		echo '<option value="">Select Standard</option>';
   		foreach($standard_data as $std_data){
   			$product = explode(",",$std_data['Standard']['procatid']);
   			if (in_array($id, $product)) {
			echo '<option value="'.$std_data['Standard']['id'].'">'.$std_data['Standard']['standard_name'].'</option>'."\n";
	  		} 
		}
		echo '</select>';
		exit;
	}
	/*
   	 * Name: getgdmm via Ajaxb
   	 * Use: Fetch gdmm list.
	 */	
	function getgdmm() {
		$this->loadmodel('Size');
		$this->autoRender = false;
   		$gdmm_data = $this->Size->find('all',array('recursive'=>-1));
   		echo '<select>';
   		echo '<option value="">Select Size</option>';
   		foreach($gdmm_data as $gdmmdata){
			echo '<option value="'.$gdmmdata['Size']['id'].'">'.$gdmmdata['Size']['gdmm'].'</option>'."\n";
		}
		echo '</select>';
		exit;
	}
	function getnb($id=null) {		
		$data = array();
		$this->loadmodel('Size');
		$this->loadmodel('Physical');
		$this->autoRender = false;
		if(empty($id)){
			$data = '';
		}
		$gd_data = $this->Size->find('first',array('conditions'=> array('Size.id'=>$id),'recursive'=>-1));
		$density_data = $this->Physical->find('first',array('conditions'=>array('Physical.grade_id'=>$_POST['grade']),'recursive'=>-1));
		if(!empty($id)){
			$data['gdnb'] = $gd_data['Size']['gdnb'];
			if(!empty($density_data)){
				$data['finalratio'] = $gd_data['Size']['calc_ratio'] * $density_data['Physical']['density'];	   		
	   			$data['ratio'] = $gd_data['Size']['calc_ratio'];
	   			$data['density'] = $density_data['Physical']['density'];
	   		}			
		}else{
			$data = '';
		}
		echo json_encode($data);
		exit;
	}
	/* Get Calculation Ration oon change of Gradde */
	function getRatio($id=null) {		
		$data = array();
		$this->loadmodel('Size');
		$this->loadmodel('Physical');
		$this->autoRender = false;
		if(empty($id)){
			$data = '';
		}
		$size_data = $this->Size->find('first',array('conditions'=> array('Size.id'=>$_POST['size']),'recursive'=>-1));
		$density_data = $this->Physical->find('first',array('conditions'=>array('Physical.grade_id'=>$id),'recursive'=>-1));
		if(!empty($density_data)){
			$data['finalratio'] = $size_data['Size']['calc_ratio'] * $density_data['Physical']['density'];
	   		$data['size'] = $size_data['Size']['gdnb'];
	   		$data['ratio'] = $size_data['Size']['calc_ratio'];
	   		$data['density'] = $density_data['Physical']['density'];
	   	}else {
	   		$data = '';
	   	}
		echo json_encode($data);
		exit;
	}
	function delete($id = null)
	{
		$this->loadmodel('Proformaitem');
		$id = base64_decode($id);	
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Proformainvoice->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Proformainvoice has been Deleted successfully</div>'));
		}
		$this->Proformaitem->deleteAll(array('Proformaitem.proforma_id' => $id), true);
		return $this->redirect(array('action' => 'index'));
	}
	/**
	* Name : getclients
   	* Use : Getting clients based on suggestion
   	*/
	public function getclients()
	{
		$this->loadmodel('Client');
		$this->autoRender = false;
		$resp = $this->Client->find('all',array('conditions'=>array('company_name LIKE' => "%".$_REQUEST['q']."%"),'fields'=>array('Client.company_name','Client.id')));
		$final_resp = array();
		foreach($resp as $key => $value){
			$final_resp[] = array('text'=>$value['Client']['company_name'],'id'=>$value['Client']['id']);
		}
		echo json_encode($final_resp);
	}

	/**
	* Name : getclientdetail
   	* Use : Fetch client details
   	*/
	public function getclientdetail(){
		$this->autoRender = false;
		/*if($_POST['id']!=''){
			$this->loadmodel('Client');			
			$recursive = -1;
			$resp = $this->Client->find('first',array('fields'=>array('address1'),'recursive'=>-1,'conditions'=>array('id' => $_POST['id'])));
			echo nl2br($resp['Client']['address1']);
		}
		else{
			echo '';
		}*/
		$address_arr = array();
		if($_POST['id']!=''){
			$this->loadmodel('Address');
			$resp = $this->Address->find('all',array('conditions'=>array('clientid' => $_POST['id'])));
			foreach($resp as $address){
				$id = $address['Address']['id'];
				$clientid = $address['Address']['clientid'];
				$streetaddress = $address['Address']['streetaddress'];
				$city = $address['Address']['city'];
				$state = $address['Address']['state'];
				$country = $address['Address']['country'];
			 $address_arr[] = array("id" => $id, "clientid" => $clientid,"streetaddress" => $streetaddress,"city"=>$city,"state"=>$state,"country"=>$country);
			}
			if(!empty($address_arr)){
				echo json_encode($address_arr);
				die;
			}else{
				echo '0';
			}
		}
	}

	/**
   	 * Name: generatepdf
   	 * Use: generate pdf file of record
     * @param int $id id of record
     * @return pdf file   	 
	*/
	public function generatepdf($id = null) {

		
		$this->pdfConfig = array(
			'filename' => 'invoice',
			'download' => $this->request->query('download')
		);
		$data = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=> $id),'recursive' => 2));
    	$this->set('data', $data);
    	/* Make sure the controller doesn't auto render. */
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		//$view->set('text', 'Hello World');		
		$view->viewPath = 'Proformainvoices';
		ini_set('memory_limit', '1024M');
		$html = $view->render('generatepdf');
   		$this->set('post',$html);

   		
	}
	
	/*
	Use : Send email of Custom function of Proforma invoce
	Name : sendEmailCoordinator
	*/
	public function sendEmailCoordinator($id = null){
		
		$this->layout = 'default';
		$this->loadmodel('Client');
		$id = $this->Proformainvoice->getLastInsertID();
		$data = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=> $id),'recursive' => 2));		
		$this->set('data', $data);
		/* get co-ordinator name */
		$co_email = $this->Client->find('first',array('conditions'=>array('Client.id'=>$data['Client']['id'])));
		/* Make sure the controller doesn't auto render. */
		$this->autoRender = false;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		//$view->set('text', 'Hello World');
		$view->viewPath = 'Proformainvoices';/* controller name, a folder exists in views folder */
		 
		/* Grab output into variable without the view actually outputting! */
		$view_output = $view->render('generatepdf');/* a file exists in views/folder/file_name.ctp */

        ini_set('memory_limit', '1024M');
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF(); 
		$customPaper = array(0,0,960,960);
		$dompdf->set_paper($customPaper);
		$log_first = AuthComponent::user('first_name');
		$log_last = AuthComponent::user('last_name');
		//$dompdf->set_paper = 'A4';
		$dompdf->load_html(utf8_decode($view_output), Configure::read('App.encoding'));
		$dompdf->render();
		$pdfname = $dompdf->output();
		//$file_location = $_SERVER['DOCUMENT_ROOT']."/app/webroot/uploads/invoices/".$data["Proformainvoice"]["id"].".pdf";
		$file_location = $_SERVER['DOCUMENT_ROOT']."/app/webroot/uploads/invoices/Proformainvoice-".$data["Proformainvoice"]["id"].".pdf";
		//$file_location = $_SERVER['DOCUMENT_ROOT']."/shalcoerp/app/webroot/uploads/invoices/".$data[0]["Proformainvoice"]["id"].".pdf";
		file_put_contents($file_location,$pdfname);
		$Email = new CakeEmail();
		$Email->attachments($file_location);
		$Email->to($co_email['Coordinator']['email']);
		$Email->subject('Verify Proforma Invoice');
		$Email->from(array('shalco@gmail.com' => 'My Site'));
		$Email->emailFormat('html');
		$url = WEBSITE_PATH."proformainvoices/getVerified/".$data["Proformainvoice"]["id"];
		$verify_url = '<a href="'.$url.'">'.$url.'</a>';
						
		$Email->send('Respected '.$co_email['Coordinator']['first_name'].' '.$co_email['Coordinator']['last_name'].'<br><br>'.
				  'As per task assigned me, I have created proforma invoice<br>
				  Please check detail view either by attachment or from here :<br>
				  Please verify if by click on below link :<a href="'.$url.'">'.$url.'</a>'.'<br>'.'
				  Thanks & Regards<br>'.$log_first.' '.$log_last);
		//$this->redirect('index');		
	}

	

   	/*
	* Name : rejectproformainvoice
	* Use : reject proforma invoice
   	*/
   	function rejectproformainvoice()
   	{	
   		if(empty($_POST['id'])) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}
   		$this->autoRender = false;
   		$reject_date = date('Y-m-d H:i:s');
   		$rejectby = $this->Auth->user('id');   		

	   	$data = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=>$_POST['id']),'fields'=>array('Proformainvoice.status'),'recursive'=>-1));		
	   	$status = $data['Proformainvoice']['status']; 
	   	/*Reject if status is pending (0), so check if status is pending or not*/
	   	if($status == 0){
   			$rejectdata  = array('rejectcomment' => $_POST['rejectcomment'],'status'=>2,'reject_date'=>$reject_date,'rejectby'=>$rejectby);
   	   		$this->Proformainvoice->id = $_POST['id'];
	   		$this->Proformainvoice->save($rejectdata);	
	   }
	   return;
   	}

   	/*
	* Name : approveproformainvoice
	* Use : approve proforma invoice
   	*/
   	function approveproformainvoice()
   	{	
   	    $this->autoRender = false;
   		if(empty($_POST['id'])) {
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
		}
			
   		$this->loadmodel('Order');
   		$this->loadmodel('Orderitem');

		$order_iddata = $this->Order->find('first',array('fields'=>array('Order.orderno','Order.fy'),
			'conditions'=>array('ismanual'=>0),'recursive'=> -1,'order' => 'Order.id DESC'));
		$date = date('Y-m-d');/*default date*/		
		$new_finan_year = $this->calculateFinancialYearForDate($date);
		if(!empty($order_iddata) && $order_iddata['Order']['fy'] == $new_finan_year){
			//$ord_id = $order_iddata['Order']['orderno'];			
			$exp_id = explode('/',$order_iddata['Order']['orderno']);			
			$ord_id = $exp_id[1] + 1;
			$generate_order = 'SHO/'.$ord_id.'/'.$this->calculateFinancialYearForDate($date);;			
		}	
		else{
			$year = date("Y");
			$generate_order = 'SHO/'.'1/'.$this->calculateFinancialYearForDate($date);;			
		}

   		$current_time = date('Y-m-d H:i:s');
   		$logged_user_id = $this->Auth->user('id'); 

   		$getproforma = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=>$_POST['id']),'recursive' => 1));

   		/*Check proforma already accepted or rejected means not pending state than redirect */
   		if($getproforma['Proformainvoice']['status'] == 1 || $getproforma['Proformainvoice']['status'] == 2){
			$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));
   		}
		/*Update status of proforma invoice*/
		if(!empty($_FILES['file']['name'])){
            move_uploaded_file($_FILES['file']['tmp_name'],'uploads/clientpo/'.$_FILES['file']['name']);
            $po_upload = $_POST['caption_avatar'];
        }else {
            $po_upload = '';
        }
        
        $clientpodate = '0000-00-00';
		if(isset($_POST['clientpodate'])){
			$clientpodate = date('Y-m-d', strtotime(str_replace('/','-',$_POST['clientpodate'])));	
		}		
   		$status_update  = array('id' => $_POST['id'],'status' => 1, 'approval_date'=> $current_time,'approvalby' => $logged_user_id,'orderno'=> $generate_order,'dispatch_id'=>$_POST['dispatch_id'],'dispatchcode' => $_POST['dispatchcode'],'clientpo'=>$_POST['clientpo'],'clientpodate' => $clientpodate,'acceptcomment' => $_POST['acceptcomment'],'po_upload'=>$po_upload);
   		
   		//$this->Proformainvoice->id = $_POST['id'];
	   	$this->Proformainvoice->save($status_update);

	   	/*Generate Order from proforma invoice*/
	   		$proformadata = $getproforma['Proformainvoice'];
	   		$orderdata['proformaid'] = $proformadata['id'];
	   		$orderdata['orderno'] = $generate_order;
	   		$orderdata['fy'] = $this->calculateFinancialYearForDate($date);
	   		$orderdata['delivery_type'] = $proformadata['delivery_type'];
	   		$orderdata['orderdate'] = date('Y-m-d');
	   		$orderdata['tod'] = $proformadata['tod'];
	   		$orderdata['top'] = $proformadata['top'];
	   		$orderdata['bill_to'] = $proformadata['bill_to'];
	   		$orderdata['ship_to'] = $proformadata['ship_to'];
	   		$orderdata['billaddress_id'] = $proformadata['billaddress_id'];   		
	   		$orderdata['shipaddress_id'] = $proformadata['shipaddress_id'];   	   		
	   		$orderdata['bank_id'] = $proformadata['bank_id'];
	   		$orderdata['total'] = $proformadata['total'];
	   		$orderdata['dispatch_id'] = $_POST['dispatch_id'];
	   		$orderdata['dispatchcode'] = $_POST['dispatchcode'];
	   		$orderdata['clientpo'] = $_POST['clientpo'];
	   		$orderdata['clientpodate'] = $clientpodate;
	   		$orderdata['acceptcomment'] = $_POST['acceptcomment'];
	   		$orderdata['local'] = $proformadata['local'];
	   		$orderdata['priceid'] = $proformadata['priceid'];	
	   		$orderdata['quantity'] = $proformadata['quantity'];
	   		$orderdata['delivery_period'] = $proformadata['delivery_period'];
	   		$orderdata['dimensions'] = $proformadata['dimensions'];
	   		$orderdata['certifications'] = $proformadata['certifications'];
	   		$orderdata['comment'] = $proformadata['comment'];
	   		$orderdata['modified'] = $current_time;
	   		$orderdata['createdby'] = $logged_user_id;
	   		$orderdata['modifiedby'] = $logged_user_id;   		
			if(isset($_POST['caption_avatar'])){
				$orderdata['po_upload'] = $_POST['caption_avatar']; 	
			}
			$this->Order->save($orderdata);
			
	   	/*Generate Order item from proforma invoice item*/
	   		$getlast_id = $this->Order->getLastInsertID();

	   		foreach($getproforma['Proformaitem'] as $proforma_items){
	   			$proforma_items['orderid'] = $getlast_id;
	   			$proforma_items['proformaitemid'] = $proforma_items['id'];
	   			unset($proforma_items['id']);	   			
	   			$this->Orderitem->saveall($proforma_items);	   			
	   		}
	   	return 1;
   	}

   	/*
	Use : Send email of Custom function of Proforma invoce
	Name : sendEmailClient
	*/
	public function sendEmailClient($id = null){
		
		$this->layout = 'default';
		$this->loadmodel('Client');
		$data = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=> $id),'recursive' => 2));		
		$this->set('data', $data);
		/* get co-ordinator name */
		$client_email = $this->Client->find('first',array('conditions'=>array('Client.id'=>$data['Client']['id'])));
		/* Make sure the controller doesn't auto render. */
		$this->autoRender = false;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		//$view->set('text', 'Hello World');
		$view->viewPath = 'Proformainvoices';/* controller name, a folder exists in views folder */
		 
		/* Grab output into variable without the view actually outputting! */
		$view_output = $view->render('generatepdf');/* a file exists in views/folder/file_name.ctp */

        ini_set('memory_limit', '1024M');
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF(); 
		$customPaper = array(0,0,960,960);
		$dompdf->set_paper($customPaper);
		
		//$dompdf->set_paper = 'A4';
		//$test =$dompdf->set_base_path(realpath(APPLICATION_PATH . '/css/pdf.css'));
		$dompdf->load_html(utf8_decode($view_output), Configure::read('App.encoding'));
		$dompdf->render();
		$pdfname = $dompdf->output();
		$file_location = $_SERVER['DOCUMENT_ROOT']."/app/webroot/uploads/invoices/Proformainvoice-".$data["Proformainvoice"]["id"].".pdf";
		//$file_location = $_SERVER['DOCUMENT_ROOT']."/shalcoerp/app/webroot/uploads/invoices/".$data["Proformainvoice"]["id"].".pdf";
		//$file_location = $_SERVER['DOCUMENT_ROOT']."/shalcoerp/app/webroot/uploads/invoices/".$data[0]["Proformainvoice"]["id"].".pdf";
		file_put_contents($file_location,$pdfname);
		$Email = new CakeEmail();
		$Email->attachments($file_location);
		$Email->to($client_email['Client']['email']);
		$Email->subject('Shalco Industry : Proforma Invoice');
		$Email->from(array('shalco@gmail.com' => 'Shalco Industry'));
		$Email->emailFormat('html');
		/*$Email->send();*/
		//$this->redirect('index');		
	}

	/**
	* Name : getVerified
   	* Use : Verify proforma invoice
    * @param int $id id of record
    * @return void
   	*/
   	function getVerified($id) {
	   	$this->autoRender = false;
	   	$data = $this->Proformainvoice->find('first',array('conditions'=> array('Proformainvoice.id'=>$id),'fields'=>array('Proformainvoice.isverified,Proformainvoice.id'),'recursive' => -1));
		//pr($data);exit;
	   	$verified = $data['Proformainvoice']['isverified'];
	   	$verified_user = $this->Auth->user('id');
	   	/*Check if proforma is already verified or not, if not verified than we send email and update info*/
	   	if($verified == 0){ 
	   		$update = array('isverified' => 1, 'verifiedby'=> $verified_user,'verified_date' => date('Y-m-d H:i:s'));
	   		$this->Proformainvoice->id = $id;
	   		$this->Proformainvoice->save($update);
	   		$this->sendEmailClient($data['Proformainvoice']['id']);
		}else{$this->redirect(array('controller' => 'proformainvoices', 'action' => 'index'));} 
	}

	/*
		Name : getProformaNO
		Use : Get Unique Proforma NO
	*/
	function getProformaNO($type=null,$date=null){ 
		if($type == 'ajax'){ /*For on change event inside add and edit chalan form*/
			$this->autoRender=false;
		}
		/*Default current date*/
		if(empty($date)){$date = date('Y-m-d');}
		else{
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $date)));
		}  
		/*If user fill date than override default date with user entered date*/
		if(isset($_POST['date'])){ 
			$date=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
		}

		$new_finan_year = $this->calculateFinancialYearForDate($date);
		$proforma_id = $this->Proformainvoice->find('first',array('conditions'=>array('Proformainvoice.fy' => $new_finan_year),'fields'=>array('Proformainvoice.proforma_no','Proformainvoice.fy'),'recursive'=> -1,'order' => 'Proformainvoice.id DESC'));
		
		if(!empty($proforma_id) && $proforma_id['Proformainvoice']['fy'] == $new_finan_year){
			$pro_id = $proforma_id['Proformainvoice']['proforma_no'];
			$exp_id = explode('/',$pro_id);
			
			$proforma_id = $exp_id[1] + 1;
			$pr_id = 'SHPI/'.$proforma_id.'/'.$this->calculateFinancialYearForDate($date);
			return $pr_id;
		}	
		else{
			$pr_id = 'SHPI/'.'1/'.$this->calculateFinancialYearForDate($date);			
			return $pr_id;
		}
	}

	/*
		Name : getManualID
		Use : Get Unique Proforma Id for Invoice for Import
	*/
	function getManualID(){
		$manual_id = $this->Proformainvoice->find('all',array('conditions'=> array('ismanual' => 1),'limit' => 1,'order' => 'Proformainvoice.id DESC'));
			$this->request->data['Proformainvoice']['proforma_date'] = date('d/m/Y');
			if(!empty($manual_id)){
				$pro_id = $manual_id[0]['Proformainvoice']['proforma_no'];
				$exp_id = explode('/',$pro_id);
				$manual_cut = substr($exp_id[2], 4); 
				$manual_id = $manual_cut + 1;
				$year = date("Y");
				$manual = 'SHL/OL/'.$manual_id.'/'.$this->getFinancialyear();
				return $manual;
			}
			else{
					$year = date("Y");
					$manual = 'SHL/OL/'.'1/'.$this->getFinancialyear();
					return $manual;
			}
	}
	/*
   	 * Name: order
   	 * Use: Approval Order listing page.
	*/
	function order() {
		$this->loadmodel('Client');
		$this->loadmodel('Proformaitem');	
        $this->set('pageTitle',' Order List');
        $this->set('ajaxaction','orderlisting');
         if ($this->request->is('post')) {
         $this->Proformainvoice->create();
            $filename = WWW_ROOT.$this->request->data['Proformainvoice']['uploadfile']['name']; 
           
			if (($handle = fopen($filename, "r")) !== FALSE) {
				$data = array();
				$item = array();
				$errors = '';
				$i = 0;
				$r = 1; /*pointing excel sheet row number, which is useful while set error on line number*/
				$sum = 0;
    			while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
    				if($i == 1)
    				{
	       				$data['proforma_no'] = $this->getManualID();
	       				if(!empty($data['proform_date'])){
	       					$data['proforma_date'] = $row[0];
	       				}
	       				else{
	       					$data['proforma_date'] =  date('Y-m-d');
	       				}
	       				if(!empty($row[4])){
	       					$data['tod'] = $row[4];
	       				}
	       				else{
	       					$errors = '<li>Shipping Terms is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[5])){
	       					$data['top'] = $row[5];
	       				}
	       				else{
	       					$errors .= '<li>Terms of payment is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[1])){	       					
	       					if($this->getClientID($row[1])!=''){
	       						$data['bill_to'] = $this->getClientID($row[1]);	
	       					}
	       					else{
	       						$errors .= '<li>Client name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Client name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[2])){
	       					$data['ship_to'] = $row[2];	
	       				}
	       				else{
	       					$errors .= '<li>Shipping client is missing at line no '.$r.'</li>';			
	       				}
	       				if(!empty($row[3])){
	       					$data['ship_address'] = $row[3];	
	       				}
	       				else{
	       					$errors .= '<li>Shipping address is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[6])){	       					
	       					if($this->getBankID($row[6]) != ''){
	       						$data['bank_id'] = $this->getBankID($row[6]);	
	       					}
	       					else{
	       						$errors .= '<li>Bank name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Bank name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[7])){	       					
	       					if($this->getUserID($row[7]) != ''){
	       						$data['approvalby'] = $this->getUserID($row[7]);	
	       					}
	       					else{
	       						$errors .= '<li>Approval name is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Approval name is missing at line no '.$r.'</li>';
	       				}
	       				if(!empty($row[8])){	       					
	       					if($this->getDispatchID($row[8]) != ''){
	       						$dispa_id = $this->getDispatchID($row[8]);	
	       						$data['dispatch_id'] = $dispa_id[1];
	       					}
	       					else{
	       						$errors .= '<li>Dispatch location is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					
	       				}
	       				else{
	       					$errors .= '<li>Dispatch location is missing at line no '.$r.'</li>';
	       				}
	       				$data['status'] = 1;
	       				$data['approval_date'] = date('Y-m-d H:i:s');
	       				$data['dispatchcode'] = $dispa_id[0];
	       				$data['ismanual'] = 1;
	       			}

	       			if($i > 3){
					//	$item[$i]['proforma_id'] = $last_proformaid;
	       				if(!empty($row[0])){
	       					if($this->getStandardID($row[0])!='') {
	       						$item[$i]['standard_id'] = $this->getStandardID($row[0]);	
	       					}
	       					else{
	       						$errors .= '<li>Standard is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}
	       				}
	       				else{
	       					$errors .= '<li>Standard is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[1])){	       					
	       					if($this->getGradeID($row[1])!=''){
	       						$item[$i]['grade_id'] = $this->getGradeID($row[1]);	
	       					}
	       					else{
	       						$errors .= '<li>Grade is not exist in master module please insert it at line no '.$r.'</li>';	
	       					}	       					       					
	       				}
	       				else{
	       					$errors .= '<li>Grade is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[2]) && $row[3]){
	       					$item[$i]['size_id'] = $this->getSizeID($row[2],$row[3]);	
	       				}
	       				else{
	       					$errors .= '<li>Size is missing at line no '.$r.'</li>';	
	       				}
	       				if(!empty($row[4])){
	       					$item[$i]['length'] = $row[4];	
	       				}
	       				else{
	       					$errors .= '<li>Length is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[5])){
	       					$item[$i]['qty'] = $row[5];
	       				}
	       				else{
	       					$errors .= '<li>Quantity is missing at line no '.$r.'</li>';		
	       				}
	       				if(!empty($row[6])){
	       					$item[$i]['price'] = $row[6];	
	       				}
	       				else{
	       					$errors .= '<li>Price is missing at line no '.$r.'</li>';			
	       				}
						if(!empty($row[7])){
							$item[$i]['amount'] = $row[7];
							$sum += $item[$i]['amount'];
						}
						else{
							$item[$i]['amount'] = $row[5] * $row[6];
							$sum += $item[$i]['amount'];
						}
						if(!empty($row[8])){
							$item[$i]['productname'] = $row[8];
						}
						else{
							$errors .= '<li>Product name is missing at line no '.$r.'</li>';
						}
					}
					$i++;
					$r++;
		    	}
		    	if(!empty($errors)){
		    		$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-times"></i> Please try Again.<ul>'.$errors.'</ul></div>'));
		    			return false;
	       		}
	       		
		    	$data['total'] = $sum;
				if($this->Proformainvoice->save($data)){
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Order imported Successfully.</div>'));
				}
				$last_proformaid = $this->Proformainvoice->getLastInsertID();
				if(!empty($last_proformaid)){
					foreach($item as $items){
						$items['proforma_id'] = $last_proformaid;
						$this->Proformaitem->saveall($items);
					}
				}
				move_uploaded_file($this->request->data['Proformainvoice']['uploadfile']['tmp_name'],WWW_ROOT.'files/orders/'.time().$this->request->data['Proformainvoice']['uploadfile']['name']);
				fclose($handle);
			}	
			
        }
	}

	 /**
   	 * Name: getClientID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getClientID($value) {
		$this->loadmodel('Client');
		$clientdata = $this->Client->find('first', array(
   			'conditions' => array('company_name' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($clientdata)){
			return $clientdata['Client']['id'];
		}
		else{
			return;
		}
	}


	 /**
   	 * Name: getBankID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getBankID($value) {
		$this->loadmodel('Bank');
		$bank = $this->Bank->find('first', array(
   			'conditions' => array('bank_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));		
		if(!empty($bank)){
			return $bank['Bank']['id'];
		}
		else{
			return;
		}		
	}

	 /**
   	 * Name: getStandardID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getStandardID($value) {
		$this->loadmodel('Standard');
		$std = $this->Standard->find('first', array(
   			'conditions' => array('standard_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($std)){
			return $std['Standard']['id'];
		}
		else{
			return;
		}		
	}	

	 /**
   	 * Name: getGradeID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getGradeID($value) {
		$this->loadmodel('Grade');
		$grd = $this->Grade->find('first', array(
   			'conditions' => array('grade_name' => $value),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($grd)){
			return $grd['Grade']['id'];
		}
		else{
			return;
		}		
	}		 

	 /**
   	 * Name: getSizeID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */
	function getSizeID($gdmm,$gdnb) {
		$this->loadmodel('Size');
		$log_id = $this->Auth->user('id');
		$size = $this->Size->find('first', array(
   			'conditions' => array('gdmm' => $gdmm,'gdnb' => $gdnb),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		if(!empty($size)){
			return $size['Size']['id'];	
		}
		else{
			$exp_gdmm = explode(' ',$gdmm);
			$datarow['od_mm'] = $exp_gdmm[0];
			$datarow['wt_mm'] = $exp_gdmm[3];
			$exp_gdnb = explode(' ',$gdnb);
			$datarow['od_nb'] = $exp_gdnb[0];
			$datarow['wt_nb'] = $exp_gdnb[2];
			$subtract_ratio = $exp_gdmm[0] - $exp_gdmm[3];
			$mult_ratio = $subtract_ratio * $exp_gdmm[3];
			$datarow['calc_ratio'] = $mult_ratio * 0.0248;
			$datarow['gdmm'] = $gdmm;
			$datarow['gdnb'] = $gdnb;
			$datarow['createdby'] = $log_id;
		//	pr($datarow);
			$this->Size->create();
			$this->Size->save($datarow);
		//	$log = $this->Size->getDataSource()->getLog(false, false);
		//	debug($log);
		
			$last_sizeid = $this->Size->getLastInsertID();
			//pr($last_sizeid);
			//exit;
			return $last_sizeid;

		}
		
	}

	/* 
	Name : Print 
	Use  : Ms word documrnt for dynamic data 
	*/
	public function printoword($id = null){
		$file = new File(APP.DS.'webroot'.DS.'css'.DS.'custom.css', false); //1
    	$this->set('inlineCss',$file->read()); //2
    	$file->close(); 
    	$bootstrap = new File(APP.DS.'webroot'.DS.'global/plugins/bootstrap/css'.DS.'bootstrap.min.css', false);
    	$this->set('bootstrap',$bootstrap->read()); //2
    	$file->close(); 
    	$component = new File(APP.DS.'webroot'.DS.'global/css'.DS.'components.min.css', false);
    	$this->set('component',$component->read()); //2
    	$file->close(); 
		$this->layout = 'msword';
		$id = base64_decode($id);
		$proforma = $this->Proformainvoice->find('first', array(
   			'conditions' => array('Proformainvoice.id' => $id ), // URL to fetch the required page
    		'recursive' => 2
		));		
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$proforma);
		Configure::write('debug',0);
		/*Redirect if ID empty */
		
		/*Fetch data of particular ID*/
		/*Redirect if ID not exist in database*/
		
		/*$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'proformainvoices/">Proforma Invoices</a><i class="fa fa-circle"></i></li>');		
		$this->set('data',$proforma);
		$this->set('pageTitle',' Proforma Invoice View');
	    */
	    //Configure::write('debug',0);
	}
	/*
	*
	* Name : export
	* Use : Export the Proformainvoice data
	*/
	function export($id = null) {
		$this->layout = false;
		$this->autoRender=false;
		ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
		$reportname = '';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objRichText = new PHPExcel_RichText();

		$results = $this->Proformainvoice->find('first',array('conditions'=>array('Proformainvoice.id' => $id),'recursive'=>2));
		$reportname = 'Proformainvoice-'.$results['Proformainvoice']['proforma_no'];
		
		if($results['Price']['sign'] == '&#8377;'){
			$sym = '₹';
		}else if($results['Price']['sign'] == '&#36'){
			$sym = '$';
		}else if($results['Price']['sign'] == '&#8364;'){
			$sym = '€'; 
		}else {
			$sym = '£';
		}

		$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$style_vertical = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP));
		$style_vertical_center = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
		$hori_right = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
		$hori_left = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));

			$border_style = array('borders' => array('outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_bottom = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_right = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
			$border_left_thick = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_top_thick = array('borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
			$border_bottom_thick = array('borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));

			$setting= $this->Session->read('setting_data');
			$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'img/print_logo.png';
			$objDrawing->setPath($path);
			$objDrawing->setCoordinates('A1');
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "CIN NO : ". $setting["Setting"]["cin_no"]."\n"."Office: 24, 2nd Floor, Dayabhai Building"."\n"."Gulalwadi, Mumbai - 400004"."\n". "Maharastra, (INDIA)."."\n"."Tel: + 91-22-40416214/ 40416221/ 25"."\n". "Fax: +91-22-23452161"."\n"."Email: info@shalco.in Web: www.shalco.in");
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F1:J6');
			$objPHPExcel->getActiveSheet()->getStyle('F1:J6')->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('F1:F6')->applyFromArray($hori_left);
			$objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E6');

			$dyn_cell = 7;
			$for_next_cell = $dyn_cell + 3;
			$cl_address = str_replace('<br/>', "\n", searchFromAddress($results['Proformainvoice']['billaddress_id'], $results['Client']['Address']));
			$shp_address = str_replace('<br/>', "\n ", searchFromAddress($results['Proformainvoice']['shipaddress_id'], $results['Client']['Address']));
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $dyn_cell,'PROFORMA INVOICE')->mergeCells('A'.$dyn_cell.':J'.$dyn_cell);

			$styleArray = array(
		      'font'  => array(
		        'bold'  => true,
		        'size'  => 12,
		        'name'  => 'Calibri'
		       )
			);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($styleArray);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, 'Office Address');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.++$for_next_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $setting["Setting"]["office_address"])->mergeCells('C'.$dyn_cell.':E'.$for_next_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':B'.$for_next_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':E'.$for_next_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, 'PROFORMA NO.');
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, 'Date')->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, $results['Proformainvoice']['proforma_no']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $this->requestAction('App/date_ymd_to_dmy/'.strtotime($results['Proformainvoice']['proforma_date'])))->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);

			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.++$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);

			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:B5')->applyFromArray($style);			
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, "Shiping Terms");
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $this->requestAction('App/get_delivery_type/'.$results['Proformainvoice']['delivery_type']).' '.$results['Proformainvoice']['tod'])->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$dyn_cell, 'Factory Address');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$new_cell = $dyn_cell + 2;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.++$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':B'.$new_cell)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$dyn_cell, $setting["Setting"]["factory_address"])->mergeCells('C'.$dyn_cell.':E'.$new_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.++$dyn_cell, "Terms Of Payment");
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$dyn_cell, $results['Proformainvoice']['top'])->mergeCells('G'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.++$dyn_cell.':B'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.++$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$dyn_cell.':F'.$dyn_cell)->applyFromArray($border_right);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, "Bill To")->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			//$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, "Ship To")->mergeCells('F'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.++$dyn_cell.':G'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$dyn_cell.':D'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $results['Client']['company_name'])->mergeCells('A'.$dyn_cell.':E'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $results['Proformainvoice']['ship_to'])->mergeCells('F'.$dyn_cell.':J'.$dyn_cell);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$dyn_cell)->applyFromArray($border_left);
			
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell.':J'.$dyn_cell)->applyFromArray($border_top);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$dyn_cell, $cl_address);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$dyn_cell)->getAlignment()->setWrapText(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$dyn_cell, $shp_address);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->applyFromArray($style_vertical);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell)->getAlignment()->setWrapText(true);
			$new_row = $dyn_cell + 2;

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$dyn_cell.':E'.++$new_row);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$dyn_cell.':J'.$new_row);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$dyn_cell.':G'.$new_row)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$new_row.':J'.$new_row)->applyFromArray($border_bottom);

			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.'13', 'Client Po: - '.$results['Order']['clientpo']);
			$headerRow = "Sr"."\n"."No.,Product Name,Hscode,Standard,Grade,Good Description,,QTY,Unit Price"."\n"."(".$sym."),Amount"."\n"."(.".$sym.")";
			//echo $dyn_cell;exit;
			$k = ++$new_row;

			$objPHPExcel->getActiveSheet()->getStyle($k)->applyFromArray($style);
				$objPHPExcel->getActiveSheet()->getStyle($k)->getFont()->setBold(true);

				$maxSize = explode(",", $headerRow);
				for($i=0,$j='A';$i<sizeof($maxSize);$i++,$j++) {

					if($maxSize[$i] == 'Good Description'){
						$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$k.':'.'G'.$k);
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}
					elseif($maxSize[$i] == 'Sr No.'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(8);
					}
					elseif($maxSize[$i] == 'Product Name'){
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(22);
					}else{
						$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(12);
					}
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$k, $maxSize[$i]);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k, $maxSize[$i])->applyFromArray($border_left);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style_vertical_center);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->applyFromArray($style);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle($j.$k)->getAlignment()->setWrapText(true);
				}
			$dataArray = array();
			$idx = '1';
			$index = '1';
			
			foreach($results['Proformaitem'] as $result) {
				$dataArray[$index]['0'] = $idx;
					$dataArray[$index]['1'] = $result["Productcategory"]['productname'];
					$dataArray[$index]['2'] = $result["Productcategory"]["Hscode"]["hscode"];
					$dataArray[$index]['3'] = $result['Standard']['standard_name'];
					$dataArray[$index]['4'] = $result['Grade']['grade_name'];
					$dataArray[$index]['5'] = setGDmmformat($result['Size']['gdmm'],$result['length']);
					$dataArray[$index]['6'] = setGDnbformat($result['Size']['gdnb'],$result['length']);
					$dataArray[$index]['7'] = $result['qty'].' '.$result['Productcategory']['Producttaxonomy']['unit'];
					$dataArray[$index]['8'] = $result['price'];
					$dataArray[$index]['9'] = $result['amount'];
					$idx++;
					$index++;
			}
			$id = ++$new_row;
			$size = count($dataArray);

			foreach ($dataArray as $key => $value) {
				end($value);
				$keys = key($value);
				for($i=0,$j='A';$i<=$keys;$i++,$j++) {
					if(isset($value[$i])) {
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_right);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_top);
						$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_bottom);
						
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, $value[$i]);

						/*Start logic for add decimal position because excel removed automatically if value has empty zero like its show 5.00 as 5*/
						$ex = explode('.', $value[$i]);
						if(isset($ex[1])){
							if(is_numeric($ex[1])){
								$zero = '';
								if(strlen($ex[1]) == 2){
									$zero = '0.00';
								}
								if(strlen($ex[1]) == 3){
									$zero = '0.000';
								}
								$objPHPExcel->getActiveSheet()->getStyle($j.$id)->applyFromArray($border_left)->getNumberFormat()->setFormatCode($zero);	
							}					
						}
						/*End logic of add decimal position with zeros*/	

						/*PHP_Excel default set numberic value to align right, while string value to align left*/
						/*Set quntity align right*/
						if($i == 0){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						}
						if($i == 7){
							$objPHPExcel->getActiveSheet()->getStyle($j.$id)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
						}												
					}
					else {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($j.$id, '');
					}
				}
				$id++;
			}
			$nxt_val = $size + $new_row;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$nxt_val,'Total Amount '.$results['Price']['fullform'].'('.$sym.')');
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_left);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$nxt_val,$results['Proformainvoice']['total']);
			$objPHPExcel->getActiveSheet()->getStyle('J'.$nxt_val)->applyFromArray($border_left);	
			/* Extra Fields */

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Quantity Tolerance')->mergeCells('A'.$nxt_val.':B'.$nxt_val);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Proformainvoice']['quantity'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($hori_right);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Delivery Period ')->mergeCells('A'.$nxt_val.':B'.$nxt_val);

			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Proformainvoice']['delivery_period'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($hori_right);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Dimensions')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Proformainvoice']['dimensions'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Certification ')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Proformainvoice']['certifications'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($hori_right);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Comment  ')->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Proformainvoice']['comment'])->mergeCells('C'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val)->applyFromArray($hori_right);
			//$objPHPExcel->getActiveSheet()->getStyle('F3:F11')->applyFromArray($border_left);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
				//$objPHPExcel->getActiveSheet()->getStyle('A8:I8')->applyFromArray($border_top);

			/* image */

			/*$objDrawing = new PHPExcel_Worksheet_Drawing();
			$path = WWW_ROOT .'uploads/signature/logo1.png';
			//echo $path;exit;0110
			$objDrawing->setPath($path);-*/
			/* Bank Details */
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.++$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()
			    ->getStyle('G'.$nxt_val)
			    ->getNumberFormat()
			    ->setFormatCode(
			        PHPExcel_Style_NumberFormat::FORMAT_TEXT
			    );
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Bank Details');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($style_vertical_center);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);						
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':J'.++$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle($nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Bank Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['branch_name'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':J'.$nxt_val)->applyFromArray($border_top);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'For, SHALCO IND. PVT. LTD.	');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Swift Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['swift_code'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'DIRECTOR');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			/*$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objDrawing->setCoordinates('G'.$nxt_val);*/
			//$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$nxt_val,'DIRECTOR')->mergeCells('G'.$nxt_val.':J'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'AD Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ad_code'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$nxt_val.':B'.$nxt_val);
			//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$nxt_val.':D'.$nxt_val);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Beneficiary’s A/C No');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ac_no'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'Beneficiary’s Name');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results['Bank']['ac_name'])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'IEC Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results["Bank"]["iec_code"])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.++$nxt_val,'IFC Code');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$nxt_val,$results["Bank"]["ifc_code"])->mergeCells('C'.$nxt_val.':F'.$nxt_val);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$nxt_val.':F'.$nxt_val)->applyFromArray($border_bottom);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$nxt_val.':D'.$nxt_val)->applyFromArray($border_left);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$nxt_val.':H'.$nxt_val)->applyFromArray($border_left);
			
			

			$total = $size + 12 + 13;

			$objPHPExcel->getActiveSheet()->getStyle('A7:J'.$nxt_val)->applyFromArray($border_style);
			//$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$nxt_val)->getFont()->setSize(9);
		
			ob_clean();
			$filename = $reportname.".xlsx";
			/*header('Content-Type: application/vnd.ms-excel');*/
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			$objWriter->save('php://output');
			exit();
	}		 		  

}