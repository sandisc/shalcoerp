<?php
/*
 * @Controller name: Permissions Controller
 * @Version: 1.1.0
 * @Author: Agile Infoways
 * @Author URI: http://www.agileinfoways.com
 * @Description: This controller is use to Permissions management.
 */
 
App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class PermissionsController extends AppController{
	var $name = 'Permissions';
	var $helpers  =  array('Html','Form');

	var $components = array('Auth','RequestHandler','Session','Email');

	var $uses = array('User','Userpermission','Grouppermission','Module','Group','Usertype');

	function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('*');
	}

	/*
   	 * Name : userspermissionadd
   	 * Use : add, edit Permission for user.
	 */
	function userspermissionadd( $id = null ){

		$id = base64_decode($id);
		$this->set('pageTitle',' User Permission Add');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'permissions/groupspermission/">Permission For User</a><i class="fa fa-circle"></i></li>');
		$this->layout = "default";
		$this->Session->write('title','Add/Edit Permission For User');
		$this->Userpermission->id = $id;
		$this->old = $this->Userpermission->findById($id,array('recursive'=>0));
		if(!$id){$con = 'Userpermission.user_id IS NULL';}else{$con = 'Userpermission.id = '.$id;}
		
		//$users = $this->User->find('all',array('conditions'=>array('User.usertype_id '!= '1'),'fields'=>array('User.id','User.first_name','User.last_name'),'group'=>'User.id','order'=>'User.first_name ASC'));
		$users = $this->User->find('all', array('conditions' => array('NOT' => array('User.usertype_id' => array(1))),'fields'=>array('User.id','User.first_name','User.last_name'),'group'=>'User.id','order'=>'User.first_name ASC'));
		$permi = array();
		foreach ($users as $user) {			
			$permission = $this->Userpermission->findByUserId($user['User']['id']);
			if(!$permission) {
				array_push($permi,$user);

			}
			if($permission) {
				if($id != '' && $permission['Userpermission']['id'] == $id) {
					array_push($permi,$user);
					//pr($userspermission);exit;
				}
			}
		}

		//exit;
		$this->set('users',$permi);
		$modules = $this->Module->find('all',array('conditions'=>array('Module.status'=>'1'),'order'=>'id asc'));
		// $modules = $this->Module->find('all',array('order'=>'module_name asc,order asc'));
		$this->set('modules',$modules);
		
		if(!empty($this->data)) {
			if(!empty($id)) {
				$this->Userpermission->id = $id;
				$this->set('id',$id);
			}

			if(!empty($this->data['Userpermission']['access_modules'])){
				$this->request->data['Userpermission']['access_modules'] = $this->arraytostring($this->data['Userpermission']['access_modules']);	
			}
			else{
				$this->request->data['Userpermission']['access_modules'] = '';
			}

			$this->Userpermission->set($this->request->data);
			if($this->Userpermission->validates($this->request->data)){
				if($this->Userpermission->save($this->request->data)){
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = "Permission has been updated"; 
						$result = array_diff_assoc($this->old['Userpermission'],$this->request->data['Userpermission']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'User permission','Edit',$result);/*Add Data in Log*/
					} 
					else { 
						$msg = "Permission has been added";
						$Activities->addlog($this->loggedin_user_info['id'],'User permission','Add',$this->request->data['Userpermission']);/*Add Data in Log*/
					}

					$employeeName = $this->getEmployeename($this->data['Userpermission']['user_id']);
					$name = $employeeName['User']['first_name']." ".$employeeName['User']['last_name'];

					/* Add log */
					$loginUser = $this->Auth->User();
					$moduleName = 'permissions/groupspermissionadd';

					$userdata = array(
						'loingName' => $loginUser['first_name']." ".$loginUser['last_name'],
						"empName" => $name
						);
					//$activity = $this->replaceTemplate($MSG,$userdata);
					//$this->addlog($this->loggedin_user_info['id'],$moduleName,$activity);
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> '.$msg.'</div>'));
					$this->redirect('/permissions/groupspermission');
				}
			}
			else {
				$errors = $this->Userpermission->validationErrors;
				foreach($errors as $key => $value) {
					if($key == 'access_modules'){
					$val = '<div class="error-message">'.$value['0'].'</div>';
					$this->set('ErrorMsg',$val);}
				}
			}
		}

		if(!empty($id)) {
			/*$this->Userpermission->id = $id;*/
			$this->data = $this->Userpermission->findById($id);
			$this->set('id',$id);
		}
	}

	/*
   	 * Name : userspermissionview
   	 * Use : view Permission for user.
	 */
	function userspermissionview( $id = null ){
		$this->set('pageTitle',' User Permission View');
		$id = base64_decode($id);
		$this->layout = "default";
		$this->Session->write('title','View Permission For User');
	
		$users = $this->User->find('all',array('conditions'=>array('usertype_id!=1 AND usertype_id!=2')));
		$this->set('users',$users);

		$modules = $this->Module->find('all',array('conditions'=>array('Module.status'=>'1'),'order'=>'module_name asc,order asc'));
		$this->set('modules',$modules);

		$this->Userpermission->id = $id;
		$this->data = $this->Userpermission->read();

		$empname = $this->User->findById($this->data['Userpermission']['user_id']);		
		$this->set('empname',$empname['User']['first_name'].' '.$empname['User']['last_name']);
		$this->set('id',$id);
	}

	/*
   	 * Name: userspermission
   	 * Use: Listing Permission for user.
	 */
	function userspermission(){
		$this->Session->write('title','Permission For User');
		$permissions = $this->Userpermission->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'prefix' => 'hr',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'User.id = Userpermission.user_id',
                    )
                ),
            ),          
            'fields' => array('Userpermission.*,User.id,User.first_name,User.last_name,User.email,User.email'),
            'order' => 'Userpermission.created desc'
        ));		
		$this->set('permissions',$permissions);
	}
	
	/*
   	 * Name: groupspermissionadd
   	 * Use: admin can add, edit Permission for group.
	 */
	function groupspermissionadd( $id = null ){
		$this->set('pageTitle',' Usertype Permission Add');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'permissions/groupspermission/">Permission For User</a><i class="fa fa-circle"></i></li>');
		$id = base64_decode($id);
		$this->layout = "default";
		$this->Session->write('title','Add/Edit Permission For Usertype');
	    $this->Grouppermission->id = $id;		
		$this->old = $this->Grouppermission->findById($id,array('recursive'=>0));
		$grouprows = $this->Usertype->find('all',array('conditions' => array('NOT' => array('Usertype.id' => array(1))),'fields'=>array('Usertype.usertype_name','Usertype.id')));
		$groups = array();
		/*Fetch only those group which has no permission set till now*/
		//pr($grouprows);
		foreach ($grouprows as $groupdata) {			
			$gpermission = $this->Grouppermission->findByUsertypeId($groupdata['Usertype']['id']);
			if(!$gpermission) {
				array_push($groups,$groupdata);
			}
			if($gpermission) {
				if($id != '' && $gpermission['Grouppermission']['id'] == $id) {
					array_push($groups,$groupdata);
				}
			}
		}
		
		$this->set('groups',$groups);

		$modules = $this->Module->find('all',array('conditions'=>array('Module.status'=>'1'),'order'=>'id asc'));
		/*$modules = $this->Module->find('all',array('order'=>'module_name asc,order asc'));*/
		$this->set('modules',$modules);
		
		if(!empty($this->data)){	
			if(!empty($id)) {
				$this->Grouppermission->id = $id;
         		$this->set('id',$id);				
			}									
			
			if(!empty($this->data['Grouppermission']['access_modules'])){
				$this->request->data['Grouppermission']['access_modules'] = $this->arraytostring($this->data['Grouppermission']['access_modules']);	
			}
			else{
				$this->request->data['Grouppermission']['access_modules'] = '';
			}

			$this->Grouppermission->set($this->request->data);	
			if($this->Grouppermission->validates($this->request->data)) {
				if($this->Grouppermission->save($this->request->data)){
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = "Permission has been updated"; /*$MSG = getlogmessage('updategroupspermission');*/
						$result = array_diff_assoc($this->old['Grouppermission'],$this->request->data['Grouppermission']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Group permission','Edit',$result);/*Add Data in Log*/						
					} 
					else { 
						$msg = "Permission has been added";  /*$MSG = getlogmessage('addgroupspermission');*/
						$Activities->addlog($this->loggedin_user_info['id'],'Group permission','Add',$this->request->data['Grouppermission']);/*Add Data in Log*/
					}

					$groupName = $this->getRoleName($this->data['Grouppermission']['usertype_id']);

					/* Add log */
					$loginUser = $this->Auth->User();
					$moduleName = 'permissions/groupspermissionadd';

					$userdata = array(
						'loingName' => $loginUser['first_name']." ".$loginUser['last_name'],
						"group" => $groupName
						);
				/*	$activity = $this->replaceTemplate($MSG,$userdata);
					$this->addlog($this->loggedin_user_info['id'],$moduleName,$activity);
				*/
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> '.$msg.'</div>'));
					$this->redirect('/permissions/groupspermission');
				}				
			}
			else {
				$errors = $this->Grouppermission->validationErrors;
				foreach($errors as $key => $value) {
					if($key == 'access_modules'){
					$val = '<div class="error-message">'.$value['0'].'</div>';
					$this->set('ErrorMsg',$val);}
				}
				}
		}

		if(!empty($id)) {
			/*$this->Grouppermission->id = $id;*/
			$this->data = $this->Grouppermission->findById($id);
			$this->set('id',$id);
		}
	}

	/*
   	 * Name: groupspermission
   	 * Use: Listing Permission for group.
	 */
	function groupspermission(){
		$this->Session->write('title','Permission For User');
		$this->set('pageTitle','Permission For User');
		$permissions = $this->Grouppermission->find('all', array(
            'joins' => array(
                array(
                    'table' => 'usertypes',
                   // 'prefix' => 'sr',
                    'alias' => 'Usertype',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Usertype.id = Grouppermission.usertype_id',
                    )
                ),
            ),
            'fields' => array('Grouppermission.*,Usertype.*'),
            'order' => 'Grouppermission.created ASC'
        ));
		$this->set('permissions',$permissions);
		$userPermissions = $this->Userpermission->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'prefix' => 'sr',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'User.id = Userpermission.user_id',
                    )
                ),
            ),
            'fields' => array('Userpermission.*,User.id,User.first_name,User.last_name,User.email,User.email,User.usertype_id'),
            'order' => 'Userpermission.created desc'
        ));
		$this->set('userPermissions',$userPermissions);
		//pr($userPermissions);
	}

	/*
   	 * Name: groupspermissionview
   	 * Use: view Permission for group.
	 */
	function groupspermissionview( $id = null ) {
		$this->set('pageTitle','Permission For Role');
		$id = base64_decode($id);
		$this->layout = "default";
		$this->Session->write('title','View Permission Of Group');

		$modules = $this->Module->find('all',array('conditions'=>array('Module.status'=>'1'),'order'=>'module_name asc,order asc'));
		$this->set('modules',$modules);

		$this->Grouppermission->id = $id;
		$this->data = $this->Grouppermission->read();

		$groupname = $this->Usertype->findById($this->data['Grouppermission']['usertype_id']);
		//pr($groupname);exit;
		$this->set('groupname',$groupname['Usertype']['usertype_name']);
		$this->set('id',$id);
	}
}