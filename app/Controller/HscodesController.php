<?php
/*
 * @Controller name: HS Code Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Hscodes management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class HscodesController extends AppController {
	var $name = 'Hscodes';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','HS Code Management');

	}
	/*
   	 * Name: index
   	 * Use: HScode Listing.
	 */
	function index() {
        $this->set('pageTitle',' HS Code List');
        $this->set('ajaxaction','ajaxlisting');
	}
	function ajaxlisting(){
    	$column = array('id','hscode','hscode_alias','rate','Hscode.modified','first_name');
    	$order = array('Hscode.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Hscode->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$Hscodes = $this->Hscode->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('code',$Hscodes);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: admin can add,update hscode.
	 */
	function add($id = null) {
		$id = base64_decode($id);		
        $this->Hscode->id = $id;		
		$this->old = $this->Hscode->findById($id,array('recursive'=>0));

		if(!empty($id)) {
			$this->set('pageTitle','Edit HS Code');
			$this->request->data = $this->Hscode->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add HS Code');
		}
		$this->set('id',$id);
		$Hscode = $this->Hscode->find('list', array(
	        'fields' => array('Hscode.id', 'Hscode.hscode'),
	        'recursive' => 0
	    ));
		$this->set('Hscode',$Hscode);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'Hscodes/">Hscode</a><i class="fa fa-circle"></i></li>');
	}

	/*Form add/edit submit using ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Hscode->set($this->request->data);
			$response=array();
			/* set reference location if add Hscode form open in modal dialog means popup */
			if(isset($this->request->data['Hscode']['targetid'])){
				$response['targetid'] = $this->request->data['Hscode']['targetid'];
				unset($this->request->data['Hscode']['targetid']);	
			}

			if($this->Hscode->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Hscode']['modifiedby'] = $logged_user;			
				$id = '';
				if(isset($this->request->data['Hscode']['id'])){
					$id = $this->request->data['Hscode']['id'];
					$this->old = $this->Hscode->findById($id,array('recursive'=>0));
				}				
				if(empty($id)) {
					$this->request->data['Hscode']['createdby'] = $logged_user;
				}
				if($this->Hscode->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = 'HS Code has been Updated successfully';	
						$result = array_diff_assoc($this->old['Hscode'],$this->request->data['Hscode']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Hscode','Edit',$result);/*Add Data in Log*/											
					}else { 
					   $Activities->addlog($this->loggedin_user_info['id'],'Hscode','Add',$this->request->data['Hscode']);/*Add Data in Log*/
						$msg = 'HS Code has been Added successfully';
						$response['idvalue'] = $this->Hscode->getLastInsertID();
						$response['text'] = $this->request->data['Hscode']['hscode'];
					}
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						//$this->redirect('/hscodes/index');
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'hscode';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);
				}
			}
			else{
                $Hscode = $this->Hscode->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Hscode could not be saved. Please, try again.';
                $response['data'] = compact('Hscode');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/		
	}	
	
	/**
   	 * Name: delete
   	 * Use: delete hscode
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Hscode->id = $id;
		$data = $this->Hscode->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Hscode','Delete',$data['Hscode']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Hscode->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> HS Code has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}
}