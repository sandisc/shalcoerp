<?php
App::uses('AppController', 'Controller');
/**
 * Name : ActivitiesController
 * Use : For Activity Log
 */
class ActivitiesController extends AppController {
  var $name = 'Activities';
  var $helpers  =  array('Html','Form');
  var $components = array('Auth','RequestHandler','Session','Email');

  function beforeFilter(){
    parent::beforeFilter();
    $this->set('mainTitle','Activity Management');
  }

/**
  * Name : index
  * Use : Listing Details
  */
  public function index(){
    $this->set('pageTitle','Activity List');
    $this->set('ajaxaction','ajaxlisting');
  }

  function ajaxlisting(){
    $column = array('id','module','activity','url','Activity.modified','User.first_name');
    $order = array('Activity.modified' => 'desc');  

    $res = $this->datatable_append($order,$column);    
    $count = $this->Activity->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));    
    $activities = $this->Activity->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
    $this->set('activity',$activities);
    $this->set('count',$count);
  } 

/**
 * Name : view
 * Use : View Activity
 * @param int id
 * @return void
 */
  public function view($id){ 
    $id = base64_decode($id);
    if(empty($id)) {
      $this->redirect(array('controller' => 'activities', 'action' => 'index'));
    }
    $this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'activities/">Activity</a><i class="fa fa-circle"></i></li>');
    $this->set('pageTitle','Activity Details');
    $que = $this->Activity->findById($id);
    $this->set('res',$que); 
    if(!$que) {$this->redirect('index');}
  }

  public function addlog($userid,$modulename,$activity,$details){          
    $data['user_id'] = $userid;
    $data['module'] = $modulename;
    $data['activity'] = $activity;
    $data['url'] = $_SERVER['REQUEST_URI'];    
    $data['details'] = serialize($details);
    $this->Activity->create();
    $this->Activity->save($data);      
  }

  public function delete($id=''){
    $id = base64_decode($id);
    $data = $this->Activity->findById($id);
    $this->Activity->delete($id);
    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Activity has been Deleted successfully</div>'));
    $this->redirect('index');
  }
}