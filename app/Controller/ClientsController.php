<?php
/*
 * @Controller name: Client Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to client management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class ClientsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login','logout');
		$this->set('mainTitle','Client Management');
	}
	function index() {
        $this->set('pageTitle',' Client List');
        $this->set('ajaxaction','ajaxlisting');
	}
	function ajaxlisting(){
    	$column = array('Client.id','company_name','Client.email','contact_num1','mob','contact_person','clienttypeid','Client.modified','User.first_name','Client.status');
    	$order = array('Client.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);

		$count = $this->Client->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$clients = $this->Client->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('client',$clients);
		$this->set('count',$count);
		
	}

	/*
   	 * Name: View
   	 * Use: CLient can View all Details.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'clients/">Client</a><i class="fa fa-circle"></i></li>');
		$clients = $this->Client->find('first', array(
   			'conditions' => array('Client.id' => $id ),
		));
		$this->set('data',$clients);
		$this->set('pageTitle',' View Client');
	}

	/*
   	 * Name: add
   	 * Use: admin can add,update client.
	 */
	function add($id = null) {
		$this->loadmodel('Address');
	    $id = base64_decode($id);
	    $this->Client->id = $id;		
		$this->old = $this->Client->findById($id,array('recursive'=>0));

		
		if(!empty($id)) {
		$this->set('pageTitle','Edit Client');
		$this->request->data = $this->Client->findById($id);
		$address = $this->Address->find('all',array('conditions'=>array('Address.clientid' => $id)));
		$this->set('address',$address);
		
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Client');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'clients/">Client</a><i class="fa fa-circle"></i></li>');
		$this->loadmodel('User');
		$this->loadmodel('Industry');
		$this->loadmodel('Vendor');
		$this->loadmodel('Productcategory');
		$this->set('coordinator',$this->User->find('list',array('fields' => array('id','first_name'),'conditions'=>array('usertype_id'=>1))));				
		$industry = $this->Industry->find('list',array('fields' => array('id','industryname')));				
		$vendors = $this->Vendor->find('list',array('fields' => array('id','vendorname')));				
		$category = $this->Productcategory->find('list',array('id','name'));
		$this->set('category',$category);
		$this->set('industry',$industry);
		$this->set('vendors',$vendors);


	}

	/*Form submit using ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
	    /* Start : Add/Edit Submit Form Event*/
	    $this->loadmodel('Address');
	    if(!empty($this->request->data)) {
			$this->Client->set($this->request->data);
			$response=array();
			/* set reference location if add Client form open in modal dialog means popup */
			if(isset($this->request->data['Client']['targetid'])){
				$response['targetid'] = $this->request->data['Client']['targetid'];
				unset($this->request->data['Client']['targetid']);	
			}

			if($this->Client->validates()) {
				$logged_user = $this->Auth->user('id');				
				$this->request->data['Client']['modifiedby'] = $logged_user;
				$id = '';
				if(isset($this->request->data['Client']['id'])){
					$id = $this->request->data['Client']['id'];
					$this->old = $this->Client->findById($id,array('recursive'=>0));
				}				
				if(empty($id)) {
					$this->request->data['Client']['createdby'] = $logged_user;
				}
				/*$this->request->data['Client']['address1'] = $this->request->data['Client']['streetaddress1'].' <br/>'.$this->request->data['Client']['city1'].' '.$this->request->data['Client']['state1'].' <br/>'.$this->request->data['Client']['country1'];
				$this->request->data['Client']['address2'] = $this->request->data['Client']['streetaddress2'].' <br/>'.$this->request->data['Client']['city2'].' '.$this->request->data['Client']['state2'].' <br/>'.$this->request->data['Client']['country2'];*/
				if($this->request->data['Client']['clienttypeid'] == 1){
						$this->request->data['Client']['vendorid'] = NULL;
				}
				if($this->request->data['Client']['clienttypeid'] == 2){
						$this->request->data['Client']['industryid'] = NULL;
				}
				if(isset($this->request->data['Client']['industryid'])){
					$this->request->data['Client']['industryid'] = implode(",",$this->request->data['Client']['industryid']);
				}
				if(isset($this->request->data['Client']['vendorid'])){
					$this->request->data['Client']['vendorid'] = $this->request->data['Client']['vendorid'];
				}

				if(isset($this->request->data['Client']['procatid'])){
					$this->request->data['Client']['procatid'] = implode(',',$this->request->data['Client']['procatid']);
				}	
				//pr($this->request->data);exit;
				if($this->Client->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$del_ids = $this->request->data['address_removal_id'];
						if(!empty($del_ids)){
            				$del=array($del_ids);
	         				$condition = array('Address.id IN ('.$del_ids.')');
	        				$this->Address->deleteAll($condition,false);
	        			}
							$address = $this->request->data['Client']['address'];
	           				foreach($address as $keyitem => $items){
		           				if(!empty($items['id'])){
		           					$this->Address->id = $items['id'];	
			           				$address[$keyitem]['clientid'] = $items['clientid'];
				           			$this->Address->save($address[$keyitem]);
				           		}else{
				           			$address[$keyitem]['clientid'] = $id;
	        	   					$this->Address->saveall($address[$keyitem]);
			    	       		}
		        			}
							
					/*$this->request->data['Client']['address'] = $address;/*updated value so we pass it into activity log*/
	           		/*$this->request->data['Client']['address'] = $address;
	           		*/
						$result = array_diff_assoc($this->old['Client'],$this->request->data['Client']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Client','Edit',$result);/*Add Data in Log*/						
						$msg = 'Client has been Updated successfully';						
					}else { 
						$lastid = $this->Client->getLastInsertID(); 
	            		$address = $this->request->data['Client']['address'];
	           			foreach($address as $keyitem => $items){	
	           				$address[$keyitem]['clientid'] = $lastid;
	           				$this->Address->saveall($address[$keyitem]);
	           			}
						$msg = 'Client has been Added successfully';
						$Activities->addlog($this->loggedin_user_info['id'],'Client','Add',$this->request->data['Client']);/*Add Data in Log*/
						$response['idvalue'] = $this->Client->getLastInsertID();
						$response['text'] = $this->request->data['Client']['company_name'];
					}	
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						//$this->redirect('/clients/index');
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}

					$response['topic'] = 'client';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);
				}
			}
			else{

                $Client = $this->Client->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Client could not be saved. Please, try again.';
                $response['data'] = compact('Client');
                echo json_encode($response);				
			}
		}
		/* End : Add/Edit Submit Form Event*/		
	}
	/**
   	 * Name: delete
   	 * Use: delete client
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->loadmodel('Address');
		$this->Client->id = $id;
		$data = $this->Client->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Client','Delete',$data['Client']);/*Add Data in Log*/
    	}
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Client->delete($id)) {
			$condition = array('Address.clientid' => $id);
	       	$this->Address->deleteAll($condition,false);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Client has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}
   
    /**
   	 * Name: getID
   	 * Use: get id of data, it will be use for import order
     * @param string $id name of record
     * @return int id of record
     */

	function getID($value) {
		echo "f";
		$data = $this->Client->find('first', array(
   			'conditions' => array('company_name' => $value ),
   			'fields' => array('id'), 
    		'recursive' => -1
		));
		pr($data);exit;
	}	
	/**
	 * Name: getSupply
   	 * Use: get Industry data from Indutry master
     * @param string $id name of record
     * @return int id of record*/
	function getSupply($id){
		$this->loadmodel('Industry');
		$industry = $this->Industry->find('all',array('fields'=>array('id','industryname')));
		echo '<div class="col-md-2"><label for="control-label col-md-3" style="font-weight:none;">Industry *</label></div>';
		echo '<div class="col-md-10 hide_row multicheck">';
		echo '<div class="multiselect"><input name="data[Client][industryid]" value="" id="RoleModuleId" type="hidden">';
		$i = 1;
		foreach( $industry as $indus){
							echo '<div class="checkbox"><input name="data[Client][industryid][]" value="'.$indus['Industry']['id'].'" id="proModuleId'.$i.'" type="checkbox"><label for="proModuleId'.$i.'">'.$indus['Industry']['industryname'].'</label></div>';
							
		}
		echo '</div></div>';
		exit;
	}
	/**
	 * Name: getVendor
   	 * Use: get Vendor data from Vendor master
     * @param string $id name of record
     * @return int id of record*/
	function getVendor($id){
		$this->loadmodel('Vendor');
		$vendors = $this->Vendor->find('all',array('fields'=>array('id','vendorname')));
		echo '<label class="control-label col-md-3">Vendor<span class="required" aria-required="true"> * </span></label>';
		echo '<div class="col-md-9"><div class="input select"><select name="data[Client][vendorid]" required="required" class="form-control" id="ClientClientvendor">';
		foreach( $vendors as $vendor){
                 echo '<option value="'.$vendor['Vendor']['id'].'">'.$vendor['Vendor']['vendorname'].'</option>';
                   
		}
		echo '</select><span class="help-block"><br></span></div></div>';
		exit;
	}
}