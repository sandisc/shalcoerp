<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
/**
 * Name : SettingController
 * Use : For edit Setting
 */
class SettingsController extends AppController {

  public $uses = array('Setting');
  public $components = array('Auth','Session'); 

  /**
   * Name : index
   * Use : Edit Setting
   * @param int id
   * @return void
   */
  public function index($id="")
  { 
     $id='1';
     /*
      * For Edit Setting
      */
         $this->set('pageTitle','Edit Setting');
         $this->set('mainTitle','Setting Managment');
         $this->Setting->id = $id;
         $this->set('id',$id);
         $this->old = $this->Setting->read();
         $this->set('old',$this->old);
      //     pr($this->old);exit;
          if(!empty($this->data)){
            $this->Setting->set($this->data);
            if($this->Setting->validates($this->data))
            { 
            //  pr($this->request->data);exit;            
              $this->request->data['Setting']['modified'] = date('Y-m-d H:i:s');
              $this->request->data['Setting']['modifiedby'] = $this->Auth->user('id');
              if(!empty($this->request->data['Setting']['signature'])){
                  move_uploaded_file($this->request->data['Setting']['signature']['tmp_name'],'uploads/signature/'.$this->request->data['Setting']['signature']['name']);
                  $this->request->data['Setting']['signature'] = $this->request->data['Setting']['signature']['name'];
              }else{
                  $this->request->data['Setting']['signature'] = $this->old['Setting']['signature'];
              }

              $this->Setting->save($this->request->data);
              $Activities = new ActivitiesController;
              $result = array_diff_assoc($this->old['Setting'],$this->request->data['Setting']);/*Get only changed fields*/
              $Activities->addlog($this->loggedin_user_info['id'],'Setting','Edit',$result);/*Add Data in Log*/
              $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> The Setting has been Updated</div>'));
                $this->redirect('index');
              //}
            }
          }
          else
          { 
              $this->data = $this->Setting->read();
          }
     }
  
}  