<?php
/*
 * @Controller name: Standard Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Standard management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class StandardsController extends AppController {
	var $name = 'Standards';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Standard Management');
	}
	/**
   	 * Name: index
   	 * Use: Standard view.
	*/
	function index() {
        $this->set('pageTitle','Standard List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/**
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','standard_name','procatid','description','Standard.modified','first_name');
    	$order = array('Standard.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Standard->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		//$this->Standard->bindModel(array('hasOne' => array('Productcategory'=>array('conditions'=>array('FIND_IN_SET (Productcategory.id,Standard.procatid)'),'foreignKey'=>false,GROUP_CONCAT('Productcategory.name as ids')))));
		$standards = $this->Standard->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		//pr($standards); exit;
		$this->set('standard',$standards);
		$this->set('count',$count);
	}
	/**
   	 * Name: add
   	 * Use: user can add,update Standard.
	 */
	function add($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Productcategory');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'standards/">Standard</a><i class="fa fa-circle"></i></li>');
        $this->Standard->id = $id;		
		$this->old = $this->Standard->findById($id,array('recursive'=>0));		
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Standard->set($this->request->data);
			
			if(!empty($this->request->data['Standard']['procatid'])){
				$this->request->data['Standard']['procatid'] = implode(",",$this->request->data['Standard']['procatid']);	
			}			
			
			if($this->Standard->validates($this->data)) {
				$logged_user = $this->Auth->user('id');				
				$this->request->data['Standard']['modifiedby'] = $logged_user;			
			
				if(!empty($id)) {
					$this->request->data['Standard']['id'] = $id;					
				}else{
					$this->request->data['Standard']['createdby'] = $logged_user;
				}
				if($this->Standard->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Standard'],$this->request->data['Standard']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Standard','Edit',$result);/*Add Data in Log*/						
						$msg = 'Standard has been Updated successfully';						
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Standard','Add',$this->request->data['Standard']);/*Add Data in Log*/
						$msg = 'Standard has been Added successfully';
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					$this->redirect('/standards/');
				}
			}
			else{
				$errors = $this->Standard->validationErrors;
			}
		}
		/* End : Add/Edit Submit Form Event*/
		
		if(!empty($id)){
			$this->set('pageTitle','Edit Standard');
			$this->request->data = $this->Standard->findById($id);
			$selected = $this->Standard->find('all',array('fields' => array('Standard.procatid'),'conditions' => array('Standard.id' => $id)));
			$this->set('selected',$selected);
			if(empty($this->request->data)){ /*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Standard');
		}
		$this->set('id',$id);
		$category = $this->Productcategory->find('list',array('id','name'));
		$this->set('category',$category);
	}

	/**
   	 * Name: delete
   	 * Use: delete standard
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Standard->id = $id;
		$data = $this->Standard->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Standard','Delete',$data['Standard']);/*Add Data in Log*/
    	}
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Standard->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Standard has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}

	/*
	* Name: getProcat
	* Use: get product category name
	* 
	*
	*/
	function getProcat($procatid = null){
		$this->loadmodel('Productcategory');
		$test = $this->Productcategory->query('Select name From sr_productcategories WHERE id IN('.$procatid.')');
		foreach($test as $t => $k){
		$imp[] = $k['sr_productcategories']['name'];
	
		}
		$procatname = implode(',',$imp);
		return $procatname;
	}
}