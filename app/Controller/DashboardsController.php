<?php
App::uses('AppController', 'Controller');

/**
 * Name : DashboardsController
 * Use : For display dashboard
 */
class DashboardsController extends AppController {

  public $uses = array('User','Client','Proformainvoice','Order','Invoice','Chalan','Certificate','Dummycertificate');
  public $layout = 'admin';
  var $components = array('Auth','Session');
  

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('login');
    $this->set('mainTitle','Dashboard');
    
  }

  public function index($fromdate='',$todate='') { 
  	$this->layout = 'default'; 
    $this->set('pageTitle','Dashboard');
  }

/**
  * Name : generalstatistics
  * Use :  fetch general statistics details of invoice,chalan, order etc.
  * @param date $fromdate starting date
  * @param date $todate ending date
  * @return all general statistics of all main module like invoice,chalan, order etc.
  */ 
  public function generalstatistics($fromdate='',$todate='') { 

    $this->autoRender = false;
    $date = date("Y-m-d");

    if(!isset($_POST['fromdate'])){
      $fromdate = $date.' 00:00:00';
    }else{
      $fromdate = $_POST['fromdate'].' 00:00:00';
    }

    if(!isset($_POST['todate'])){
      $todate = $date.' 23:59:59';
    }else{
      $todate = $_POST['todate'].' 23:59:59'; 
    }   

    $client_count = $this->Client->find('count', array(
                      'conditions' => array('Client.created >=' => $fromdate, 'Client.created <=' => $todate),'recursive'=>-1
                    ));
    $proforma_count = $this->Proformainvoice->find('count', array(
                      'conditions' => array('Proformainvoice.created >=' => $fromdate, 'Proformainvoice.created <=' => $todate),'recursive'=>-1
                    ));
    $order_count = $this->Order->find('count', array(
                      'conditions' => array('Order.created >=' => $fromdate, 'Order.created <=' => $todate),'recursive'=>-1
                    ));    
    $chalan_count = $this->Chalan->find('count', array(
                      'conditions' => array('Chalan.created >=' => $fromdate, 'Chalan.created <=' => $todate),'recursive'=>-1
                    ));    
    $invoice_count = $this->Invoice->find('count', array(
                      'conditions' => array('Invoice.created >=' => $fromdate, 'Invoice.created <=' => $todate),'recursive'=>-1
                    ));
    $certi_count = $this->Certificate->find('count', array(
                      'conditions' => array('Certificate.created >=' => $fromdate, 'Certificate.created <=' => $todate),'recursive'=>-1
                    ));
    $dummycerti_count = $this->Dummycertificate->find('count', array(
                      'conditions' => array('Dummycertificate.created >=' => $fromdate, 'Dummycertificate.created <=' => $todate),'recursive'=>-1
                    ));   
                          
    return json_encode(array('client_count'=>$client_count,
                       'proforma_count'=>$proforma_count,
                       'order_count'=>$order_count,
                       'chalan_count'=>$chalan_count,
                       'invoice_count'=>$invoice_count,
                       'certi_count'=>$certi_count,
                       'dummycerti_count'=>$dummycerti_count));
  }
  
/**
  * Name : salesstatistics
  * Use :  fetch sales statistics details of invoice,chalan
  * @param date $fromdate starting date
  * @param date $todate ending date
  * @return all sales statistics of all main module like invoice,chalan
  */ 
  public function salesstatistics($fromdate='',$todate='') { 

    $this->autoRender = false;
    $date = date("Y-m-d");

    if(!isset($_POST['fromdate'])){
      $fromdate = $date.' 00:00:00';
    }else{
      $fromdate = $_POST['fromdate'].' 00:00:00';
    }

    if(!isset($_POST['todate'])){
      $todate = $date.' 23:59:59';
    }else{
      $todate = $_POST['todate'].' 23:59:59'; 
    }
     $chalan_count = $this->Chalan->find('first', array(
                      'conditions' => array('Chalan.created >=' => $fromdate, 'Chalan.created <=' => $todate),
                      'fields' => array('sum(Chalan.totalqty) AS Total_qty','sum(Chalan.totalamount) as Total_amount'),
                      'recursive'=>-1
                    ));
     $invoice_count = $this->Invoice->find('first', array(
                      'conditions' => array('Invoice.created >=' => $fromdate, 'Invoice.created <=' => $todate),
                      'fields' => array('sum(Invoice.totalqty) AS Total_qty','sum(Invoice.totalnetweight) as Total_amount'),
                      'recursive'=>-1
                    ));
      return json_encode(array('total_chalan_qty'=>$chalan_count[0]['Total_qty'],
                       'total_chalan_amount'=>$chalan_count[0]['Total_amount'],
                       'total_invoice_qty'=>$invoice_count[0]['Total_qty'],
                       'total_invoice_amount'=>$invoice_count[0]['Total_amount']
                      ));
  }

/**
  * Name : usersalesstatistics
  * Use :  fetch admin user sales statistics details of invoice,chalan
  * @param date $fromdate starting date
  * @param date $todate ending date
  * @return all admin user sales statistics of all main module like invoice,chalan
  */ 
  public function usersalesstatistics($fromdate='',$todate='') { 

    $this->autoRender = false;
    $date = date("Y-m-d");

    if(!isset($_POST['fromdate'])){
      $fromdate = $date.' 00:00:00';
    }else{
      $fromdate = $_POST['fromdate'].' 00:00:00';
    }

    if(!isset($_POST['todate'])){
      $todate = $date.' 23:59:59';
    }else{
      $todate = $_POST['todate'].' 23:59:59'; 
    }
    $chalan_data = $this->Chalan->query('select first_name as Name,SUM(sc1.totalqty) as Total_qty,SUM(sc1.totalamount) as Total_amount from sr_users as su
                inner join sr_usertypes as sut on sut.id = su.usertype_id
                inner join sr_clients as sc on sc.coordinator = su.id
                inner join sr_orders as so on so.bill_to = sc.id
                inner join sr_chalans as sc1 on sc1.orderid = so.id
                where su.usertype_id = 1 
                and so.local =1
                and sc1.created >= "'.$fromdate.'" and sc1.created <= "'.$todate.'"
                GROUP by su.id');
    $invoice_data = $this->Invoice->query('select su.first_name as Name,SUM(si.totalnetweight) as Total_amount_international,SUM(si.totalqty) as Total_Qty_international from sr_users as su inner join sr_usertypes as sut on sut.id = su.usertype_id inner join sr_clients as sc on sc.coordinator = su.id inner join sr_orders as so on so.bill_to = sc.id inner join sr_invoices as si on si.orderid = so.id where su.usertype_id = 1 and so.local = 0 and si.created >= "'.$fromdate.'" and si.created <= "'.$todate.'" GROUP by su.id');

      $ch = array();
      $i = 0;
      foreach ($chalan_data as $key => $value) {
        $ch[$i]['Name'] = $value['su']['Name'];
        $ch[$i]['Total_qty'] = $value[0]['Total_qty'];
        $ch[$i]['Total_amount'] = $value[0]['Total_amount'];
        $ch[$i]['Total_qty_inter'] = 0;
        $ch[$i]['Total_amount_inter'] = 0;
        $i++;
        # code...
      }
      $j = 0;
      $inv = array();
      foreach($invoice_data as $key => $value) {
        $inv[$j]['Name'] = $value['su']['Name'];
        $inv[$j]['Total_qty'] = 0;
        $inv[$j]['Total_amount'] = 0;
        $inv[$j]['Total_qty_inter'] = $value[0]['Total_Qty_international'];
        $inv[$j]['Total_amount_inter'] = $value[0]['Total_amount_international'];
        $j++;
        # code...
      }
      return json_encode((array_merge($ch,$inv)));
  }

/**
  * Name : sizesalesstatistics
  * Use :  fetch admin size sales statistics details of invoice,chalan
  * @param date $fromdate starting date
  * @param date $todate ending date
  * @return all admin size sales statistics of all main module like invoice,chalan
  */ 
  public function sizesalesstatistics($fromdate='',$todate='') { 

    $this->autoRender = false;
    $date = date("Y-m-d");

    if(!isset($_POST['fromdate'])){
      $fromdate = $date.' 00:00:00';
    }else{
      $fromdate = $_POST['fromdate'].' 00:00:00';
    }

    if(!isset($_POST['todate'])){
      $todate = $date.' 23:59:59';
    }else{
      $todate = $_POST['todate'].' 23:59:59'; 
    }

    /* Do business logic here*/
    $chalan_data=$this->Chalan->query('select sz.gdmm,sum(sc1.qty_mtr) as sum_qty_mtr,sum(sc1.qty_mt) as sum_qty_mt FROM sr_chalans as sc 
                          inner join sr_chalanitems as sc1 on sc1.chalanid = sc.id
                          inner join sr_orders as so on so.id = sc.orderid
                          inner join sr_orderitems as so1 on so1.orderid = so.id
                          inner join sr_sizes as sz on sz.id = so1.size_id
                          where sc.created >= "'.$fromdate.'" and sc.created <= "'.$todate.'" and so.local = 1 GROUP by sz.id');
    $invoice_data=$this->Invoice->query('select sz.gdmm,sum(si1.qty_mtr) as sum_qty_mtr,sum(si1.qty_mt) as sum_qty_mt FROM sr_invoices as si 
                          inner join sr_invoiceitems as si1 on si1.invoiceid = si.id
                          inner join sr_orders as so on so.id = si.orderid
                          inner join sr_orderitems as so1 on so1.orderid = so.id
                          inner join sr_sizes as sz on sz.id = so1.size_id
                          where si.created >= "'.$fromdate.'" and si.created <= "'.$todate.'" and so.local = 0 GROUP by sz.id');
    $ch = array();
    $i = 0;
    foreach ($chalan_data as $key => $value) {
      $ch[$i]['gdmm'] = $value['sz']['gdmm'];
      $ch[$i]['qty_mtr'] = $value[0]['sum_qty_mtr'];
      $ch[$i]['qty_mt'] = $value[0]['sum_qty_mt'];

      $i++;
      # code...
    }
    $j = 0;
    $inv = array();
    foreach($invoice_data as $key => $value) {
      $inv[$j]['gdmm'] = $value['sz']['gdmm'];
      $inv[$j]['qty_mtr_inter'] = $value[0]['sum_qty_mtr'];
      $inv[$j]['qty_mt_inter'] = $value[0]['sum_qty_mt'];
      $j++;
      # code...
    }
    return json_encode(array('chalan'=>$ch,'invoice'=>$inv));
  }          
}