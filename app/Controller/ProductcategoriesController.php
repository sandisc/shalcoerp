<?php
/*
 * @Controller name: Productcategory Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Productcategory management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class ProductcategoriesController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Product Category Management');
	}
	/*
   	 * Name: index
   	 * Use: Productcategory view.
	 */
	function index() {
        $this->set('pageTitle',' Product Category List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','Productcategory.productname','Productcategory.name','Productcategory.producttaxonomy_id','Productcategory.hscode_id','Productcategory.description','Productcategory.modified','Productcategory.modifiedby');
    	$order = array('Productcategory.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Productcategory->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$productcategorys = $this->Productcategory->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));

		$this->set('productcategory',$productcategorys);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Productcategory.
	 */
	function add($id = null) {
		$this->loadmodel('Producttaxonomy');
		$this->loadmodel('Hscode');
		$id = base64_decode($id);
		$this->Productcategory->id = $id;
		$this->old = $this->Productcategory->findById($id,array('recursive'=>0));
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit Product Category');
			$this->request->data = $this->Productcategory->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Product Category');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'productcategories/">Product Category</a><i class="fa fa-circle"></i></li>');
		$product_cat = $this->Producttaxonomy->find('all',array('fields'=>array('Producttaxonomy.id','Producttaxonomy.name','Producttaxonomy.unit')));
		/* Product name and unit name merge in Array */
		$list = array();
		foreach($product_cat as $cat){
			$list[$cat['Producttaxonomy']['id']] = $cat['Producttaxonomy']['name'].' ( '.$cat['Producttaxonomy']['unit'].' ) ';
		}	
		$this->set('taxonomy',$list);
		/* hs code get */
		$hscode = $this->Hscode->find('list',array('fields'=>array('Hscode.id','Hscode.hscode')));
		$this->set('hscode',$hscode);
	}	

	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Productcategory->set($this->request->data);	
			$response=array();
			/* set reference location if add Productcategory form open in modal dialog means popup */
			if(isset($this->request->data['Productcategory']['targetid'])){
				$response['targetid'] = $this->request->data['Productcategory']['targetid'];
				unset($this->request->data['Productcategory']['targetid']);	
			}
			
			if($this->Productcategory->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Productcategory']['modifiedby'] = $logged_user;				
				$id = '';
				if(isset($this->request->data['Productcategory']['id'])){
					$id = $this->request->data['Productcategory']['id'];
					$this->old = $this->Productcategory->findById($id,array('recursive'=>0));
				}				
				if(empty($id)) {
					$this->request->data['Productcategory']['createdby'] = $logged_user;
				}
				if($this->Productcategory->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$msg = 'Product category has been Updated successfully';
						$result = array_diff_assoc($this->old['Productcategory'],$this->request->data['Productcategory']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Productcategory','Edit',$result);/*Add Data in Log*/												
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Productcategory','Add',$this->request->data['Productcategory']);/*Add Data in Log*/	
						$msg = 'Product category has been Added successfully';
						$response['idvalue'] = $this->Productcategory->getLastInsertID();
						//$response['text'] = $this->request->data['Productcategory']['gdmm'];
					}
					$redirect = '';
					$currentpage_controller = $this->request->params['controller'];
					$refer_url = $this->referer('/', true); /*reference page url*/
					$parse_url_params = Router::parse($refer_url);
					$referencepage_controller = $parse_url_params['controller'];
					/*if(Router::url(null, true) == $this->referer()){*/
					if($currentpage_controller == $referencepage_controller){
						//$this->redirect('/Productcategorys/index');
						$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
					    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					}
					$response['topic'] = 'productcategory';
                    $response['status'] = 'success';
                    $response['message'] = $msg;
                    $response['redirect'] = $redirect;
                    echo json_encode($response);					
				}
			}
			else{
                $Productcategory = $this->Productcategory->invalidFields();                
                $response['status'] = 'error';
                $response['message'] = 'The Product category could not be saved. Please, try again.';
                $response['data'] = compact('Productcategory');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	
	/**
   	 * Name: delete
   	 * Use: delete Productcategory
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Productcategory->id = $id;
		$data = $this->Productcategory->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Productcategory','Delete',$data['Productcategory']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Productcategory->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Product Category has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}

		
}