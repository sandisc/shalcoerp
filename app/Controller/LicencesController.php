<?php
/*
 * @Controller name: Licence Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Licence management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'dompdf', array('file'=>'dompdf/dompdf_config.inc.php'));
class LicencesController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Licence Management');
	}
	/*
   	 * Name: index
   	 * Use: Licence view.
	 */
	function index() {
        $this->set('pageTitle',' Licence List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: View
   	 * Use: Details Of Advance licence no.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Licenceitem');
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'licences/">Licences</a><i class="fa fa-circle"></i></li>');
		$licence = $this->Licence->find('first', array(
   			'conditions' => array('Licence.id' => $id ),'recursive'=> 2
		));
		$this->set('data',$licence);
		$this->set('pageTitle',' View Licence');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','advancelicenceno','issue_date','validity','expiry_date','hscode','first_name','Licence.modified');
    	$order = array('Licence.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Licence->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$Licences = $this->Licence->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('licence',$Licences);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Licence.
	 */
	function add($id = null) {
		$this->loadmodel('Gradetag');
		$this->loadmodel('Hscode');
		$this->loadmodel('Licenceitem');
		$id = base64_decode($id);
		$this->Licence->id = $id;
		$this->old = $this->Licence->findById($id,array('recursive'=>0));
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit Licence');
			$this->request->data = $this->Licence->findById($id);
			$licenitem = $this->Licenceitem->find('all',array('conditions'=>array('Licenceitem.licenceid' => $id)));
			$this->set('licenitem',$licenitem);
			$gradetag = $this->Gradetag->find('all');
			$this->set('gradetag',$gradetag);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Licence');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'licences/">Licences</a><i class="fa fa-circle"></i></li>');
		$hscodes = $this->Hscode->find('all',array('fields'=>array('Hscode.id','Hscode.hscode','Hscode.rate'),'order' => array('Hscode.hscode' => 'ASC')));
		
		 $list = array();
		foreach($hscodes as $hsc){
				$list[$hsc['Hscode']['id']] = array('id'=>$hsc['Hscode']['id'],'name'=>$hsc['Hscode']['hscode'],'data-value'=>$hsc['Hscode']['rate']);
		}	
		$this->set('hscodes',$list);
		$gradetags = $this->Gradetag->find('list',array('fields'=>array('Gradetag.id','Gradetag.gradetagname')));
		$this->set('gradetags',$gradetags);
		$gradetags = $this->Gradetag->find('all',array('fields'=>array('Gradetag.id','Gradetag.gradetagname')));
		$this->set('grades',$gradetags);
		
	}	

	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->loadmodel('Licenceitem');
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Licence->set($this->request->data);	
			$response=array();
			/* set reference location if add Licence form open in modal dialog means popup */
			if(isset($this->request->data['Licence']['targetid'])){
				$response['targetid'] = $this->request->data['Licence']['targetid'];
				unset($this->request->data['Licence']['targetid']);	
			}

			if($this->Licence->validates($this->request->data)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Licence']['modifiedby'] = $logged_user;
				$id = $this->request->data['Licence']['id'];
				if(!empty($id)){
					$this->old = $this->Licence->findById($id,array('recursive'=>0));
					$licenseitems = $this->request->data['Licence'];
					$date = $this->request->data['Licence']['issue_date'];
					$ex_date = $this->request->data['Licence']['expiry_date'];
					$licenseitems['issue_date'] = date('Y-m-d', strtotime(str_replace('/','-', $date)));
					$licenseitems['expiry_date'] = date('Y-m-d', strtotime(str_replace('/','-', $ex_date)));
					$del_ids = $this->request->data['removal_id'];
					if(!empty($del_ids)){
            			$del=array($del_ids);
	         			$condition = array('Licenceitem.id IN ('.$del_ids.')');
	        			$this->Licenceitem->deleteAll($condition,false);
	        		}

					if($this->Licence->save($licenseitems)) {
	            		$item = $this->request->data['Licence']['item'];
	           			foreach($item as $keyitem => $items){
		           			if(!empty($items['id'])){
		           				$this->Licenceitem->id = $items['id'];	
			           			$item[$keyitem]['licenceid'] = $items['licenceid'];
			           			$this->Licenceitem->save($item[$keyitem]);
			           		}else{
			           			$item[$keyitem]['licenceid'] = $id;
	           					$this->Licenceitem->saveall($item[$keyitem]);
			           		}
		        		}
					}
					$licenseitems['item'] = $item;/*updated value so we pass it into activity log*/
	           		$licenseitems['Licenceitem'] = $item;
	           		$Activities->addlog($this->loggedin_user_info['id'],'Licence','Add',$licenseitems);	
				}			

				if(empty($id)) {
					$this->request->data['Licence']['createdby'] = $logged_user;
					$this->request->data['Licence']['issue_date'] = date('Y-m-d', strtotime(str_replace('/','-', $this->request->data['Licence']['issue_date'])));
					$this->request->data['Licence']['expiry_date'] = date('Y-m-d', strtotime(str_replace('/','-', $this->request->data['Licence']['expiry_date'])));
					if($this->Licence->save($this->request->data)) {
						$lastid = $this->Licence->getLastInsertID(); 
			            $item = $this->request->data['Licence']['item'];
			            $totalamount = 0;
			           	foreach($item as $keyitem => $items){
			           		$item[$keyitem]['licenceid'] = $lastid;
			           		$this->Licenceitem->saveall($item[$keyitem]);
				        }
		          	}
		          	$licenseitems['item'] = $item;/*updated value so we pass it into activity log*/
		           	$licenseitems['Licenceitem'] = $item;
		           	$Activities->addlog($this->loggedin_user_info['id'],'Licence','Add',$licenseitems);
				}
				if(!empty($id)) { 
					$msg = 'Licence has been Updated successfully';
					$result = array_diff_assoc($this->old['Licence'],$this->request->data['Licence']);/*Get only changed fields*/
					$Activities->addlog($this->loggedin_user_info['id'],'Licence','Edit',$result);/*Add Data in Log*/												
				}else { 
					$Activities->addlog($this->loggedin_user_info['id'],'Licence','Add',$this->request->data['Licence']);/*Add Data in Log*/	
					$msg = 'Licence has been Added successfully';
					$response['idvalue'] = $this->Licence->getLastInsertID();
					//$response['text'] = $this->request->data['Licence']['gdmm'];
				}
				$redirect = '';
				$currentpage_controller = $this->request->params['controller'];
				$refer_url = $this->referer('/', true); /*reference page url*/
				$parse_url_params = Router::parse($refer_url);
				$referencepage_controller = $parse_url_params['controller'];
				/*if(Router::url(null, true) == $this->referer()){*/
				if($currentpage_controller == $referencepage_controller){
					//$this->redirect('/Licences/index');
					$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
				    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
				}
				$response['topic'] = 'Licence';
	            $response['status'] = 'success';
	            $response['message'] = $msg;
	            $response['redirect'] = $redirect;
	            echo json_encode($response);				
			}
			else{
                $Licence = $this->Licence->invalidFields(); 
                $response['status'] = 'error';
                $response['message'] = 'The Licence could not be saved. Please, try again.';
                $response['data'] = compact('Licence');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	/*
   	 * Name: getGrades via Ajax
   	 * Use: Fetch Grades for particular Standard .
	 */
	function getGrades($id) {
		$this->loadmodel('Grade');
		$this->autoRender = false;
   		$grade_data = $this->Grade->find('all',array('fields' => array('Grade.id','Grade.grade_name'),'conditions'=> array('Grade.standard_id'=>$id)));
   		
   		echo '<select>';
		echo '<option value="">Select Grade</option>';
			foreach($grade_data as $grades) 
			{ 
	  		echo '<option value="'.$grades['Grade']['id'].'">'.$grades['Grade']['grade_name'].'</option>'."\n";
	  		}
		echo '</select>';
		exit;
	}
	/**
   	 * Name: delete
   	 * Use: delete Licence
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Licence->id = $id;
		$this->autoRender = false;
		$this->loadmodel('Licenceitem');		
		$data = $this->Licence->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Licence','Delete',$data['Licence']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Licence->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Licence has been Deleted successfully</div>'));
		}
		$this->Licenceitem->deleteAll(array('Licenceitem.licenceid' => $id), true);		
		return $this->redirect(array('action' => 'index'));
	}
	/* 
	Name : Extension
	Use : Extension Date add
	*/
	function extension(){
		$this->loadmodel('Licenceitem');
		$data = json_decode(stripslashes($_POST['data']),true);
		if(empty($_POST['id'])) {
			$this->redirect(array('controller' => 'licences', 'action' => 'index'));
		}
	
		$this->autoRender = false;	
   		$logged_user_id = $this->Auth->user('id');
   		$date = date('Y-m-d', strtotime(str_replace('/','-', $_POST['ex_date'])));
   		$this->Licence->id = $_POST['id'];
   		$status_update  = array('is_extended'=>'1','ex_validity' => $_POST['ex_validity'], 'ex_expirydate'=>$date,'ex_created'=>$logged_user_id,'ex_modified'=>$logged_user_id); 
   		$this->Licence->save($status_update);
   		
   		$i = 0;
   		$size= count($data['id']);
   		for($i = 0;$i < $size; $i++){
   			$vara['row'][] = array('id'=>$data['id'][$i],'ex_qty'=>$data['ex_qty'][$i],'im_qty'=>$data['im_qty'][$i]);
   		}
   		foreach($vara['row'] as $keyitem => $items){
   				
   			$this->Licenceitem->id = $items['id'];	
   			$item_update  = array('ex_qty' => $items['ex_qty'],'im_qty'=>$items['im_qty']);
   			$this->Licenceitem->save($item_update);
		}exit;
	}
	/*
	Name : extendlicencereminder
	Use : This is Simple reminder for the admin for expiry of Advance Licence
	*/
	function extendlicencereminder(){
		$this->loadmodel('User');
		$this->autoRender = false;	
   		$logged_user_id = $this->Auth->user('id');
   		//$date = date('Y-m-d', strtotime(str_replace('/','-', $_POST['ex_date'])));
   		$date = date('Y-m-d', strtotime("+1 month"));
   		$cronRemider = $this->Licence->find('all',array('conditions'=>array('Licence.expiry_date' => $date)));
   		$admin_user = $this->User->find('all',array('conditions'=>array('User.usertype_id' => 5),'fields' => array('User.first_name','User.last_name','User.email')));
   		$roles = array();
		foreach($admin_user as $user)
		{
    		$roles[] = $user['User']['email'];
		}

		if(!empty($cronRemider)){
   			foreach($cronRemider as $remindemail){
	   			$roles[] = $remindemail['User']['email'];
		   		$Email = new CakeEmail();
				$Email->to($roles);
				$Email->subject('Reminder Licence no expiry.');
				$Email->from(array('shalco@gmail.com' => 'My Site'));
				$Email->emailFormat('html');
				$Email->send('Hello Sir,<br>Your Licence no is going to expire on date :'.$remindemail['Licence']['expiry_date'].'<br><br><br>Licence number:'.$remindemail['Licence']['advancelicenceno'].'<br>Issue date:'.$remindemail['Licence']['issue_date'].'<br>Issues by:'.$remindemail['User']['first_name'].' '.$remindemail['User']['last_name'].'<br>Expiry date:'.$remindemail['Licence']['expiry_date'].'<br><br> Thanks');
			}
		}
	}

	/**
   	 * Name: licenceimportpdf
   	 * Use: licence import download as pdf
     * @param int $id id of licence
     * @return pdf download
     */
	function licenceimportpdf($id = null){
		$this->loadmodel('Licenceitem');
		$this->loadmodel('Licenceimport');
		$this->pdfConfig = array(
			'filename' => 'licence',
			'download' => $this->request->query('download')
		);
		$this->Licence->unbindModel(array('belongsTo' =>array('User')));
		$this->Licence->bindModel(array('hasMany' => array('Licenceimport' => array(
	            'className' => 'Licenceimport',
	            'foreignKey' => 'licenceid',
	            'conditions' => array('Licenceimport.licenceid'=>$id)
	        ))));

		$licencedata = $this->Licence->find('first', array('conditions' => array(
        'Licence.id' => $id),'recursive'=> 2));
        $this->set('licencedata', $licencedata);

    	/* Make sure the controller doesn't auto render. */
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		$view->set('text', 'Hello World');		
		$view->viewPath = 'Licences';
		ini_set('memory_limit', '1024M');
		$html = $view->render('licenceimportpdf');
   		$this->set('post',$html);
	}	

	/**
   	 * Name: licenceexportpdf
   	 * Use: licence export download as pdf
     * @param int $id id of licence
     * @return pdf download
     */
	function licenceexportpdf($id = null){
		$this->loadmodel('Licenceitem');
		$this->loadmodel('Licenceexport');
		$this->pdfConfig = array(
			'filename' => 'licence',
			'download' => $this->request->query('download')
		);
		$this->Licence->unbindModel(array('belongsTo' =>array('User')));
		$this->Licence->bindModel(array('hasMany' => array('Licenceexport' => array(
	            'className' => 'Licenceexport',
	            'foreignKey' => 'licenceid',
	            'conditions' => array('Licenceexport.licenceid'=>$id)
	        ))));

		$licencedata = $this->Licence->find('first', array('conditions' => array('Licence.id' => $id),'recursive'=> 2));
        $this->set('licencedata', $licencedata);

    	/* Make sure the controller doesn't auto render. */
		$this->autoRender = true;
		/* Set up new view that won't enter the ClassRegistry */
		$view = new View($this, false);
		$view->layout = null; 
		$view->set('text', 'Hello World');		
		$view->viewPath = 'Licences';
		ini_set('memory_limit', '1024M');
		$html = $view->render('licenceexportpdf');
   		$this->set('post',$html);
	}		
	/**
   	 * Name: extendqty
   	 * Use: Extend licence import and export qty
     * @param int $id id of licence
     * @return pdf download
     */
	function extendqty(){
		$this->loadmodel('Licenceitem');
		$this->autoRender = false;
		$licenceitem = $this->Licenceitem->find('all',array('conditions'=>array('Licenceitem.licenceid' => $_POST['id'])));
		$item_array = array();
		foreach($licenceitem as $key => $value){
			$item_array[] = array('id'=>$value['Licenceitem']['id'],'gradetag'=>$value['Gradetag']['gradetagname'],'ex_qty'=>$value['Licenceitem']['ex_qty'],'im_qty'=>$value['Licenceitem']['im_qty']);
		}
		
		echo json_encode($item_array);die;	
	}
}