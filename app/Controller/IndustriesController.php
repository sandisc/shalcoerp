<?php
/*
 * @Controller name: Industry Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Industry management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class IndustriesController extends AppController {
	var $name = 'Industries';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Industry Management');
	}

	/*
   	 * Name: index
   	 * Use: Industry view.
	*/
	function index() {
        $this->set('pageTitle','Industry  List');
        $this->set('ajaxaction','ajaxlisting');
        
	}
	
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','Industry.industryname','Industry.modified','first_name');
    	$order = array('id' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Industry->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$industries = $this->Industry->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('industry',$industries);
		$this->set('count',$count);
	}
	/*
   	 * Name: add
   	 * Use: user can add,update Industry.
	 */
	function add($id = null) {
		$id = base64_decode($id);
        $this->Industry->id = $id;		
		$this->old = $this->Industry->findById($id,array('recursive'=>0));
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Industry->set($this->request->data);
			if($this->Industry->validates($this->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Industry']['modifiedby'] = $logged_user;				
				if(!empty($id)) {				
					$this->request->data['Industry']['id'] = $id;
				}else{
					$this->request->data['Industry']['createdby'] = $logged_user;
				}
				if($this->Industry->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Industry'],$this->request->data['Industry']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Industry','Edit',$result);/*Add Data in Log*/						
						$msg = 'Industry has been Updated successfully';						
					}else { 
						$msg = 'Industry has been Added successfully';
						$Activities->addlog($this->loggedin_user_info['id'],'Industry','Add',$this->request->data['Industry']);/*Add Data in Log*/
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));		
					$this->redirect('/industries/');
				}
			}
			else{
				$errors = $this->Industry->validationErrors;
			}
		}
		/* End : Add/Edit Submit Form Event*/

		if(!empty($id)){
			$this->set('pageTitle','Edit Industry');
			$this->request->data = $this->Industry->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Industry');
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'industries/">Industry</a><i class="fa fa-circle"></i></li>');		
	}
	
	/*
   	 * Name: delete
   	 * Use: admin can delte industry.
	 */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Industry->id = $id;
		$this->autoRender = false;
		$this->loadmodel('Client');
		$res = $this->Client->find('all',array('conditions' => array('FIND_IN_SET(\''. $id .'\',Client.industryid)'),'recursive'=>-1));
		if(empty($res)){
			$data = $this->Industry->findById($id,array('recursive'=>0));
			if(!empty($data)){
				$Activities = new ActivitiesController;
	    		$Activities->addlog($this->loggedin_user_info['id'],'Industry','Delete',$data['Industry']);/*Add Data in Log*/
	    	}		
			if (!$id) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
			}
			if ($this->Industry->delete($id)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Industry has been Deleted successfully</div>'));			
			}	
		}		
		return $this->redirect(array('action' => 'index'));		
	}	
}