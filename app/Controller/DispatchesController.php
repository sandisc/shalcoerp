<?php
/*
 * @Controller name: Dispatch Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Dispatch management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class DispatchesController extends AppController {
	var $name = 'Dispatches';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Dispatch Location Management');
	}
	/*
   	 * Name: index
   	 * Use: Dispatch view.
	*/
	function index() {
        $this->set('pageTitle','Dispatch Location List');
        $this->set('ajaxaction','ajaxlisting');
        
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	*/
	function ajaxlisting(){
    	$column = array('id','location','code');
    	$order = array('id' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Dispatch->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$dispatches = $this->Dispatch->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('dispatch',$dispatches);
		$this->set('count',$count);
	}
	/*
   	 * Name: add
   	 * Use: user can add,update Dispatch.
	 */
	function add($id = null) {
		$id = base64_decode($id);
        $this->Dispatch->id = $id;		
		$this->old = $this->Dispatch->findById($id,array('recursive'=>0));
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Dispatch->set($this->request->data);
			if($this->Dispatch->validates($this->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Dispatch']['modifiedby'] = $logged_user;				
				if(!empty($id)) {				
					$this->request->data['Dispatch']['id'] = $id;
				}else{
					$this->request->data['Dispatch']['createdby'] = $logged_user;
				}
				if($this->Dispatch->save($this->request->data)) {
					$Activities = new ActivitiesController;
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Dispatch'],$this->request->data['Dispatch']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Dispatch','Edit',$result);/*Add Data in Log*/						
						$msg = 'Dispatch Location has been Updated successfully';						
					}else { 
						$msg = 'Dispatch Location has been Added successfully';
						$Activities->addlog($this->loggedin_user_info['id'],'Dispatch','Add',$this->request->data['Dispatch']);/*Add Data in Log*/
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));		
					$this->redirect('/dispatches/');
				}
			}
			else{
				$errors = $this->Dispatch->validationErrors;
			}
		}
		/* End : Add/Edit Submit Form Event*/

		if(!empty($id)){
			$this->set('pageTitle','Edit Dispatch Location');
			$this->request->data = $this->Dispatch->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Dispatch Location');
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'dispatches/">Dispatch</a><i class="fa fa-circle"></i></li>');		
	}
	
	/*
   	 * Name: delete
   	 * Use: admin can delte standard.
	 */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Dispatch->id = $id;
		$data = $this->Dispatch->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Dispatch','Delete',$data['Dispatch']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Dispatch->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Dispatch Location has been Deleted successfully</div>'));			
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));		
	}
	/*
   	 * Name: orderonlocation
   	 * Use: Location of Listing proformainvoice.
	*/
	function orderonlocation($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Dispatch');
        $this->set('pageTitle','Order on location');
        $this->set('ajaxaction','orderonlocationlist/'.$id);
        $this->set('dispatch_loc',$this->Dispatch->find('first',array('conditions'=>array('Dispatch.id'=>$id),'fields' => array('Dispatch.location'),'recursive' => 0)));
	}	
	/*	
		Name : List
		Use : Particular one location list
	*/
	function orderonlocationlist(){
		$id = $this->params['pass'][0];
		$this->loadmodel('Order');
		$column = array('orderno','company_name','dispatchcode');
    	$order = array('Order.id' => 'desc');
		$res = $this->datatable_append($order,$column);	
		$count = $this->Order->find('count',array('conditions'=>array($res['con'],'Order.dispatch_id'=>$id),'order'=>$res['order_by']));
		$locationlist = $this->Order->find('all',array('conditions'=>array($res['con'],'Order.dispatch_id'=>$id),'fields' => array('Order.id','Order.orderno','Client.company_name','Order.dispatchcode'),'order'=>$res['order_by'],'recursive' => 0));
		$this->set('locationlist',$locationlist);
		$this->set('count',$count);
			
	}
}