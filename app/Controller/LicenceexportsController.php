<?php
/*
 * @Controller name: Licence Export Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Licence Export management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
App::uses('CakeEmail', 'Network/Email');
class LicenceexportsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Licence Export Management');
	}
	/*
   	 * Name: index
   	 * Use: Licence view.
	 */
	function index() {
        $this->set('pageTitle',' Licence Export List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: View
   	 * Use: Details Of licence export
	*/
	function view($id = null) {
		$id = base64_decode($id);
		$this->loadmodel('Licenceexportitem');
		$this->loadmodel('Licenceitem');
		//$this->loadmodel('Invoiceitem');
		$this->loadmodel('Invoice');
		$this->loadmodel('Licence');
		//$this->loadmodel('Licenceitem');

		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'licenceexports/">Licence Export</a><i class="fa fa-circle"></i></li>');
		$this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem'),'belongsTo' => array('Hscode','Order')));
		$this->Licence->unbindModel(array('hasMany' => array('Licenceitem'),'belongsTo' => array('User')));
		/*$this->Licenceexportitem->bindModel(array('belongsTo' => array('Licenceitem' => array(
	            'className' => 'Licenceitem',
	             'foreignKey' => 'licenceitemid',
	            'conditions' => array('Licenceitem.id = Licenceexportitem.licenceitemid'),
	     ))));*/
		$licence = $this->Licenceexport->find('first', array(
   			'conditions' => array('Licenceexport.id' => $id ),'recursive'=>2
		));
		$this->set('data',$licence);
		
		//$this->set('licenceitems',$licenceitems);
		$this->set('pageTitle',' View Licence Export');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','advancelicenceno','invoiceno','invoicedate','sbno','sbdate','company_name','first_name','Licenceexport.modified',);
    	$order = array('Licenceexport.modified' => 'desc');  
		$res = $this->datatable_append($order,$column);
		$count = $this->Licenceexport->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$Licences = $this->Licenceexport->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('licence',$Licences);
		$this->set('count',$count);		
	}
	
	
	/**
   	 * Name: delete
   	 * Use: delete Licence export
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null) {
		$id = base64_decode($id);
		$this->autoRender = false;
		$this->loadmodel('Licenceexportitem');
		$this->Licenceexport->id = $id;
		$data = $this->Licenceexport->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Licence Export','Delete',$data['Licenceexport']);/*Add Data in Log*/
    	}		
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Licenceexport->delete($id)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Licence Export has been Deleted successfully</div>'));
		}
		$this->Licenceexportitem->deleteAll(array('Licenceexportitem.licenceexportid' => $id), true);
		return $this->redirect(array('action' => 'index'));
	}

	/*
	*
	*	Name : ExportGradetagbalanceQty
	*	Use : Count balance value as per Gradetag and Licence item
	*/
	function ExportGradetagbalanceQty($licenceid = null,$gradetag_id = null){
		$this->autoRender = false;
		$this->loadmodel('Licence');
		$this->loadmodel('Invoice');
		$this->Licence->unbindModel(array('hasMany' => array('Licenceitem'),'belongsTo' => array('Hscode')));
		$this->Invoice->unbindModel(array('hasMany' => array('Invoiceitem'),'belongsTo' => array('Hscode','Order')));
		$data = $this->Licenceexport->find('all',array('conditions'=>array('Licenceexport.licenceid' => $licenceid),'recursive'=>2));

		$used_qty = 0;
		$allow_qty = 0;
		/*If licenceitem and grade tag not used in invoice means no data in licence export than*/
		if(empty($data)){
			$this->loadmodel('Licenceitem');
			$data = $this->Licenceitem->find('first',array('conditions'=>array('licenceid' => $licenceid,'gradetagid' => $gradetag_id),'recursive'=>-1));
			$allow_qty = $data['Licenceitem']['ex_qty'];
		}
		/*If licenceitem and grade tag used in invoice means data in licence export than*/
		else{
			foreach($data as $all_value){
				foreach($all_value['Licenceexportitem'] as $export_value){
				  	if(!empty($gradetag_id)){
						if($export_value['Licenceitem']['gradetagid'] == $gradetag_id){
							$allow_qty = $export_value['Licenceitem']['ex_qty'];
							$used_qty += $export_value['exportqty'];	
						}
					}	
				}
			}
		}

		$balance_qty = $allow_qty - $used_qty;
		return number_format((float)$balance_qty,3, '.', '');
	}
}