<?php
/*
 * @Controller name: Division Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Division management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class DivisionsController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();
		$this->set('mainTitle','Division Management');
	}
	/*
   	 * Name: index
   	 * Use: Division view.
	 */
	function index() {
        $this->set('pageTitle',' Division List');
        $this->set('ajaxaction','ajaxlisting');
	}
	/*
   	 * Name: View
   	 * Use: Details Of Division no.
	*/
	function view($id = null) {
		$id = base64_decode($id);
		
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'Divisions/">Divisions</a><i class="fa fa-circle"></i></li>');
		$Division = $this->Division->find('first', array(
   			'conditions' => array('Division.id' => $id ),'recursive'=> 2
		));
		$this->set('data',$Division);
		$this->set('pageTitle',' View Division');
	}
	/*
   	 * Name: Listing with Ajax
   	 * Use: View,Multiple search,Multiple delete From All Records.
	 */
	function ajaxlisting(){
    	$column = array('id','division_name','division_alias','excise_regn_no','first_name','Division.modified');
    	$order = array('Division.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$count = $this->Division->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$divisions = $this->Division->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('divisions',$divisions);
		$this->set('count',$count);		
	}

	/*
   	 * Name: add
   	 * Use: user can add,update Division.
	 */
	function add($id = null) {
		$id = base64_decode($id);
		$this->Division->id = $id;
		$this->old = $this->Division->findById($id,array('recursive'=>0));
		
		if(!empty($id)) {
			$this->set('pageTitle','Edit Division');
			$this->request->data = $this->Division->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Division');		
		}
		$this->set('id',$id);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'divisions/">Divisions</a><i class="fa fa-circle"></i></li>');
		
	}	

	/*Form submit usign ajax*/
	function ajaxsubmit(){
		$this->autoRender = false;
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {  
			$this->Division->set($this->request->data);	
			$response=array();
			/* set reference location if add Division form open in modal dialog means popup */
			if(isset($this->request->data['Division']['targetid'])){
				$response['targetid'] = $this->request->data['Division']['targetid'];
				unset($this->request->data['Division']['targetid']);	
			}

			if($this->Division->validates($this->request->data)) {
				$Activities = new ActivitiesController;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Division']['modifiedby'] = $logged_user;
				$id = $this->request->data['Division']['id'];
				if(!empty($id)){
					$this->old = $this->Division->findById($id,array('recursive'=>0));
					$division = $this->request->data['Division'];
					$this->Division->save($division);
					$Activities->addlog($this->loggedin_user_info['id'],'Division','Add',$division);	
				}			

				if(empty($id)) {
					$this->request->data['Division']['createdby'] = $logged_user;
					$this->Division->save($this->request->data); 
					$division = $this->request->data['Division'];
		          	$Activities->addlog($this->loggedin_user_info['id'],'Division','Add',$division);
				}
				if(!empty($id)) { 
					$msg = 'Division has been Updated successfully';
					$result = array_diff_assoc($this->old['Division'],$this->request->data['Division']);/*Get only changed fields*/
					$Activities->addlog($this->loggedin_user_info['id'],'Division','Edit',$result);/*Add Data in Log*/												
				}else { 
					$Activities->addlog($this->loggedin_user_info['id'],'Division','Add',$this->request->data['Division']);/*Add Data in Log*/	
					$msg = 'Division has been Added successfully';
					$response['idvalue'] = $this->Division->getLastInsertID();
					//$response['text'] = $this->request->data['Division']['gdmm'];
				}
				$redirect = '';
				$currentpage_controller = $this->request->params['controller'];
				$refer_url = $this->referer('/', true); /*reference page url*/
				$parse_url_params = Router::parse($refer_url);
				$referencepage_controller = $parse_url_params['controller'];
				/*if(Router::url(null, true) == $this->referer()){*/
				if($currentpage_controller == $referencepage_controller){
					//$this->redirect('/Divisions/index');
					$redirect = WEBSITE_PATH.''.$this->params['controller'].'/index/';
				    $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
				}
				$response['topic'] = 'Division';
	            $response['status'] = 'success';
	            $response['message'] = $msg;
	            $response['redirect'] = $redirect;
	            echo json_encode($response);				
			}
			else{
                $Division = $this->Division->invalidFields(); 
                $response['status'] = 'error';
                $response['message'] = 'The Division could not be saved. Please, try again.';
                $response['data'] = compact('Division');
                echo json_encode($response);
			}
		}
		/* End : Add/Edit Submit Form Event*/	
	}		
	
	/**
   	 * Name: delete
   	 * Use: delete Division
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{
		$id = base64_decode($id);
		$this->Division->id = $id;
		$this->autoRender = false;
			
		$data = $this->Division->findById($id,array('recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Division','Delete',$data['Division']);/*Add Data in Log*/
    	}	
    	$this->Division->delete($id);	
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		
		return $this->redirect(array('action' => 'index'));
	}
		
}