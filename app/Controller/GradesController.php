<?php
/*
 * @Controller name: Grade Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to Grade management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class GradesController extends AppController {
	var $name = 'Grades';
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	function beforeFilter() {
		parent::beforeFilter();		
		$this->set('mainTitle','Grade Management');
	}
	/*
   	 * Name: index
   	 * Use: grade listing.
	 */
	function index() {
        $this->set('pageTitle',' Grade List');
        $this->set('ajaxaction','ajaxlisting');
	}
	function ajaxlisting(){
    	$column = array('id','standard_name','grade_name','gradetagname','uns','Grade.modified','User.first_name','Grade.status');
    	$order = array('Grade.modified' => 'desc');  

		$res = $this->datatable_append($order,$column);
		$this->Grade->unbindModel(array('belongsTo' => array('Chemical','Physical','Mechanical')));
		$count = $this->Grade->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
		$this->Grade->unbindModel(array('belongsTo' => array('Chemical','Physical','Mechanical')));
		$grades = $this->Grade->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
		$this->set('grade',$grades);
		$this->set('count',$count);
	}

	/*
   	 * Name: view
   	 * Use: Grade view.
	 */
	function view($id = null) {
		$id = base64_decode($id);
		if(empty($id)) {
			$this->redirect(array('controller' => 'grades', 'action' => 'index'));
		}
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'grades/">Grades</a><i class="fa fa-circle"></i></li>');
		$gradesitem = $this->Grade->find('first', array(
   			'conditions' => array('Grade.id' => $id ),
    		'recursive' => 1
		));
		$this->set('gradedata',$gradesitem);

		/*Now, fetching records of grade and its mechanical property which has same grade name*/
		$same_gradename_id = $this->Grade->find('all',array('conditions'=>array('Grade.grade_name'=>$gradesitem['Grade']['grade_name'],"Grade.id NOT IN ($id)"),'fields'=>array('Grade.id'),'recursive' => -1)); 
		$gdid = array();
		foreach($same_gradename_id as $grd_id){
			$gdid[] = $grd_id['Grade']['id'];
		}
		
		if(!empty($gdid)){
			$implode_grade = implode(',',$gdid); 
			$ar_grd = array($implode_grade);
		}
		$this->loadmodel('Mechanical');
		$same_grade_mechanical = '';
		if(!empty($implode_grade)){
			$same_grade_mechanical = $this->Mechanical->find('all',array('conditions' => array("grade_id IN ($implode_grade)"),'recursive'=>1));
		}
		$this->set('same_grade_mechanical',$same_grade_mechanical);
		
		$this->set('pageTitle','Grade View');
	}

	/*
   	 * Name: add
   	 * Use: admin can add,update grade.
	 */
	function add($id = null) {
		$this->loadmodel('Standard');
		$this->loadmodel('Gradetag');
		$this->loadmodel('Physical');
		$this->loadmodel('Chemical');
		$this->loadmodel('Mechanical');
		$id = base64_decode($id);
        $this->Grade->id = $id;		
		$this->old = $this->Grade->findById($id,array('recursive'=>0));
		/* Start : Add/Edit Submit Form Event */
	    if(!empty($this->request->data)) {
			$this->request->data['Grade']['grade_name'] = trim($this->request->data['Grade']['grade_name']);
			$this->Grade->set($this->request->data);
			if($this->Grade->validates($this->request->data)) {
				$logged_user = $this->Auth->user('id');
				$this->request->data['Grade']['modifiedby'] = $logged_user;
				if(!empty($id)) {
					$this->request->data['Grade']['id'] = $id;
				}
				else{
					$this->request->data['Grade']['createdby'] = $logged_user;
				}
				$grade_name = $this->Grade->find('all',array('conditions'=>array('Grade.grade_name' => trim($this->request->data['Grade']['grade_name'])),'fields'=>array('Grade.id')));
				foreach($grade_name as $grd_id){
					$gdid[] = $grd_id['Grade']['id'];
				}
				if(!empty($gdid)){
				$implode_grade = implode(',',$gdid);
				$ar_grd = array($implode_grade);
				}
				if($this->Grade->save($this->request->data)){
					$last_id = $this->Grade->getLastInsertID();				
					if(empty($id)){
						$mechanicals['Mechanical']['grade_id'] = $last_id;
						$mechanicals['Mechanical']['createdby'] = $logged_user;
						$mechanicals['Mechanical']['modifiedby'] = $logged_user;
						$this->Mechanical->save($mechanicals);							
						if(!empty($ar_grd)){					 	
					 		$chemicals = $this->Chemical->find('first',array('conditions'=>array('Chemical.grade_id' => $ar_grd),'recursive'=> -1));
					 		unset($chemicals['Chemical']['id']);
					 	}
					 	//if(!empty($chemicals)){
					 		$chemicals['Chemical']['grade_id'] = $last_id;					 		
					 		$chemicals['Chemical']['createdby'] = $logged_user;
					 		$chemicals['Chemical']['modifiedby'] = $logged_user;
							
							$this->Chemical->save($chemicals);
						//}
						if(!empty($ar_grd)){	
							$physicals = $this->Physical->find('first',array('conditions'=>array('Physical.grade_id' => $ar_grd),'recursive'=> -1));
							unset($physicals['Physical']['id']);
						}
						//if(!empty($physicals)){
							$physicals['Physical']['grade_id'] = $last_id;							
							$physicals['Physical']['createdby'] = $logged_user;
							$physicals['Physical']['modifiedby'] = $logged_user;
							//if(isset($physicals)) {  }
							$this->Physical->save($physicals);
						//}						
					}
					$Activities = new ActivitiesController;                   	
					if(!empty($id)) { 
						$result = array_diff_assoc($this->old['Grade'],$this->request->data['Grade']);/*Get only changed fields*/
						$Activities->addlog($this->loggedin_user_info['id'],'Grade','Edit',$result);/*Add Data in Log*/
						$msg = 'Grade has been Updated successfully';						
					}else { 
						$Activities->addlog($this->loggedin_user_info['id'],'Grade','Add',$this->request->data['Grade']);/*Add Data in Log*/
						$msg = 'Grade has been Added successfully';
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));		
					$this->redirect('/grades/index');
				}
			}
			else{
				$errors = $this->Grade->validationErrors;
			}
		}
		/* End : Add/Edit Submit Form Event */

		if(!empty($id)) {
			$this->set('pageTitle','Edit Grade');
			$this->request->data = $this->Grade->findById($id);
			if(empty($this->request->data)){/*If Record not exist than*/
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Grade');
		}
		$this->set('id',$id);
		$std_id = $this->Standard->find('list', array(
        	'fields' => array('Standard.id', 'Standard.standard_name'),
        	'recursive' => 0
    	));
		$this->set('std_id',$std_id);
		$grade_tag = $this->Gradetag->find('list', array(
        	'fields' => array('Gradetag.id', 'Gradetag.gradetagname'),
        	'recursive' => 0
    	));
    	$this->set('grade_tag',$grade_tag);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'grades/">Grade</a><i class="fa fa-circle"></i></li>');		
	}

	/*
   	 * Name: physical
   	 * Use: admin can add,update physical.
	 */
	function physical($gradeid = null) {
		$this->loadmodel('Physical');
		$this->loadmodel('Grade');
		$gradeid = base64_decode($gradeid);
	    $physical_data = $this->Physical->findByGradeId($gradeid);
	    $id = null;
	    if(!empty($physical_data)){
	    	$id = $physical_data['Physical']['id'];
	    }
		$this->set('physical',$physical_data);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'grades/">Grade</a><i class="fa fa-circle"></i></li>');
			/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Physical->set($this->request->data);
			if($this->Physical->validates($this->request->data)) {
				$this->request->data['Physical']['grade_id'] = $gradeid;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Physical']['modifiedby'] = $logged_user;				
				if(!empty($id)) {
					$this->request->data['Physical']['id'] = $id;					
				}else{
					$this->request->data['Physical']['createdby'] = $logged_user;
				}
				if($this->Physical->save($this->request->data)) {
					if(!empty($id)) { 
						$physic = $this->Physical->find('first',array('conditions'=>array('Physical.grade_id' => $gradeid),'fields' =>array('Grade.grade_name')));
					$grade_all = $this->Grade->find('all',array('conditions'=>array('Grade.grade_name' => $physic['Grade']['grade_name']),'fields'=>array('Grade.id')));
					$grd_id = array();
					foreach($grade_all as $grd){
						if($gradeid != $grd['Grade']['id']){
							$grd_id[] = $grd['Grade']['id'];
						}
					}

					$implode_gd = implode(',',$grd_id);
					if(!empty($implode_gd)){
						$phy_ids = $this->Physical->find('all',array('conditions' => array("grade_id IN ($implode_gd)"),'fields' =>array('Physical.id')));
						foreach($phy_ids as $ids){
							$id = $ids['Physical']['id'];
							$this->request->data['Physical']['id'] = $id;	
							unset($this->request->data['Physical']['grade_id']);
							$this->Physical->save($this->request->data);
						}
					}
						$msg = 'Physical Property has been Updated successfully';						
					}else { 
						$msg = 'Physical Property has been Added successfully';
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					$this->redirect('/grades/index');
				}
			}
		}
		if(!empty($id)) {		
			$this->set('pageTitle','Edit Physical Property');
			$this->request->data = $this->Physical->findById($id);/*If Record not exist than*/
			if(empty($this->request->data)){
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Physical Property');
		}
	}

	/**
   	 * Name: mechanical
   	 * Use: admin can add,update mechanical.
	 */
	function mechanical($gradeid = null) {
		$this->loadmodel('Mechanical');
		$gradeid = base64_decode($gradeid);
	    $mechanical_data = $this->Mechanical->findByGradeId($gradeid);
	    $id = null;
	    if(!empty($mechanical_data)){
	    	$id = $mechanical_data['Mechanical']['id'];
	    }
		$this->set('mechanical',$mechanical_data);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'grades/">Grade</a><i class="fa fa-circle"></i></li>');
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Mechanical->set($this->request->data);
			if($this->Mechanical->validates($this->request->data)) {
				$this->request->data['Mechanical']['grade_id'] = $gradeid;
				$logged_user = $this->Auth->user('id');
				$this->request->data['Mechanical']['modifiedby'] = $logged_user;				
				if(!empty($id)) {
					$this->request->data['Mechanical']['id'] = $id;
				}else{
					$this->request->data['Mechanical']['createdby'] = $logged_user;
				}
				if($this->Mechanical->save($this->request->data)) {
					if(!empty($id)) { 
						$msg = 'Mechanical Property has been Updated successfully';						
					}else { 
						$msg = 'Mechanical Property has been Added successfully';
					}
					$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
					$this->redirect('/grades/index');
				}
			}
		}
		if(!empty($id)) {
			$this->set('pageTitle','Edit Mechanical Property');
			$this->request->data = $this->Mechanical->findById($id);/*If Record not exist than*/
			if(empty($this->request->data)){
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Mechanical Property');
		}
	}

	/**
   	 * Name: chemical
   	 * Use: admin can add,update chemical.
	 */
	function chemical($gradeid = null) {
		$this->loadmodel('Chemical');
		$this->loadmodel('Grade');
		$gradeid = base64_decode($gradeid);
		$chemical_data = $this->Chemical->findByGradeId($gradeid);
	    $id = null;
	    if(!empty($chemical_data)){
	    	$id = $chemical_data['Chemical']['id'];
	    }
		$this->set('chemical',$chemical_data);
		$this->set('middle_breadcrumb','<li><a href="'.WEBSITE_PATH.'grades/">Grade</a><i class="fa fa-circle"></i></li>');
		/* Start : Add/Edit Submit Form Event*/
	    if(!empty($this->request->data)) {
			$this->Chemical->set($this->request->data);
			$this->request->data['Chemical']['grade_id'] = $gradeid;
			$logged_user = $this->Auth->user('id');
			$this->request->data['Chemical']['modifiedby'] = $logged_user;
			if(!empty($id)) {
				$this->request->data['Chemical']['id'] = $id;		
			}
			else{
				$this->request->data['Chemical']['createdby'] = $logged_user;
			}
			if($this->Chemical->save($this->request->data)) {
				if(!empty($id)) {
					$chemi = $this->Chemical->find('first',array('conditions'=>array('Chemical.grade_id' => $gradeid),'fields' =>array('Grade.grade_name')));
					$grade_all = $this->Grade->find('all',array('conditions'=>array('Grade.grade_name' => $chemi['Grade']['grade_name']),'fields'=>array('Grade.id')));
					$grd_id = array();
					foreach($grade_all as $grd){
						if($gradeid != $grd['Grade']['id']){
							$grd_id[] = $grd['Grade']['id'];
						}
					}
					$implode_gd = implode(',',$grd_id);
					if(!empty($implode_gd)){
						$chemi_ids = $this->Chemical->find('all',array('conditions' => array("grade_id IN ($implode_gd)"),'fields' =>array('Chemical.id')));
						foreach($chemi_ids as $ids){
								$id = $ids['Chemical']['id'];
								$this->request->data['Chemical']['id'] = $id;
								unset($this->request->data['Chemical']['grade_id']);
								$this->Chemical->save($this->request->data);
						}
					}
					$msg = 'Chemical Property has been Updated successfully';						
				}else { 
					$msg = 'Chemical Property has been Added successfully';
				}
				$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i>'.$msg.'</div>'));
				$this->redirect('/grades/index');
			}
		}
		if(!empty($id)) {
			$this->set('pageTitle','Edit Chemical Property');
			$this->request->data = $this->Chemical->findById($id);/*If Record not exist than*/
			if(empty($this->request->data)){
				$this->redirect('index');
			}
		}
		else{
			$this->set('pageTitle','Add Chemical Property');
		}
		/* End : Add/Edit Submit Form Event*/		
	}

	/**
   	 * Name: delete
   	 * Use: delete grade
     * @param int $id id of record
     * @return nothing
     */
	function delete($id = null)
	{	
		$id = base64_decode($id);
		$this->loadmodel('Chemical');
		$this->loadmodel('Physical');
		$this->loadmodel('Mechanical');
		$this->Grade->id = $id;

		$data = $this->Grade->findById($id,array('recursive'=>0));
		$chemical = $this->Chemical->find('first',array('conditions'=>array('Chemical.grade_id'=>$id),'recursive'=>0));
		$mechanical = $this->Mechanical->find('first',array('conditions'=>array('Mechanical.grade_id'=>$id),'recursive'=>0));
		$physical = $this->Physical->find('first',array('conditions'=>array('Physical.grade_id'=>$id),'recursive'=>0));
		if(!empty($data)){
			$Activities = new ActivitiesController;
    		$Activities->addlog($this->loggedin_user_info['id'],'Grade','Delete',$data['Grade']);/*Add Data in Log*/
    		$Activities->addlog($this->loggedin_user_info['id'],'Chemical','Delete',$chemical['Chemical']);/*Add Data in Log*/
    		$Activities->addlog($this->loggedin_user_info['id'],'Physical','Delete',$physical['Physical']);/*Add Data in Log*/
    		$Activities->addlog($this->loggedin_user_info['id'],'Mechanical','Delete',$mechanical['Mechanical']);/*Add Data in Log*/

    	}
		if (!$id) {
			$this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Invalid Record !</div>'));
		}
		if ($this->Grade->delete($id)) {
			$this->Chemical->deleteAll(array('Chemical.grade_id' => $id), false);
			$this->Mechanical->deleteAll(array('Mechanical.grade_id' => $id), false);
			$this->Physical->deleteAll(array('Physical.grade_id' => $id), false);
			$this->Session->setFlash(__('<div class="alert alert-success alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Grade has been Deleted successfully</div>'));
		}
		$this->autoRender = false;
		return $this->redirect(array('action' => 'index'));
	}

   /**
   	 * Name: getchemical
   	 * Use: fetch property of chemical
     * @param int $id id of grade
     * @param string $type type of request, by default its null means for any requrest, if $type=ajax means ajax request.
     * @return chemical property of grade
	*/   	 
	function getchemical($id = null,$type=null){
		$this->loadmodel('Chemical');
		$this->autoRender = false;
		$data = $this->Chemical->findByGradeId($id);
		$chemdata = array();
		if(empty($id)){
			return $chemdata;
		}
		if(!empty($data['Chemical']['c_mn']) || !empty($data['Chemical']['c_mx'])){
			$chemdata[] = array('c',$data['Chemical']['c_mn'],$data['Chemical']['c_mx']);
		}
		if(!empty($data['Chemical']['mn_mn']) || !empty($data['Chemical']['mn_mx'])){
			$chemdata[] = array('mn',$data['Chemical']['mn_mn'],$data['Chemical']['mn_mx']);
		}
		if(!empty($data['Chemical']['p_mn']) || !empty($data['Chemical']['p_mx'])){
			$chemdata[] = array('p',$data['Chemical']['p_mn'],$data['Chemical']['p_mx']);
		}
		if(!empty($data['Chemical']['s_mn']) || !empty($data['Chemical']['s_mx'])){
			$chemdata[] = array('s',$data['Chemical']['s_mn'],$data['Chemical']['s_mx']);
		}
		if(!empty($data['Chemical']['si_mn']) || !empty($data['Chemical']['si_mx'])){
			$chemdata[] = array('si',$data['Chemical']['si_mn'],$data['Chemical']['si_mx']);
		}
		if(!empty($data['Chemical']['cr_mn']) || !empty($data['Chemical']['cr_mx'])){
			$chemdata[] = array('cr',$data['Chemical']['cr_mn'],$data['Chemical']['cr_mx']);
		}
		if(!empty($data['Chemical']['ni_mn']) || !empty($data['Chemical']['ni_mx'])){
			$chemdata[] = array('ni',$data['Chemical']['ni_mn'],$data['Chemical']['ni_mx']);
		}
		if(!empty($data['Chemical']['mo_mn']) || !empty($data['Chemical']['mo_mx'])){
			$chemdata[] = array('mo',$data['Chemical']['mo_mn'],$data['Chemical']['mo_mx']);
		}
		if(!empty($data['Chemical']['n_mn']) || !empty($data['Chemical']['n_mx'])){
			$chemdata[] = array('n',$data['Chemical']['n_mn'],$data['Chemical']['n_mx']);
		}
		if(!empty($data['Chemical']['fe_mn']) || !empty($data['Chemical']['fe_mx'])){
			$chemdata[] = array('fe',$data['Chemical']['fe_mn'],$data['Chemical']['fe_mx']);
		}
		if(!empty($data['Chemical']['nb_mn']) || !empty($data['Chemical']['nb_mx'])){
			$chemdata[] = array('nb',$data['Chemical']['nb_mn'],$data['Chemical']['nb_mx']);
		}
		if(!empty($data['Chemical']['ti_mn']) || !empty($data['Chemical']['ti_mx'])){
			$chemdata[] = array('ti',$data['Chemical']['ti_mn'],$data['Chemical']['ti_mx']);
		}
		if(!empty($data['Chemical']['cu_mn']) || !empty($data['Chemical']['cu_mx'])){
			$chemdata[] = array('cu',$data['Chemical']['cu_mn'],$data['Chemical']['cu_mx']);
		}
		if(!empty($data['Chemical']['v_mn']) || !empty($data['Chemical']['v_mx'])){
			$chemdata[] = array('v',$data['Chemical']['v_mn'],$data['Chemical']['v_mx']);
		}
		if(!empty($data['Chemical']['b_mn']) || !empty($data['Chemical']['b_mx'])){
			$chemdata[] = array('b',$data['Chemical']['b_mn'],$data['Chemical']['b_mx']);
		}
		if(!empty($data['Chemical']['ai_mn']) || !empty($data['Chemical']['ai_mx'])){
			$chemdata[] = array('ai',$data['Chemical']['ai_mn'],$data['Chemical']['ai_mx']);
		}
		if(!empty($data['Chemical']['ce_mn']) || !empty($data['Chemical']['ce_mx'])){
			$chemdata[] = array('ce',$data['Chemical']['ce_mn'],$data['Chemical']['ce_mx']);
		}
		if(!empty($data['Chemical']['co_mn']) || !empty($data['Chemical']['co_mx'])){
			$chemdata[] = array('co',$data['Chemical']['co_mn'],$data['Chemical']['co_mx']);
		}
		if(!empty($data['Chemical']['w_mn']) || !empty($data['Chemical']['w_mx'])){
			$chemdata[] = array('w',$data['Chemical']['w_mn'],$data['Chemical']['w_mx']);
		}
		if(!empty($data['Chemical']['la_mn']) || !empty($data['Chemical']['la_mx'])){
			$chemdata[] = array('la',$data['Chemical']['la_mn'],$data['Chemical']['la_mx']);
		}
		$data = $chemdata;
		if($type=='ajax'){ 
			return json_encode($data);
		}
		else{ 
			return $data; 
		}										
	}

   /**
   	 * Name: getmechanical
   	 * Use: fetch property of mechanical
     * @param int $id id of grade
     * @param string $type type of request, by default its null means for any requrest, if $type=ajax means ajax request.
     * @return mechanical property of grade
	*/   	 
	function getmechanical($id = null,$type=null){
		$this->loadmodel('Mechanical');
		$this->autoRender = false;
		if(empty($id)){
			return;
		}
		$data = $this->Mechanical->findByGradeId($id);
		if($type=='ajax'){ 
			return json_encode($data);
		}
		else{ 
			return $data; 
		}
	}

   /**
   	 * Name: getphysical
   	 * Use: fetch property of physical
     * @param int $id id of grade
     * @param string $type type of request, by default its null means for any requrest, if $type=ajax means ajax request.
     * @return physical property of grade
	*/   	 
	function getphysical($id = null,$type=null){
		$this->loadmodel('Physical');
		$this->autoRender = false;
		if(empty($id)){
			return;
		}
		$data = $this->Physical->findByGradeId($id);
		if($type=='ajax'){ 
			return json_encode($data);
		}
		else{ 
			return $data; 
		}		
	}
}