<?php
/*
 * @Controller name: Client Controller
 * @Version: 1.1.0
 * @Author: Shalco Industies
 * @Author URI: http://www.shalco.com
 * @Description: This controller is use to client management.
 */

App::uses('AppController', 'Controller');
App::import('Controller', 'Activities');
class ReportcategoriesController extends AppController {
	
	var $helpers  =  array('Html','Form');
	var $components = array('Auth','RequestHandler','Session','Email');
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login','logout');
		$this->set('mainTitle','Report Category Listing');
	}
	function index() {
        $this->set('pageTitle',' Category List');
        $this->set('report',$this->Reportcategory->find('all',array('fields' => array('id','reportcategory_name','reporturl'))));
        $this->set('ajaxaction','ajaxlisting');

	}
    /**
     * Name: Listing with Ajax
     * Use: View,Multiple search,Multiple delete From All Records.
    */
    function ajaxlisting(){
        $column = array('id','reportcategory_name','reporturl','Reportcategory.modified','User.first_name',);
        $order = array('Reportcategory.modified' => 'desc');  
        $res = $this->datatable_append($order,$column); 
        $count = $this->Reportcategory->find('count',array('conditions'=>array($res['con']),'order'=>$res['order_by']));
        $reports = $this->Reportcategory->find('all',array('conditions'=>array($res['con']),'limit'=>$_POST['length'], 'offset'=>$_POST['start'],'order'=>$res['order_by']));
        $this->set('report',$reports);
        $this->set('count',$count);
    }
	function countgradebasesale(){
		$this->set('pageTitle',' Gradebase Sale');
		$this->loadmodel('Gradetag');
		$this->loadmodel('Client');
		$gradetag = $this->Gradetag->find('list',array('fields'=>array('Gradetag.id','Gradetag.gradetagname')));
		$client = $this->Client->find('list',array('fields'=>array('Client.id','Client.company_name')));
		$this->set('gradetag',$gradetag);
		$this->set('client',$client);

	}
	function gradebasesale(){
		$this->autoRender = false;
		$this->loadmodel('Invoice');
		$this->loadmodel('Grade');
		$this->loadmodel('Gradetag');
		$this->loadmodel('Order');
		$this->loadmodel('Orderitem');
		$this->loadmodel('Invoiceitem');
		//pr($_POST);
		$grade_tag_id = implode(",",$_POST['data']['Gradebasesale']['gradetag_id']);
		$year = $_POST['data']['Gradebasesale']['year'];
        $query = '';
       
        if(!empty($_POST['data']['Gradebasesale']['client'])){
            $im_client = implode(",",$_POST['data']['Gradebasesale']['client']);
            $query .= ' AND (so1.bill_to IN ('.$im_client.'))';
        }
         if(!empty($_POST['data']['Gradebasesale']['year'])){
            $im_year = implode(",",$_POST['data']['Gradebasesale']['year']);
            if($_POST['data']['Gradebasesale']['type'][0] == 1 && !isset($_POST['data']['Gradebasesale']['type'][1]))
            {            
                $query .= ' AND (si1.fy IN ('.$im_year.'))';
            }
            if($_POST['data']['Gradebasesale']['type'][0] == 2){            
                $query .= ' AND (sc1.fy IN ('.$im_year.'))';
            }
            
        }
       // echo $query;exit;
		$client = $_POST['data']['Gradebasesale']['client'];

		
 
    
        if($_POST['data']['Gradebasesale']['save'] == 1){
        	$reports = $this->request->data['Gradebasesale'];
        	$reports['reportname'] = $this->request->data['Gradebasesale']['reportname'];
        	$reports['created'] = date('Y-m-d H:i:s');
   			$reports['createdby'] = $this->Auth->user('id');   			
   			$this->Report->save($reports);

        }
        $html = '<table class="table table-bordered table-striped table-condensed flip-content" id="report_id">
                <thead class="flip-content portlet box green">
                      <tr>
		              	<th width="25%">#</th>
		                <th width="25%">Gradetag Name</th>
		                <th width="25%">Total Qty MTR</th>
		                <th width="25%">Action</th>
		            </tr>
                  </thead>';
         $i = 1;
        foreach($sql as $grade_row => $val){
            
        	$html .= '<tr>';
        	$html .= '<td>'.$i.'</td>';
        	$html .= '<td>'.$val[0]['Gradetag'].'</td>';
        	$html .= '<td>'.$val[0]['qty_mtr'].'</td>';
        	$html .= '<td><a href="" class="">View</td>';
        	$html .= '</tr>';
        	$i++;
        }
         $html .='</tbody>
            </table>';
            echo $html;die;
	}

}