<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'logout')
        )
    );
    var $helpers = array('Session','Html');
    var $uses = array('User','Userpermission','Grouppermission','Usertype','Module');
    public $loggedin_user_info;  /* global scope */

    /*disable previous page using back button after logout*/
    function beforeRender() {     
        $this->response->disableCache();
	    }

	function beforeFilter() {
		parent::beforeFilter();
		$this->loggedin_user_info = $this->Auth->user(); // for access user data in any controller
		$this->Auth->authenticate = array('Form');
        $this->Auth->authenticate = array('Form' => array('userModel' => 'User'));
        $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email','password' => 'password'),'scope' => array('status' => '1')));
        $this->Auth->loginAction=array('controller' => 'users','action' => 'login');
        $this->Auth->loginRedirect=array('controller' => 'users','action' => 'index');
        $this->Auth->logoutRedirect=array('controller' => 'users','action' => 'login');
        $this->Auth->autoRedirect = true;
        /* Load Setting */
        /*Setting table data is frequently using so Set Setting table data in session */
        //$this->Session->destroy();
		$this->checkUser();
		/*$this->getNotification();*/

		/*---- Start code to check permission ----*/
		$userdata = $this->Auth->user(); /*Fetch login user's information*/
		/*Fetch user's permission*/
		$userspermission = $this->Userpermission->findByUserId($userdata['id']);
		
		$access_modules = '';
		if($userspermission){
			$access_modules = $userspermission['Userpermission']['access_modules'];
		}
		/*Fetch user's group's permission*/
		if(empty($access_modules)) {
			$groupdata = $this->getUserGroupId($userdata['usertype_id']);
			if($groupdata) {
				$grouppermissiondata = $this->Grouppermission->findByUsertypeId($groupdata['Usertype']['id']);
				if($grouppermissiondata){	
					$access_modules = $grouppermissiondata['Grouppermission']['access_modules'];
				}
			}
		}
		$accesscontrollers_actions = $this->Session->read('access_controller');

		/*get module's which is allowed to access except admin*/
		if($this->Session->read('login_usertype_id') != 1){
			/*Fetch module's for user which is permissible by admin*/
			if($access_modules) {
				$accessData = $this->Module->find('all',array('conditions'=>array('id IN ('.$access_modules.')')));
			}
			else{ /*Fetch module for user while admin cant give any permission*/
				$accessData = $this->Module->find('all');
			}
			/* Dashboard controlle for Admin */
			if($this->Session->read('login_usertype_id') == 1){
				$access_controller = array('dashboards');
			}
			/* Access Controllers of Access Modules */
			//$accesscontrollers_actions = array('dashboards/index');
			foreach($accessData as $data){
				$accesscontrollers_actions[] = $data['Module']['controller'].'/'.$data['Module']['action'];
				$access_controller[] = $data['Module']['controller'];
			}
			//pr($accesscontrollers_actions);exit;			
			/* Update Session variable of accesscontrollers */

			$this->Session->write('controller_access',$access_controller);/*Set all accessible controller from database*/

			$this->Session->write('accesscontrollers_actions',$accesscontrollers_actions); /*Set all accessible controller with action from bootstrap and database*/
			//pr($accesscontrollers_actions);exit;

			if($this->params->controller == 'App') { $this->Auth->allow('*'); }
			elseif(in_array(strtolower($this->params->controller).'/'.$this->params->action, $this->Session->read('accesscontrollers_actions'))){			
				$this->Auth->allow('*');
			} 
			else {
				$this->unauthorize();
			}
		}
		else{

			$this->Auth->allow('*');
		}
		/*---- End code to check permission ----*/
	
        if($this->Session->check('setting_data') != true){
            $this->loadModel('Setting'); 
            $settings = $this->Setting->find('first');
            $this->set('setting',$settings);/*This varibale accessible in all view file*/

            $this->Session->write('setting_data', $settings ); 
            $setting= $this->Session->read('setting_data');

        }else{
            $setting= $this->Session->read('setting_data');
        }

		$this->set('ajaxaction','');
	}

	function datatable_append($order,$column){
		/*Set order for ordering*/
		if(isset($_POST['order']))
        {
            $order_by = $column[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'];
        }
        else if(isset($order))
        {
            $order_by = key($order).' '.$order[key($order)];
        }	
		/*else if(isset($this->order))
        {
            $order = $this->order;
            $order_by = key($order).' '.$order[key($order)];
        }	*/
        /*Set Condition for filter*/
		$con = '';        
        if(isset($_POST['action']) && $_POST['action'] == 'filter'){
			$i = 0;
			
			foreach($column as $item){			
		
			    if(isset($_POST[$item]) && !strstr($item,'modified')){ 		/* Check if column value is set in post data or not */			    	
			    	if($_POST[$item]!=''){
				    	if($i == 0){
					       $con .= " `";
					    } else {
					       $con .= " AND `";
					    }	
			    		$date_related_fields = array('modified_from','modified_to');
			    		/*if(in_array($item, $date_related_fields)){
			    		    if($item == 'modified_from'){ $con .= "modified` >= '".date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST[$item])))."'";  }
			                if($item == 'modified_to') { $con .= "modified` <= '".date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST[$item])))."'"; }
			    		}
			    		else if($item == 'status'){
			    			$con .= $item."` = '".$_POST[$item]."'";
			    		}
			    		else{	*/
					    	$con .= $item."` LIKE '%".$_POST[$item]."%'";
					   //	} 
					    $i++;		    		
			    	}
			    } 

			    /*Filter Logic for date range*/
				if(strpos($item,'modified') !== false){	
					$x = explode('.',$item);
					if($x['1'] == 'modified'){
		    		    if($_POST['modified_from']!=''){ 
	    		    		if(strlen($con)== 0){
						       $con .= " `";
						    } else {
						       $con .= " AND `";
						    }
		    		    	$con .= $x['0'].".modified` >= '".date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['modified_from'])))."'";  
		    		    }
		                if($_POST['modified_to']!='') { 
	    		    		if(strlen($con)== 0){
						       $con .= " `";
						    } else {
						       $con .= " AND `";
						    }		                
		                	$con .= $x['0'].".modified` <= '".date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['modified_to'])))."'"; 
		                }
		                $i++;	
		    		}
			    }

			    /*Filter Logic for status field*/
				if(strpos($item,'status') !== false){	
					$x = explode('.',$item);
					if($x['1'] == 'status' && $_POST['status']!=''){		    		    
    		    		if(strlen($con)== 0){
					       $con .= " `";
					    } else {
					       $con .= " AND `";
					    }
	    		    	$con .= $item."` = '".$_POST['status']."'";
		                $i++;	
		    		}
			    }			    
			}
		}	
		/*Change Muliple record's Status*/
		if(isset($_POST['customActionType']) && $_POST['customActionType'] == 'group_action'){			
			foreach ($_POST['id'] as $id) {				
				$this->{$this->modelClass}->id = $id;
				$statusval = $_POST['customActionName']; /* get selected option value from select tag */
				$this->{$this->modelClass}->saveField('status', $statusval);
				$this->{$this->modelClass}->saveField('modifiedby', $this->Auth->user('id'));
			}
		}
		/*Multiple Delete*/
		if(isset($_POST['customActionType']) && $_POST['customActionType'] == 'group_delete'){ 
			foreach ($_POST['id'] as $id) {				
				$this->{$this->modelClass}->delete($id);
			}
		}
		return array('order_by'=>$order_by,'con'=>$con);
	}

	/**
	* Name : rand_str
   	* Use : Generate Random Character String.
    * @return string $string random number string
   	*/
	function rand_str($length = 4, $patterntype){
		if($patterntype == 1){
			$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		}
		else{
			$chars = '1234567890';	
		} 
		
	    $chars_length = (strlen($chars) - 1); // Length of character
	    $string = $chars{rand(0, $chars_length)}; // Starts Our String
		for ($i = 1; $i < $length; $i = strlen($string)){
	        $r = $chars{rand(0, $chars_length)};
	        if ($r != $string{$i - 1}) $string .=  $r;
	    }
	   	return $string;
	}

	/**
	* Name : date_ymd_to_dmy
   	* Use : change date from ymd to dmy
    * @param int $id id of record
    * @return date in dmy format
   	*/
   	function date_ymd_to_dmy($date = null) {
   		if($date != 0000-00-00){
			$Convertdate = date("d/m/Y",$date);
			return $Convertdate;
		}else{
			return '';
		}
	}

	/**
	* Name : date_ymd_to_dmy_licence
	* Use : Change date from ymd to dmy 
    * @param int $id id of record
    * @return date in dmy format
	*/
   	function date_ymd_to_dmy_licence($date = null) {
   		if($date != 0000-00-00){
			$Convertdate = date("d/m/Y",strtotime($date));
			return $Convertdate;
		}else{
			return '';
		}
	}


	/**
	* Name : getFinancialyear
   	* Use : get financial year
    * @param int $id id of record
    * @return date in dmy format
   	*/
   	
   	function getFinancialyear() {
		if(date('m') < '04'){
			 $finan_date = date('y',strtotime('-1 year')).'-'.date('y');
			 
		}else{
			$finan_date = date('y').'-'.date('y',strtotime('+1 year'));	
		} 
		return $finan_date;
	}

	function calculateFinancialYearForDate($inputDate=null){  
		/*If user fill date than override default date with user entered date*/
		if(isset($_POST['date'])){ 
			$inputDate=date('Y-m-d', strtotime(str_replace('/', '-', $_POST['date'])));
			$this->autoRender=false;
		}


		$month = date('m',strtotime($inputDate));
		if($month < '04'){
			 $fy = date('y',strtotime('-1 year',strtotime($inputDate))).'-'.date('y',strtotime($inputDate));			 
		}else{
			$fy = date('y',strtotime($inputDate)).'-'.date('y',strtotime('+1 year', strtotime($inputDate)));	
		}
	    return $fy; 
	}


	/**
	* Name : date_ymdhis_to_dmyhis
   	* Use : change date from ymdhis to dmyhis
    * @param int $id id of record
    * @return date in dmyhis format
   	*/
	function date_ymdhis_to_dmyhis($date = null) {
		if($date != '0000-00-00 00:00:00'){
			$Convertdate = date("d/m/Y H:i:s",$date);
			return $Convertdate;
		}
		return;
	}

    /* Find IP address of user's system */
    function get_ip() {
        /*Just get the headers if we can or else use the SERVER global*/
        if ( function_exists( 'apache_request_headers' ) ) {
          $headers = apache_request_headers();
        } else {
         $headers = $_SERVER;
        }
        /*Get the forwarded IP if it exists*/
        if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
          $the_ip = $headers['X-Forwarded-For'];
        } elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
        ) {
          $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
        } else {
          $the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
        }
       
        return $the_ip;               
    }

	/*
	* Name : convertNumber
	* Use : amount converts in words
	*
	*/

 function numtowords($number,$sign){ 
//$number = 190908100.25;
	   $no = round($number);
	   $point = round($number - $no, 2) * 100;
	   $hundred = null;
	   $digits_1 = strlen($no);
	   $i = 0;
	   $str = array();
	   $words = array('0' => '', '1' => 'one', '2' => 'two',
	    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
	    '7' => 'seven', '8' => 'eight', '9' => 'nine',
	    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
	    '13' => 'thirteen', '14' => 'fourteen',
	    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
	    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty','21' => 'twenty-one','22' => 'twenty-two','23' => 'twenty-three','24' => 'twenty-four','25' => 'twenty-five','26' => 'twenty-six','27' => 'twenty-seven','28' => 'twenty-eight','29' => 'twenty-nine','30' => 'thirty','31' => 'thirty-one','32' => 'thirty-two','33' => 'thirty-three','34' => 'thirty-four','35' => 'thirty-five','36' => 'thirty-six', '37' => 'thirty-seven', '38' => 'thirty-eight','39' => 'thirty-nine','40'=>'fourty','31' => 'thirty-one','32' => 'thirty-two','33' => 'thirty-three','34' => 'thirty-four','35' => 'thirty-five','36' => 'thirty-six', '37' => 'thirty-seven', '38' => 'thirty-eight','39' => 'thirty-nine','40'=>'fourty',
	    '41' => 'fourty-one','42' => 'fourty-two','43' => 'fourty-three','44' => 'fourty-four','45' => 'fourty-five','46' => 'fourty-six', '47' => 'fourty-seven', '48' => 'fourty-eight','49' => 'fourty-nine','50'=>'fifty',
	    '51' => 'fifty-one','52' => 'fifty-two','53' => 'fifty-three','54' => 'fifty-four','55' => 'fifty-five','56' => 'fifty-six', '57' => 'fifty-seven', '58' => 'fifty-eight','59' => 'fifty-nine','60'=>'sixty',
	    '61' => 'sixty-one','62' => 'sixty-two','63' => 'sixty-three','64' => 'sixty-four','65' => 'sixty-five','66' => 'sixty-six', '67' => 'sixty-seven', '68' => 'sixty-eight','69' => 'sixty-nine','70'=>'seventy',
	    '71' => 'seventy-one','72' => 'seventy-two','73' => 'seventy-three','74' => 'seventy-four','75' => 'seventy-five','36' => 'seventy-six', '77' => 'seventy-seven', '78' => 'seventy-eight','79' => 'seventy-nine','80'=>'eighty',
	    '81' => 'eighty-one','82' => 'eighty-two','83' => 'eighty-three','84' => 'eighty-four','85' => 'eighty-five','86' => 'eighty-six', '87' => 'eighty-seven', '88' => 'eighty-eight','89' => 'eighty-nine','90'=>'ninety',
	    '91' => 'ninety-one','92' => 'ninety-two','93' => 'ninety-three','94' => 'ninety-four','95' => 'ninety-five','96' => 'ninety-six', '97' => 'ninety-seven', '98' => 'ninety-eight','99' => 'ninety-nine'
	 	);
	   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
	   while ($i < $digits_1) {
	     $divider = ($i == 2) ? 10 : 100;
	     $number = floor($no % $divider);
	     $no = floor($no / $divider);
	     $i += ($divider == 10) ? 1 : 2;
	     if ($number) {
	        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
	        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
	        $str [] = ($number < 21) ? $words[$number] .
	            " " . $digits[$counter] . $plural . " " . $hundred
	            :
	            $words[floor($number / 10) * 10]
	            . " " . $words[$number % 10] . " "
	            . $digits[$counter] . $plural . " " . $hundred;
	     } else $str[] = null;
	  } 
	  $str = array_reverse($str);
	  $result = implode('', $str);
	  if($point < 0){
	  	$minus = 100 - abs($point);
	  	$points = ($minus) ?
	    "" . $words[$minus / 10] . " " . 
	          $words[$minus = $minus % 10] : '';
	  }else{
	  $points = ($point) ?
	    "" . $words[$point / 10] . " " . 
	          $words[$point = $point % 10] : '';
	    }
	    $result = ucwords($result);
	   	if(!empty($result)){
	 		if(!empty($points)){
	 			if($sign == 'Rupees'){
	 				$paisa = ' Paisa';
	 			}else{
	 				$paisa = ' Cent';
	 			}

	    	 return $sign." ".$result ." and ".$paisa." ".$points;        
			}else {
			return $sign ." ". $result;
			}
		}else {
			$result = '';
		} 

		return $result;
	}

    /**
   	 * Name: getHeatnumber
   	 * Use: fetch heat number of invoiceitem
     * @param int $id id of invoice
     * @param int $itemid id of invoice item
     * @return heat number of invoice
	*/   
	function getHeatnumber($id = null,$itemid = null) {
		$this->loadmodel('Certificate');
		/*echo "SELECT
  b.id,
  b.heatno
FROM
  sr_certificateitems as b where b.refitemid = 40
  AND FIND_IN_SET(b.certificateid,(
    SELECT a.id from sr_certificates as a where a.reftype = 0 and a.refid =11)
  )"; */		
		$res = $this->Certificate->query("SELECT b.id, b.heatno,b.refitemid FROM sr_certificateitems as b where b.refitemid = '".$itemid."' AND b.certificateid IN( SELECT a.id from sr_certificates as a where a.reftype = 0 and a.refid =".$id.")");
		if(!empty($res)){ 
			return $res[0]['b']['heatno'];
		}
		else{
			return '-----';
		}
	}


	/*
   	 * Name: arraytostring
   	 * Use: In this function create user array for select option.
   	 * Parameters: user data array
	 */
	function arraytostring( $val = null ) {
		$data = '';
		for ($i=0; $i < sizeof($val); $i++) {
			$data = $data.$val[$i].",";
		}
		return rtrim($data, ",");
	}
	function getEmployeename($id = null) {
		$this->loadmodel('User');
		$fields = array('User.id','User.first_name','User.last_name','User.middle_name','User.preferred_name','User.email','User.supervisor','User.usertype_id','User.status');
		$userName = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'fields'=>$fields));
		//pr($userName);exit;
		return $userName;
	}

	/*
   	 * Name: getClientInfo
   	 * Use: fetch client details
   	 * Parameters: client id
	 */
	function getClientInfo($id = null) {
		$this->loadmodel('Client');
		$clientinfo = $this->Client->find('first',array('conditions'=>array('Client.id'=>$id)));
		return $clientinfo;
	}
	/*
   	 * Name: getRoleName
   	 * Use: This function use to get Group name by user id or all group.
   	 * Parameters: userid
	 */
	function getRoleName($id = null) {
		$this->loadmodel('Usertype');
		if(!empty($id)) {
			$datas = $this->Usertype->find("first",array('conditions' => array('Usertype.id'=>$id)));
					}
		return $datas['Usertype']['usertype_name'];
	}


	/*
   	 * Name: getEmailtemplate
   	 * Use: This function use to get Email template.
   	 * Parameters: Template array (id)
	 */
	function getEmailtemplate($shortcode = '') {
		$this->loadmodel('Emailtemplate');
		if(!empty($shortcode)) {
			$Emailtemplate = $this->Emailtemplate->find('first',array('conditions'=> array('Emailtemplate.shortcode'=>$shortcode)));
		}
		else {
			$Emailtemplate = $this->Emailtemplate->find('all');
		}
		return $Emailtemplate;
	}
	function _group_by($array, $key) {
	    $return = array();
	    foreach($array as $val) {
	        $return[$val[$key]][] = $val;
	    }
	    return $return;
	}
	/*
   	 * Name: getUserGroupId
   	 * Use: This function use to get Group name by user id.
   	 * Parameters: userid
	 */
	function getUserGroupId($id = null) {
		if(!empty($id)) {
			return $data = $this->Usertype->find("first",array('conditions' => array('Usertype.id'=>$id)));
		}
	}

	function getLoginUserDetails($val = "") {
		return $this->Auth->User($val);
	}

	function checkUser() {
		$loginId = $this->getLoginUserDetails('usertype_id');
		/* Call setValuinVacatoinMeta function */
		$userId = $this->getLoginUserDetails('id');
	//	$this->setValuinVacatoinMeta($userId);
		$link = $this->request->params;
		$links = $link['controller']."/".$link['action'];
		$admin = array('users/index','users/createletter',);
		$hr = array('users/index',);
		$supervisor = array('users/index',);
		$employee = array('users/index',);
	}


	/* Unauthorize Page */
	public function unauthorize() {
		$this->set('pageTitle','Access Denied');
		
		if($this->Auth->user()) {
			$this->redirect(array('controller'=>'errors','action'=>'accessdenied'));
			$this->set('mainTitle','Unauthorize');
		}
	}

	/**
	* Name : get_delivery_type
   	* Use : fetch delivery type name like CNF, CIF, fright,Ex-work
    * @param int $deliverytype id of deliverytype
    * @return fetch delivery type name like CNF, CIF, fright,Ex-work
   	*/
   	function get_delivery_type($deliverytype_id = null) {
		$deliverytype_name = '';
		if($deliverytype_id == 1){ $deliverytype_name = 'CIF ';}
		if($deliverytype_id == 2){ $deliverytype_name = 'FOB ';}
		if($deliverytype_id == 3){ $deliverytype_name = 'Ex-Work ';}
		if($deliverytype_id == 4){ $deliverytype_name = 'CNF ';}
		return $deliverytype_name;
	}

	/**
	* Name : check_nextstage
   	* Use : check record entered in next stage means record used in other modules
    * @return true or false
   	*/
   	function check_nextstage() {
		$dependantmodel = $this->request->params['dependantmodel'];/*dependant model name*/
		$condition = $this->request->params['conditional_parameter'];/*condition*/
        $this->loadModel($dependantmodel); 
        $count = $this->$dependantmodel->find('count',array('conditions'=>$condition));
        if($count){
        	return true;
        }
        return false;
	}	
	function getURI(){
		$image = ROOT_PATH.'uploads/signature/doc_image.png';
        $data = '<img src="'.$image.'">';
        return $data;
    }
    /**
   	 * Name: getStandard
   	 * Use: fetch name of standard
     * @param int $id id of grade
     * @return physical property of grade
	*/   	 
	function getStandard($id = null){
		$this->loadmodel('Standard');
		$standrd_name = $this->Standard->find('all',array('conditions'=>array('Standard.id' => $id),'fields'=>array('Standard.standard_name'),'recursive'=>-1));
		return $standrd_name[0]['Standard']['standard_name'];
	}

    /**
   	 * Name: getLicenceOption
   	 * Use: fetch licence of related grade tag and hs code
     * @param int $hscodeid id of hscode
     * @param int $gradetagid id of gradetag
     * @param int $selectedlicenceid id of licence(used for Edit invoice section)
     * @return available licence option list
	*/   	 
	function getLicenceOption($hscodeid = null,$gradetagid = null,$selectedlicenceid = null){
		$this->loadmodel('Licence');
		/*$this->loadmodel('Licenceitem');
		$this->Licence->unbindModel(array('belongsTo' =>array('User','Hscode')));
		$this->Licence->hasMany['Licenceitem']['conditions'] = array('Licenceitem.gradetagid' => $gradetagid);
		$licencelist = $this->Licence->find('all',array('conditions'=>array('hscode_id' => $hscodeid,
			'OR' => array('expiry_date >=' => date('Y-m-d'),'ex_expirydate >=' => date('Y-m-d'))
			)));*/

		$licencelist = $this->Licence->query("select Licence.id,Licence.advancelicenceno,Licence.is_extended,Licence.expiry_date,Licence.ex_expirydate,
			Licenceitem.id,Licenceitem.gradetagid,Licenceitem.ex_qty,
			Licenceexportitem.id,Licenceexportitem.licenceitemid,sum(ifnull(Licenceexportitem.exportqty,0)) as exportqty 
			from sr_licenceitems as Licenceitem 
			JOIN 
			sr_licences as Licence 
			ON Licenceitem.licenceid = Licence.id 
			LEFT OUTER JOIN sr_licenceexportitems as Licenceexportitem 
			ON Licenceitem.id = Licenceexportitem.licenceitemid 
			where Licence.hscode_id = '".$hscodeid."' and Licenceitem.gradetagid = '".$gradetagid."' and (Licence.expiry_date >=  '".date('Y-m-d')."' OR Licence.ex_expirydate >=  '".date('Y-m-d')."') 
			group by Licenceitem.id,Licenceexportitem.licenceitemid,Licenceitem.licenceid 
			ORDER BY Licenceitem.id ASC"); 
		$licenceoptiondropdown = '';
        if(!empty($licencelist)){
	        $licenceoptiondropdown .= '<option value="" itemid="">Select Licence</option>';
	        foreach ($licencelist as $value) {
	            if(!empty($value['Licenceitem'])){
	                if($value['Licence']['is_extended'] == 0){
	                   $expirydate = date('d/m/Y',strtotime($value['Licence']['expiry_date']));
	                }else{
	                	$expirydate = date('d/m/Y',strtotime($value['Licence']['ex_expirydate']));
	              	}
	              	$bal_qty = $value['Licenceitem']['ex_qty'] - $value[0]['exportqty'];
	              	if($selectedlicenceid == $value['Licence']['id']){	              		
	              		$licenceoptiondropdown .= "<option value=".$value['Licence']['id']." itemid=".$value['Licenceitem']['id']." selected='selected'>".$value['Licence']['advancelicenceno']." (EX ".$expirydate.") (Bal Qty ".decimalQty($bal_qty).")</option>";
	              	}
	              	else{
	              		$licenceoptiondropdown .= "<option value=".$value['Licence']['id']." itemid=".$value['Licenceitem']['id'].">".$value['Licence']['advancelicenceno']." (EX ".$expirydate.") (Bal Qty ".decimalQty($bal_qty).")</option>";
	              	}
	              	                                
	              	/*foreach ($value['Licenceitem'] as $key2 => $value2){
	                	$licenceoptiondropdown .= "<option value=".$value['Licence']['id']." itemid=".$value2['id'].">".$value['Licence']['advancelicenceno']." (EX ".date('d/m/Y',strtotime($value['Licence']['expiry_date'])).")</option>";      
	              	}*/
	            }
	        } 
        }
		//pr($licencelist); exit;
		return $licenceoptiondropdown;
	}

    /**
   	 * Name: addDayswithdate
   	 * Use: add number of days in particular date
     * @param date $date date which you want 
     * @param int $days number of days
     * @return new date with include number of days.
	*/	
	function addDayswithdate($date,$days){
		if($date = '0000-00-00'){
		 return '';
		}
	    $date = strtotime("+".$days." days", strtotime($date));
	    return  date("Y-m-d", $date);
	}
}