<?php 
  if(!isset($notauthorized)) {
?>   
        <?php echo $this->Form->create('User',array('method'=>'POST','class'=>'shalcoform')); ?>
        <h3 class="font-green">Reset Password</h3>
            <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9">New Password</label>
              <?php echo $this->Form->input('password',array('type'=>'password','label'=>false,'maxlength'=>'50', 'placeholder'=>"Please enter your new password","class" => "form-control",'required'));?>
            </div>
            <div class="form-group">
              <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
              <?php echo $this->Form->input('con_password',array('type'=>'password','label'=>false,'maxlength'=>'50', 'placeholder'=>"Please enter your confirm password","class" => "form-control",'required'));?>
            </div>            
            <div class="form-actions">
                 <a href="<?php echo WEBSITE_PATH;?>users/login" class="btn btn-default">Cancel</a>
                <?php echo $this->Form->button('Change',array('type'=>'submit','class'=>'btn btn-success')); ?>
            </div>
      <?php echo $this->Form->end();?>
<?php }
else {
  echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> '.$noti['Notificationtemplate']['message'].'</div>';
}?>