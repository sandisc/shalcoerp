<!-- BEGIN LOGIN FORM -->
            	<?php echo $this->Form->create('User',array('class'=>'shalcoform'));?>
                  <h3 class="font-green">Forget Password ?</h3>
                  <p> Enter your e-mail address below to reset your password. </p>
                  <?php echo $this->Session->flash();?>
              	  	<div class="form-group">                    
                	    <label class="control-label visible-ie8 visible-ie9">Email</label>
                           <?php echo $this->Form->input('email', array(
                            'type'=>'text',
                            'autocomplete'=>'on',
                            'label'=>false,
                            'placeholder'=>'Email',
                            'required' => true,
                            'class'=>'form-control placeholder-no-fix'
                         )); ?>
                	</div>
                <div class="form-actions">
                  <?php echo $this->Html->link('Back',array('action' => 'login','controller' =>'users'),array('class' => 'btn btn-default')); ?>
                    <?php echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn green uppercase')); ?>
                </div>
            <?php echo $this->Form->end(); ?>
            <!-- END LOGIN FORM -->