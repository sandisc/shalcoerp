

<h3 class="page-title"> User Management </h3>
<div class="row">
   <div class="col-md-12">
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase">User Lists</span>
            </div>
           
         </div>
         <div class="portlet-body">
            <div class="table-container" style="">
               <div id="datatable_ajax_wrapper" class="dataTables_wrapper dataTables_extended_wrapper no-footer">
                  <div class="custom-alerts alert alert-success fade in" id="prefix_577988153126"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><i class="fa-lg fa fa-check"></i>  Group action successfully has been completed. Well done!</div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended" id="datatable_ajax_paginate">
                           <div class="pagination-panel"> Page <a class="btn btn-sm default prev disabled" href="#"><i class="fa fa-angle-left"></i></a><input type="text" style="text-align:center; margin: 0 5px;" maxlenght="5" class="pagination-panel-input form-control input-sm input-inline input-mini"><a class="btn btn-sm default next" href="#"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">18</span></div>
                        </div>
                        <div class="dataTables_length" id="datatable_ajax_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select name="datatable_ajax_length" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
                                 <option value="10">10</option>
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info" id="datatable_ajax_info" role="status" aria-live="polite"><span class="seperator">|</span>Found total 178 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="table-group-actions pull-right">
                           <span></span>
                           <select class="table-group-action-input form-control input-inline input-small input-sm">
                              <option value="">Select...</option>
                              <option value="Cancel">Cancel</option>
                              <option value="Cancel">Hold</option>
                              <option value="Cancel">On Hold</option>
                              <option value="Close">Close</option>
                           </select>
                           <button class="btn btn-sm green table-group-action-submit">
                           <i class="fa fa-check"></i> Submit</button>
                        </div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
                        <thead>
                           <tr class="heading" role="row">
                              <th width="2%" class="sorting_disabled" rowspan="1" colspan="1" aria-label="
                                 ">
                                 <div class="checker"><span><input type="checkbox" class="group-checkable"></span></div>
                              </th>
                              <th width="5%" class="sorting_asc" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" Record&amp;nbsp;# : activate to sort column descending"> Name </th>
                              <th width="200" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-label=" Email : activate to sort column ascending"> Email </th>
                              <th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-label=" Mobile : activate to sort column ascending"> Mobile</th>
                              <th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-label=" User Type : activate to sort column ascending"> User Type </th>
                              <th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending"> Status </th>
                              <th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending"> Actions </th>
                           </tr>
                           <tr class="filter" role="row">
                              <td rowspan="1" colspan="1"> </td>
                              <td rowspan="1" colspan="1">
                                 <input type="text" name="order_id" class="form-control form-filter input-sm"> 
                              </td>
                              <td rowspan="1" colspan="1">
                                 <input type="text" name="order_customer_name" class="form-control form-filter input-sm"> 
                              </td>
                              <td rowspan="1" colspan="1">
                                 <input type="text" name="order_ship_to" class="form-control form-filter input-sm"> 
                              </td>
                              <td rowspan="1" colspan="1">
                                 <div class="margin-bottom-5">
                                    <input type="text" placeholder="From" name="order_price_from" class="form-control form-filter input-sm"> 
                                 </div>
                                 <input type="text" placeholder="To" name="order_price_to" class="form-control form-filter input-sm"> 
                              </td>
                              <td rowspan="1" colspan="1">
                                 <select class="form-control form-filter input-sm" name="order_status">
                                    <option value="">Select...</option>
                                    <option value="pending">Pending</option>
                                    <option value="closed">Closed</option>
                                    <option value="hold">On Hold</option>
                                    <option value="fraud">Fraud</option>
                                 </select>
                              </td>
                              <td rowspan="1" colspan="1">
                                 <div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                    <i class="fa fa-search"></i> Search</button>
                                 </div>
                                 <button class="btn btn-sm red btn-outline filter-cancel">
                                 <i class="fa fa-times"></i> Reset</button>
                              </td>
                           </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($users as $users) {
                        //for ($i=0; $i < sizeof($users); $i++) {?>
                           <tr class="odd" role="row">
                              <td><div class="checker"><span><input type="checkbox" name="id[]" value="<?php echo $users['User']['id']?>"></span></div></td>
                              <td class="sorting_1"><?php echo $users['User']['first_name'].' '.$users['User']['last_name']?></td>
                              
                              <td><?php echo $users['User']['email']?></td>
                              <td><?php echo $users['User']['mobile']?></td>
                              <td><?php echo $users['Usertype']['usertype_name']?></td>
                              <td><span class="label label-sm label-success">Pending</span></td>
                              <td><?php echo $this->Html->link('Edit User', array('controller' => 'users','action' => 'add',base64_encode($users['User']['id'])),array('class'=>'btn btn-sm btn-outline grey-salsa')); 
                               echo $this->Html->link('Delete User',array('controller' => 'users','action' => 'delete'),array('confirm'=>'Are you sure you want to delete?',base64_encode($users['User']['id'])),array('class'=>'btn btn-sm btn-outline grey-salsa')); 
                               ?></td>
                           </tr>
                        <?php } ?>   
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-12">
                        <div class="dataTables_paginate paging_bootstrap_extended">
                           <div class="pagination-panel"> Page <a class="btn btn-sm default prev disabled" href="#"><i class="fa fa-angle-left"></i></a><input type="text" style="text-align:center; margin: 0 5px;" maxlenght="5" class="pagination-panel-input form-control input-sm input-inline input-mini"><a class="btn btn-sm default next" href="#"><i class="fa fa-angle-right"></i></a> of <span class="pagination-panel-total">18</span></div>
                        </div>
                        <div class="dataTables_length">
                           <label>
                              <span class="seperator">|</span>View 
                              <select name="datatable_ajax_length" aria-controls="datatable_ajax" class="form-control input-xs input-sm input-inline">
                                 <option value="10">10</option>
                                 <option value="20">20</option>
                                 <option value="50">50</option>
                                 <option value="100">100</option>
                                 <option value="150">150</option>
                                 <option value="-1">All</option>
                              </select>
                              records
                           </label>
                        </div>
                        <div class="dataTables_info"><span class="seperator">|</span>Found total 178 records</div>
                     </div>
                     <div class="col-md-4 col-sm-12"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End: life time stats -->
   </div>
</div>