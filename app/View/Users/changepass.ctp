<h3><?php echo $pageTitle;?></h3>
 <?php echo $this->Session->flash();?>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Change Password </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
            
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                   <?php echo $this->Form->create('User',array('method'=>'POST','class'=>'form-horizontal form-row-seperated')); ?>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Current Password<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-3">
                                   <?php echo $this->Form->input('old_password',array('type'=>'password','label'=>false,'maxlength'=>'16', 'placeholder'=>"Please enter current password","class" => "form-control",'required','autocomplete'=>'off'));?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">New Password<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-3">
                                     <?php echo $this->Form->input('password',array('type'=>'password','label'=>false,'maxlength'=>'16', 'placeholder'=>"Please enter new password","class" => "form-control",'required'));?>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-md-3">Confirm Password<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-3">
                                   <?php echo $this->Form->input('conpassword',array('type'=>'password','label'=>false,'maxlength'=>'16', 'placeholder'=>"Please enter confirm password","class" => "form-control",'required'));?>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">                        
                                     <button class="btn green" type="submit">Change Password</button>
                                             <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'users'),array('class' => 'btn default')); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>