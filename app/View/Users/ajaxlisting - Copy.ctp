<?php
  /* 
   * Paging
   */


  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($users as $users) {
      if($users['User']['status'] == 1){
        $status = $status_list[0];
      }
      else{
        $status = $status_list[1];
      }
      
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$users['User']['id'].'">',
      $users['User']['first_name'].' '.$users['User']['last_name'],
      $users['User']['email'],
      $users['User']['mobile'],
      $users['Usertype']['usertype_name'],
      $users['User']['modified'],
      '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
      '<a href="'.WEBSITE_PATH.'users/add/'.base64_encode($users['User']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'users/delete/'.base64_encode($users['User']['id']).'" class="btn btn-danger" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>',);
      
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
 // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>
<script type="text/javascript">
 $("#sel_ct").change(function () {
         var group = $(this).val();
         alert(group);

});
</script>