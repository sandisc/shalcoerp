            <!-- BEGIN LOGIN FORM -->
            <?php echo $this->Form->create('User',array('class'=>'shalcoform'));?>
                <h3 class="form-title font-green">Sign In</h3>
                <?php echo $this->Session->flash();?>
                <div class="form-group">                    
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                           <?php echo $this->Form->input('email', array(
                            'type'=>'text',
                            'autocomplete'=>'on',
                            'label'=>false,
                            'placeholder'=>'Email',
                            'required' => true,
                            'class'=>'form-control form-control-solid placeholder-no-fix'
                         )); ?>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                     <?php echo $this->Form->input('password', array(
                            'type'=>'password',
                            'autocomplete'=>'off',
                            'label'=>false,
                            'placeholder'=>'Password',
                            'class'=>'form-control form-control-solid placeholder-no-fix',
                            'required' => true
                         ));?>
                </div>
                <div class="form-actions">
                    <?php echo $this->Form->button('Login',array('type'=>'submit','class'=>'btn green uppercase')); ?>

                    <label class="rememberme check">
                    <?php echo $this->Form->checkbox('remember', array(
                             'label'=>false,
                            'class'=>'checker',
                            'checked'=> @$checked
                         ));?>
                         <label for="ModelName1" class="selected">Remember</label>
                       <!-- <div class="checker">
                            <span><input type="checkbox" value="1" name="remember"></span>
                        </div>Remember -->
                    </label>
                     <?php echo  $this->Html->link('Forgot Password?', array('controller'=>'users','action'=>'forgotpass'),array('id' => 'forget-password','class' => 'forget-password'
    ));?>
                    
                </div>
            <?php echo $this->Form->end(); ?>
            <!-- END LOGIN FORM -->