            <h3 class="page-title"> <?php echo $mainTitle;?></h3>
             <?php echo $this->Session->flash();?>
                                <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="fa fa-gift"></i>My Profile</div>
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                             <?php echo $this->Form->create('User');
                                              echo $this->Form->input('id', array('type'=>'hidden'));  ?> 
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">First Name<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('first_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Last Name</label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('last_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Gender</label>
                                                                    <div class="col-md-9">
                                                                    <?php
                                                                       echo $this->Form->input('gender', array('class'=>'form-control','label'=>false,'required'=>true,'options' => array('Male','Female')));

                                                                        ?>

                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Date of Birth</label>
                                                                         <div class="col-md-9">
                                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                        <?php

                                                                   echo $this->Form->input('dob', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                                       
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Email</label>
                                                                    <div class="col-md-9">
                                                                        <?php echo $user['User']['email'];

                                                                    ?>
                                                                         <span class="help-block"><br></span>
                                                                          </div>

                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Phone</label>
                                                                    <div class="col-md-9">  <?php

                                                                   echo $this->Form->input('mobile', array('type' => 'numeric','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                

                                                                        <button class="btn green" type="submit">Submit</button>
                                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'users'),array('class' => 'btn default')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                    <?php echo $this->Form->end();?>
                                              
                                                <!-- END FORM-->
                                            </div>
                                        </div>

