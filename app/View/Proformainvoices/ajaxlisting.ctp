<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Approval"),
    array("danger" => "Rejected"),
    array("danger" => "Pending")

  );

  foreach ($proformainvoice as $proformainvoices) {
    $records = array(); 
    if($proformainvoices['Proformainvoice']['status'] == 1){
      $status = $status_list[0];
    }
    else if($proformainvoices['Proformainvoice']['status'] == 2){
      $status = $status_list[1];
    }
    else{
      $status = $status_list[2];
    }
    $id = $proformainvoices['Proformainvoice']['id'];
    /*check record entered in next stage means used in another module*/
    $record_nextstage = $this->requestAction(
                  array('controller' => 'App', 'action' => 'check_nextstage'),
                  array('dependantmodel' => 'Order','conditional_parameter' => 'Order.proformaid = '.$id)
    ); 
    if(empty($record_nextstage)){
      $records[] = '<input type="checkbox" name="id[]" value="'.$proformainvoices['Proformainvoice']['id'].'">';
    }
    else{
      $records[] = '';
    }
    
    $records[] = '<a href="'.WEBSITE_PATH.'proformainvoices/view/'.base64_encode($proformainvoices['Proformainvoice']['id']).'" title="View Proforma Invoice">'.$proformainvoices['Proformainvoice']['proforma_no'].'</a>';
    $records[] = date('d/m/Y', strtotime($proformainvoices['Proformainvoice']['proforma_date']));
    $records[] = $proformainvoices['Client']['company_name'];
    $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($proformainvoices['Proformainvoice']['modified']));
    $records[] = $proformainvoices['User']['first_name']." ".$proformainvoices['User']['last_name'];
    
    //$records[] = '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>';
    
    /*IF Not Verified than*/
    if($proformainvoices['Proformainvoice']['isverified'] == 0){
      $records[] = '<span class="label label-sm label-danger test">Pending</span>';
      $records[] = '<div class="form-group"><div class="col-md-9"><button id = "btn_verify" value="'.$proformainvoices['Proformainvoice']['id'].'" class = "demo-loading-btn btn blue verify" data-loading-text="Loading..." type="button" title="Click to Verify"> Verify </button>';
    }
    /*IF Verified than*/
    else{
      if($proformainvoices['Proformainvoice']['status'] == 1){
        $records[] = '<span class="label label-sm label-success">Accepted</span>';
        $records[] = '<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i><label>';
      }
      else if($proformainvoices['Proformainvoice']['status'] == 2){
        $records[] = '<span class="label label-sm label-danger">Rejected</span>';
        $records[] = '<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i><label>';
      }
      else{
         $records[] = '<button class="btn blue btn-outline" type="button" id="btn_accept" value="1" data-pid="'.$proformainvoices['Proformainvoice']['id'].'">Accept</button>&nbsp;<button class="btn red btn-outline" type="button" id="btn_reject" value="2" data-pid="'.$proformainvoices['Proformainvoice']['id'].'">Reject</button>';         
        $records[] = '<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i><label>';
      }
    }
    $permiss_ses = '';
    if(in_array('proformainvoices/edit',$this->Session->read('accesscontrollers_actions'))){
        if($proformainvoices['Proformainvoice']['status'] != 1){
          $permiss_ses .= '<a href="'.WEBSITE_PATH.'proformainvoices/edit/'.base64_encode($proformainvoices['Proformainvoice']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
        }
    }
   
    if(in_array('proformainvoices/delete',$this->Session->read('accesscontrollers_actions')) && empty($record_nextstage)){
      if($proformainvoices['Proformainvoice']['status'] != 1){
        $permiss_ses .= '<a href="'.WEBSITE_PATH.'proformainvoices/delete/'.base64_encode($proformainvoices['Proformainvoice']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this invoice?"><span class="glyphicon glyphicon-trash"></span></a>';
      }
    }
    $print = '<a id="printrow" data-controllername="proformainvoices" data-actionname="prints" data-id ="'.base64_encode($proformainvoices['Proformainvoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Proformainvoice" targer="_blank"><span class="glyphicon glyphicon-print"></span></a>';   
    $records[] = '<a href="'.WEBSITE_PATH.'proformainvoices/view/'.base64_encode($proformainvoices['Proformainvoice']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>'.$permiss_ses ;
    $record['data'][] = $records;
  }

  $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>