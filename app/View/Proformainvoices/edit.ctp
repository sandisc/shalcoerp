<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""></a></div>
    </div>
    <div class="portlet-body form">    
        <?php echo $this->Form->create('Proformainvoice',array('accept'=>'UTF-8','class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxeditsubmit/'));
            echo $this->Form->input('id',array('type'=>'hidden')) ;?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Proforma No.<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-4">
                                <?php echo $this->Form->input('proforma_no', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'readonly')); ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label col-md-3">Date<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-4">
                                   <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo $this->Form->input('proforma_date', array('type' => 'text','class'=>'form-control changedatepicker','label'=>false,'required'=>true,'value' => date('d/m/Y',strtotime($this->request->data['Proformainvoice']['proforma_date']))));?>
                                    </div>
                                    <span class="help-block"><br></span>                                 
                                </div>
                            </div>
                            <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>
                        </div>                    
                    </div> 
                    <!-- row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Shipping Terms<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-3 input select">
                                    <select id="delivery_type" name="data[Proformainvoice][delivery_type]" class="form-control">
                                        <option value="">Delivery Type</option>
                                        <option value="1" <?php if($proformainvoice['Proformainvoice']['delivery_type'] == '1') {?>selected="selected"<?php }?>>CIF</option>
                                        <option value="2" <?php if($proformainvoice['Proformainvoice']['delivery_type'] == '2') {?>selected="selected"<?php }?>>FOB</option>
                                        <option value="3" <?php if($proformainvoice['Proformainvoice']['delivery_type'] == '3') {?>selected="selected"<?php }?>>Ex-work</option>
                                        <option value="4" <?php if($proformainvoice['Proformainvoice']['delivery_type'] == '4') {?>selected="selected"<?php }?>>CNF</option>
                                    </select>
                                </div>                                
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('tod', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false,'required'=>true,'type'=>'textarea','maxlength'=>'1000')); ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Terms of Payment<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <?php echo $this->Form->input('top', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false,'required'=>true,'type'=>'textarea','maxlength'=>'1000'));?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bill To<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-7">
                                    <?php echo $this->Form->input('bill_to', array('class'=>'form-control bill_to comboselection','type'=>'select','options'=>$clients,'empty' => 'Select Client','label'=>false,'required'=>true));?>
                                    <span class="help-block"><br></span>
                                </div>
                                <div class="col-md-2 checkbox margin0px">
                                    <a onclick="addClient('ProformainvoiceBillTo')" data-target="#addClientModal" data-toggle="modal">Add Client</a>
                                </div>                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Ship To<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <?php echo $this->Form->input('ship_to', array('type' => 'text','class'=>'form-control ship_to','label'=>false,'required'=>true)); ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bill Address</label>
                                <div class="col-md-9">
                                 <?php  echo $this->Form->input('billaddress_id', array('class'=>'form-control bill_to comboselection','options'=>$options,'type'=>'select', 'empty' => 'Select Address','label'=>false,'required'=>true,'id'=>'bill_address'));?>
                                    <?php //echo $this->Form->textarea('bill_address', array('class'=>'form-control','cols'=>'2','rows'=>'2','label'=>false,'required'=>true,'id'=>'edit_billaddress','value'=>$proformainvoice['Client']['address1'],'readonly')); ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Ship Address <span class="required" aria-required="true"> *</span></label>
                                <div class="col-md-9">
                                   <?php  echo $this->Form->input('shipaddress_id', array('class'=>'form-control bill_to comboselection','options'=>$options,'type'=>'select', 'empty' => 'Select Address','label'=>false,'required'=>true,'id'=>"ship_address"));?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row-->
                    <?php 
                    /*Note: accepted proforma can not be make as reject or pending,if you want to make it reject after accpet than you have only one option is : cancel it from order */
                    /*Note : pending proforma also can not be accept and reject from here, bcz we set logic of send email etc from button of it showing in listing*/
                    /*if rejected than u can allow to make it pending or reject*/
                    if($proformainvoice['Proformainvoice']['status'] == 2){?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <?php echo $this->Form->input('status', array('label'=>false,'type'=>'select','options'=>array('0'=>'Pending','2'=>'Rejected'),'required'=>true,'class'=>'form-control','id'=>'status'));?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row-->
                    <?}?>
                    

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">   
                               <div class="col-md-12">
                                    <label><input type="checkbox" value="Same As" id="sameas" name="sameas" class="sameas">Copy billing details to shipping details</label>
                                </div>
                            </div>
                        </div>    
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-5">
                                    <label><input type="checkbox" value="1" id="local" name="data[Proformainvoice][local]">Is Indian order ?</label>
                                </div>
                                <label class="control-label col-md-3">Price Unit<span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-4"> 
                                    <div class="input select">
                                        <select required="required" id="price" class="form-control price" name="data[Proformainvoice][priceid]">
                                            <option value="0">Select Price Unit</option>
                                            <?php foreach($price as $pr =>$v){?>
                                            <option value="<?php echo $pr;?>" <?php if($proformainvoice['Proformainvoice']['priceid'] == $pr) {?>selected="selected"<?php }?>><?php echo $v;?></option><?php }?>
                                        </select>                            
                                    </div>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>                              
                    </div>
                    <!-- row-->
                    <!-- Item List Toggle Option Start-->
                    <div class="form-group">
                        <div class="pull-right">
                            <div class="margin-bottom-10">
                                <label for="option1">Show Item more details</label>            
                                <span id="btnToggle"><input name="switchtoggle" type="checkbox" class="make-switch" checked data-on-color="primary" data-off-color="info"></span>
                            </div>
                        </div>
                    </div>
                    <!-- Item List Toggle Option End-->                    
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content portlet box green">
                            <?php echo proformaitemTableColumn('('.$proformainvoice['Price']['sign'].')');?> 
                            </thead>
                            <tbody>
                                <?php
                                $performaitem = $proformainvoice['Proformaitem'];    
                                $count = sizeof($performaitem);
                                foreach($standard_all as $std){                                            
                                    $std_all[] = $std['Standard']['standard_name'];
                                    foreach($std['Grade'] as $grd){
                                        $grd_name[] = $grd['grade_name'];
                                    }
                                }                                                  
                                foreach($performaitem as $key => $v) {   ++$key; ?>
                                    <tr class="addmore<?php echo $key;?> filldetail">
                                        <td rowspan="2" class="toggleRowSpan"> 
                                            <?php echo $this->Form->input('sr_no', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no'.$key,'value'=>$v['sr_no'],'name'=>'data[Proformainvoice][item]['.$key.'][sr_no]'));?>
                                        </td>
                                        <td> 
                                            <select id='procatid<?php echo $key;?>' class='form-control procat comboselection nameclass' name='data[Proformainvoice][item][<?php echo $key; ?>][procatid]' data-id=<?php echo $key; ?>>
                                                <?php foreach($procat as $pro_cat) {?>
                                                <option value='<?php echo $pro_cat['Productcategory']['id'];?>' <?php if($pro_cat['Productcategory']['id'] == $v['procatid']){ ?> selected = 'selected' <?php } ?>><?php echo $pro_cat['Productcategory']['name'];?></option>
                                                    <?php } ?>
                                        </td>                                      
                                        <td> 
                                            <select id='standard_id<?php echo $key;?>' class='form-control std comboselection nameclass' name='data[Proformainvoice][item][<?php echo $key; ?>][standard_id]' data-id=<?php echo $key;?>>
                                                <?php foreach($standard_all as $std) {
                                                    $product = explode(",",$std['Standard']['procatid']);
                                                    if (in_array($v['procatid'], $product)) {
                                                ?>

                                                <option value='<?php echo $std['Standard']['id'];?>' <?php if($std['Standard']['id'] == $v['standard_id']){ ?> selected = 'selected' <?php } ?>><?php echo $std['Standard']['standard_name'];?></option>
                                                    <?php } }?>
                                        </td>
                                        <td>
                                            <select id='grade_id<?php echo $key;?>' class='form-control grd comboselection nameclass' name='data[Proformainvoice][item][<?php echo $key; ?>][grade_id]'>
                                                <?php foreach($standard_all as $grd) {
                                                         foreach($grd['Grade'] as $grds){
                                                            if($grds['standard_id'] == $v['standard_id']){
                                                    ?>
                                                <option value='<?php echo $grds['id'];?>' <?php if($grds['id'] == $v['grade_id']){ ?> selected = 'selected' <?php } ?>><?php echo $grds['grade_name'];?></option>
                                                    <?php  } } } ?>
                                        </td>
                                        <td style="white-space:nowrap;">
                                            <select id='size_id<?php echo $key;?>' class='form-control size comboselection nameclass' name='data[Proformainvoice][item][<?php echo $key; ?>][size_id]' data-id=<?php echo $key;?>>
                                                <?php foreach($gdmm_all as $gd) {
                                                    ?>
                                                <option value='<?php echo $gd['Size']['id'];?>' <?php if($gd['Size']['id'] == $v['size_id']){ ?> selected = 'selected' <?php } ?>><?php echo $gd['Size']['gdmm'];?></option>
                                                    <?php  }   ?>   
                                            </select>                                                 
                                        </td>
                                        <td> 
                                            <?php echo $this->Form->input('length', array('class'=>'form-control length nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'length'.$key,'value'=>$v['length'],'name'=>'data[Proformainvoice][item]['.$key.'][length]'));?>
                                        </td>                                
                                        <td> 
                                            <?php echo $this->Form->input('qty', array('class'=>'form-control qty nameclass decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>$v['qty'],'name'=>'data[Proformainvoice][item]['.$key.'][qty]','data-id'=>$key));?>
                                        </td>
                                        <td> 
                                           <?php echo $this->Form->input('unit', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'unit'.$key,'name'=>'data[Proformainvoice][item]['.$key.'][unit]','readonly','value'=>$v['Productcategory']['Producttaxonomy']['unit'],'data-id'=>$key));?>
                                        </td>
                                       
                                        <td rowspan="2" class="toggleRowSpan"> 
                                            <?php echo $this->Form->input('price', array('class'=>'form-control prc nameclass decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'price'.$key,'value'=>$v['price'],'name'=>'data[Proformainvoice][item]['.$key.'][price]','data-id'=>$key));?>
                                        </td>
                                        <td rowspan="2" class="toggleRowSpan">
                                            <?php echo $this->Form->input('amount', array('class'=>'form-control amount nameclass','label'=>false,'type'=>'text','required'=>true,'id'=>'amount'.$key,'readonly','value'=>$v['amount'],'name'=>'data[Proformainvoice][item]['.$key.'][amount]'));
                                            ?>
                                        </td>
                                        <td rowspan="2" class="toggleRowSpan"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','id'=>'delete1','class'=>'deleteno','mydelnum'=> $key,'data-id'=> $v['id'],'data-amount'=>$v['amount'])); ?>
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              <img width="16" height="16" id="add" class="addno" alt="add" src="<?php echo ROOT_PATH;?>img/add.png">
                                        </td>
                                       
                                        <?php echo $this->Form->input('id', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'item_id'.$key,'readonly','value'=>$v['id'],'name'=>'data[Proformainvoice][item]['.$key.'][id]'));
                                            ?>
                                           <?php echo $this->Form->input('ratio', array('class'=>'form-control ratio nameclass','label'=>false,'type'=>'hidden','id'=>'ratio'.$key,'readonly','value'=>$v['ratio'],'name'=>'data[Proformainvoice][item]['.$key.'][ratio]'));
                                            ?>
                                    </tr>
                                    <tr class="dispalydetails readonlydetails<?php echo $key;?>">
                                        <td colspan="3" class="productname<?php echo $key;?>"><b>Product Name : </b><?php echo $v['Productcategory']['productname'];?></td>
                                        <td colspan="3" class="sizedata<?php echo $key;?>"><a onclick="addSize('size_id<?php echo $key;?>')" data-target="#addSizeModal" data-toggle="modal" title="Click to Add New Size"> Add Size </a><span class="gdnbdata<?php echo $key;?> nameclass">&nbsp;
                                        <?php if($v['ratio'] != 0.00){?><b>Ratio : </b><?php echo $v['ratio'];?>&nbsp;<?php }if($v['Size']['gdnb']!=''){?><b>GD(NB) : </b><?php echo $v['Size']['gdnb'];}?></span></td>
                                    </tr>                                         
                                <?php  } ?>
                                    <tr class="item_total">    
                                    <td colspan="8"></td>
                                       <td id="total_lbl"> Total </td>
                                       <td><?php echo $this->Form->input('total', array('class'=>'form-control final','label'=>false,'type'=>'text','id'=>'total','readonly'));?></td>
                                    </tr>

                                <input type="hidden" name="updateno" id="up" value="<?php echo $count; ?>" class="input" />
                                <input type="hidden" name="remno" id="remno" value="<?php echo $count;?>" width="35px;"/>
                                <input type="hidden" name="removal_id" id="removal_id" value="" width="35px;"/>
                            </tbody>
                        </table>
                    </div>

            <!-- New Row Qunatity -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Quantity Tolerance<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-9">
                            <?php echo $this->Form->input('quantity', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- New Row Delivery Period -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Delivery Period<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-9">
                            <?php echo $this->Form->input('delivery_period', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Dimension -->
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Dimensions<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-9">
                            <?php echo $this->Form->input('dimensions', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Certifications -->
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Certification<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-9">
                            <?php echo $this->Form->input('certifications', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Comments -->
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Comment/Notes</label>
                            <div class="col-md-9">
                                <?php echo $this->Form->input('comment', array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','id'=>'comment','type'=>'textarea','maxlength'=>'1000')); ?>
                            <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bank Name<span class="required" aria-required="true"> *</span></label>
                                <div class="col-md-9">
                                    <?php echo $this->Form->input('bank_id', array('label'=>false,'type'=>'select','options'=>$bank,'required'=>true,'class'=>'form-control')); ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--row-->
                </div>
                <!-- END form-body class-->
                                        
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn green" type="submit">Submit</button>
                                     <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'proformainvoices'),array('class' => 'btn default')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> </div>
                    </div>
                </div>
        <?php echo $this->Form->end();?>
    </div>
</div>
<?php echo $this->element('proforma_commonfunction'); ?>
<script type="text/javascript">
  
 $(document).on('click', '.addno', function () {
    var no = $('#up').val();
    var nxt = parseInt(no) + 1;      
    var address = [];
    $(this).closest('tr').find('.nameclass').each(function() {
        var id = $(this).val();
       // var text = $("option:selected").text();
        address.push(id);
      // address.push();
    });
    console.log(address);
        var testy = [];
        $(this).closest('tr').next('tr').find('.nameclass').each(function() {
            var id1 = $(this).html();
            testy.push(id1);
        });     
         console.log(testy); 
    var ad = address[2].replace(/"/g, '&quot;');
    var t = address[6].replace(/"/g, '&quot;'); 
    var nextsizerowid = "'size_id"+nxt+"'";

    /*Check if toggle is off state, if off than make it on*/
    if(rwspanval == 1){
        $("[name='switchtoggle']").bootstrapSwitch('state', true, true);
        $("#btnToggle").trigger('click');
    }

      $(".item_total").before('<tr class="filldetail addmore'+nxt+'"><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input text"><input type="text" id="sr_no'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][sr_no]"" value="'+nxt+'"></div></td><td><div class="input select"><select id="procatid'+nxt+'" required="required" class="form-control procat comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][procatid]" data-id='+nxt+'><option value="">Select Category</option><?php foreach($procat as $prcat){ ?><option value="<?php echo $prcat['Productcategory']['id']; ?>"><?php echo $prcat['Productcategory']['name'];?></option><?php } ?></div></select></td><td><div class="input select"><select id="standard_id'+nxt+'" required="required" class="form-control std comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][standard_id]" data-id='+nxt+'><option value="">Select Standard</option></select></div></td><td><div class="input select"><select id="grade_id'+nxt+'" required="required" class="form-control grd comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][grade_id]"><option value="">Select Grade</option></select></div></td><td style="white-space:nowrap"><select id="size_id'+nxt+'" required="required" class="form-control size comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][size_id]" data-id='+nxt+'><option value="">Select Size</option><?php foreach($gdmm as $gdmm_all => $values){ ?><option value="<?php echo $gdmm_all; ?>"><?php echo $values;?></option><?php } ?></select></div></td><td style="white-space:nowrap;"><div class="input text"><input type="text" id="length'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][length]" value="'+address[5]+'"><div></td><td><div class="input text"><input type="text" id="qty'+nxt+'" required="required" class="form-control qty nameclass decimalallow" data-id='+nxt+' name="data[Proformainvoice][item]['+nxt+'][qty]"></div></td><td><div class="input text"><input type="text" readonly="readonly" id="unit'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][unit]" value="'+address[7]+'"></div></td><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input text"><input type="text" id="price'+nxt+'" required="required" data-id='+nxt+' class="form-control prc nameclass decimalallow" name="data[Proformainvoice][item]['+nxt+'][price]"></div></td><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input number"><input type="text" readonly="readonly" id="amount'+nxt+'" required="required" class="form-control amount nameclass" name="data[Proformainvoice][item]['+nxt+'][amount]"></div></td><td rowspan="'+rwspanval+'" class="toggleRowSpan"><img class="deleteno" alt="delete" id="delete1'+nxt+'" mydelnum="'+nxt+'" data-id="" src="<?php echo ROOT_PATH;?>img/warning.png" data-amount="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="16" height="16" id="add" class="addno" alt="add" src="<?php echo ROOT_PATH;?>img/add.png"></td><input type="hidden" id="ratio'+nxt+'" class="form-control ratio nameclass" name="data[Proformainvoice][item]['+nxt+'][ratio]" value="'+address[10]+'"></tr><tr class="dispalydetails readonlydetails'+nxt+' "><td colspan="3" class="productname'+nxt+'"></td><td colspan="3" class="sizedata'+nxt+'"><a onclick="addSize('+nextsizerowid+')" data-target="#addSizeModal" data-toggle="modal" title="Click to Add New Size"> Add Size</a><span class="gdnbdata'+nxt+' nameclass">'+testy+'</span></td></tr>');

        $('#up').val(nxt);

        $("#procatid"+nxt+" option[value='"+address[1]+"']").attr("selected", true);        
        $("#grade_id"+nxt+" option[value='"+address[2]+"']").attr("selected", true);
        $("#size_id"+nxt+" option[value='"+address[4]+"']").attr("selected", true);                     

       /* $.ajax({
            url: '<?php echo WEBSITE_PATH;?>proformainvoices/getgdmm',
            type: 'POST',
            cache: false,
            success: function (data) {
                $('#size_id'+nxt).html(data);
                $("#size_id"+nxt+" option[value='"+address[5]+"']").attr("selected", true);
                $("#select2-size_id"+nxt+"-container").text($("#size_id"+nxt+" option[value='"+address[5]+"']").text());
            }
        });*/
        $("#procatid"+nxt).val(address[1]);
        $("#procatid"+nxt).val(address[1]).trigger('change',[address[2],address[3],address[4]]);

       $(".comboselection").select2();
       var remmno = $('#remno').val();
       /*var remnextno = parseInt(remmno) + 1;
       var t = $('#remno').val(remnextno);*/
     //  $('#remno').val(remnextno);
      //$('#remupno').val(remnextno);
        
    });
  $(document).on('click', '.deleteno', function () {
                 
    var id = $(this).attr('data-id');
    var upno = $('#up').val();
      //console.log(upno);
    var t = $(this).parent().prev().find('input').attr('value');
    var gd = $(this).attr('mydelnum');
    var mtr = gd.substr(gd.length - 1);
    var amount = $('#amount'+mtr).val();
    console.log(amount);
    
        //var g = t.find(":value");
    //  console.log(t);
    if(upno != 1 ){
        if(id == ''){
            $('#removal_id').val();
        }
        else{   
            $('#removal_id').val(function(i,val) { 
                return val + (!val ? '' : ',') + id;
            });
        }
        var myno = $(this).attr('mydelnum');
       // console.log(myno);
       $(this).closest('tr').next('tr').remove();
        $(this).parent().parent().remove();
        var remainingno = parseInt(upno) - 1;
        //console.log(remainingno);
        var upno = $('#up').val(remainingno);
      //  console.log(upno);
   
    var rem_amount = $(this).data("amount");
    var final_total = $('#total').val();
    if(!rem_amount){
        if(!isNaN(rem_amount) && rem_amount.length != 0) { 
            var del_amount = parseFloat(final_total) - parseFloat(rem_amount);
            $('#total').val((del_amount).toFixed(2));
        }
    }else{
        var del_amount = parseFloat(final_total) - parseFloat(rem_amount);
        $('#total').val((del_amount).toFixed(2));
    }
}
});
</script>