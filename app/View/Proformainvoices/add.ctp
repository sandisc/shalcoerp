<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<?php echo $this->Form->create('Proformainvoice',array('type' => 'file','class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxaddsubmit/')); ?>				  
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
		<div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
	</div>
	<div class="portlet-body form">
		<div class="form-body">	
			<div class="row">
				<div class="form-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">Proforma No.<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-4">
							<?php echo $this->Form->input('proforma_no', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'readonly')); ?>
								<span class="help-block"><br></span>
							</div>
						</div>
					</div>															
					<div class="col-md-6">
						<div class="form-group">
						    <label class="control-label col-md-3">Date<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-4">
							   <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
							   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<?php echo $this->Form->input('proforma_date', array('type' => 'text','class'=>'form-control changedatepicker','label'=>false,'required'=>true));?>
								</div>
								<span class="help-block"><br></span>
	                        </div>
						</div>
					</div>
					<? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>		
				</div>
			</div>
			<!--/row--> 
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Shipping Terms<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-3 input select">
                            <select id="delivery_type" name="data[Proformainvoice][delivery_type]" class="form-control">
                                <option value="">Delivery Type</option>
                                <option value="1">CIF</option>
                                <option value="2">FOB</option>
                                <option value="3">Ex-work</option>
                                <option value="4">CNF</option>
                            </select>
                        </div>                                
                        <div class="col-md-6">																			
							<?php echo $this->Form->input('tod', array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','required'=>true,'type'=>'textarea','maxlength'=>'1000')); ?>							
							<span class="help-block"><br></span>
						</div>
					</div>
				</div>															
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Terms of Payment<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('top', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false,'required'=>true,'type'=>'textarea','maxlength'=>'1000')); ?>
                            <span class="help-block"><br></span>
						</div>
					</div>
				</div>
			</div>
            <!--/row-->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Bill To<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-7">						
                            <?php  echo $this->Form->input('bill_to', array('class'=>'form-control bill_to comboselection','options'=>$clients,'type'=>'select', 'empty' => 'Select Client','label'=>false,'required'=>true));?>
                            <span class="help-block"><br></span>
					    </div>
					    <div class="col-md-2 checkbox margin0px">
                            <a onclick="addClient('ProformainvoiceBillTo')" data-target="#addClientModal" data-toggle="modal">Add Client</a>
                        </div>
					</div>					
                </div>                                								
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Ship To<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">  <?php echo $this->Form->input('ship_to', array('type' => 'text','class'=>'form-control ship_to','label'=>false,'required'=>true)); ?>
						 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
			</div>
            <!--/row-->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					   <label class="control-label col-md-3">Bill Address</label>
					   <div class="col-md-9"> 
					   		<!--<div id="bill_address"></div> -->
					   		 <?php  echo $this->Form->input('billaddress_id', array('class'=>'form-control bill_to comboselection','options'=>'','type'=>'select', 'empty' => 'Select Address','label'=>false,'required'=>true,'id'=>'bill_address'));?>
					    	<?php //echo $this->Form->textarea('bill_address', array('class'=>'form-control','cols'=>'2','rows'=>'2','label'=>false,'required'=>true,'id'=>'bill_address','readonly'));?>
					    	<span class="help-block"><br></span>						
						</div>
					</div>
				</div>
                <div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Ship Address <span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">   <?php  echo $this->Form->input('shipaddress_id', array('class'=>'form-control bill_to comboselection','options'=>'','type'=>'select', 'empty' => 'Select Address','label'=>false,'required'=>true,'id'=>"ship_address"));?>
						 <span class="help-block"><br></span>
						</div>
					</div>
				</div>															
			</div>
			<!--/row-->
			
           	<div class="row">
				 <div class="col-md-6">
                    <div class="form-group">                    	
                    	<!-- <div class="col-md-1"></div> -->
                    	<div class="col-md-12">
                    	<label><input type="checkbox" value="Same As" id="sameas" name="sameas" class="sameas">Copy billing details to shipping details</label>
                        <!--     <label><input type="checkbox" value="1" id="local" name="data[Proformainvoice][local]">Is Indian order ?</label> -->
                        </div>
	                </div>
	             </div>
                <div class="col-md-6">
					<div class="form-group">
						<!--<div class="col-md-6 checkbox margin0px">-->
	                    <div class="col-md-5">
                            <label><input type="checkbox" value="1" id="local" name="data[Proformainvoice][local]">Is Indian order ?</label>
                        </div>
						<label class="control-label col-md-3">Price Unit<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-4"> 
	                        <div class="input select"><select required="required" id="price" class="form-control price" name="data[Proformainvoice][priceid]" required="required">
	                        	<option value="0">Select Price Unit</option>
	                        	<?php foreach($price as $pr =>$v){?>
								<option value="<?php echo $pr;?>" <?php if($pr == '1'){ ?> selected="selected" <?php } ?>><?php echo $v;?></option><?php  }?>
								</select>
							</div>
							<span class="help-block"><br></span>
	                    </div>
                	</div>
  				</div>
  			</div>
  			<!-- Item List Toggle Option Start-->
			<div class="form-group">
			    <div class="pull-right">
			        <div class="margin-bottom-10">
			            <label for="option1">Show Item more details</label>            
			            <span id="btnToggle"><input name="switchtoggle" type="checkbox" class="make-switch" checked data-on-color="primary" data-off-color="info"></span>
			        </div>
			    </div>
			</div>
			<!-- Item List Toggle Option End-->
  			<div class="table-scrollable">
				<table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
					<thead class="flip-content portlet box green">
						<?php echo proformaitemTableColumn();?>												   
	                </thead>
					<tbody>
					<tr class="addmore1 filldetail" data-id="1">
								
							<td rowspan="2" class="toggleRowSpan"><?php echo $this->Form->input('sr_no', array('class'=>'form-control sr_no nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no1','name'=>'data[Proformainvoice][item][1][sr_no]','value'=>1));?>
							</td>
							<td><?php echo $this->Form->input('procatid', array('class'=>'form-control procat comboselection nameclass','type'=>'select','options' => $procat, 'empty' => 'Select Product Category','label'=>false,'required'=>true,'id'=>'procatid1','data-id'=>1,'name'=>'data[Proformainvoice][item][1][procatid]'));?>
							</td>
							<td><?php echo $this->Form->input('standard_id', array('class'=>'form-control std comboselection nameclass','type'=>'select', 'empty' => 'Select Standard','label'=>false,'required'=>true,'id'=>'standard_id1','name'=>'data[Proformainvoice][item][1][standard_id]','data-id'=>1));?>
							</td>
							<td><?php echo $this->Form->input('grade_id', array('class'=>'form-control grd comboselection nameclass','type'=>'select','empty' => 'Select Grade','label'=>false,'required'=>true,'id'=>'grade_id1','name'=>'data[Proformainvoice][item][1][grade_id]'));?>
							</td>
							<td style="white-space:nowrap;" class="custom_cls">
	                          <?php echo $this->Form->input('size_id', array('class'=>'form-control size comboselection nameclass','type'=>'select','options'=>'','empty'=>'Select Size','label'=>false,'required'=>true,'id'=>'size_id1','name'=>'data[Proformainvoice][item][1][size_id]','data-id'=>1)); ?>	                         
							</td>
							<td style="white-space:nowrap;">
							  <?php echo $this->Form->input('length', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'length1','name'=>'data[Proformainvoice][item][1][length]'));?>
							</td>							
							<td> 
							  <?php echo $this->Form->input('qty', array('class'=>'form-control qty nameclass decimalallow','type'=>'text' ,'label'=>false,'required'=>true,'id'=>'qty1','name'=>'data[Proformainvoice][item][1][qty]','data-id'=>1));?>
							</td>
							<td style="white-space:nowrap;">
							  <?php echo $this->Form->input('unit', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'unit1','name'=>'data[Proformainvoice][item][1][unit`]','readonly'));?>
							</td>
							<td rowspan="2" class="toggleRowSpan"> 
							  <?php echo $this->Form->input('price', array('class'=>'form-control prc nameclass decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'price1','name'=>'data[Proformainvoice][item][1][price]','data-id'=>1));?>
							</td>
							<td rowspan="2" class="toggleRowSpan">
							<?php echo $this->Form->input('amount', array('class'=>'form-control amount nameclass','label'=>false,'type'=>'text','required'=>true,'id'=>'amount1','readonly','name'=>'data[Proformainvoice][item][1][amount]','data-id'=>1));?>
							</td>
							<td rowspan="2" class="toggleRowSpan"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'deleteno','value'=>'1','title'=>'Delete')); ?>&nbsp;&nbsp;
	                            <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addno','value'=>'1')); ?>
	                        </td>
	                        <?php echo $this->Form->input('ratio', array('class'=>'form-control ratio nameclass','type'=>'hidden','label'=>false,'id'=>'ratio1','name'=>'data[Proformainvoice][item][1][ratio]','value'=>''));?> 														
						</tr>
						<tr class="dispalydetails readonlydetails1">
						  <td colspan="3" class="productname1"></td>
						  <td colspan="3" class="sizedata1"><a onclick="addSize('size_id1')" data-target="#addSizeModal" data-toggle="modal" title="Click to Add New Size"> Add Size</a><span class="gdnbdata1 nameclass"></span></td>
						</tr>					
					   
						<tr id="1" class="item_total">
	     				   <td colspan="8"></td>
	     				   <td id="total_lbl"> Total </td>
	     				   <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly'));
							?></td>
					     </tr>											
					</tbody>
				</table>
			</div>
			<input type="hidden" name="updateno" id="up" value="1" class="input" />
			<input type="hidden" name="remno" id="remno" value="1" width="35px;"/>

			<!-- New Row Qunatity -->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Quantity Tolerance<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('quantity', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- New Row Delivery Period -->
	        <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Delivery Period<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('delivery_period', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Dimension -->
	         <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Dimensions<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('dimensions', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
			<!-- Certifications -->
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Certification<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('certifications', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Comments -->
	         <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Comment/Notes</label>
							<div class="col-md-9">
							<?php echo $this->Form->input('comment', array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','id'=>'comment','type'=>'textarea','maxlength'=>'1000')); ?>					 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Bank Details -->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Bank Name<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('bank_id', array('label'=>false,'type'=>'select','options'=>$bank,'required'=>true,'class'=>'form-control','empty'=>'Select Your Bank')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>

	        <!-- row-->			
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
							<?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'proformainvoices'),array('class' => 'btn default')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6"> </div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->

<?php echo $this->element('proforma_commonfunction'); ?>
<script type="text/javascript">
$(document).ready(function() {		
    /* Use  : Get Price of Amount ex $,& */
	$("#amount_lbl").html('Amount '+'($)');
	$("#prc_lbl").html('Price '+'($)');
	$("#total_lbl").html('Total '+'($)');
});


/*Use : Get grade name to related Standard via ajax*/

/*$(document).on('change', '.procat', function (event,selected,grd) {
	// event.stopImmediatePropagation();
	var procat_value = $(this).val();
	var row_id = $(this).attr('data-id');
	//console.log(selected);
	console.log('row='+row_id+'=procat id='+procat_value+'=standard='+selected +'=grade='+ grd);
	//var prod = row_id.substr(row_id.length - 1);
	//console.log(row_id);

	$.ajax({
		url: '<?php echo WEBSITE_PATH;?>proformainvoices/getStandards/'+procat_value,
		type: 'POST',
		cache: false,
	    success: function (data) {		   	
		   	//console.log($("standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true));
			$('#standard_id'+row_id).html(data);
			$("#standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true);		
			$('#standard_id'+row_id).val(selected);
			$('#standard_id'+row_id).val(selected).trigger('change',[grd]);
			var sel = $('#standard_id'+row_id+' option:selected').html();
			$('#select2-standard_id'+row_id+'-container').html(sel);
			//console.log($("#grade_id"+row_id+" option[value='"+grade+"']").attr("selected", true));
			//var grd = $('#grade_id'+row_id+' option:selected').html();
			//$('#select2-grade_id'+row_id+'-container').html(grd);		 
		}
	});
});*/




	/*Use : Add more Row Functionality.*/
	$(document).on('click', '.addno', function () {
		var no = $('#up').val();
		var nxt = parseInt(no) + 1;
		var address = [];
		$(this).closest('tr').find('.nameclass').each(function() {
	        var id = $(this).val();
	        address.push(id);
	    });	
		var ratio_cpy = [];
		$(this).closest('tr').next('tr').find('.nameclass').each(function() {
	        var id1 = $(this).html();
	        ratio_cpy.push(id1);
	    });	
	    //console.log(address);
	    //console.log(address[6] +'==='+ address[7]+'==='+ address[8]+'==='+ address[9]+'==='+ address[10]+'==='+ address[11]);	
	    
	   // var ad = address[2].replace(/"/g, '&quot;');
	   // var t = address[6].replace(/"/g, '&quot;');	
	    var nextsizerowid = "'size_id"+nxt+"'"; 

		/*Check if toggle is off state, if off than make it on*/
		if(rwspanval == 1){
		  	$("[name='switchtoggle']").bootstrapSwitch('state', true, true);
		  	$("#btnToggle").trigger('click');
		}

	    $(".item_total").before('<tr class="filldetail addmore'+nxt+'" data-id="'+nxt+'"><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input text"><input type="text" id="sr_no'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][sr_no]" value="'+nxt+'"></div></td><td><div class="input select"><select id="procatid'+nxt+'" required="required" data-id='+nxt+' class="form-control procat comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][procatid]" data-id='+nxt+'><option value="">Select Product Category</option><?php foreach($procat as $pro_cat => $val){ ?><option value=<?php echo $pro_cat;?>><?php echo $val;?></option><?php } ?></select></div></td><td><div class="input select"><select id="standard_id'+nxt+'" required="required" class="form-control std comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][standard_id]" data-id='+nxt+'><option value="">Select Standard</option></select></div></td><td><div class="input select"><select id="grade_id'+nxt+'" required="required" class="form-control grd comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][grade_id]"><option value="">Select Grade</option></select></div></td><td style="white-space:nowrap"><select id="size_id'+nxt+'" required="required" class="form-control size comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][size_id]" data-id='+nxt+'><option value="">Select Size</option></select></div></td><td style="white-space:nowrap;"><div class="input text"><input type="text" id="length'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][length]" value="'+address[5]+'"><div></td><td><div class="input text"><input type="text" id="qty'+nxt+'" required="required" class="form-control qty nameclass decimalallow" name="data[Proformainvoice][item]['+nxt+'][qty]" data-id='+nxt+'></div></td><td style="white-space:nowrap;"><div class="input text"><input type="text" id="unit'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][unit]" value="'+address[7]+'" readonly="readonly"><div></td><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input text"><input type="text" id="price'+nxt+'" required="required" class="form-control prc nameclass decimalallow" name="data[Proformainvoice][item]['+nxt+'][price]" data-id='+nxt+'></div></td><td rowspan="'+rwspanval+'" class="toggleRowSpan"><div class="input number"><input type="text" readonly="readonly" id="amount'+nxt+'" required="required" class="form-control amount nameclass" name="data[Proformainvoice][item]['+nxt+'][amount]"></div></td><td class="numeric toggleRowSpan" rowspan="'+rwspanval+'"><img class="deleteno" alt="delete" mydelnum="'+nxt+'"src="<?php echo ROOT_PATH;?>img/warning.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="16" height="16" id="add" class="addno" alt="add" src="<?php echo ROOT_PATH;?>img/add.png"></td><input type="hidden" id="ratio'+nxt+'" class="form-control ratio nameclass" name="data[Proformainvoice][item]['+nxt+'][ratio]" value="'+address[10]+'"></tr><tr class="dispalydetails readonlydetails'+nxt+' "><td colspan="3" class="productname'+nxt+'"></td><td colspan="3" class="sizedata'+nxt+'"><a onclick="addSize('+nextsizerowid+')" data-target="#addSizeModal" data-toggle="modal" title="Click to Add New Size"> Add Size</a><span class="gdnbdata'+nxt+' nameclass">'+ratio_cpy+'</span></td></tr>');

	    

		var upno = $('#up').val(nxt);	
	    var remmno = $('#remno').val();
	    var remnextno = parseInt(remmno) + 1;
	    var t = $('#remno').val(remnextno);
				    
	    $("#procatid"+nxt+" option[value='"+address[1]+"']").attr("selected", true);		
		$("#grade_id"+nxt+" option[value='"+address[2]+"']").attr("selected", true);
		$("#size_id"+nxt+" option[value='"+address[4]+"']").attr("selected", true);		    		    

	   /* $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getgdmm',
			type: 'POST',
			cache: false,
		    success: function (data) {
				$('#size_id'+nxt).html(data);
				$("#size_id"+nxt+" option[value='"+address[5]+"']").attr("selected", true);
				$("#select2-size_id"+nxt+"-container").text($("#size_id"+nxt+" option[value='"+address[5]+"']").text());
			}
		});*/
		$("#procatid"+nxt).val(address[1]);
		$("#procatid"+nxt).val(address[1]).trigger('change',[address[2],address[3],address[4]]);
   	    $(".comboselection").select2();
	});

	/* Use : Delete row from list. */
	$(document).on('click', '.deleteno', function () {
			
		var upno = $('#up').val();
		var rm = $('#remno').val();
		var t = $(this).parent().prev().find('input').attr('value');
	    var gd = $(this).attr('mydelnum');
		var mtr = '';
	    if(gd){mtr = gd.substr(gd.length - 1);}
		var amount1 = $('#amount'+mtr).val();    	

		var value=$(this).closest('tr').children('td:first').data("id");
		$('#expense_table tr:last-child td:first-child input').attr("name");		 
	  	if(upno != '1'){
			var myno = $(this).attr('mydelnum');
			$(this).closest('tr').next('tr').remove();
			$(this).parent().parent().remove();

			var remainingno = parseInt(upno) - 1;
			var upno = $('#up').val(remainingno);	
	  

	    var rem_amount = $(this).data("amount");
	    var final_total = $('#total').val();
	    if(!isNaN(amount1) && amount1.length != 0) { 
				var del_amount = parseFloat(final_total) - parseFloat(amount1);
				$('#total').val((del_amount).toFixed(2));
	    }
	 }
	});


</script>