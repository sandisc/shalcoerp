<?php 
header("Cache-Control: ");// leave blank to avoid IE errors
header("Pragma: ");// leave blank to avoid IE errors
header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Disposition: attachment; filename="Proformainvoice.'.$data['Proformainvoice']['proforma_no'].'.doc"');
?>
<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>
<head><title>Microsoft Office HTML Example</title>
<!--[if gte mso 9]>
<xml>
<w:WordDocument>
<w:View>Print</w:View>
<w:Zoom>100</w:Zoom>
<w:DoNotOptimizeForBrowser/>
</w:WordDocument>
</xml>
<![endif]-->

<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
</head>
<body>
<style>
<?php echo $bootstrap; ?>

<?php echo $inlineCss; ?>

</style>
<style>

</style>
  <div class="clearfix"></div>
  <?php  $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>             
                                <div id="tab_3" class="tab-pane active">
                                  <div class="portlet box blue">                                      
                                      <div class="portlet-body form">                                        
                                          <div class="form-body portlet-body flip-scroll doc_file">     
                                         <?php header('content-type: image/png');  ob_start();?>

                                                  <div><?php echo $this->requestAction('App/getURI/');?></div>
                                                  <div class="" style="text-align:center;font-family:'Tahoma';font-size:14px;"><b>PROFORMAINVOICE</b></div>
                                                  <hr>
                                              <table id="proforma_table1" width="100%" style="font-family:'Tahoma';font-size:11px" class="">
                                               <tr class="">
                                                      <td><b>Office Address</b> </td>
                                                      <td><?php echo nl2br($setting['Setting']['office_address']); ?>
                                                      </td>
                                                      <td style="padding-left:20px"><b>PROFORMA NO.</b> <br>
                                                        <?php echo h($data['Proformainvoice']['proforma_no']); ?>
                                                      </td>

                                                      <td><b>Date </b><br>
                                                        <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Proformainvoice']['proforma_date'])); ?>
                                                      </td>
                                                  </tr>
                                                  <tr>&nbsp;</tr>
                                                  <?php $delvery_type = $this->requestAction('App/get_delivery_type/'.$data['Proformainvoice']['delivery_type']);?>
                                                  <tr class="">
                                                      <td width="20%" rowspan="2"><b> Factory Address</b> </td>
                                                      <td rowspan="2"><?php echo nl2br($setting["Setting"]["factory_address"]); ?></td>
                                                      <td width="25%" style="padding-left:20px"><b>Shipping Terms</b></td>
                                                      <td width="25%"><?php echo $delvery_type.$data['Proformainvoice']['tod']; ?></td>
                                                      <tr>
                                                <td style="padding-left:20px"><b>Terms Of Payment</b></td>
                                                <td colspan="2"><?php echo nl2br($data['Proformainvoice']['top']); ?></td>
                                              </tr>
                                            </td>

                                                  </tr>

                                                </table>  
                                                <br>
                                                <table id="proforma_table1" width="100%" class="doc_file">
                                                  <tr class="">
                                                      <td><b>Bill To</b></td>
                                                      <td width="47%"><b>Ship To</b></td>
                                                  </tr>
                                                  <tr class="">
                                                      <td><?php echo nl2br($data['Client']['company_name']); ?><br><?php echo searchFromAddress($data['Proformainvoice']['billaddress_id'], $data['Client']['Address']); ?></td>
                                                      <td><?php echo nl2br($data['Proformainvoice']['ship_to']); ?><br>
                                                      <?php echo searchFromAddress($data['Proformainvoice']['shipaddress_id'], $data['Client']['Address']); ?></td>
                                                  </tr>
                                              </table>
                                              <br>         
                                              <table id="proforma_table" width="100%" style="font-family:'Palatino Linotype';font-size:11px;"> 
                                                  <tr class="border_bottom" bgcolor="#32c5d2">
                                                      <td width="6%" align="center"><b>No.</b></th>
                                                      <td width="14%" align="center"><b>Product Name</b></th>
                                                      <td width="10%" align="center"><b>HS Code</b></th>
                                                      <td width="10%" align="center"><b>Standard</b></th>
                                                      <td width="10%" align="center"><b>Grade</b></th>
                                                      <td width="30%" align="center"><b>Good Description</b></th>
                                                      <td width="10%" align="center"><b>QTY</b></th>
                                                      <td width="10%" align="center"><b>Unit Price<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
                                                      <td width="10%" align="center"><b>Amount<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
                                                  </tr>                                      
                                              <tbody>
                                              <?php $i = 1;
                                                  
                                                foreach($data['Proformaitem'] as $value) {  ?>
                                                  <tr class="border_bottom">    
                                                      <?php $length = $value['length'] * 1000;?>
                                                      <td align="center"><?php echo $value['sr_no'];?></td>
                                                      <td align="center"><?php echo $value['Productcategory']['productname'];?></td>
                                                      <td align="center"><?php echo $value['Productcategory']['Hscode']['hscode'];?></td>
                                                      <td align="center"><?php echo $value['Standard']['standard_name'];?></td>
                                                      <td align="center"><?php echo $value['Grade']['grade_name'];?></td>
                                                      <td style="white-space:nowrap;padding-bottom: 5px;" align="center"><?php echo setGDmmformat($value['Size']['gdmm'],$value['length']).'<br><small style="font-size:11px;">'.'('.setGDnbformat($value['Size']['gdnb'],$value['length']).')</small>';?></td>
                                                      <td class="numalign"><?php echo $value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                                                      <td class="numalign"><?php echo $value['price'];?></td>
                                                      <td class="numalign"><?php echo $value['amount'];?></td>
                                                  </tr>                                            
                                                <?php $i++; } ?>
                                                <tr class="border_total">    
                                                    <td colspan="6"></td>
                                                    <td colspan="2" class="numalign" align="center" style="border-bottom:1px solid black;padding: 5px;"><b>Total <?php echo $data['Price']['fullform'] .'('.$data['Price']['sign'].')';?></b></td>
                                                    <td class="numalign" align="center" style="border-bottom:1px solid black;padding: 5px;"><b><?php echo $data['Proformainvoice']['total'];?></b></td>
                                                </tr>
                                                </tbody>
                                                </table>
                                                <table class="bank_details" id="tbl_bank" style="font-family:'Tahoma';font-size:11px;">
                                              <!-- New Fields -->
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Quantity Tolerance</b></td>
                                                  <td colspan="7"><?php echo h($data['Proformainvoice']['quantity']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Delivery Period </b></td>
                                                  <td colspan="7"><?php echo h($data['Proformainvoice']['delivery_period']); ?></td>                                                  
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Dimensions </b></td>
                                                  <td colspan="7"><?php echo h($data['Proformainvoice']['dimensions']); ?></td>
                                                  
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Certification </b></td>
                                                  <td colspan="7"><?php echo h($data['Proformainvoice']['certifications']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Comment </b></td>
                                                  <td colspan="7"><?php echo h($data['Proformainvoice']['comment']); ?></td>
                                            </tr>
                                            <!-- -->
                                                
                                            <tr>
                                                  <td colspan="9" class="notes_padd"><b><u>Bank Details</u></b></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Bank Name </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['bank_name']); ?></td>
                                                  <td colspan="3"><b>For, SHALCO IND. PVT. LTD. </b></td>
                                                  <td colspan="3" rowspan="10"><b>For, SHALCO IND. PVT. LTD. <br/>DIRECTOR<br/></b><br/><img src="<?php echo ROOT_PATH.'uploads/signature/'.$setting['Setting']['signature'];?>" width="200"></td>                                                  
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Branch Name </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['branch_name']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Swift Code </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['swift_code']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>AD Code </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['ad_code']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Beneficiary’s EEFC A/C No </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['ac_no']); ?></td>
                                            </tr>
                                            <tr>
                                                  <td colspan="2" class="notes_padd"><b>Beneficiary’s Name </b></td>
                                                  <td colspan="4"><?php echo h($data['Bank']['ac_name']); ?></td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" class="notes_padd"><b>IEC Code </b></td>
                                              <td colspan="4"><?php echo $data["Bank"]["iec_code"];?></td>
                                            </tr>
                                            <tr>
                                              <td colspan="2" class="notes_padd"><b>IFC Code </b></td>
                                              <td colspan="4"><?php echo $data["Bank"]["ifc_code"]; ?></td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
</body>
</html>