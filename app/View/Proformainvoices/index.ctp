<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
      <?php echo $this->Session->flash();?>
      <div class="portlet light portlet-fit portlet-datatable bordered">
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
            </div>
           <div class="actions">
               <div class="btn-group btn-group-devided">
                <?php echo $this->Html->link(' Add Proforma Invoice ',array('action' => 'add','controller' =>'proformainvoices'),array('class' => 'fa fa-plus btn sbold green')); ?>
               </div>
               <div class="btn-group">
               </div>
            </div>
         </div>
         
          <div class="portlet-body">
              <div class="table-container">
                  <div class="table-actions-wrapper">
                      <span> </span>
                      <button class="btn btn-sm red table-group-action-delete">
                          <i class="fa fa-trash"></i> Delete</button>
                  </div>
                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                      <thead>
                          <tr role="row" class="heading">
                              <th width="2%"><input type="checkbox" class="group-checkable"> </th>
                              <th width="10%"> Proforma NO </th>
                              <th width="10%"> Proforma Date </th> 
                              <th width="20%"> Client Name </th>
                              <th width="10%"> Modified Date</th>
                              <th width="10%"> Modified By </th>
                              <th width="15%"> Status </th>
                              <th width="8%"> Is Verified? </th>
                             <th width="15%"> Actions </th>
                          </tr>
                          <tr role="row" class="filter">
                              <td> </td>
                              <td><input type="text" class="form-control form-filter input-sm" name="proforma_no"> </td>
                               <td>
                                  <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="proforma_date" placeholder="From">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>

                              </td> 
                              <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>
                              <td>
                                  <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>
                                  <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>
                              </td> 
                              <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                              <td>
                                  <select name="status" class="form-control form-filter input-sm">
                                      <option value="">Select...</option>
                                      <option value="0">Pending</option>
                                      <option value="1">Approval</option>
                                      <option value="2">Rejected</option>
                                  </select>
                              </td> 
                               <td>
                          </td>                                   
                              <td>
                                  <div class="margin-bottom-5">
                                      <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                      <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                  </div>
                              </td>
                          </tr>
                      </thead>
                      <tbody> </tbody>
                  </table>
              </div>
          </div>
      </div>
      <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->


<!-- Start Reject Proforma Invoice Dialog Modal -->
<div class="modal fade" id="rejectform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Proformainvoice',array('method'=>'POST','class'=>'shalcoform')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Reason for Rejecct</h4>
      </div>
      <div class="modal-body">
          <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'rejectid'));
          echo $this->Form->textarea('rejectcomment',array('label'=>false, 'placeholder'=>"Please Write Reason for Reject","class" => "form-control txtarea",'id'=>'rejectcomment','required'));?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input  type="submit" id="btnRejectSubmit" class="btn btn-primary">
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!-- End Reject Proforma Invoice Dialog Modal -->

<!--- Start Approve Proforma Invoice Dialog Modal -->
<div class="modal fade" id="accepctform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Proformainvoice',array('method'=>'POST','class'=>'shalcoform','type' => 'file')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Dispatch Location</h4>
      </div>
      <div class="modal-body">
        <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'approveproformaid'));

          echo '<div class="col-md-6 form-group">';
          echo $this->Form->input('dispatch_id',array('type'=>'select','label'=>false, 'placeholder'=>"Please Select Dispatch Location","class" => "form-control dispatch",'empty'=>'Select Dispatch Location','id'=>'dispatch_id','options'=>$dispatch,'required'));
          echo '</div><div class="col-md-6 form-group">';
          echo $this->Form->input('dispatchcode',array('type' => 'text','label'=>false,'readonly','placeholder'=>"Dispatch Code","class" => "form-control",'id'=>'dispatchcode','required'));           
          echo '</div>';

          echo '<div class="col-md-6 form-group">';
          echo $this->Form->input('clientpo',array('type' => 'text','label'=>false,'placeholder'=>"Enter Client PO Number", "class" => "form-control",'id'=>'clientpo','required'));
          echo '</div><div class="col-md-6 form-group">';
          echo $this->Form->input('clientpodate',array('type' => 'text','label'=>false,'placeholder'=>"Enter Client PO Date", "class" => "form-control",'id'=>'clientpodate','required','before'=>'<div class="input-group date date-picker" data-date-format="dd/mm/yyyy"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>','after'=>'</div>'));
          echo '</div>';

          echo '<div class="col-md-12 form-group">';
          echo $this->Form->textarea('acceptcomment',array('cols'=>'2','rows'=>'2','label'=>false, 'placeholder'=>"Description/Notes","class" => "form-control",'id'=>'acceptcomment','required'));
          echo '</div>';

          echo '<div class="col-md-12 form-group">';
          echo $this->form->input('poupload',array('label'=>false,'type'=>'file','class' => 'form-control','id'=>'poupload','name'=>'data[Proformainvoice][poupload]','required'));
          echo '</div>';
          echo '<span style="color:#337ab7;">(Only doc,pdf,excell files are allowed)</span>';
        ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btnApprove" class="btn btn-primary">Save</button>
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!--- End Approve Proforma Invoice Dialog Modal -->

<script>
  /*Start Click Event of 'Verify' Button for Verify Proforma Invoice*/
    $(document).on('click', '#btn_verify', function () {
      $("#spinner").show();
      var id = $(this).val();
      var ref = $(this); /*set current object reference for use into ajax success event*/
      
      $.ajax({
        url: '<?php echo WEBSITE_PATH;?>proformainvoices/getVerified/'+id,
        type: 'POST',
        cache: false,
        success: function (data) { 
          ref.parent().parent().parent().closest('td').prev().html('<button class="btn blue btn-outline" type="button" id="btn_accept" value="1" data-pid='+id+'>Accept</button>&nbsp;<button class="btn red btn-outline" type="button" id="btn_reject" value="2" data-pid='+id+'>Reject</button>');
          ref.parent().html('<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i>Verified</lable>');
          $("#spinner").hide();
        }
      });
    });
  /*End Click Event of 'Verify' Button for Verify Proforma Invoice*/

  $(document).ready(function() {
  
    /*Start Click Event of 'Accept' Button for accept Proforma Invoice*/   
    $(document).on('click', '#btn_accept', function (e) {
        e.preventDefault();
        //e.stopImmediatePropagation();
        var accept = $(this);
        var status_id = $(this).val();
        var performaid = $(this).attr('data-pid');
        $("#rejectid").val(performaid);
        $("#approveproformaid").val(performaid);
        /*Active*/
        /*      
          $.ajax({
            url: '<?php echo WEBSITE_PATH;?>proformainvoices/changeStatus/'+performaid+'/'+status_id,
            type: 'POST', 
            cache: false,
            success: function (data) {   
              accept.parent().html('<span class="label label-sm label-success">Accepted</span>');
            }
          }); 
        */
        /*Open model form consists dispatch location related info */  
        $('#accepctform').modal({
            show: true,
        });
        $('body').on('click', '#btnApprove', function(e){ 
          $("#spinner").show();
          e.preventDefault();
          //e.stopImmediatePropagation();      
          var fd = new FormData();
          //var form=$("#myForm");
          var id = $('#approveproformaid').val();
          var dispatch_id = $('select[id="dispatch_id"]').val();         
          var clntpo = $("#clientpo").val();
          var clientpodate = $("#clientpodate").val();
          
          
          if($("#poupload").val() != ''){
            var file = $(document).find('input[type="file"]');
            var individual_file = file[0].files[0];
            fd.append("file", individual_file); 
            fd.append("caption_avatar", individual_file.name); 
          }
  
          fd.append("id", id);  

          if(dispatch_id != ''){
          fd.append("dispatch_id", dispatch_id);    
          }else{
            alert('Please select dispatch code');
            return false;
          }
          
          fd.append("dispatchcode", $("#dispatchcode").val());  
          
          if(clntpo != ''){
            fd.append("clientpo", clntpo);    
          }else{
            alert('Please enter client po');
            return false;
          } 

          if(clientpodate != ''){
            fd.append("clientpodate", clientpodate);    
          }else{
            alert('Please enter Client PO Date');
            return false;
          }          
          
          fd.append("acceptcomment", $("#acceptcomment").val());  
          
          $.ajax({
            url: '<?php echo WEBSITE_PATH;?>proformainvoices/approveproformainvoice/',
            type: 'POST', 
            data:fd,
            mimeType:"multipart/form-data",
            dataType: "json",
            contentType: false,
            cache: false,
            processData:false,
            success: function (data) {
              if(data == 1){
              $("#spinner").hide();
              $('.modal').modal('hide');
              $(".modal .close").click();
              $('.modal-body').find('select').val('');
              $('.modal-body').find('textarea,input').val('');         
              accept.parent().html('<span class="label label-sm label-success">Accepted</span>');
            }
            }
          });         
        });
    });
    /*End Click Event of 'Accept' Button for accept Proforma Invoice*/   

    /* Start Click Event of 'Accept' Button for Reject Proformainvoice */
    $(document).on('click', '#btn_reject', function (e) {
      e.preventDefault();
      //e.stopImmediatePropagation();
      var status_id = $(this).val();
      var performaid = $(this).attr('data-pid');
      var reject = $(this);
      $("#rejectid").val(performaid);
      $('#rejectform').modal({
        show: true,
      });

      $("#btnRejectSubmit").click(function(e){ 
        e.preventDefault();
        //e.stopImmediatePropagation();
        $.ajax({
          url: '<?php echo WEBSITE_PATH;?>proformainvoices/rejectproformainvoice/',
          type: 'POST', 
          data: 'id='+$('#rejectid').val()+'&rejectcomment='+$("#rejectcomment").val(),
          cache: false,
          success: function (data) {
            //$('.modal').modal('hide');
             $(".modal .close").click();
            $('.modal-body').find('textarea,input').val('');
            reject.parent().html('<span class="label label-sm label-danger test">Rejected</span>');
          }
        }); 
      });
    });
    /* End Click Event of 'Accept' Button for Reject Proformainvoice */
  });

  $(document).on('change', '.dispatch', function (e) {
    var location = $(this).val();
    $.ajax({
      url: '<?php echo WEBSITE_PATH;?>proformainvoices/getDispatchcode/'+location,
      type: 'POST',
      cache: false,
      success: function (data) {
        $('#dispatchcode').val(data.trim());
      }
    });
  });

  $(document).ready(function() {
       $('#ProformainvoiceIndexForm').validate({
    rules: {
        dispatchcode: {
            minlength: 2,
            required: true
        },
        clientpo: {
            required: true,
        },
        message: {
            minlength: 2,
            required: true
        }
    },
    highlight: function (element) {
        $(element).closest('.control-group').removeClass('success').addClass('error');
    },
    success: function (element) {
        element.text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
    }

    });
});

</script>