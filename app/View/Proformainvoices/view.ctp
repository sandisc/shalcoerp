<div class="pull-left"><h3 class="page-title"> <?php echo $mainTitle;?></h3></div>
  <div class="pull-right">
<!--        <a id="printrow" data-controllername="proformainvoices" data-actionname="prints" data-id ="<?php echo base64_encode($data['Proformainvoice']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints dt-button buttons-print btn dark btn-outline" title="Print Invoice" target="_blank"><span class="glyphicon glyphicon-print"></span> Print 
        </a> -->
      <?php  
        /*Set Edit link*/
      if(in_array('proformainvoices/edit',$this->Session->read('accesscontrollers_actions'))){      
        echo $this->Html->link(
           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
            array(
                'controller'=>'proformainvoices',
                'action'=>'edit',
                base64_encode($data['Proformainvoice']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Edit',
                'title' => 'Edit',
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );        
      }
        /*Set PDF Download link*/
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> PDF',
            array(
                'controller'=>'proformainvoices',
                'action'=>'generatepdf',
                'ext' => 'pdf',
                $data['Proformainvoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'PDF',
                'title' => 'PDF',
                'download'=>'Proformainvoice-'.$data['Proformainvoice']['proforma_no'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
  
      /* MS word output */
        /*echo $this->Html->link(
           '<i class="fa fa-file-word-o" aria-hidden="true"></i> DOC',
            array(
                'controller'=>'proformainvoices',
                'action'=>'printoword',
                base64_encode($data['Proformainvoice']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'DOC',
                'title' => 'DOC',
                'download'=>'Proformainvoice-'.$data['Proformainvoice']['proforma_no'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );*/
      
        /* MS Excel Output */
         echo $this->Html->link(
         '<i class="fa fa-file-excel-o"></i> Excel',
          array(
              'controller'=>'proformainvoices',
              'action'=>'export',
              $data['Proformainvoice']['id']
          ),
          array(
              'rel'=>'tooltip',
              'title' => 'Download',
              'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
              'escape'=>false  //NOTICE THIS LINE ***************
          )   
      );  
             
      ?>
  </div>

  <div class="clearfix"></div>
  <?php  $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>             
                      <div id="tab_3" class="tab-pane active">
                        <div class="portlet box blue">                                      
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?> </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body form">                                        
                                <div class="form-body portlet-body flip-scroll">                                       
                                    <table id="proforma_table" width="100%"  class="table table-bordered table-striped table-condensed flip-content view_invoice">
                                      <tbody>
                                        <tr>
                                            <td colspan="10" align="center"><b>PROFORMAINVOICE</b> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1"> <b>Office <br>Address</b> </td>
                                            <td colspan="3"><?php echo nl2br($setting['Setting']['office_address']); ?>
                                            </td>
                                            <td></td><!-- Blank td -->
                                            <td colspan="1"> <b>PROFORMA NO.</b> <br>
                                              <?php echo h($data['Proformainvoice']['proforma_no']); ?>
                                            </td>

                                            <td colspan="4"><b>Date</b> <br>
                                                <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Proformainvoice']['proforma_date'])); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" colspan="1"><b> Factory <br>Address</b> </td>
                                            <td rowspan="2" colspan="3" ><?php echo nl2br($setting["Setting"]["factory_address"]); ?></td>
                                            <td rowspan="2"></td><!-- Blank td -->
                                            <td colspan="1"><b>Shipping Terms</b></td>
                                            <td colspan="4"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Proformainvoice']['delivery_type']).nl2br($data['Proformainvoice']['tod']); ?></td>
                                              <tr>
                                              <!-- <td></td> --><!-- Blank td -->
                                                <td colspan="1"><b>Terms Of Payment</b></td>
                                                <td colspan="4"><?php echo nl2br($data['Proformainvoice']['top']); ?></td>
                                              </tr>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><b>Bill To </b></td>
                                            <td></td><!-- Blank td -->
                                            <td colspan="5"><b>Ship To</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><b><?php echo nl2br($data['Client']['company_name']); ?></b><br>
                                            <?php echo searchFromAddress($data['Proformainvoice']['billaddress_id'], $data['Client']['Address']); ?></td>
                                            <td></td><!-- Blank td -->
                                            <td colspan="5"><b><?php echo nl2br($data['Proformainvoice']['ship_to']); ?></b><b><br></b><?php echo searchFromAddress($data['Proformainvoice']['shipaddress_id'], $data['Client']['Address']); ?></td>
                                        </tr>                                                      
                                    <?php 
                                      $proformaitems= '';
                                      $i = 1;
                                      foreach($data['Proformaitem'] as $value) {
                                        $proformaitems.= '<tr class="addmore">    
                                            <td>'.$value['sr_no'].'</td>
                                            <td>'.$value['Productcategory']['productname'].'</td>
                                            <td>'.$value['Productcategory']['Hscode']['hscode'].'</td>
                                            <td>'.$value['Standard']['standard_name'].'</td>
                                            <td>'.$value['Grade']['grade_name'].'</td>
                                            <td style="white-space:nowrap;">'.setGDmmformat($value['Size']['gdmm'],$value['length']).'</td>
                                            <td style="white-space:nowrap;">'.setGDnbformat($value['Size']['gdnb'],$value['length']).'</td>
                                            <td class="numalign">'.$value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'].'</td>
                                            <td class="numalign">'.$value['price'].'</td>
                                            <td class="numalign">'.$value['amount'].'</td>
                                        </tr>';                                            
                                        $i++; } ?>                                        
                                        <tr>
                                            <td width="6%" align="center"><b>Sr.<br> No.</b></th>
                                            <td width="14%" align="center"><b>Product Name</b></th>
                                            <td width="10%" align="center"><b>HS Code</b></th>
                                            <td width="10%" align="center"><b>Standard</b></th>
                                            <td width="10%" align="center"><b>Grade</b></th>
                                            <td width="20%" colspan="2" align="center"><b>Good Description</b></th>
                                            <td width="10%" align="center"><b>QTY</b></th>
                                            <td width="10%" align="center"><b>Unit Price <?php echo '('.$data['Price']['sign'].')';?><br></b></th>
                                            <td width="10%" align="center"><b>Amount<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
                                        </tr>                                      
                                    
                                      <?php echo $proformaitems; ?>
                                      <tr>    
                                          <td colspan="7" ></td>
                                          <td colspan="2" class="numalign"><b>Total <?php echo $data['Price']['fullform'].'('.$data['Price']['sign'].')';?></b></td>
                                          <td class="numalign"><b><?php echo $data['Proformainvoice']['total'];?></b></td>
                                      </tr>
                                    <!-- New Fields -->
                                      <tr>
                                          <td colspan="2"><b>Quantity Tolerance</b></td>
                                          <td colspan="8"><?php echo h($data['Proformainvoice']['quantity']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Delivery Period</b></td>
                                          <td colspan="8"><?php echo h($data['Proformainvoice']['delivery_period']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Dimensions</b></td>
                                          <td colspan="8"><?php echo h($data['Proformainvoice']['dimensions']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Certification</b></td>
                                          <td colspan="8"><?php echo h($data['Proformainvoice']['certifications']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Comment/Notes</b></td>
                                          <td colspan="8"><?php echo h($data['Proformainvoice']['comment']); ?></td>
                                      </tr>
                                      <!-- -->
                                      <tr>
                                          <td colspan="10"><b><u>Bank Details</u></b></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Bank Name</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['bank_name']); ?></td>
                                          <td colspan="3" rowspan="9"><b>For, SHALCO IND. PVT. LTD. <br/> DIRECTOR</b></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Bank Alias</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['bank_alias']); ?></td>
                                           
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Branch Name</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['branch_name']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Swift Code</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['swift_code']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>AD Code</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['ad_code']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>Beneficiary’s A/C No</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['ac_no']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>IEC Code</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['iec_code']); ?></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2"><b>IFC Code</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['ifc_code']); ?></td>
                                      </tr>                                                                                        
                                      <tr>
                                          <td colspan="2"><b>Beneficiary’s Name</b></td>
                                          <td colspan="5"><?php echo h($data['Bank']['ac_name']); ?></td>
                                      </tr>
                              <?php if($data['Proformainvoice']['status'] == 1){ ?>
                                      <tr class="print_none">
                                        <td colspan="2"><b>Order Status</b></td>
                                        <td colspan="8">Accepted</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Dispatch Location</b></td>
                                        <td colspan="8"><?php echo $data['Dispatch']['location']?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Client P.O</b></td>
                                        <td colspan="8"><?php echo $data['Proformainvoice']['clientpo'];?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Client P.O. Date</b></td>
                                        <td colspan="10"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Proformainvoice']['clientpodate'])); ?></td>
                                      </tr>                                      
                                      <tr>
                                        <td colspan="2"><b>Dispatch code</b></td>
                                        <td colspan="8"><?php echo $data['Proformainvoice']['dispatchcode']?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Description</b></td>
                                        <td colspan="8"><?php echo nl2br($data['Proformainvoice']['acceptcomment']);?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Accepted by</b></td>
                                        <td colspan="8"><?php echo $this->requestAction('Users/getUsername/'.$data['Proformainvoice']['approvalby'])?></td>
                                      </tr>                                           
                                      <tr>
                                        <td colspan="2"><b>Accepted Date</b></td>
                                        <td colspan="8"><?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($data['Proformainvoice']['approval_date']))?></td>
                                     </tr>
                                 <?php } 
                                  if($data['Proformainvoice']['status'] == 0){ ?>
                                      <tr class="print_none">
                                        <td colspan="2"><b>Order Status</b></td>
                                        <td colspan="8"><?php echo "Pending"; ?></td>
                                      </tr>
                                  <?php } 
                                  if($data['Proformainvoice']['status'] == 2){ ?>
                                      <tr class="print_none">
                                        <td colspan="2"><b>Order Status</b></td>
                                        <td colspan="8"><?php echo "Rejected";?></b></td>
                                      </tr>                                         
                                      <tr class="print_none">
                                        <td colspan="2"><b>Reject reason</b></td>
                                        <td colspan="8"><?php echo nl2br($data['Proformainvoice']['rejectcomment']); ?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Reject Date</b></td>
                                        <td colspan="8"><?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($data['Proformainvoice']['reject_date']))?></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><b>Reject By</b></td>
                                        <td colspan="8"><?php echo $this->requestAction('Users/getUsername/'.$data['Proformainvoice']['rejectby'])?></td>
                                      </tr>	
                                <?php } ?>
                                </tbody>
                              </table> 
                                
                              <div class="form-actions">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="row">
                                      <div class="col-md-offset-3 col-md-9">
                                        <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'proformainvoices'),array('class' => 'btn default')); ?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6"> </div>
                                </div>
                              </div>

                            </div>
                          </div>
                      </div>