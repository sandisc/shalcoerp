<?php  $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>             
                <div id="tab_3" class="tab-pane active">
                  <div class="portlet box">
                      
                      <div class="portlet-title">
                          <div class="caption"><?php echo $pageTitle;?> </div>
                      </div>

                      <div class="portlet-body form">                          
                        <div class="form-body portlet-body flip-scroll">
                            <?php header('content-type: image/png');  ob_start();?>

                            <div><?php echo $this->requestAction('App/getURI/');?></div>
                            <div class="" style="text-align:center;font-family:'Tahoma';font-size:14px;"><b>PROFORMAINVOICE</b></div>                        
                          <table id="proforma_table" width="100%"  class="table table-bordered table-striped table-condensed flip-content view_invoice">
                            <!--<tr>
                                <td colspan="9" align="center">PROFORMAINVOICE</td>
                              </tr> -->

                              <tr>
                                <td colspan="1"><b> Office Address </b></td>
                                <td colspan="3"><?php echo nl2br($setting['Setting']['office_address']); ?></td>
                                <td></td><!-- Blank td -->
                                <td colspan="1"> <b>PROFORMA NO.</b> <br><?php echo h($data['Proformainvoice']['proforma_no']); ?></td>
                                <td colspan="4"><b>Date </b><br><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Proformainvoice']['proforma_date'])); ?></td>
                              </tr>

                              <tr>
                                <td rowspan="2" colspan="1"><b> Factory Address </b></td>
                                <td rowspan="2" colspan="3"><?php echo nl2br($setting["Setting"]["factory_address"]); ?></td>
                                <td></td><!-- Blank td -->
                                <td colspan="1"><b>Shipping Terms</b></td>
                                <td colspan="4"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Proformainvoice']['delivery_type']).nl2br($data['Proformainvoice']['tod']); ?></td>
                                  <tr>
                                  <td></td><!-- Blank td -->
                                    <td colspan="1"><b>Terms Of Payment</b></td>
                                    <td colspan="4"><?php echo nl2br($data['Proformainvoice']['top']); ?></td>
                                  </tr>
                                </td>
                              </tr>
                              
                              <tr>
                                <td colspan="4"><b>Bill To </b></td>
                                <td></td><!-- Blank td -->
                                <td colspan="5"><b>Ship To</b></td>
                              </tr>
                              
                              <tr>
                                <td colspan="4"><b><?php echo nl2br($data['Client']['company_name']); ?></b><br><?php echo searchFromAddress($data['Proformainvoice']['billaddress_id'], $data['Client']['Address']); ?></td>
                                <td></td><!-- Blank td -->
                                <td colspan="5"><b><?php echo nl2br($data['Proformainvoice']['ship_to']); ?></b><b><br></b><?php echo searchFromAddress($data['Proformainvoice']['shipaddress_id'], $data['Client']['Address']); ?></td>
                              </tr>                                                      
                              
                              <tr>
                                <td width="5%" align="center"><b>Sr. No.</b></th>
                                <td width="10%" align="center"><b>Product Name</b></th>
                                <td width="10%" align="center"><b>HScode</b></th>
                                <td width="10%" align="center"><b>Standard</b></th>
                                <td width="10%" align="center"><b>Grade</b></th>
                                <td width="25%" colspan="2" align="center"><b>Good Description</b></th>
                                <td width="10%" align="center"><b>QTY</b></th>
                                <td width="10%" align="center"><b>Price <?php echo '('.$data['Price']['sign'].')';?>/<br>MTR </b></th>
                                <td width="10%" align="center"><b>Amount <?php echo '('.$data['Price']['sign'].')';?></b></th>
                              </tr>

                          <tbody>
                            <?php                                 
                            foreach($data['Proformaitem'] as $value) {  ?>
                                <tr class="addmore">
                                    <td><?php echo $value['sr_no'];?></td>
                                    <td><?php echo $value['Productcategory']['productname'];?></td>
                                    <td><?php echo $value['Productcategory']['Hscode']['hscode'];?></td>
                                    <td><?php echo $value['Standard']['standard_name'];?></td>
                                    <td><?php echo $value['Grade']['grade_name'];?></td>
                                    <td style="white-space:nowrap;"><?php echo setGDmmformat($value['Size']['gdmm'],$value['length']);?></td>
                                    <td style="white-space:nowrap;"><?php echo setGDnbformat($value['Size']['gdnb'],$value['length']);?></td>
                                    <td class="numalign"><?php echo $value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                                    <td class="numalign"><?php echo $value['price'];?></td>
                                    <td class="numalign"><?php echo $value['amount'];?></td>
                                </tr>
                           <?php  } ?>

                              <tr>    
                                  <td colspan="7"></td>
                                  <td colspan="2" class="numalign"><b>Total <?php echo $data['Price']['fullform'].'('.$data['Price']['sign'].')';?></b></td>
                                  <td class="numalign"><b><?php echo $data['Proformainvoice']['total']?></b></td>
                              </tr>

                                  <!-- New Fields -->
                                <tr>
                                      <td colspan="2"><b>Quantity Tolerance</b></td>
                                      <td colspan="8"><?php echo h($data['Proformainvoice']['quantity']); ?></td>
                                </tr>
                                <tr>
                                      <td colspan="2"><b>Delivery Period</b></td>
                                      <td colspan="8"><?php echo h($data['Proformainvoice']['delivery_period']); ?></td>
                                </tr>
                                <tr>
                                      <td colspan="2"><b>Dimensions</b></td>
                                      <td colspan="8"><?php echo h($data['Proformainvoice']['dimensions']); ?></td>
                                </tr>
                                <tr>
                                      <td colspan="2"><b>Certification</b></td>
                                      <td colspan="8"><?php echo h($data['Proformainvoice']['certifications']); ?></td>
                                </tr>
                                <tr>
                                      <td colspan="2"><b>Comment</b></td>
                                      <td colspan="8"><?php echo h($data['Proformainvoice']['comment']); ?></td>
                                </tr>
                                <!-- -->
                                            
                              <tr>
                                  <td colspan="10"><b><u>Bank Details</u></b></td>
                              </tr>
                              
                              <tr>
                                  <td colspan="2"><b>Bank Name</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['bank_name']); ?></td>
                                  <td colspan="3" rowspan="10"><b>For, SHALCO IND. PVT. LTD. <br/>DIRECTOR<br/></b><br/><img src="<?php echo ROOT_PATH.'uploads/signature/'.$setting['Setting']['signature'];?>" width="200"></td>
                              </tr>
                              <tr>
                                  <td colspan="2"><b>Bank Alias </b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['bank_alias']); ?></td>
                              </tr>
                              
                              <tr>
                                  <td colspan="2"><b>Branch Name</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['branch_name']); ?></td>
                              </tr>
                              <tr>
                                  <td colspan="2"><b>Swift Code</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['swift_code']); ?></td>                                  
                              </tr>
                              
                              <tr>
                                  <td colspan="2"><b>AD Code</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['ad_code']); ?></td>
                              </tr>
                              
                              <tr>
                                  <td colspan="2"><b>Beneficiary’s A/C No</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['ac_no']); ?></td>
                              </tr>
                              
                              <tr>
                                  <td colspan="2"><b>Beneficiary’s Name</b></td>
                                  <td colspan="5"><?php echo h($data['Bank']['ac_name']); ?></td>
                              </tr>

                              <tr>
                                <td colspan="2"><b>IEC Code</b></td>
                                <td colspan="5"><?php echo $data["Bank"]["iec_code"];?></td>
                              </tr>

                              <tr>
                                <td colspan="2"><b>IFC Code </b></td>
                                <td colspan="5"><?php echo $data["Bank"]["ifc_code"]; ?></td>
                              </tr>
                          </tbody>
                        </table>
                      </div>  
                    </div>
                  </div>
              </div>