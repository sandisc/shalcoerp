<?php
  $setting= $this->Session->read('setting_data'); /*Read setting table's data*/
  
  $html = '<style>
		table {float: none;margin: 0 auto;width: 100%;border-collapse: collapse;border-spacing: 0;}
		.flip-scroll table, .table td .img-responsive {width: 100%;}
		.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
	  		border: 1px solid #e7ecf1
		}
	  	.table {margin-bottom: 20px;width: 100%;}
		</style>';

	$html .= '<html>
			  <head>
			  		<title>Proforma Email</title>
			  </head>
			  <body>
				  <table id="proforma_table" width="100%" border="1" class="table table-bordered table-striped table-condensed flip-content view_invoice">
					 <tbody> 
           <tr>
              <td colspan="10">'.$this->requestAction('App/getURI/').'</td>
          </tr>
		 			  <tr>
                <td colspan="8"><b>PROFORMA INVOICE</b> </td>
            </tr>
            <tr>
                <td colspan="2"> <b>Office Address </b></td>
                <td colspan="2">'.nl2br($setting["Setting"]["office_address"]).'</td>
        				<td colspan="2"> <b>PROFORMA NO. </b><br>'.$data["Proformainvoice"]["proforma_no"].'</td>
        				<td colspan="2"><b>Date </b><br>'.$data["Proformainvoice"]["proforma_date"].'</td>
            </tr>
            <tr>
                <td rowspan="2" colspan="2"><b> Factory Address</b> </td>
                <td rowspan="2" colspan="2" >'.nl2br($setting["Setting"]["factory_address"]).'</td>
                <td colspan="2"><b>Shipping Terms</b></td>
                <td colspan="2">'.$this->requestAction('App/get_delivery_type/'.$data["Proformainvoice"]["delivery_type"]).
                                   nl2br($data["Proformainvoice"]["tod"]).'</td>
            		<tr>
                    <td colspan="2"><b>Terms Of Payment</b></td>
                    <td colspan="2">'.nl2br($data["Proformainvoice"]["top"]).'</td>
           		  </tr>
                </td>
            </tr>
            <tr>
                <td colspan="4"><b>Bill To </b></td>
                <td colspan="4"><b>Ship To</b></td>
            </tr>
            <tr>
                <td colspan="4"><b>'.$data["Client"]["company_name"].'</b><br>'.searchFromAddress($data['Proformainvoice']['billaddress_id'], $data['Client']['Address']).'</td>
                <td colspan="4"><b>'.$data["Proformainvoice"]["ship_to"].'</b><br>'.searchFromAddress($data['Proformainvoice']['shipaddress_id'], $data['Client']['Address']).'</td>
            </tr>
            <tr>
            	<td width="5%" align="center"><b>Sr. No.</b></th>
            	<td width="10%" align="center"><b>Product Name</b></th>
              <td width="10%" align="center"><b>HS Code</b></th>
              <td width="10%" align="center"><b>Standard</b></th>
              <td width="10%" align="center"><b>Grade</b></th>
              <td width="20%" colspan="2" align="center"><b>Good Description</b></th>
              <td width="10%" align="center"><b>QTY</b></th>
              <td width="10%" align="center"><b>Unit Price ('.$data['Price']['sign'].') </b></th>
              <td width="10%" align="center"><b>Amount<br>('.$data['Price']['sign'].')</b></th>
            </tr>';
            
            $i = 1;
            foreach($data["Proformaitem"] as $value) {  
          	  $html .= '<tr class="addmore">';
                $html .= '<td>'.$i.'</td>
                <td>'.$value["Productcategory"]["productname"].'</td>
                <td>'.$value["Productcategory"]["Hscode"]["hscode"].'</td>
                <td>'.$value["Standard"]["standard_name"].'</td>
                <td>'.$value["Grade"]["grade_name"].'</td>
                <td style="white-space:nowrap;">'.setGDmmformat($value['Size']['gdmm'],$value['length']).'</td>
                <td style="white-space:nowrap;">'.setGDnbformat($value['Size']['gdnb'],$value['length']).'</td>
                <td align="right" style="padding-right:2px!important;">'.$value["qty"].' '.$value['Productcategory']['Producttaxonomy']['unit'].'</td>
                <td align="right" style="padding-right:2px!important;">'.$value["price"].'</td>
                <td align="right" style="padding-right:2px!important;">'.$value["amount"].'</td>
              </tr>';
              $i++;
            } 

  $html .= '<tr>    
        		<td colspan="7"></td>
                <td align="right" style="padding-right:2px!important;"><b>Total</b></td>
                <td align="right" style="padding-right:2px!important;"><b>'.$data["Proformainvoice"]["total"].'</b></td>
            </tr>         
                    <tr>
                        <td colspan="2"><b>Quantity Tolerance</b></td>
                        <td colspan="7">'.$data['Proformainvoice']['quantity'].'</td>
                  </tr>
                  <tr>
                         <td colspan="2"><b>Delivery Period</b></td>
                        <td colspan="7">'.$data['Proformainvoice']['delivery_period'].'</td>
                  </tr>
                  <tr>
                         <td colspan="2"><b>Dimensions</b></td>
                        <td colspan="7">'.$data['Proformainvoice']['dimensions'].'</td>
                  </tr>
                  <tr>
                        <td colspan="2"><b>Certification</b></td>
                        <td colspan="7">'.$data['Proformainvoice']['certifications'].'</td>
                  </tr>
                  <tr>
                        <td colspan="2"><b>Comment</b></td>
                        <td colspan="7">'.$data['Proformainvoice']['comment'].'</td>
                  </tr>
                  <!-- -->
            <tr>
                <td colspan="7"><b><u>Bank Details</u></b></td>
            </tr>
            <tr>
                <td colspan="2">Bank Name:</td>
                <td colspan="3">'.$data["Bank"]["bank_name"].'</td>
                <td colspan="3">For, SHALCO IND. PVT. LTD. </td>
            </tr>
            <tr>
                <td colspan="2">Branch Name:</td>
                <td colspan="3">'.$data["Bank"]["branch_name"].'</td>
                    <td colspan="3">Director</td>
            </tr>
            <tr>
                <td colspan="2">Swift Code:</td>
                <td colspan="3">'.$data["Bank"]["swift_code"].'</td>
            </tr>
            <tr>
                <td colspan="2">AD Code: </td>
                <td colspan="3">'.$data["Bank"]["ad_code"].'</td>
            </tr>
            <tr>
                <td colspan="2">Beneficiary’s A/C No:</td>
                <td colspan="3">'.$data["Bank"]["ac_no"].'</td>
            </tr>
            <tr>
                <td colspan="2">Beneficiary’s Name:</td>
                <td colspan="3">'.$data["Bank"]["ac_name"].'</td>
            </tr>
            <tr>
                <td colspan="2">IEC Code</td>
                <td colspan="8">'.$data["Bank"]["iec_code"].'</td>
                
            </tr>
            <tr>
                <td colspan="2">IFC Code </td>
                <td colspan="8">'.$data["Bank"]["ifc_code"].'</td>
            </tr>
           </tbody>
       	</table>
      </body>
    </html>';
  echo $html;
?>