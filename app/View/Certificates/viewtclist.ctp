<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
      <div class="form-body metroform"> 
        
        <div class="row separatordiv">
          <div class="col-md-6">
            <div class="col-lg-12">
              <label class="control-label">Order No.</label>
              <div class="input text"><?php echo $orders['Order']['orderno']?></div>
              <span class="help-block"><br></span>
            </div>                   
          </div>                
          <div class="col-md-6">                     
            <label class="control-label">Client</label>
            <div class="input text"><?php echo $orders['Client']['company_name']?></div>
            <span class="help-block"><br></span>            
          </div>
        </div>  <!--/row-->

        <?php 
          $generated = array(); /*For Save Generated orderitem and Certi item records*/
          $pendingtc = ''; /*For Save pending order item records*/
          $certinumber = ''; /*certificate number of orderitem to compare it with next orderitem and make group of it if both have same certificate number  */
          foreach($orders['Orderitem'] as $key=>$item){
            /*Check Cerificate generated or not*/
            if(!empty($item['Certificateitem'])){
              $ccno = $item['Certificateitem']['Certificate']['certificateno'];
              $generated[$ccno][$key]['sr_no'] = $item['sr_no'];
              $generated[$ccno][$key]['productname'] = $item['productname'];
              $generated[$ccno][$key]['qty'] = $item['qty'];
              $generated[$ccno][$key]['heatno'] = $item['Certificateitem']['heatno'];
              $generated[$ccno][$key]['clientheatno'] = $item['Certificateitem']['clientheatno'];      
              $generated[$ccno][$key]['standard'] = $item['Standard']['standard_name'];
              $generated[$ccno][$key]['grade'] = $item['Grade']['grade_name'];
              $generated[$ccno][$key]['certificateno'] = $item['Certificateitem']['Certificate']['certificateno'];
              $generated[$ccno][$key]['certificateid'] = $item['Certificateitem']['Certificate']['id'];
            }
            else{
              $pendingtc.= '<tr><td>'.$item['sr_no'].'</td><td>'.$item['Standard']['standard_name'].'</td><td>'.$item['Grade']['grade_name'].'</td><td>'.$item['productname'].'</td><td>'.$item['qty'].'</td></tr>';
            }
          }
        $generatedtc = '';
          foreach ($generated as $key => $value) {
            $sametc = count($generated[$key]);/*Count TC which have same TC number*/
            $i=1;
            $jointcell = '';
            foreach ($generated[$key] as $value) {
              $generatedtc .= '<tr>';
              if($sametc != 0){ /*If TC has only one TC item than*/
                $jointcell = "rowspan='".$sametc."' style='vertical-align:middle!important;'";
              }
              if($i==1){  /*For first 1st TC item than*/
                $generatedtc .= '<td rowspan='.$sametc.' style="vertical-align:middle!important;">'.$value['certificateno'].'</td>
                <td '.$jointcell.'>'.$value['standard'].'</td>
                <td '.$jointcell.'>'.$value['grade'].'</td>';
              }
              $generatedtc .= '<td>'.$value['sr_no'].'</td>
                  <td>'.$value['productname'].'</td>
                  <td>'.$value['heatno'].'</td>
                  <td>'.$value['clientheatno'].'</td>
                  <td>'.$value['qty'].'</td>';
              if($i==1){  /*For first 1st TC item than*/
                $generatedtc .= '<td '.$jointcell.'><a href="'.WEBSITE_PATH.'certificates/view/'.base64_encode($value['certificateid']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View Certificate"></span></a>
                    <a href="'.WEBSITE_PATH.'certificates/edit/'.base64_encode($value['certificateid']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit Certificate"></span></a></td>';
              }
              $generatedtc .= '</tr>';
              $i++;
            } 
          }
        ?>
         <!-- Start Of Generated Test Certificate HTML -->
         <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
            <thead class="flip-content portlet box green">
              <tr>
                <th width="10%">Certificate No</th>
                <th width="10%">STANDARD</th>
                <th width="10%">GRADE</th>
                <th width="10%">P.O. SR No.</th>
                <th width="20%">Product Name</th>
                <th width="10%">Heat No</th>
                <th width="10%">Client Heat No</th>
                <th width="10%">Qty</th>
                <th width="20%">Action</th>                        
              </tr>                                          
            </thead>
            <tbody>
              <?php echo $generatedtc; ?>   
            </tbody>
          </table>
          <!-- End Of Generated Test Certificate HTML -->

          <!-- Start Of Pending Order Item which Test Certificate is pending HTML -->
          <?php if($pendingtc){?>
          <h4>Pending Order Items for generate Test Certificate</h4>
          <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
            <thead class="flip-content portlet box green">
              <tr>
                <th width="10%">P.O. SR No.</th>
                <th width="10%">STANDARD</th>
                <th width="10%">GRADE</th>
                <th width="20%">Product Name</th>
                <th width="50%">Qty</th>
              </tr>                                          
            </thead>
            <tbody>
              <?php echo $pendingtc; ?>   
            </tbody>
          </table>
          <!-- End Of Pending Order Item which Test Certificate is pending HTML -->
          <?}?>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'certificates'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>