<?php
ini_set('memory_limit', '4092M');
require_once(ROOT . DS . 'app' . DS . 'Vendor' . DS . 'tcpdf' . DS . "certificatepdf.php");

$tcpdf = new XTCPDF();
$textfont = 'arial'; // looks better, finer, and more condensed than 'dejavusans'
//$tcpdf->SetPrintHeader(false);
$tcpdf->SetAuthor("KBS Homes & Properties at http://kbs-properties.com");
//$tcpdf->SetAutoPageBreak( false );

$tcpdf->SetLeftMargin(5);
$tcpdf->SetTopMargin(10);
$tcpdf->SetRightMargin(5);
//$tcpdf->SetMargins(4, 4, 4);
//
//add a page (required with recent versions of tcpdf)
$tcpdf->AddPage('L');

//$tcpdf->SetFont('Arial', 8, 8);
$tcpdf->SetFont('helvetica', '', 10);

$htm = $this->render('certificatecontain', false);

//echo $htm; exit;
$tcpdf->writeHTML($htm, true, false, false, false, '');

// see the TCPDF examples 
ob_end_clean(); //exit;
echo $tcpdf->Output('Receipt.pdf', 'FI');
//ob_end_clean();
?>