<?php
$setting= $this->Session->read('setting_data'); /*Read setting table's data*/
if ($certi['Certificate']['reftype'] == 1) {
    $itemmodel = 'Chalanitem';
    $mainmodel = 'Chalan';
} else {
    $itemmodel = 'Invoiceitem';
    $mainmodel = 'Invoice';
}
$itemcount = count($certi['Certificateitem']); /* Number of item in certificate */

$grade_id = $certi['Certificateitem'][0][$itemmodel]['Orderitem']['grade_id']; /* Grade id of first item */
$chem_req = $this->requestAction('Grades/getchemical/' . $grade_id);
$chem_comp = ''; /* Chemical component */
$chem_min = ''; /* Chemical component Min value */
$chem_max = ''; /* Chemical component Max value */
$chem_comp_name = array();
//echo count($chem_req); exit;
$proprty_counter = count($chem_req);
foreach ($chem_req as $value) {
    //pr($value); exit;
    $width = 60 / $proprty_counter;
    $chem_comp_name[] = $value[0];
    $chem_comp .= '<td width="' . $width . '%" align="center" ><b>' . ucfirst($value[0]) . '</b></td>';
    $chem_min .= '<td width="' . $width . '%" align="center" >' . $value[1] . '</td>';
    $chem_max .= '<td width="' . $width . '%" align="center" >' . $value[2] . '</td>';
}
$mech_req = $this->requestAction('Grades/getmechanical/' . $grade_id);
$phy_req = $this->requestAction('Grades/getphysical/' . $grade_id);
$ord = '';
$chem = ''; /* Chemical value measured of particular item */
$mech = ''; /* Physical value measured of particular item */
$product_detail = ''; /* Product name,size etc of item */

foreach ($certi['Certificateitem'] as $item) {
    $keyid = $item['id'];
    $ord.= $this->Form->input('id', array('class' => 'form-control', 'type' => 'hidden', 'label' => false, 'required' => true, 'value' => $item['id'], 'name' => 'data[Certificate][Certificateitem][' . $keyid . '][id]'));

    $product_detail .= '<tr align="center"><td class="text-center">' . $item[$itemmodel]['Orderitem']['sr_no'] . '</td><td width="40%" class="text-center">' . $item[$itemmodel]['Orderitem']['Productcategory']['productname'] . '</td><td width="40%" class="text-center">' . $this->requestAction('Sizes/getsizedimension/' . $item[$itemmodel]['Orderitem']['size_id'] . '/' . $item[$itemmodel]['Orderitem']['length'] . '/2') . '</td></tr>';

    $ord.= '<td width="10%" align="cgenter">' . $item[$itemmodel]['Orderitem']['sr_no'] . '</td>';
    $ord.= '<td width="9%">' . $item['heatno'] . '</td>';
    $ord.= '<td width="9%">' . $item['clientheatno'] . '</td>';
    $ord.= '<td width="9%">' . $item['testref_no'] . '</td>';
    $ord.= '<td width="9%">' . $item['surface_finish'] . '</td>';
    $ord.= '<td width="11%">' . $item['dimension'] . '</td>';
    $ord.= '<td width="11%">' . $item[$itemmodel]['Orderitem']['length'] . '</td>';
    $ord.= '<td width="11%">' . $item[$itemmodel]['qty_mtr'] . '</td>';
    $ord.= '<td width="11%">' . $item[$itemmodel]['netweight'] . '</td>';
    //$ord.= '</tr>';

    $chem.= '<tr align="center">';
    $chem.= '<td>' . $item['heatno'] . '</td>';
    $chem.= '<td>Obeserved</td>';
    foreach ($chem_comp_name as $val) {
        $chem.= '<td>' . $item[$val] . '</td>';
    }
    $chem.= '</tr>';

    $mech.= '<td>' . $item['heatno'] . '</td>';
    $mech.= '<td>' . $item['tensile_strength'] . '</td>';
    $mech.= '<td>' . $item['grain_size'] . '</td>';
    $mech.= '<td>' . $item['yield_strength'] . '</td>';
    $mech.= '<td>' . $item['elongation'] . '</td>';
    $mech.= '<td>' . $item['flattening'] . '</td>';
    $mech.= '<td>' . $item['flaring'] . '</td>';
    $mech.= '<td>' . $item['eddy'] . '</td>';
    $mech.= '<td>' . $item['ultrasonic'] . '</td>';
    $mech.= '<td>' . $item['hardness'] . '</td>';
    $mech.= '<td>' . $item['hydro'] . '</td>';
    $mech.= '<td>' . $item['igc'] . '</td>';
    //$mech.= '</tr>';
}
?>
<table width="100%" class="table" cellpadding="2">
    <tbody>  
        <tr>
            <td align="left"><img src="<?php echo ROOT_PATH ?>img/print_logo.png"></td>
            <td align="right" style="vertical-align: bottom !important;"> 
                <b>CIN NO </b><?php echo $setting["Setting"]["cin_no"]; ?><br>
                <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building<br/>Gulalwadi, Mumbai - 400004<br/> Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25<br/> Fax: +91-22-23452161<br/>
                Email: info@shalco.in Web: www.shalco.in</span>
            </td>                    
        </tr>
    </tbody>
</table>
<table border="1" width="100%" cellpadding="2" >
    <tr>
        <td width="15%" align="center"><b>Client</b></td>
        <td width="35%" align="left"><?php echo $certi[$mainmodel]['Order']['Client']['company_name']; ?></td>
        <td width="15%" align="center"><b>Contract</b></td>
        <td width="35%" align="left"><?php echo $certi['Order']['clientpo']; ?>  Dt: <?php echo date('d/m/Y',strtotime($certi['Order']['clientpodate'])); ?></td>
    </tr>
    <tr>
        <td width="15%" align="center"><b>Specification</b></td>
        <td width="35%" align="left"><?php echo $certi['Certificate']['std_spec']; ?></td>
        <td width="15%" align="center"><b>Grade</b></td>
        <td width="35%" align="left"><?php echo $certi['Certificate']['grd_spec']; ?></td>
    </tr>
    <tr align="center">
        <td width="10%"><b>P.O. Sr No</b></td>
        <td width="40%"><b>Product</b></td>
        <td width="40%"><b>Size</b></td>
    </tr>
    <?php echo $product_detail; ?>
    <tr align="center">
        <td width="10%"><b>Supply Conditions</b></td>
        <td width="10%"><b>P.O. SR. NO</b></td>
        <td width="9%"><b>Heat No</b></td>
        <td width="9%"><b>Client Heat No</b></td>
        <td width="9%"><b>Test Ref. No</b></td>
        <td width="9%"><b>Surface Finish</b></td>
        <td width="11%"><b>Dimension Check</b></td>
        <td width="11%"><b>Total Length</b></td>
        <td width="11%"><b>Total Quantity</b></td>
        <td width="11%"><b>Total Net Weight</b></td>
    </tr>
    <tr>
        <td width="10%" rowspan="<?php echo $itemcount; ?>" ><?php echo $certi['Certificate']['supply_condition']; ?></td>
        <?php echo $ord; ?>
    </tr>
    <tr align="center">
        <td width="15%"><b>Solution Annealing Temp.</b></td>
        <td width="85%" colspan="10"><b>Chemical Composition %</b></td>
    </tr>
    <tr align="center">
        <td width="15%" rowspan="<?php echo $itemcount + 3; ?>"><?php echo (isset($phy_req['Physical']['annealing_c']) && $phy_req['Physical']['annealing_c'] != '') ? $phy_req['Physical']['annealing_c'] : ''; ?></td>            
        <td width="15%"><b>Heat No</b></td>
        <td width="10%"></td>
        <?php echo $chem_comp; ?>
    </tr>
    <tr>
        <td></td>
        <td align="center"><b>Minimum</b></td>
        <?php echo $chem_min; ?>
    </tr>
    <tr>
        <td></td>
        <td align="center"><b>Maximum</b></td>
        <?php echo $chem_max; ?>
    </tr>    
    <?php echo $chem; ?>
    <tr  align="center">
        <td width="15%" colspan="2" rowspan="2"><b>Testing Item</b></td>
        <td width="8%"><b>Tensile <br/>Strength</b></td>
        <td width="8%"><b>GRAIN <br/>SIZE</b></td>
        <td width="8%"><b>Yield <br/>Strength </b></td>
        <td width="8%" rowspan="2"><b>Elongation %</b></td>
        <td width="8%" rowspan="2"><b>Flattening <br/>Test</b></td>
        <td width="8%" rowspan="2"><b>Flaring <br/>Test</b></td>
        <td width="8%" rowspan="2"><b>Eddy Current <br/>Test</b></td>
        <td width="8%" rowspan="2"><b>Ultrasonic <br/>Test</b></td>
        <td width="7%" rowspan="2"><b>Hardness <br/>HRB</b></td>
        <td width="7%" rowspan="2"><b>Hydro test<br>(Pressure) PSI</b></td>
        <td width="7%" rowspan="2"><b>IGC - Test</b></td>
    </tr>
    <tr align="center">
        <td width="8%"><b>(MPA)</b></td>
        <td width="8%"><b>Coarser</b></td>
        <td width="8%"><b>RP 0.2%</b></td>                    
    </tr>
    <tr>
        <td width="6%">Required</td>
        <th width="9%" align="center"><b><?php echo $certi['Certificate']['grd_spec']; ?></b></th>
        <td><?php echo $mech_req['Mechanical']['tensile_strength']; ?></td>
        <td><?php echo $mech_req['Mechanical']['grain_size']; ?></td>
        <td><?php echo $mech_req['Mechanical']['yield_strength']; ?></td>
        <td><?php echo $mech_req['Mechanical']['elongation']; ?></td>
        <td><?php echo $mech_req['Mechanical']['flattening']; ?></td>
        <td><?php echo $mech_req['Mechanical']['flaring']; ?></td>
        <td><?php echo $mech_req['Mechanical']['eddy']; ?></td>
        <td><?php echo $mech_req['Mechanical']['ultrasonic']; ?></td>
        <td><?php echo $mech_req['Mechanical']['hardness']; ?></td>
        <td>text 001</td>
        <td><?php echo $mech_req['Mechanical']['igc']; ?></td>
    </tr>
    <tr> 
        <?php echo '<td rowspan="' . $itemcount . '" class="v-align">Testing Data</td>'; ?>                      
        <?php echo $mech; ?>
    </tr>
    <tr>
        <td><div style="min-height: 100px;"><b>Remarks :- </b><br/><?php echo $certi['Certificate']['remarks'];?></div></td>
        <td><div style="min-height: 100px;"><br/><br/>HEAD QA/QC</div></td>        
    </tr>
</table>