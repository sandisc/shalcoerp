<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""></a></div>
    </div>
    <div class="portlet-body form">    
        <?php echo $this->Form->create('Certificate',array('accept'=>'UTF-8'));
            echo $this->Form->input('id',array('type'=>'hidden')) ;?>
                <div class="certificate form-body">
<?php
if($certi['Certificate']['reftype'] == 1){ $itemmodel = 'Chalanitem';
$mainmodel = 'Chalan';} else { $itemmodel = 'Invoiceitem'; $mainmodel = 'Invoice';}

$itemcount = count($certi['Certificateitem']); /*Number of item in certificate*/

$grade_id = $certi['Certificateitem'][0][$itemmodel]['Orderitem']['grade_id']; /*Grade id of first item*/
$chem_req = $this->requestAction('Grades/getchemical/'.$grade_id);
$chem_comp = ''; /*Chemical component*/
$chem_min = ''; /*Chemical component Min value*/
$chem_max = ''; /*Chemical component Max value*/
$chem_comp_name = array();
foreach ($chem_req as $value) {
    $chem_comp_name[] = $value[0]; 
    $chem_comp .= '<th>'.ucfirst($value[0]).'</th>';
    $chem_min .= '<td class="text-center">'.$value[1].'</td>';
    $chem_max .= '<td class="text-center">'.$value[2].'</td>';
}
$mech_req = $this->requestAction('Grades/getmechanical/'.$grade_id);
$phy_req = $this->requestAction('Grades/getphysical/'.$grade_id);

/*Set solution Annalealing Temparature with its grade solution temp */
if(empty($certi['Certificate']['required_annealing_temp'])){
    $certi['Certificate']['required_annealing_temp'] = $phy_req['Physical']['annealing_c'];
}

$ord = ''; 
$chem = ''; /*Chemical value measured of particular item*/
$mech = ''; /*Physical value measured of particular item*/
$product_detail = ''; /*Product name,size etc of item*/

foreach($certi['Certificateitem'] as $item){
    $keyid = $item['id'];
    $ord.= $this->Form->input('id', array('class'=>'form-control','type'=>'hidden','label'=>false,'required'=>true,'value'=>$item['id'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][id]'));

    $product_detail .= '<tr><td class="text-center">'.$item[$itemmodel]['Orderitem']['sr_no'].'</td><td width="35%" class="text-center">'.$item[$itemmodel]['Orderitem']['Productcategory']['productname'].'</td><td width="35%" class="text-center">'.$this->requestAction('Sizes/getsizedimension/'.$item[$itemmodel]['Orderitem']['size_id'].'/'.$item[$itemmodel]['Orderitem']['length'].'/2').'</td></tr>';
    
    $ord.= '<td class="text-center">'.$item[$itemmodel]['Orderitem']['sr_no'].'</td>';
    $ord.= '<td>'.$item['heatno'].'</td>';
    $ord.= '<td>'.$this->Form->input('clientheatno', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'clientheatno','value'=>$item['clientheatno'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][clientheatno]')).'</td>';

    $ord.= '<td>'.$this->Form->input('printheatno', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'printheatno','value'=>$item['printheatno'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][printheatno]')).'</td>'; 

    $ord.= '<td>'.$this->Form->input('testref_no', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'testref_no','value'=>$item['testref_no'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][testref_no]')).'</td>';
    $ord.= '<td>'.$this->Form->input('surface_finish', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'surface_finish','value'=>$item['surface_finish'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][surface_finish]')).'</td>';
    $ord.= '<td>'.$this->Form->input('dimension', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'dimension','value'=>$item['dimension'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][dimension]')).'</td>';
    $ord.= '<td class="numalign">'.$item[$itemmodel]['Orderitem']['length'].'</td>';
    $ord.= '<td class="numalign">'.$item[$itemmodel]['qty_mtr'].'</td>';
    $ord.= '<td class="numalign">'.$item[$itemmodel]['netweight'].'</td>';
    $ord.= '</tr>';

    $chem.= '<td>'.$item['heatno'].'</td>';
    $chem.= '<td>Observed</td>';
    foreach ($chem_comp_name as $val) {
        $chem.= '<td>'.$this->Form->input('a', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'value'=>$item[$val],'name'=>'data[Certificate][Certificateitem]['.$keyid.']['.$val.']')).'</td>';
    }    
    $chem.= '</tr>';

    $mech.= '<td>'.$item['heatno'].'</td>';
    $mech.= '<td>'.$this->Form->input('tensile_strength', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'tensile_strength','value'=>$item['tensile_strength'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][tensile_strength]')).'</td>';
    $mech.= '<td>'.$this->Form->input('grain_size', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'grain_size','value'=>$item['grain_size'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][grain_size]')).'</td>';
    $mech.= '<td>'.$this->Form->input('yield_strength', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'yield_strength','value'=>$item['yield_strength'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][yield_strength]')).'</td>';
    $mech.= '<td>'.$this->Form->input('elongation', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'elongation','value'=>$item['elongation'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][elongation]')).'</td>';
    $mech.= '<td>'.$this->Form->input('flattening', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'flattening','value'=>$item['flattening'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][flattening]')).'</td>';
    $mech.= '<td>'.$this->Form->input('flaring', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'flaring','value'=>$item['flaring'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][flaring]')).'</td>';
    $mech.= '<td>'.$this->Form->input('eddy', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'eddy','value'=>$item['eddy'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][eddy]')).'</td>';
    $mech.= '<td>'.$this->Form->input('ultrasonic', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'ultrasonic','value'=>$item['ultrasonic'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][ultrasonic]')).'</td>';
    $mech.= '<td>'.$this->Form->input('hardness', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'hardness','value'=>$item['hardness'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][hardness]')).'</td>';
    $mech.= '<td>'.$this->Form->input('hydro', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'hydro','value'=>$item['hydro'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][hydro]')).'</td>';
    $mech.= '<td>'.$this->Form->input('igc_a', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'igc_a','value'=>$item['igc_a'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][igc_a]')).'</td>';
    $mech.= '<td>'.$this->Form->input('igc_e', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'igc_e','value'=>$item['igc_e'],'name'=>'data[Certificate][Certificateitem]['.$keyid.'][igc_e]')).'</td>';
    $mech.= '</tr>';
}
?>
<div class="table-responsive">
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="15%">Certificate No</th>
                    <td width="35%"><?php echo $this->Form->input('certificateno', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'type'=>'text','value'=>$certi['Certificate']['certificateno'],'readonly'));?></td>
                    <th width="15%">Issue Date</th>
                    <td width="35%">
                        <div class="col-lg-6 date date-picker" data-date-format="dd/mm/yyyy"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php echo $this->Form->input('issuedate', array('class'=>'form-control changedatepicker','label'=>false,'required'=>true,'type'=>'text','value' => date('d/m/Y',strtotime($certi['Certificate']['issuedate'])),'autocomplete'=>'off')); ?></div></div>
                    </td>
                    <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>
                </tr>
                <tr role="row">
                    <th width="15%">Client</th>
                    <td width="35%"><?php echo $certi[$mainmodel]['Order']['Client']['company_name'];?></td>
                    <th width="15%">Contract</th>
                    <td width="35%"><?php echo $certi['Order']['clientpo'];?>  Dt: <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($certi['Order']['clientpodate']));?></td>
                </tr>
                <!-- row-->
                <tr role="row">
                    <th width="15%">Specification</th>
                    <td width="35%"><?php echo $this->Form->input('std_spec', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'std_spec','value'=>$certi['Certificate']['std_spec']));?></td>
                    <th width="15%">Grade</th>
                    <td width="35%"><?php echo $this->Form->input('grd_spec', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'grd_spec','value'=>$certi['Certificate']['grd_spec']));?></td>
                </tr>
                <!-- row-->
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="20%">P.O. Sr No</th>
                    <th width="40%">Product</th>
                    <th width="40%">Size</th>
                </tr>
                <?php echo $product_detail;?>
                <!-- row-->                      
            </tbody>
        </table>
    </div>                
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="10%">Supply <br/>Conditions</th>
                    <th width="5%">P.O. <br/>SR. NO</th>
                    <th width="10%">System <br/>Heat No</th>
                    <th width="10%">Client <br/>Heat No</th>
                    <th width="10%">Print <br/>Heat No</th>
                    <th width="10%">Test Ref.<br/> No</th>
                    <th width="10%">Surface Finish</th>
                    <th width="10%">Dimension check & <br/>pmi check</th>
                    <th width="7%">Total Length</th>
                    <th width="7%">Total Quantity</th>
                    <th width="7%">Total Net Weight</th>
                </tr>
                <tr role="row">
                    <td rowspan="<?php echo $itemcount ;?>" class="v-align"><?php echo $this->Form->input('supply_condition', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'supply_condition','value'=>$certi['Certificate']['supply_condition']));?></td>
                    <?php echo $ord;?>
                    <!-- row-->                  
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%">Solution Annealing Temp.</th>
                    <th width="85%" colspan="10">Chemical Composition %</th>
                </tr>
                <tr role="row">
                    <td rowspan="<?php echo $itemcount + 3;?>" class="v-align"><?php echo $this->Form->input('required_annealing_temp', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'required_annealing_temp','value'=>$certi['Certificate']['required_annealing_temp'],'name'=>'data[Certificate][required_annealing_temp]')); ?></td>         
                    <th width="10%">Heat No</th>
                    <th width="8%"></th>
                    <?php echo $chem_comp;?>
                </tr>    
                <!-- row-->
                <tr>
                    <td></td>
                    <th>Minimum</th>
                    <?php echo $chem_min;?>
                </tr>    
                <!-- row-->
                <tr>
                    <td></td>
                    <th>Maximum</th>
                    <?php echo $chem_max;?>
                </tr>    
                <!-- row-->
                <?php echo $chem;?>
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%" colspan="2" rowspan="2">Testing Item</th>
                    <th width="8%">Tensile <br/>Strength</th>
                    <th width="8%">GRAIN <br/>SIZE</th>
                    <th width="8%">Yield <br/>Strength </th>
                    <th width="8%" rowspan="2">Elongation %</th>
                    <th width="8%" rowspan="2">Flattening <br/>Test</th>
                    <th width="8%" rowspan="2">Flaring <br/>Test</th>
                    <th width="8%" rowspan="2">Eddy Current <br/>Test</th>
                    <th width="8%" rowspan="2">Ultrasonic <br/>Test</th>
                    <th width="7%" rowspan="2">Hardness <br/>HRB</th>
                    <th width="6%" rowspan="2">Hydro Test </br>(Pressure) PSI</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice A</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice E</th>
                </tr>
                <tr role="row">
                    <th width="8%">(MPA)</th>
                    <th width="8%">Coarser</th>
                    <th width="8%">RP 0.2%</th>                    
                </tr>
                <!-- row-->
                <tr>
                    <td width="6%">Required</td>
                    <th width="9%"><?php echo $certi['Certificate']['grd_spec'];?></th>
                    <td><?php echo $mech_req['Mechanical']['tensile_strength'];?></td>
                    <td><?php echo $mech_req['Mechanical']['grain_size'];?></td>
                    <td><?php echo $mech_req['Mechanical']['yield_strength'];?></td>
                    <td><?php echo $mech_req['Mechanical']['elongation'];?></td>
                    <td><?php echo $mech_req['Mechanical']['flattening'];?></td>
                    <td><?php echo $mech_req['Mechanical']['flaring'];?></td>
                    <td><?php echo $mech_req['Mechanical']['eddy'];?></td>
                    <td><?php echo $mech_req['Mechanical']['ultrasonic'];?></td>
                    <td><?php echo $mech_req['Mechanical']['hardness'];?></td>
                    <td><?php echo $this->Form->input('required_hydro', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'required_hydro','value'=>$certi['Certificate']['required_hydro'],'name'=>'data[Certificate][required_hydro]')); ?></td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                </tr>    
                <!-- row-->
                <tr> 
                <?php echo '<td rowspan="'.$itemcount.'" class="v-align">Testing Data</td>'; ?>                      
                    <?php echo $mech;?>
                <!-- row-->
            </tbody>
        </table>
    </div>            
</div>          

<div class="row"><br/>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-1">Remarks</label>
            <div class="col-md-11">
                <?php echo $this->Form->textarea('remarks', array('cols'=>'2','rows'=>'5','class'=>'form-control','label'=>false));?>
            </div>
        </div>
    </div>                            
</div>                                            
                </div>
                <!-- END form-body class-->
                                        
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn green" type="submit">Submit</button>
                                     <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'certificates'),array('class' => 'btn default')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> </div>
                    </div>
                </div>
        <?php echo $this->Form->end();?>
    </div>
</div>

<script type="text/javascript">

/*Fetch certificate Number based on Change event of Date.*/
$(document).ready(function() {  
  $(document).on('change', '.changedatepicker',function(){
    var date = $('.changedatepicker').val(); 
    var current_fy = $('#fy').val();
    // var dateObject = $('.changedatepicker').datepicker('getDate');
      $.ajax({
          url: '<?php echo WEBSITE_PATH;?>app/calculateFinancialYearForDate/',
          type: 'POST',
          cache: false,
          data : "date="+date,
          success: function (new_fy) {
            /*Check new financial year is same as current financial year*/
            if(current_fy != new_fy){ 
            /*If not than fetch new auto increment number*/
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>certificates/getCertificateNO/ajax/',
                    type: 'POST',
                    cache: false,
                    data : "date="+date,
                    success: function (data) {
                        $('.uniqueNo').val(data);
                    }
                });
            }/*End If*/
          }
      });
  });
});
</script>