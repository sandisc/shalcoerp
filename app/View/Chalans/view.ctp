<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<!-- <a id="printrow" data-controllername="chalans" data-actionname="prints" data-id ="<?php echo base64_encode($view_chalan['Chalan']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline  prints" title="Print Chalan" targer="_blank"><span class="glyphicon glyphicon-print"></span> Print  </a> -->

  <?php
    echo $this->Html->link(
     '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
      array(
          'controller'=>'chalans',
          'action'=>'edit',
          base64_encode($view_chalan ['Chalan']['id'])
      ),
      array(
          'rel'=>'tooltip',
          'data-placement'=>'left',
          'data-original-title'=>'Edit',
          'title' => 'Edit',
          'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
          'escape'=>false  //NOTICE THIS LINE ***************
      )
    );

    /*Set PDF Download link*/
      echo $this->Html->link(
         '<i class="fa fa-file-pdf-o"></i> PDF',
          array(
              'controller'=>'chalans',
              'action'=>'generatepdf',
              'ext' => 'pdf',
              $view_chalan['Chalan']['id']
          ),
          array(
              'rel'=>'tooltip',
              'data-placement'=>'left',
              'data-original-title'=>'PDF',
              'title' => 'PDF',
              'download'=>'Chalan-'.$view_chalan['Chalan']['chalanno'],
              'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
              'escape'=>false  //NOTICE THIS LINE ***************
          )   
      );
      echo $this->Html->link(
        '<i class="fa fa-file-excel-o"></i> Download',
            array(
                'controller'=>'chalans',
                'action'=>'export',
                $view_chalan['Chalan']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Download',
                'download'=>'Chalan-'.$view_chalan['Chalan']['chalanno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );        
  ?>
       
<div class="portlet box green">
  <div class="portlet-title"> 
    <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
    <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
  </div>
  <div class="portlet-body form">
    <div class="form-body"> 
      <div class="row separatordiv">
        <div class="col-md-12">
          <div class="col-lg-9">
            <label class="control-label"><b>Chalan NO : </b><?php echo $view_chalan['Chalan']['chalanno'];?></label><span class="help-block"></span>                        
            <div class="col-lg-12">
              <div class="col-lg-1">
                <label class="control-label"><p><b></b></p></label>
              </div>              
              <div class="col-lg-12 text-center">
                  <label class="control-label"><p><b>M/s <u><?php echo $view_chalan['Order']['Client']['company_name'];?> </u>  </b></p> </label><br>
                 <label class="control-label"><p><b><u><?php echo searchFromAddress($view_chalan['Order']['billaddress_id'], $view_chalan['Order']['Client']['Address']);?> </u>  </b></p></label>
              </div>          
            </div>
          </div>
          <div class="col-lg-3">
            <label class="control-label"><b>Chalan Date : </b><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($view_chalan['Chalan']['chalandate']));?></label>
            <label class="control-label"><b>Shalco Order No : </b><?php echo $view_chalan['Order']['orderno'];?></label>
            <label class="control-label"><b>Shalco Order Date : </b><?php echo date('d/m/Y',strtotime($view_chalan['Order']['orderdate']));?></label>
          </div>
          <div class="col-md-12">                
            <p>Please Receive the undermentioned goods per your order <u><b><?php echo $view_chalan['Order']['clientpo'];?></b></u> dt. <u><b><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($view_chalan['Order']['clientpodate']));?></b></u></p>
            <p><b>and returns us the duplicate copy duly signed</b></p>                                  
          </div>
        </div>              
      </div>        
      <!--/row-->    
      <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
        <thead class="flip-content portlet box green">
          <tr>
            <th width="7%">Party <br> Invoice No</th>
            <th width="8%">Original <br>Product name</th>
            <th width="8%">Display <br> Product name</th>
            <th width="9%">Standard</th>
            <th width="8%">Grade</th>
            <th width="20%" colspan="2">Good Description</th>
            <th width="8%">Pcs</th>
            <th width="8%"> QTY</th>
            <th width="8%"> QTY/MT</th>
            <th width="5%">Unit Price<?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></th>
            <th width="10%">Net Amount<br> <?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></th>
          </tr>                      
        </thead>
        <tbody>
          <?php  
            $chalans = $view_chalan['Order']['Orderitem'];
            $chalan_item = $view_chalan['Chalanitem'];
            $i = 0;$j=0;
            foreach($chalans as $v) {   
              if(isset($chalan_item[$i]['orderitemid'])){
                if($chalan_item[$i]['orderitemid'] == $v['id']){  ?>
                    <tr class="addmore">
                      <td><?php echo $chalan_item[$i]['partyinvoiceno'];?></td>
                      <td><?php echo $chalans[$j]['Productcategory']['productname'];?></td>
                      <td><?php echo $chalan_item[$i]['label_productname'];?></td>
                      <td><?php echo $chalans[$j]['Productcategory']['Standard']['standard_name'];?></td>
                      <td><?php echo $chalans[$j]['Productcategory']['Grade']['grade_name'];?></td>
                      <td><?php echo setGDmmformat($chalans[$j]['Productcategory']['Size']['gdmm'],$chalans[$j]['length']);?></td>
                      <td><?php echo setGDnbformat($chalans[$j]['Productcategory']['Size']['gdnb'],$chalans[$j]['length']);?></td>
                      <td style="white-space:nowrap;"><?php echo $chalan_item[$i]['pcs'];?></td>
                      <td class="numalign"><?php echo decimalQty($chalan_item[$i]['qty_mtr']) .' '.$chalans[$j]['Productcategory']['Producttaxonomy']['unit']?></td>
                      <td class="numalign"><?php echo decimalQty($chalan_item[$i]['qty_mt']) .' '.$chalans[$j]['Productcategory']['Producttaxonomy']['unit']?></td>
                      <td class="numalign"><?php echo decimalPrice($chalan_item[$i]['price']);?></td>
                      <td class="numalign"><?php echo decimalPrice($chalan_item[$i]['netprice']); ?></td>
                    </tr>
                    <?php 
                  ++$i;
                }
                ++$j;
              }
            } ?>
            <tr id="1">               
             <td colspan="10"><?php $num = $this->requestAction('App/numtowords/'.$view_chalan['Chalan']['totalamount'].'/'.$view_chalan['Order']['Price']['fullform']);?><b style="text-transform: uppercase;"><?php echo $num; ?></td>
             <td id="total_lbl" > Total <?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></td>
             <td class="numalign"><?php echo decimalPrice($view_chalan['Chalan']['totalamount']);?></td>
           </tr>              
        </tbody>
      </table>
    </div>

    <div class="form-actions">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-offset-3 col-md-9">
              <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'chalans'),array('class' => 'btn default')); ?>
            </div>
          </div>
        </div>
        <div class="col-md-6"> </div>
      </div>
    </div>
  </div>
</div>
<!-- END View -->