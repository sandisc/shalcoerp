<!-- BEGIN FORM-->
<div class="portlet box">
  <div class="portlet-title"> 
    <div class="caption"><?php echo $pageTitle;?></div>
  </div>
  <div class="portlet-body form">
    <div class="form-body"> 
      <div class="row separatordiv">
        <div class="col-md-12">
          <div class="col-lg-9">
            <label class="control-label"><b>Chalan NO : </b> <?php echo $view_chalan['Chalan']['chalanno'];?></label><span class="help-block"></span>
            <div class="col-lg-12">
              <div class="col-lg-12 text-center">
                <p><b>M/s <u><?php echo $view_chalan['Order']['Client']['company_name'];?> </u> </b></p> <br>
                <label class="control-label"><p><b><u><?php echo searchFromAddress($view_chalan['Order']['billaddress_id'], $view_chalan['Order']['Client']['Address']);?> </u>  </b></p></label>
              </div>          
            </div>
          </div>
          <div class="col-lg-3">
            <label class="control-label"><b>Chalan Date : </b><?php echo date('d/m/Y',strtotime($view_chalan['Chalan']['chalandate']));?></label>
            <label class="control-label"><b>Shalco Order NO : </b><?php echo $view_chalan['Order']['orderno'];?></label>
            <label class="control-label"><b>Shalco Order Date : </b><?php echo date('d/m/Y',strtotime($view_chalan['Order']['orderdate']));?></label>
          </div>
          <div class="col-md-12">
            <p>Please Receive the undermentioned goods per your order <u><b><?php echo $view_chalan['Order']['clientpo'];?></b></u> dt. <u><b><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($view_chalan['Order']['clientpodate']));?></b></u></p>
            <p><b>and returns us the duplicate copy duly signed</b></p>
          </div>
        </div>              
      </div>
        
      <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
        <thead class="flip-content portlet box green">
          <tr>
            <th width="20%">Product Name</th>
            <th width="10%">Standard</th>
            <th width="10%">Grade</th>
            <th width="20%" colspan="2">Good Description</th>
            <th width="10%">Pcs</th>
            <th width="10%" id="qty_lbl"> QTY</th>
            <th width="10%" id="prc_lbl">Unit Price<?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></th>
            <th width="10%" id="amount_lbl">Net Amount<br> <?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></th>
          </tr>                      
        </thead>
        <tbody>
          <?php  
            $chalans = $view_chalan['Order']['Orderitem'];
            $chalan_item = $view_chalan['Chalanitem'];
            $i =0;$j =0;
            foreach($chalans as $v) {   
              if(isset($chalan_item[$i]['orderitemid'])){
                if($chalan_item[$i]['orderitemid'] == $v['id']){  ?>             
                  <tr class="addmore">
                    <td><?php echo $chalan_item[$j]['label_productname'];?></td>
                    <td><?php echo $chalans[$j]['Productcategory']['Standard']['standard_name'];?></td>
                    <td><?php echo $chalans[$j]['Productcategory']['Grade']['grade_name'];?></td>
                    <td><?php echo setGDmmformat($chalans[$j]['Productcategory']['Size']['gdmm'],$chalans[$j]['length']);?></td>
                    <td><?php echo setGDnbformat($chalans[$j]['Productcategory']['Size']['gdnb'],$chalans[$j]['length']);?></td>
                    <td  class="numalign" style="white-space:nowrap;"><?php echo $chalan_item[$i]['pcs'];?></td>
                    <td class="numalign"><?php echo decimalQty($chalan_item[$i]['qty_mtr']);?></td>
                    <td class="numalign"><?php echo decimalPrice($chalan_item[$i]['price']);?></td>
                    <td class="numalign"><?php echo decimalPrice($chalan_item[$i]['netprice']);?></td>           
                 </tr>      
                  <?php 
                  ++$i;
                }
                ++$j;
              }
            } ?>
            <tr id="1" class="">    
              <td colspan="7"><?php $num = $this->requestAction('App/numtowords/'.$view_chalan['Chalan']['totalamount'].'/'.$view_chalan['Order']['Price']['fullform']);?><b style="text-transform: uppercase;"><?php echo $num;?></td>
               <td id="total_lbl"> Total <?php echo '('.$view_chalan['Order']['Price']['sign'].')';?></td>
               <td><?php echo decimalPrice($view_chalan['Chalan']['totalamount']);?></td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>