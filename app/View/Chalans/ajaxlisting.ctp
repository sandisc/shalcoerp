<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Approval"),
    array("danger" => "Rejected"),
    array("danger" => "Pending")

  );

    foreach ($chalan as $chalans) { 
      $records = array(); 
      $records[] = '<input type="checkbox" name="id[]" value="'.$chalans['Chalan']['id'].'">';

      $records[] = '<a href="'.WEBSITE_PATH.'chalans/view/'.base64_encode($chalans['Chalan']['id']).'" title="View Chalan">'.$chalans['Chalan']['chalanno'].'</a>';
      $records[] = $this->requestAction('App/date_ymd_to_dmy/'.strtotime($chalans['Chalan']['chalandate']));
      $records[] = $chalans['Division']['division_name'];
      $records[] = '<a href="'.WEBSITE_PATH.'orders/view/'.base64_encode($chalans['Order']['id']).'" title="View Chalan">'.$chalans['Order']['orderno'].'</a>';
      $records[] = $chalans['Order']['Client']['company_name'];      
      $records[] = decimalQty($chalans['Chalan']['totalqty']);
      $records[] = decimalPrice($chalans['Chalan']['totalamount']);
      $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($chalans['Chalan']['modified']));
      $records[] = $chalans['User']['first_name']." ".$chalans['User']['last_name'];
      $id = $chalans['Chalan']['id'];
      //$records[] = '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>';
   
      $nextaction = '';
      /*Set Link of test certificate*/
        $tc_check = $this->requestAction('Certificates/checkCertiGenerate/1/'.$id);//check Test certificate generate or not
        if($tc_check == 0){
              $nextaction.= '<a href="'.WEBSITE_PATH.'certificates/generatetc/1/'.base64_encode($chalans['Chalan']['orderid']).'/'.base64_encode($chalans['Chalan']['id']).'" title="Generate Test Certificate" class="btn btn-success btn-double"><span class="glyphicon glyphicon-certificate"></span></a>';
        }
        else{
          
          $nextaction.= '<a href="'.WEBSITE_PATH.'certificates/viewtclist/1/'.base64_encode($id).'" title="View Test Certificate" class="btn btn-warning btn-double"><span class="glyphicon glyphicon-certificate"></span></a>';
          
        }
        $permiss_sess = '';
        if(in_array('chalans/edit',$this->Session->read('accesscontrollers_actions'))){
          $permiss_sess .= '<a href="'.WEBSITE_PATH.'chalans/edit/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
        }
        if(in_array('chalans/delete',$this->Session->read('accesscontrollers_actions'))){
          $permiss_sess .= '<a href="'.WEBSITE_PATH.'chalans/delete/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this chalan?"><span class="glyphicon glyphicon-trash"></span></a>';
        }
       $print = '<a id="printrow" data-controllername="chalans" data-actionname="prints" data-id ="'.base64_encode($chalans['Chalan']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Chalan" targer="_blank"><span class="glyphicon glyphicon-print"></span> </a>';            
      $records[] = '<a href="'.WEBSITE_PATH.'chalans/view/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>'.$permiss_sess .$nextaction;
      $record['data'][] = $records;
    }

    $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
 // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>