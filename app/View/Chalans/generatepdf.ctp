<?php
    $setting= $this->Session->read('setting_data'); /*Read setting table's data*/
    $cl_address = searchFromAddress($data['Order']['billaddress_id'], $data['Order']['Client']['Address']);
    $clientinfo = $this->requestAction('App/getClientInfo/'.$data['Order']['bill_to']);
    $html = '<style>
    body {font-family:helvetica; font-size:12px;}

table {float: none;margin: 0 auto;width: 100%;border-spacing: 0; cellspacing:5; cellpadding:2;}   
.table {margin-bottom: 0px;width: 100%; }     
.left tr td,th{text-align:left;}
.left tr th{border:0px;}
table tr th,td{padding:3px; letter-spacing: 1px!important;border:1px solid; }
.header tr td{border:0px;!important;}
.table1{width:45%; vertical-align:top;padding:0px;}
.table2{width:55%; vertical-align:top;padding:0px;}
.border_bottom{border-bottom:1px solid !important;}
.border_left{border-left:1px solid;}
.col-md-10{width:100%;}
.col-md-9{width:90%;}
.col-md-8{width:80%;}
.col-md-7{width:70%;}
.col-md-6{width:60%;}
.col-md-5{width:50%;}
.col-md-4{width:40%;}
.col-md-3{width:30%;}
.col-md-2{width:20%;}
.col-md-1{width:10%;}
.numalign { text-align: right;padding-right: 10px !important;margin-right: 10px;margin-left: 3px;}
th span{}
td span{}
.item tr th,td{}
table {
  font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
  font-size: 13px;
  font-style: normal;
  font-variant: normal;
  font-weight: 500;
  line-height: 15px;
}
</style>';
$html .= '<html>
              <head>
                    <title>Delivery Chalan</title>
              </head>
              <body>
          <table class="table header"> 
            <tr>
              <td align="left"><img src="'.ROOT_PATH.'img/print_logo.png"></td>
              <td align="center" style="vertical-align: center !important;">
                <h2 align="center"><b>CHALLAN # '.$data["Chalan"]["chalanno"].' </b></h2><br>
                <h2><b>DATE '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($data["Chalan"]["chalandate"])).' </b></h2>
              </td>                     
            </tr>
            <tr>
                <td colspan="10" align="center">
                  <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building Gulalwadi, Mumbai - 400004 Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25 Fax: +91-22-23452161<br/>
                  Email: info@shalco.in Web: www.shalco.in</span> 
                </td>
            </tr>
            </table>
          <table class="table">
          <tbody> 
              <tr>
                  <td colspan="10" align="center" style="font-size:9px!important;"><b>SUBJECT TO MUMBAI JURISDICTION</b></td>
              </tr>
              <tr>
                  <td colspan="10" align="center"><b><h3>DELIVERY CHALLAN</h3></b> </td>
              </tr>
              <tr>
                <td colspan="10"><b><h2>M/s '.$data['Order']['Client']['company_name'].'</h2></b></td>
              </tr>
              <tr>
                <td colspan="7" rowspan="2">'.$cl_address.'</td>
                <td colspan="3" text-align="right"><b>SHALCO Order NO.</b>'.$data['Order']['orderno'].'</td>
              </tr>
              <tr>
                <td colspan="3"></td>
              </tr>             
               <tr>
                <td colspan="4">Client PAN NO. '.$clientinfo['Client']['pan_no'].'</td>
                <td colspan="3">Client TIN NO. '.$clientinfo['Client']['vat_tin_no'].'</td>
                <td colspan="3">Client GST NO. '.$clientinfo['Client']['gst_no'].'</td>
              </tr>
              <tr>
                <td colspan="10">Please Receive the undermentioned goods per your order <u><b>'.$data["Order"]["clientpo"].'</b></u> dt. <u><b>'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate'])).'</b></u><br>and returns us the duplicate copy duly signed</td>
              </tr>
            </tbody>
          </table>
          <table class="table">
            <tbody> 
              <tr>
                <th width="3%" class="numalign"><b>Sr. No.</b></th>
                <th colspan="3" align="center"><b>Product Description</b></th>
                <th colspan="2" align="center"><b>Good Description</b></th>
                <th align="center"><b>PCS</b></th>
                <th align="center"><b>QTY</b></th>
                <th align="center"><b>Unit Price <span style="font-family: DejaVu Sans; sans-serif;">('.$data['Order']['Price']['sign'].')</span></b></th>
                <th align="center"><b>Amount <span style="font-family: DejaVu Sans; sans-serif;">('.$data['Order']['Price']['sign'].')</span></th>
              </tr>';
              $chalans = $data['Order']['Orderitem'];
              $chalan_item = $data['Chalanitem'];
              $i = 0;$j=0;

            foreach($chalans as $v) {   
              if(isset($chalan_item[$i]['orderitemid'])){
                if($chalan_item[$i]['orderitemid'] == $v['id']){  
                $html .= '<tr class="addmore" >
                      <td class="numalign" style="padding-top:15px!important;padding-bottom:15px!important;">'.$chalans[$i]['sr_no'].'</td>
                      <td colspan="3">'.$chalan_item[$i]['label_productname'].'</td>
                      <td width="15%">'.setGDmmformat($chalans[$j]['Productcategory']['Size']['gdmm'],$chalans[$j]['length']).'</td>
                      <td width="15%">'.setGDnbformat($chalans[$j]['Productcategory']['Size']['gdnb'],$chalans[$j]['length']).'</td>
                      <td class="numalign" align="right">'.$chalan_item[$i]['pcs'].'</td>
                      <td class="numalign" align="right">'.decimalQty($chalan_item[$i]['qty_mtr']) .' '.$chalans[$j]['Productcategory']['Producttaxonomy']['unit'].'</td>
                      <td class="numalign" align="right">'.decimalPrice($chalan_item[$i]['price']).'</td>
                      <td class="numalign" align="right">'.decimalPrice($chalan_item[$i]['netprice']).'</td>
                    </tr>';
                  ++$i;
                }
                ++$j;
              }
            } 
          $html .= '<tr>
                      <td colspan="8"><span style="font-family: DejaVu Sans; sans-serif;">('.$data['Order']['Price']['sign'].')</span>'.ucfirst($this->requestAction('App/numtowords/'.$data['Chalan']['totalamount'].'/'.$data['Order']['Price']['fullform'])).'<b style="text-transform: uppercase;"></b></td>
                      <td>Total Amount <span style="font-family: DejaVu Sans; sans-serif;">('.$data['Order']['Price']['sign'].')</span></td>
                      <td class="numalign" align="right">'.decimalPrice($data['Chalan']['totalamount']).'</td>
                    </tr>
                    <tr>
                      <td colspan="4"><b>CIN NO </b>'.$setting['Setting']['cin_no'].'</td>
                      <td colspan="4"><b>GST NO </b>'.$setting['Setting']['gst_no'].'</td>
                      <td colspan="2"><b>GST (18%) Extra</b></td>
                    </tr>
                    <tr>
                      <td colspan="4"><b>PAN NO </b>'.$setting['Setting']['pan_no'].'</td>
                      <td colspan="4"></td>
                      <td colspan="2"></td>
                    </tr>
                    
                    <tr>
                      <td colspan="10">Any Mistake in Weight and Quantity must be immediately reported to us failing which no claims will be entertained.</td>
                    </tr>
                    <tr><td colspan="10">Goods once sold will not be taken back.</td></tr>
                    <tr rowspan=10>
                      <td colspan="10">Note:'.$data['Chalan']['remarks'].'</td>
                    </tr>
                    
                    <tr>
                      <td colspan="10"><b><h3>Bank Details</h3></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Bank Name </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["bank_name"].'</td>
                    </tr>
                    <tr>
                        <td colspan="2">Branch Name </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["branch_name"].'</td>
                    </tr>
                    <tr>
                        <td colspan="2">Swift Code </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["swift_code"].'</td>                
                    </tr>
                    <tr>
                        <td colspan="2">AD Code </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["ad_code"].'</td>                
                    </tr>
                    <tr>
                        <td colspan="2">Beneficiary&#039s A/C No </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["ac_no"].'</td>                
                    </tr>
                    <tr>
                        <td colspan="2">Beneficiary&#039s Name </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["ac_name"].'</td>                
                    </tr>
                    <tr>
                        <td colspan="2">IEC Code </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["iec_code"].'</td>                
                    </tr>
                    <tr>
                        <td colspan="2">IFC Code </td>
                        <td colspan="8">'.$data["Order"]["Bank"]["ifc_code"].'</td>
                    </tr>
                    <tr>
                      <td colspan="6" style="vertical-align:top;height: 100px;">RECEIVER SIGNATURE</td>                      
                      <td colspan="4" style="vertical-align:top;height: 100px;"><b>For, SHALCO INDUSTRIES PVT LTD <br/>
                      AUTHORISED SIGNATORY</b></td>
                    </tr>
                    <tr>
                      <td colspan="10">'.nl2br($setting['Setting']['work_phase']).'</td>
                    </tr>';

          $html .= '</tbody>
        </table>
      </body>
    </html>';
    echo $html;
?>