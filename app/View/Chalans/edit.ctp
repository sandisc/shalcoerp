<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
      <?php echo $this->Form->create('Chalan',array('type' => 'file','class'=>'metroform shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxeditsubmit/')); 
      echo $this->Form->input('id',array('type'=> 'hidden','value'=>$chalan_data['Chalan']['id']));
      echo $this->Form->input('orderid',array('type'=> 'hidden','value'=>$chalan_data['Chalan']['orderid']));
      echo $this->Form->input('old_division_id', array('label'=>false,'type'=>'hidden','id'=>'old_division_id','value'=>$chalan_data['Chalan']['division_id']));
      echo $this->Form->input('old_chalanno', array('label'=>false,'type'=>'hidden','id'=>'old_chalanno','value'=>$chalan_data['Chalan']['chalanno']));
      ?>

        <div class="form-body"> 
          <div class="row separatordiv">
              <div class="col-md-4">
                <div class="col-md-12">
                  <label class="control-label">Select Division<span class="required" aria-required="true"> * </span></label>
                    
                    <?php echo $this->Form->input('division_id', array('label'=>false,'type'=>'select','options'=>$division,'required'=>true,'class'=>'form-control division_id','value'=>$chalan_data['Chalan']['division_id'])); ?>
                     <span class="help-block"><br></span>
                  
                </div>
              </div>          
            <div class="col-md-4">
              <div class="col-lg-12">
                <label class="control-label">Chalan NO.<span class="required" aria-required="true"> * </span></label>
                <?php echo $this->Form->input('chalanno', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'id'=>'chalanno','type'=>'text','readonly','value'=>$chalan_data['Chalan']['chalanno'])); ?>
                <span class="help-block"><br></span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-lg-6 date date-picker" data-date-format="dd/mm/yyyy">
                <label class="control-label">Chalan Date<span class="required" aria-required="true"> * </span></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  <?php echo $this->Form->input('chalandate', array('type' => 'text','class'=>'form-control changedatepicker','label'=>false,'required'=>true,'value'=> date('d/m/Y',strtotime($chalan_data['Chalan']['chalandate'])),'autocomplete'=>'off')); ?>
                </div>
              </div>
              <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>
            </div>
            <div class="row separatordiv">
              <div class="col-md-12">
                <div class="col-lg-12 text-center">
                  <p>M/s  <b><u><?php echo $chalan_data['Order']['Client']['company_name'];?> </u></b></p>
                  <p><b><u><?php echo searchFromAddress($chalan_data['Order']['billaddress_id'], $chalan_data['Order']['Client']['Address']);?> </u>  </b></p>
                </div>          
              </div>
            </div>
            <!--/row-->
          </div>
          <!--/row-->
          <div class="row separatordiv">
            <div class="col-md-12">
              <div class="col-lg-12">
                <p><b>Please Recieve the undermentioned goods per your order <u><?php echo $chalan_data['Order']['clientpo'];?></u> dt. <u><?php echo date('d/m/Y',strtotime($chalan_data['Order']['orderdate']));?></u></b></p>
                <p><b>and returns us the duplicate copy duly signed</b></p>                              
              </div>
            </div>               
          </div>
          <!--/row-->
          <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
            <thead class="flip-content portlet box green">
            <?php echo chalanitemTableColumn('('.$chalan_data['Order']['Price']['sign'].')');?>
            </thead>
            <tbody>
              <?php  
                $chalans = $chalan_data['Order']['Orderitem'];
                $i = 0;
                $sum_total = 0;
                $chalan_item_count = 0;
                foreach($chalans as $key => $v) {   ++$key; 
                    /*ignore if item has status = 3 means cancelled*/
                    if($v['status']!=3){ 
                        /*if item has invoice than allow and if item has not invoice and in pending state than allow*/
                        if(!empty($v['Chalanitem']['id']) || (empty($v['Chalanitem']) && $v['status'] == 0)){ ?>
                          <tr class="dispalydetails showinfo">
                              <td rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $key;?></td>
                              <td colspan="7" class="basicdisplayinfo">
                                <b>Product :</b> <?php echo $v['Productcategory']['productname'];?> &nbsp;&nbsp;
                                <b>Unit :</b> <?php echo $v['Productcategory']['Producttaxonomy']['unit'];?> &nbsp;&nbsp; 
                                <b>STD :</b> <?php echo $v['Productcategory']['Standard']['standard_name'];?>&nbsp;&nbsp;
                                <b>Grade :</b> <?php echo $v['Productcategory']['Grade']['grade_name'];?> &nbsp;&nbsp;
                                <b>Gradetag :</b> <?php echo $v['Productcategory']['Gradetag']['gradetagname'];?> &nbsp;&nbsp;
                                <b>Size :</b> <?php echo setGDmmformat($v['Productcategory']['Size']['gdmm'],$v['length']);?>&nbsp;&nbsp;
                                <b>Ratio :</b> <?php echo $v['Productcategory']['Size']['calc_ratio'];?>&nbsp;&nbsp;
                                <b>HSCode :</b> <?php echo $v['Productcategory']['Hscode']['hscode']; ?>&nbsp;&nbsp;
                                <b>HS Value :</b> <?php echo $v['Productcategory']['Hscode']['rate'];?>
                              </td>                                                                
                             <td class="numeric" rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','id'=>'delete1','class'=>'deleteno','mydelnum'=> $key,'data-id'=> $v['id'])); ?>
                             </td>
                          </tr>
                          <tr class="addmore<?php echo $key;?>">
                            <?php 
                             ++$chalan_item_count;
                            if(isset($v['Chalanitem']['id'])){ $in_id = $v['Chalanitem']['id']; }else { $in_id = '';}
                            echo $this->Form->input('id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'id'.$key,'name'=>'data[Chalan][item]['.$key.'][id]','value'=>$in_id));
                            echo $this->Form->input('orderitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id'],'name'=>'data[Chalan][item]['.$key.'][orderitemid]'));
                            echo $this->Form->input('qty', array('type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['qty'],'name'=>'data[Chalan][item]['.$key.'][qty]'));
                            ?>
                          <td><?php if(isset($v['Chalanitem']['partyinvoiceno'])){ $partyinvoiceno = $v['Chalanitem']['partyinvoiceno']; } else { $partyinvoiceno = '';} 
                                    echo $this->Form->input('partyinvoiceno', array('class'=>'form-control ','type'=>'text','label'=>false,'id'=>'partyinvoiceno'.$key,'name'=>'data[Chalan][item]['.$key.'][partyinvoiceno]','value'=>$partyinvoiceno));?>
                          </td>
                          <td style="white-space:nowrap;"><?php if(isset($v['Chalanitem']['label_productname'])){ $label_productname = $v['Chalanitem']['label_productname']; } else { $label_productname = '';} ?>
                            <?php echo $this->Form->input('label_productname', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'name'=>'data[Chalan][item]['.$key.'][label_productname]','value'=>$label_productname));?>
                          </td>                                                              
                          <td style="white-space:nowrap;"><?php if(isset($v['Chalanitem']['pcs'])){ $pcs = $v['Chalanitem']['pcs']; } else { $pcs = '';} 
                          ?>
                            <?php echo $this->Form->input('pcs', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'name'=>'data[Chalan][item]['.$key.'][pcs]','value'=>$pcs));?>
                          </td>
                          <td> 
                            <?php if(isset($v['Chalanitem']['qty_mtr'])){ $qty_mtr = $v['Chalanitem']['qty_mtr']; } else { $qty_mtr = '';}
                            $phy_density  = 1;
                            if(isset($v['Productcategory']['Physical']['density']) && !empty($v['Productcategory']['Physical']['density'])){
                              $phy_density = $v['Productcategory']['Physical']['density'];
                            }?>
                            <?php echo $this->Form->input('qty_mtr', array('class'=>'form-control qty numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>decimalQty($qty_mtr) ,'name'=>'data[Chalan][item]['.$key.'][qty_mtr]','data-id'=>$key,'data-sizeratio' => $v['Productcategory']['Size']['calc_ratio'],'data-id'=>$key,'data-density'=>$phy_density,'data-unit'=> $v['Productcategory']['Producttaxonomy']['unit']));?>
                          </td>
                          <td> 
                            <?php if(isset($v['Chalanitem']['qty_mt'])){ $qty_mt = $v['Chalanitem']['qty_mt']; } else { $qty_mt = '';} ?>
                            <?php echo $this->Form->input('qty_mt', array('class'=>'form-control qty_mt numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'onkeyup'=>'countqtymt()','id'=>'qty_mt'.$key,'name'=>'data[Chalan][item]['.$key.'][qty_mt]','value'=>$qty_mt,'data-id'=>$key,'autocomplete'=>'off'));?>
                          </td>
                          <td>
                            <?php if(isset($v['Chalanitem']['price'])){ $price = $v['Chalanitem']['price']; } else { $price = '';} ?>
                            <?php echo $this->Form->input('price', array('class'=>'form-control prc numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'price'.$key,'value'=>decimalPrice($price),'name'=>'data[Chalan][item]['.$key.'][price]','data-id'=>$key));?>
                          </td>
                          <td>
                            <?php if(isset($v['Chalanitem']['netprice'])){ $netprice = $v['Chalanitem']['netprice']; } else { $netprice = '';} ?>
                            <?php echo $this->Form->input('netprice', array('class'=>'form-control amount numalign decimalallow','label'=>false,'type'=>'text','required'=>true,'id'=>'amount'.$key,'readonly','value'=>decimalPrice($netprice),'name'=>'data[Chalan][item]['.$key.'][netprice]'));?>
                          </td>    
                            

                        </tr>      
                      <?php  ++$i;}
                    } 
                  } ?>
                  <input type="hidden" name="updateno" id="up" value="<?php echo $chalan_item_count; ?>" class="input" />
                  <input type="hidden" name="removal_id" id="removal_id" value="" />
                    
          <tr id="1">               
            <td colspan="3"></td>
            <td id="total_lbl"> Total <?php echo '('.$chalan_data['Order']['Price']['sign'].')';?></td>
            <td style="white-space:nowrap;">
                <?php echo $this->Form->input('totalqty', array('class'=>'form-control','type'=>'text','readonly','label'=>false,'required'=>true,'name'=>'data[Chalan][totalqty]','value'=>decimalQty($chalan_data['Chalan']['totalqty'])));?>                      
            </td>
             <td style="white-space:nowrap;">
                <?php echo $this->Form->input('totalqtymt', array('class'=>'form-control','type'=>'text','readonly','label'=>false,'required'=>true,'name'=>'data[Chalan][totalqtymt]','value'=>decimalQty($chalan_data['Chalan']['totalqtymt'])));?>  
              </td>
            <td></td>
            <td><?php echo $this->Form->input('totalamount', array('class'=>'form-control total numalign','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>decimalPrice($chalan_data['Chalan']['totalamount'])));?></td>
          </tr>
        </tbody>
      </table>

        <div class="row">                        
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2">Notes/Remarks</label>
                    <div class="col-md-10 input-icon right">
                        <?php echo $this->Form->textarea('remarks', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false,'value'=>$chalan_data['Chalan']['remarks']));?>
                    </div>
                </div>
            </div>
        </div>      
    </div>
        
      <div class="form-actions">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button class="btn green" type="submit">Submit</button>
                <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'chalans'),array('class' => 'btn default')); ?>
              </div>
            </div>
          </div>
          <div class="col-md-6"> </div>
        </div>
      </div>
    </div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->
<?php echo $this->element('chalan_commonfunction'); ?>