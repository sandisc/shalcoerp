<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Approval"),
    array("danger" => "Rejected"),
    array("danger" => "Pending")
  );

    foreach ($chalan as $chalans) {
      $records = array();      
      $records[] = '<input type="checkbox" name="id[]" value="'.$chalans['Chalan']['id'].'">';
      $records[] = $chalans['Chalan']['chalanno'];
      $records[] = $this->requestAction('App/date_ymd_to_dmy/'.strtotime($chalans['Chalan']['chalandate']));
      $records[] = decimalQty($chalans['Chalan']['totalqty']);
      $records[] = decimalPrice($chalans['Chalan']['totalamount']);
      $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($chalans['Chalan']['modified']));
      $records[] = $chalans['User']['first_name']." ".$chalans['User']['last_name'];
      $id = $chalans['Chalan']['id'];
      //$records[] = '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>';
      $print = '<a id="printrow" data-controllername="chalans" data-actionname="prints" data-id ="'.base64_encode($chalans['Chalan']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Chalan" targer="_blank"><span class="glyphicon glyphicon-print"></span> Print 
        </a>';
      $records[] = '<a href="'.WEBSITE_PATH.'chalans/view/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>
       <a href="'.WEBSITE_PATH.'chalans/edit/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'chalans/delete/'.base64_encode($chalans['Chalan']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this chalan?"><span class="glyphicon glyphicon-trash"></span></a>
      ';
      $record['data'][] = $records;
    }

  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
    $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
 // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>