<h3 class="page-title"> <?php echo $mainTitle;?></h3>
	  
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
		<div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
	</div>
	<div class="portlet-body form">
    <!-- BEGIN FORM-->
    <?php echo $this->Form->create('Chalan',array('type' => 'file','class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
      if(isset($this->params['named']['targetid'])){
        echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
      }
      echo $this->Form->input('orderid',array('type'=> 'hidden','value'=>$id));
      echo $this->Form->input('old_division_id', array('label'=>false,'type'=>'hidden','id'=>'old_division_id','value'=>''));
      echo $this->Form->input('old_chalanno', array('label'=>false,'type'=>'hidden','id'=>'old_chalanno','value'=>''));?>       
		  <div class="form-body">	
          <div class="row separatordiv">
              <div class="col-md-4">
                <div class="col-md-12">
                  <label class="control-label">Select Division<span class="required" aria-required="true"> * </span></label>
                    
                    <?php echo $this->Form->input('division_id', array('label'=>false,'type'=>'select','options'=>$division,'required'=>true,'class'=>'form-control division_id')); ?>
                     <span class="help-block"><br></span>
                  
                </div>
              </div>
              <div class="col-md-4">
                  <div class="col-lg-12">
                  	<label class="control-label">Delivery Chalan NO.<span class="required" aria-required="true"> * </span></label>
                  	<?php echo $this->Form->input('chalanno', array('class'=>'form-control uniqueNo','readonly','label'=>false,'required'=>true,'id'=>'chalanno','type'=>'text')); ?>
                      <span class="help-block"><br></span>
                  </div>
              </div>
              <div class="col-md-4">
              	<div class="col-lg-6 date date-picker" data-date-format="dd/mm/yyyy">
                  	<label class="control-label">Chalan Date<span class="required" aria-required="true"> * </span></label>
                  	<div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    	<?php echo $this->Form->input('chalandate', array('type' => 'text','class'=>'form-control changedatepicker','label'=>false,'required'=>true,'autocomplete'=>'off')); ?>                           
                    </div>
                </div>
                <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>  
              </div>              
              <div class="row separatordiv">
                <div class="col-md-12">
                    <div class="col-lg-12 text-center">
                    <input type="hidden" name="client_id" id="client_id" class="input" value="<?php echo $order_item['Order']['bill_to']; ?>" />
                        <p>M/s  <b><u><?php echo $order_item['Client']['company_name'];?> </u>  </b></p>
                        <p><b><u><?php echo searchFromAddress($order_item['Order']['billaddress_id'], $order_item['Client']['Address']);?> </u>  </b></p>                              
                    </div>
                </div>               
              </div>         
              <div class="row separatordiv">
                  <div class="col-md-12">
                      <div class="col-lg-12">
                          <p><b>Please Recieve the undermentioned goods per your order <u><?php echo $order_item['Order']['clientpo'];?></u> dt. <u><?php echo date('d/m/Y',strtotime($order_item['Order']['orderdate']));?></u></b></p>
                          <p><b>and returns us the duplicate copy duly signed</b></p>                              
                      </div>
                  </div>               
              </div>
            <!--/row-->    
              <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
        				<thead class="flip-content portlet box green">
        					<?php echo chalanitemTableColumn('('.$order_item['Price']['sign'].')');?>
                </thead>
        			  <tbody>
        		      <?php  
                    $orders = $order_item['Orderitem'];
                    $chalan_item_count = 0;
                    foreach($orders as $key => $v) {   ++$key; 
                       	/*Fetch only those orderitem which is in pending or process state*/
                    	if($v['status'] == 0 || $v['status'] == 1){
                        ++$chalan_item_count;?>

                      <tr class="dispalydetails showinfo">
                        <th rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $key;?></th>
                        <td colspan="7" class="basicdisplayinfo">
                          <b>Product :</b> <?php echo $v['Productcategory']['productname'];?> &nbsp;&nbsp;
                          <b>Unit :</b> <?php echo $v['Productcategory']['Producttaxonomy']['unit'];?> &nbsp;&nbsp;
                          <b>STD :</b> <?php echo $v['Productcategory']['Standard']['standard_name'];?>&nbsp;&nbsp;
                          <b>Grade :</b> <?php echo $v['Productcategory']['Grade']['grade_name'];?> &nbsp;&nbsp;
                          <b>Gradetag :</b> <?php echo $v['Productcategory']['Gradetag']['gradetagname'];?> &nbsp;&nbsp;
                          <b>Size :</b> <?php echo setGDmmformat($v['Productcategory']['Size']['gdmm'],$v['length']);?>&nbsp;&nbsp;
                          <b>Ratio :</b> <?php echo $v['Productcategory']['Size']['calc_ratio'];?>&nbsp;&nbsp;                        
                          <b>HSCode :</b> <?php echo $v['Productcategory']['Hscode']['hscode']; ?>&nbsp;&nbsp;
                          <b>HS Value :</b> <?php echo $v['Productcategory']['Hscode']['rate'];?>
                        </td>
                        <td class="numeric" rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','id'=>'delete1','class'=>'deleteno','mydelnum'=> $key,'data-id'=> $v['id'])); ?>
                        </td>
                      </tr>
                      
                      <tr class="addmore<?php echo $key;?> filldetail">
                          <?php echo $this->Form->input('orderitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id'],'name'=>'data[Chalan][item]['.$key.'][orderitemid]'));
                            echo $this->Form->input('qty', array('type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['qty'],'name'=>'data[Chalan][item]['.$key.'][qty]'));?>
                          <td> <?php echo $this->Form->input('partyinvoiceno', array('class'=>'form-control ','type'=>'text','label'=>false,'id'=>'partyinvoiceno'.$key,'name'=>'data[Chalan][item]['.$key.'][partyinvoiceno]'));?></td>
                          <td><?php echo $this->Form->input('label_productname', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'label_productname'.$key,'name'=>'data[Chalan][item]['.$key.'][label_productname]','data-id'=>$key));?></td>
      						        <td style="white-space:nowrap;"><?php echo $this->Form->input('pcs', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'name'=>'data[Chalan][item]['.$key.'][pcs]'));?></td>
  						              <?php $remaining_qty = $v['qty'] - $v['dlvr_qty'];
                       			$net_remainig_amt = $remaining_qty * $v['price'];
                              $qty_mt= '';
                            $phy_density = '1';
                            if(isset($v['Productcategory']['Physical']['density']) && !empty($v['Productcategory']['Physical']['density'])){
                              $phy_density = $v['Productcategory']['Physical']['density'];
                            }
                            if($v['Productcategory']['Producttaxonomy']['unit'] == 'mtr'){
                              $qty_mt = ($remaining_qty * $v['Productcategory']['Size']['calc_ratio'] / 1000) * $phy_density ;
                            }
                          if($v['Productcategory']['Producttaxonomy']['unit'] == 'kgs'){
                            $qty_mt = ($remaining_qty * 1 / 1000) * $phy_density;
                          }
                            ?>                      
  					              <td><?php echo $this->Form->input('qty_mtr', array('class'=>'form-control qty numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>decimalQty($remaining_qty),'name'=>'data[Chalan][item]['.$key.'][qty_mtr]','data-id'=>$key,'data-sizeratio' => $v['Productcategory']['Size']['calc_ratio'],'autocomplete'=>'off','data-unit'=> $v['Productcategory']['Producttaxonomy']['unit'],'data-density'=>$phy_density));?></td>
                          <td><?php echo $this->Form->input('qty_mt', array('class'=>'form-control qty_mt numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'qty_mt'.$key,'value'=>decimalQty($qty_mt),'name'=>'data[Chalan][item]['.$key.'][qty_mt]','data-id'=>$key,'onkeyup'=>'countqtymt()'));?></td>
  					              <td><?php echo $this->Form->input('price', array('class'=>'form-control prc numalign decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'price'.$key,'value'=>decimalPrice($v['price']),'name'=>'data[Chalan][item]['.$key.'][price]','data-id'=>$key));?></td>
  					              <td><?php echo $this->Form->input('netprice', array('class'=>'form-control amount numalign decimalallow','label'=>false,'type'=>'text','required'=>true,'id'=>'amount'.$key,'readonly','value'=>decimalPrice($v['amount']),'name'=>'data[Chalan][item]['.$key.'][netprice]'));?></td>
  					              
  					            </tr>      
          				      <?php  
          				    } 
          			    } ?>
                    <input type="hidden" name="updateno" id="up" value="<?php echo $chalan_item_count; ?>" class="input" />
                    <input type="hidden" name="removal_id" id="removal_id" value="" />

  		          	<tr id="1">
  		               <td colspan="3"></td>
                     <td id="total_lbl"> Total <?php echo '('.$order_item['Price']['sign'].')';?></td>
                     <td style="white-space:nowrap;"><?php echo $this->Form->input('totalqty', array('class'=>'form-control','type'=>'text','readonly','label'=>false,'required'=>true,'name'=>'data[Chalan][totalqty]'));?></td>
                     <td style="white-space:nowrap;"><?php echo $this->Form->input('totalqtymt', array('class'=>'form-control','type'=>'text','readonly','label'=>false,'required'=>true,'name'=>'data[Chalan][totalqtymt]'));?></td>
                     
  		               <td></td>
  		               <td><?php echo $this->Form->input('totalamount', array('class'=>'form-control total numalign','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>decimalPrice($order_item['Order']['total'])));?></td>
  		            </tr>
        				</tbody>
      			  </table>
              <div class="row">                        
                  <div class="col-md-12">
                      <div class="form-group">
                          <label class="control-label col-md-2">Notes/Remarks</label>
                          <div class="col-md-10 input-icon right">
                              <?php echo $this->Form->textarea('remarks', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false));?>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <br/>
      		<div class="form-actions">
      			<div class="row">
      				<div class="col-md-6">
      					<div class="row">
      						<div class="col-md-offset-3 col-md-9">
                      <button class="btn green" type="submit">Submit</button>
      							<?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'chalans'),array('class' => 'btn default')); ?>
      						</div>
      					</div>
      				</div>
      				<div class="col-md-6"> </div>
      			</div>
      		</div>
    	</div>
    </div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->
<?php echo $this->element('chalan_commonfunction'); ?>