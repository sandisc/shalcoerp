<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
      <?php echo $this->Session->flash();?>   
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?> NO <span class="success"><?php echo $orders['Order']['orderno']; ?></span>
            </div>
            <div class="actions">
               <div class="btn-group btn-group-devided">
                 <?php if($orders['Order']['status'] == '1' || $orders['Order']['status'] == '0') { 
                    echo $this->Html->link(' Add Chalan',array('action' => 'add','controller' =>'chalans',base64_encode($orders['Order']['id'])),array('class' => 'fa fa-plus btn sbold green','title'=>'Add Chalan for this order')); ?>
                <?php } ?>
                 </div>
               <div class="btn-group">
               </div>
            </div>
            </div>
            <!--  Start Order View -->
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Order Details </div>
                                        <div class="actions">
                                            <a href="<?php echo WEBSITE_PATH.'orders/view/'.base64_encode($orders['Order']['id']); ?>" class="btn btn-info" target="_blank"><span class="glyphicon glyphicon-eye-open" title="View"> View</span></a> 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Order No : </div>
                                            <div class="col-md-9 value"> <?php echo $orders['Order']['orderno']; ?>
                                                <lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i>Verified</lable>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Order Date : </div>
                                            <div class="col-md-9 value"> <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($orders['Order']['orderdate'])); ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Client P.O. No : </div>
                                            <div class="col-md-9 value"> <?php echo $orders['Order']['clientpo']; ?></div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Client P.O. Date : </div>
                                            <div class="col-md-9 value"> <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($orders['Order']['clientpodate'])); ?> </div>
                                        </div>                                         
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Order Status : </div>
                                            <div class="col-md-9 value">
                                                <span class="label label-success"> 
                                                <?php if($orders['Order']['status'] == '0'){
                                                          echo 'Pending' ;
                                                        }elseif($orders['Order']['status'] == '1'){
                                                          echo 'Process';
                                                        }elseif($orders['Order']['status'] == '2'){
                                                          echo 'Closed';
                                                        }else {
                                                          echo 'Cancelled';
                                                         }
                                                        ?>
                                                </span>
                                            </div>
                                        </div>                                        
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Grand Total : </div>
                                            <div class="col-md-9 value"><?php echo $orders['Price']['sign'].' '.$orders['Order']['total']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Customer Information </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Customer Name : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['company_name']; ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Email : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['email']; ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Contact Person : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['contact_person']; ?>  </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Contact Num1 : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['contact_num1']; ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Contact Num2 : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['contact_num2']; ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-3 name"> Mobile : </div>
                                            <div class="col-md-9 value"> <?php echo $clients['Client']['mob']; ?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/row -->

                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet green-meadow box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Billing Address </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-12 value"> <?php echo $clients['Client']['company_name']; ?>
                                                <br> <?php echo searchFromAddress($orders['Order']['billaddress_id'], $orders['Client']['Address']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet red-sunglo box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Shipping Address </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-12 value"> <?php echo $orders['Order']['ship_to']; ?>
                                                <br><?php echo searchFromAddress($orders['Order']['shipaddress_id'], $orders['Client']['Address']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Delivery Chalans Of Order </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
<!--                                             <select class="table-group-action-input form-control input-inline input-small input-sm" id ="sel_ct">
                                                <option value="">Select...</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                <option value="delete">Delete</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button class="btn btn-sm red table-group-action-delete">
                                                <i class="fa fa-trash"></i> Delete</button> -->
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="head*ing">
                                                    <th width="2%"><input type="checkbox" class="group-checkable"> </th>
                                                    <th width="13%"> Chalan No. </th>
                                                    <th width="10%"> Chalan Date </th> 
                                                    <th width="15%"> Total Qty </th>
                                                    <th width="15%"> Total Amount </th>
                                                    <th width="15%"> Modified Date</th>
                                                    <th width="15%"> Modified By </th>
                                                    <th width="15%"> Actions </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="chalanno"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="chalandate" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                      <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td> 
                                                    <td><input type="text" class="form-control form-filter input-sm" name="totalqty"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="totalamount"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                          <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                                          <span class="input-group-btn">
                                                              <button class="btn btn-sm default" type="button">
                                                                  <i class="fa fa-calendar"></i>
                                                              </button>
                                                          </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                          <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                                          <span class="input-group-btn">
                                                              <button class="btn btn-sm default" type="button">
                                                                  <i class="fa fa-calendar"></i>
                                                              </button>
                                                          </span>
                                                        </div>
                                                    </td> 
                                                    <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                          <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                                          <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                     </div>
                                    </div>
                                </div>
                            </div>  
                        </div> <!---/row -->

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-cogs"></i>Order Items Detail </div>
                                    </div>
                                    <div class="portlet-body">
                                        Filter With Order Item Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a id="show_all" class="btn blue margin-top-10 margin-bottom-10 margin-right-10" title="Show All">Show All </a>
                                        <a id="process_status" class="btn blue margin-top-10 margin-bottom-10 margin-right-10" title="Process"> Process </a>
                                        <a id="close_status" class="btn blue margin-top-10 margin-bottom-10 margin-right-10" title="Close">Close </a>
                                        <a id="pending_status" class="btn blue margin-top-10 margin-bottom-10 margin-right-10" title="Pending">Pending </a>
                                        <a id="cancel_status" class="btn blue margin-top-10 margin-bottom-10 margin-right-10" title="Cancel">Cancel </a>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered table-striped order_status">
                                                <thead>
                                                    <tr>
                                                        <th width="5%" align="center"><b>Sr. No.</b></th>
                                                        <th width="15%" align="center"><b>Product Name</b></th>
                                                        <th width="8%" align="center"><b>Standard</b></th>
                                                        <th width="8%" align="center"><b>Grade</b></th>
                                                        <th width="16%" colspan="2" align="center"><b>Good Description</b></th>
                                                        <th width="8%" align="center"><b>QTY</b></th>
                                                        <th width="8%" align="center"><b>Delivered <br>QTY</b></th>
                                                        <th width="8%" align="center"><b>Status</b></th>
                                                        <th width="7%" align="center"><b>UNIT PRICE <?php echo '('.$orders['Price']['sign'].')';?></b></th>
                                                        <th width="8%" align="center"><b>AMOUNT<br><?php echo '('.$orders['Price']['sign'].')';?></b></th>
                                                        <th width="10%" align="center">Delivered Amount <?php echo '('.$orders['Price']['sign'].')';?></b></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $sum = 0;
                                                foreach($orders['Orderitem'] as $value) {  ?>
                                                    <tr class="status<?php echo $value['status']?>" style="font-size: 8px!important;">
                                                        <td><?php echo $value['sr_no'];?></td>
                                                        <td><?php echo $value['Productcategory']['productname'];?> </td>
                                                        <td><?php echo $value['Productcategory']['Standard']['standard_name'];?></td>
                                                        <td><?php echo $value['Productcategory']['Grade']['grade_name'];?></td>
                                                        <td><?php echo setGDmmformat($value['Productcategory']['Size']['gdmm'],$value['length']);?></td>
                                                        <td><?php echo setGDnbformat($value['Productcategory']['Size']['gdnb'],$value['length']);?></td>
                                                        <td><?php echo $value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                                                        <td><?php echo $value['dlvr_qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                                                        <?php if($value['status'] == '0'){
                                                          $status = '<span class="label label-sm label-warning">Pending</span>
                                                            <a id="changeitemstatus" data-value="3" data-id="'.$value['id'].'" class="btn blue margin-top-10 ">Cancel</a>';
                                                        }elseif($value['status'] == '1'){
                                                          $status = '<span class="label label-sm label-info">Process</span>
                                                            <a id="changeitemstatus" data-value="2" data-id="'.$value['id'].'" class="btn blue margin-top-10">Close</a>';
                                                        }elseif($value['status'] == '2'){
                                                          $status = '<span class="label label-sm label-success">Closed</span>';
                                                        }else {
                                                          $status = '<span class="label label-sm label-danger">Cancelled</span>';
                                                        }
                                                        ?>
                                                        <td><?php echo $status;?></td>
                                                        <td class="numalign"><?php echo $value['price'];?></td>
                                                        <td class="numalign"><?php echo $value['amount'];?></td>
                                                        <td class="numalign"><?php 
                                                        if(isset($value['Chalanitem']) && !empty($value['Chalanitem'])){
                                                            $total = 0;
                                                            foreach($value['Chalanitem'] as $val) {
                                                                if(!empty($val['netprice'] && $val['orderitemid'] == $value['id'])){
                                                                    $dlv_amt = $val['netprice'] * 100;
                                                                    $total = $total + $dlv_amt;
                                                                    $test = $total / 100;
                                                                    //$test = $val['netprice'];
                                                                }else{
                                                                    $test = '--';
                                                                }
                                                            }
                                                            echo decimalPrice($test); ?></td>
                                                      <?php $sum+= $test;}                                                       
                                                        }?>
                                                     </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/row -->

                        <div class="row">
                            <div class="col-md-6"> </div>
                            <div class="col-md-6">
                                <div class="well">
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name"> Order Total Amount : </div>
                                        <div class="col-md-3 value"> <?php echo $orders['Price']['sign'].' '.decimalPrice($orders['Order']['total'])?></div>
                                         <div class="col-md-8 name">Delivered Total Amount :</div>
                                        <div class="col-md-3 value"> <?php echo $orders['Price']['sign'].' '.decimalPrice($sum);?></div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/row -->

                    </div>
                    <!-- End: DIV tab-pane -->
                </div><!-- End: DIV tab-content -->
            </div><!-- End: DIV portlet-body -->
            <!--  End Order View -->
        </div><!--/End DIV portlet light portlet-fit portlet-datatable bordered -->
    </div>
</div>
        
<script>
    /*Start Change status of orderitem*/
    $(document).on('click', '#changeitemstatus', function () {
      var status_id = $(this).data('value');
      var ref= $(this);
      var id = $(this).data('id'); /*Order item id*/
      $.ajax({
        url: '<?php echo WEBSITE_PATH;?>orders/changeitemstatus/'+id+'/'+status_id,
        type: 'POST',
        data: {'status_id':status_id,'orderitem_id':id},
        cache: false,
        success: function (data) {
          if(status_id == 3){
            ref.parent().html('<span class="label label-sm label-danger">Cancelled</span>');
          }else{
            ref.parent().html('<span class="label label-sm label-success">Closed</span>');
          }
        }
      });
      return false;
    });
    /*End Change status of orderitem*/  

    /*Start Sorting based on orderitem status*/  
    var rows = $('table.order_status tr');
    $(document).on('click', '#process_status', function () {
    
      var process_status = rows.filter('.status1').show();
      rows.not( process_status ).hide();
    });
    $(document).on('click', '#close_status', function () {
      
      var close = rows.filter('.status2').show();
      rows.not( close ).hide();
    });
    $(document).on('click', '#pending_status', function () {
      
      var pending = rows.filter('.status0').show();
      rows.not( pending ).hide();
    });
    $(document).on('click', '#cancel_status', function () {
      var cancel = rows.filter('.status3').show();
      rows.not( cancel ).hide();
    });
     $(document).on('click', '#show_all', function () {
       rows.show();
    });
    /*End Sorting based on orderitem status*/  
  </script>