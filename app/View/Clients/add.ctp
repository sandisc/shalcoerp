<style>
.checkbox {
  float: left;
  margin: 2px 0 !important;
  width: 23%;
}
.multicheck {
  padding-bottom: 15px;
}
div.checker span.checked {
    background-position: -76px -260px !important;
}
</style>
<h3 class="page-title"> <?php echo $mainTitle;?></h3>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <?php echo $this->Form->create('Client',array('class'=>'shalcopopupajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                                        echo $this->Form->input('id', array('type'=>'hidden')); 
                                        /*set target if this form open in modal dialoag box instead of separate window*/
                                        if(isset($this->params['named']['targetid'])){
                                            echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                        } ?> 
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Company Name<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9 input-icon right">
                                                            <?php echo $this->Form->input('company_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Contact Person<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                              <?php echo $this->Form->input('contact_person', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));?>
                                                                <span class="help-block"><br></span>                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Email<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('email', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Mobile</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('mob', array('type' => 'text','class'=>'form-control','label'=>false,'maxlength'=>20));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Contact Number 1<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('contact_num1', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Contact Number 2</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('contact_num2', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Fax</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input("fax",array('type'=>'text','label'=>false,'class'=>'form-control'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Website</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('website', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Excise Regn NO </label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input("excise_regn_no",array('type'=>'text','label'=>false,'class'=>'form-control'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">GST NO </label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('gst_no', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">PAN NO </label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('pan_no', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>                                                  
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">VAT TIN NO </label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('vat_tin_no', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">CST TIN NO </label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('cst_tin_no', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                              
                                                <hr><h4 class="bold">Primary Address</h4>
                                                <?php if(!empty($this->request->data['Client']['id'])){     
                                                    $count = sizeof($address);
                                                    
                                                   foreach($address as $key => $v) {   ++$key; ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="col-md-4">
                                                                <?php echo $this->Form->textarea('streetaddress', array('label'=>false,'required'=>true,'class'=>'form-control nameClass','placeholder'=>'Street Address*','id'=>'streetaddress'.$key,'data-id'=>$key,'name'=>'data[Client][address]['.$key.'][streetaddress]','value'=>$v['Address']['streetaddress']));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("city",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'City ','id'=>'city'.$key,'data-id'=>$key,'name'=>'data[Client][address]['.$key.'][city]','value'=>$v['Address']['city']));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("state",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'State ','id'=>'state'.$key,'data-id'=>$key,'name'=>'data[Client][address]['.$key.'][state]','value'=>$v['Address']['state']));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("country",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'Country ','id'=>'country'.$key,'data-id'=>$key,'name'=>'data[Client][address]['.$key.'][country]','value'=>$v['Address']['country']));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'addressdeleteno','value'=>'1','title'=>'Delete','data-id'=>$v['Address']['id'])); ?>&nbsp;&nbsp;
                                                                <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addressaddno','value'=>'1')); ?>
                                                                <?php echo $this->Form->input('clientid', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'clientid'.$key,'readonly','value'=>$v['Address']['clientid'],'name'=>'data[Client][address]['.$key.'][clientid]'));?>
                                                                <?php echo $this->Form->input('id', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'cid'.$key,'readonly','value'=>$v['Address']['id'],'name'=>'data[Client][address]['.$key.'][id]'));?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>    
                                                <br>
                                                <?php  } }  
                                                if(empty($this->request->data['Client']['id']) || empty($this->request->data['Address'])){ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="col-md-4">
                                                                <?php echo $this->Form->textarea('streetaddress', array('label'=>false,'required'=>true,'class'=>'form-control nameClass','placeholder'=>'Street Address*','name'=>'data[Client][address][1][streetaddress]'));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("city",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'City ','name'=>'data[Client][address][1][city]'));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("state",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'State ','name'=>'data[Client][address][1][state]'));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Form->input("country",array('type'=>'text','label'=>false,'class'=>'form-control nameClass','placeholder'=>'Country ','name'=>'data[Client][address][1][country]'));?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'addressdeleteno','value'=>'1','title'=>'Delete')); ?>&nbsp;&nbsp;
                                                                <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addressaddno','value'=>'1')); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            <?php } ?>
                                                <input type="hidden" name="updateno" id="addressup" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" class="input" />
                                                <input type="hidden" name="addressremno" id="addressremno" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" width="35px;"/>
                                                <input type="hidden" name="address_removal_id" id="address_removal_id" value="" width="35px;"/>

                                                <!--/row-->
                                                <!--/row-->                                               
                                                <!-- <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Street Address 1<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->textarea('streetaddress1', array('label'=>false,'required'=>true,'class'=>'form-control'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Street Address 2</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->textarea('streetaddress2', array('class'=>'form-control','label'=>false));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City 1</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input("city1",array('type'=>'text','label'=>false,'class'=>'form-control'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City 2</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('city2', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">State 1</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input("state1",array('type'=>'text','label'=>false,'class'=>'form-control'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">State 2</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('state2', array('type' => 'text','class'=>'form-control','label'=>false));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country 1<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('country1', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country 2</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('country2', array('type' => 'text','label'=>false,'class'=>'form-control'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div> -->
                                                <hr class="before_hr">  
                                                <?php $clientype = array('2'=>'Vendor','1'=>'Customer','3'=>'Both'); ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Client Type<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('clienttypeid', array('type' => 'select','empty'=>'Select Client type','options'=>$clientype,'label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Co-ordinator<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('coordinator', array('type' => 'select','empty'=>'Select co-ordinator','options'=>$coordinator,'label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>                                             
                                                </div>
                                                <?php if(!empty($this->request->data['Client']['vendorid'])){ ?>
                                                <div class="row both_row">
                                                     <div class="col-md-6">
                                                            <div class="form-group client_type">
                                                                <label class="control-label col-md-3">Vendor<span class="required" aria-required="true"> * </span></label>
                                                                <div class="col-md-9">&nbsp;
                                                                <?php
                                                                    echo $this->Form->input('vendorid', array('type' => 'select','options'=>$vendors,'class'=>'form-control','label'=>false,'required'));?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <?php } ?> 
                                                <?php if(empty($this->request->data['Client']['vendorid'])){ ?>
                                                <div class="row both_row" style="display:none;">
                                                     <div class="col-md-6">
                                                            <div class="form-group client_type">
                                                                <label class="control-label col-md-3">Vendor<span class="required" aria-required="true"> * </span></label>
                                                                <div class="col-md-9">&nbsp;
                                                                <?php
                                                                    echo $this->Form->input('vendorid', array('type' => 'select','options'=>$vendors,'class'=>'form-control','label'=>false,'required'));?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="col-md-6"></div>
                                                </div>
                                                <?php }  ?>
                                                    
                                                <?php if(empty($this->request->data['Client']['industryid'])){ ?>
                                                <div class="row sec_row" style="display:none;">
                                                    <div class="col-md-12 hide_row multicheck">
                                                        <div class="multiselect required">
                                                         <label class="control-label col-md-3" for="RoleModuleId">Industry<span class="required" aria-required="true"> * </span></label>
                                                            <input name="data[Role][module_id]" value="" id="RoleModuleId" type="hidden">
                                                            <?php $i = 1;
                                                            foreach($industry as $k =>$v){ ?>
                                                                <div class="checkbox"><input name="data[Client][industryid][]" value="<?php echo $k;?>" id="RoleModuleId" type="checkbox">
                                                                <label for="RoleModuleId<?php echo $i;?>"><?php echo $v;?></label></div>
                                                            <?php $i++;} ?>
                                                            <span class="help-block"><br></span>
                                                        </div>
                                                    </div>
                                                </div>   
                                                <?php } 
                                                $selected = array();
                                                if(isset($this->request->data['Client']['industryid'])){
                                                    $selected= explode(',',$this->request->data['Client']['industryid']);
                                                }?>
                                                <?php if(!empty($this->request->data['Client']['industryid'])){ ?>
                                                <div class="row sec_row">
                                                    <div class="col-md-2">
                                                        <label class="control-label col-md-3">Industry <span class="required" aria-required="true"> * </span></label>
                                                    </div>
                                                    <div class="col-md-10 hide_row multicheck">
                                                        <div class="multiselect">
                                                            <input name="data[Client][industryid]" value="" id="RoleModuleId" type="hidden">
                                                            <?php $i = 1;
                                                            foreach($industry as $k =>$v){ ?>
                                                            <div class="checkbox">
                                                                <input name="data[Client][industryid][]" value="<?php echo $k;?>" id="RoleModuleId<?php echo $i;?>" type="checkbox" <?php if(in_array($k,$selected)) { ?> checked <?php } ?>>
                                                                <label for="RoleModuleId<?php echo $i;?>"><?php echo $v;?></label>
                                                            </div>
                                                            <?php $i++;} ?>
                                                            <span class="help-block"><br></span>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <?php } ?>
                                                                                     
                                                <!--/row-->
                                            <!--</div>-->
                                                <?php 
                                                $procat = array();
                                                if(isset($this->request->data['Client']['procatid'])){
                                                    $procat= explode(',',$this->request->data['Client']['procatid']);
                                                }
 
                                                ?>

                                                <div class="row">
                                                     <div class="col-md-2">
                                                        <label class="control-label col-md-12">Product Category <span class="required" aria-required="true"> * </span></label>
                                                     </div>
                                                    <div class="col-md-10 hide_row multicheck">
                                                        <div class="multiselect">
                                                            <input name="data[Client][procatid]" value="" id="RoleModuleId" type="hidden">
                                                            <?php $i = 1;
                                                            foreach($category as $k =>$v){ ?>
                                                            <div class="checkbox">
                                                                <input name="data[Client][procatid][]" value="<?php echo $k;?>" id="procat<?php echo $i;?>" type="checkbox" <?php if(in_array($k,$procat)){ ?> checked="checked"<?php } ?>>
                                                                <label for="proId<?php echo $i;?>"><?php echo $v;?></label>
                                                            </div>
                                                            <?php $i++;} ?>
                                                            <span class="help-block"><br></span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>   
                                                 <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Description</label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->textarea('description', array('label'=>false,'required'=>true,'class'=>'form-control'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                     
<!--                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('status', array('type' => 'select','options'=>array('1'=>'Active','0'=>'Inactive'),'label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                    <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </div>                                                     
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">    
                                                                <button class="btn green" type="submit">Submit</button>
                                                                <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'clients'),array('id'=>'cancelbutton','class' => 'btn default')); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6"> </div>
                                                </div>
                                            </div>
                                        <?php echo $this->Form->end();?>                                      
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <!-- END DIV portlet box green -->
<script src="<?php echo ROOT_PATH; ?>pages/scripts/customajaxpopup.js" type="text/javascript"></script>                                
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<!-- <script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script> -->
<?php } ?>
<script type="text/javascript">
$( document ).ready(function() {
  // Handler for .ready() called.
  //$('.sec_row').show();
});
$(document).on('change', '#ClientClienttypeid', function() {  
  var id = $(this).val();  
    if(id == 1){
        $('.both_row').hide();
        //$('.both_row').children(':first-child').next().remove();
        $('.sec_row').show();
       $.ajax({
          url: '<?php echo WEBSITE_PATH;?>clients/getSupply/'+id,
          type: 'POST',
          cache: false,
           success: function (data) {
            $('.sec_row').html(data);
          
      }
     });
    }
    if(id == 2){
        $('.both_row').show();
        $('.both_row').children(':first-child').next().remove();
         $('.sec_row').hide();
        $.ajax({
          url: '<?php echo WEBSITE_PATH;?>clients/getVendor/'+id,
          type: 'POST',
          cache: false,
           success: function (data) {
            $('.client_type').html(data);
          
      }
     });   
    }
    if(id == 3){
        $('.sec_row').show();
        $('.both_row').show();
        $(".both_row").html('<div class="col-md-6"><div class="form-group client_type"><label class="control-label col-md-3">Vendor<span class="required" aria-required="true"> * </span></label><div class="col-md-9"><div class="input select"><select name="data[Client][vendorid]" required="required" class="form-control" id="ClientClientvendor"><?php foreach($vendors as $key => $val){?><option value="<?php echo $key;?>"><?php echo $val;?></option><?php } ?></select><span class="help-block"><br></span></div></div></div></div>');
        $('.sec_row').html('<div class="col-md-2"><label class="control-label col-md-12">Industry <span class="required" aria-required="true"> * </span></label></div><div class="col-md-10 hide_row multicheck"><div class="multiselect"><input name="data[Client][industryid]" value="" id="RoleModuleId" type="hidden"><?php foreach($industry as $k =>$v){ ?><div class="checkbox"><input name="data[Client][industryid][]" value="<?php echo $k;?>" id="RoleModuleId<?php echo $i;?>" type="checkbox"><label for="RoleModuleId<?php echo $i;?>"><?php echo $v;?></label></div><?php } ?></div></div></div>');
    }
    if(id == ''){
        $('.both_row').hide();
        $('.sec_row').hide();
    }
});
$(document).on('click', '.addressaddno', function () {
    var no = $('#addressup').val();
    var nxt = parseInt(no) + 1;
    var address = [];
        $(this).closest('.row').find('.nameClass').each(function() {
            var id = $(this).val();
            address.push(id);
        });
        console.log(address);
    $(".before_hr").before('<div class="row"><div class="col-md-12"><div class="form-groaddressup"><div class="col-md-4"><textarea name="data[Client][address]['+nxt+'][streetaddress]" required="required" class="form-control nameClass" placeholder="Street Address*" id="ClientStreetaddress">'+address[0]+'</textarea></div><div class="col-md-2"><div class="input text"><input name="data[Client][address]['+nxt+'][city]" class="form-control nameClass" placeholder="City " id="ClientCity" type="text" value="'+address[1]+'"></div></div><div class="col-md-2"><div class="input text"><input name="data[Client][address]['+nxt+'][state]" class="form-control nameClass" placeholder="State " id="ClientState" type="text" value="'+address[2]+'"></div></div><div class="col-md-2"><div class="input text"><input name="data[Client][address]['+nxt+'][country]" class="form-control nameClass" placeholder="Country " id="ClientCountry" type="text" value="'+address[3]+'"></div></div><div class="col-md-2"><img src="/img/warning.png" alt="delete" class="addressdeleteno"  mydelnum="'+nxt+'" value="1" title="Delete">&nbsp;&nbsp;<img src="/img/add.png" alt="Add" title="Add" id="add" class="addressaddno" value="1"></div></div></div></div><br>');

    var upno = $('#addressup').val(nxt);   
    var remmno = $('#addressremno').val();
    var remnextno = parseInt(remmno) + 1;
        
});

    /* Use : Delete row from address. */
    $(document).on('click', '.addressdeleteno', function () {
            
        var upno = $('#addressup').val();
        var rm = $('#addressremno').val();
        var id = $(this).attr('data-id');
        var gd = $(this).attr('mydelnum');
        if(upno != '1'){
            if(id == ''){
                $('#address_removal_id').val();
            }
            else{   
                $('#address_removal_id').val(function(i,val) { 
                    return val + (!val ? '' : ',') + id;
                });
            }
            var myno = $(this).attr('mydelnum');
            
            $(this).parent().parent().parent().parent().remove();

            var remainingno = parseInt(upno) - 1;
            var upno = $('#addressup').val(remainingno);   
        }
    });

</script>