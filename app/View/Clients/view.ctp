<div class="pull-left"><h3 class="page-title"> <?php echo $mainTitle;?></h3></div>
<div class="pull-right">
    <?php echo $this->Html->link(
           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
            array(
                'controller'=>'clients',
                'action'=>'add',
                base64_encode($data['Client']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Edit',
                'title' => 'Edit',
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );?>
</div>
<div class="clearfix"></div>
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                </div>
                            </div>
                            <div class="portlet-body form metroform">
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" role="form">
                                    <div class="form-body">                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Company Name</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">  <?php echo h($data['Client']['company_name']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Contact Person</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">  <?php echo h($data['Client']['contact_person']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Email</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">  <?php echo h($data['Client']['email']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Mobile</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Client']['mob']); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Contact Number 1</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">  <?php echo h($data['Client']['contact_num1']); ?></p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Contact Number 2</label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">  <?php echo h($data['Client']['contact_num2']); ?></p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Fax </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">   <?php echo h($data['Client']['fax']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Website </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"><?php echo h($data['Client']['website']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        </div>
                                        <!--/row-->
                                        <hr>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">VAT TIN NO </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">   <?php echo h($data['Client']['vat_tin_no']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">CST TIN NO </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"><?php echo h($data['Client']['cst_tin_no']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Excise Regn No </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">   <?php echo h($data['Client']['excise_regn_no']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">PAN NO </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"><?php echo h($data['Client']['pan_no']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        </div>
                                        <!--/row-->
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">GST NO </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static">   <?php echo h($data['Client']['gst_no']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                                                                           
                                        </div>
                                        <!--/row-->                                        
                                        <hr>                                                                                
                                        <?php $i = 1;
                                            foreach($data['Address'] as $address) { ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label"> Street Address <?php echo $i;?> </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo nl2br($address['streetaddress']); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label"> City <?php echo $i;?></label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo nl2br($address['city']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">State <?php echo $i;?></label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($address['state']); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Country <?php echo $i;?></label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($address['country']); ?></p>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <?php 
                                        $i++; } ?>
                                        <!--
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">State 1 </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Client']['state1']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">State 2 </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Client']['state2']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </div>                                      
                                        <!--/row-->
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Client Type </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"><?php if($data['Client']['clienttypeid'] == 1) { echo 'Customer'; }elseif($data['Client']['clienttypeid'] == 2) { echo 'Vendor'; } else { echo 'Customer & Vendor';} ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Description </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Client']['description']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Vendor </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"><?php if($data['Client']['clienttypeid'] == 1) { echo 'Customer'; }elseif($data['Client']['clienttypeid'] == 2) { echo 'Vendor'; } else { echo 'Customer & Vendor';} ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Co-ordinator </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Coordinator']['first_name'].' '.$data['Coordinator']['last_name']); ?></p>
                                                    </div>
                                                </div>
                                            </div>                                        
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Modified By </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['User']['first_name'].' '.$data['User']['last_name']); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="">
                                                    <label class="control-label col-md-3 view-label">Modified On </label>
                                                    <div class="col-md-9 view-data">
                                                        <p class="form-control-static"> <?php echo h($data['Client']['modified']); ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->  
                                    </div>                                             
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                    <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'clients'),array('class' => 'btn default')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>