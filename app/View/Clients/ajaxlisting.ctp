<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($client as $clients) {
      if($clients['Client']['status'] == 1){
        $status = $status_list[0];
      }
      else{
        $status = $status_list[1];
      }
      $type = '';
      if($clients['Client']['clienttypeid'] == 1){
        $type = 'Customer';
      }
      if($clients['Client']['clienttypeid'] == 2){
        $type = 'Vendor';
      }
      if($clients['Client']['clienttypeid'] == 3){
        $type = 'Both';
      }
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$clients['Client']['id'].'">',
      $clients['Client']['company_name'],
      $clients['Client']['email'],
      $clients['Client']['contact_num1'],
      $clients['Client']['mob'],
      $clients['Client']['contact_person'],
      $type,
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($clients['Client']['modified'])),
      $clients['User']['first_name']." ".$clients['User']['last_name'] ,

      '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
       '<a href="'.WEBSITE_PATH.'clients/view/'.base64_encode($clients['Client']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>
       <a href="'.WEBSITE_PATH.'clients/add/'.base64_encode($clients['Client']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'clients/delete/'.base64_encode($clients['Client']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this Client?"><span class="glyphicon glyphicon-trash"></span></a>',);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }   
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>