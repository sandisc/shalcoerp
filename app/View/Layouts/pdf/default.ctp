<?php  
require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
spl_autoload_register('DOMPDF_autoload'); 
$dompdf = new DOMPDF(); 
$customPaper = array(0,0,960,960);

$dompdf->set_paper($customPaper);
$dompdf->set_base_path(realpath($this->webroot.'/global/plugins/bootstrap/css/bootstrap.min.css'));
$dompdf->set_base_path($this->webroot.'/css/pdf.css');
$dompdf->load_html(utf8_decode($post), Configure::read('App.encoding'));
$dompdf->render();
$pdfname = $dompdf->output();
echo $pdfname;	
//$file_location = $_SERVER['DOCUMENT_ROOT']."/shalcoerp/app/webroot/uploads/invoices/".rand()."Proformainvoice.pdf";
//file_put_contents($file_location,$pdfname);
