<?php echo $this->element('header'); ?>
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo WEBSITE_PATH;?>"><img src="<?php echo ROOT_PATH;?>img/logo.png" alt="Shalco" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <div style="display: none;" class="load_bg" id="spinner">
                <img alt="loader" src="<?php echo ROOT_PATH;?>img/loader1.gif">        
            </div>        
        	<?php echo $this->fetch('content'); ?>
        </div>        
        <!-- END LOGIN -->
    	<?php echo $this->element('login_footer')?>
   