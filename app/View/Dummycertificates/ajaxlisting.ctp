<?php
  /* 
   * Paging
   */
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

    foreach ($certi as $certi) {
      $permiss_ses = '';
      if(in_array('dummycertificates/edit',$this->Session->read('accesscontrollers_actions'))){
        $permiss_ses .= '<a href="'.WEBSITE_PATH.'dummycertificates/edit/'.$certi['Dummycertificate']['reftype'].'/'.base64_encode($certi['Dummycertificate']['id']).'" class="btn btn-success" title="Edit Dummycertificate"><span class="glyphicon glyphicon-pencil"></span></a>';
      }
      if(in_array('dummycertificates/delete',$this->Session->read('accesscontrollers_actions'))){
        $permiss_ses .= '<a href="'.WEBSITE_PATH.'dummycertificates/delete/'.base64_encode($certi['Dummycertificate']['id']).'" class="btn btn-danger" title="Delete Dummycertificate" id="delete" data-confirm="Are you sure to delete this certificate?"><span class="glyphicon glyphicon-trash"></span></a>';
      }   
      if($certi['Dummycertificate']['isverified'] == 0){
        $verify_tc = '<div class="form-group"><div class="col-md-9"><button id = "btn_verify_tc" value="'.$certi['Dummycertificate']['id'].'" class = "demo-loading-btn btn blue verify" data-loading-text="Loading..." type="button" title="Click to Verify"> Verify </button>';
      }else {
        $verify_tc = '<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i>Verified<label>';
      }

      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$certi['Dummycertificate']['id'].'">',
      '<a href="'.WEBSITE_PATH.'dummycertificates/view/'.$certi['Dummycertificate']['reftype'].'/'.base64_encode($certi['Dummycertificate']['id']).'">'.$certi['Dummycertificate']['certificateno'].'</a>',
      $certi['Dummycertificate']['print_certificateno'],
      $certi['Dummycertificate']['std_spec'],
      $certi['Dummycertificate']['grd_spec'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($certi['Dummycertificate']['modified'])),
      $certi['User']['first_name']." ".$certi['User']['last_name'],
      $verify_tc,
      '<a href="'.WEBSITE_PATH.'dummycertificates/view/'.$certi['Dummycertificate']['reftype'].'/'.base64_encode($certi['Dummycertificate']['id']).'" class="btn btn-info" title="View Dummycertificate"><span class="glyphicon glyphicon-eye-open"></span></a>'.$permiss_ses,);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
 // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>