<?php
$setting= $this->Session->read('setting_data'); /*Read setting table's data*/
if ($certi['Dummycertificate']['reftype'] == 1) {
    $itemmodel = 'Chalanitem';
    $mainmodel = 'Chalan';
} else {
    $itemmodel = 'Invoiceitem';
    $mainmodel = 'Invoice';
}
$itemcount = count($certi['Dummycertificateitem']); /* Number of item in certificate */

$grade_id = $certi['Dummycertificate']['grade_id'];/* Grade id of first item */
$chem_req = $this->requestAction('Grades/getchemical/' . $grade_id);
$chem_comp = ''; /* Chemical component */
$chem_min = ''; /* Chemical component Min value */
$chem_max = ''; /* Chemical component Max value */
$chem_comp_name = array();
//echo count($chem_req); exit;
$proprty_counter = count($chem_req);
foreach ($chem_req as $value) {
    //pr($value); exit;
    $width = 65 / $proprty_counter;
    $chem_comp_name[] = $value[0];
    $chem_comp .= '<td width="' . $width . '%" align="center" ><b>' . ucfirst($value[0]) . '</b></td>';
    $chem_min .= '<td width="' . $width . '%" align="center" >' . $value[1] . '</td>';
    $chem_max .= '<td width="' . $width . '%" align="center" >' . $value[2] . '</td>';
}
$mech_req = $this->requestAction('Grades/getmechanical/' . $grade_id);
$phy_req = $this->requestAction('Grades/getphysical/' . $grade_id);
$ord = '';
$chem = ''; /* Chemical value measured of particular item */
$mech = ''; /* Physical value measured of particular item */
$product_detail = ''; /* Product name,size etc of item */

foreach ($certi['Dummycertificateitem'] as $item) {
    $keyid = $item['id'];
    
    $product_detail.='<tr align="center"><td>'.$item['sr_no'].'</td><td>'.$item['Productcategory']['productname'].'</td><td>'.$this->requestAction('Sizes/getsizedimension/'.$item['size_id'].'/'.$item['length'].'/2').'</td></tr>';

    if(!empty($ord)){ /*adding tr for each row except first row*/
        $ord .= '<tr align="center">';
    }
    $ord.= '<td align="center">' . $item['sr_no'] . '</td>';
    $ord.= '<td>' . $item['heatno'] . '</td>';    
    $ord.= '<td>' . $item['testref_no'] . '</td>';
    $ord.= '<td>' . $item['surface_finish'] . '</td>';
    $ord.= '<td>' . $item['dimension'] . '</td>';
    $ord.= '<td>' . $item['length'] . '</td>';
    $ord.= '<td>' . $item['qty_mtr'] . '</td>';
    $ord.= '<td>' . $item['netweight'] . '</td>';
    $ord.= '</tr>';

    $chem.= '<tr align="center">';
    $chem.= '<td>'.$item['heatno'].'</td>';
    $chem.= '<td>Observed</td>';
    foreach ($chem_comp_name as $val) {
        $chem.= '<td>' . $item[$val] . '</td>'; 
    }
    $chem.= '</tr>';

    if(!empty($mech)){ /*adding tr for each row except first row*/
        $mech .= '<tr align="center">';
    }
    $mech.= '<td>' . $item['heatno'] . '</td>';
    $mech.= '<td>' . $item['tensile_strength'] . '</td>';
    $mech.= '<td>' . $item['grain_size'] . '</td>';
    $mech.= '<td>' . $item['yield_strength'] . '</td>';
    $mech.= '<td>' . $item['elongation'] . '</td>';
    $mech.= '<td>' . $item['flattening'] . '</td>';
    $mech.= '<td>' . $item['flaring'] . '</td>';
    $mech.= '<td>' . $item['eddy'] . '</td>';
    $mech.= '<td>' . $item['ultrasonic'] . '</td>';
    $mech.= '<td>' . $item['hardness'] . '</td>';
    $mech.= '<td>' . $item['hydro'] . '</td>';
    $mech.= '<td>'.$item['igc_a'].'</td>';
    $mech.= '<td>'.$item['igc_e'].'</td>';
    $mech.= '</tr>';
}
//exit;
$chemcount = $itemcount + 3;
$html = '<style> 
body {font-family:helvetica; font-size:10px;}
table {float: none;margin: 0 auto;width: 100%;border-spacing: 0; cellspacing:5; cellpadding:2; }        
.table {margin-bottom: 0px;width: 100%; }
table tr td{padding:0 5px!important; letter-spacing: 1px!important;border:1px solid;}
.smallletter{text-align:right;}
.header tr td{border:0px!important;}
</style>';

$html .= '<html>
                <head>
                    <title>Test certificate</title>
                </head>
                <body>
<table class="table header">
    <tr>
        <td align="left" width="45%"><img src="'.ROOT_PATH.'img/print_logo.png"></td>
        <td align="center" width="15%"><img src="'.ROOT_PATH.'img/nabcb.png"></td>
        <td align="center" width="15%"><img src="'.ROOT_PATH.'img/tuv_reinland.png"></td>
        <td align="right" width="25%" style="vertical-align: bottom !important;"> 
            <b>CIN NO </b>'.$setting["Setting"]["cin_no"].'<br>
            <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building<br/>Gulalwadi, Mumbai - 400004<br/> Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25<br/> Fax: +91-22-23452161<br/>
            Email: info@shalco.in Web: www.shalco.in</span>
        </td>                    
    </tr>
</table>
<table class="table header"> 
    <tr>
        <td align="center"><b><h2>TEST CERTIFICATE</h2></b></td>
    </tr>
</table>
<table class="table header"> 
    <tr>
        <td align="center"><b>Certificate No. : '.$certi['Dummycertificate']['print_certificateno'].'</b></td>
        <td align="center"><b>PED '.$setting["Setting"]['ped_certificate'].'</b></td>
        <td align="center"><b>Certificate No.'.$setting["Setting"]['iso_certificate'].'</b></td>
        <td align="center"><b>ISO 9001:2008</b></td>
    </tr>
</table>            
<table class="table">
    <tr>
        <td width="15%" align="center"><b>Client</b></td>
        <td width="35%" align="left">'.$certi['Client']['company_name'].'</td>
        <td width="15%" align="center"><b>Contract</b></td>
        <td width="35%" align="left">'.$certi['Dummycertificate']['clientpo'].'  Dt: '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($certi['Dummycertificate']['orderdate'])).'</td>
    </tr>
    <tr>
        <td width="15%" align="center"><b>Specification</b></td>
        <td width="35%" align="left">'.$certi['Dummycertificate']['std_spec'].'</td>
        <td width="15%" align="center"><b>Grade</b></td>
        <td width="35%" align="left">'.$certi['Dummycertificate']['grd_spec'].'</td>
    </tr>
</table>
<table class="table">
    <tr align="center">
        <td width="15%"><b>P.O. Sr No</b></td>
        <td width="35%"><b>Product</b></td>
        <td width="50%"><b>Size</b></td>
    </tr>'.$product_detail.'
</table>
<table class="table">    
    <tr align="center"><td>&nbsp;</td></tr>
</table>

<table class="table">    
    <tr align="center">
        <td width="10%"><b>Supply Conditions</b></td>
        <td width="5%"><b>P.O. SR. NO</b></td>
        <td width="10%"><b>Heat No</b></td>        
        <td width="10%"><b>Test Ref. No</b></td>
        <td width="15%"><b>Surface Finish</b></td>
        <td width="15%"><b>Dimension check & <br/>pmi check</b></td>
        <td width="12%"><b>Total Length</b></td>
        <td width="10%"><b>Total Quantity</b></td>
        <td width="13%"><b>Total Net Weight</b></td>
    </tr>
    <tr align="center">
        <td width="10%" rowspan="'.$itemcount.'" >'.$certi['Dummycertificate']['supply_condition'].'</td>
        '.$ord.'
</table>
<table class="table">    
    <tr align="center"><td>&nbsp;</td></tr>
</table>  


<table class="table">    
    <tr align="center">
        <td width="15%"><b>Solution Annealing Temp.</b></td>
        <td width="85%" colspan="10"><b>Chemical Composition %</b></td>
    </tr>
</table>
<table class="table">  
    <tr align="center">
        <td width="15%" rowspan="'.$chemcount.'">'.$certi['Dummycertificate']['required_annealing_temp'].'</td>           
        <td width="10%"><b>Heat No</b></td>
        <td width="10%"></td>
        '.$chem_comp.'
    </tr>
    <tr align="center">
        <td></td>
        <td align="center"><b>Minimum</b></td>
        '.$chem_min.'
    </tr>
    <tr align="center">
        <td></td>
        <td align="center"><b>Maximum</b></td>
        '.$chem_max.'
    </tr>
    '.$chem.' 
</table>  
<table class="table">    
    <tr align="center"><td>&nbsp;</td></tr>
</table>


<table class="table">
    <tr align="center">
        <td width="15%" colspan="2" rowspan="2"><b>Testing Item</b></td>
        <td width="7%"><b>Tensile <br/>Strength</b></td>
        <td width="7%"><b>GRAIN <br/>SIZE</b></td>
        <td width="7%"><b>Yield <br/>Strength </b></td>
        <td width="7%" rowspan="2"><b>Elongation %</b></td>
        <td width="7%" rowspan="2"><b>Flattening <br/>Test</b></td>
        <td width="7%" rowspan="2"><b>Flaring <br/>Test</b></td>
        <td width="7%" rowspan="2"><b>Eddy Current <br/>Test</b></td>
        <td width="7%" rowspan="2"><b>Ultrasonic <br/>Test</b></td>
        <td width="7%" rowspan="2"><b>Hardness <br/>HRB</b></td>
        <td width="8%" rowspan="2"><b>Hydro Test<br>(Pressure) PSI</b></td>
        <td width="7%" rowspan="2">IGC - Test<br/>Practice A</td>
        <td width="7%" rowspan="2">IGC - Test<br/>Practice E</td>
    </tr>
    <tr align="center">
        <td width="8%"><b>(MPA)</b></td>
        <td width="8%"><b>Coarser</b></td>
        <td width="8%"><b>RP 0.2%</b></td>                    
    </tr>
</table>
<table class="table">    
    <tr align="center">
        <td width="6%">Required</td>
        <td width="9%" align="center"><b>'.$certi['Dummycertificate']['grd_spec'].'</b></td>
        <td width="7%">'.$mech_req['Mechanical']['tensile_strength'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['grain_size'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['yield_strength'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['elongation'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['flattening'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['flaring'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['eddy'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['ultrasonic'].'</td>
        <td width="7%">'.$mech_req['Mechanical']['hardness'].'</td>
        <td width="8%">'.$certi['Dummycertificate']['required_hydro'].'</td>
        <td width="7%" align="center">-</td>
        <td width="7%" align="center">-</td>
    </tr>
    <tr align="center"> 
        '.'<td rowspan="' . $itemcount . '" class="v-align">Testing Data</td>'.'                      
        '.$mech.'
    </tr>
</table>
<table class="table">
    <tr>
        <td width="60%" height="150px" style="vertical-align: top;"><b>Remarks :- </b><br>'.nl2br($certi['Dummycertificate']['remarks']).'</td>
        <td width="20%" height="150px" style="vertical-align: top;" align="center"><br><br>QC Engineer</td>
        <td width="20%" height="150px" style="vertical-align: top;" align="center"><br><br>HEAD QA/QC</td>        
    </tr>
</table>

      </body>
    </html>';
    echo $html;
?>