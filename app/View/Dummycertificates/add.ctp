<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""></a></div>
    </div>
    <div class="portlet-body form">    
        <?php echo $this->Form->create('Dummycertificate',array('type' => 'file','class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxaddsubmit/'));
            echo $this->Form->input('id',array('type'=>'hidden')) ;?>
                <div class="certificate form-body">
<?php $itemcount = 2; ?>
<div class="table-responsive">
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="15%">Certificate No<span class="required" aria-required="true"> * </span></th>
                    <td width="35%"><?php echo $this->Form->input('certificateno', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'type'=>'text','readonly'));?></td>
                    <th width="12%">Print Certificate No<span class="required" aria-required="true"> * </span></th>
                    <td width="13%"><?php echo $this->Form->input('print_certificateno', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text','value'=>''));?></td>                    
                    <th width="12%">Issue Date<span class="required" aria-required="true"> * </span></th>
                    <td width="13%">
                        <div class="date date-picker" data-date-format="dd/mm/yyyy"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php echo $this->Form->input('issuedate', array('class'=>'form-control changedatepicker','label'=>false,'required'=>true,'type'=>'text','value' => '','autocomplete'=>'off')); ?></div></div>
                    </td>
                    <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>  
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="15%">Client <span class="required" aria-required="true"> * </span></th>
                    <td width="35%"><?php  echo $this->Form->input('clientid', array('class'=>'form-control clientid comboselection','options'=>$clients,'type'=>'select', 'empty' => 'Select Client','label'=>false,'required'=>true));?>
                        <span class="help-block"><br></span>
                    </td>
                    <th width="12%">Contract (P.O. No.)<span class="required" aria-required="true"> * </span></th>
                    <td width="13%"><?php echo $this->Form->input('clientpo', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true)); ?><span class="help-block"><br></span></td>
                    <th width="12%">P.O. Dt. <span class="required" aria-required="true"> * </span></th>
                    <td width="13%"><div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php echo $this->Form->input('orderdate', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true)); ?>
                        </div>
                        <span class="help-block"><br></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="15%">Specification<span class="required" aria-required="true"> * </span></th>
                    <td width="15%"><?php echo $this->Form->input('standard_id', array('class'=>'form-control std comboselection nameclass','options'=>$standards,'type'=>'select', 'empty' => 'Select Standard','label'=>false,'data-id'=>1,'required'=>true));?><span class="help-block"><br></span></td>
                    <td width="20%"><?php echo $this->Form->input('std_spec', array('class'=>'form-control std_spec','type'=>'text','label'=>false,'required'=>true));?><span class="help-block"><br></span></td>
                    
                    <th width="12%">Grade<span class="required" aria-required="true"> * </span></th>
                    <td width="13%"><?php echo $this->Form->input('grade_id', array('class'=>'form-control grd comboselection nameclass','options'=>'','type'=>'select', 'empty' => 'Select Grade','label'=>false,'required'=>true,'id'=>"grade_id1",'data-id'=>1,'required'=>true));?><span class="help-block"><br></span></td>
                    <td width="25%"><?php echo $this->Form->input('grd_spec', array('type'=>'text','class'=>'form-control grd_spec','label'=>false,'required'=>true));?><span class="help-block"><br></span></td>
                </tr>
                <!-- row-->
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>             
                <tr role="row">
                    <th width="15%">P.O. Sr No</th>
                    <th width="35%">Product</th>
                    <th width="35%">Size</th>
                    <th width="15%">Length</th>
                </tr>
                <? for($i=1;$i <= $itemcount;$i++){?>
                <tr>
                    <td class="text-center"><?php echo $this->Form->input('sr_no', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][sr_no]'));?></td>
                    <td><?php echo $this->Form->input('procatid', array('class'=>'form-control procat comboselection nameclass','type'=>'select','options' => $procat, 'empty' => 'Select Product Category','label'=>false,'required'=>true,'id'=>'procatid'.$i.'','data-id'=>$i,'name'=>'data[Dummycertificate][item]['.$i.'][procatid]'));?>
                         <span class="help-block"><br></span><span class="productname<?php echo $i;?>"></span></td>
                    <td><?php echo $this->Form->input('size_id', array('class'=>'form-control size comboselection nameclass','type'=>'select','options' => '', 'empty' => 'Select Size','label'=>false,'required'=>true,'id'=>'size_id'.$i.'','data-id'=>$i,'name'=>'data[Dummycertificate][item]['.$i.'][size_id]'));?>
                         <span class="help-block"><br></span><span class='gdnbdata<?php echo $i;?>'></span></td>
                    <td><?php echo $this->Form->input('length', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'length'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][length]'));?></td>     
                </tr>
                <? } ?>
                <!-- row-->                      
            </tbody>
        </table>
    </div>                
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="10%">Supply <br/>Conditions</th>
                    <th width="5%">P.O. <br/>SR. NO</th>
                    <th width="10%">System <br/>Heat No</th>
                    <th width="9%">Client <br/>Heat No</th>
                    <th width="10%">Test Ref.<br/> No</th>
                    <th width="10%">Surface Finish</th>
                    <th width="10%">Dimension check & <br/>pmi check</th>
                    <th width="7%">Total Length</th>
                    <th width="7%">Total Quantity</th>
                    <th width="7%">Total Net Weight</th>
                </tr>
                <tr role="row">
                    <td rowspan="<?php echo $itemcount;?>" class="v-align"><?php echo $this->Form->input('supply_condition', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'supply_condition'));?></td>
                    <? for($i=1;$i<= $itemcount;$i++){ 
                            if($i == 1){
                                $required = 'true';
                            }else{  
                                $required = 'false';
                            }
                        ?>
                        <td class="text-center"><?php echo $this->Form->input('sr_no', array('class'=>'form-control','type'=>'text',
                        'id'=>'sr_no'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][sr_no]','label'=>false, 'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('heatno', array('class'=>'form-control','type'=>'text','id'=>'heatno'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][heatno]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('clientheatno', array('class'=>'form-control','type'=>'text','id'=>'clientheatno'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][clientheatno]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('testref_no', array('class'=>'form-control','type'=>'text','id'=>'testref_no'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][testref_no]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('surface_finish', array('class'=>'form-control','type'=>'text','id'=>'surface_finish'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][surface_finish]','label'=>false,
                        'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('dimension', array('class'=>'form-control','type'=>'text','id'=>'dimension'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][dimension]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('totallength', array('class'=>'form-control','type'=>'text','id'=>'totallength'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][totallength]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('qty_mtr', array('class'=>'form-control','type'=>'text','id'=>
                        'qty_mtr'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][qty_mtr]','label'=>false,'required'=>true));?></td>
                        <td class="text-center"><?php echo $this->Form->input('netweight', array('class'=>'form-control','type'=>'text','id'=>'netweight'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][netweight]','label'=>false,'required'=>true));?></td>
                        </tr>
                <?}?>
                    <!-- row-->                  
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%">Solution Annealing Temp.</th>
                    <th width="85%" colspan="10">Chemical Composition %</th>
                </tr>
                <tr role="row" class="chem_comp">
                    <td rowspan="<?php echo $itemcount + 3;?>" class="v-align"><?php echo $this->Form->input('required_annealing_temp', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'required_annealing_temp','name'=>'data[Dummycertificate][required_annealing_temp]')); ?></td>
                    <th width="10%">Heat No</th>
                    <th width="8%"></th>
                    <th class="chem_comp_value"></th>
                </tr>    
                <!-- row-->
                <tr class="chem_min">
                    <td></td>
                    <th>Minimum</th>
                    <th class="chem_min_value"></th>
                </tr>    
                <!-- row-->
                <tr class="chem_max">
                    <td></td>
                    <th>Maximum</th>
                    <th class="chem_max_value"></th>
                </tr>    
                <!-- row-->
                <?php for($i=1;$i <= $itemcount;$i++){?>
                    <tr class="chem_comp_name<?php echo $i;?>">
                        <td></td>
                        <td></td>
                        <td class="chem_comp_name_value<?php echo $i;?>"></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%" colspan="2" rowspan="2">Testing Item</th>
                    <th width="8%">Tensile <br/>Strength</th>
                    <th width="8%">GRAIN <br/>SIZE</th>
                    <th width="8%">Yield <br/>Strength </th>
                    <th width="8%" rowspan="2">Elongation %</th>
                    <th width="8%" rowspan="2">Flattening <br/>Test</th>
                    <th width="8%" rowspan="2">Flaring <br/>Test</th>
                    <th width="8%" rowspan="2">Eddy Current <br/>Test</th>
                    <th width="8%" rowspan="2">Ultrasonic <br/>Test</th>
                    <th width="7%" rowspan="2">Hardness <br/>HRB</th>
                    <th width="6%" rowspan="2">Hydro Test </br>(Pressure) PSI</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice A</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice E</th>
                </tr>
                <tr role="row">
                    <th width="8%">(MPA)</th>
                    <th width="8%">Coarser</th>
                    <th width="8%">RP 0.2%</th>                    
                </tr>
                <!-- row-->
                <tr class="mech_property">
                    <td width="6%">Required</td>
                    <th width="9%"></th>
                    <th colspan="9" class="mech_element_value"></th>
                    <td><?php echo $this->Form->input('required_hydro', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'required_hydro','name'=>'data[Dummycertificate][required_hydro]')); ?></td>
                    <td align="center">-</td>
                    <td align="center">-</td>                                        
                </tr>    
                <!-- row-->
                <tr> 
                <?php echo '<td rowspan="'.$itemcount.'" class="v-align">Testing Data</td>'; ?>                      
                    <? for($i=1;$i<= $itemcount;$i++){?>
                    <td class="text-center"><?php //echo $this->Form->input('heatno', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'heatno'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][heatno]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('tensile_strength', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'tensile_strength'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][tensile_strength]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('grain_size', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'grain_size'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][grain_size]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('yield_strength', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'yield_strength'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][yield_strength]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('elongation', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'elongation'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][elongation]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('flattening', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'flattening'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][flattening]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('flaring', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'flaring'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][flaring]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('eddy', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'eddy'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][eddy]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('ultrasonic', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'ultrasonic'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][ultrasonic]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('hardness', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'hardness'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][hardness]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('hydro', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'hydro'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][hydro]'));?></td>

                    <td class="text-center"><?php echo $this->Form->input('igc_a', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'igc_a'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][igc_a]'));?></td>
                    <td class="text-center"><?php echo $this->Form->input('igc_e', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'igc_e'.$i.'','name'=>'data[Dummycertificate][item]['.$i.'][igc_e]'));?></td>
                    </tr>
                <?}?>

                <!-- row-->
            </tbody>
        </table>
    </div>            
</div>          

<div class="row"><br/>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-1">Remarks</label>
            <div class="col-md-11">
                <?php echo $this->Form->textarea('remarks', array('cols'=>'2','rows'=>'5','class'=>'form-control','label'=>false));?>
            </div>
        </div>
    </div>                            
</div>                                            
                </div>
                <!-- END form-body class-->
                                        
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn green" type="submit">Submit</button>
                                     <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'dummycertificates'),array('class' => 'btn default')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> </div>
                    </div>
                </div>
        <?php echo $this->Form->end();?>
    </div>
</div>

<script type="text/javascript">

/*Fetch Certificate Number based on Change event of Date.*/
$(document).ready(function() {  
  $(document).on('change', '.changedatepicker',function(){
    var date = $('.changedatepicker').val(); 
    var current_fy = $('#fy').val();
    // var dateObject = $('.changedatepicker').datepicker('getDate');
    $.ajax({
        url: '<?php echo WEBSITE_PATH;?>app/calculateFinancialYearForDate/',
        type: 'POST',
        cache: false,
        data : "date="+date,
        success: function (new_fy) {
            /*Check new financial year is same as current financial year*/
            if(current_fy != new_fy){ 
            /*If not than fetch new auto increment number*/
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>dummycertificates/getCertificateNO/ajax/',
                    type: 'POST',
                    cache: false,
                    data : "date="+date,
                    success: function (data) {
                        $('.uniqueNo').val(data);
                    }
                });
            }/*End If*/
        }
      });
    });
});

/*Use : Get grade name to related Standard via ajax*/
$(document).on('change', '.std', function (event,selectedgradeid) {
  var id = $(this).val();
  var row_ids = $(this).attr('data-id');
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getGrades/'+id,
    type: 'POST',
    cache: false,
    success: function (data) {
      $('#grade_id'+row_ids).html(data);
      $("#grade_id"+row_ids+" option[value='"+selectedgradeid+"']").attr("selected", true);
      var vel = $('#grade_id'+row_ids+' option:selected').html();
      $('#select2-grade_id'+row_ids+'-container').html(vel);
      $('.std_spec').val($('.std').find(":selected").text());/*set standard text inside standard spec box*/
    }
  });
});
/*Use : Get size to related product category via ajax*/
$(document).on('change', '.procat', function (event) {
  var procat_value = $(this).val();
  var row_id = $(this).attr('data-id');
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/change_procat/'+procat_value,
    type: 'POST',
    cache: false,
    dataType : 'json',
    success: function (data) {     
      var rtrn = JSON.parse(JSON.stringify(data));
        if(rtrn.productname){ 
        $('.productname'+row_id).html('<b>Product Name</b> : '+rtrn.productname);
      }
      $('#unit'+row_id).val(rtrn.unit);
      $('#size_id'+row_id).html(rtrn.size);
      //$("#size_id"+row_id+" option[value='"+sel_size+"']").attr("selected", true);    
      //$('#size_id'+row_id).val(sel_size);
      //var sel2 = $('#size_id'+row_id+' option:selected').html();
      //$('#select2-size_id'+row_id+'-container').html(sel2); 
    }
  });
});

/*Use : Get property related grade via ajax*/
$(document).on('change', '.grd', function (event,selectedgradeid) {
    var id = $(this).val();
    $('.grd_spec').val($('.grd').find(":selected").text());/*set grade text inside grade spec box*/
    var itemcount = '<?php echo $itemcount;?>';

    /*Fetch selected grade's physical property value*/
    $.ajax({
        url: '<?php echo WEBSITE_PATH;?>Grades/getphysical/'+id+'/ajax',
        type: 'POST',
        cache: false,
        dataType : 'json',
        success: function (data) {
          $('#required_annealing_temp').val(data.Physical.annealing_c);
        }
    });

    /*Fetch selected grade's mechanical property value*/
    $.ajax({
        url: '<?php echo WEBSITE_PATH;?>Grades/getmechanical/'+id+'/ajax',
        type: 'POST',
        cache: false,
        dataType : 'json',
        success: function (data) {
          $('.mech_element_value').remove();
          $('.mech_property > th:nth-child(2)').after('<th class="mech_element_value">'+data.Mechanical.tensile_strength+'</th><th class="mech_element_value">'+data.Mechanical.grain_size+'</th><th class="mech_element_value">'+data.Mechanical.yield_strength+'</th><th class="mech_element_value">'+data.Mechanical.elongation+'</th><th class="mech_element_value">'+data.Mechanical.flattening+'</th><th class="mech_element_value">'+data.Mechanical.flaring+'</th><th class="mech_element_value">'+data.Mechanical.eddy+'</th><th class="mech_element_value">'+data.Mechanical.ultrasonic+'</th><th class="mech_element_value">'+data.Mechanical.hardness+'</th>');
        }
    });

    /*Fetch selected grade's Chemical Component and its minimum and maximum value*/
    $.ajax({
        url: '<?php echo WEBSITE_PATH;?>Grades/getchemical/'+id+'/ajax',
        type: 'POST',
        cache: false,
        dataType : 'json',
        success: function (data) {
            $('.chem_comp_value').remove();
            $('.chem_min_value').remove();
            $('.chem_max_value').remove();
            for (i = 1; i <= itemcount; i++) {
                $('.chem_comp_name_value'+i).remove();
            }
            $.each(data, function(index) {
                $('.chem_comp').append('<th class="chem_comp_value">'+data[index][0]+'</th>');
                $('.chem_min').append('<td class="chem_min_value text-center">'+data[index][1]+'</td>');
                $('.chem_max').append('<td class="chem_max_value text-center">'+data[index][2]+'</td>');

                for (i = 1; i <= itemcount; i++) {
                    $('.chem_comp_name'+i).append('<td class="chem_comp_name_value'+i+'"><div class="input text"><input type="text" class="form-control" name="data[Dummycertificate][item]['+i+']['+data[index][0]+']" required="required"></div></td>');
                }                
            });
        }
    });
});
</script>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>