<h3 class="page-title"> <?php echo $mainTitle;?></h3>

    <?php 
        echo $this->Html->link(
           '<i class="fa fa-file-excel-o"></i> Excel',
            array(
                'controller'=>'dummycertificates',
                'action'=>'export',
                $certi['Dummycertificate']['reftype'],
                $certi['Dummycertificate']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Download',
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );

        /*Set PDF Download link*/
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> PDF',
            array(
                'controller'=>'dummycertificates',
                'action'=>'generatepdf',
                'ext' => 'pdf',
                $certi['Dummycertificate']['reftype'],
                $certi['Dummycertificate']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'PDF',
                'title' => 'PDF',
                'download'=>'Test Dummycertificate-'.$certi['Dummycertificate']['id'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
      
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""></a></div>
    </div>
    <div class="portlet-body form">    
        
                <div class="certificate form-body">
<?php
if($certi['Dummycertificate']['reftype'] == 1){ $itemmodel = 'Chalanitem';
$mainmodel = 'Chalan';} else { $itemmodel = 'Invoiceitem'; $mainmodel = 'Invoice';}
$itemcount = count($certi['Dummycertificateitem']); /*Number of item in certificate*/

$grade_id = $certi['Dummycertificate']['grade_id']; /*Grade id of first item*/
$chem_req = $this->requestAction('Grades/getchemical/'.$grade_id);
$chem_comp = ''; /*Chemical component*/
$chem_min = ''; /*Chemical component Min value*/
$chem_max = ''; /*Chemical component Max value*/
$chem_comp_name = array();
foreach ($chem_req as $value) {
    $chem_comp_name[] = $value[0]; 
    $chem_comp .= '<th>'.ucfirst($value[0]).'</th>';
    $chem_min .= '<td class="text-center">'.$value[1].'</td>';
    $chem_max .= '<td class="text-center">'.$value[2].'</td>';
}
$mech_req = $this->requestAction('Grades/getmechanical/'.$grade_id);
$phy_req = $this->requestAction('Grades/getphysical/'.$grade_id);
$ord = ''; 
$chem = ''; /*Chemical value measured of particular item*/
$mech = ''; /*Physical value measured of particular item*/
$product_detail = ''; /*Product name,size etc of item*/

foreach($certi['Dummycertificateitem'] as $item){
    $keyid = $item['id'];
    $ord.= $this->Form->input('id', array('class'=>'form-control','type'=>'hidden','label'=>false,'required'=>true,'value'=>$item['id'],'name'=>'data[Dummycertificate][Dummycertificateitem]['.$keyid.'][id]'));

    $product_detail .= '<tr><td class="text-center">'.$item['sr_no'].'</td><td width="35%" class="text-center">'.$item['Productcategory']['productname'].'</td><td width="35%" class="text-center">'.$this->requestAction('Sizes/getsizedimension/'.$item['size_id'].'/'.$item['length'].'/2').'</td></tr>';
    
    $ord.= '<td class="text-center">'.$item['sr_no'].'</td>';
    $ord.= '<td>'.$item['heatno'].'</td>';
    $ord.= '<td>'.$item['clientheatno'].'</td>';    
    $ord.= '<td>'.$item['testref_no'].'</td>';
    $ord.= '<td>'.$item['surface_finish'].'</td>';
    $ord.= '<td>'.$item['dimension'].'</td>';
    $ord.= '<td class="numalign">'.$item['length'].'</td>';
    $ord.= '<td class="numalign">'.$item['qty_mtr'].'</td>';
    $ord.= '<td class="numalign">'.$item['netweight'].'</td>';
    $ord.= '</tr>';

    $chem.= '<tr align="center">';
    $chem.= '<td>'.$item['heatno'].'</td>';
    $chem.= '<td>Observed</td>';
    foreach ($chem_comp_name as $val) {
        $chem.= '<td>'.$item[$val].'</td>';
    }    
    $chem.= '</tr>';

    $mech.= '<td>'.$item['heatno'].'</td>';
    $mech.= '<td>'.$item['tensile_strength'].'</td>';
    $mech.= '<td>'.$item['grain_size'].'</td>';
    $mech.= '<td>'.$item['yield_strength'].'</td>';
    $mech.= '<td>'.$item['elongation'].'</td>';
    $mech.= '<td>'.$item['flattening'].'</td>';
    $mech.= '<td>'.$item['flaring'].'</td>';
    $mech.= '<td>'.$item['eddy'].'</td>';
    $mech.= '<td>'.$item['ultrasonic'].'</td>';
    $mech.= '<td>'.$item['hardness'].'</td>';
    $mech.= '<td>'.$item['hydro'].'</td>';
    $mech.= '<td>'.$item['igc_a'].'</td>';
    $mech.= '<td>'.$item['igc_e'].'</td>';
    $mech.= '</tr>';
}
?>
<div class="table-responsive">
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="15%">Certificate No</th>
                    <td width="35%"><?php echo $certi['Dummycertificate']['certificateno'];?></td>
                    <th width="15%">Issue Date</th>
                    <td width="35%"><?php echo date('d/m/Y',strtotime($certi['Dummycertificate']['issuedate'])); ?></td>
                </tr>             
                <tr role="row">
                    <th width="15%">Client</th>
                    <td width="35%"><?php echo $certi['Client']['company_name'];?></td>
                    <th width="15%">Contract</th>
                    <td width="35%"><?php echo $certi['Dummycertificate']['clientpo'];?>  Dt: <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($certi['Dummycertificate']['orderdate']));?></td>
                </tr>
                <!-- row-->
                <tr role="row">
                    <th width="15%">Specification</th>
                    <td width="35%"><?php echo $certi['Dummycertificate']['std_spec'];?></td>
                    <th width="15%">Grade</th>
                    <td width="35%"><?php echo $certi['Dummycertificate']['grd_spec'];?></td>
                </tr>
                <!-- row-->
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody> 
                <tr role="row">
                    <th width="20%">P.O. Sr No</th>
                    <th width="40%">Product</th>
                    <th width="40%">Size</th>
                </tr>
                <?php echo $product_detail;?>
                <!-- row-->                      
            </tbody>
        </table>
    </div>    
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="10%">Supply <br/>Conditions</th>
                    <th width="5%">P.O. <br/>SR. NO</th>
                    <th width="10%">System <br/>Heat No</th>
                    <th width="10%">Client <br/>Heat No</th>
                    <!-- <th width="10%">Print <br/>Heat No</th> -->
                    <th width="10%">Test Ref.<br/> No</th>
                    <th width="10%">Surface Finish</th>
                    <th width="10%">Dimension check & <br/>pmi check</th>
                    <th width="7%">Total Length</th>
                    <th width="7%">Total Quantity</th>
                    <th width="7%">Total Net Weight</th>
                </tr>
                <tr role="row">
                    <td rowspan="<?php echo $itemcount ;?>" class="v-align"><?php echo $certi['Dummycertificate']['supply_condition'];?></td>
                    <?php echo $ord;?>
                    <!-- row-->                  
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%">Solution Annealing Temp.</th>
                    <th width="85%" colspan="10">Chemical Composition %</th>
                </tr>
                <tr role="row">
                    <td rowspan="<?php echo $itemcount + 3;?>" class="v-align"><?php echo $certi['Dummycertificate']['required_annealing_temp']; ?></td>            
                    <th width="10%">Heat No</th>
                    <th width="8%"></th>
                    <?php echo $chem_comp;?>
                </tr>    
                <!-- row-->
                <tr>
                    <td></td>
                    <th>Minimum</th>
                    <?php echo $chem_min;?>
                </tr>    
                <!-- row-->
                <tr>
                    <td></td>
                    <th>Maximum</th>
                    <?php echo $chem_max;?>
                </tr>    
                <!-- row-->
                <?php echo $chem;?>
            </tbody>
        </table>
    </div>
    <div class="table-scrollable">
        <table id="datatable_ajax" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" aria-describedby="datatable_ajax_info" role="grid">
            <tbody>
                <tr role="row">
                    <th width="15%" colspan="2" rowspan="2">Testing Item</th>
                    <th width="8%">Tensile <br/>Strength</th>
                    <th width="8%">GRAIN <br/>SIZE</th>
                    <th width="8%">Yield <br/>Strength </th>
                    <th width="8%" rowspan="2">Elongation %</th>
                    <th width="8%" rowspan="2">Flattening <br/>Test</th>
                    <th width="8%" rowspan="2">Flaring <br/>Test</th>
                    <th width="8%" rowspan="2">Eddy Current <br/>Test</th>
                    <th width="8%" rowspan="2">Ultrasonic <br/>Test</th>
                    <th width="7%" rowspan="2">Hardness <br/>HRB</th>
                    <th width="6%" rowspan="2">Hydro Test <br/>(Pressure) PSI</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice A</th>
                    <th width="8%" rowspan="2">IGC - Test<br/>Practice E</th>
                </tr>
                <tr role="row">
                    <th width="8%">(MPA)</th>
                    <th width="8%">Coarser</th>
                    <th width="8%">RP 0.2%</th>                    
                </tr>
                <!-- row-->
                <tr>
                    <td width="6%">Required</td>
                    <th width="9%"><?php echo $certi['Dummycertificate']['grd_spec'];?></th>
                    <td><?php echo $mech_req['Mechanical']['tensile_strength'];?></td>
                    <td><?php echo $mech_req['Mechanical']['grain_size'];?></td>
                    <td><?php echo $mech_req['Mechanical']['yield_strength'];?></td>
                    <td><?php echo $mech_req['Mechanical']['elongation'];?></td>
                    <td><?php echo $mech_req['Mechanical']['flattening'];?></td>
                    <td><?php echo $mech_req['Mechanical']['flaring'];?></td>
                    <td><?php echo $mech_req['Mechanical']['eddy'];?></td>
                    <td><?php echo $mech_req['Mechanical']['ultrasonic'];?></td>
                    <td><?php echo $mech_req['Mechanical']['hardness'];?></td>
                    <td><?php echo $certi['Dummycertificate']['required_hydro'];?></td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                </tr>    
                <!-- row-->
                <tr> 
                <?php echo '<td rowspan="'.$itemcount.'" class="v-align">Testing Data</td>'; ?>                      
                    <?php echo $mech;?>
                <!-- row-->
            </tbody>
        </table>
    </div>            
</div>                                                      
            
<div class="row"><br/>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-1"><b>Remarks</b></label>
            <div class="col-md-11">
                <?php //echo $this->Form->textarea('remarks', array('cols'=>'2','rows'=>'5','class'=>'form-control','label'=>false,'value'=>$certi['Dummycertificate']['remarks']));?>
                <?php echo $certi['Dummycertificate']['remarks'];?>
            </div>
        </div>
    </div>                            
</div>
<table id="datatable_ajax" width="100%"  class="table table-bordered table-striped table-condensed flip-content view_invoice">
        <tr class="print_none">
            <td colspan="2"><b>Createdby</b></td>
            <td colspan="7"> <?php echo  $this->requestAction('Users/getUsername/'.$certi['Dummycertificate']['createdby']);?></td>
        </tr>
        <tr>
            <td colspan="2"><b>Created on</b></td>
            <td colspan="7"> <?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($certi['Dummycertificate']['created']));?></td>
        </tr>
        <tr>
            <td colspan="2"><b>Modified by</b></td>
            <td colspan="7"> <?php echo $this->requestAction('Users/getUsername/'.$certi['Dummycertificate']['modifiedby']);?></td>
        </tr>
        <tr>
            <td colspan="2"><b>Modified on</b></td>
            <td colspan="7"> <?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($certi['Dummycertificate']['modified']));?></td>
        </tr>
        <?php if($certi['Dummycertificate']['verifiedby'] == 1){ ?>
        <tr>
            <td colspan="2"><b>Verified by</b></td>
            <td colspan="7"><?php echo $this->requestAction('Users/getUsername/'.$certi['Dummycertificate']['verifiedby']);?></td>
        </tr>
        <tr>
            <td colspan="2"><b>Verified on</b></td>
            <td colspan="7">  <?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($certi['Dummycertificate']['verified_date']));?></td>
        </tr>
        <?php } ?>
    </table>

            </div>
    
            <!-- END form-body class-->
                                    
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">                                    
                                 <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'dummycertificates'),array('class' => 'btn default')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>

    </div>
</div>