<?php
  /* 
   * Paging
   */
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($code as $codes) {   
      $record_nextstage = $this->requestAction(
        array('controller' => 'App', 'action' => 'check_nextstage'),
        array('dependantmodel' => 'Productcategory','conditional_parameter' => 'Productcategory.hscode_id = '.$codes['Hscode']['id'])
      );
     
      if(empty($record_nextstage)){
        $delete_record = '<a href="'.WEBSITE_PATH.'hscodes/delete/'.base64_encode($codes['Hscode']['id']).'" class="btn btn-danger" title="Delete HS Code" id="delete" data-confirm="Are you sure to delete this HS Code?"><span class="glyphicon glyphicon-trash"></span></a>';
        $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$codes['Hscode']['id'].'">';
      }else{
        $delete_record = '';
        $checkbox_delete = '';
      }
      $records["data"][] = array($checkbox_delete,
      $codes['Hscode']['hscode'],
      $codes['Hscode']['hscode_alias'],
      $codes['Hscode']['rate'],
      $codes['Hscode']['description'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($codes['Hscode']['modified'])),
      $codes['User']['first_name']." ".$codes['User']['last_name'],
      '<a href="'.WEBSITE_PATH.'hscodes/add/'.base64_encode($codes['Hscode']['id']).'" class="btn btn-success" title="Edit HS Code"><span class="glyphicon glyphicon-pencil"></span></a>'.$delete_record);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>