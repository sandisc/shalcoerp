
<h3 class="page-title"> <?php echo $mainTitle;?></h3>
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption"><?php echo $pageTitle;?></div>
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                             <?php echo $this->Form->create('Hscode',array('class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/')); 
                                              echo $this->Form->input('id', array('type'=>'hidden'));
                                                /*set target if this form open in modal dialoag box instead of separate window*/
                                                if(isset($this->params['named']['targetid'])){
                                                    echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                                }?>
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">HS Code<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php echo $this->Form->input('hscode', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Value - Rate(%)<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php echo $this->Form->input('rate', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>
                                                            <!-- Hscode Alias -->
                                                            <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">HS Code Alias<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php echo $this->Form->input('hscode_alias', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Description</label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php echo $this->Form->input('description', array('type' => 'textarea','label'=>false,'class'=>'form-control','cols'=>'2','rows'=>'3'));?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">    
                                                                        <button class="btn green" type="submit">Submit</button>
                                                                        <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'hscodes'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                    <?php echo $this->Form->end();?>                                              
                                                <!-- END FORM-->
                                            </div>
                                        </div>
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>                                        