<?php
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($industry as $industry) {
      $record_nextstage = $this->requestAction(
        array('controller' => 'App', 'action' => 'check_nextstage'),
        array('dependantmodel' => 'Client','conditional_parameter' => 'FIND_IN_SET(\''. $industry['Industry']['id'] .'\',Client.industryid)')
      );
      if(empty($record_nextstage)){
        $delete_record = '<a href="'.WEBSITE_PATH.'industries/delete/'.base64_encode($industry['Industry']['id']).'" class="btn btn-danger" title="Delete Industry" id="delete" data-confirm="Are you sure to delete this industry?"><span class="glyphicon glyphicon-trash"></span></a>';
        $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$industry['Industry']['id'].'">';
      }else{
        $delete_record = '';
        $checkbox_delete = '';
      }      
      $records["data"][] = array($checkbox_delete,
      $industry['Industry']['industryname'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($industry['Industry']['modified'])),
      $industry['User']['first_name']." ".$industry['User']['last_name'],
      '<a href="'.WEBSITE_PATH.'industries/add/'.base64_encode($industry['Industry']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>'.$delete_record);
    }
 
  if(isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
   // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>