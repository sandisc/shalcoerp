<h3 class="page-title"> <?php echo $mainTitle;?></h3>
                  
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools">
            <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
     <?php echo $this->Form->create('Industry',array('class'=>'shalcoform'));
     echo $this->Form->input('id', array('type'=>'hidden')); ?> 
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Industry Name<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-8 input-icon right">
                                <?php echo $this->Form->input('industryname', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                <span class="help-block"><br></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">                        
                                <button class="btn green" type="submit">Submit</button>
                                 <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'industries'),array('class' => 'btn default')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
            <?php echo $this->Form->end();?>      
        <!-- END FORM-->
    </div>
</div>