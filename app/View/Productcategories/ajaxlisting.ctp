<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

 
    foreach ($productcategory as $productcategory) {
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$productcategory['Productcategory']['id'].'">',
      $productcategory['Productcategory']['productname'],
      $productcategory['Productcategory']['name'],
      $productcategory['Producttaxonomy']['name'].'-'.$productcategory['Producttaxonomy']['unit'],
      $productcategory['Hscode']['hscode'],
      $productcategory['Productcategory']['description'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($productcategory['Productcategory']['modified'])),
      $productcategory['User']['first_name']." ".$productcategory['User']['last_name'],
       '<a href="'.WEBSITE_PATH.'productcategories/add/'.base64_encode($productcategory['Productcategory']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'productcategories/delete/'.base64_encode($productcategory['Productcategory']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this size?"><span class="glyphicon glyphicon-trash"></span></a>',);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>