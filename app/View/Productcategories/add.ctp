                            <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
                            <div class="portlet box green">
                              <div class="portlet-title">
                                  <div class="caption">
                                      <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                  <div class="tools">
                                      <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                  </div>
                              </div>
                              <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?php echo $this->Form->create('Productcategory',array('class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                                    echo $this->Form->input('id', array('type'=>'hidden')); 
                                    if(isset($this->params['named']['targetid'])){
                                        echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                    }?> 
                                    <div class="form-body">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label col-md-4">Product Category Name<span class="required" aria-required="true"> * </span></label>
                                                  <div class="col-md-8"><?php
                                                 echo $this->Form->input('productname', array('type' => 'text','class'=>'form-control odmm','label'=>false,'required'=>true));
                                                  ?>
                                                  <span class="help-block"><br></span>
                                                  </div>
                                              </div>
                                            </div>

                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Product Category Alias<span class="required" aria-required="true"> * </span></label>                                                    
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('name', array('type' => 'text','class'=>'form-control odnb','label'=>false,'required'=>true));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                  <label class="control-label col-md-4">Product Taxonomy<span class="required" aria-required="true"> * </span></label>                                                    
                                                  <div class="col-md-8"><?php echo $this->Form->input('producttaxonomy_id', array('type' => 'select','class'=>'form-control producttaxonomy','label'=>false,'required'=>true,'options'=>$taxonomy,'empty'=>'Select Product taxonomy'));?>
                                                  <span class="help-block"><br></span>
                                                  </div>
                                                </div>
                                            </div>                                                        
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label class="control-label col-md-4">HS Code<span class="required" aria-required="true"> * </span></label>                                                   
                                                  <div class="col-md-5">
                                                    <?php echo $this->Form->input('hscode_id',array('type'=>'select','label'=>false,'required'=>true,'class'=>'form-control hscode','options'=>$hscode,'empty'=>'Select HS code','id'=>'hscode_id'));
                                                    ?>
                                                      <span class="help-block"><br></span>
                                                  </div>
                                                  <div class="col-md-3 checkbox margin0px">
                                                      <a onclick="addHSCode()" data-target="#addHSCodeModal" data-toggle="modal">Add HS Code</a> 
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Description</label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('description', array('type'=>'textarea','cols'=>'2','rows'=>'2','class'=>'form-control description','label'=>false));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>                                                         
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-8">
                                                        <button class="btn green" type="submit">Submit</button>
                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'productcategories'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                 <?php echo $this->Form->end();?>                                        
                                <!-- END FORM-->
                                </div>
                            </div>
<script type="text/javascript">
/*Add HS Code using modal dialog*/
function addHSCode() {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>hscodes/add/targetid:hscode_id',
    type: 'POST',
    success: function(data) {
      $('#addHSCodeModal .previewText').html(data);
      $('#addHSCodeModal .page-title').remove();
      $('#addHSCodeModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}
</script>
<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addHSCodeModalLabel" role="dialog" tabindex="-1" id="addHSCodeModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>