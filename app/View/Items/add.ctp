
<h3 class="page-title"> <?php echo $mainTitle;?>
                        <!--<small>form layouts</small>-->
                    </h3>
                  
<div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                             <?php echo $this->Form->create('Item');
                                              echo $this->Form->input('id', array('type'=>'hidden')); ?> 
                                                    <div class="form-body">
                                                       
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Select Standard<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                    <?php
                                                                       echo $this->Form->input('standard_id', array('class'=>'form-control','type'=>'select','options'=>$options,'label'=>false,'required'=>true,'id'=>'standard'));

                                                                        ?>

                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Select Grade<span class="required" aria-required="true"> * </span></label>
                                                                         <div class="col-md-9">
                                                                <?php

                                                                   echo $this->Form->input('grade_id', array('type' => 'select','class'=>'form-control','options'=>array('-'=>'Select Grade',),'label'=>false,'required'=>true,'id'=>'grade'));
                                                                    ?>
                                                            
                                                                       
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">OD (NB)<span class="required" aria-required="true"> * </span></label>
                                                                    
                                                                    <div class="col-md-9">
                                                                        <?php

                                                                   echo $this->Form->input('od_nb', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'id'=>'odnb'));
                                                                    ?>
                                                                         <span class="help-block"> <br></span>
                                                                          </div>

                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">OD (MM)<span class="required" aria-required="true"> * </span></label>
                                                                    
                                                                    <div class="col-md-9"><?php

                                                                   echo $this->Form->input('od_mm', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'id'=>'odmm'));
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">WT (NB)<span class="required" aria-required="true"> * </span></label>
                                                                   
                                                                    <div class="col-md-9">
                                                                <?php 
                                                                      echo $this->Form->input("wt_nb",array('type'=>'text','label'=>false,'required'=>true,'class'=>'form-control','id'=>'wtnb'));
                                                                    ?>
                                                                      <span class="help-block"><br></span>
                                                                  </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                             <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">WT (MM)<span class="required" aria-required="true"> * </span></label>
                                                                    
                                                                    <div class="col-md-9">   <?php

                                                                   echo $this->Form->input('wt_mm', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'id'=>'wtmm'));
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Length<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                        <?php

                                                                   echo $this->Form->input('length', array('label'=>false,'required'=>true,'type'=>'text','class'=>'form-control','id'=>'length'));
                                                                    ?>
                                                                         <span class="help-block"><br></span>
                                                                          </div>

                                                                </div>
                                                            </div>

                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Total <span class="required" aria-required="true"></span></label>
                                                                    <div class="col-md-9">   <?php

                                                                   echo $this->Form->input('calc_ratio', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'total','readonly'=>'readonly'));
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>

                                                        
                                                         <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Good Description (NB)<span class="required" aria-required="true"></span></label>
                                                                    <div class="col-md-9">
                                                                        <?php

                                                                   echo $this->Form->input('gdnb', array('label'=>false,'required'=>true,'class'=>'form-control','id'=>'goodnb','readonly' => 'readonly'));
                                                                    ?>
                                                                         <span class="help-block"><br></span>
                                                                          </div>

                                                                </div>
                                                            </div>

                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Good Description (MM) <span class="required" aria-required="true"></span></label>
                                                                    <div class="col-md-9">   <?php

                                                                   echo $this->Form->input('gdmm', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'goodmm','readonly' => 'readonly'));
                                                                    ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <!--/row-->
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                

                                                                        <button class="btn green" type="submit">Submit</button>
                                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'items'),array('class' => 'btn default')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                    <?php echo $this->Form->end();?>
                                              
                                                <!-- END FORM-->
                                            </div>

                                        </div>

<script type="text/javascript">
  $('#standard').change(function () {
         var id = $(this).val();
        $.ajax({
            url: '<?php echo WEBSITE_PATH;?>items/getGrades/'+id,
            type: 'POST',
            cache: false,
           success: function (data) {
        $('#grade').html(data);
    }
 });
});
function compute() {
          var odmm = $('#odmm').val();
          var wt = $('#wtmm').val();
          var total =  parseFloat(odmm) -  parseFloat(wt);
          var mult =  parseFloat(total) * parseFloat(wt);
          var final = parseFloat(mult) * parseFloat(0.0248);
          $('#total').val(final);
}
$('#odmm, #wtmm').keyup(compute); 

function goodnb() {
          var odnb = $('#odnb').val();
          var wtnb = $('#wtnb').val();
          var length = $('#length').val();
          var convert = length * parseInt(1000);
          var desc = odnb+'" X '+wtnb+' X '+length+' MTR';
          console.log(desc);
           $('#goodnb').val(desc);
        }
$('#length').keyup(goodnb);

function goodmm() {
          var odmm = $('#odmm').val();
          var wtmm = $('#wtmm').val();
          var length = $('#length').val();
          var convert = length * parseInt(1000);
          var desc = odmm+' MM X '+wtmm+' MM X '+convert+' MM';
          
           $('#goodmm').val(desc);
        }
$('#length').keyup(goodmm);
</script>