<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

 
    foreach ($size as $sizes) {
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$sizes['Size']['id'].'">',
      $sizes['Producttaxonomy']['name'],
      $sizes['Size']['gdmm'],
      $sizes['Size']['gdnb'],
      $sizes['Size']['calc_ratio'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($sizes['Size']['modified'])),
      $sizes['User']['first_name']." ".$sizes['User']['last_name'],
       '<a href="'.WEBSITE_PATH.'sizes/add/'.base64_encode($sizes['Size']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'sizes/delete/'.base64_encode($sizes['Size']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this size?"><span class="glyphicon glyphicon-trash"></span></a>',);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>