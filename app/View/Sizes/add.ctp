                            <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
                            <div class="portlet box green">
                              <div class="portlet-title">
                                  <div class="caption">
                                      <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                  <div class="tools">
                                      <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                  </div>
                              </div>
                              <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?php /*added 'novalidate' => true, for remove default validation class added by cakephp*/
                                echo $this->Form->create('Size',array('novalidate' => true,'class'=>'shalcopopupajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                                    echo $this->Form->input('id', array('type'=>'hidden')); 
                                    /*set target if this form open in modal dialoag box instead of separate window*/
                                    if(isset($this->params['named']['targetid'])){
                                        echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                    }?> 
                                    <div class="form-body">
                                      <div class="row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Product Taxonomy<span class="required" aria-required="true"> * </span></label>
                                                    <div class="col-md-3">
                                                      <?php echo $this->Form->input('producttaxonomy_id', array('label'=>false,'type'=>'select','class'=>'form-control taxonomy','options'=>$taxonomy,'empty'=>'Select Product Taxonomy'));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Pipes Div -->

                                      <div class="col-md-12 pipes bs-callout bs-callout-info " id="callout-help-text-accessibility" 
                                      <?php if(isset($this->request->data['Size']['id'])){
                                              if($this->request->data['Size']['producttaxonomy_id'] == 1 || $this->request->data['Size']['producttaxonomy_id'] == 2){ 
                                                  echo 'style=display:block;';
                                                }
                                                else{ 
                                                  echo 'style=display:none;';
                                                }
                                              }
                                              else{
                                                echo 'style=display:none;';
                                            }?>>
                                        <h4 class="bold">Parameters</h4>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                  <label class="control-label col-md-6">OD (MM)</label>
                                                        <?php
                                                 echo $this->Form->input('od_mm', array('type' => 'text','class'=>'form-control odmm','label'=>false));
                                                  ?>
                                                  <span class="help-block"></span>
                                                </div>
                                              </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6">OD (NB)</label>
                                                        <?php echo $this->Form->input('od_nb', array('type' => 'text','class'=>'form-control odnb','label'=>false));?>
                                                        <span class="help-block"></span>
                                                </div>
                                            </div>
                                           <div class="col-md-3">
                                                <div class="form-group">
                                                  <label class="control-label col-md-6">WT (MM)</label>
                                                        <?php
                                                 echo $this->Form->input('wt_mm', array('type' => 'text','class'=>'form-control wtmm','label'=>false));
                                                  ?>
                                                        <span class="help-block"></span>
                                                </div>
                                              </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6">WT (NB)</label>
                                                        <?php echo $this->Form->input('wt_nb', array('type' => 'text','class'=>'form-control wtnb','label'=>false));?>
                                                        <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                      </div>

                                      <!-- Plates Div -->
                                      <div class="col-md-12 plates bs-callout bs-callout-info " id="callout-help-text-accessibility"
                                      <?php if(isset($this->request->data['Size']['id'])){
                                              if($this->request->data['Size']['producttaxonomy_id'] == 3){ 
                                                  echo 'style=display:block;';
                                                }
                                                else{ 
                                                  echo 'style=display:none;';
                                                }
                                              }
                                              else{
                                                echo 'style=display:none;';
                                            }?>>
                                        <h4 class="bold">Parameters</h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                  <label class="control-label col-md-6">Thickness (MM)</label>
                                                  <?php echo $this->Form->input('thickness', array('type' => 'text','class'=>'form-control thickness','label'=>false));?>
                                                  <span class="help-block"></span>
                                                </div>
                                              </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6">Width (MM)</label>
                                                        <?php echo $this->Form->input('width', array('type' => 'text','class'=>'form-control width_mm','label'=>false));?>
                                                        <span class="help-block"></span>
                                                </div>
                                            </div>
                                           <div class="col-md-4">
                                                <div class="form-group">
                                                  <label class="control-label col-md-6">Length (MM)</label>
                                                  <?php echo $this->Form->input('length', array('type' => 'text','class'=>'form-control length','label'=>false));?>
                                                    <span class="help-block"></span>
                                                </div>
                                              </div>
                                           </div>
                                      </div>

                                      <!-- Bar Div -->
                                      <div class="col-md-12 bar bs-callout bs-callout-info" id="callout-help-text-accessibility"
                                      <?php if(isset($this->request->data['Size']['id'])){
                                              if($this->request->data['Size']['producttaxonomy_id'] == 6){ 
                                                  echo 'style=display:block;';
                                                }
                                                else{ 
                                                  echo 'style=display:none;';
                                                }
                                              }
                                              else{
                                                echo 'style=display:none;';
                                            }?>>                                      
                                        <h4 class="bold">Parameters</h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                  <label class="control-label col-md-6">Diameter (MM)</label>
                                                        <?php
                                                 echo $this->Form->input('diameter', array('type' => 'text','class'=>'form-control diameter','label'=>false));
                                                  ?>
                                                        <span class="help-block"></span>
                                                </div>
                                              </div>
                                           </div>
                                      </div>

                                      <div class="col-md-12 row"><br/></div>
                                         <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Good Description (MM) <span class="required" aria-required="true"> * </span></label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('gdmm', array('class'=>'form-control gdmm','label'=>false));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>                                                         
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Good Description (NB)</label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('gdnb', array('label'=>false,'class'=>'form-control gdnb'));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label class="control-label col-md-4">Ratio <span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-8">
                                                  <?php echo $this->Form->input('calc_ratio', array('class'=>'form-control ratio','type'=>'text','label'=>false)); ?>
                                                  <span class="help-block"><br></span>
                                                </div>
                                              </div>
                                            </div>         
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-8">
                                                        <button class="btn green" type="submit">Submit</button>
                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'sizes'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                 <?php echo $this->Form->end();?>                                        
                                <!-- END FORM-->
                                </div>
                            </div>

<script type="text/javascript">
 function compute() {
          var odmm = $('.odmm').val();
          var wt = $('.wtmm').val();
          var total =  parseFloat(odmm) -  parseFloat(wt);
          var mult =  parseFloat(total) * parseFloat(wt);
          var final = parseFloat(mult) * parseFloat(0.00314);
          if(!isNaN(total) && total.length != 0) {
            $('.ratio').val(final);
          }
}
$('.odmm, .wtmm').keyup(compute); 

function goodnb() {
    var odnb = $('.odnb').val();
    var wtnb = $('.wtnb').val();
    var desc = odnb+' X '+wtnb;
    //var desc = parseFloat(odnb).toFixed(2)+' X '+parseFloat(wtnb).toFixed(2);
    $('.gdnb').val(desc);
}
$('.odnb,.wtnb').keyup(goodnb);

function goodmm() { 
    var odmm = $('.odmm').val(); 
    var wtmm = $('.wtmm').val();
/*  var length = $('#length').val();
    var convert = length * parseInt(1000);*/
    var desc = parseFloat(odmm).toFixed(2)+' MM X '+parseFloat(wtmm).toFixed(2)+' MM';          
    $('.gdmm').val(desc);
}
$('.odmm,.wtmm').keyup(goodmm);


function plates() { 
    var thickness = $('.thickness').val(); 
    var width = $('.width_mm').val();
    var lengths = $('.length').val();
    var count = parseFloat(thickness) * (parseFloat(width) / parseInt(1000));
    var final_count = parseFloat(count) * (parseFloat(lengths) / parseInt(1000));
    var all_concat = parseFloat(thickness).toFixed(2)+ ' MM X '+parseFloat(width).toFixed(2)+' MM X '+parseFloat(lengths).toFixed(2)+' MM';
    //var all_concat = thickness+ 'MM X '+width+' MM X '+lengths+' MM';
    $('.gdmm').val(all_concat);
    if(!isNaN(final_count) && final_count.length != 0) {
     $('.ratio').val(final_count);
    }
}
$('.thickness,.width_mm,.length').keyup(plates);
/* Bars calculation */
function bars() { 
    var diameter = $('.diameter').val();
    var double_val = parseFloat(diameter) * parseFloat(diameter);
    //$('.gdmm').val(diameter);
    $('.gdmm').val(parseFloat(diameter).toFixed(2));
    $('.ratio').val(double_val * parseFloat(0.000785));
}
$('.diameter').keyup(bars);

  $(document).ready(function(){
      $('#SizeProducttaxonomyId').on('change', function() {
        $(".gdmm").val('');
        $(".gdnb").val('');
        $(".ratio").val('');
        $(".pipes").css('display', (this.value == '1' || this.value == '2') ? 'block' : 'none');
        $(".plates").css('display', (this.value == '3') ? 'block' : 'none');
        $(".bar").css('display', (this.value == '6') ? 'block' : 'none');
      });
  });
</script>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/customajaxpopup.js" type="text/javascript"></script>
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<!-- <script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script> -->
<?php } ?>