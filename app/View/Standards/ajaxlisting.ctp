<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

    foreach ($standard as $standards) {
       $record_nextstage = $this->requestAction(
                  array('controller' => 'App', 'action' => 'check_nextstage'),
                  array('dependantmodel' => 'Proformaitem','conditional_parameter' => 'Proformaitem.standard_id = '.$standards['Standard']['id'])
    );
      $record_order_stage = $this->requestAction(
                  array('controller' => 'App', 'action' => 'check_nextstage'),
                  array('dependantmodel' => 'Orderitem','conditional_parameter' => 'Orderitem.standard_id = '.$standards['Standard']['id'])
    ); 

      if(empty($record_nextstage)){          
        if(empty($record_order_stage)){
          $delete_record = '<a href="'.WEBSITE_PATH.'standards/delete/'.base64_encode($standards['Standard']['id']).'" class="btn btn-danger" title="Delete Standard" id="delete" data-confirm="Are you sure to delete this standard?"><span class="glyphicon glyphicon-trash"></span></a>';
          $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$standards['Standard']['id'].'">';
        }
        else{
            $delete_record = '';
            $checkbox_delete = '';
        }
      }else{
         $delete_record = '';
         $checkbox_delete = '';
      }

      $records["data"][] = array($checkbox_delete,
      $standards['Standard']['standard_name'],
      $this->requestAction('/standards/getProcat/'.$standards['Standard']['procatid']),
      $standards['Standard']['description'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($standards['Standard']['modified'])),
      $standards['User']['first_name']." ".$standards['User']['last_name'],
      '<a href="'.WEBSITE_PATH.'standards/add/'.base64_encode($standards['Standard']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>'.$delete_record);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }*/

  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
 
    $records["draw"] = $sEcho;
    $records["recordsTotal"] = $iTotalRecords;
    $records["recordsFiltered"] = $iTotalRecords;
    
    echo json_encode($records); exit;
?>