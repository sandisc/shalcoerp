<style>
.checkbox {
  float: left;
  margin: 2px 0 !important;
  width: 23%;
}
.multicheck {
  padding-bottom: 15px;
}
</style>
<h3 class="page-title"> <?php echo $mainTitle;?></h3>                  
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
             <?php echo $this->Form->create('Standard',array('class'=>'shalcoform'));
             echo $this->Form->input('id', array('type'=>'hidden')); ?> 
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Standard Name <span class="required" aria-required="true">*</span></label>
                                <div class="col-md-9 input-icon right">
                                    <?php echo $this->Form->input('standard_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9 input-icon right">
                                    <?php echo $this->Form->textarea('description', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false));?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Terms and Condition <span class="required" aria-required="true">*</span></label>
                                <div class="col-md-9 input-icon right">
                                    <?php echo $this->Form->textarea('termsandcondition', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false));?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>                                                    
                    </div>
                      <div class="row">
                         <div class="col-md-2">
                            <label for="control-label col-md-3" style="font-weight: bold;">Product Category *</label>
                         </div>
                        <div class="col-md-10 hide_row multicheck">
                            <div class="multiselect">
                                <input name="data[Standard][procatid]" value="" id="proModuleID" type="hidden">
                                <?php 
                                    $selected = array();
                                    if(isset($this->request->data['Standard'])){
                                        $selected= explode(',',$this->request->data['Standard']['procatid']);
                                    }
                                $i = 1;
                                foreach($category as $k =>$v){ ?>
                                <div class="checkbox">
                                    <input name="data[Standard][procatid][]" value="<?php echo $k;?>" id="procat<?php echo $i;?>" type="checkbox" <?php if(in_array($k,$selected)){ ?> checked="checked"<?php } ?>>
                                    <label for="proId<?php echo $i;?>"><?php echo $v;?></label>
                                </div>
                                <?php $i++;} ?>
                                <span class="help-block"><br></span>
                                
                            </div>
                        </div>
                    </div>   
                </div>
                
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">    
                                    <button class="btn green" type="submit">Submit</button>
                                     <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'standards'),array('class' => 'btn default')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> </div>
                    </div>
                </div>
                <?php echo $this->Form->end();?>              
                <!-- END FORM-->
            </div>
        </div>