<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
    <div class="col-md-12">
     <?php echo $this->Session->flash();?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                   <i class="icon-settings font-dark"></i>
                   <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                      <?php echo $this->Html->link(' Add Bank ',array('action' => 'add','controller' =>'banks'),array('class' => 'fa fa-plus btn sbold green')); ?>
                    </div>
                    <div class="btn-group">
                    </div>
                </div>
            </div>
             
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <span> </span>
                        <select class="table-group-action-input form-control input-inline input-small input-sm" id ="sel_ct">
                            <option value="">Select...</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                            <option value="delete">Delete</option>
                        </select>
                        <button class="btn btn-sm green table-group-action-submit">
                            <i class="fa fa-check"></i> Submit</button>
                         <button class="btn btn-sm red table-group-action-delete">
                            <i class="fa fa-trash"></i> Delete</button>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%">
                                    <input type="checkbox" class="group-checkable"> </th>
                                <th width="8%"> Bank Name </th>
                                <th width="8%"> Bank Alias</th>
                                <th width="10%"> Branch Name </th>
                                <th width="10%"> Swift code </th> 
                                <th width="10%"> Ad Code </th>
                                <th width="10%"> A/C No. </th>
                                <th width="10%"> A/C name </th>
                                <th width="10%"> Actions </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="bank_name"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="bank_alias"> </td>
                                 <td><input type="text" class="form-control form-filter input-sm" name="branch_name"> </td>
                                
                                 <td><input type="text" class="form-control form-filter input-sm" name="swift_code"> </td>   
                                 <td><input type="text" class="form-control form-filter input-sm" name="ad_code"> </td>   
                                 <td><input type="text" class="form-control form-filter input-sm" name="ac_no"> </td>              
                                 <td><input type="text" class="form-control form-filter input-sm" name="ac_name"> </td>    
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                        <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
         <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->
