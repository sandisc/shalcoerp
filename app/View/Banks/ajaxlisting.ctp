<?php
  /* 
   * Paging
   */
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
  foreach ($banks as $banks) {
    $record_nextstage = $this->requestAction(
      array('controller' => 'App', 'action' => 'check_nextstage'),
      array('dependantmodel' => 'Proformainvoice','conditional_parameter' => 'Proformainvoice.bank_id = '.$banks['Bank']['id'])
    );
    $record_order_stage = $this->requestAction(
      array('controller' => 'App', 'action' => 'check_nextstage'),
      array('dependantmodel' => 'Order','conditional_parameter' => 'Order.bank_id = '.$banks['Bank']['id'])
    );
    if(empty($record_nextstage)){          
      if(empty($record_order_stage)){
        $delete_record = '<a href="'.WEBSITE_PATH.'banks/delete/'.base64_encode($banks['Bank']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this Bank?"><span class="glyphicon glyphicon-trash" ></span></a>';
        $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$banks['Bank']['id'].'">';    
      }
      else{
          $delete_record = '';
          $checkbox_delete = '';
      }
    }else{
       $delete_record = '';
       $checkbox_delete = '';
    }      

    $records["data"][] = array($checkbox_delete,
    $banks['Bank']['bank_name'],
    $banks['Bank']['bank_alias'],
    $banks['Bank']['branch_name'],
    $banks['Bank']['swift_code'],
    $banks['Bank']['ad_code'],
    $banks['Bank']['ac_no'],
    $banks['Bank']['ac_name'],
    '<a href="'.WEBSITE_PATH.'banks/add/'.base64_encode($banks['Bank']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>'.$delete_record,);
  }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }*/

  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
   // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>