<h3 class="page-title"> <?php echo $mainTitle;?></h3>

        <!-- start main details -->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""></a></div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Module</label>
                                <div class="col-md-8">
                                    <p class="input text"><?php echo h($res['Activity']['module']); ?> </p>
                                </div>
                            </div>
                        </div>                                                            
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Activity</label>
                                <div class="col-md-8">
                                    <p class="input text"><?php echo h($res['Activity']['activity']); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Activity Performed By</label>
                                <div class="col-md-8">
                                    <p class="input text"> <?php echo h($res['User']['first_name'].' '.$res['User']['last_name']); ?></p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Activity Performed Date</label>
                                <div class="col-md-8">
                                    <p class="input text"> <?php echo h($res['Activity']['modified']); ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">URL</label>
                                <div class="col-md-8">
                                    <p class="input text"><a href=<?php echo $res['Activity']['url'].'>'.$res['Activity']['url']; ?></a></p>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <!--/row-->                                   
                </div>
            </div>
        </div>
        <!-- end activity details -->

        <!-- start activity details -->
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>Activity [OLD] Details</div> 
                <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row form-group">
                        <div class="col-md-12">
                        <?php 
                            $data= unserialize($res['Activity']['details']);
                            foreach ($data as $key => $value) {
                                if($key == 'item'){
                                    print_r($data[$key]);
                                }
                                elseif($key == 'createdby' || $key == 'modifiedby'){
                                    echo '<b>'.$key.'</b> : &nbsp&nbsp&nbsp&nbsp';
                                    $userdetail = $this->requestAction('App/getEmployeename/'.$data[$key]);
                                    echo $userdetail['User']['first_name']." ".$userdetail['User']['last_name'];
                                }else {
                                    echo '<b>'.$key.'</b> : &nbsp&nbsp&nbsp&nbsp'.$value;
                                }
                                echo "</br>";
                            }
                        ?>
                        </div>
                    </div>                 
                </div>                                              
            </div>
        </div>
        <!-- end activity details -->

        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                        <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'activities'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>           