<?php
  /* 
   * Paging
   */
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

    foreach ($activity as $activities) {
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$activities['Activity']['id'].'">',
      $activities['Activity']['module'],
      $activities['Activity']['activity'],
      $activities['Activity']['url'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($activities['Activity']['modified'])),
      $activities['User']['first_name']." ".$activities['User']['last_name'],
      
      '<a href="'.WEBSITE_PATH.'activities/view/'.base64_encode($activities['Activity']['id']).'" class="btn btn-info" title="View Activity"><span class="glyphicon glyphicon-eye-open"></span></a>
      <a href="'.WEBSITE_PATH.'activities/delete/'.base64_encode($activities['Activity']['id']).'" class="btn btn-danger" title="Delete Activity" id="delete" data-confirm="Are you sure to delete this activity record?"><span class="glyphicon glyphicon-trash"></span></a>'
      ,);
    }

  $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
  $records["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>