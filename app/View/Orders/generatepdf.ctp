<?php
    $setting= $this->Session->read('setting_data'); /*Read setting table's data*/
  
  $html = '<style> 
body {font-family:helvetica; font-size:10px;}
table {float: none;margin: 0 auto;width: 100%;border-spacing: 0; cellspacing="5"}        
.table {margin-bottom: 20px;width: 100%;}
p  {
    border-style: solid;
    
}
.view_invoice{
  border-collapse:collapse; 
  border-color:#000000; 
  border-style:solid; 
  margin-top: 1em;
  border-width:1px;
}
table tr td{padding:0 5px!important; letter-spacing: 1px!important;}
.smallletter{text-align:right;}
</style>';

$html .= '<html>
                <head>
                    <title>Order</title>
                </head>
                <body>
                    <table class="table" cellpadding="2">
                        <tbody>  
                            <tr>
                                <td align="left"><img src="'.ROOT_PATH.'img/print_logo.png"></td>
                                <td align="right" style="vertical-align: bottom !important;"> 
                                    <b>CIN NO </b>'.$setting["Setting"]["cin_no"].'<br>
                                    <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building<br/>Gulalwadi, Mumbai - 400004<br/> Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25<br/>
                                    Email: info@shalco.in Web: www.shalco.in</span>
                                </td>                    
                            </tr>
                        </tbody>
                    </table>
                    <table id="proforma_table" border="1" class="table table-bordered table-striped table-condensed flip-content view_invoice">
                        <tbody> 
                            <!--<tr>
                                <td colspan="10">'.$this->requestAction('App/getURI/').'</td>
                            </tr>-->
                    
            <tr>
                <td colspan="11" align="center"><b><h2>Sales Order</h2></b></td>
            </tr>
            <tr>
                <td colspan="2"><b>Office Address</b></td>
                <td colspan="2">'.nl2br($setting["Setting"]["office_address"]).'</td>
                <td></td><!-- Blank td -->
                <td colspan="1"> <b>ORDER NO.</b> <br>'.$data["Order"]["orderno"].'</td>
                <td colspan="5">Date <br>'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($data["Order"]["orderdate"])).' <br>
                </td>
            </tr>
            <tr>
                <td rowspan="2" colspan="2"><b>Factory Address</b></td>
                <td rowspan="2" colspan="2" >'.nl2br($setting["Setting"]["factory_address"]).'</td>
                <td rowspan="2"></td><!-- Blank td -->
                <td colspan="1"><b>Shipping Terms</b></td>
                <td colspan="5">'.$this->requestAction('App/get_delivery_type/'.$data["Order"]["delivery_type"]).nl2br($data["Order"]["tod"]).'</td>
                <tr>
                    <!-- <td colspan="5"></td>--> <!-- Blank td -->
                    <td colspan="1"><b>Terms Of Payment</b></td>
                    <td colspan="5">'.nl2br($data["Order"]["top"]).'</td>
                </tr>
                
            </tr>
            <tr>
                <td colspan="4"><b>Bill To </b></td>
                <td></td>
                <td colspan="6"><b>Ship To</b></td>
            </tr>
            <tr>
                <td colspan="4"><b>'.$data["Client"]["company_name"].'</b><br>'.searchFromAddress($data['Order']['billaddress_id'], $data['Client']['Address']).'</td>
                <td></td>
                <td colspan="6"><b>'.$data["Order"]["ship_to"].'</b><br>'.searchFromAddress($data['Order']['shipaddress_id'], $data['Client']['Address']).'</td>
            </tr>
            <tr>
                <th width="2%" align="center"><b>Sr.<br>No.<br></b></th>
                <th width="14%" align="center"><b>Product Name</b></th>
                <th width="10%" align="center"><b>Hscode</b></th>
                <th width="10%" align="center"><b>Standard</b></th>
                <th width="10%" align="center"><b>Grade</b></th>
                <th width="20%" colspan="2" align="center"><b>Good Description</b></th>
                <th width="10%" align="center"><b>QTY </b></th>
                <th width="7%" align="center"><b>Unit Price<br><span style="font-family: DejaVu Sans; sans-serif;">('.$data['Price']['sign'].')</span> </b></th>
                <th width="13%" colspan="2" align="center"><b>Amount<br><span style="font-family: DejaVu Sans; sans-serif;">('.$data['Price']['sign'].')</span></th>
               
            </tr>';
            
            $i = 1;
            foreach($data["Orderitem"] as $value) {  
              $html .= '<tr class="addmore">
                  <td align="right">'.$value['sr_no'].'</td>
                  <td>'.$value["Productcategory"]["productname"].'</td>
                  <td>'.$value["Productcategory"]["Hscode"]["hscode"].'</td>
                  <td>'.$value["Productcategory"]["Standard"]["standard_name"].'</td>
                  <td>'.$value["Productcategory"]["Grade"]["grade_name"].'</td>
                  <td style="white-space:nowrap;">'.setGDmmformat($value['Productcategory']['Size']['gdmm'],$value['length']).'</td>
                  <td style="white-space:nowrap;">'.setGDnbformat($value['Productcategory']['Size']['gdnb'],$value['length']).'</td>
                  <td class="numalign" align="right">'.$value["qty"].' '.$value['Productcategory']['Producttaxonomy']['unit'].'</td>
                  <td class="numalign" align="right">'.$value["price"].'</td>
                  <td class="numalign" colspan="2" align="right">'.$value["amount"].'</td>
              </tr>';$i++;
            } 

  $html .= '<tr>    
                <td colspan="7"></td>
                <td colspan="2" align="right"><b>Total<span style="font-family: DejaVu Sans; sans-serif;"> '.$data['Price']['fullform'].'('.$data['Price']['sign'].')</span></b></td>
                <td align="right" colspan="2"><b>'.$data["Order"]["total"].'</b></td>
            </tr>
            <tr>
                <td colspan="2"><b>Quantity Tolerance</b></td>
                <td colspan="9">'.$data['Order']['quantity'].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Delivery Period </b></td>
                <td colspan="9">'.$data['Order']['delivery_period'].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Dimensions </b></td>
                <td colspan="9">'.$data['Order']['dimensions'].'</td>                 
            </tr>
            <tr>
                <td colspan="2"><b>Certification </b></td>
                <td colspan="9">'.$data['Order']['certifications'].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Comment </b></td>
                <td colspan="9">'.$data['Order']['comment'].'</td>
            </tr>          
            <tr>
                <td colspan="11"><b><h3>Bank Details<h3></b></td>
            </tr>
            <tr>
                <td colspan="2">Bank Name </td>
                <td colspan="4">'.$data["Bank"]["bank_name"].'</td>
                <td colspan="5" rowspan="8" style="vertical-align: top !important;"><b>For, SHALCO IND. PVT. LTD. <br/>DIRECTOR</b><br/>
                <!--<img src="'.ROOT_PATH.'uploads/signature/'.$setting['Setting']['signature'].'">--></td>
            </tr>
            <tr>
                <td colspan="2"><b>Branch Name </b></td>
                <td colspan="4">'.$data["Bank"]["branch_name"].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Swift Code </b></td>
                <td colspan="4">'.$data["Bank"]["swift_code"].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>AD Code </b></td>
                <td colspan="4">'.$data["Bank"]["ad_code"].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>Beneficiary&#039s A/C No </b></td>
                <td colspan="4">'.$data["Bank"]["ac_no"].'</td>
            </tr>
            <tr>
                <td colspan="2">Beneficiary&#039s Name </td>
                <td colspan="4">'.$data["Bank"]["ac_name"].'</td>                
            </tr>            
            <tr>
                <td colspan="2"><b>IEC Code </b></td>
                <td colspan="4">'.$data["Bank"]["iec_code"].'</td>
            </tr>
            <tr>
                <td colspan="2"><b>IFC Code </b></td>
                <td colspan="4">'.$data["Bank"]["ifc_code"].'</td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>';
    echo $html;
?>