<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
      <?php echo $this->Session->flash();?>
      <div class="portlet light portlet-fit portlet-datatable bordered">
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
            </div>
             <?php echo $this->Form->create('Order',array('enctype' => 'multipart/form-data')); ?>    
           <div class="actions custom_action">
               <div class="btn-group btn-group-devided browse_div">
               
                    <?php echo $this->Form->input('uploadfile', array('type' => 'file','class'=>'form-control','label'=>false,'required'=>'required')); ?>
               </div>
                <button class="btn green" type="submit">Import</button>
                <!--<a download="sameple-file.csv" class="btn blue margin-top-10 margin-bottom-10" href="<?php echo WEBSITE_PATH;?>test.csv" title="ImageName">Sample File</a>-->
                <a onclick="window.location='orders/download'" class="btn blue margin-top-10 margin-bottom-10">Sample File</a>
                <?php echo $this->Form->end();?>
               <div class="btn-group">
               </div>
            </div>
                         
               <div class="btn-group">
               </div>
           </div>
         
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm" id ="sel_ct">
                                                <option value="">Select...</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                <option value="delete">Delete</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button class="btn btn-sm red table-group-action-delete">
                                                <i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"> <input type="checkbox" class="group-checkable"> </th>
                                                    <th width="10%"> Order NO </th>
                                                    <th width="10%"> Order Date </th> 
                                                    <th width="15%"> Client Name </th>
                                                    <th width="8%"> Modified Date</th>
                                                    <th width="8%"> Modified By </th>
                                                    <th width="15%"> Status </th>
                                                   <th width="15%"> Actions </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="orderno"> </td>
                                                     <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="orderdate" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                    
                                                    </td> 
                                                    <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td> 
                                                    <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                                                    <td></td>
                                                     <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
      </div>
        <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->
<script>
  /*Start Change status of orderitem*/  
  $(document).on('click', '#changeorderstatus', function () {
    var status_id = $(this).data('value');
    var ref= $(this);
    var id = $(this).data('id'); /*Order id*/
    $.ajax({
      url: '<?php echo WEBSITE_PATH;?>orders/changeorderstatus/'+id+'/'+status_id,
      type: 'POST',
      data: {'status_id':status_id,'orderid':id},
      cache: false,
      success: function (data) {
        if(status_id == 3){
        ref.parent().html('<span class="label label-sm label-danger">Cancelled</span>');
        }else{
          ref.parent().html('<span class="label label-sm label-success">Closed</span>');
        }
      }
    });
    return false;
  });
 /*End Change status of order*/  
</script>