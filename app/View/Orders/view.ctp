  <div class="pull-left"><h3 class="page-title"> <?php echo $mainTitle;?></h3></div>
  <div class="pull-right">
   <!--  <a id="printrow" data-controllername="orders" data-actionname="prints" data-id ="<?php echo base64_encode($data['Order']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Order" targer="_blank"><span class="glyphicon glyphicon-print"></span> Print  </a> -->
    <?php  
    if(in_array('orders/edit',$this->Session->read('accesscontrollers_actions'))){
      echo $this->Html->link(
         '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
          array(
              'controller'=>'orders',
              'action'=>'edit',
              base64_encode($data['Order']['id'])
          ),
          array(
              'rel'=>'tooltip',
              'data-placement'=>'left',
              'data-original-title'=>'Edit',
              'title' => 'Edit',
              'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
              'escape'=>false  //NOTICE THIS LINE ***************
          )
      );        
    }
    if(!empty($data['Order']['po_upload'])){
    	echo '<a download="'.$data['Order']['po_upload'].'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline " href="'.WEBSITE_PATH.'uploads/clientpo/'.$data['Order']['po_upload'].'" title="ImageName" id="clntpo">Download Client PO</a>';
    }else {
      echo '<a rel="tooltip" data-placement="left" data-original-title="Upload" title="Upload" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline " id="clntpo" onClick=openModel();><span title="Edit" class="glyphicon glyphicon-upload"></span> PO Upload</a>';
     }
      /*Set PDF Download link*/
      echo $this->Html->link(
         '<i class="fa fa-file-pdf-o"></i> PDF',
          array(
              'controller'=>'orders',
              'action'=>'generatepdf',
              'ext' => 'pdf',
              $data['Order']['id']
          ),
          array(
              'rel'=>'tooltip',
              'data-placement'=>'left',
              'data-original-title'=>'PDF',
              'title' => 'PDF',
              'download'=>'Order-'.$data['Order']['orderno'],
              'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
              'escape'=>false  //NOTICE THIS LINE ***************
          )   
      );
      
      echo $this->Html->link(
         '<i class="fa fa-file-excel-o"></i> Excel',
          array(
              'controller'=>'orders',
              'action'=>'export',
              $data['Order']['id']
          ),
          array(
              'rel'=>'tooltip',
              'title' => 'Download',
              'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
              'escape'=>false  //NOTICE THIS LINE ***************
          )   
      );  
        //echo $this->Html->link('Download',array('controller'=>'orders','action'=>'export',$data['Order']['id']));
      /* MS word output */
        /*echo $this->Html->link(
           '<i class="fa fa-file-word-o" aria-hidden="true"></i> DOC',
            array(
                'controller'=>'orders',
                'action'=>'printoword',
                base64_encode($data['Order']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'DOC',
                'title' => 'DOC',
                'download'=>'Order-'.$data['Order']['orderno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );*/
      

      ?>
    
  </div>
  <div class="clearfix"></div>
  <?php $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>             
  <div id="tab_3" class="tab-pane active">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?> </div>
          <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
      </div>
      <div class="portlet-body form">
        <div class="form-body portlet-body flip-scroll">
          <table id="proforma_table" width="100%"  class="table table-bordered table-striped table-condensed flip-content view_invoice">
            <tr>
              <td colspan="2"> <b>Office Address</b> </td>
              <td colspan="3"><?php echo nl2br($setting['Setting']['office_address']); ?></td>
              <td></td><!-- Blank td -->
              <td colspan="2"> <b>Order NO.</b> <br>
                <?php echo h($data['Order']['orderno']); ?>
              </td>
              <td colspan="4"><b>Date</b> <br>
                  <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['orderdate'])); ?>
              </td>
            </tr>
            <!--/tr -->
            <tr>
              <td rowspan="2" colspan="2"><b> Factory Address</b> </td>
              <td rowspan="2" colspan="3" ><?php echo nl2br($setting["Setting"]["factory_address"]);?></td>
              <td></td><!-- Blank td -->
              <td colspan="2"><b>Shipping Terms</b></td>
              <td colspan="4"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).nl2br($data['Order']['tod']); ?></td>
              <tr>
                <td></td><!-- Blank td -->
                <td colspan="2"><b>Terms Of Payment</b></td>
                <td colspan="4"><?php echo nl2br($data['Order']['top']); ?></td>
              </tr>
            </tr>
            <!--/tr -->                        
            <tr>
                <td colspan="5"><b>Bill To </b></td>
                <td></td>
                <td colspan="6"><b>Ship To</b></td>
            </tr>
            <!--/tr -->
            
            <tr>
              <td colspan="5"><b><?php echo nl2br($data['Client']['company_name']); ?></b><br><?php echo searchFromAddress($data['Order']['billaddress_id'], $data['Client']['Address']); ?></td>
              <td></td>
              <td colspan="6"><b><?php echo nl2br($data['Order']['ship_to']); ?></b><br><?php echo searchFromAddress($data['Order']['shipaddress_id'], $data['Client']['Address']); ?></td>
            </tr>
            <!--/tr -->                              
            <tr>
                <td width="5%" align="center"><b>Sr. No.</b></th>
                <td width="16%" align="center"><b>Product Name</b></th>
                <td width="12%" align="center"><b>Hscode</b></th>
                <td width="10%" align="center"><b>Standard</b></th>
                <td width="8%" align="center"><b>Grade</b></th>
                <td width="26%" colspan="2" align="center"><b>Good Description</b></th>
                <td width="6%" align="center"><b>QTY</b></th>
                <td width="6%" align="center"><b>Delivered <br>QTY</b></th>
                <td width="8%" align="center"><b>Status</b></th>
                <td width="8%" align="center"><b>Unit Price<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
                <td width="8%" align="center"><b>Amount<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
            </tr>
            <!--/tr -->
            <tbody>
                <?php $i = 1;    
                foreach($data['Orderitem'] as $value) {  ?>
                    <tr class="addmore">    
                      <?php $length = $value['length'] * 1000;?>
                      <td align="right" style="padding-left:5px!important;"><?php echo $value['sr_no'];?></td>
                      <td><?php echo $value['Productcategory']['productname'];?></td>
                      <td><?php echo $value['Productcategory']['Hscode']['hscode'];?></td>
                      <td><?php echo $value['Productcategory']['Standard']['standard_name'];?></td>
                      <td><?php echo $value['Productcategory']['Grade']['grade_name'];?></td>
                      <td style="white-space:nowrap;"><?php echo setGDmmformat($value['Productcategory']['Size']['gdmm'],$value['length']);?></td>
                      <td style="white-space:nowrap;"><?php echo setGDnbformat($value['Productcategory']['Size']['gdnb'],$value['length']);?></td>
                      <td class="numalign"><?php echo $value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                      <td class="numalign"><?php echo $value['dlvr_qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                      <?php if($value['status'] == '0'){
                        $status = '<span class="label label-sm label-warning">Pending</span>
                          <a id="changeitemstatus" data-value="3" data-id="'.$value['id'].'" class="btn blue margin-top-10 ">Cancel</a>';
                      }elseif($value['status'] == '1'){
                        $status = '<span class="label label-sm label-info">Process</span>
                          <a id="changeitemstatus" data-value="2" data-id="'.$value['id'].'" class="btn blue margin-top-10">Close</a>';
                      }elseif($value['status'] == '2'){
                        $status = '<span class="label label-sm label-success">Closed</span>';
                      }elseif($value['status'] == '3'){
                        $status = '<span class="label label-sm label-danger">Cancelled</span>';
                      }?>
                      <td align="center" style="padding-right:3px!important;"><?php echo $status;?></td>
                      <td class="numalign"> <?php echo $value['price'];?></td>
                      <td class="numalign"> <?php echo $value['amount'];?></td>
                    </tr>                                            
                <?php $i++; } ?>
                <!--/foreach -->                            
                  <tr>    
                    <td colspan="8" ></td>
                    <td colspan="3" class="numalign"><b>Total Amount <?php echo $data['Price']['fullform'].'('.$data['Price']['sign'].')';?></b></td>
                    <td align="right"><b style="padding-right:5px!important;"><?php echo $data['Order']['total'];?></b></td>
                  </tr>
                    <!--/tr -->
                    <!-- New Fields -->
                    <tr>
                        <td colspan="2"><b>Quantity Tolerance</b></td>
                        <td colspan="10"><?php echo h($data['Order']['quantity']); ?></td>
                        
                  </tr>
                  <tr>
                         <td colspan="2"><b>Delivery Period</b></td>
                        <td colspan="10"><?php echo h($data['Order']['delivery_period']); ?></td>
                  </tr>
                  <tr>
                         <td colspan="2"><b>Dimensions</b></td>
                        <td colspan="10"><?php echo h($data['Order']['dimensions']); ?></td>
                  </tr>
                  <tr>
                        <td colspan="2"><b>Certification</b></td>
                        <td colspan="10"><?php echo h($data['Order']['certifications']); ?></td>
                  </tr>
                  <tr>
                        <td colspan="2"><b>Comment</b></td>
                        <td colspan="10"><?php echo h($data['Order']['comment']); ?></td>
                  </tr>
                  <!-- -->
                   <!--/tr -->                            
                  <tr>
                    <td colspan="12"><b><u>Bank Details</u></b></td>
                  </tr>
                  
                  <tr>
                    <td colspan="2"><b>Bank Name</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['bank_name']); ?></td>
                    <td colspan="5" rowspan="9"><b>For, SHALCO IND. PVT. LTD. <br/> DIRECTOR</b></td>                    
                  </tr>
                <!--/tr -->                          
                <tr>
                    <td colspan="2"><b>Bank Alias</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['bank_alias']); ?></td>
                </tr>
                  <tr>
                    <td colspan="2"><b>Branch Name</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['branch_name']); ?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Swift Code</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['swift_code']); ?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>AD Code</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['ad_code']); ?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Beneficiary’s A/C No</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['ac_no']); ?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Beneficiary’s Name</b></td>
                    <td colspan="5"><?php echo h($data['Bank']['ac_name']); ?></td>
                  </tr>
                <!--/tr -->  
                 <tr>
                  <td colspan="2"><b>IEC Code</b></td>
                  <td colspan="5"><?php echo $data["Bank"]["iec_code"]; ?></td>
                  
              </tr>
              <tr>
                  <td colspan="2"><b>IFC Code</b></td>
                  <td colspan="5"><?php echo $data["Bank"]["ifc_code"];?></td>
              </tr>                        
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Client P.O number</b></td>
                    <td colspan="10"><?php echo $data['Order']['clientpo']?></td>
                  </tr>
                  <tr>
                    <td colspan="2"><b>Client P.O Date</b></td>
                    <td colspan="10"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate'])); ?></td>
                  </tr>                   
                  <tr>
                    <td colspan="2"><b>Dispatch Location</b></td>
                    <td colspan="10"><?php echo $data['Dispatch']['location']?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Dispatch code</b></td>
                    <td colspan="10"><?php echo $data['Order']['dispatchcode']?></b></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Order generated on</b></td>
                    <td colspan="10"><?php echo $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($data['Order']['created']))?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Order generated by</b></td>
                    <td colspan="10"><?php echo $this->requestAction('Users/getUsername/'.$data['Order']['modifiedby'])?></td>
                  </tr>
                <!--/tr -->                          
                  <tr>
                    <td colspan="2"><b>Description</b></td>
                    <td colspan="10"><?php echo nl2br($data['Order']['acceptcomment']);?></td>
                  </tr>
                <!--/tr -->                          
            </tbody>
          </table> 
          <div class="form-actions">
            <div class="row">
              <div class="col-md-6">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'orders'),array('class' => 'btn default')); ?>
              </div>
            </div>
              </div>
              <div class="col-md-6"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>   

<!-- Start Reject Proforma Invoice Dialog Modal -->
<div class="modal fade" id="uploadform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Order',array('method'=>'POST','class'=>'shalcoform','type'=>'file')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Upload PO</h4>
      </div>
      <div class="modal-body">
          <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'orderid','value'=>$data['Order']['id']));
          echo $this->Form->input('poupload',array('type'=>'file','id'=>'poupload','required'));?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btnUpload" class="btn btn-primary">Upload</button>
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!-- End Reject Proforma Invoice Dialog Modal -->             
  <script>
    /*Start Change status of orderitem*/
    $(document).on('click', '#changeitemstatus', function () {
      var status_id = $(this).data('value');
      var ref= $(this);
      var id = $(this).data('id'); /*Order item id*/
      $.ajax({
        url: '<?php echo WEBSITE_PATH;?>orders/changeitemstatus/'+id+'/'+status_id,
        type: 'POST',
        data: {'status_id':status_id,'orderitem_id':id},
        cache: false,
        success: function (data) {
          if(status_id == 3){
            ref.parent().html('<span class="label label-sm label-danger">Cancelled</span>');
          }else{
            ref.parent().html('<span class="label label-sm label-success">Closed</span>');
          }
        }
      });
      return false;
    });
    /*End Change status of orderitem*/  
    /* Uplaod File Upload */
    function openModel(){
    	$('#uploadform').modal({
            show: true,
        });	
    }
    $(document).on('click', '#btnUpload', function (e) {
    		 e.preventDefault();
          //e.stopImmediatePropagation();      
          var fd = new FormData();
          //var form=$("#myForm");
          var id = $('#orderid').val();
          var file = $(document).find('input[type="file"]');
          console.log(file);
         // return false;
          if(file.length > 0){
            var individual_file = file[0].files[0];
          } else{

            var individual_file = '';
          }


          fd.append("file", individual_file);
          fd.append("caption_avatar", individual_file.name);  
          fd.append("id", id);  
         $.ajax({
            url: '<?php echo WEBSITE_PATH;?>orders/uploadpo/',
            type: 'POST', 
            data:fd,
            mimeType:"multipart/form-data",
            dataType: "json",
            contentType: false,
            cache: false,
            processData:false,
            success: function (data) {
              if(data == 1){
	              $("#spinner").hide();
	              $('.modal').modal('hide');
	              $('#clntpo').replaceWith('<a download="'+individual_file.name+'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline " href="<?php echo WEBSITE_PATH;?>uploads/clientpo/'+individual_file.name+'" title="ImageName" id="clntpo">Download Client PO</a>');
	              $("#spinner").hide();
              	  $('.modal').modal('hide');
            	}
            }
        });         
    });
    
    
  </script>