<?php $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>  
<style> 
body {font-family:helvetica; font-size:10px;}
table {float: none;margin: 0 auto;width: 100%;border-spacing: 0; cellspacing:5;}        
.table {margin-bottom: 20px;width: 100%;}
p  {
    border-style: solid;
    
}
.view_invoice{
  border-collapse:collapse; 
  border-color:#000000; 
  border-style:solid; 
  margin-top: 1em;
  border-width:1px;
}
table tr td{padding:0 5px!important; letter-spacing: 1px!important;}
.smallletter{text-align:right;}
</style>           
<div id="tab_3" class="tab-pane active">
                                    <div class="portlet box">
                                      <div class="portlet-title">
                                        <div class="caption"><?php echo $pageTitle;?> </div>
                                      </div>
                                      <div class="portlet-body form">
                                        <div class="form-body portlet-body flip-scroll">
                                            <table width="100%" class="table" cellpadding="2">
                                              <tbody>  
                                                  <tr>
                                                      <td align="left"><img src="<?php echo ROOT_PATH.'img/print_logo.png'  ?>"></td>
                                                      <td align="right" style="vertical-align: bottom !important;"> 
                                                          <b>CIN NO </b><?php echo $setting["Setting"]["cin_no"]; ?><br>
                                                          <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building<br/>Gulalwadi, Mumbai - 400004<br/> Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25<br/>
                                                          Email: info@shalco.in Web: www.shalco.in</span>
                                                      </td>                    
                                                  </tr>
                                              </tbody>
                                          </table>
                                          <table id="proforma_table" width="100%"  class="table table-bordered table-striped table-condensed flip-content view_invoice">
                                                  <tr>
                                                    <td colspan="10" align="center"><b><h2>Sales Order</h2></b></td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan="1"> <b>Office <br>Address</b> </td>
                                                      <td colspan="3"><?php echo nl2br($setting['Setting']['office_address']); ?></td>
                                                      <td></td><!-- Blank td -->
                                                      <td colspan="1"> <b>Order NO.</b> <br><?php echo h($data['Order']['orderno']); ?></td>
                                                      <td colspan="4"><b>Date</b> <br>
                                                          <?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['orderdate'])); ?>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td rowspan="2" colspan="1"><b> Factory <br>Address</b> </td>
                                                      <td rowspan="2" colspan="3" ><?php echo nl2br($setting["Setting"]["factory_address"]); ?></td>
                                                      <td></td><!-- Blank td -->
                                                      <td colspan="1"><b>Shipping Terms</b></td>
                                                      <td colspan="4"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).nl2br($data['Order']['tod']); ?></td>
                                                  <tr>
                                                        <td></td><!-- Blank td -->
                                                        <td colspan="1"><b>Terms Of Payment</b></td>
                                                        <td colspan="4"><?php echo nl2br($data['Order']['top']); ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan="4"><b>Bill To </b></td>
                                                      <td></td><!-- Blank td -->
                                                      <td colspan="5"><b>Ship To</b></td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan="4"><b><?php echo nl2br($data['Client']['company_name']); ?></b><br><?php echo searchFromAddress($data['Order']['billaddress_id'], $data['Client']['Address']); ?></td>
                                                      <td></td><!-- Blank td -->
                                                      <td colspan="5"><b><?php echo nl2br($data['Order']['ship_to']); ?></b><br><?php echo searchFromAddress($data['Order']['shipaddress_id'], $data['Client']['Address']); ?></td>
                                                  </tr>                                                      
                                                  <tr>
                                                      <td width="6%" align="center"><b>Sr. No.</b></th>
                                                      <td width="16%" align="center"><b>Product Name</b></th>
                                                      <td width="10%" align="center"><b>STANDARD</b></th>
                                                      <td width="10%" align="center"><b>GRADE</b></th>
                                                      <td width="32%" colspan="2" align="center"><b>GOOD DESCRIPTION</b></th>
                                                      <td width="8%" align="center"><b>QTY</b></th>
                                                      <td width="8%" align="center"><b>PRICE <?php echo '('.$data['Price']['sign'].')';?>/<br>MTR </b></th>
                                                      <td width="8%" align="center"><b>AMOUNT<br><?php echo '('.$data['Price']['sign'].')';?></b></th>
                                                  </tr>                                      

                                              <tbody>
                                                    <?php $i = 1;
                                                        
                                                    foreach($data['Orderitem'] as $value) {  ?>
                                                        <tr class="addmore">    
                                                                <?php $length = $value['length'] * 1000;?>
                                                            <td align="right" style="padding-left:5px!important;"><?php echo $value['sr_no'];?></td>
                                                            <td class="padding-left"><?php echo $value['Productcategory']['productname'];?></td>
                                                            <td class="padding-left"><?php echo $value['Productcategory']['Standard']['standard_name'];?></td>
                                                            <td class="padding-left"><?php echo $value['Productcategory']['Grade']['grade_name'];?></td>
                                                            <td style="white-space:nowrap;"><?php echo setGDmmformat($value['Productcategory']['Size']['gdmm'],$value['length']);?></td>
                                                            <td style="white-space:nowrap;"><?php echo setGDnbformat($value['Productcategory']['Size']['gdnb'],$value['length']);?></td>
                                                            <td class="numalign"><?php echo $value['qty'].' '.$value['Productcategory']['Producttaxonomy']['unit'];?></td>
                                                            <td class="numalign"><?php echo $value['price'];?></td>
                                                            <td class="numalign"><?php echo $value['amount'];?></td>
                                                        </tr>                                            
                                                         <?php $i++; } ?>
                                                         <tr class="td_center">    
                                                                <td colspan="7" ></td>
                                                                <td class="numalign"><b>Total <?php echo '('.$data['Price']['sign'].')';?></b></td>
                                                                <td class="numalign"><b><?php echo $data['Order']['total']?></b></td>
                                                        </tr>
                                                        <tr>
                                                              <td colspan="2"><b>Quantity Tolerance</b></td>
                                                              <td colspan="7"><?php echo h($data['Order']['quantity']); ?></td>
                                                        </tr>
                                                        <tr>
                                                               <td colspan="2"><b>Delivery Period</b></td>
                                                              <td colspan="7"><?php echo h($data['Order']['delivery_period']); ?></td>
                                                        </tr>
                                                        <tr>
                                                               <td colspan="2"><b>Dimensions</b></td>
                                                              <td colspan="7"><?php echo h($data['Order']['dimensions']); ?></td>
                                                        </tr>
                                                        <tr>
                                                              <td colspan="2"><b>Certification</b></td>
                                                              <td colspan="7"><?php echo h($data['Order']['certifications']); ?></td>
                                                        </tr>
                                                        <tr>
                                                              <td colspan="2"><b>Comment</b></td>
                                                              <td colspan="7"><?php echo h($data['Order']['comment']); ?></td>
                                                        </tr>
                                                  <tr>
                                                    <td colspan="9"><b><h3>Bank Details<h3></b></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>Bank Name</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['bank_name']); ?></td>
                                                    <td colspan="6" colspan="10"><b>For, SHALCO IND. PVT. LTD. <br/> DIRECTOR</b></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>Branch Name</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['branch_name']); ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>Swift Code</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['swift_code']); ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>AD Code</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['ad_code']); ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>Beneficiary’s A/C No</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['ac_no']); ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>Beneficiary’s Name</b></td>
                                                    <td colspan="3"><?php echo h($data['Bank']['ac_name']); ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>IEC Code</b></td>
                                                    <td colspan="3"><?php echo $data["Bank"]["iec_code"]; ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="2"><b>IFC Code</b></td>
                                                    <td colspan="3"><?php echo $data["Bank"]["ifc_code"];?></td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>