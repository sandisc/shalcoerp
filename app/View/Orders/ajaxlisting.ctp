<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

    foreach ($order as $orders) { 
      $id = $orders['Order']['id'];
      /*check record entered in next stage means used in another module*/
      if($orders['Order']['local'] == 1){/*Check record exist in chalan for local order*/
        $record_nextstage = $this->requestAction(
                      array('controller' => 'App', 'action' => 'check_nextstage'),
                      array('dependantmodel' => 'Chalan','conditional_parameter' => 'Chalan.orderid = '.$id)
        );
      }
      else{/*Check record exist in invoice for international order*/
        $record_nextstage = $this->requestAction(
                      array('controller' => 'App', 'action' => 'check_nextstage'),
                      array('dependantmodel' => 'Invoice','conditional_parameter' => 'Invoice.orderid = '.$id)
        );        
      }
      
      $records = array(); 
      if(empty($record_nextstage)){$records[] = '<input type="checkbox" name="id[]" value="'.$id.'">';  }
      else{$records[] = '';  }
      
      $records[] = '<a href="'.WEBSITE_PATH.'orders/view/'.base64_encode($orders['Order']['id']).'" title="View Order">'.$orders['Order']['orderno'].'</a>';
      $records[] = date('d/m/Y', strtotime($orders['Order']['orderdate']));
      
      $records[] = $orders['Client']['company_name'];
      $records[] = '<span class="modifieddate">'.$this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($orders['Order']['modified'])).'</span>';
      $records[] = $orders['User']['first_name']." ".$orders['User']['last_name'];

        if($orders['Order']['status'] == '0'){
          $status = '<span class="label label-sm label-warning">Pending</span>
            <a id="changeorderstatus" data-value="3" data-id="'.$id.'" class="btn blue margin-top-10" title="Click here for Cancel Order">Cancel</a>';
        }elseif($orders['Order']['status'] == '1'){
          $status = '<span class="label label-sm label-info">Process</span>
            <a id="changeorderstatus" data-value="2" data-id="'.$id.'" class="btn blue margin-top-10" title="Click here for Close Order">Close</a>';
        }elseif($orders['Order']['status'] == '2'){
          $status = '<span class="label label-sm label-success">Closed</span>';
        }elseif($orders['Order']['status'] == '3'){
          $status = '<span class="label label-sm label-danger">Cancelled</span>';
        }

      $records[] = $status;
      $nextaction = '';
      /*If Order is not local than*/
      if($orders['Order']['local'] == 0){
        /*If invoice is not generate than*/
        if($orders['Order']['status'] == 0){
          if(in_array('invoices/add',$this->Session->read('accesscontrollers_actions'))){
              $nextaction.= '<a href="'.WEBSITE_PATH.'invoices/add/'.base64_encode($id).'" class="btn btn-success btn-double" title="Add Invoice for This Order"><span class="glyphicon glyphicon-briefcase" ></span> Add Invoice</a>';
          }
        }
        if($orders['Order']['status'] == 1 || $orders['Order']['status'] == 2){
          if(in_array('invoices/invoiceoforder',$this->Session->read('access_controller'))){
             $nextaction.= '<a href="'.WEBSITE_PATH.'invoices/invoiceoforder/'.base64_encode($id).'" title="View Invoices of This Order" class="btn btn-success btn-double">View Invoices</a>';
          }
        }
      }
      else{
        /*If chalan is not generate than*/
        if($orders['Order']['status'] == 0){
          if(in_array('chalans/add',$this->Session->read('accesscontrollers_actions'))){
            $nextaction.= '<a href="'.WEBSITE_PATH.'chalans/add/'.base64_encode($id).'" class="btn btn-success btn-double" title="Add Chalan for This Order"><span class="glyphicon glyphicon-road" ></span> Add Chalan</a>';
          }
        }
        if($orders['Order']['status'] == 1 || $orders['Order']['status'] == 2){
          if(in_array('chalans/chalanoforder',$this->Session->read('access_controller'))){
            $nextaction.= '<a href="'.WEBSITE_PATH.'chalans/chalanoforder/'.base64_encode($id).'" title="View Chalans of This Order" class="btn btn-success btn-double">View Chalans</a>';
          }
        }
      }
       $permiss_ses = '';
       if(in_array('orders/edit',$this->Session->read('accesscontrollers_actions'))){
        $permiss_ses .= '<a href="'.WEBSITE_PATH.'orders/edit/'.base64_encode($id).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
       }
       if(in_array('orders/delete',$this->Session->read('accesscontrollers_actions')) && empty($record_nextstage)){
        $permiss_ses .= '<a href="'.WEBSITE_PATH.'orders/delete/'.base64_encode($id).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this order?"><span class="glyphicon glyphicon-trash"></span></a>';
       }   

       $print = '<a id="printrow" data-controllername="orders" data-actionname="prints" data-id ="'.base64_encode($id).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Order" targer="_blank"><span class="glyphicon glyphicon-print"></span></a>';
      $records[] = '<a href="'.WEBSITE_PATH.'orders/view/'.base64_encode($id).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>'.$permiss_ses. $nextaction;
       
      $record['data'][] = $records;
    }

  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
    $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
 // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>