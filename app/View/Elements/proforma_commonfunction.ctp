<script type="text/javascript">
/*Add client using modal dialog*/
function addClient(id) {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>clients/add/targetid:'+id,
    type: 'POST',
    success: function(data) {
      $('#addClientModal .previewText').html(data);
      $('#addClientModal .page-title').remove();
      $('#addClientModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}

/*Add size using modal dialog*/
function addSize(id) {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>sizes/add/targetid:'+id,
    type: 'POST',
    success: function(data) {
      $('#addSizeModal .previewText').html(data);
      $('#addSizeModal .page-title').remove();
      $('#addSizeModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}
</script>

<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addClientModalLabel" role="dialog" tabindex="-1" id="addClientModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addSizeModalLabel" role="dialog" tabindex="-1" id="addSizeModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

/*Fetch Proforma Number based on Change event of Date.*/
$(document).ready(function() {  
  $(document).on('change', '.changedatepicker',function(){
    var date = $('.changedatepicker').val(); 
    var current_fy = $('#fy').val();
    // var dateObject = $('.changedatepicker').datepicker('getDate');
      $.ajax({
          url: '<?php echo WEBSITE_PATH;?>app/calculateFinancialYearForDate/',
          type: 'POST',
          cache: false,
          data : "date="+date,
          success: function (new_fy) {
            /*Check new financial year is same as current financial year*/
            if(current_fy != new_fy){ 
            /*If not than fetch new auto increment number*/
              $.ajax({
                  url: '<?php echo WEBSITE_PATH;?>proformainvoices/getProformaNO/ajax/',
                  type: 'POST',
                  cache: false,
                  data : "date="+date,
                  success: function (data) {
                    $('.uniqueNo').val(data);
                  }
              });
            }/*End If*/
          }
      });
  });
});

/*Change price unit sign.*/
$(document).ready(function() {  
  $(document).on('change', '.price',function(){
      var newPrice = $('option:selected',this).text();
      if(newPrice != "Select Price Unit"){            
        $("#amount_lbl").html('Amount '+'('+newPrice+')');
        $("#prc_lbl").html('Price '+'('+newPrice+')');
        $("#total_lbl").html('Total '+'('+newPrice+')');
      }
    });
});
/*Use : Get grade name to related Standard via ajax*/
$(document).on('change', '.std', function (event,selectedgradeid) {
  
  //event.stopImmediatePropagation();
  var id = $(this).val();
  var row_ids = $(this).attr('data-id');
  //var grade = id1.substr(id1.length - 1);
  //console.log('id='+id+'row='+row_ids+'grade='+selectedgradeid);
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getGrades/'+id,
    type: 'POST',
    cache: false,
    success: function (data) {
      $('#grade_id'+row_ids).html(data);
      $("#grade_id"+row_ids+" option[value='"+selectedgradeid+"']").attr("selected", true);
      //$('#grade_id'+row_ids).val(selectedgradeid);
      var vel = $('#grade_id'+row_ids+' option:selected').html();
      $('#select2-grade_id'+row_ids+'-container').html(vel);
    }
  });
});

$(document).on('change', '.procat', function (event,selected,grd,sel_size) {
  // event.stopImmediatePropagation();
  var procat_value = $(this).val();
  var row_id = $(this).attr('data-id'); 
  //console.log('row='+row_id+'=procat id='+procat_value+'=standard='+selected +'=grade='+ grd);
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/change_procat/'+procat_value,
    type: 'POST',
    cache: false,
    dataType : 'json',
      success: function (data) {     

      var rtrn = JSON.parse(JSON.stringify(data));
        if(rtrn.productname){ 
        $('.productname'+row_id).html('<b>Product Name</b> : '+rtrn.productname);
      }
      $('#unit'+row_id).val(rtrn.unit);
      $('#standard_id'+row_id).html(rtrn.standard);
      $("#standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true);    
      $('#standard_id'+row_id).val(selected);  
      $('#standard_id'+row_id).val(selected).trigger('change',[grd]);
      var sel = $('#standard_id'+row_id+' option:selected').html();
      $('#select2-standard_id'+row_id+'-container').html(sel);
      $('#size_id'+row_id).html(rtrn.size);
      $("#size_id"+row_id+" option[value='"+sel_size+"']").attr("selected", true);    
      $('#size_id'+row_id).val(sel_size);
      var sel2 = $('#size_id'+row_id+' option:selected').html();
      $('#select2-size_id'+row_id+'-container').html(sel2); 
    }
  });
});


 $(document).on('change', '.grd', function () {
     var id = $(this).val();
     var gd = $(this).attr('id');
     if(gd){
      var mtr = gd.substr(gd.length - 1); 
     }
     
     var next_size = $(this).closest('td').next().find(':selected').val();
     //console.log(next_size);
     $.ajax({
      url: '<?php echo WEBSITE_PATH;?>proformainvoices/getRatio/'+id,
      type: 'POST',
      cache: false,
      'data':{'size': next_size},
      dataType : 'json',
       success: function (data) {
        var rts = JSON.parse(JSON.stringify(data));
        $('#ratio'+mtr ).val(rts.finalratio);     
        var size_row ='';
        var final_rat = '';
      if(rts.size){     
        size_row = '<b>   GD(NB): </b>'+rts.size;
      }else{
        size_row = '';
      }
      if(rts.finalratio){
        final_rat = '<b>   Ratio: </b>'+rts.finalratio;
      }else{
        final_rat ='';
      }
      $('.gdnbdata'+mtr).html('  '+final_rat+' '+size_row);
      
     
    }
  });
});

  /*Use : Get goods description MTR from related to goods description MM via Ajax*/
 $(document).on('change', '.size', function () {
     var id = $(this).val();
     var size = $(this).attr('data-id'); 
     var prev_grade = $(this).closest('td').prev('td').find("select").val();
     //var size = id1.substr(id1.length - 1);
     //$("#gd_mtr"+size).val();
     //console.log('id'+id+'===size=='+size+'====grade=='+prev_grade);
     $.ajax({
      url: '<?php echo WEBSITE_PATH;?>proformainvoices/getnb/'+id,
      type: 'POST',
      cache: false,
      'data':{'grade': prev_grade},
      dataType : 'json',
       success: function (data) {
        var nb = JSON.parse(JSON.stringify(data));
         $('#ratio'+size).val(nb.finalratio);    
        var gdnb_value ='';
      if(nb.gdnb){      
        gdnb_value = '<b>   GD(NB): </b>'+nb.gdnb;
      }else{
        gdnb_value = '';
      }
      var final_ratio = '';
      if(nb.finalratio){
        final_ratio = '<b>   Ratio: </b>'+nb.finalratio;
      }else{
        final_ratio ='';
      }
        $('.gdnbdata'+size).html('  '+final_ratio+'  '+gdnb_value);
        /*var te = data.trim();
    $('#gd_mtr'+size).val(te);
    $('#gd_mtr'+size).val(te);*/
  }

 });
});

 /*Use: Count final Total and amout from quantity and price.*/
$(document).on('keyup', '.qty, .prc', function() {
  var mtr = $(this).attr('data-id');  
  var qty = $('#qty'+mtr).val();
  var price = $('#price'+mtr).val();
  var total =  parseFloat(qty) * parseFloat(price);
  var num = total.toFixed(2);
  if(!isNaN(num) && num.length != 0) {
    var amount = $('#amount'+mtr).val(num);
  }
  var sum = 0;
  $('.amount').each(function() {
    if(!isNaN(this.value) && this.value.length != 0) {
      sum += parseFloat(this.value);
      var sum1 = sum.toFixed(2);
      $('#total').val(sum1);
    }
  });
});

/*Start Fetch client name with autosuggestion*/
            $(".bill_to").select2({
                width: "on",
                //dropdownCssClass : 'bigdrop',
                width: '100%',
              //  dropdownAutoWidth : true,
                ajax: {
                    url: "<?php echo WEBSITE_PATH;?>proformainvoices/getclients/",
                    dataType: 'json',
                    delay: 10,
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
            });
            /*End Fetch client name with autosuggestion*/
            /*Fetch address based on selected client*/
            $(document.body).on("change","#ProformainvoiceBillTo,#OrderBillTo",function(){
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getclientdetail/',
                    data: 'id='+(this).value,
                    type: 'POST',
                    cache: false,
                    dataType: "json",
                    success: function (response) {
                    if(response != 0){
                        $('#bill_address').empty().append('<option value="0">Select Address</option>').find('option:first').attr("selected","selected");
                       $('#ship_address').empty().append('<option value="0">Select Address</option>').find('option:first').attr("selected","selected");
                        $("#select2-bill_address-container").html('Select Adress');
                        $("#select2-ship_address-container").html('Select Address');
                        $.each(response, function (index, item) {
                          html  = '<option value="' + item.id + '">' + item.streetaddress + ' '+ item.city +','+ item.state + ','+item.country+'</option>';
                          $('#bill_address').append(html);
                          $('#ship_address').append(html);
                        })
                        //$('#bill_address').html(address1);
                        /*If checkbox checked than set ship_to and its address*/
                        if($('#sameas').is(':checked')) {
                            $('#OrderShipTo').val($('#OrderBillTo option:selected').html());
                            $('.ship_to').val($('#ProformainvoiceBillTo option:selected').html());
                            $('#ship_address').val($('#bill_address').html().replace(/<br ?\/?>/g, ""));
                        }
                    } else {
                      $('#bill_address').empty().append('<option value="0">Select Address</option>').find('option:first').attr("selected","selected");
                       $('#ship_address').empty().append('<option value="0">Select Address</option>').find('option:first').attr("selected","selected");
                        $("#select2-bill_address-container").html('Select Adress');
                        $("#select2-ship_address-container").html('Select Address')
                     // $('#ship_address').empty();
                    }
                  }
                });    
            });

            /*If checkbox checked than set ship_to and its address*/
            $('#sameas').click(function () {
                if($('#sameas').is(':checked')) {
                    $('.ship_to').val($('.bill_to option:selected').html());
                    var bill = $('select#bill_address').val();
                    $("select#ship_address option[value="+bill+"]").prop("selected",true);
                    $("#select2-ship_address-container").html($('select#ship_address option:selected').html())
                }
            });
           $('#local').click(function () {
          var x = $("#price").val();
          if ($(this).is(':checked')) {
              var y = $(this).val();
              if (x != '2') {
                  $("#price").val('2');
                  $("#amount_lbl").html('Amount '+'(₹)');
                $("#prc_lbl").html('Price '+'(₹)');
                $("#total_lbl").html('Total '+'(₹)');
              }
          }
          if($(this).prop('checked') == false){
           var y = $(this).val();
              if (x == '2') {
                  $("#price").val('1');
                  $("#amount_lbl").html('Amount '+'($)');
                $("#prc_lbl").html('Price '+'($)');
                $("#total_lbl").html('Total '+'($)');
                 
              }
          } 
      });
</script>