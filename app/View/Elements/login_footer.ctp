        <div class="copyright"> <?php echo date("Y"); ?> &copy; Shalco. </div>
        <script type="text/javascript" src="<?php echo ROOT_PATH; ?>pages/scripts/login.min.js"></script>
        <script type="text/javascript">
			jQuery(document).ready(function() { 
			/*form submit start*/
			  $(".shalcoform").submit(function(e){
			    var formObj = $(this);
			    var formURL = formObj.attr("action"); 
			    //var formData = new FormData(this);    
			    $("#spinner").show();
			        $.ajax({
			          url: formURL,
			          type: 'POST',
			          data:  formData,
			          mimeType:"multipart/form-data",
			          dataType : 'json',
			          contentType: false,
			          cache: false,
			          processData:false, 
			          success: function(data, textStatus, jqXHR){ 
			            $("#spinner").hide();
			          },
			          error: function(jqXHR, textStatus, errorThrown){ 
			            $("#spinner").hide();
			          }           
			       });
			      e.preventDefault();
			  });
			  /*form submit end*/
			});  
        </script>>
    </body>
</html>