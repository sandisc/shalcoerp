<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo $pageTitle;?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
       <link rel="shortcut icon" href="<?php echo ROOT_PATH; ?>favicon.gif">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo ROOT_PATH;?>css/custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo ROOT_PATH;?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo ROOT_PATH;?>global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo ROOT_PATH;?>global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo ROOT_PATH;?>layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo ROOT_PATH;?>layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />

        <link href="<?php echo ROOT_PATH;?>layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />   

        <script src="<?php echo ROOT_PATH; ?>global/plugins/jquery.min.js" type="text/javascript"></script>

        <script type="text/javascript">
          var action = '<?php echo $this->params['action'];?>';
          var controller = '<?php echo $this->params['controller'];?>';
          var WEBSITE_PATH = '<?php echo WEBSITE_PATH;?>';  
          var ajaxaction = '<?php echo $ajaxaction; ?>';        
        </script>           
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/scripts/app.min.js" type="text/javascript"></script>
        <!-- start Script fo autosuggestion combo box-->
        <script src="<?php echo ROOT_PATH; ?>global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo ROOT_PATH; ?>global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>

        <!-- End Script fo autosuggestion combo box-->        
        <?php if($this->params['action'] == 'login' || $this->params['action'] == 'forgotpass' || $this->params['action'] == 'replacepass'){?>
        <link href="<?php echo ROOT_PATH;?>pages/css/login.min.css" rel="stylesheet" type="text/css">
        <?php } ?> 
        <!-- END PAGE LEVEL STYLES -->
        <script src="<?php echo ROOT_PATH; ?>pages/scripts/customheader.js" type="text/javascript"></script>
    </head>
    <!-- END HEAD -->