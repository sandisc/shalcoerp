<?php echo $this->element('sql_dump');?>
        <div class="page-footer">
            <div class="page-footer-inner"> <?php echo date("Y"); ?> &copy; Shalco.</div>
            <div class="scroll-to-top"><i class="icon-arrow-up"></i></div>
        </div>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/js.cookie.min.js" type="text/javascript"></script>

        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- DAte Picker -->
       
        
        <!-- Start Script for datatable typeahead-->
        <script src="<?php echo ROOT_PATH; ?>global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- End Script for datatable typeahead-->

        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        

        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!--<script src="<?php echo ROOT_PATH; ?>pages/scripts/form-validation.min.js" type="text/javascript"></script>-->
        <script src="<?php echo ROOT_PATH; ?>pages/scripts/table-datatables-ajax.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
               
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo ROOT_PATH; ?>layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo ROOT_PATH; ?>layouts/layout/scripts/demo.min.js" type="text/javascript"></script>

        <script src="<?php echo ROOT_PATH; ?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

        <script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
        
    </body>
</html>            