<script type="text/javascript">
/*Licence change event save, licence item id in hidden , which will be use while we add data into export table and export item table.*/

countqty(); 
countqtymt();
$(document).on('click', '.deleteno', function () {
  var t = $(this).parent().prev().find('input').attr('value');
  var mtr = $(this).attr('mydelnum');
  var upno = $('#up').val();
  var amount1 = $('#amount'+mtr).val();
  var qty1 = $('#qty'+mtr).val();
  var value=$(this).closest('tr').children('td:first').data("id");
  $('#expense_table tr:last-child td:first-child input').attr("name");     
    if(upno != '1'){
      var myno = $(this).attr('mydelnum');
      $(this).closest('tr').next('tr').remove();
      $(this).parent().parent().remove();
      var remainingno = parseInt(upno) - 1;
      var upno = $('#up').val(remainingno);
      var rem_amout = $(this).data("amount");
      var total = $('#total').val();
      if(!isNaN(amount1) && amount1.length != 0) { 
          var del_amount = parseFloat(total) - parseFloat(amount1);
      }
      $('#total').val((del_amount).toFixed(2));
      var qty_total = $('#ChalanTotalqty').val()
      if(!isNaN(qty1) && qty1.length != 0) { 
        var del_qty = parseFloat(qty_total) - parseFloat(qty1);
      }
      $('#ChalanTotalqty').val((del_qty).toFixed(3));
    }
});



 /*Use: Count final Total and amout from quantity and price.*/
$(document).on('keyup', '.qty, .prc', function() {
  var mtr = $(this).attr('data-id');
  // var mtr = gd.substr(gd.length - 1);
  var qty = $('#qty'+mtr).val();
  var price = $('#price'+mtr).val();
  var total1 =  parseFloat(qty) * parseFloat(price);
  var total = total1.toFixed(2);
  if(!isNaN(total) && total.length != 0) {
    var amount = $('#amount'+mtr).val(total);
  }
  var density = 1;
  if(density != null && density != 'undefined'){
      density = $(this).attr('data-density');    
  }
  var size_ratio = $(this).attr('data-sizeratio');
  var total =  parseFloat(qty) * parseFloat(price);
  var num = total.toFixed(2);
  var unit = $(this).attr('data-unit');
  if(!isNaN(num) && num.length != 0) {
    var amount = $('#netprice'+mtr).val(num);
  }
  
  if(unit =='kgs'){
    var total_qtymt = (parseFloat(qty) * parseFloat(1) / 1000) * parseFloat(density);
    console.log(total_qtymt);
    var total_qtymetric = total_qtymt.toFixed(3);    
   
  } else {

    var total_qtymt = (parseFloat(qty) * parseFloat(size_ratio) / 1000) * parseFloat(density);
    var total_qtymetric = total_qtymt.toFixed(3);
   
  }
  if(!isNaN(total_qtymetric) && total_qtymetric.length != 0) {
      var qty_mt = $('#qty_mt'+mtr).val(total_qtymetric);
      var totalnetweight = parseFloat(qty_mt.val()) * 1000;
  }
    
  var sum = 0;
  $('.amount').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
                var sum1 = sum.toFixed(2);
                $('#total').val(sum1);
        }

    });
   countqty();
   countqtymt();
});   
 /*calculate total of qty */
    function countqty(){
         var qtytotal = 0;  
        $('.qty').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                qtytotal += parseFloat(this.value);
                $('#ChalanTotalqty').val((qtytotal.toFixed(3)));
            }
        });
    }

    /*calculate total of qty mt*/
    function countqtymt(){
        var qtymtrtotal = 0;
        $('.qty_mt').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                qtymtrtotal += parseFloat(this.value);
                mt_total = qtymtrtotal.toFixed(3);
                $('#ChalanTotalqtymt').val(mt_total);
            }
        });
    }
 
  /*Use : Get chalan number based on change division id via Ajax*/
  $(document).on('change', '.division_id', function () {
    var old_division_id = $('#old_division_id').val();
    var id = $(this).val();
    var date = $('.changedatepicker').val(); 
    if(date  === ''){
      date = '';
    }
    /*To avoid call ajax for already set division specifically in edit case if already set division and user click on another division than back again for choose same division*/
    if(old_division_id != id){
      $.ajax({
        url: '<?php echo WEBSITE_PATH;?>chalans/getChalanNO/ajax',
        type: 'POST',
        data : 'division_id='+id+'&date='+date,
        cache: false,
        success: function (data) {
          $('#chalanno').val(data);
        }
      });
    }else{
      var old_chalanno = $('#old_chalanno').val();
      $('#chalanno').val(old_chalanno);
    }
  });

  /*Fetch chalan Number based on Change event of Date.*/
  $(document).ready(function() {  
    $(document).on('change', '.changedatepicker',function(){
      var date = $('.changedatepicker').val(); 
      var id = $('.division_id').val(); 
      var current_fy = $('#fy').val();
      // var dateObject = $('.changedatepicker').datepicker('getDate');
      $.ajax({
          url: '<?php echo WEBSITE_PATH;?>app/calculateFinancialYearForDate/',
          type: 'POST',
          cache: false,
          data : "date="+date,
          success: function (new_fy) {
            /*Check new financial year is same as current financial year*/
            if(current_fy != new_fy){ 
            /*If not than fetch new auto increment number*/      
            // var dateObject = $('.changedatepicker').datepicker('getDate');
              $.ajax({
                url: '<?php echo WEBSITE_PATH;?>chalans/getChalanNO/ajax/',
                  type: 'POST',
                  cache: false,
                  data : "date="+date,
                  success: function (data) {
                    $('.uniqueNo').val(data);
                  }
              });
            }/*End If*/
          }
      });
  });
});
</script>