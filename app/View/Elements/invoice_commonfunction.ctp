<script type="text/javascript">
/*Licence change event save, licence item id in hidden , which will be use while we add data into export table and export item table.*/
$('.licenceid').on('change', function() {
    var licitemid = $(this).find('option:selected').attr('itemid');
    //$('.licenceid').closest('tr').find('.mylicenceitemid').val(licitemid);
    $(this).closest('tr').find('.mylicenceitemid').val(licitemid);
});

if($("#gradebaseweight").length!==0) {
    var text =  $("#gradebaseweight").val();
    var grd_bse = text.replace(/<br[^>]*>/g, "\n");
    $("#gradebaseweight").val(grd_bse);
    $('#customgradebaseweight').val(grd_bse);
}

/*Fetch invoice Number based on Change event of Date.*/
$(document).ready(function() {  
  $(document).on('change', '.changedatepicker',function(){
    var date = $('.changedatepicker').val(); 
    // var dateObject = $('.changedatepicker').datepicker('getDate');
    var current_fy = $('#fy').val();
    // var dateObject = $('.changedatepicker').datepicker('getDate');
      $.ajax({
          url: '<?php echo WEBSITE_PATH;?>app/calculateFinancialYearForDate/',
          type: 'POST',
          cache: false,
          data : "date="+date,
          success: function (new_fy) {
            /*Check new financial year is same as current financial year*/
            if(current_fy != new_fy){ 
            /*If not than fetch new auto increment number*/    
                $.ajax({
                  url: '<?php echo WEBSITE_PATH;?>invoices/getInvoiceNO/ajax/',
                  type: 'POST',
                  cache: false,
                  data : "date="+date,
                  success: function (data) {
                    $('.uniqueNo').val(data);
                  }
              });
            }/*End If*/
          }
      });
  });
});
    /*Use: Count final Total and amout from quantity and price.*/
    
    /*calculate total of pcs*/
    function countpcs(){
        var loadtotal = 0;
        $('.pcs').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                    loadtotal += parseFloat(this.value);
                    $('#totalpcs').val(loadtotal);
            }
        });
    }
    /*calculate total of qty */
    function countqty(){
        var qtytotal = 0;  
        $('.qty_mtr').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                qtytotal += parseFloat(this.value);
                $('#qtymtrtotal').val((qtytotal.toFixed(3)));
            }
        });
    }
    
    /*calculate total of qty mt*/
    function countqtymt(){
        var qtymtrtotal = 0;
        $('.qty_mt').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                qtymtrtotal += parseFloat(this.value);
                mt_total = qtymtrtotal.toFixed(3);
                $('#totalqtymt').val(mt_total);
            }
        });
    }
 
    $(document).on('keyup', '.prc,.qty_mtr', function(event) {
        var totalqty_mtr = 0;
        var mtr = $(this).attr('data-id');
        
        var qty = $('#qty_mtr'+mtr).val();
        var price = $('#price'+mtr).val();
        //density add
        
        var density = 1;
        if(density != null && density != 'undefined'){
            density = $(this).attr('data-density');    
        }
        var size_ratio = $(this).attr('data-sizeratio');
        var total =  parseFloat(qty) * parseFloat(price);
        var num = total.toFixed(2);
        var unit = $(this).attr('data-unit');
        if(!isNaN(num) && num.length != 0) {
            var amount = $('#netprice'+mtr).val(num);
        }
        if(unit=='kgs'){
            var total_qtymt = (parseFloat(qty) * parseFloat(1) / 1000) * parseFloat(density);
            var total_qtymetric = total_qtymt.toFixed(3);    
        } else {
            var total_qtymt = (parseFloat(qty) * parseFloat(size_ratio) / 1000) * parseFloat(density);
            var total_qtymetric = total_qtymt.toFixed(3);
        }
        if(!isNaN(total_qtymetric) && total_qtymetric.length != 0) {
            var qty_mt = $('#qty_mt'+mtr).val(total_qtymetric);
            var totalnetweight = parseFloat(qty_mt.val()) * 1000;
        }
        if(!isNaN(totalnetweight) && totalnetweight.length != 0) {
            var totnet = $('#totalnetweight'+mtr).val(totalnetweight.toFixed(3));
            if(totalnetweight != 0){
                var totalgrossweight = parseFloat(totalnetweight) + 5;
            }
            else { 
                var totalgrossweight = parseFloat(totalnetweight) + 0;
            }
            $('#totalgrossweight'+mtr).val((totalgrossweight.toFixed(3)));
        }
            
        countqty();
        countqtymt();
        calculateamount();
        counttotalnetweight();
        counttotalgrossweight();         
    });   

    /*Count Total amount related each field value*/
    function calculateamount(){
        var sum = 0;
        $('.netprice').each(function() {
            var on_discount = $('.discount').val(); 
            if(!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value)
                $('#ciftotal').val((sum.toFixed(2)));
                var on_advance = $('#advancereceive').val();
                var dlvr_type = $('.dlvr_type').val();
                var packing = $('#packing').val();
                //$('#finalvalue').val((sum.toFixed(2)));
                //console.log(sum);
                //$('#balancevalue').val((sum.toFixed(2)));
                if(dlvr_type == 2 || dlvr_type == 3){
                    if(!isNaN(packing) && packing.length != 0) {
                        var dis_packing = parseFloat(sum) + parseFloat(packing);
                        
                        //console.log($('#finalvalue').val((dis_packing.toFixed(2))));
                        if (on_advance.length == 0) {
                             $('#balancevalue').val((dis_packing.toFixed(2)));
                        }
                        //console.log('fist bal' + dis_packing.toFixed(2));
                    }  
                    if(!isNaN(on_discount) && on_discount.length != 0) {
                        var dis_fob = parseFloat(sum) - parseFloat(on_discount);
                        $('#finalvalue').val((dis_fob.toFixed(2)));
                        if(on_advance.length == 0){
                            $('#balancevalue').val((dis_fob.toFixed(2)));
                        }
                        //console.log('2nd bal' + dis_fob.toFixed(2));
                    }
                    if(!isNaN(on_advance) && on_advance.length != 0){
                        var dis_advance = parseFloat($('#finalvalue').val()) - parseFloat(on_advance); 
                        $('#balancevalue').val((dis_advance.toFixed(2)));console.log('final bal' + dis_advance.toFixed(2));
                    }
                }
            }
        });
    }
    /*Count Total Net Weight*/
    function counttotalnetweight(){
        var totalnet = 0;
         $('.totalnet').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                totalnet += parseFloat(this.value);
                $('#totalnetweight').val((totalnet.toFixed(3)));
                $('#total_net_weight').val((totalnet.toFixed(3)));
            }
        });
    }
    /*Count Total gross Weight*/
    function counttotalgrossweight(){
        var totalgross = 0;
         $('.totalgross').each(function() {
           if(!isNaN(this.value) && this.value.length != 0) {
                totalgross += parseFloat(this.value);
                $('#totalgrossweight').val((totalgross.toFixed(3)));
                $('#total_gross_weight').val((totalgross.toFixed(3)));
            }
        });
    }

    $(document).ready(function(){      
        //var final_total = 0;    
        var all_total = 0;      
    }); 
    var fcif = $('#finalcif').val($('#ciftotal').val());
    $(document).on('keyup', '.packing,.common_cls', function() {
        var lengthtotal = 0;
        var packing = $(this).val();
        var dis = $('.discount').val();
        var advanc_value = $('#advancereceive').val();
        if(!isNaN(this.value) && this.value.length != 0){
            var final_dyn_total = parseFloat(packing) + parseFloat($('#ciftotal').val());
            $('#finalvalue').val((final_dyn_total.toFixed(2)));
            $('#balancevalue').val((final_dyn_total.toFixed(2)))
            if(!isNaN(advanc_value) && advanc_value.length != 0){
                var packing_total = parseFloat(final_dyn_total) - parseFloat(advanc_value);
                $('#balancevalue').val((packing_total.toFixed(2)))
            } 
        }
        else{
            var final_dyn_total = 0 + parseFloat($('#ciftotal').val());
            $('#finalvalue').val((final_dyn_total.toFixed(2)));
            $('#balancevalue').val((final_dyn_total.toFixed(2)))
        }
        var bal_value = $('#balancevalue').val();
        /*all_total = 0;
        var new_all_total = 0;
        $('.common_cls').each(function() {
            if(!isNaN(this.value) && this.value.length != 0){
                all_total += parseFloat(this.value);
                //final_total = parseFloat(all_total) + parseFloat($('#ciftotal').val());
                $('#finalvalue').val((all_total.toFixed(2)));
                if(!isNaN(advanc_value) && advanc_value.length != 0){
                    var t = $('#finalvalue').val((all_total.toFixed(2)));
                    var bal_total = parseFloat(all_total) - parseFloat(advanc_value);
                    $('#balancevalue').val((bal_total.toFixed(2)));
                }
                else{
                    var new_bal = parseFloat(all_total) - 0;
                    $('#balancevalue').val((new_bal.toFixed(2)));
                }  
            }
            if(!isNaN(dis) && dis.length != 0){
                if(!isNaN(this.value) && this.value.length != 0){
                new_all_total += parseFloat(this.value);
                //var new_final_total = parseFloat(new_all_total) + parseFloat($('#ciftotal').val());
                var mi_dis = parseFloat(new_all_total) - parseFloat(dis);
                if(!isNaN(advanc_value) && advanc_value.length != 0){
                    var t = $('#finalvalue').val((mi_dis.toFixed(2)));
                    var bal_total = parseFloat(t.val()) - parseFloat(advanc_value);
                    $('#balancevalue').val((bal_total.toFixed(2)));
                    }
                    else{
                        $('#balancevalue').val((mi_dis.toFixed(2)));
                    }
                }
            }
        });*/
        countfrieghtfobetc();
        recallAllTotal();
    });
    function countfrieghtfobetc(){
        var all_total = 0;
        var new_all_total = 0;
        var dis = $('.discount').val();
        var advanc_value = $('#advancereceive').val();

        $('.common_cls').each(function() {
            if(!isNaN(this.value) && this.value.length != 0){
                all_total += parseFloat(this.value);
                //final_total = parseFloat(all_total) + parseFloat($('#ciftotal').val());
                    $('#finalvalue').val((all_total.toFixed(2)));
                    if(!isNaN(dis) && dis.length != 0){
                        var dis_val = parseFloat(all_total) - parseFloat(dis);
                        $('#finalvalue').val((dis_val.toFixed(2)));
                        //$('#balancevalue').val((bal_total.toFixed(2)));
                    }
                    if(!isNaN(advanc_value) && advanc_value.length != 0){
                        var t = $('#finalvalue').val((all_total.toFixed(2)));
                        var bal_total = parseFloat(all_total) - parseFloat(advanc_value);
                        $('#balancevalue').val((bal_total.toFixed(2)));
                    }
                    else{
                        var new_bal = parseFloat(all_total) - 0;
                        $('#balancevalue').val((new_bal.toFixed(2)));
                    }     
            }
            if(!isNaN(dis) && dis.length != 0){
                if(!isNaN(this.value) && this.value.length != 0){
                new_all_total += parseFloat(this.value);
                //var new_final_total = parseFloat(new_all_total) + parseFloat($('#ciftotal').val());
                var mi_dis = parseFloat(new_all_total) - parseFloat(dis);
                if(!isNaN(advanc_value) && advanc_value.length != 0){
                    var t = $('#finalvalue').val((mi_dis.toFixed(2)));
                    var bal_total = parseFloat(t.val()) - parseFloat(advanc_value);
                    $('#balancevalue').val((bal_total.toFixed(2)));
                    }
                    else{
                        $('#balancevalue').val((mi_dis.toFixed(2)));
                    }
                }
            }
        });
    }
    $(document).on('keyup', '.discount', function() {
        var discount = $(this).val();
        var dis_mins = 0;
        var advanc_value1 = $('#advancereceive').val();
        if(!isNaN(discount) && discount.length != 0){
            if(typeof all_total == 'undefined'){
               var new_ex_val = parseFloat($('#ciftotal').val()) - parseFloat(discount);
                $('#finalvalue').val((new_ex_val.toFixed(2)));
                $('#balancevalue').val((new_ex_val.toFixed(2)));
                if(!isNaN(advanc_value1) && advanc_value1.length != 0){
                    var new_final_ex = parseFloat(new_ex_val) - parseFloat(advanc_value1);
                    $('#balancevalue').val((new_final_ex.toFixed(2)));
                }
            }
            dis_minus = parseFloat(all_total) - parseFloat(discount);
            $('#finalvalue').val((dis_minus.toFixed(2)));
            $('#balancevalue').val((dis_minus.toFixed(2)));
            if(!isNaN(advanc_value1) && advanc_value1.length != 0){
                var bal_total = parseFloat(dis_minus) - parseFloat(advanc_value1);
                $('#balancevalue').val((bal_total.toFixed(2)));
            }
        }
        else {
            if(typeof all_total == 'undefined'){
                var new_ex_val1 = parseFloat($('#ciftotal').val()) - 0;
                $('#finalvalue').val((new_ex_val1.toFixed(2)));
                $('#balancevalue').val((new_ex_val1.toFixed(2)));
                if(!isNaN(advanc_value1) && advanc_value1.length != 0){
                    var new_final_ex = parseFloat(new_ex_val1) - parseFloat(advanc_value1);
                    $('#balancevalue').val((new_final_ex.toFixed(2)));
                }
            }
            dis_minus = parseFloat(all_total) - 0;
            $('#finalvalue').val((dis_minus.toFixed(2)));
            if(!isNaN(advanc_value1) && advanc_value1.length != 0){
                var bal_total1 = parseFloat(dis_minus) - parseFloat(advanc_value1);
                $('#balancevalue').val((bal_total1.toFixed(2)));
            }
            //$('#balancevalue').val((dis_minus.toFixed(2)));
        }
    });
    $(document).on('keyup', '#advancereceive', function() {
        var fcif = $('#finalvalue').val();
        var advancereceive = $('#advancereceive').val();
        final_val = parseFloat(fcif) - parseFloat(advancereceive);
        if(!isNaN(final_val) && final_val.length != 0) {
            $('#balancevalue').val((final_val.toFixed(2)));
        }
        if(!isNaN(fcif) && finalcif.length != 0){
            $('#balancevalue').val((final_val.toFixed(2)));
        }
        if(!isNaN(advancereceive) && advancereceive.length != 0){
            $('#balancevalue').val((final_val.toFixed(2)));
        }
        else {
          final_val = parseFloat(fcif) - 0;
          $('#balancevalue').val((final_val.toFixed(2)));
        }
    });  
    $(document).on('click', '.deleteno', function () {
        var t = $(this).parent().prev().find('input').attr('value');
        var mtr = $(this).attr('mydelnum');
        
        var value=$(this).closest('tr').children('td:first').data("id");
        $('#expense_table tr:last-child td:first-child input').attr("name");     
        var upno = $('#up').val();
        var amount1 = $('#netprice'+mtr).val();
        var grossweight = $('#totalgrossweight'+mtr).val();
        var net_w = $('#totalnetweight'+mtr).val();
        var qty_m = $('#qty_mt'+mtr).val();
        var qt_mtr = $('#qty_mtr'+mtr).val();
        var id = $(this).attr('data-id');
        if(upno != '1'){
        /* set Deleted item row value */
            if(id == ''){
                $('#removal_id').val();
            }
            else{   
                $('#removal_id').val(function(i,val) { 
                    return val + (!val ? '' : ',') + id;
                });
            }
            var myno = $(this).attr('mydelnum');
            $(this).closest('tr').next('tr').remove();
            $(this).parent().parent().remove();
            var remainingno = parseInt(upno) - 1;
            var upno = $('#up').val(remainingno);

            countpcs();
            countqty();
            countqtymt();
            calculateamount();
            counttotalnetweight();
            counttotalgrossweight();  
            /*var cif_total  = $('#ciftotal').val();
            var total_grs_weight = $('#totalgrossweight').val();
            var total_nt_wgt = $('#totalnetweight').val();
            var tot_qt = $('#totalqtymt').val();
            var tot_mt = $('#qtymtrtotal').val();
            if(!isNaN(amount1) && amount1.length != 0) { 
                var del_amount = parseFloat(cif_total) - parseFloat(amount1);
            }
            if(!isNaN(grossweight) && grossweight.length != 0) { 
                var del_amount_gross = parseFloat(total_grs_weight) - parseFloat(grossweight);
            }
            if(!isNaN(net_w) && net_w.length != 0) { 
                var del_amount_net = parseFloat(total_nt_wgt) - parseFloat(net_w);
            }
            if(!isNaN(qty_m) && qty_m.length != 0) { 
                var del_amount_qt = parseFloat(tot_qt) - parseFloat(qty_m);
            }
            if(!isNaN(qt_mtr) && qt_mtr.length != 0) { 
                var del_amount_mt = parseFloat(tot_mt) - parseFloat(qt_mtr);
            }
            $('#ciftotal').val((del_amount).toFixed(2));
            $('#totalgrossweight').val((del_amount_gross).toFixed(2));
            $('#totalnetweight').val((del_amount_net).toFixed(2));
            $('#totalqtymt').val((del_amount_qt).toFixed(2));
            $('#qtytotal').val((del_amount_mt).toFixed(2));
            $('#total_net_weight').val((del_amount_net).toFixed(3));
            $('#total_gross_weight').val((del_amount_gross).toFixed(3));*/
        }
    });
</script>