			<ul class="page-breadcrumb">
			    <li>
			        <a href="<?php echo WEBSITE_PATH;?>">Home</a>
			        <i class="fa fa-circle"></i>
			    </li>
			    <?php if(isset($middle_breadcrumb)){ echo $middle_breadcrumb; } ?>
			    <li>
			    	<span><?php echo $pageTitle; ?></span>
			    </li>
			</ul>