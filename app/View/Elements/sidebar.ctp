<div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->

                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    
                    <!-- If want to show sidebar than open below tag-->
                    <!--<ul style="padding-top: 20px" data-slide-speed="200" data-auto-scroll="true" data-keep-expanded="false" class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed">-->
                    <ul data-slide-speed="200" data-auto-scroll="true" data-keep-expanded="false" class="page-sidebar-menu  page-header-fixed">

                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>

                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <!-- <li class="heading">
                            <h3 class="uppercase">Features</h3>
                        </li> -->
                        
                        <?php //in_array('users', $this->Session->read('controller_access'));
                        $roles =$this->Session->read('Auth');
                        if(in_array('users', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'users') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-users"></i>
                                <span class="title">Users</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('User List',array('controller'=>'users','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add User',array('controller'=>'users','action'=>'add'));?>
                                </li>
                            </ul>
                        </li>
                        <?php } 
                          if(in_array('clients', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item  <?php if($this->request->params["controller"] == 'clients') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-user-following"></i>
                                <span class="title">Client</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Client List',array('controller'=>'clients','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add Client',array('controller'=>'clients','action'=>'add'));?>
                                </li>                            
                            </ul>
                        </li>
                    <?php }  if($roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'standards')  { echo 'active open';}else if($this->request->params["controller"] == 'grades'){ echo 'active open';}else if($this->request->params["controller"] == 'sizes'){ echo 'active open';} ?> ">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-briefcase"></i>
                                <span class="title">Masters</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'standards')  { echo 'active open';} ?>">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Standard</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                          <?php echo $this->Html->link('Standard List',array('controller'=>'standards','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">
                                          <?php echo $this->Html->link('Add Standard',array('controller'=>'standards','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'grades')  { echo 'active open';} ?> ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Grade</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                        <?php echo $this->Html->link('Grade List',array('controller'=>'grades','action'=>'index'));?>                                        
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">                                            
                                          <?php echo $this->Html->link('Add Grade',array('controller'=>'grades','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'sizes')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Size</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                        <?php echo $this->Html->link('Size List',array('controller'=>'sizes','action'=>'index'));?>                                        
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">                                            
                                          <?php echo $this->Html->link('Add Size',array('controller'=>'sizes','action'=>'add'));?>
                                        </li>
                                    </ul>

                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'banks')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Bank</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                        <?php echo $this->Html->link('Bank List',array('controller'=>'banks','action'=>'index'));?>                                        
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">                                            
                                          <?php echo $this->Html->link('Add Bank',array('controller'=>'banks','action'=>'add'));?>
                                        </li>
                                    </ul>

                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'dispatches')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Dispatch Location</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Dispatch Location List',array('controller'=>'dispatches','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Dispatch Location',array('controller'=>'dispatches','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'productcategories')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Product Category</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Product Category List',array('controller'=>'productcategories','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Product Category',array('controller'=>'productcategories','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>                              
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'hscodes')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">HS Code</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('HS Code List',array('controller'=>'hscodes','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add HS Code',array('controller'=>'hscodes','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Industry -->

                                <li class="nav-item  <?php if($this->request->params["controller"] == 'industries')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Industry</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Industry List',array('controller'=>'industries','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Industry',array('controller'=>'industries','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Vendor -->

                                <li class="nav-item  <?php if($this->request->params["controller"] == 'vendors')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Vendor</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Vendor List',array('controller'=>'vendors','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Vendor',array('controller'=>'vendors','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item  <?php if($this->request->params["controller"] == 'factories')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Factory</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Factory List',array('controller'=>'factories','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Factory',array('controller'=>'factories','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'divisions')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Division</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                            <?php echo $this->Html->link('Division List',array('controller'=>'divisions','action'=>'index'));?>
                                        </li>
                                        <li class="nav-item <?php if($this->request->params["action"] == 'add')  { echo 'active open';} ?>">  <?php echo $this->Html->link('Add Division',array('controller'=>'divisions','action'=>'add'));?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  <?php if($this->request->params["controller"] == 'activities')  { echo 'active open';} ?>  ">
                                    <a class="nav-link nav-toggle" href="javascript:;">
                                        <span class="title">Activity</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($this->request->params["action"] == 'index')  { echo 'active open';} ?>">
                                           <?php echo $this->Html->link('Activity List',array('controller'=>'activities','action'=>'index'));?>
                                        </li>
                                    </ul>
                                </li>                                
                            </ul>
                        </li>
                    <?php }  if(in_array('licences', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'licences' || $this->request->params["controller"] == 'licenceimports' || $this->request->params["controller"] == 'licenceexports') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-tag"></i>
                                <span class="title">Licence</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index' && $this->request->params["controller"] == 'licences') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Licence List',array('controller'=>'licences','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add' && $this->request->params["controller"] == 'licences') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add Licence',array('controller'=>'licences','action'=>'add'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'index' && $this->request->params["controller"] == 'licenceimports') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Licence Import List',array('controller'=>'licenceimports','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add' && $this->request->params["controller"] == 'licenceimports') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add Licence Import',array('controller'=>'licenceimports','action'=>'add'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'index' && $this->request->params["controller"] == 'licenceexports') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Licence Export List',array('controller'=>'licenceexports','action'=>'index'));?>
                                </li>
<!--                                 <li class="nav-item <?php if($this->request->params["action"] == 'add' && $this->request->params["controller"] == 'licenceexports') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add Export Licence',array('controller'=>'licenceexports','action'=>'add'));?>
                                </li>        -->                                                         
                            </ul>
                        </li>
                        <?php } if(in_array('proformainvoices', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'proformainvoices') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-notebook"></i>
                                <span class="title">Proforma Invoice</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Proforma Invoice List',array('controller'=>'proformainvoices','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Add Proforma Invoice',array('controller'=>'proformainvoices','action'=>'add'));?>
                                </li>
                            </ul>
                        </li>
                        <?php }  if(in_array('orders', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'orders') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-basket"></i>
                                <span class="title">Order</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Order List',array('controller'=>'orders','action'=>'index'));?>
                                </li>                 
                            </ul>
                        </li>
                        <?php }  if(in_array('invoices', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'invoices') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-symbol-male"></i>
                                <span class="title">Invoice</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Invoice List',array('controller'=>'invoices','action'=>'index'));?>
                                </li>                 
                            </ul>
                        </li>
                        <?php } if(in_array('chalans', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'chalans') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-symbol-female"></i>
                                <span class="title"> Chalan</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Chalan List',array('controller'=>'chalans','action'=>'index'));?>
                                </li>                 
                            </ul>
                        </li>
                        <?php }  if(in_array('certificates', $this->Session->read('controller_access')) || $roles['User']['usertype_id'] == '1') { ?>
                        <!--<li class="nav-item <?php if($this->request->params["controller"] == 'packinglists') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-present icons"></i>
                                <span class="title">Packing</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Packing List',array('controller'=>'packinglists','action'=>'index'));?>
                                </li>                 
                            </ul>
                        </li>-->
                        <li class="nav-item <?php if($this->request->params["controller"] == 'certificates') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-graduation icons"></i>
                                <span class="title">Certificate</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Certificate List',array('controller'=>'certificates','action'=>'index'));?>
                                </li>                 
                            </ul>
                        </li>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'dummycertificates') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-graduation icons"></i>
                                <span class="title">Dummy Certificate</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Dummy Certificate List',array('controller'=>'dummycertificates','action'=>'index'));?>
                                </li>
                                <li class="nav-item <?php if($this->request->params["action"] == 'add') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Dummy Certificate Add',array('controller'=>'dummycertificates','action'=>'add'));?>
                                </li>                 
                            </ul>
                        </li>                     
                          <li class="nav-item <?php if($this->request->params["controller"] == 'reports') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-graduation icons"></i>
                                <span class="title">Report Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Report List',array('controller'=>'reports','action'=>'index'));?>
                                </li>
                            </ul>
                        </li>          
                        <li class="nav-item <?php if($this->request->params["controller"] == 'reportcategories') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-graduation icons"></i>
                                <span class="title">Report Category Listing</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'index') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Report Category List',array('controller'=>'reportcategories','action'=>'index'));?>
                                </li>
                            </ul>
                        </li>          
                        <?php } if($roles['User']['usertype_id'] == '1') { ?>
                        <li class="nav-item <?php if($this->request->params["controller"] == 'permissions') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="javascript:;">
                                <i class="icon-lock icons"></i>
                                <span class="title">Roles & Permissions</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($this->request->params["action"] == 'groupspermission') { echo 'active';} ?>">
                                  <?php echo $this->Html->link('Role / Permission',array('controller'=>'permissions','action'=>'groupspermission'));?>
                                </li>                 
                            </ul>
                        </li>                        
                         <li class="nav-item <?php if($this->request->params["controller"] == 'settings') { echo 'active open';} ?>">
                            <a class="nav-link nav-toggle" href="<?php echo WEBSITE_PATH;?>settings/index">
                                <i class="fa fa-gear"></i>
                                <span class="title">Setting</span>                                
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>