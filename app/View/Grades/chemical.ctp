<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<?php $grdname  = '';
if(!empty($this->request->data)) { $grdname = ' (GRADE : '.$this->request->data['Grade']['grade_name'].')';} ?>
<div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $pageTitle.$grdname;?>
                                                </div>                                           
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                        <div class="portlet-body form">
                                          <!-- BEGIN FORM-->
                                          <?php echo $this->Form->create('Chemical',array('class'=>'shalcoform'));?> 
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Carbon(C)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('c_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('c_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Manganese(Mn)</label>
                                                                <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('mn_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('mn_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phosphorus(P)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('p_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('p_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Sulfur(S)</label>
                                                                <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('s_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('s_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Silicon(Si)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('si_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('si_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Chromium(Cr)</label>
                                                                <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('cr_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('cr_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Nickel(Ni)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('ni_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('ni_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Molybdenum(Mo)</label>
                                                                <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('mo_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('mo_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                              <label class="control-label col-md-3">Nitrogen(N)</label>
                                                                 <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('n_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('n_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Iron(Fe)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('fe_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('fe_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Niobium(Nb)</label>
                                                                <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('nb_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('nb_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Titanium(Ti)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('ti_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('ti_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Copper(Cu)</label>
                                                                 <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('cu_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('cu_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Vanadium(V)</label>
                                                                  <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('v_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('v_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Boron(B)</label>
                                                                  <div class="col-md-4">
                                                                    <?php echo $this->Form->input('b_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));?>
                                                                    <span class="help-block"><br></span>
                                                                  </div>
                                                                 <div class="col-md-4">
                                                                  <?php echo $this->Form->input('b_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max')); ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                              <label class="control-label col-md-3">Aluminium(Al)</label>
                                                              <div class="col-md-4 input-icon right">
                                                                <?php echo $this->Form->input('ai_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));?>
                                                                  <span class="help-block"><br></span>
                                                                </div>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php echo $this->Form->input('ai_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Cerium(Ce)</label>
                                                                 <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('ce_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('ce_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Cobalt(Co)</label>
                                                                <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('co_mn', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4 input-icon right">
                                                                <?php

                                                               echo $this->Form->input('co_mx', array('type' => 'text','label'=>false,'class'=>'form-control','placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Tungsten(W)</label>
                                                               <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('w_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               echo $this->Form->input('w_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Lanthanum(La)</label>
                                                                <div class="col-md-4">
                                                                  <?php
                                                               echo $this->Form->input('la_mn', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Min'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                                 <div class="col-md-4">
                                                                  <?php

                                                               
                                                               echo $this->Form->input('la_mx', array('type' => 'text','class'=>'form-control','label'=>false,'placeholder'=>'Max'));
                                                                ?>
                                                                    <span class="help-block"><br></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                              </div>
                                                        </div>
                                                        <!--/span-->
                                                       
                                                        <!--/span-->
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                              <div class="col-md-offset-3 col-md-9">    
                                                                <button class="btn green" type="submit">Submit</button>
                                                                <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'grades'),array('class' => 'btn default')); ?>
                                                              </div>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-6"> </div>
                                                    </div>
                                                </div>
                                          <?php echo $this->Form->end();?>                                              
                                          <!-- END FORM-->
                                        </div>
                                    </div>

