<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<?php $grdname  = '';
if(!empty($this->request->data)) { $grdname = ' (GRADE : '.$this->request->data['Grade']['grade_name'].')';} ?>
<div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $pageTitle.$grdname;?>
                                                </div>                                           
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <?php echo $this->Form->create('Physical',array('class'=>'shalcoform'));?> 
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Annealing 2f<span class="required" aria-required="true"> * </span></label>
                                                        <div class="col-md-9 input-icon right">
                                                        <?php

                                                       echo $this->Form->input('annealing_f', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));
                                                        ?>
                                                            <span class="help-block"><br></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Annealing 2c</label>
                                                        <div class="col-md-9">
                                                          <?php

                                                       echo $this->Form->input('annealing_c', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                        ?>
                                                            <span class="help-block"><br></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Density<span class="required" aria-required="true"> * </span></label>
                                                        <div class="col-md-9">
                                                            <?php

                                                       echo $this->Form->input('density', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                        ?>
                                                             <span class="help-block"><br></span>
                                                              </div>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                               
                                                <!--/span-->
                                            </div>

                                       


                                            
                                            <!--/row-->
                                            <!--/row-->
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                    

                                                            <button class="btn green" type="submit">Submit</button>
                                                            <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'grades'),array('class' => 'btn default')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6"> </div>
                                            </div>
                                        </div>
                                    <?php echo $this->Form->end();?>                                              
                                    <!-- END FORM-->
                                </div>
                            </div>

