<?php
  /* 
   * Paging
   */
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($grade as $grades) {
      if($grades['Grade']['status'] == 1){
        $status = $status_list[0];
      }
      else{
        $status = $status_list[1];
      }
      $record_nextstage = $this->requestAction(
            array('controller' => 'App', 'action' => 'check_nextstage'),
            array('dependantmodel' => 'Proformaitem','conditional_parameter' => 'Proformaitem.grade_id = '.$grades['Grade']['id'])
      );
      $record_order_stage = $this->requestAction(
            array('controller' => 'App', 'action' => 'check_nextstage'),
            array('dependantmodel' => 'Orderitem','conditional_parameter' => 'Orderitem.grade_id = '.$grades['Grade']['id'])
      );
      if(empty($record_nextstage)){          
        if(empty($record_order_stage)){
          $delete_record = '<a href="'.WEBSITE_PATH.'grades/delete/'.base64_encode($grades['Grade']['id']).'" class="btn btn-danger" title="Delete Grade" id="delete" data-confirm="Are you sure to delete this grade?"><span class="glyphicon glyphicon-trash"></span></a>';
          $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$grades['Grade']['id'].'">';      
        }
        else{
            $delete_record = '';
            $checkbox_delete = '';
        }
      }else{
         $delete_record = '';
         $checkbox_delete = '';
      }
      
      $records["data"][] = array($checkbox_delete,
      $grades['Standard']['standard_name'],
      $grades['Grade']['grade_name'],
      $grades['Gradetag']['gradetagname'],
      $grades['Grade']['uns'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($grades['Grade']['modified'])),
      $grades['User']['first_name']." ".$grades['User']['last_name'],
      '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
      '<a href="'.WEBSITE_PATH.'grades/view/'.base64_encode($grades['Grade']['id']).'" class="btn btn-info" title="View Grade"><span class="glyphicon glyphicon-eye-open"></span></a>
      <a href="'.WEBSITE_PATH.'grades/add/'.base64_encode($grades['Grade']['id']).'" class="btn btn-success" title="Edit Grade"><span class="glyphicon glyphicon-pencil"></span></a>'.$delete_record.'
      <a href="'.WEBSITE_PATH.'grades/chemical/'.base64_encode($grades['Grade']['id']).'" class="btn btn-primary" title="Chemical Property">Chem</a>
      <a href="'.WEBSITE_PATH.'grades/physical/'.base64_encode($grades['Grade']['id']).'" class="btn btn-warning" title="Physical Property">Phy</a>
       <a href="'.WEBSITE_PATH.'grades/mechanical/'.base64_encode($grades['Grade']['id']).'" class="btn btn-info" title="Mechanical Property">Mech</a>'
      ,);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  echo json_encode($records); exit;
?>