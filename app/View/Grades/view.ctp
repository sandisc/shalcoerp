<h3 class="page-title"> <?php echo $mainTitle;?></h3>                  
        <!-- Grade Information -->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                <div class="tools">
                <!-- Set Edit link -->
                 <?php  echo $this->Html->link(
                       '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
                        array(
                            'controller'=>'grades',
                            'action'=>'add',
                            base64_encode($gradedata['Grade']['id'])
                        ),
                        array(
                            'rel'=>'tooltip',
                            'data-placement'=>'left',
                            'data-original-title'=>'Edit',
                            'title' => 'Edit Grade',
                            'class'=>'',
                            'escape'=>false  //NOTICE THIS LINE ***************
                        )
                    );    ?>      
                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                </div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Standard Name</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Standard']['standard_name'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Grade Name</label>
                                <div class="col-md-8">
                                 <?php echo $gradedata['Grade']['grade_name'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>                        
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Grade Tag Name</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Gradetag']['gradetagname'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">UNS</label>
                                <div class="col-md-8">
                                 <?php echo $gradedata['Grade']['uns'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>            
                    <!-- -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                   <?php $status = $gradedata['Grade']['status'];
                                        if($status == 1){
                                            echo "Active";
                                        }
                                        else{
                                            echo "Inactive";
                                        }?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                        
                </div>
            </div>
        </div>

        <!-- Physical -->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>Physical Property</div> 
                <div class="tools">
                    <!-- Set Edit link -->
                     <?php  echo $this->Html->link(
                           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
                            array(
                                'controller'=>'grades',
                                'action'=>'physical',
                                base64_encode($gradedata['Grade']['id'])
                            ),
                            array(
                                'rel'=>'tooltip',
                                'data-placement'=>'left',
                                'data-original-title'=>'Edit',
                                'title' => 'Edit Physical',
                                'class'=>'',
                                'escape'=>false  //NOTICE THIS LINE ***************
                            )
                        );    ?>                 
                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Annealing 2f</label>
                                    <div class="col-md-8 input-icon right">
                                   <?php echo $gradedata['Physical']['annealing_f'];?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Annealing 2c</label>
                                    <div class="col-md-8">
                                     
                                   <?php echo $gradedata['Physical']['annealing_c'];
                                    ?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Density</label>
                                    <div class="col-md-8">
                                        <?php echo $gradedata['Physical']['density'];?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>                                                  
            </div>
        </div>

        <!-- Chemical -->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>Chemical Property</div>                    
                <div class="tools">
                    <!-- Set Edit link -->
                     <?php  echo $this->Html->link(
                           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
                            array(
                                'controller'=>'grades',
                                'action'=>'chemical',
                                base64_encode($gradedata['Grade']['id'])
                            ),
                            array(
                                'rel'=>'tooltip',
                                'data-placement'=>'left',
                                'data-original-title'=>'Edit',
                                'title' => 'Edit Chemical',
                                'class'=>'',
                                'escape'=>false  //NOTICE THIS LINE ***************
                            )
                        );    ?>                 
                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-4 input-icon right">
                                   <b> Min </b>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                   <b> Max </b>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-4 input-icon right">
                                   <b> Min </b>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                    <b> Max </b>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Carbon(C)</label>
                                <div class="col-md-4 input-icon right">
                                    <?php echo $gradedata['Chemical']['c_mn'];?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                    <?php echo $gradedata['Chemical']['c_mx'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Manganese(Mn)</label>
                                <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['mn_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['mn_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Phosphorus(Ph)</label>
                                <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['p_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                              <?php

                               echo $gradedata['Chemical']['p_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Sulfur(S)</label>
                                <div class="col-md-4">
                                <?php

                               echo $gradedata['Chemical']['s_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['s_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Silicon(Si)</label>
                                <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['si_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['si_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Chromium(Cr)</label>
                                <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['cr_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['cr_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Nickel(Ni)</label>
                                <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['ni_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['ni_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Molybdenum(Mo)</label>
                                <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['mo_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                  <?php


                               echo $gradedata['Chemical']['mo_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label col-md-4"> Nitrogen(N)</label>
                               <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['n_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['n_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Iron(Fe)</label>
                                <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['fe_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['fe_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Niobium(Nb)</label>
                                <div class="col-md-4">
                                   <?php

                               echo $gradedata['Chemical']['nb_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                   <?php

                               echo $gradedata['Chemical']['nb_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Titanium(Ti)</label>
                                <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['ti_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                 <?php

                               echo $gradedata['Chemical']['ti_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Copper(Cu)</label>
                                 <div class="col-md-4">
                                <?php

                               echo $gradedata['Chemical']['cu_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['cu_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vanadium(V)</label>
                                  <div class="col-md-4 input-icon right">
                                 <?php

                               echo $gradedata['Chemical']['v_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['v_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Boron(B)</label>
                                  <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['b_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                   <?php

                               echo $gradedata['Chemical']['b_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Aluminium(aI</label>
                              <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['ai_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['ai_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Cerium(Ce)</label>
                                 <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['ce_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                   <?php

                               echo $gradedata['Chemical']['ce_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Cobalt(Co)</label>
                                <div class="col-md-4 input-icon right">
                               <?php

                               echo $gradedata['Chemical']['co_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4 input-icon right">
                                <?php

                               echo $gradedata['Chemical']['co_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->                                                        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tungsten(W)</label>
                               <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['w_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['w_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Lanthanum(La)</label>
                                  <div class="col-md-4">
                                 <?php

                               echo $gradedata['Chemical']['la_mn']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                                 <div class="col-md-4">
                                  <?php

                               echo $gradedata['Chemical']['la_mx']
                                ?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>                                                         
                    <!--/row-->
                </div>                                                   
            </div>
        </div>


        <!-- Mechanical Property-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>Mechanical Property  <p class="std_name">
                Standard:- <?php echo $this->requestAction('App/getStandard/'.$gradedata['Grade']['standard_id']); ?></p></div> 

                <div class="tools">
                    <!-- Set Edit link -->
                     <?php  echo $this->Html->link(
                           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
                            array(
                                'controller'=>'grades',
                                'action'=>'mechanical',
                                base64_encode($gradedata['Mechanical']['grade_id'])
                            ),
                            array(
                                'rel'=>'tooltip',
                                'data-placement'=>'left',
                                'data-original-title'=>'Edit',
                                'title' => 'Edit Mechanical',
                                'class'=>'',
                                'escape'=>false  //NOTICE THIS LINE ***************
                            )
                        );    ?>                 
                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tensile Strength (Mpa)</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Mechanical']['tensile_strength'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Yield Strength (Mpa)</label>
                                <div class="col-md-8">
                                   <?php echo $gradedata['Mechanical']['yield_strength'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    
                    <!--/row-->
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Elongation (%)</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Mechanical']['elongation'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Hardness</label>
                                <div class="col-md-8">
                                  <?php echo $gradedata['Mechanical']['hardness'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Flattening Test</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Mechanical']['flattening'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Flaring Test</label>
                                <div class="col-md-8">
                                  <?php echo $gradedata['Mechanical']['flaring'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Eddy Current Test</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $gradedata['Mechanical']['eddy'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Ultrasonic Test</label>
                                <div class="col-md-8">
                                  <?php echo $gradedata['Mechanical']['ultrasonic'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">IGC Test</label>
                                <div class="col-md-8">
                                  <?php echo $gradedata['Mechanical']['igc'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Other Standard's Mechanical Property-->
        <?php  if(!empty($same_grade_mechanical)){
            foreach($same_grade_mechanical as $samegrade_mechdata) {?>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>Mechanical Property  <p class="std_name">
                Standard:- <?php echo $this->requestAction('App/getStandard/'.$samegrade_mechdata['Grade']['standard_id']); ?></p></div> 

                <div class="tools">
                    <!-- Set Edit link -->
                     <?php  echo $this->Html->link(
                           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
                            array(
                                'controller'=>'grades',
                                'action'=>'mechanical',
                                base64_encode($samegrade_mechdata['Mechanical']['grade_id'])
                            ),
                            array(
                                'rel'=>'tooltip',
                                'data-placement'=>'left',
                                'data-original-title'=>'Edit',
                                'title' => 'Edit Mechanical',
                                'class'=>'',
                                'escape'=>false  //NOTICE THIS LINE ***************
                            )
                        );    ?>                 
                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form metroform">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tensile Strength (Mpa)</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $samegrade_mechdata['Mechanical']['tensile_strength'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Yield Strength (Mpa)</label>
                                <div class="col-md-8">
                                   <?php echo $samegrade_mechdata['Mechanical']['yield_strength'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    
                    <!--/row-->
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Elongation (%)</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $samegrade_mechdata['Mechanical']['elongation'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Hardness</label>
                                <div class="col-md-8">
                                  <?php echo $samegrade_mechdata['Mechanical']['hardness'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Flattening Test</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $samegrade_mechdata['Mechanical']['flattening'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Flaring Test</label>
                                <div class="col-md-8">
                                  <?php echo $samegrade_mechdata['Mechanical']['flaring'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Eddy Current Test</label>
                                <div class="col-md-8 input-icon right">
                                <?php echo $samegrade_mechdata['Mechanical']['eddy'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Ultrasonic Test</label>
                                <div class="col-md-8">
                                  <?php echo $samegrade_mechdata['Mechanical']['ultrasonic'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">IGC Test</label>
                                <div class="col-md-8">
                                  <?php echo $samegrade_mechdata['Mechanical']['igc'];?>
                                    <span class="help-block"><br></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } }?>
