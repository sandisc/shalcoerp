<h3 class="page-title"> <?php echo $mainTitle;?></h3>
                  
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                <div class="tools">
                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
             <?php echo $this->Form->create('Grade',array('class'=>'shalcoform')); 
              echo $this->Form->input('id', array('type'=>'hidden'));?> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Standard<span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9 input-icon right">
                                        <?php echo $this->Form->input('standard_id', array('type' => 'select','options'=>$std_id,'label'=>false,'required'=>true,'class'=>'form-control','required'));?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Grade Name<span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <?php echo $this->Form->input('grade_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Grade Tag<span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <?php echo $this->Form->input('gradetagid', array('type' => 'select','class'=>'form-control','label'=>false,'options'=>$grade_tag,'empty'=>'Please select Gradetag','required')); ?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">UNS<span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <?php echo $this->Form->input('uns', array('type' => 'text','class'=>'form-control','label'=>false,'required')); ?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>                                                                                      
                            <!--/span-->
                        </div>
                       
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-9">
                                        <?php echo $this->Form->input('status', array('type' => 'select','options'=>array('1'=>'Active','0'=>'Inactive'),'label'=>false,'required'=>true,'class'=>'form-control','required')); ?>
                                        <span class="help-block"><br></span>
                                    </div>
                                </div>
                            </div>                                                                                      
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">                                
                                        <button class="btn green" type="submit">Submit</button>
                                        <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'grades'),array('class' => 'btn default')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end();?>              
                <!-- END FORM-->
            </div>
        </div>