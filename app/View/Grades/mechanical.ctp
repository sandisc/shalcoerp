<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<?php $grdname  = '';
if(!empty($this->request->data)) { $grdname = ' (GRADE : '.$this->request->data['Grade']['grade_name'].')';} ?>
<div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <?php echo $pageTitle.$grdname;?>
                                                </div>                                           
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                             <?php echo $this->Form->create('Mechanical',array('class'=>'shalcoform'));
                                                   
                                              ?> 

                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Tensile Strength (Mpa)<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('tensile_strength', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Yield Strength (Mpa)<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('yield_strength', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        
                                                        <!--/row-->
                                                          <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Elongation(%)<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('elongation', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Hardness(HRB)<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('hardness', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Flattening Test<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('flattening', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Flaring Test<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('flaring', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Eddy Current Test<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('eddy', array('type' => 'text','label'=>false,'required'=>true,'class'=>'form-control','required'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Ultrasonic Test<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('ultrasonic', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>


                                                          <div class="row">
                                                            
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">IGC TEST<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('igc', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                

                                                                        <button class="btn green" type="submit">Submit</button>
                                                                        <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'grades'),array('class' => 'btn default')); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                    <?php echo $this->Form->end();?>
                                              
                                                <!-- END FORM-->
                                            </div>
                                        </div>