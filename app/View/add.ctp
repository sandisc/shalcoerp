<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<?php echo $this->Form->create('Proformainvoice',array('type' => 'file','class'=>'shalcoform')); ?>				  
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
		<div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
	</div>
	<div class="portlet-body form">
		<div class="form-body">	
			<div class="row">
				<div class="form-group">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">Proforma No.<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-4">
							<?php echo $this->Form->input('proforma_no', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','readonly')); ?>
								<span class="help-block"><br></span>
							</div>
						</div>
					</div>															
					<div class="col-md-6">
						<div class="form-group">
						    <label class="control-label col-md-3">Date<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-4">
							   <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
							   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<?php echo $this->Form->input('proforma_date', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true)); ?>
								</div>
								<span class="help-block"><br></span>
	                        </div>
						</div>
					</div>															
				</div>
			</div>
			<!--/row--> 
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Shipping terms<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-3 input select">
                            <select id="delivery_type" name="data[Proformainvoice][delivery_type]" class="form-control">
                                <option value="">Delivery Type</option>
                                <option value="1">CIF</option>
                                <option value="2">FOB</option>
                                <option value="3">Ex-work</option>
                                <option value="4">CNF</option>
                            </select>
                        </div>                                
                        <div class="col-md-6">																			
							<?php echo $this->Form->input('tod', array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','required'=>true,'id'=>'tod','type'=>'textarea','maxlength'=>'1000')); ?>							
							<span class="help-block"><br></span>
						</div>
					</div>
				</div>															
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Terms of Payment<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('top', array('cols'=>'2','rows'=>'2','class'=>'form-control','label'=>false,'required'=>true,'id'=>'top','type'=>'textarea','maxlength'=>'1000')); ?>
                            <span class="help-block"><br></span>
						</div>
					</div>
				</div>
			</div>
            <!--/row-->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Bill To<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-7">						
                            <?php  echo $this->Form->input('bill_to', array('class'=>'form-control bill_to comboselection','options'=>$clients,'type'=>'select', 'empty' => 'Select Client','label'=>false,'required'=>true,'id'=>'bill_to'));?>
                            <span class="help-block"><br></span>
					    </div>
					    <div class="col-md-2 checkbox margin0px">
                            <a onclick="addClient()" data-target="#addClientModal" data-toggle="modal">Add Client</a>
                        </div>
					</div>					
                </div>                                								
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Ship To<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">  <?php echo $this->Form->input('ship_to', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'id'=>'ship_to')); ?>
						 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
			</div>
            <!--/row-->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					   <label class="control-label col-md-3">Bill Address</label>
					   <div class="col-md-9"> 
					   		<div id="bill_address"></div> 
					    	<?php //echo $this->Form->textarea('bill_address', array('class'=>'form-control','cols'=>'2','rows'=>'2','label'=>false,'required'=>true,'id'=>'bill_address','readonly'));?>
					    	<span class="help-block"><br></span>						
						</div>
					</div>
				</div>
                <div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Ship Address <span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">   <?php echo $this->Form->input('ship_address', array('class'=>'form-control','cols'=>'2','rows'=>'2','label'=>false,'required'=>true,'id'=>'ship_address','type'=>'textarea','maxlength'=>'1000')); ?>
						 <span class="help-block"><br></span>
						</div>
					</div>
				</div>															
			</div>
			<!--/row-->
			
           	<div class="row">
				 <div class="col-md-6">
                    <div class="form-group">                    	
                    	<!-- <div class="col-md-1"></div> -->
                    	<div class="col-md-12">
                    	<label><input type="checkbox" value="Same As" id="sameas" name="sameas" class="sameas">Copy billing details to shipping details</label>
                        <!--     <label><input type="checkbox" value="1" id="local" name="data[Proformainvoice][local]">Is Indian order ?</label> -->
                        </div>
	                </div>
	             </div>
                <div class="col-md-6">
					<div class="form-group">
						<!--<div class="col-md-6 checkbox margin0px">-->
	                    <div class="col-md-5">
                            <label><input type="checkbox" value="1" id="local" name="data[Proformainvoice][local]">Is Indian order ?</label>
                        </div>
						<label class="control-label col-md-3">Price Unit<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-4"> 
	                        <div class="input select"><select required="required" id="price" class="form-control price" name="data[Proformainvoice][priceid]" required="required">
	                        	<option value="0">Select Price Unit</option>
	                        	<?php foreach($price as $pr =>$v){?>
								<option value="<?php echo $pr;?>" <?php if($pr == '1'){ ?> selected="selected" <?php } ?>><?php echo $v;?></option><?php  }?>
								</select>
							</div>
							<span class="help-block"><br></span>
	                    </div>
                	</div>
  				</div>
  			</div>
  			<button id="btnAdd" name="btnAdd" class="span1" >ADD</button>
  			<div class="table-scrollable">
				<table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
					<thead class="flip-content portlet box green">
						<?php //echo proformaitemTableColumn();?>	
<tr>
		<th width="5%">Sr. No.</th>
		<th width="15%">Product Category</th>
		<!-- <th width="15%">Product Name</th> -->
		<th width="12%">Standard</th>
		<th width="12%">Grade</th>
		<th width="18%">Good Description</th>
		<!-- <th width="6%"> Ratio</th> -->
		<th width="6%"> Length</th>		
		<th width="12%" id="qty_lbl"> Qty</th>
		<th width="6%"> Unit</th>
		<th width="12%" id="prc_lbl">Price</th>
		<th width="12%" id="amount_lbl">Amount</th>
		<th width="10%"> Action </th>
	</tr>												   
	                </thead>
					<tbody>
					<tr class="addmore1" data-id="1">
						
							<td rowspan="2"><?php echo $this->Form->input('sr_no', array('class'=>'form-control sr_no nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no1','name'=>'data[Proformainvoice][item][1][sr_no]','value'=>1));?>
							</td>
							<td><?php echo $this->Form->input('procatid', array('class'=>'form-control procat comboselection nameclass','type'=>'select','options' => $procat, 'empty' => 'Select Product Category','label'=>false,'required'=>true,'id'=>'procatid1','data-id'=>1,'name'=>'data[Proformainvoice][item][1][procatid]'));?>
							</td>
							<td><?php echo $this->Form->input('standard_id', array('class'=>'form-control std comboselection nameclass','type'=>'select', 'empty' => 'Select Standard','label'=>false,'required'=>true,'id'=>'standard_id1','name'=>'data[Proformainvoice][item][1][standard_id]','data-id'=>1));?>
							</td>
							<td><?php echo $this->Form->input('grade_id', array('class'=>'form-control grd comboselection nameclass','type'=>'select','empty' => 'Select Grade','label'=>false,'required'=>true,'id'=>'grade_id1','name'=>'data[Proformainvoice][item][1][grade_id]'));?>
							</td>
							<td style="white-space:nowrap;">
	                          <?php echo $this->Form->input('size_id', array('class'=>'form-control size comboselection nameclass','type'=>'select','options'=>$gdmm,'empty'=>'Select Size','label'=>false,'required'=>true,'id'=>'size_id1','name'=>'data[Proformainvoice][item][1][size_id]','data-id'=>1)); ?>	                         
							</td>
							<td style="white-space:nowrap;">
							  <?php echo $this->Form->input('length', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'length1','name'=>'data[Proformainvoice][item][1][length]'));?>
							</td>							
							<td> 
							  <?php echo $this->Form->input('qty', array('class'=>'form-control qty nameclass','type'=>'text' ,'label'=>false,'required'=>true,'id'=>'qty1','name'=>'data[Proformainvoice][item][1][qty]','data-id'=>1));?>
							</td>
							<td style="white-space:nowrap;">
							  <?php echo $this->Form->input('unit', array('class'=>'form-control nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'unit1','name'=>'data[Proformainvoice][item][1][unit`]','readonly'));?>
							</td>
							<td rowspan="2"> 
							  <?php echo $this->Form->input('price', array('class'=>'form-control prc nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'price1','name'=>'data[Proformainvoice][item][1][price]','data-id'=>1));?>
							</td>
							<td rowspan="2">
							<?php echo $this->Form->input('amount', array('class'=>'form-control amount nameclass','label'=>false,'type'=>'text','required'=>true,'id'=>'amount1','readonly','name'=>'data[Proformainvoice][item][1][amount]','data-id'=>1));?>
							</td>
							<td rowspan="2" class="numeric"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'deleteno','value'=>'1','title'=>'Delete')); ?>&nbsp;&nbsp;
	                            <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addno','value'=>'1')); ?>
	                        </td> 														
						</tr>
						<tr class="readonlydetails">
						  <td colspan="3" class="productname1"></td>
						  <td colspan="3" class="sizedata1"></td>
						</tr>					
					   
						<tr id="1" class="item_total">
	     				   <td colspan="8"></td>
	     				   <td id="total_lbl"> Total </td>
	     				   <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly'));
							?></td>
					     </tr>											
					</tbody>
				</table>
			</div>
			<input type="hidden" name="updateno" id="up" value="1" class="input" />
			<input type="hidden" name="remno" id="remno" value="1" width="35px;"/>

			<!-- New Row Qunatity -->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Quantity<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('quantity', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- New Row Delivery Period -->
	        <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Delivery Period<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('delivery_period', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Dimension -->
	         <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Dimensions<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('dimensions', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
			<!-- Certifications -->
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Certification<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('certifications', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Comments -->
	         <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Comment</label>
							<div class="col-md-9">
							<?php echo $this->Form->input('comment', array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','required'=>true,'id'=>'comment','type'=>'textarea','maxlength'=>'1000')); ?>					 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>
	        <!-- Bank Details -->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Bank Name<span class="required" aria-required="true"> * </span></label>
							<div class="col-md-9">
							<?php echo $this->Form->input('bank_id', array('label'=>false,'type'=>'select','options'=>$bank,'required'=>true,'class'=>'form-control','empty'=>'Select Your Bank','id'=>'bank_id')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
	        </div>

	        <!-- row-->			
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
							<?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'proformainvoices'),array('class' => 'btn default')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6"> </div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->
<script type="text/javascript">
/*Add client using modal dialog*/
function addClient() {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>clients/add/targetid:bill_to',
    type: 'POST',
    success: function(data) {
      $('#addClientModal .previewText').html(data);
      $('#addClientModal .page-title').remove();
      $('#addClientModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}

/*Add size using modal dialog*/
function addSize(id) {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>sizes/add/targetid:'+id,
    type: 'POST',
    success: function(data) {
      $('#addSizeModal .previewText').html(data);
      $('#addSizeModal .page-title').remove();
      $('#addSizeModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}
</script>

<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addClientModalLabel" role="dialog" tabindex="-1" id="addClientModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addSizeModalLabel" role="dialog" tabindex="-1" id="addSizeModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
//var prc = document.getElementById("price").value="&#x24;";
//document.getElementById("price").innerHTML='<option value="1">&#x24;</option>';  
/* Use  : Get Price of Amount ex $,& */
$(document).ready(function() {		
$('#btnAdd').on('click', function (e) {
        e.preventDefault();
       $('.readonlydetails').toggle();
    });

			$("#amount_lbl").html('Amount '+'($)');
       		$("#prc_lbl").html('Price '+'($)');
        	$("#total_lbl").html('Total '+'($)');
   $(document).on('change', '.price',
      function(){
        var newPrice = $('option:selected',this).text();
        if(newPrice != "Select Price Unit") 
         {
         	
       		$("#amount_lbl").html('Amount '+'('+newPrice+')');
       		$("#prc_lbl").html('Price '+'('+newPrice+')');
        	$("#total_lbl").html('Total '+'('+newPrice+')');
      	}
  });
     });


/*Use : Get grade name to related Standard via ajax*/
$(document).on('change', '.std', function (event,stand) {
	//event.stopImmediatePropagation();
		 var id = $(this).val();
		 var grade = $(this).attr('data-id');

		 //var grade = id1.substr(id1.length - 1);
console.log('id='+id+'row='+grade+'grade='+stand);
		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getGrades/'+id,
			type: 'POST',
			cache: false,
		   success: function (data) {
		$('#grade_id'+grade).html(data);
		$("#grade_id"+grade+" option[value='"+stand+"']").attr("selected", true);
		var vel = $('#grade_id'+grade+' option:selected').html();
		$('#select2-grade_id'+grade+'-container').html(vel);
	}

 });
});
/*$(document).on('change', '.procat', function (event,selected,grd) {
	// event.stopImmediatePropagation();
	var procat_value = $(this).val();
	var row_id = $(this).attr('data-id');
	//console.log(selected);
	console.log('row='+row_id+'=procat id='+procat_value+'=standard='+selected +'=grade='+ grd);
	//var prod = row_id.substr(row_id.length - 1);
	//console.log(row_id);

	$.ajax({
		url: '<?php echo WEBSITE_PATH;?>proformainvoices/getStandards/'+procat_value,
		type: 'POST',
		cache: false,
	    success: function (data) {		   	
		   	//console.log($("standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true));
			$('#standard_id'+row_id).html(data);
			$("#standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true);		
			$('#standard_id'+row_id).val(selected);
			$('#standard_id'+row_id).val(selected).trigger('change',[grd]);
			var sel = $('#standard_id'+row_id+' option:selected').html();
			$('#select2-standard_id'+row_id+'-container').html(sel);
			//console.log($("#grade_id"+row_id+" option[value='"+grade+"']").attr("selected", true));
			//var grd = $('#grade_id'+row_id+' option:selected').html();
			//$('#select2-grade_id'+row_id+'-container').html(grd);		 
		}
	});
});*/
$(document).on('change', '.procat', function (event,selected,grd,sel_size) {
	// event.stopImmediatePropagation();
	var procat_value = $(this).val();
	var row_id = $(this).attr('data-id');
	console.log('row='+row_id+'=procat id='+procat_value+'=standard='+selected +'=grade='+ grd);

	$.ajax({
		url: '<?php echo WEBSITE_PATH;?>proformainvoices/change_procat/'+procat_value,
		type: 'POST',
		cache: false,
		dataType : 'json',
	    success: function (data) {		 
	    var rtrn = JSON.parse(JSON.stringify(data));
  			if(rtrn.productname){
				$('.productname'+row_id).text('Product name:-'+rtrn.productname);
			}
			$('#unit'+row_id).val(rtrn.unit);

			$('#standard_id'+row_id).html(rtrn.standard);
			$("#standard_id"+row_id+" option[value='"+selected+"']").attr("selected", true);		
			$('#standard_id'+row_id).val(selected);
			$('#standard_id'+row_id).val(selected).trigger('change',[grd]);
			var sel = $('#standard_id'+row_id+' option:selected').html();
			$('#select2-standard_id'+row_id+'-container').html(sel);

			$('#size_id'+row_id).html(rtrn.size);
			$("#size_id"+row_id+" option[value='"+sel_size+"']").attr("selected", true);		
			$('#size_id'+row_id).val(sel_size);
			var sel2 = $('#size_id'+row_id+' option:selected').html();
			$('#select2-size_id'+row_id+'-container').html(sel2); 
		}
	});
});
 $(document).on('change', '.grd', function () {
		 var id = $(this).val();
		 var gd = $(this).attr('id');
		 if(gd){
		 	var mtr = gd.substr(gd.length - 1);	
		 }
		 var next_size = $(this).closest('td').next('td').find("select").val();		 	  
		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getRatio/'+id,
			type: 'POST',
			cache: false,
			'data':{'size': next_size},
			dataType : 'json',
		   success: function (data) {
		   	var rts = JSON.parse(JSON.stringify(data));
			$('#gd_mtr'+mtr).val(rts.size);
			$('#ratio'+mtr ).val(rts.finalratio);   	
		 
		}
	});
});

 /*Use : Get goods description MTR from related to goods description MM via Ajax*/
 $(document).on('change', '.size', function () {
		 var id = $(this).val();
		 var size = $(this).attr('data-id');
		 var prev_grade = $(this).closest('td').prev('td').find("select").val();
		 //var size = id1.substr(id1.length - 1);
		 //$("#gd_mtr"+size).val();
		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getnb/'+id,
			type: 'POST',
			cache: false,
			'data':{'grade': prev_grade},
			dataType : 'json',
		   success: function (data) {
		   	var nb = JSON.parse(JSON.stringify(data));
			$('#gd_mtr'+size).val(nb.gdnb);
			$('#ratio'+size).val(nb.finalratio);   	
		   	/*var te = data.trim();
		$('#gd_mtr'+size).val(te);
		$('#gd_mtr'+size).val(te);*/
	}

 });
});

 /*Use: Count final Total and amout from quantity and price.*/
$(document).on('keyup', '.qty, .prc', function() {
	var mtr = $(this).attr('data-id');
	//var mtr = gd.substr(gd.length - 1);
	//alert(mtr);
	var qty = $('#qty'+mtr).val();
	var price = $('#price'+mtr).val();
	var total =  parseFloat(qty) * parseFloat(price);
	var num = total.toFixed(2);
	if(!isNaN(num) && num.length != 0) {
	 	var amount = $('#amount'+mtr).val(num);
	}
    var sum = 0;
   $('.amount').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
                var sum1 = sum.toFixed(2);
				$('#total').val(sum1);
        }

    });
});   

	/*Use : Add more Row Functionality.*/
	$(document).on('click', '.addno', function () {
		var no = $('#up').val();
		var nxt = parseInt(no) + 1;
		var address = [];

		$(this).closest('tr').find('.nameclass').each(function() {
	        var id = $(this).val();
	        address.push(id);
	    });	
	    //console.log(address[6] +'==='+ address[7]+'==='+ address[8]+'==='+ address[9]+'==='+ address[10]+'==='+ address[11]);
	   // var ad = address[2].replace(/"/g, '&quot;');
	   // var t = address[6].replace(/"/g, '&quot;');	
	    var nextsizerowid = "'size_id"+nxt+"'";


	    $(".item_total").before('<tr class="addmore'+nxt+'" data-id="'+nxt+'"><td rowspan="2"><div class="input text"><input type="text" id="sr_no'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][sr_no]" value="'+nxt+'"></div></td><td><div class="input select"><select id="procatid'+nxt+'" required="required" data-id='+nxt+' class="form-control procat comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][procatid]" data-id='+nxt+'><option value="">Select Category</option><?php foreach($procat as $pro_cat => $val){ ?><option value=<?php echo $pro_cat;?>><?php echo $val;?></option><?php } ?></select></div></td><td><div class="input select"><select id="standard_id'+nxt+'" required="required" class="form-control std comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][standard_id]" data-id='+nxt+'><option value="">Select Standard</option></select></div></td><td><div class="input select"><select id="grade_id'+nxt+'" required="required" class="form-control grd comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][grade_id]"><option value="">Select Grade</option></select></div></td><td style="white-space:nowrap"><select id="size_id'+nxt+'" required="required" class="form-control size comboselection nameclass" name="data[Proformainvoice][item]['+nxt+'][size_id]" data-id='+nxt+'><option value="">Select Size</option></select></div><a onclick="addSize('+nextsizerowid+')" data-target="#addSizeModal" data-toggle="modal">  Add Size</a></td><td style="white-space:nowrap;"><div class="input text"><input type="text" id="length'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][length]" value="'+address[5]+'"><div></td><td><div class="input text"><input type="text" id="qty'+nxt+'" required="required" class="form-control qty" name="data[Proformainvoice][item]['+nxt+'][qty]" data-id='+nxt+'></div></td><td style="white-space:nowrap;"><div class="input text"><input type="text" id="unit'+nxt+'" required="required" class="form-control nameclass" name="data[Proformainvoice][item]['+nxt+'][unit]" value="'+address[7]+'" readonly="readonly"><div></td><td rowspan="2"><div class="input text"><input type="text" id="price'+nxt+'" required="required" class="form-control prc nameclass" name="data[Proformainvoice][item]['+nxt+'][price]" data-id='+nxt+'></div></td><td rowspan="2"><div class="input number"><input type="text" readonly="readonly" id="amount'+nxt+'" required="required" class="form-control amount nameclass" name="data[Proformainvoice][item]['+nxt+'][amount]"></div></td><td class="numeric" rowspan="2"><img class="deleteno" alt="delete" mydelnum="'+nxt+'"src="<?php echo ROOT_PATH;?>img/warning.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="16" height="16" id="add" class="addno" alt="add" src="<?php echo ROOT_PATH;?>img/add.png"></td></tr><tr class="readonlydetails"><td colspan="3" class="productname'+nxt+'"></td><td colspan="3" class="sizedata'+nxt+'"></td></tr>');

	    
		var upno = $('#up').val(nxt);	
	    var remmno = $('#remno').val();
	    var remnextno = parseInt(remmno) + 1;
	    var t = $('#remno').val(remnextno);
	    
	    $("#procatid"+nxt+" option[value='"+address[1]+"']").attr("selected", true);		
		$("#grade_id"+nxt+" option[value='"+address[2]+"']").attr("selected", true);
		$("#size_id"+nxt+" option[value='"+address[4]+"']").attr("selected", true);		    		    

	   /* $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getgdmm',
			type: 'POST',
			cache: false,
		    success: function (data) {
				$('#size_id'+nxt).html(data);
				$("#size_id"+nxt+" option[value='"+address[5]+"']").attr("selected", true);
				$("#select2-size_id"+nxt+"-container").text($("#size_id"+nxt+" option[value='"+address[5]+"']").text());
			}
		});*/
		$("#procatid"+nxt).val(address[1]);
		$("#procatid"+nxt).val(address[1]).trigger('change',[address[2],address[3],address[4]]);
   	    $(".comboselection").select2();
	});

	/* Use : Delete row from list. */
	$(document).on('click', '.deleteno', function () {
			
		var upno = $('#up').val();
		var rm = $('#remno').val();
		var t = $(this).parent().prev().find('input').attr('value');
	    var gd = $(this).attr('mydelnum');
		var mtr = '';
	    if(gd){mtr = gd.substr(gd.length - 1);}
		var amount1 = $('#amount'+mtr).val();    	

		var value=$(this).closest('tr').children('td:first').data("id");
		$('#expense_table tr:last-child td:first-child input').attr("name");		 
	  	if(upno != '1'){
			var myno = $(this).attr('mydelnum');
			$(this).parent().parent().remove();
			var remainingno = parseInt(upno) - 1;
			var upno = $('#up').val(remainingno);	
	  

	    var rem_amount = $(this).data("amount");
	    var final_total = $('#total').val();
	    if(!isNaN(amount1) && amount1.length != 0) { 
				var del_amount = parseFloat(final_total) - parseFloat(amount1);
				$('#total').val((del_amount).toFixed(2));
	    }
	 }
	});

/*Start Fetch client name with autosuggestion*/
            $(".bill_to").select2({
                width: "on",
                //dropdownCssClass : 'bigdrop',
                width: '100%',
              //  dropdownAutoWidth : true,
                ajax: {
                    url: "<?php echo WEBSITE_PATH;?>proformainvoices/getclients/",
                    dataType: 'json',
                    delay: 10,
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
            });
            /*End Fetch client name with autosuggestion*/
            /*Fetch address based on selected client*/
            $(document.body).on("change",".bill_to",function(){
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getclientdetail/',
                    data: 'id='+(this).value,
                    type: 'POST',
                    cache: false,
                    success: function (address1) {
                       // var datares = jQuery.parseJSON(data);
                        $('#bill_address').html(address1);
                        /*If checkbox checked than set ship_to and its address*/
                        if($('#sameas').is(':checked')) {
                            $('#ship_to').val($('#bill_to option:selected').html());
                            $('#ship_address').val($('#bill_address').html().replace(/<br ?\/?>/g, ""));
                        }
                    }
                });    
            });

            /*If checkbox checked than set ship_to and its address*/
            $('#sameas').click(function () {
                if($('#sameas').is(':checked')) {
                    $('#ship_to').val($('#bill_to option:selected').html());
                    $('#ship_address').val($('#bill_address').html().replace(/<br ?\/?>/g, ""));
                }
            });
           $('#local').click(function () {
			var x = $("#price").val();
			    if ($(this).is(':checked')) {
			        var y = $(this).val();
			        if (x != '2') {
			            $("#price").val('2');
			            $("#amount_lbl").html('Amount '+'(₹)');
       					$("#prc_lbl").html('Price '+'(₹)');
        				$("#total_lbl").html('Total '+'(₹)');
			        }
			    }
			    if($(this).prop('checked') == false){
			     var y = $(this).val();
			        if (x == '2') {
			            $("#price").val('1');
			            $("#amount_lbl").html('Amount '+'($)');
       					$("#prc_lbl").html('Price '+'($)');
        				$("#total_lbl").html('Total '+'($)');
			           
			        }
			    }	
			});
</script>