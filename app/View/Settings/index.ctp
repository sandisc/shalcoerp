
<h3 class="page-title"> <?php echo $mainTitle;?>
                        <!--<small>form layouts</small>-->
                    </h3>
  <?php echo $this->Session->flash();?>                
<div class="portlet box green"> 
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                                <div class="tools">
                                                    <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                             <?php echo $this->Form->create('Setting',array('type'=>'file'));
                                              echo $this->Form->input('id', array('type'=>'hidden','value'=>$id)); ?> 
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Office Address<span class="required" aria-required="true"> * </span></label>
                                                                    <div class="col-md-9 input-icon right">
                                                                    <?php

                                                                   echo $this->Form->input('office_address', array('type' => 'textarea','class'=>'form-control','label'=>false,'required'=>true,'rows'=>'3','cols'=>'3'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Factory Address<span class="required" aria-required="true" > * </span></label>
                                                                    <div class="col-md-9">
                                                                      <?php

                                                                   echo $this->Form->input('factory_address', array('type' => 'textarea','class'=>'form-control','label'=>false,'required'=>true,'rows'=>'3','cols'=>'3'));
                                                                    ?>
                                                                        <span class="help-block"><br></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">CIN NO<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input("cin_no",array('type'=>'text','label'=>false,'class'=>'form-control','required'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">PAN NO<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-8">
                                                                <?php echo $this->Form->input("pan_no",array('type'=>'text','label'=>false,'class'=>'form-control','required'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">GST NO<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-8">
                                                                <?php echo $this->Form->input('gst_no', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">                                                   
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Vat Tin No<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-8">
                                                                <?php echo $this->Form->input('vat_tin_no', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">CST Tin No<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-8">
                                                                <?php echo $this->Form->input('cst_tin_no', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                                                  <span class="help-block"><br></span>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">ISO Certificate No<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-9">
                                                                <?php echo $this->Form->input('iso_certificate',array('type'=>'text','label'=>false,'class'=>'form-control','required'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">PED Certificate No<span class="required" aria-required="true" > * </span></label>
                                                            <div class="col-md-8">
                                                                <?php echo $this->Form->input('ped_certificate',array('type'=>'text','label'=>false,'class'=>'form-control','required'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-1">Work Phase<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9 input-icon right">
                                                                <?php echo $this->Form->textarea('work_phase',array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','required'=>true,'id'=>'work_phase'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                            <div class="col-md-2">    
                                                                <span class="help-block">(<b>Note :</b> It will be display in delivery chalan.)</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Signature<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9 input-icon right">
                                                            <?php

                                                           echo $this->Form->input('signature', array('type' => 'file','class'=>'form-control','label'=>false));
                                                            ?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                     <img src="<?php echo ROOT_PATH.'uploads/signature/'.$old['Setting']['signature'];?>" width="200">
                                                </div>
                                                <!-- New field add TC Remarks -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">TC remarks<span class="required" aria-required="true"> * </span></label>
                                                            <div class="col-md-9 input-icon right">
                                                                <?php echo $this->Form->textarea('tc_remarks',array('class'=>'form-control','label'=>false,'cols'=>'2','rows'=>'2','required'=>true,'id'=>'tcremarks'));?>
                                                                <span class="help-block"><br></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                        

                                                                <button class="btn green" type="submit">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6"> </div>
                                                </div>
                                            </div>
                                            <?php echo $this->Form->end();?>
                                              
                                                <!-- END FORM-->
                                            </div>
                                        </div>

