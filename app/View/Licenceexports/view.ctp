<div class="pull-left"><h3 class="page-title"> <?php echo $mainTitle;?></h3></div>
<div class="pull-right"></div>
<div class="clearfix"></div>
                  <div class="portlet box green">
                      <div class="portlet-title">
                          <div class="caption">
                              <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                          <div class="tools">
                              <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                          </div>
                      </div>
                      <div class="portlet-body form metroform">
                          <!-- BEGIN FORM-->
                          <form class="form-horizontal" role="form">
                              <div class="form-body"> 
                                  <div class="row">
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">Licence No</label>
                                              <div class="col-md-8">
                                                  <p class="form-control-static"> <?php echo $data['Licence']['advancelicenceno'];?></p>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">Invoice No</label>
                                              <div class="col-md-8">
                                                 <p class="form-control-static"><?php echo $data['Invoice']['invoiceno'];?></p>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">Invoice Date</label>
                                              <div class="col-md-8">
                                                 <p class="form-control-static"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Invoice']['invoicedate']));?></p>
                                              </div>
                                          </div>
                                      </div>                                                                                        
                                </div>
                                <!-- 2ND row -->
                                <div class="row">                                     
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">Party Name</label>
                                              <div class="col-md-8">
                                                   <p class="form-control-static"><?php echo $data['Client']['company_name'];?></p>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">Hs Code</label>
                                              <div class="col-md-8">
                                                <p class="form-control-static"><?php echo $data['Licence']['Hscode']['hscode'];?></p>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <label class="control-label col-md-4 view-label">HScode Value</label>
                                              <div class="col-md-8">
                                                   <p class="form-control-static"><?php echo $data['Licence']['Hscode']['rate'];?></p>
                                              </div>
                                          </div>
                                      </div>                                                                                        
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 view-label"> SB No</label>
                                            <div class="col-md-8">
                                              <p class="form-control-static"><?php echo $data['Invoice']['sbno'];?></p>
                                            </div>
                                        </div>
                                     </div>                                                               
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 view-label">SB Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><?php echo $this->requestAction('App/date_ymd_to_dmy_licence/'.$data['Invoice']['sbdate']);?></p>
                                            </div>
                                        </div>
                                    </div>                                      
                                </div>
                               <!--/row-->
                                <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                                  <thead class="flip-content portlet box green">
                                    <tr>
                                      <th colspan="6" style="color:red;"><b>Export Information <span class="glyphicon glyphicon-export" style="color:black;"></span></b></th>
                                    </tr>
                                    <tr>
                                      <th width="15%">Grade Tag</th>
                                      <th width="15%">Allow Qty</th>
                                      <th width="15%">Export Qty</th>
                                      <th width="15%">Dollar Value</th>
                                      <th width="15%">Balance Qty</th>
                                    </tr>                       
                                  </thead>
                                   <tbody class="custom_export">
                                <?php 
                                   
                                    foreach($data['Licenceexportitem'] as $licence) {?>
                                      <tr>
                                        <td class="numalign"><?php echo $licence['Licenceitem']['Gradetag']['gradetagname'];?></td>
                                        <td class="numalign"><?php echo $licence['Licenceitem']['ex_qty'];?></td>
                                        <td class="numalign"><?php echo $licence['exportqty'];?></td>
                                        <td class="numalign"><?php echo $licence['dollarvalue'];?></td>
                                        <td class="numalign"><?php echo $this->requestAction('Licenceexports/ExportGradetagbalanceQty/'.$data['Licenceexport']['licenceid'].'/'.$licence['Licenceitem']['gradetagid']);?></td>
                                     </tr>
                                    <?php }  ?>
                                  </tbody>
                                </table>
                              </div>                                             
                              <div class="form-actions">
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="row">
                                              <div class="col-md-offset-3 col-md-9">
                                              <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'licenceexports'),array('class' => 'btn default')); ?>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6"> </div>
                                  </div>
                              </div>
                          </form>
                          <!-- END FORM-->
                      </div>
                  </div>