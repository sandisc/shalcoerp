<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  
 
    foreach ($licence as $licences) { 
      
      $records["data"][] = array('',
      
      '<a href="'.WEBSITE_PATH.'licences/view/'.base64_encode($licences['Licenceexport']['licenceid']).'" title="View Licence">'.$licences['Licence']['advancelicenceno'].'</a>',
      
      '<a href="'.WEBSITE_PATH.'invoices/view/'.base64_encode($licences['Licenceexport']['invoiceid']).'" title="View Invoice">'.$licences['Invoice']['invoiceno'].'</a>',
      $this->requestAction('App/date_ymd_to_dmy/'.strtotime($licences['Invoice']['invoicedate'])),

      $licences['Invoice']['sbno'],      
      $this->requestAction('App/date_ymd_to_dmy_licence/'.$licences['Invoice']['sbdate']),      

      $licences['Client']['company_name'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($licences['Licenceexport']['modified'])),
      $licences['User']['first_name']." ".$licences['User']['last_name'],
       '<a href="'.WEBSITE_PATH.'licenceexports/view/'.base64_encode($licences['Licenceexport']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>'
      /*<a href="'.WEBSITE_PATH.'licenceexports/delete/'.base64_encode($licences['Licenceexport']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this record?"><span class="glyphicon glyphicon-trash"></span></a>'*/
      );
    }

  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>