<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
        <?php echo $this->Session->flash();?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                   <i class="icon-settings font-dark"></i>
                   <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
                </div>
                <div class="actions">
                   <div class="btn-group btn-group-devided">
                   </div>
                   <div class="btn-group">
                   </div>
                </div>
            </div>
         
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <span> </span>
                        <!--<button class="btn btn-sm red table-group-action-delete">
                            <i class="fa fa-trash"></i> Delete</button>-->
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%"><!--<input type="checkbox" class="group-checkable"> --></th>
                                <th width="12%"> Licence No </th>
                                <th width="10%"> Invoice No </th>
                                <th width="8%"> Invoice Date </th>                                
                                <th width="8%"> SB No </th>
                                <th width="8%"> SB Date </th>
                                <th width="15%"> Client Name </th>
                                <th width="12%"> Modified Date </th>
                                <th width="10%"> Modified By </th>                               
                                <th width="15%"> Actions </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="advancelicenceno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="invoiceno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="invoicedate"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="sbno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="sbdate"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>
                                
                                <td><div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                        <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->
<!--- Start Approve Proforma Invoice Dialog Modal -->
<div class="modal fade" id="shoppingform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Licenceexport',array('method'=>'POST','class'=>'shalcoform')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Shopping Info</h4>
      </div>
      <div class="modal-body">
        <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'exportid'));
          echo '<div class="clearfix"> <label for="foo">SB NO.<span class="required"> * </span></label>';
          echo $this->Form->input('sbno',array('type'=>'text','label'=>false,'placeholder'=>"SB No","class" => "form-control sbno",'id'=>'sbno','required'));           
          echo '</div><div class="clearfix"><br><label for="Extended Expiry Date">SB Date</label>
          <div class="input-group date date-picker" id="dp3" data-date-format="dd/mm/yyyy">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
          echo $this->Form->input('sbdate',array('type'=>'text','label'=>false,'placeholder'=>"SB date", "class" => "form-control input-group",'id'=>'ex_date','required'));
          echo '</div></div>'
        ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btnExport" class="btn btn-primary">Save</button>
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!--- End Approve Proforma Invoice Dialog Modal -->
<script>

  $(document).ready(function() {
  
  /*Start Click Event of 'Accept' Button for accept Proforma Invoice*/   
    $(document).on('click', '#btn_shopp', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      $('#shoppingform').modal({
          show: true,
      });
        var licenceid = $(this).attr('data-lid');
        $('#exportid').val(licenceid);
        var sbdate = $(this).attr('data-sbdate');
        $('#ex_date').val(sbdate);
        var sbno = $(this).attr('data-sbno');
        $('#sbno').val(sbno);
     $(document).on('click', '#btnExport', function () {
        $("#spinner").show();
        e.preventDefault();
        e.stopImmediatePropagation();      
        var id = $('#exportid').val();
         var sbno = $('#sbno').val();
        var date =  $('#ex_date').val();
        $.ajax({
          url: '<?php echo WEBSITE_PATH;?>licenceexports/shoppinginfo/',
          type: 'POST', 
          data: 'id='+licenceid+'&sb_date='+date+'&sb_no='+sbno,
          cache: false,
          success: function (data) {
            $("#spinner").hide();
            $('.modal').modal('hide');
            if(sbno != 0){
              $('.test'+id).hide();
            }
            $('.test'+id).parent().parent('tr:first').find('td:eq(4)').text(sbno);
            $('.test'+id).parent().parent('tr:first').find('td:eq(5)').text(date);
            
         }
        });         
      });
    
    });
});
</script>