<div class="row header_main">
  <div class="col-lg-12">
    <?php
      if(!empty($id)) { $value = 'Edit'; }
      else { $value = 'Add'; }
    ?>
    <h1 class="page-header">Roles & Permission</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">Roles & Permission</div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home" aria-expanded="true">Group Permission</a></li>
          <li class=""><a data-toggle="tab" href="#profile" aria-expanded="false">User Permission</a></li>
        </ul>
        <div class="tab-content">
          <h5>&nbsp;</h5>
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="tabnav">Add User Permission</span>
                <a href="<?php echo WEBSITE_PATH;?>permissions/userspermission" style="float: right;" class="btn btn-primary">View User's Permission</a>               
              </div>
      <div class="panel-body">
        <?php echo $this->Form->create('Permission',array('method'=>'POST'));
            $options = array();
            $options[''] = 'Select Employee';
            echo '<div class="form-group">';
            foreach ($users as $users) {
              $options[$users['User']['id']] = $users['User']['first_name'].' '.$users['User']['last_name'];
            }
            echo $this->Form->input("employee_id",array('type'=>'select','options'=>$options,'label'=>'Select Employee <span class="requiredLabel">*</span>','div'=>array('class'=>'form-group col-lg-3'),'class'=>'form-control','required'));
            echo '</div>';
            
            echo '<div class="form-group">
                <div class="col-lg-12">
                  <label><p class="text-warning">'.$value.' Permission</p></label>
                </div>';
            echo '<div class="col-lg-12">';
              if(isset($id) && $id != ''){                   
                    $selected= explode(',',$this->data['Permission']['access_modules']);
                }else{
                    $selected='';                
              }
              
              //$options = array();
              $name = '';
              $checked = '';
              foreach ($modules as $moduledata) {            
                //$options[$moduledata['Module']['id']] = $moduledata['Module']['action_name'];              
                if($name != $moduledata['Module']['module_name'])
                {
                  $name = $moduledata['Module']['module_name']; 
                  echo '</div><div class="col-lg-12"><label>'.$name.'</label></div><div class="col-lg-12">';
                } 
                if(!empty($selected)){
                  in_array($moduledata['Module']['id'],$selected)? $checked = 'checked' : $checked = '';
                }
                
                echo '<div class="col-lg-3 checkbox">
                    <label>
                        <input type="checkbox" name="data[Permission][access_modules][]" value="'.$moduledata['Module']['id'].'" '.$checked.'>'.$moduledata['Module']['action_name'].'
                    </label>
                </div>';
                /*echo $this->Form->input('access_modules',array(
                                                      'label'=>$moduledata['Module']['action_name'],
                                                      
                                                      'multiple' => 'checkbox',
                                                      'type'=>'checkbox',
                                                      //'options'=>$options,
                                                      'value'=>$moduledata['Module']['id'],
                                                      'selected'=>  $selected,
                                                      'class' => '','div'=>array('class'=>'col-lg-3')));*/             
              } 
        echo '</div>
        </div>';

        echo '<div class="col-lg-12" style="padding:0;">';
        echo $this->Form->button('Save', array('type' => 'submit','class' => 'btn btn-primary mrgright'));
        echo $this->Form->button('Cancel', array('type' => 'button','class' => 'btn btn-default','onclick' => "location.href='".WEBSITE_PATH."permissions/userspermission'")); 
        echo '</div>';

        echo $this->Form->end();?>
      </div>
    </div>
  </div>
      </div>
    </div>
  </div>
</div>        