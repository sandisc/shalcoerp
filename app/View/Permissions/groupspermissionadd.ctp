<div class="row header_main">
  <div class="col-lg-12">
    <?php if(!empty($id)) { $value = 'Edit'; } else { $value = 'Add'; } ?>
    <h1 class="page-header">Permission Management</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <span class="tabnav"><?php echo $value; ?> User type Permission</span>
        <a href="<?php echo WEBSITE_PATH;?>permissions/groupspermission" style="float: right;" class="btn btn-primary">View User Type's permission</a>
      </div>
      <div class="panel-body">
        <?php echo $this->Form->create('Grouppermission',array('method'=>'POST'));?>
        <div class="col-lg-12">
          <div class="form-group">
            <?php
              $Gouproptions = array();
              $Gouproptions[''] = 'Select Usertype';
              foreach ($groups as $groups) {
                $Gouproptions[$groups['Usertype']['id']] = $groups['Usertype']['usertype_name'];
              }
              echo $this->Form->input("usertype_id",array('type'=>'select','options'=>$Gouproptions,'label'=>'Select Usertype <span class="requiredLabel">*</span>','div'=>array('class'=>'form-group col-lg-3 padding0'),'class'=>'form-control','required'));
            ?>
          </div>
          <div class="col-lg-12 padding0"><label><p class="text-warning">Check Permission</p></label></div>
          <div class="col-lg-12 padding0">
            <?php 
              if(isset($id) && $id != '') {
                $selected = explode(',',$this->data['Grouppermission']['access_modules']);
              }
              else {
                $selected = '';
              }

              $name = '';
              $checked = '';
              if(empty($this->params->pass) && empty($this->data)) { $checked = 'checked'; }

              $dataArray = array();
              $i = '0';
              foreach ($modules as $moduledata) {
                if($name != $moduledata['Module']['module_name']) {
                  $name = $moduledata['Module']['module_name'];
                  $i = '0';
                }

                if(!empty($selected)){
                  in_array($moduledata['Module']['id'],$selected)? $checked = 'checked' : $checked = '';
                }
                $dataArray[$name][$i]['checked'] = $checked;
                $dataArray[$name][$i]['modulename'] = $moduledata['Module']['id'];
                $dataArray[$name][$i]['actionname'] = $moduledata['Module']['action_name'];
                $i++;
              }
            ?>
            <?php
              // asort($dataArray);
              $i = '1';
              foreach ($dataArray as $key => $value) {
                /*if($key != 'Course' && $key != 'Benefit' && $key != 'Performance Evaluation') {*/
                  if($i % 2 == 0 ) {
                    $class = 'paddingright0';
                  }
                  else {
                    $class = 'paddingleft0';
                  }

                  echo '<div class="col-lg-3 paddingleft0"><div class="panel panel-default">';
                  echo '<div class="panel-heading">'.$key.'</div><div class="panel-body">';
                  foreach ($value as $keys => $values) {
                    echo '<div class="checkbox"><label><input type="checkbox" name="data[Grouppermission][access_modules][]" value="'.$values['modulename'].'" '.$values['checked'].'>'.$values['actionname'].'</label></div>';
                  }  
                  echo '</div></div></div>';
                  $i++;
                /*}*/
              }
            ?>
            <div class="col-lg-12 padding0" style="margin-bottom:10px;"><?php if(isset($ErrorMsg)) { echo $ErrorMsg;}?></div>
          </div>
          
          <div class="col-lg-12" style="padding:0;">
          <?php
            echo $this->Form->button('Save', array('type' => 'submit','class' => 'btn btn-primary mrgright'));
            echo $this->Form->button('Cancel', array('type' => 'button','class' => 'btn btn-default','onclick' => "location.href='".WEBSITE_PATH."permissions/groupspermission'")); 
          ?>
          </div>
        </div>
        <?php echo $this->Form->end();?>
      </div>
    </div>
  </div>
</div>