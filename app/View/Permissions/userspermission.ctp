<div class="row header_main">
  <div class="col-lg-12">   
    <h1 class="page-header">Roles & Permission</h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">Roles & Permission</div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/groupspermission" aria-expanded="true">User Type Permission</a></li>
          <li class=""><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/userspermission" aria-expanded="false">User Permission</a></li>
        </ul>
        <div class="tab-content">
          <h5>&nbsp;</h5>
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="tabnav">View User's Permission</span>
                <a href="<?php echo WEBSITE_PATH;?>permissions/userspermissionadd" style="float: right;"class="btn btn-primary">Add User Permission</a>               
              </div>
              <div class="panel-body">
                <div class="dataTable_wrapper">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <tr>
                        <th>#</th>                               
                        <th>Name</th>
                        <th>Usertype</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        foreach($permissions as $key=>$datares){
                     //   $groupname = $this->requestAction('App/getGroupName/'.$datares['User']['id']);
                        echo '<tr>
                          <td>'.++$key.'</td>                                   
                          <td>'.$datares['User']['first_name'].' '.$datares['User']['last_name'].'</td>
                          <td>'.$groupname['0']['title'].'</td>
                          <td>'.$datares['User']['email'].'</td>
                          <td>'.$this->Html->link('Edit', array('controller' => 'permissions', 'action' => 'userspermissionadd', base64_encode($datares['Userpermission']['id'])), array('class'=>'btn btn-info')).' '.$this->Html->link('View', array('controller' => 'permissions', 'action' => 'userspermissionview', base64_encode($datares['Userpermission']['id'])), array('class'=>'btn btn-primary')).'</td>
                          </tr>';
                      } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>