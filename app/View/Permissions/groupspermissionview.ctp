<div class="row header_main">
  <div class="col-lg-12">
    <h1 class="page-header">Roles & Permission</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">Roles & Permission</div>
      <div class="panel-body">
        <!-- <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/groupspermission" aria-expanded="true">Group Permission</a></li>
          <li class=""><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/userspermission" aria-expanded="false">User Permission</a></li>
        </ul>
        <div class="tab-content">
          <h5>&nbsp;</h5> -->
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">               
                <span class="tabnav">View <b><?php echo $groupname; ?></b> Permission</span>
                <a href="<?php echo WEBSITE_PATH;?>permissions/groupspermission" style="float: right;" class="btn btn-primary">Add Usertype Permission</a>
              </div>
              <div class="panel-body">

        <?php            
            echo '<div class="form-group">  
                <div class="col-lg-12">';
              if(isset($id) && $id != ''){                   
                    $selected= explode(',',$this->data['Grouppermission']['access_modules']);
                }else{
                    $selected='';                
              }
              
              $name = '';
              $data = '';
              foreach ($modules as $moduledata) {            
                        
                if($name != $moduledata['Module']['module_name'])
                {
                  $name = $moduledata['Module']['module_name']; 
                  $data.='</div><div class="col-lg-12"><label><b>'.$name.'</b></label></div><div class="col-lg-12">';
                } 
                if(!empty($selected)){
                  if(in_array($moduledata['Module']['id'],$selected)){
                    $data.='<div class="col-lg-3 checkbox">
                      <label class="actioname"><i class="fa fa-check-square-o"></i> <span>'.$moduledata['Module']['action_name'].'</span></label>
                    </div>';
                  }
                  else{
                    $data.='<div class="col-lg-3 checkbox">
                      <label class="actioname"><i class="fa fa-square-o"></i> <span>'.$moduledata['Module']['action_name'].'</span></label>
                    </div>';
                  }
                }                                        
              } 
              echo $data;
        echo '</div>
        </div>';

        echo '<div class="col-lg-12" style="padding:0;">';       
        echo $this->Form->button('Back', array('type' => 'button','class' => 'btn btn-default','onclick' => "location.href='".WEBSITE_PATH."permissions/groupspermission'")); 
        echo '</div>';

        ?>
            </div>
          </div>
        </div>
      <!-- </div> -->
    </div>
  </div>
</div>        