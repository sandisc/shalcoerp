<div class="row header_main">
  <div class="col-lg-12">
    <?php
      if(!empty($id)) { $value = 'Edit'; }
      else { $value = 'Add'; }
    ?>
    <h1 class="page-header">Roles & Permission</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <?php echo $value;?> Roles & Permission
      </div>
      <div class="panel-body">
        <?php echo $this->Form->create('Permission',array('method'=>'POST'));

            $options = array();
            $options[''] = 'Select Employee';
            foreach ($users as $users) {
              $options[$users['User']['id']] = $users['User']['first_name'].' '.$users['User']['last_name'];
            }
            echo $this->Form->input("employee_id",array('type'=>'select','options'=>$options,'label'=>'Select Employee <span class="requiredLabel">*</span>','div'=>array('class'=>'form-group col-lg-3'),'class'=>'form-control','required'));

            echo '<div class="col-lg-12">';

          if(isset($id) && $id != ''){   
            if(is_array($this->data['Permission']['access_modules'])){
                $selected=$this->data['Permission']['access_modules'];
            }else{
                $selected=explode(',',$this->data['Permission']['access_modules']);
            }
          }else{
              $selected='';
          } 

            $options = array();

            $name = '';
            foreach ($modules as $moduledata) {
              $options[$moduledata['Module']['id']] = $moduledata['Module']['action_name'];              
              if($name != $moduledata['Module']['module_name'])
              {
                $name = $moduledata['Module']['module_name']; 
                echo '</div><div class="col-lg-12">'.$name;
              }
              
              echo $this->Form->input('access_modules',array(
                                                    //'label'=>$moduledata['Module']['action_name'],
                                                    'label'=>'Check Permission',
                                                    'multiple' => 'checkbox',
                                                    //'type'=>'checkbox',
                                                    'options'=>$options,
                                                    //'value'=>$moduledata['Module']['id'],
                                                    'selected'=>  $selected,
                                                    'class' => '','div'=>array('class'=>'col-lg-3')));
            } 
              
        


        
        echo '<div class="col-lg-8" style="padding:0;">';
        echo $this->Form->button('Save', array('type' => 'submit','class' => 'btn btn-primary mrgright'));
        echo $this->Form->button('Cancel', array('type' => 'button','class' => 'btn btn-default','onclick' => "location.href='".WEBSITE_PATH."permissions'")); 
        echo '</div>';

        echo $this->Form->end();?>
      </div>
    </div>
  </div>
</div>        