<div class="row header_main">
  <div class="col-lg-12">   
    <h1 class="page-header">Permission Management</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-12"><?php echo $this->Session->flash();?>
    <div class="panel panel-default">
      <div class="panel-heading">
         Permission List
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
          <li id="group"><a data-toggle="tab" href="#groupspermission">Usertype Permission</a></li>
          <li id="users"><a data-toggle="tab" href="#userspermission">User Permission</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div id="groupspermission" class="tab-pane fade group">
            <div class="col-lg-12 padding0">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <!-- Group Permission -->
                  &nbsp;
                  <a href="<?php echo WEBSITE_PATH;?>permissions/groupspermissionadd" style="float: right;" class="btn btn-primary">Add Usertype Permission</a>
                </div>
                <div class="panel-body">
                  <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>User Type</th>
                          <th>Modified</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach($permissions as $key=>$datares) {
                          echo '<tr>
                            <td>'.++$key.'</td>
                            <td>'.$datares['Usertype']['usertype_name'].'</td>
                            <td>'.$datares['Grouppermission']['modified'].'</td>
                            <td>'.$this->Html->link('Edit', array('controller' => 'permissions', 'action' => 'groupspermissionadd', base64_encode($datares['Grouppermission']['id'])), array('class'=>'btn btn-info')).' '.$this->Html->link('View', array('controller' => 'permissions', 'action' => 'groupspermissionview', base64_encode($datares['Grouppermission']['id'])), array('class'=>'btn btn-primary')).'</td>
                            </tr>';
                        } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="userspermission" class="tab-pane fade users">
            <div class="col-lg-12 padding0">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <!-- User Permission -->
                  &nbsp;
                  <a href="<?php echo WEBSITE_PATH;?>permissions/userspermissionadd" style="float: right;"class="btn btn-primary">Add User permission</a>
                </div>
                <div class="columnFilter col-lg-12">
                  <div class="col-lg-3" id="Group"></div>
                </div>
                <div class="panel-body">
                  <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example10">
                      <thead>
                        <!-- <tr>
                          <th></th>
                          <th></th>
                          <th>Group</th>
                          <th></th>
                          <th></th>
                        </tr> -->
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>User Type</th>
                          <th>Email</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          foreach($userPermissions as $key=>$datares){
                          $rolename = $this->requestAction('App/getRoleName/'.$datares['User']['usertype_id']);

                          echo '<tr>
                            <td>'.++$key.'</td>
                            <td>'.$datares['User']['first_name'].' '.$datares['User']['last_name'].'</td>
                            <td>'.$rolename.'</td>
                            <td>'.$datares['User']['email'].'</td>
                            <td>'.$this->Html->link('Edit', array('controller' => 'permissions', 'action' => 'userspermissionadd', base64_encode($datares['Userpermission']['id'])), array('class'=>'btn btn-info')).' '.$this->Html->link('View', array('controller' => 'permissions', 'action' => 'userspermissionview', base64_encode($datares['Userpermission']['id'])), array('class'=>'btn btn-primary')).'</td>
                            </tr>';
                        } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#dataTables-example10').dataTable();

/*dataTable.columnFilter({
    sPlaceHolder: "head:after",
    aoColumns: [
      null,
      null,
      { sSelector: "#Group", type: "select" },
      null,
      null
    ]
  });*/
  $('#Group select option[class="search_init"]').html('Select Group');
});

 /* Get Coockie and active cookie tab */
  //var tabVal = readCookie('perValue');
  //console.log(tabVal);
    $("#group").trigger('click').attr('class','active');
    $(".group").addClass('active in');
  
  /* Set cookie id in tab */
  $('.nav li').click(function () {
    var val = this.id;
    createCookie('perValue',val);
  });
</script>

<!-- <div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">Roles & Permission</div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/groupspermission;?>" aria-expanded="true">Group Permission</a></li>
          <li class=""><a data-toggle="tab" href="<?php echo WEBSITE_PATH;?>permissions/userspermission;?>" aria-expanded="false">User Permission</a></li>
        </ul>
        <div class="tab-content">
          <h5>&nbsp;</h5>
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="tabnav">View Group's Permission</span>
                <a href="<?php echo WEBSITE_PATH;?>permissions/groupspermissionadd" style="float: right;" class="btn btn-primary">Add Group Permission</a>
              </div>
              <div class="panel-body">
                <div class="dataTable_wrapper">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Group</th>
                        <th>Permission</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        foreach($permissions as $key=>$datares) {
                        echo '<tr>
                          <td>'.++$key.'</td>
                          <td>'.$datares['Group']['title'].'</td>
                          <td>'.$this->Html->link('Edit', array('controller' => 'permissions', 'action' => 'groupspermissionadd', base64_encode($datares['Grouppermission']['id'])), array('class'=>'btn btn-info')).' '.$this->Html->link('View', array('controller' => 'permissions', 'action' => 'groupspermissionview', base64_encode($datares['Grouppermission']['id'])), array('class'=>'btn btn-primary')).'</td>
                          </tr>';
                      } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
