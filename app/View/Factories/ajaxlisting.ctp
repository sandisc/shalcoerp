<?php
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($factories as $factory) {
      $record_nextstage = $this->requestAction(
        array('controller' => 'App', 'action' => 'check_nextstage'),
        array('dependantmodel' => 'Client','conditional_parameter' => 'FIND_IN_SET(\''. $factory['Factory']['id'] .'\',Client.industryid)')
      );
      if(empty($record_nextstage)){
        $delete_record = '<a href="'.WEBSITE_PATH.'factories/delete/'.base64_encode($factory['Factory']['id']).'" class="btn btn-danger" title="Delete Industry" id="delete" data-confirm="Are you sure to delete this factory?"><span class="glyphicon glyphicon-trash"></span></a>';
        $checkbox_delete = '<input type="checkbox" name="id[]" value="'.$factory['Factory']['id'].'">';
      }else{
        $delete_record = '';
        $checkbox_delete = '';
      }      
      $records["data"][] = array($checkbox_delete,
      $factory['Factory']['factory_address'],
      $factory['Factory']['factory_alias'],
      $factory['Factory']['division_name'],
      $factory['Factory']['division_alias'],
      $factory['Factory']['division_range'],
      $factory['Factory']['excise_regn_no'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($factory['Factory']['modified'])),
      $factory['User']['first_name']." ".$factory['User']['last_name'],
      '<a href="'.WEBSITE_PATH.'factories/add/'.base64_encode($factory['Factory']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>'.$delete_record);
    }
 
  if(isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
   // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>