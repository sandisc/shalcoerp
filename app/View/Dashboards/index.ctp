<link href="<?php echo ROOT_PATH;?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo ROOT_PATH;?>global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_PATH;?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/dashboard.js" type="text/javascript"></script>
<style>
    .page-bar-dashboard .page-breadcrumb {
    display: inline-block!important;
    float: left!important;
    padding: 8px!important;
    margin: 0!important;
    list-style: none!important;}
    .page-bar-dashboard{
        margin-left: 10px!important;
        margin-right: 10px!important;
        padding: 0px !important;
        background-color: #f1f4f7!important;
        margin-bottom: 25px!important;
    }
    .page-toolbar{
        padding: 0px !important;
    }
    .dashboard-stat .visual > i {
        margin-left: 0px;
        font-size: 70px;
        line-height: 90px;
    }
</style>
<h3 class="page-title">&nbsp;</h3>

                    <!-- BEGIN GENERAL STATISTICS -->
                    <div class="row widget-row">
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar page-bar-dashboard">
                            <ul class="page-breadcrumb">
                                <li><span class=""><b>General Stats</b></span></li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="generalstat-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR --> 
                        <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 proformacount">
                                <div class="dashboard-stat blue">
                                    <div class="visual">
                                        <i class="icon-notebook"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span class="stat" data-counter="counterup" data-value=""></span>
                                        </div>
                                        <div class="desc"> Proforma Invoice </div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 ordercount">
                                <div class="dashboard-stat red">
                                    <div class="visual">
                                        <i class="icon-basket"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span class="stat" data-counter="counterup" data-value=""></span></div>
                                        <div class="desc"> Order </div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 chalancount">
                                <div class="dashboard-stat green">
                                    <div class="visual">
                                        <i class="fa icon-symbol-female"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span class="stat" data-counter="counterup" data-value=""></span>
                                        </div>
                                        <div class="desc"> Chalan</div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 invoicecount">
                                <div class="dashboard-stat purple">
                                    <div class="visual">
                                        <i class="fa icon-symbol-male"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> 
                                            <span class="stat" data-counter="counterup" data-value=""></span></div>
                                        <div class="desc"> Invoice </div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 certicount">
                                <div class="dashboard-stat yellow">
                                    <div class="visual">
                                        <i class="fa icon-graduation"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span class="stat" data-counter="counterup" data-value=""></span>
                                        </div>
                                        <div class="desc"> Certificate</div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 dummycerticount">
                                <div class="dashboard-stat grey">
                                    <div class="visual">
                                        <i class="fa icon-graduation"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> 
                                            <span class="stat" data-counter="counterup" data-value=""></span></div>
                                        <div class="desc"> Dummy Certificate </div>
                                    </div>
                                    <a class="more" href="javascript:;"><!--  View more<i class="m-icon-swapright m-icon-white"></i> --></a>
                                </div>
                            </div>                        
                        </div>
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->  
                    </div>
                    <!-- END GENERAL STATISTICS -->
                    <div class="clearfix">&nbsp;</div>
                    
                    <!-- BEGIN SALES STATISTICS -->
                    <div class="row widget-row">
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar page-bar-dashboard">
                            <ul class="page-breadcrumb">
                                <li><span class=""><b>Sales Stats</b></span></li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="salesstat-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN sales STATS 1-->
                        <div class="table-scrollable table-scrollable-borderless" id="sales_stats">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Sales type </th>
                                        <th> Quantity </th>
                                        <th> Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="uppercase">
                                        <td align="center">Domestic</td>
                                        <td align="center" class="domestic_qty"> - </td>
                                        <td align="center" class="domestic_amount"> - </td>
                                    </tr>
                                    <tr class="uppercase">
                                        <td align="center"> International</td>
                                        <td align="center" class="internation_qty"> - </td>
                                        <td align="center" class="internation_amount"> - </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- END sales STATS 1-->  
                    </div>
                    <!-- END SALES STATISTICS -->
                    <div class="clearfix">&nbsp;</div>

                    <!-- BEGIN User Sales STATISTICS -->
                    <div class="row widget-row">
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar page-bar-dashboard">
                            <ul class="page-breadcrumb">
                                <li><span class=""><b>User Sales Stats</b></span></li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="usersalesstat-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN user sales STATS 1-->
                        <div class="table-scrollable" id="userssales_stats">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Admin Name </th>
                                        <th> Domestic Qty </th>
                                        <th> Domestic Amount </th>
                                        <th> International Qty </th>
                                        <th> International Amount </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>    
                        </div>
                        <div class="clearfix"></div>
                        <!-- END user sales STATS 1-->  
                    </div>
                    <!-- END User Sales STATISTICS -->
                    <div class="clearfix">&nbsp;</div>

                    <!-- BEGIN Size Sales STATISTICS -->
                    <div class="row widget-row">
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar page-bar-dashboard">
                            <ul class="page-breadcrumb">
                                <li><span class=""><b>Size Sales Stats</b></span></li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="sizesalesstat-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN Size STATS 1-->
                        <div class="row col-md-12">
                            <div class="col-md-6">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase">Domestic Stat of Size</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable" id="sizesales_stat">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Size </th>
                                                        <th> Qty MTR </th>
                                                        <th> Qty MT </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="size_domestic">
                                                    <tr class="active">
                                                        <td> - </td>
                                                        <td> - </td>
                                                        <td> - </td>
                                                    </tr>
                                                </tbody>
                                            </table>                                                     
                                        </div>
                                    </div>     
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase">International Stat of Size</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">                            
                                        <div class="table-scrollable" id="sizesales_stat">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Size </th>
                                                        <th> Qty MTR </th>
                                                        <th> Qty MT </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="size_invoice">
                                                    <tr class="active">
                                                        <td> - </td>
                                                        <td> - </td>
                                                        <td> - </td>
                                                    </tr>
                                                </tbody>
                                            </table>                                                     
                                        </div>
                                    </div>
                                </div>                                        
                            </div>                            
                        </div>
                        <!-- END Size STATS 1--> 
                        <div class="clearfix"></div>
                        <!-- END Size STATS 1-->  
                    </div>
                    <!-- END User Sales STATISTICS -->
<script type="text/javascript">
    function generalstat(fromdate,todate){ 
        $.ajax({
            url: '<?php echo WEBSITE_PATH;?>dashboards/generalstatistics/',
            type: 'POST',
            data: 'fromdate='+fromdate+'&todate='+todate,
            success: function(data) {
                var jsonresp = $.parseJSON(data);
                
                $('.proformacount .stat').html(jsonresp.proforma_count);
                $('.proformacount .stat').attr('data-value',jsonresp.proforma_count);
                $('.ordercount .stat').html(jsonresp.order_count);
                $('.ordercount .stat').attr('data-value',jsonresp.order_count);
                $('.chalancount .stat').html(jsonresp.chalan_count);
                $('.chalancount .stat').attr('data-value',jsonresp.chalan_count);
                $('.invoicecount .stat').html(jsonresp.invoice_count);
                $('.invoicecount .stat').attr('data-value',jsonresp.invoice_count);
                $('.certicount .stat').html(jsonresp.certi_count);
                $('.certicount .stat').attr('data-value',jsonresp.certi_count);
                $('.dummycerticount .stat').html(jsonresp.dummycerti_count);
                $('.dummycerticount .stat').attr('data-value',jsonresp.dummycerti_count);                                                                               
            },
            error: function(e) {}
        });
    }
    function salesstat(fromdate,todate){ 
        $.ajax({
            url: '<?php echo WEBSITE_PATH;?>dashboards/salesstatistics/',
            type: 'POST',
            data: 'fromdate='+fromdate+'&todate='+todate,
            success: function(data) {
                var jsonresp = $.parseJSON(data);
                $('.domestic_qty').html(jsonresp.total_chalan_qty);
                $('.internation_qty').html(jsonresp.total_invoice_qty);
                $('.domestic_amount').html(jsonresp.total_chalan_amount);
                $('.internation_amount').html(jsonresp.total_invoice_amount);
                /*Do you json here*/                                                                               
            },
            error: function(e) {}
        });
    }
    function usersalesstat(fromdate,todate){ 
        $.ajax({
            url: '<?php echo WEBSITE_PATH;?>dashboards/usersalesstatistics/',
            type: 'POST',
            data: 'fromdate='+fromdate+'&todate='+todate,
            success: function(data) {
                var jsonresp = $.parseJSON(data);
                var html = '';
                $.each(jsonresp, function(i, item) {
                    html += '<tr class="">';
                        html += '<td align="center">'+jsonresp[i].Name+'</td>';
                        html += '<td align="center">'+jsonresp[i].Total_qty+'</td>';
                        html += '<td align="center">'+jsonresp[i].Total_amount+'</td>';
                        html += '<td align="center">'+jsonresp[i].Total_qty_inter+'</td>';
                        html += '<td align="center">'+jsonresp[i].Total_amount_inter+'</td>';
                    html += '</tr>';
                });
                
                $('#userssales_stats tbody').html(html);
                /*Do you json here*/                                                                                
            },
            error: function(e) {}
        });
    }
    function sizesalesstat(fromdate,todate){ 
        $.ajax({
            url: '<?php echo WEBSITE_PATH;?>dashboards/sizesalesstatistics/',
            type: 'POST',
            data: 'fromdate='+fromdate+'&todate='+todate,
            success: function(data) {
                var jsonresp = $.parseJSON(data);
                var size_domestic = '';
                var jch = jsonresp.chalan;
                $.each(jch, function(i, item) { 
                    size_domestic += '<tr class="">';
                        size_domestic += '<td class="numalign">'+jch[i].gdmm+'</td>';
                        size_domestic += '<td class="numalign">'+jch[i].qty_mtr+'</td>';
                        size_domestic += '<td class="numalign">'+jch[i].qty_mt+'</td>';
                    size_domestic += '</tr>';
                });

                var size_invoice = '';
                var jin = jsonresp.invoice;
                $.each(jin, function(i, item) { 
                    size_invoice += '<tr class="">';
                        size_invoice += '<td class="numalign">'+jin[i].gdmm+'</td>';
                        size_invoice += '<td class="numalign">'+jin[i].qty_mtr_inter+'</td>';
                        size_invoice += '<td class="numalign">'+jin[i].qty_mt_inter+'</td>';
                    size_invoice += '</tr>';
                });                
                
                $('#sizesales_stat .size_invoice').html(size_invoice);
                $('#sizesales_stat .size_domestic').html(size_domestic);
            },
            error: function(e) {}
        });
    }              
</script>                   