<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->

<!-- <a id="printrow" data-controllername="invoices" data-actionname="printinvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']).'?p=2'; ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Custom Invoice </a>

<a id="printrow" data-controllername="invoices" data-actionname="printinvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']).'?p=1'; ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline  prints" title="Print Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Party Invoice </a>

<a id="printrow" data-controllername="invoices" data-actionname="printinvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']).'?p=3'; ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline  prints" title="Print Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Custom Packing </a>

<a id="printrow" data-controllername="invoices" data-actionname="printinvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']).'?p=0'; ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Party Packing </a> -->

<!--<a id="printrow" data-controllername="invoices" data-actionname="custominvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Print Invoice </a>

<a id="printrow" data-controllername="invoices" data-actionname="partyinvoice" data-id ="<?php echo base64_encode($data['Invoice']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print party invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Party Invoice </a>

<a id="printrow" data-controllername="invoices" data-actionname="custompacking" data-id ="<?php echo base64_encode($data['Invoice']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print party invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Packing</a>

<a id="printrow" data-controllername="invoices" data-actionname="partypacking" data-id ="<?php echo base64_encode($data['Invoice']['id']) ?>" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline prints" title="Print party invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span> Party Packing </a>-->

      <?php echo $report_button;

        if(in_array('invoices/edit',$this->Session->read('accesscontrollers_actions'))){
         echo $this->Html->link(
           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
            array(
                'controller'=>'invoices',
                'action'=>'edit',
                base64_encode($data['Invoice']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Edit',
                'title' => 'Edit',
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************//
            )
          );    
        }

        /*echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Party Invoice',
            array(
                '?'  => array('p' => '1','type'=>2),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Invoice PDF',
                'download'=>'Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );

        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Party Packing',
            array(
              '?'  => array('p' => '0','type'=>2),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Packing PDF',
                'download'=>'Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );

        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Custom Invoice',
            array(
              '?'  => array('p' => '2','type'=>2),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Invoice PDF',
                'download'=>'Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );
        
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Custom Packing',
            array(
              '?'  => array('p' => '3','type'=>2),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Packing PDF',
                'download'=>'Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );*/
        /*================== PDF  Download Link  ============================ */  
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Party Invoice',
            array(
                'controller'=>'invoices',
                'action'=>'generatepdf',
                'ext' => 'pdf',                
                1,$data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Invoice PDF',
                'download'=>'Party Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );

        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Party Packing',
            array(              
                'controller'=>'invoices',
                'action'=>'generatepdf',
                'ext' => 'pdf',                
                0,$data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Packing PDF',
                'download'=>'Party Packing-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );

        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Custom Invoice',
            array(
                'controller'=>'invoices',
                'action'=>'generatepdf',
                'ext' => 'pdf',                
                2,$data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Invoice PDF',
                'download'=>'Custom Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );
        
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Custom Packing',
            array(
               'controller'=>'invoices',
                'action'=>'generatepdf',
                'ext' => 'pdf',                
                3,$data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Packing PDF',
                'download'=>'Custom Packing-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE 
            )
        );  

        /*================== Excel Export Download Link  ============================ */      
            echo $this->Html->link(
           '<i class="fa fa-file-excel-o"></i> Party Invoice',
            array(
                '?'  => array('p' => '1','type'=>1),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Invoice Excel',
                'download'=>'Party Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );

            echo $this->Html->link(
           '<i class="fa fa-file-excel-o"></i> Party Packing',
            array(
              '?'  => array('p' => '0','type'=>1),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Party Packing Excel',
                'download'=>'Party Packing-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );

            echo $this->Html->link(
           '<i class="fa fa-file-excel-o"></i> Custom Invoice',
            array(
              '?'  => array('p' => '2','type'=>1),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Invoice Excel',
                'download'=>'Custom Invoice-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
              echo $this->Html->link(
           '<i class="fa fa-file-excel-o"></i> Custom Packing',
            array(
              '?'  => array('p' => '3','type'=>1),
                'controller'=>'invoices',
                'action'=>'export',
                $data['Invoice']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Download',
                'title' => 'Custom Packing Excel',
                'download'=>'Custom Packing-'.$data['Invoice']['invoiceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
       ?>
<?php  $settings = $this->Session->read('setting_data');/*Read setting table's data*/?>             
<div class="portlet box green" id="invoice_portlet">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
        <div class="form-body"> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                 <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"><br></span>
                  </div>
                     <div class="col-lg-6">
                        <label class="control-label">Factory Address</label>
                      <div class="input textarea"><?php echo nl2br($settings['Setting']['factory_address']); ?></div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea"><?php echo nl2br($settings['Setting']['office_address']); ?></div>
                      <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Invoice NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['invoiceno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Invoice Date</label>
                      <div class="input-group">
                      <div class="input text"><?php echo date('d/m/Y',strtotime($data['Invoice']['invoicedate']))?></div>
                      </div>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Client P.O. NO.</label>
                        <div class="input text"><?php echo $data['Order']['clientpo'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-3">
                      <label class="control-label">Client P.O. Date</label>
                      <div class="input-group">
                        <div class="input text"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate']));?></div>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <label class="control-label">Order No</label>
                      <div class="input-group">
                        <div class="input text"><?php echo $data['Order']['orderno'];?></div>
                      </div>
                    </div>                    
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Country Of Origin Goods</label>
                      <div class="input text"><?php echo $data['Invoice']['fromcountry']?></div><span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Country Of Final Destination</label>
                       <div class="input text"><?php echo $data['Invoice']['tocountry']?></div>
                       <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
              <div class="row separatordiv">
            <div class="col-md-6">
              <div class="col-lg-6">
                <label class="control-label">Consignee </label>
                <div class="input textarea"><b><?php echo $data['Order']['Client']['company_name'] .'</b></br>'.searchFromAddress($data['Order']['billaddress_id'], $data['Order']['Client']['Address']); ?></div>
                  <span class="help-block"><br></span>
              </div>
              <div class="col-lg-6"></div>
            </div><!--/col-md-6-->
            <div class="col-md-6">
              <div class="col-lg-12">
                <label class="control-label">Buyer's (If Other Than Consignee)</label>
                  <div class="input text"><?php echo nl2br($data['Invoice']['buyers']); ?></div>
                  <span class="help-block"><br></span>
              </div>
              <div class="col-lg-12">
                <label class="control-label">IEC CODE NO.</label>
                  <div class="input text"><?php echo $data['Order']['Bank']['iec_code']; ?></div>
                <span class="help-block"><br></span>
              </div>
            </div><!--/col-md-6-->
          </div>
          <!--/row-->

          <div class="row separatordiv">
            <div class="col-md-6">
              <div class="col-lg-6">
                <label class="control-label">PRE CARRIAGE BY</label>
                <div class="input text"><?php echo $data['Invoice']['carriageby']?></div>
                  <span class="help-block"><br></span>
              </div>
              <div class="col-lg-6">
                <label class="control-label"> Place of Receipt By Pre-Carrier</label>
                <div class="input text"><?php echo $data['Invoice']['receiptplace']?></div>
              </div>
            </div><!--/col-md-6-->

            <div class="col-md-6">
               <div class="col-lg-12">
                  <label class="control-label">Other Reference</label>
                  <div class="input text"><?php echo $data['Invoice']['otherref']?></div>
                  <span class="help-block"><br></span>
                </div>
            </div><!--/col-md-6-->
          </div>
        
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Vessel/Flight NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['flightno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Port of Loading</label>
                        <div class="input text"><?php echo $data['Invoice']['loadingport']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Delivery</label>
                        <div class="input text"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).' '.$data['Order']['tod'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Port of Discharge</label>
                        <div class="input text"><?php echo $data['Invoice']['dischargeport']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Place of Delivery</label>
                        <div class="input text"><?php echo $data['Invoice']['deliveryplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Payment</label>
                        <div class="input text"><?php echo $data['Order']['top'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
               <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Container NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['containerno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                   
                </div>
                
                <div class="col-md-6">
                     
            </div>
        </div>  <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
              <thead class="flip-content portlet box green">
                  <tr>
                      <th width="10%">Marks & Bundle</th>
                      <th width="6%">Party <br>Invoice No</th>
                      <th width="6%">Licence <br>No</th>
                      <th width="10%">Product Name</th>
                      <th width="6%">Standard</th>
                      <th width="6%">Grade</th>
                      <th width="12%">Good Description</th>
                      <th width="6%">PCS</th>
                      <th width="7%"> QTY</th>
                      <th width="7%"> Exempted</th>
                      <th width="7%"> QTY/MT</th>
                      <th width="7%">Net Weight<br>(KG)</th>
                      <th width="7%">Gross Weight<br>(KG)</th>
                      <th width="7%">Unit Price<?php echo '('.$data['Order']['Price']['sign'].')'; ?></th>
                      <th width="8%">Net Amount <br><?php echo '('.$data['Order']['Price']['sign'].')';?></th>
                  </tr>                                          
              </thead>
              <tbody>
                <?php  
                  $order_items = $data['Order']['Orderitem'];
                  $in_items = $data['Invoiceitem'];
                  $i=0;$j=0;
                  //pr($order_items);exit;
                  foreach($order_items as $v) { 
                    if(isset($in_items[$i]['orderitemid'])){
                      if($in_items[$i]['orderitemid'] == $v['id']){  ?>
                        <tr>
                          <td><?php echo $in_items[$i]['bundleno'];?></td>
                          <td><?php echo $in_items[$i]['partyinvoiceno'];?></td>
                          <td><?php if(isset($in_items[$i]['Licence']['advancelicenceno'])){ echo $in_items[$i]['Licence']['advancelicenceno'];}  ?></td>
                          <td><?php echo $order_items[$j]['Productcategory']['productname'];?></td>                    
                          <td><?php echo $order_items[$j]['Productcategory']['Standard']['standard_name'];?></td>
                          <td><?php echo $order_items[$j]['Productcategory']['Grade']['grade_name'];?></td>
                          <td><?php echo setGDmmformat($order_items[$j]['Productcategory']['Size']['gdmm'],$order_items[$j]['length']);?></td>
                          <td class="numalign" style="white-space:nowrap;"><?php echo $in_items[$i]['pcs']; ?></td>
                          <td class="numalign"><?php echo $in_items[$i]['qty_mtr'].' '.$order_items[$j]['Productcategory']['Producttaxonomy']['unit'];?></td>
                          <td class="numalign"><?php echo $in_items[$i]['exempted'];?></td>
                          <td class="numalign"><?php echo $in_items[$i]['qty_mt'];?></td>
                          <td class="numalign"><?php echo $in_items[$i]['netweight'];?></td>
                          <td class="numalign"><?php echo $in_items[$i]['grossweight'];?></td> 
                          <td class="numalign"><?php echo $in_items[$i]['price'];?></td>  
                          <td class="numalign"><?php echo $in_items[$i]['netprice']; ;?></td>
                        </tr> 
                        <?php ++$i;
                      } 
                    ++$j;
                    }
                  } ?>
                    <tr id="1">    
                        <td colspan="6"></td>
                        <td id="total_lbl"> Total </td>
                        <td class="numalign"><?php echo $data['Invoice']['totalpcs'];?></td>
                        <td class="numalign"><?php echo $data['Invoice']['totalqty'];?></td>
                        <td class="numalign"><?php echo $data['Invoice']['totalexempted'];?></td> 
                        <td class="numalign"><?php echo $data['Invoice']['totalqtymt']; ?></td>
                        <td class="numalign"><?php echo $data['Invoice']['totalnetweight'];?></td>
                        <td class="numalign"><?php echo $data['Invoice']['totalgrossweight'];?></td>
                          <?php $amount_field = getdelivery_type($data['Order']['delivery_type']);/*fetch amount related field from invoice table based on delivery type*/ ?>                        
                        <td class="align_text"><?php echo strtoupper($amount_field); ?></td>                        
                        <td class="numalign"><?php echo $data['Invoice']["".$amount_field.""];?></td>                        
                     </tr>

                      <!-- Start of Amount Section -->
                      <?php $amount_section = dependentAmountField($data['Invoice'],$data['Order']['delivery_type']);
                      foreach ($amount_section as $amount_sectionvalue) { ?>
                            <tr id="1">
                              <td colspan="12"></td>
                              <td colspan="2" class="align_text"><?php echo $amount_sectionvalue['label'];?></td>
                              <td class="numalign"><?php echo $amount_sectionvalue['value'];?></td>
                            </tr><?
                        }?>
                      <!-- End of Amount Section --> 
                  <tr id="1">
                    <td colspan="15">We Intended to claim rewards under Merchandise Exports From india Scheme(MEIS)</td>
                  </tr> 
          </tbody>
        </table>
      
            <div class="row metroform">
                <div class="col-md-6">
                  <div class="form-group">
                      <label class="control-label col-md-3">Total Packages</label>
                      <div class="col-md-9">
                           <div class="input text"><?php echo $data['Invoice']['totalpackage']?></div>
                           <span class="help-block"><br></span>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3">Total Net Weight</label>
                      <div class="col-md-9">
                          <div class="input text"><?php echo $data['Invoice']['totalnetweight']?></div>  <span class="help-block"><br></span>
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="control-label col-md-3">Total Gross Weight</label>
                      <div class="col-md-9">
                           <div class="input text"><?php echo $data['Invoice']['totalgrossweight']?></div> <span class="help-block"><br></span>
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"> Date</label>
                        <div class="col-md-9">
                             <div class="input text"><?php echo date('d/m/Y',strtotime($data['Invoice']['signdate']));  ?></div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label col-md-3"><b>Party Grade Total</b></label>
                    <div class="col-md-9">
                        <?php echo $data['Invoice']['gradebaseweight'];?>
                         <span class="help-block"><br></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><b>Custom Grade Total</b></label>
                    <div class="col-md-9">
                        <?php echo $data['Invoice']['customgradebaseweight'];?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
                </div>
            </div>
          </div>   
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'invoices'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>