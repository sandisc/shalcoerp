<?php 
/*item -> height
cell boarder
span of tod and top hover of each row, color of header*/
$setting= $this->Session->read('setting_data');

$html = '<style> 
body {font-family:helvetica; font-size:10px;}

table {float: none;margin: 0 auto;width: 100%;border-spacing: 0; cellspacing:5; cellpadding:2;}   
.table {margin-bottom: 0px;width: 100%; }     
.left tr td,th{text-align:left;}
.left tr th{border:0px;}
table tr th,td{padding:3px; letter-spacing: 1px!important;border:1px solid; }
.header tr td{border:0px;!important;}
.table1{width:45%; vertical-align:top;padding:0px;}
.table2{width:55%; vertical-align:top;padding:0px;}
.border_bottom{border-bottom:1px solid !important;}
.border_left{border-left:1px solid;}
.col-md-10{width:100%;}
.col-md-9{width:90%;}
.col-md-8{width:80%;}
.col-md-7{width:70%;}
.col-md-6{width:60%;}
.col-md-5{width:50%;}
.col-md-4{width:40%;}
.col-md-3{width:30%;}
.col-md-2{width:20%;}
.col-md-1{width:10%;}
.numalign { text-align: right;padding-right: 10px !important;margin-right: 10px;margin-left: 3px;}
th span{}
td span{}
.item tr th,td{}
table {
  font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
  font-size: 12px;
  font-style: normal;
  font-variant: normal;
  font-weight: 500;
  line-height: 15px;
}
</style>';

$html .= '<html>
                <head>
                    <title>Invoice</title>
                </head>
                <body>

    <table class="table header"> 
      <tr>
        <td align="left"><img src="'.ROOT_PATH.'img/print_logo.png"></td>
        <td align="right" style="vertical-align: bottom !important;"> 
          <b>CIN NO </b>'.$setting["Setting"]["cin_no"].'<br>
          <span class="smallletter">Office: 24, 2nd Floor, Dayabhai Building<br/>Gulalwadi, Mumbai - 400004<br/> Maharastra, (INDIA). <br/> Tel: + 91-22-40416214/ 40416221/ 25<br/> Fax: +91-22-23452161<br/>
            Email: info@shalco.in Web: www.shalco.in
          </span>
        </td>                    
      </tr>
    </table>';

    if($p == '1'){/*Party Invoice*/
      $reportname = 'Invoice';
    }
    if($p == '0'){/*Party Packing List*/
      $reportname = 'Packing List';
    }
    if($p == '2'){/*Custom Invoice*/
      $reportname = 'Invoice';
    }
    if($p == '3'){/*Custom Packing List*/
      $reportname = 'Packing List';
    }
    $html .= '<table class="table "> 
        <tr>
          <td align="center"><b><h2>'.strtoupper($reportname).'</h2></b></td>
        </tr>
    </table>
    <table class="table left"> 
      <tr>
        <td class="table1">
          <table class="table header">
              <tr> 
                <th>EXPORTER</th>
              </tr>
              <tr>
                <th>SHALCO INDUSTRIES PVT. LTD.</th>
              </tr> 
              <tr>
                <th class="col-md-4"><u>FACTORY ADD</u></th>
                <th class="col-md-6"><u>OFFICE ADD</u></th>
              </tr>  
              <tr>
                <td class="col-md-4">'.nl2br($setting["Setting"]["factory_address"]).'</td>
                <td class="col-md-6">'.nl2br($setting["Setting"]["office_address"]).'</td>
              </tr>
          </table>
        </td>

        <td class="table2">
          <table class="table header">
              <tr>
                <th class="col-md-4">INVOICE NO</th>
                <th class="col-md-6">INVOICE DATE</th>
              </tr>
              <tr>
                <td class="col-md-4">'.$data['Invoice']['invoiceno'].'</td>
                <td class="col-md-6">'.date('d/m/Y',strtotime($data['Invoice']['invoicedate'])).'</td>
              </tr>
              <tr>
                <td class="border_bottom">&nbsp;</td>
                <td class="border_bottom">&nbsp;</td>
              </tr>
              <tr>
                <th class="col-md-4">P.O. NO</th>
                <th class="col-md-6">P.O. DATE</th>
              </tr>
              <tr>
                <td class="border_bottom col-md-4">'.$data['Order']['clientpo'].'</td>
                <td class="border_bottom col-md-6">'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate'])).'</td>
              </tr>
              <tr>
                <th class="col-md-4">Country of Origin of Goods</th>
                <th class="col-md-6">Country of Final Destination</th>
              </tr>
              <tr>
                <td class="col-md-4">'.$data['Invoice']['fromcountry'].'</td>
                <td class="col-md-6">'.$data['Invoice']['tocountry'].'</td>
              </tr>                                    
          </table>
        </td>
      </tr>
    </table>

    <table class="table left"> 
      <tr>
        <td class="table1">
          <table class="table header">
              <tr> 
                <th>CONSIGNEE</th>
              </tr>
              <tr>
                <th>'.$data['Order']['Client']['company_name'].'</th>
              </tr>  
              <tr>
                <td>'.searchFromAddress($data['Order']['billaddress_id'], $data['Order']['Client']['Address']).'</td>              
              </tr>
          </table>
        </td>

        <td class="table2">
          <table class="table header">
              <tr>
                <th>BUYERS (IF OTHER THAN CONSIGNEE)</th>
              </tr>
              <tr>
                <td class="border_bottom">'.nl2br($data['Invoice']['buyers']).'</td>
              </tr>
              <tr>
                <th>IEC CODE NO.    <span>'.$data['Order']['Bank']['iec_code'].'</span></th>
              </tr>                                   
          </table>
        </td>
      </tr>
    </table>

    <table class="table left"> 
      <tr>
        <td class="table1">
          <table class="table header">
              <tr> 
                <th class="col-md-4">PRE CARRIAGE BY</th>
                <th class="col-md-6">PLACE OF RECEIPT BY PRE-CARRIER</th>
              </tr>
              <tr>
                <td class="col-md-4 border_bottom">'.$data['Invoice']['carriageby'].'</td>
                <td class="col-md-6 border_bottom">'.$data['Invoice']['receiptplace'].'</td>
              </tr> 
              <tr>
                <th class="col-md-4">VESSEL/FLIGHT NO.</th>
                <th class="col-md-6">PORT OF LOADING</th>
              </tr>  
              <tr>
                <td class="col-md-4">'.$data['Invoice']['flightno'].'</td>
                <td class="col-md-6">'.$data['Invoice']['loadingport'].'</td>
              </tr>
          </table>
        </td>

        <td class="table2">
          <table class="table header">
              <tr>
                <th>OTHER REFERENCES(S)</th>
              </tr>
              <tr>
                <td>'.$data['Invoice']['otherref'].'</td>
              </tr>                                    
          </table>
        </td>
      </tr>
    </table>  

    <table class="table left"> 
      <tr>
        <td class="table1">
          <table class="table header">
              <tr> 
                <th class="col-md-4">PORT OF DISCHARGE</th>
                <th class="col-md-6">PLACE OF DELIVERY</th>
              </tr>
              <tr>
                <td class="col-md-4 border_bottom">'.$data['Invoice']['carriageby'].'</td>
                <td class="col-md-6 border_bottom">'.$data['Invoice']['receiptplace'].'</td>
              </tr>
              <tr> 
                <th class="col-md-4">CONTAINER NO.</th>
                <th class="col-md-6">TOTAL PACKAGES</th>
              </tr>
              <tr>
                <td class="col-md-4">'.$data['Invoice']['containerno'].'</td>
                <td class="col-md-6">'.$data['Invoice']['totalpackage'].'</td>
              </tr>                
          </table>
        </td>

        <td class="table2">
          <table class="table header">
              <tr rowspan="2">
                <th class="col-md-4">Shipping Terms</th>
                <td class="col-md-6">'.$this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).' '.$data['Order']['tod'].'</td>                
              </tr>  
              <tr rowspan="2">
                <th class="col-md-4">TERMS OF PAYMENT</th>
                <td class="col-md-6">'.$data['Order']['top'].'</td>
              </tr>                                    
          </table>
        </td>
      </tr>
    </table>';    

    $html.= '<table class="table item">
      <tr align="center">';
        
        if($p == 0 || $p == 3){
          $html.= '<th width="10%">Marks & Bundle.</th>
                   <th width="7%">Heat No</th>
                   <th width="12%">Product Name</th>';
        }if($p == 1){
          $html.= '<th width="15%">Marks & Bundle.</th>
                   <th width="14%">Product Name</th>';
        }
        if($p == 2){
          $html.= '<th width="10%">Marks & Bundle.</th>
                   <th width="10%">Product Name</th>
                   <th width="9%">Licence No</th>';
        }        
        $html.= '
        <th width="8%">Standard</th>
        <th width="8%">Grade </th>
        <th width="10%">Good Description</th>
        <th width="7%">PCS</th>
        <th width="8%">QTY <br> UNIT (MT)</th>';
        if($p == 0 || $p == 1){
          $html.= '<th width="10%">QTY</th>';
        }
        if($p == 2 || $p == 3){
          $html.= '<th width="10%">Exempted <br> Material</th>';
        }
        if($p == 1 || $p == 2){
          $html.= '<th width="10%">Price <br> IN('.$data['Order']['Price']['sign'].')</th>
                   <th width="10%">Net Amount <br> IN('.$data['Order']['Price']['sign'].')</th>';
        }
        if($p == 0 || $p == 3){
          $html.= '<th width="10%">Net Weight <br> IN KGS. </th>
                   <th width="10%">Gross Weight <br> IN KGS.</th>';
        }                
      $html.= '</tr>';

      $order_items = $data['Order']['Orderitem'];
      $in_items = $data['Invoiceitem'];
      $i = 0;
      foreach($in_items as $key => $v) { ++$key; 
        $html.= '<tr class="addmore">';
                  $html.= '<td>'.$v['bundleno'].'</td>'; 
                  if($p == 0 || $p == 3){                    
                    $html.= '<td>'.$this->requestAction('App/getHeatnumber/'.$data['Invoice']['id'].'/'.$v['id']).'</td>';
                  }
                    $html.='<td>'.$order_items[$i]['Productcategory']['productname'].'</td>';
                    if($p == 2){
                        if(isset($in_items[$i]['Licence']['advancelicenceno'])){
                          $html.='<td>'.$in_items[$i]['Licence']['advancelicenceno'].'</td>';
                        }else{
                          $html.='<td></td>';
                        }                      
                    }
                    $html.='<td>'.$order_items[$i]['Productcategory']['Standard']['standard_name'].'</td>
                            <td>'.$order_items[$i]['Productcategory']['Grade']['grade_name'].'</td>                        
                            <td>'.setGDmmformat($order_items[$i]['Productcategory']['Size']['gdmm'],$order_items[$i]['length']).'</td>
                            <td class="numalign">'.$v['pcs'].'</td>
                            <td class="numalign">'.$v['qty_mt'].'</td>';                 
                  if($p == 0 || $p == 1){
                    $html.= '<td class="numalign">'.$v['qty_mtr'].' '.$order_items[$i]['Productcategory']['Producttaxonomy']['unit'].'</td>';
                  }
                  if($p == 2 || $p == 3){
                    $html.= '<td class="numalign">'.$v['exempted'].'</td>';
                  }
                  if($p == 1 || $p == 2){
                    $html.= '<td class="numalign">'.$v['price'].'</td>
                             <td class="numalign">'.$v['netprice'].'</td>';
                  }           
                  if($p == 0 || $p == 3){
                    $html.= '<td class="numalign">'.$v['netweight'].'</td>
                             <td class="numalign">'.$v['grossweight'].'</td>';
                  }
                $html.= '</tr>';
                ++$i;
      }
      $amount_field  = getdelivery_type($data['Order']['delivery_type']);/*fetch amount related field from invoice table based on delivery type*/
      if($p == 0 || $p == 3){
        $html.= '<tr class="totalrow">
                  <td colspan="5"></td>
                  <th class="total_lbl numalign">Total</th>
                  <td class="numalign">'.$data['Invoice']['totalpcs'].'</td>
                  <td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
                  if($p == 0){ $html.= '<td class="numalign"></td>'; }
                  if($p == 3){ $html.= '<td class="numalign">'.$data['Invoice']['totalexempted'].'</td>';}
         $html.= '<td class="numalign">'.$data['Invoice']['netweight'].'</td>
                  <td class="numalign">'.$data['Invoice']['grossweight'].'</td>
                </tr>';
      }
      if($p == 1 || $p == 2){
        $html.= '<tr class="totalrow">';
                  if($p == 1){ $html.= '<td colspan="4"></td>'; }
                  if($p == 2){ $html.= '<td colspan="5"></td>';}
         $html.= '<th class="total_lbl numalign">Total</th>
                  <td class="numalign">'.$data['Invoice']['totalpcs'].'</td>
                  <td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
                  if($p == 1){ $html.= '<td class="numalign"></td>'; }
                  if($p == 2){ $html.= '<td class="numalign">'.$data['Invoice']['totalexempted'].'</td>';}
         $html.= '<th class="numalign">'.strtoupper($amount_field).' Value</th>
                  <td class="numalign">'.$data['Invoice'][$amount_field].'</td>
                </tr>';
      }
      if($p == 1 || $p == 2){
            if($p == 1){ $colspan = 7;}
            if($p == 2){ $colspan = 8;}
        $amount_section = dependentAmountField($data['Invoice'],$data['Order']['delivery_type']); 
        /* Start Print footer amount related section */
        foreach ($amount_section as $amount_sectionvalue) {
          $html.= '<tr>';
                    if (strpos($amount_sectionvalue['label'], 'Final') !== false) {
                        $in_words = $this->requestAction('App/numtowords/'.$data['Invoice']['finalvalue'].'/'.$data['Order']['Price']['fullform']);
                        $html.= '<td colspan="'.$colspan.'">'.$in_words.'</td>';
                    }
                    elseif (strpos($amount_sectionvalue['label'], 'Balance') !== false) {
                        $in_words = $this->requestAction('App/numtowords/'.$data['Invoice']['balancevalue'].'/'.$data['Order']['Price']['fullform']);
                        $html.= '<td colspan="'.$colspan.'">'.$in_words.'</td>';
                    }
                    else{
                        $html.= '<td colspan="'.$colspan.'"></td>';
                    }
                      
          $html.= '<th colspan="2" class="numalign">'.$amount_sectionvalue['label'].'</th>
                      <td class="numalign">'.$amount_sectionvalue['value'].'</td>
                   </tr>';
        }
      }
      $html.= '</table>
    <table class="table">
      <tr>
          <td width="70%" height="150px" style="vertical-align: top;">
              <b>TOTAL NET WEIGHT </b>'.$data['Invoice']['totalgrossweight'].' MT <br/>
              <b>TOTAL GROSS WEIGHT </b>'.$data['Invoice']['totalgrossweight'].' MT <br><br/>'.
              nl2br(showAllGradeWeight($data['Invoice']['gradebaseweight'],$data['Invoice']['customgradebaseweight'],$p)).'</td>
          <td width="30%" height="150px" align="left" style="vertical-align: top;">
                <span style="vertical-align: top;"><b>Signature & Date <br/>FOR SHALCO INDUSTRIES PVT LTD</b><br/></span>
                <span style="vertical-align: bottom!important;"><b>DIRECTOR</b></span></td>        
      </tr>
    </table>        
  </body>
</html>';
echo $html;