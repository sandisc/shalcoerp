a
<?php $settings = $this->Session->read('setting_data'); ?>
<div class="portlet box" id="invoice_packing">
    <div class="portlet-title">
        <div class="caption"><?php echo 'Packing list';?></div>
    </div>
    <div class="portlet-body form">
        <div class="form-body metroform "> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                 <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"><br></span>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">Factory Address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['factory_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['office_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-6">
                        <label class="control-label">INVOICE NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['invoiceno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">INVOICE DATE</label>
                      <div class="input-group">
                      <div class="input text"><?php echo date('d-m-Y',strtotime($data['Invoice']['invoicedate']))?></div>
                      </div>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-6">
                        <label class="control-label">P.O. NO.</label>
                        <div class="input text"><?php echo $data['Order']['clientpo']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">

                      <label class="control-label">P.O. DATE</label>
                      <div class="input-group">
                        <div class="input text"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate']));?></div>
                      </div>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-6"><label class="control-label">COUNTRY OF ORIGIN OF GOODS</label>
                      <div class="input text"><?php echo $data['Invoice']['fromcountry']?></div><span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">COUNTRY OF FINAL DESTINATION</label>
                       <div class="input text"><?php echo $data['Invoice']['tocountry']?></div>
                       <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-12">
                        <label class="control-label">CONSIGNEE</label>
                       <div class="input text"><?php echo $data['Order']['Client']['company_name'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                       <div class="input text"><?php echo searchFromAddress($data['Order']['billaddress_id'], $data['Order']['Client']['Address']);?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">BUYER'S (IF OTHER THAN CONSIGNEE)</label>
                       <div class="input text"><?php echo $data['Invoice']['buyers']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                        <label class="control-label">IEC CODE NO. </label><?php echo $orderdata['Bank']['iec_code'];?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIAGE BY</label>
                        <div class="input text"><?php echo $data['Invoice']['carriageby']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> PLACE OF RECEIPT BY PRE-CARRIER</label>
                        <div class="input text"><?php echo $data['Invoice']['receiptplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>

                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">OTHER REFERENCES(S)</label>
                        <div class="input text"><?php echo $data['Invoice']['otherref']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">VESSEL/FLIGHT NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['flightno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">PORT OF LOADING</label>
                        <div class="input text"><?php echo $data['Invoice']['loadingport']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">Shipping Terms</label>
                        <div class="input text"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).' '.$data['Order']['tod']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>

            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">PORT OF DISCHARGE</label>
                        <div class="input text"><?php echo $data['Invoice']['dischargeport']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">PLACE OF DELIVERY</label>
                        <div class="input text"><?php echo $data['Invoice']['deliveryplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">TERMS OF PAYMENT</label>
                        <div class="input text"><?php echo $data['Order']['top']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
               <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-12">
                        <label class="control-label">CONTAINER NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['containerno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                   
                </div>
                
                <div class="col-xs-6">
                     
            </div>
        </div>  <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                <thead class="flip-content portlet box green">
                    <tr>
                        <th width="12%">NO. & KIND PACKAGES</th>
                        <th width="15%">Product name</th>
                        <th width="10%">STANDARD</th>
                        <th width="10%">MATERIAL<br>GRADE</th>
                        <th width="20%" colspan="1">GOOD DESCRIPTION</th>
                        <th width="6%">PC</th>
                        <th width="8%" id="qty_lbl"> EXEMPTED</th>
                        <th width="8%" id="qty_lbl"> QTY<br>MT</th>
                        <th width="10%" id="amount_lbl">Net Weight<br> IN KGS</th>
                        <th width="15%" id="amount_lbl">Gross Weight<br> IN KGS</th>
                        
                    </tr>                                          
                </thead>
                <tbody>
        <?php  
                    $order_items = $data['Order']['Orderitem'];
                    $in_items = $data['Invoiceitem'];
                    $i = 0;
                       foreach($order_items as $key => $v) { ++$key; ?>
                    <tr class="addmore<?php echo $key;?> ">
                    <td>
                        <?php echo $in_items[$i]['bundleno']; ?>

                    </td>
                    <td> 
                            <?php echo $v['Productcategory']['productname'];;?>
                  </td>
                  <td> 
                            <?php echo $v['Standard']['standard_name'];?>
                  </td>
                  <td> 
                             <?php echo $v['Grade']['grade_name'];?>
                  </td>
                        <?php $total_length = $v['length'] * 1000; ?>
                  <td> 
                        <?php echo $v['Size']['gdmm'].' X '.$v['length'];?>
                  </td>
                        <td style="white-space:nowrap;">
                          <?php echo $in_items[$i]['pcs']; ?>

                        </td>
                        <td class="numalign"> 
                          <?php echo $in_items[$i]['exempted'];?>
                        </td>
                        
                         <td class="numalign"> 
                        <?php echo $in_items[$i]['qty_mt'];?>
                    </td>
                     <td class="numalign">
                        <?php echo $in_items[$i]['netweight'];
                          ?>
                    </td>
                    <td class="numalign">
                        <?php echo $in_items[$i]['grossweight'];
                          ?>
                    </td>
                    </tr>      
          <?php  } ?>
                    <tr class="" id="1">    
                       <td colspan="4"></td>
                       <td id="total_lbl"> Total </td>
                      <td><?php echo $data['Invoice']['totalpcs'];
                        ?></td> 
                      <td><?php echo $data['Invoice']['totalexempted'];
                        ?></td> 
                        <td><?php echo $data['Invoice']['totalqtymt'];?></td>
                        <td><?php echo $data['Invoice']['totalnetweight'];?></td>
                        <td><?php echo $data['Invoice']['totalgrossweight'];?></td> 
                     </tr>
                      <?php if(!empty($data['Invoice']['advancereceive']) && $data['Invoice']['advancereceive'] != 0) {?>
                      <tr id="1" class="prints">
                        <td colspan="8"></td>
                        <td>Advance Received</td>
                        <td><?php echo $data['Invoice']['advancereceive'];?></td>
                      </tr>
                      <?php } ?>
                      <tr id="1" class="prints">    
                        <td colspan="8"></td>
                        <td>Final Value</td>
                        <td><?php echo $data['Invoice']['finalvalue'];?></td>
                      </tr>

                    
                </tbody>
            </table>
              </div>
        <div class="row metroform">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL PACKAGES<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                             <div class="input text"><?php echo $data['Invoice']['totalpackage']?></div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL NET WEIGHT<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                            <div class="input text"><?php echo $data['Invoice']['netweight']?></div>  <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL GROSS WEIGHT<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                             <div class="input text"><?php echo $data['Invoice']['grossweight']?></div> <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="control-label col-xs-3"> Date<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                             <div class="input text"><?php echo date('d-m-Y',strtotime($data['Invoice']['signdate']));?></div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3"> Signature<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                            <img src="<?php echo ROOT_PATH.'uploads/signature/'.$settings['Setting']['signature'];?>" width="200">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
              
          <div class="col-md-6">

                <div class="form-group">
                    <label class="control-label col-md-3"><b>Party Grade Total</b></label>
                    <div class="col-md-9">
                        <?php echo $data['Invoice']['gradebaseweight'];?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
        </div>
      
    </div>
</div>