<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
      <!-- Begin: life time stats -->
      <div class="portlet light portlet-fit portlet-datatable bordered">
       <?php echo $this->Session->flash();?>
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
            </div>
           <?php echo $this->Form->create('Proformainvoice',array('enctype' => 'multipart/form-data')); ?>	
           <div class="actions">
               <div class="btn-group btn-group-devided">
               
                    <?php echo $this->Form->input('uploadfile', array('type' => 'file','class'=>'form-control','label'=>false,'required'=>'required')); ?>
               </div>
                <button class="btn green" type="submit">Import</button>
                <?php echo $this->Form->end();?>
               <div class="btn-group">
               </div>
            </div>
         </div>
         
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm" id ="sel_ct">
                                                <option value="">Select...</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                                <option value="delete">Delete</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button class="btn btn-sm red table-group-action-delete">
                                                <i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="4%">
                                                        <input type="checkbox" class="group-checkable"> </th>
                                                    <th width="12%"> PROFORMA No. </th>
                                                    <th width="12%"> Proforma Date </th> 
                                                    <th width="12%"> Client Name </th>
                                                    <th width="12%"> Modified Date</th>
                                                    <th width="15%"> Modified By </th>
                                                    <th width="8%"> Status </th>
                                                    <th width="20%"> Action </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="proforma_no"> </td>
                                                     <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="proforma_date" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                    
                                                    </td> 
                                                  <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>
                                                  <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td> 
                                                    <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                                                   <td></td >                           
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
      </div>
      <!-- End: life time stats -->
   </div>
</div>




