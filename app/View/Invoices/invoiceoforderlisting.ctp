<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  foreach ($invoice as $invoices) {
    $records = array(); 
     
    $records[] = '<input type="checkbox" name="id[]" value="'.$invoices['Invoice']['id'].'">';
    $records[] = $invoices['Invoice']['invoiceno'];
    $records[] = $this->requestAction('App/date_ymd_to_dmy/'.strtotime($invoices['Invoice']['invoicedate']));
    $records[] = $invoices['Invoice']['totalqty'];
    $records[] = $invoices['Invoice']['totalgrossweight'];
    $records[] = $invoices['Invoice']['finalvalue'];    
    $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($invoices['Invoice']['modified']));
    $records[] = $invoices['User']['first_name']." ".$invoices['User']['last_name'];
    $id = $invoices['Invoice']['id'];
   
    $records[] = '<a href="'.WEBSITE_PATH.'invoices/view/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>
       <a href="'.WEBSITE_PATH.'invoices/edit/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      ';
      /*<a href="'.WEBSITE_PATH.'invoices/delete/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this invoice?"><span class="glyphicon glyphicon-trash"></span></a>*/
      /*<a id="printrow" data-controllername="invoices" data-actionname="custominvoice" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Custom Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span>Custom Invoice
      </a>
      <a id="printrow" data-controllername="invoices" data-actionname="partyinvoice" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Party Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span>Party Invoice
    </a>
        <a id="printrow" data-controllername="invoices" data-actionname="custompacking" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Custom Packing" targer="_blank"><span class="glyphicon glyphicon-print"></span>Custom Packing
        </a>
         <a id="printrow" data-controllername="invoices" data-actionname="partypacking" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Party Packing" targer="_blank"><span class="glyphicon glyphicon-print"></span>Party Packing
        </a>
      ';*/
    $record['data'][] = $records;
  }

  $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
  // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>