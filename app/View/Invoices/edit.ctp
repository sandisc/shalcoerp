<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<?php $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>         
        
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
        <?php echo $this->Form->create('Invoice',array('type' => 'file','class'=>'metroform shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxeditsubmit/')); 
            echo $this->Form->input('id',array('type'=> 'hidden'));
             echo $this->Form->input('orderid',array('type'=> 'hidden','value'=>$invoice_data['Invoice']['orderid']))?>
        <div class="form-body">
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Exporter</label>
                        <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Factory Address</label>
                        <div class="input textarea"><?php echo nl2br($setting['Setting']['factory_address']); ?></div>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                        <div class="input textarea"><?php echo nl2br($setting['Setting']['office_address']); ?></div>
                        <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Invoice NO.</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('invoiceno', array('class'=>'form-control uniqueNo','label'=>false,'required'=>true,'type'=>'text','readonly')); ?>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6 date date-picker" data-date-format="dd/mm/yyyy">
                        <label class="control-label">Invoice Date</label><span class="required" aria-required="true"> * </span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <?php echo $this->Form->input('invoicedate', array('class'=>'form-control changedatepicker','label'=>false,'required'=>true,'type'=>'text','value' => date('d/m/Y',strtotime($this->request->data['Invoice']['invoicedate'])),'autocomplete'=>'off')); ?>
                        </div>
                    </div>
                    <? echo $this->Form->input('fy', array('type'=>'hidden','id'=>'fy','class'=>'form-control','label'=>false));?>
                </div>
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Client P.O. NO.</label><div class="input text"><?php echo $invoice_data['Order']['clientpo']; ?></div>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-3">
                        <label class="control-label">Client P.O. Date</label><div class="input text"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($invoice_data['Order']['clientpodate'])); ?></div>
                        <div class="input-group"></div>
                    </div>
                   <div class="col-lg-3">
                        <label class="control-label">Order No</label><div class="input text"><?php echo $invoice_data['Order']['orderno']; ?></div>
                        <div class="input-group"></div>
                    </div>                    
                </div>                
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Country Of Origin Goods</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('fromcountry', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Country Of Final Destination</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('tocountry', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                        <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Consignee</label><?php echo $this->Form->input('clientid',array('type'=> 'hidden','value'=>$invoice_data['Order']['Client']['id'])); ?>
                        <div class="input textarea"><?php echo $invoice_data['Order']['Client']['company_name'].'</b></br>'.searchFromAddress($invoice_data['Order']['billaddress_id'], $invoice_data['Order']['Client']['Address']);?>
                        </div>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">IEC CODE NO.</label>
                        <div class="input text"> <?php echo $invoice_data['Order']['Bank']['iec_code']; ?></div>                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Buyer's (If Other Than Consignee)</label>
                        <?php echo $this->Form->textarea('buyers', array('class'=>'form-control','label'=>false,'rows'=>2,'cols'=>2)); ?>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">

                        <span class="help-block"><br></span>
                    </div>
                </div>
            </div>            
            <!--/row-->

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIAGE BY</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('carriageby', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                        <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Place of Receipt By Pre-Carrier</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('receiptplace', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                        <span class="help-block"><br></span>
                    </div>
                </div><!--/col-md-6-->
                <div class="col-md-6">
                   <div class="col-lg-12">
                        <label class="control-label">Other Reference</label>
                        <?php echo $this->Form->textarea('otherref', array('class'=>'form-control','label'=>false,'rows'=>2,'cols'=>2)); ?>
                        <span class="help-block"><br></span>
                    </div>
                </div><!--/col-md-6-->
            </div>
            <!--/row--> 
                
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Vessel/Flight NO.</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('flightno', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Port of Loading</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('loadingport', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Delivery</label>
                        <div class="input text"><?php echo $this->requestAction('App/get_delivery_type/'.$invoice_data['Order']['delivery_type']).' '.$invoice_data['Order']['tod'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->  
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Port of Discharge</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('dischargeport', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Place of Delivery</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('deliveryplace', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                            <span class="help-block"><br></span>
                    </div>
                </div>                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Payment</label>
                        <div class="input text"><?php echo $invoice_data['Order']['top'];?></div>
                        <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Container No</label><span class="required" aria-required="true"> * </span>
                        <?php echo $this->Form->input('containerno', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'text')); ?>
                        <span class="help-block"><br></span>
                    </div>                   
                </div>                
                <div class="col-md-6">
<!--                     <div class="col-lg-12">
                        <label class="control-label">HSCODE</label><span class="required" aria-required="true"> * </span>
                        <?php $selected = $invoice_data['Invoice']['hscodeid'];?>
                        <?php echo $this->Form->input('hscodeid', array('class'=>'form-control','label'=>false,'required'=>true,'type'=>'select','options'=>$hscodes,'selected'=>$selected)); ?>
                        <span class="help-block"><br></span>
                    </div> -->
                </div>
            </div>  
            <!--/row--> 
            <div class="table-scrollable">
            <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                <thead class="flip-content portlet box green">
                      <?php echo invoiceitemTableColumn('('.$invoice_data['Order']['Price']['sign'].')');?>
                </thead>
                <tbody>
                    <?php  
                    $invoices = $invoice_data['Order']['Orderitem'];
                    
                    $i = 0;
                    $sum_total = 0;
                    $invoice_item_count = 0;
                    $selectedlicenceitemid = '';/*variable to save selected licence item id in hidden*/
                    $selectedlicenceid = '';/*variable to save selected licence id in hidden*/                    
                    foreach($invoices as $key => $v) { ++$key; 
                        /*ignore if item has status = 3 means cancelled*/
                        $count = count($invoices);
                        if($v['status']!=3){ 

                            /*if item has invoice than allow and if item has not invoice and in pending state than allow*/
                            if(!empty($v['Invoiceitem']['id']) || (empty($v['Invoiceitem']) && $v['status'] == 0)){
                                ++$invoice_item_count;
                                if(isset($v['Invoiceitem']['id'])){ 
                                    $in_id = $v['Invoiceitem']['id']; 
                                    $selectedlicenceid = $v['Invoiceitem']['licenceid'];
                                }
                                else { 
                                    $in_id = '';
                                    $selectedlicenceid = '';
                                }
                                /*Fetch Licence associated with particular grade and hscode*/
                                $licenceoptions = $this->requestAction('App/getLicenceOption/'.$v['Productcategory']['Hscode']['id'].'/'.$v['Productcategory']['Grade']['gradetagid'].'/'.$selectedlicenceid);                  

                                if(empty($licenceoptions)){
                                      $licencedropdown =  '<input type="hidden" name="data[Invoice][item]['.$key.'][licenceid]" value="0" class="input" />';  
                                }else{                                
                                    $licencedropdown = '<select name="data[Invoice][item]['.$key.'][licenceid]" class="form-control licenceid" required="required">';
                                    $licencedropdown .= $licenceoptions;
                                        /*foreach ($licenceoptions as $value) {
                                          if(!empty($value['Licenceitem'])){
                                            foreach ($value['Licenceitem'] as $key2 => $value2){
                                                if(isset($v['Invoiceitem']['licenceid'])){
                                                     if($v['Invoiceitem']['licenceid'] == $value['Licence']['id']){
                                                        $selectedlicenceitemid = $value2['id'];
                                                        $selectedlicenceid = $value['Licence']['id'];
                                                        $licencedropdown .= "<option value=".$value['Licence']['id']." selected='selected' itemid=".$value2['id'].">".$value['Licence']['advancelicenceno']." (EX ".date('d/m/Y',strtotime($value['Licence']['expiry_date'])).")</option>"; 
                                                    }
                                                    else{
                                                        $licencedropdown .= "<option value=".$value['Licence']['id']." itemid=".$value2['id'].">".$value['Licence']['advancelicenceno']." (EX ".date('d/m/Y',strtotime($value['Licence']['expiry_date'])).")</option>";
                                                    }    
                                                }
                                                else{
                                                    $licencedropdown .= "<option value=".$value['Licence']['id']." itemid=".$value2['id'].">".$value['Licence']['advancelicenceno']." (EX ".date('d/m/Y',strtotime($value['Licence']['expiry_date'])).")</option>";
                                                }    
                                            }
                                          }
                                        }*/         
                                    $licencedropdown .= '</select>'; 
                                }        
                                $licencedropdown .= $this->Form->input('licenceitemid', array('type'=>'hidden','class'=>'form-control mylicenceitemid','label'=>false,'name'=>'data[Invoice][item]['.$key.'][licenceitemid]','value'=>$selectedlicenceitemid));
                                $licencedropdown .= $this->Form->input('oldlicenceid', array('type'=>'hidden','class'=>'form-control','label'=>false,'name'=>'data[Invoice][item]['.$key.'][oldlicenceid]','value'=>$selectedlicenceid));   
                                ?> 

                                <tr class="dispalydetails showinfo">
                                    <td rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $key;?></td>
                                    <td colspan="10" class="basicdisplayinfo">
                                      <b>Product :</b> <?php echo $v['Productcategory']['productname'];?> &nbsp;&nbsp;
                                      <b>Unit :</b> <?php echo $v['Productcategory']['Producttaxonomy']['unit'];?> &nbsp;&nbsp; 
                                      <b>STD :</b> <?php echo $v['Productcategory']['Standard']['standard_name'];?>&nbsp;&nbsp;
                                      <b>Grade :</b> <?php echo $v['Productcategory']['Grade']['grade_name'];?> &nbsp;&nbsp;
                                      <b>Gradetag :</b> <?php echo $v['Productcategory']['Gradetag']['gradetagname'];?> &nbsp;&nbsp;
                                      <b>Size :</b> <?php echo setGDmmformat($v['Productcategory']['Size']['gdmm'],$v['length']);?>&nbsp;&nbsp;
                                      <b>Ratio :</b> <?php echo $v['Productcategory']['Size']['calc_ratio'];?>&nbsp;&nbsp;
                                      <b>HSCode :</b> <?php echo $v['Productcategory']['Hscode']['hscode']; ?>&nbsp;&nbsp;
                                      <b>HS Value :</b> <?php echo $v['Productcategory']['Hscode']['rate'];?>
                                    </td>                                    
                                   <td class="numeric" rowspan="2" style="vertical-align: middle;text-align: center;"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','id'=>'delete1','class'=>'deleteno','mydelnum'=> $key,'data-id'=> $in_id)); ?>
                                   </td>
                                </tr> 

                                <tr class="addmore<?php echo $key;?> filldetail">

                                 <?php            
                                    echo $this->Form->input('id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'id'.$key,'name'=>'data[Invoice][item]['.$key.'][id]','value'=>$in_id));
                                    echo $this->Form->input('orderitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id'],'name'=>'data[Invoice][item]['.$key.'][orderitemid]'));
                                    echo $this->Form->input('qty', array('type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['qty'],'name'=>'data[Invoice][item]['.$key.'][qty]'));
                                    echo $this->Form->input('hscode_rate', array('type'=>'hidden','class'=>'form-control','label'=>false,'name'=>'data[Invoice][item]['.$key.'][hscode_rate]','value'=>$v['Productcategory']['Hscode']['rate']));
                                    echo $this->Form->input('productname', array('class'=>'form-control ','type'=>'hidden','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$v['Productcategory']['productname'],'readonly'));
                                    echo $this->Form->input('standard_id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'required'=>true,'id'=>'standard_name'.$key,'value'=>$v['Productcategory']['Standard']['standard_name'],'readonly'));
                                    echo $this->Form->input('gradetagid', array('type'=>'hidden','class'=>'form-control','label'=>false,'name'=>'data[Invoice][item]['.$key.'][gradetagid]','value'=>$v['Productcategory']['Grade']['gradetagid']));                                 
                                    echo $this->Form->input('grade_id', array('type'=>'hidden','class'=>'form-control','label'=>false,'name'=>'data[Invoice][item]['.$key.'][grade_id]','value'=>$v['Productcategory']['Grade']['id']));
                                    echo $this->Form->input('grade_name', array('class'=>'form-control ','type'=>'hidden','label'=>false,'required'=>true,'id'=>'grade_name'.$key,'name'=>'data[Invoice][item]['.$key.'][grade_name]','value'=>$v['Productcategory']['Grade']['grade_name']));
                                    echo $this->Form->input('size_id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'required'=>true,'value'=>$v['Productcategory']['Size']['gdmm'].' X '.$v['length'],'readonly'));
                                    if(isset($v['Invoiceitem']['qty_mtr'])){ $oldqty = $v['Invoiceitem']['qty_mtr']; } else { $oldqty = '';} 
                                    echo $this->Form->input('oldqty', array('type'=>'hidden','label'=>false,'name'=>'data[Invoice][item]['.$key.'][oldqty]','value'=>$oldqty));
                                                                       
                                    ?>
                                    <td><?php if(isset($v['Invoiceitem']['bundleno'])){ $bundle = $v['Invoiceitem']['bundleno']; } else { $bundle = '';} echo $this->Form->input('bundleno', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'bundleno'.$key,'name'=>'data[Invoice][item]['.$key.'][bundleno]','value'=>$bundle)); ?>
                                    </td>
                                    <td><?php if(isset($v['Invoiceitem']['partyinvoiceno'])){ $partyinvoiceno = $v['Invoiceitem']['partyinvoiceno']; } else { $partyinvoiceno = '';} 
                                        echo $this->Form->input('partyinvoiceno', array('class'=>'form-control ','type'=>'text','label'=>false,'id'=>'partyinvoiceno'.$key,'name'=>'data[Invoice][item]['.$key.'][partyinvoiceno]','value'=>$partyinvoiceno));?>
                                    </td>                        
                                    <td>
                                        <?php echo $licencedropdown;?>                                                                       
                                    </td>
                                    <td style="white-space:nowrap;"><?php if(isset($v['Invoiceitem']['pcs'])){ $pcs = $v['Invoiceitem']['pcs']; } else { $pcs = '';} 
                                        echo $this->Form->input('pcs', array('class'=>'form-control pcs', 'onkeyup'=>'countpcs()', 'type'=>'text','label'=>false,'required'=>true,'id'=>'length1','name'=>'data[Invoice][item]['.$key.'][pcs]','value'=>$pcs,'autocomplete'=>'off')); ?>
                                    </td>
                                    <td><?php if(isset($v['Invoiceitem']['qty_mtr'])){ $unit = $v['Invoiceitem']['qty_mtr']; } else { $unit = '';} 
                                        echo $this->Form->input('qty_mtr', array('class'=>'form-control qty_mtr','type'=>'text','label'=>false,'required'=>true,'id'=>'qty_mtr'.$key,'value'=>$unit,'name'=>'data[Invoice][item]['.$key.'][qty_mtr]','onkeyup'=>'countqty()','data-sizeratio' => $v['Productcategory']['Size']['calc_ratio'],'data-id'=>$key,'data-unit'=> $v['Productcategory']['Producttaxonomy']['unit'],'data-density'=>$v['Productcategory']['Physical']['density']));?>
                                    </td>                                   
                                    <td><?php if(isset($v['Invoiceitem']['qty_mt'])){ $qty_mt = $v['Invoiceitem']['qty_mt']; } else { $qty_mt = '';} echo $this->Form->input('qty_mt', array('class'=>'form-control qty_mt','type'=>'text','label'=>false,'required'=>true,'onkeyup'=>'countqtymt()','id'=>'qty_mt'.$key,'name'=>'data[Invoice][item]['.$key.'][qty_mt]','value'=>$qty_mt,'data-id'=>$key,'autocomplete'=>'off'));?>
                                    </td>
                                    <td><?php if(isset($v['Invoiceitem']['netweight'])){ $netweight = $v['Invoiceitem']['netweight']; } else { $netweight = '';} 
                                        echo $this->Form->input('netweight', array('class'=>'form-control totalnet','label'=>false,'type'=>'text','required'=>true,'onkeyup'=>'counttotalnetweight()','id'=>'totalnetweight'.$key,'name'=>'data[Invoice][item]['.$key.'][netweight]','value'=>$netweight,'data-id'=>$key,'autocomplete'=>'off'));?>
                                    </td>      
                                    <td><?php if(isset($v['Invoiceitem']['grossweight'])){ $grossweight = $v['Invoiceitem']['grossweight']; } else { $grossweight = '';} 
                                        echo $this->Form->input('grossweight', array('class'=>'form-control totalgross','label'=>false,'type'=>'text','required'=>true,'onkeyup'=>'counttotalgrossweight()','id'=>'totalgrossweight'.$key,'name'=>'data[Invoice][item]['.$key.'][grossweight]','value'=>$grossweight,'data-id'=>$key,'autocomplete'=>'off'));?>
                                    </td>
                                    <td><?php if(isset($v['Invoiceitem']['price'])){ $price = $v['Invoiceitem']['price']; } else { $price = '';} 
                                        echo $this->Form->input('price', array('class'=>'form-control prc','type'=>'text','label'=>false,'required'=>true,'id'=>'price'.$key,'value'=>$price,'name'=>'data[Invoice][item]['.$key.'][price]','data-id'=>$key,'autocomplete'=>'off'));?>
                                    </td>                                       
                                    <td><?php if(isset($v['Invoiceitem']['netprice'])){ $net = $v['Invoiceitem']['netprice']; } else { $net = '';} 
                                        echo $this->Form->input('netprice', array('class'=>'form-control netprice','label'=>false,'type'=>'text','required'=>true,'id'=>'netprice'.$key,'readonly','value'=>$net,'name'=>'data[Invoice][item]['.$key.'][netprice]','data-id'=>$key,'readonly')); 
                                        ?>
                                    </td>                     
                                </tr>      
                                <?php  ++$i;
                                $sum_total += $net;
                            } /*End If Condition of status = 0 */
                        }/*End If Condition of status != 3 */
                    }/*End Foreach loop */?>             
                    <input type="hidden" name="updateno" id="up" value="<?php echo $invoice_item_count; ?>" class="input" />
                    <input type="hidden" name="removal_id" id="removal_id" value="" />
                    <?php 
                        $amount_field = getdelivery_type($this->request->data['Order']['delivery_type']);/*fetch amount related field from invoice table based on delivery type*/
                    ?>         
                    <tr id="1">              
                    <input type="hidden" value="<?php echo $this->request->data['Order']['delivery_type']?>" class="dlvr_type">
                        <td colspan="3"></td>
                        <td id="total_lbl"> Total </td>
                        <td><?php echo $this->Form->input('totalpcs', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'totalpcs','readonly'));?></td>
                        <td><?php echo $this->Form->input('totalqty', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'qtymtrtotal','readonly'));?></td>
                        <td><?php echo $this->Form->input('totalqtymt', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'totalqtymt','readonly'));?></td>
                        <td><?php echo $this->Form->input('totalnetweight', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'totalnetweight','readonly'));?></td>
                        <td><?php echo $this->Form->input('totalgrossweight', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'totalgrossweight','readonly'));?></td>
                        <td class="align_text"> <?php echo strtoupper($amount_field);?> </td>                              
                        <td><?php echo $this->Form->input($amount_field, array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'ciftotal','readonly','value'=>number_format((float)$sum_total,2, '.', '')));?></td> 
                    </tr>
                    <?php if ($invoice_data['Order']['delivery_type'] == 1) { ?>
                            <tr id="1">
                                <td colspan="9"></td>
                                <td id="total_lbl" class="align_text"> FOB Value </td>
                                <td><?php echo $this->Form->input('fob', array('class'=>'form-control common_cls','label'=>false,'type'=>'text','required'=>true,'id'=>'fob'));?></td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                                <td id="total_lbl" class="align_text">Frieght</td>
                                <td><?php echo $this->Form->input('frieght', array('class'=>'form-control common_cls','label'=>false,'type'=>'text','required'=>true,'id'=>'frieght'));?></td>
                            </tr>
                            <tr id="1">    
                                <td colspan="9"></td> 
                                <td id="total_lbl" class="align_text">Insurance</td>
                                <td><?php echo $this->Form->input('insurance', array('class'=>'form-control common_cls','label'=>false,'type'=>'text','required'=>true,'id'=>'insurance'));
                                  ?></td>
                            </tr>
                            <tr id="1">
                                <td colspan="9"></td>
                                <td id="total_lbl" class="align_text">Discount</td>
                                <td><?php echo $this->Form->input('discount', array('class'=>'form-control discount','label'=>false,'type'=>'text','required'=>true,'id'=>'discount'));?></td>
                            </tr> <?php 
                        }
                        elseif ($invoice_data['Order']['delivery_type'] == 2) { ?>
                            <tr id="1">
                                <td colspan="9"></td>
                                <td id="total_lbl" class="align_text">Discount</td>
                                <td><?php echo $this->Form->input('discount', array('class'=>'form-control discount','label'=>false,'type'=>'text','required'=>true,'id'=>'discount'));?></td>
                            </tr> <?php 
                        }    
                        elseif ($invoice_data['Order']['delivery_type'] == 3) { ?>
                            <tr id="1">
                              <td colspan="9"></td>
                              <td id="total_lbl" class="align_text"> P & F </td>
                              <td><?php echo $this->Form->input('packing', array('class'=>'form-control packing','label'=>false,'type'=>'text','required'=>true,'id'=>'packing'));?></td>
                            </tr> <?php 
                        }                                
                        elseif ($invoice_data['Order']['delivery_type'] == 4) { ?>
                            <tr id="1">
                              <td colspan="9"></td>
                              <td id="total_lbl" class="align_text"> FOB Value </td>
                              <td><?php echo $this->Form->input('fob', array('class'=>'form-control common_cls','label'=>false,'type'=>'text','required'=>true,'id'=>'fob'));?></td>
                            </tr> 
                            <tr>
                              <td colspan="9"></td>
                              <td id="total_lbl" class="align_text">Frieght</td>
                              <td><?php echo $this->Form->input('frieght', array('class'=>'form-control common_cls','label'=>false,'type'=>'text','required'=>true,'id'=>'frieght'));?></td>
                            </tr>
                            <tr id="1"> 
                              <td colspan="9"></td>
                              <td id="total_lbl" class="align_text">Discount</td>
                              <td><?php echo $this->Form->input('discount', array('class'=>'form-control discount','label'=>false,'type'=>'text','required'=>true,'id'=>'discount'));?></td>              
                            </tr> <?php 
                        } ?>

                      <tr id="1">    
                        <td colspan="9">We Intended to claim rewards under Merchandise Exports From india Scheme(MEIS)</td>
                        <td class="align_text">Final <?php echo strtoupper($amount_field);?> Value</td>
                        <td><?php echo $this->Form->input('finalvalue', array('class'=>'form-control','label'=>false,'type'=>'text','id'=>'finalvalue','readonly'));?></td>
                      </tr>

                     <tr id="1">    
                      <td colspan="9">Exempted Materials Stainless Steel Seamless Pipes/Tubes(HOT Finished)</td>
                      <td class="align_text">Advance Received</td>
                      <td><?php echo $this->Form->input('advancereceive', array('class'=>'form-control','label'=>false,'type'=>'text','id'=>'advancereceive'));?></td>
                    </tr> 
                    
                    <tr id="1">    
                      <td colspan="9"></td>
                      <td class="align_text">Balance Value</td>
                      <td><?php echo $this->Form->input('balancevalue', array('class'=>'form-control','label'=>false,'type'=>'text','id'=>'balancevalue','readonly'));?></td>
                    </tr>        
                </tbody>
            </table>
        </div> <!--/End DIV table-scrollable -->

        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Total Packages<span class="required" aria-required="true"> * </span></label>
                    <div class="col-md-9">
                        <?php echo $this->Form->input('totalpackage', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Total Net Weight<span class="required" aria-required="true"> * </span></label>
                    <div class="col-md-9">
                        <?php echo $this->Form->input('netweight', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'total_net_weight')); ?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Total Gross Weight<span class="required" aria-required="true"> * </span></label>
                    <div class="col-md-9">
                        <?php echo $this->Form->input('grossweight', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'total_gross_weight')); ?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="form-group">                        
                    <div class="col-md-1"></div>
                    <div class="col-md-11 checkbox margin0px">
                        <label><?php echo $this->Form->input('manual', array('type' => 'checkbox')); ?></label>
                    </div>
                </div>              
            </div>
            <div class="col-md-6">                                
                <div class="form-group">
                    <label class="control-label col-md-3"> <!-- Signature<span class="required" aria-required="true"> * </span> --></label>
                    <div class="col-md-9">
                        <!-- <img src="<?php echo ROOT_PATH.'uploads/signature/'.$setting['Setting']['signature'];?>" width="200"> -->
                         <span class="help-block"><br></span>
                    </div>
                </div>               
            </div>
        </div><!--/row -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Party Grade Total<span class="required" aria-required="true"> * </span></label>
                    <div class="col-md-9">
                        <?php echo $this->Form->textarea('gradebaseweight', array('label'=>false,'required'=>true,'cols'=>'2','rows'=>'2','class'=>'form-control','id'=>'gradebaseweight','value'=>nl2br($invoice_data['Invoice']['gradebaseweight']))); ?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Custom Grade Total<span class="required" aria-required="true">* </span></label>
                    <div class="col-md-9">
                        <?php echo $this->Form->textarea('customgradebaseweight', array('label'=>false,'required'=>true,'class'=>'form-control','id'=>'customgradebaseweight','cols'=>'2','rows'=>'2')); ?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
        </div><!--/row -->

    </div><!--/form-body -->    
    <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
                            <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'invoices'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
    </div><!--/form-actions --> 
    </div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->

<?php echo $this->element('invoice_commonfunction'); ?>

