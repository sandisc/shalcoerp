<?php $settings = $this->Session->read('setting_data'); 
//$_GET['p'] = $p;?>
<div class="portlet box" id="invoice_print" >
    <div class="portlet-title">
        <div class="caption"><?php echo 'Invoice';?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
        <div class="form-body metroform "> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                 <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"><br></span>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">Factory Address</label>
                      <div class="input textarea"><?php echo nl2br($settings['Setting']['factory_address']); ?></div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea"><?php echo nl2br($settings['Setting']['office_address']); ?></div>
                      <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-6">
                        <label class="control-label">INVOICE NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['invoiceno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">INVOICE DATE</label>
                      <div class="input-group">
                      <div class="input text"><?php echo date('d/m/Y',strtotime($data['Invoice']['invoicedate']))?></div>
                      </div>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-6">
                        <label class="control-label">P.O. NO.</label>
                        <div class="input text"><?php echo $data['Order']['clientpo']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">

                      <label class="control-label">P.O. DATE</label>
                      <div class="input-group">
                        <div class="input text"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Order']['clientpodate']));?></div>
                      </div>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-6">
                        <label class="control-label">COUNTRY OF ORIGIN OF GOODS</label>
                      <div class="input text"><?php echo $data['Invoice']['fromcountry']?></div>      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">COUNTRY OF FINAL DESTINATION</label>
                       <div class="input text"><?php echo $data['Invoice']['tocountry']?></div>
                       <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-12">
                        <label class="control-label">CONSIGNEE</label>
                       <div class="input text"><?php echo $data['Order']['Client']['company_name'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                       <div class="input text"><?php echo searchFromAddress($data['Order']['billaddress_id'], $data['Order']['Client']['Address']);?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">BUYER'S (IF OTHER THAN CONSIGNEE)</label>
                       <div class="input text"><?php echo nl2br($data['Invoice']['buyers'])?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                        <label class="control-label">IEC CODE NO. </label><?php echo $data['Order']['Bank']['iec_code'];?></label>
                                <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIAGE BY</label>
                        <div class="input text"><?php echo $data['Invoice']['carriageby']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> PLACE OF RECEIPT BY PRE-CARRIER</label>
                        <div class="input text"><?php echo $data['Invoice']['receiptplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>

                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">OTHER REFERENCES(S)</label>
                        <div class="input text"><?php echo $data['Invoice']['otherref']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            
            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">VESSEL/FLIGHT NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['flightno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">PORT OF LOADING</label>
                        <div class="input text"><?php echo $data['Invoice']['loadingport']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">Shipping Terms</label>
                        <div class="input text"><?php echo $this->requestAction('App/get_delivery_type/'.$data['Order']['delivery_type']).' '$data['Order']['tod']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>

            <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-6">
                        <label class="control-label">PORT OF DISCHARGE</label>
                        <div class="input text"><?php echo $data['Invoice']['dischargeport']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">PLACE OF DELIVERY</label>
                        <div class="input text"><?php echo $data['Invoice']['deliveryplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-xs-6">
                     <div class="col-lg-12">
                        <label class="control-label">TERMS OF PAYMENT</label>
                        <div class="input text"><?php echo $data['Order']['top']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
               <div class="row separatordiv">
                <div class="col-xs-6">
                    <div class="col-lg-12">
                        <label class="control-label">CONTAINER NO.</label>
                        <div class="input text"><?php echo $data['Invoice']['containerno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                   
                </div>
                
                <div class="col-xs-6">
                     
            </div>
        </div>  <!--/row-->   
        <?php 
    if($data['Order']['Price']['sign'] == '&#8377;'){
      $sym = '₹';
    }else if($data['Order']['Price']['sign'] == '&#36'){
      $sym = '$';
    } else if($data['Order']['Price']['sign'] == '&#8364;'){
      $sym = '€'; 
    }else {
      $sym = '£';
    } 
    ?>
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
          <?php 
              $excelcolumn='A';
              $itemHeader[$excelcolumn] = "Marks & Bundle.";  $itemSubHeader[$excelcolumn] = 'Container No : '.$data['Invoice']['containerno']; 
              if($_GET['p'] == 0 || $_GET['p'] == 3){
                $itemHeader[++$excelcolumn] = "Heat No";  $itemSubHeader[$excelcolumn] = ''; 
              }
              $product_column_key = ++$excelcolumn;
              $product_column_nxt_key = ++$excelcolumn;

              $itemHeader[$product_column_key] = "Product Name"; $itemSubHeader[$product_column_key] = ''; 
              $standard_column_key = ++$excelcolumn;
              $itemHeader[$standard_column_key] = "Standard"; $itemSubHeader[$excelcolumn] = ''; 

              $grade_column_key = ++$excelcolumn;
              $itemHeader[$grade_column_key] = "Grade"; $itemSubHeader[$excelcolumn] = '';

              $gooddesc_column_key = ++$excelcolumn;
              $itemHeader[$gooddesc_column_key] = "Good Description"; $itemSubHeader[$excelcolumn] = ''; 

              $pcs_column_key = ++$excelcolumn;
              $itemHeader[$pcs_column_key] = "PCS"; $itemSubHeader[$excelcolumn] = ''; 

              $qtymt_column_key = ++$excelcolumn;
              $itemHeader[$qtymt_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = '(UNIT MT)'; 
              
              if($_GET['p'] == 0 || $_GET['p'] == 1){
                $qtymtr_column_key = ++$excelcolumn;
                $itemHeader[$qtymtr_column_key] = "QTY"; $itemSubHeader[$excelcolumn] = ''; 
              }
              if($_GET['p'] == 2 || $_GET['p'] == 3){
                $exempted_column_key = ++$excelcolumn;
                $itemHeader[$exempted_column_key] = "Extempted"; $itemSubHeader[$excelcolumn] = ' MATERIAL '; 
              }
              if($_GET['p'] == 1 || $_GET['p'] == 2){ 
                $price_column_key = ++$excelcolumn;
                $itemHeader[$price_column_key] = "Unit Price"; $itemSubHeader[$excelcolumn] = ' IN ('.$sym.') '; 
                $netamount_column_key = ++$excelcolumn;
                $itemHeader[$netamount_column_key] = "Net Amount";  $itemSubHeader[$excelcolumn] = ' IN '.$sym.''; 
              }     
              if($_GET['p'] == 0 || $_GET['p'] == 3){
                $netweight_column_key = ++$excelcolumn;
                $itemHeader[$netweight_column_key] = "Net Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
                $grossweight_column_key = ++$excelcolumn;
                $itemHeader[$grossweight_column_key] = "Gross Weight"; $itemSubHeader[$excelcolumn] = '( IN KGS )'; 
              }

              
           ?>

              <thead class="flip-content portlet box green">
              <?php 
                  echo '<tr>';
                foreach ($itemHeader as $itemHeaderkey => $itemHeadervalue) {
                    echo '<th>'.$itemHeadervalue.'</th>';
                }
                echo '</tr>';
                echo '<tr>';
                foreach ($itemSubHeader as $itemSubHeaderkey => $itemSubHeadervalue) {
                 echo '<th>'.$itemSubHeadervalue.'</th>';
                }
                echo '</tr>';
              ?>
              </thead>
              <tbody>
              <?php  
                    $order_items = $data['Order']['Orderitem'];
                    $in_items = $data['Invoiceitem'];
                    $i = 0; $j=0;
                    $dataArray = array();
                    $idx = '1';
                    $index = '1';           
                    $k = 0;
                foreach($order_items as $v) { 
                  if(isset($in_items[$i]['orderitemid'])){
                    if($in_items[$i]['orderitemid'] == $v['id']){ 
                        $dataArray[$index][$k] = $in_items[$i]['bundleno'];
                        if($_GET['p'] == 0 || $_GET['p'] == 3){
                          $dataArray[$index][++$k] = $in_items[$i]['heatnumber'];             
                        }           
                        $dataArray[$index][++$k] = $order_items[$j]['Productcategory']['productname']; 
                        $dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Standard']['standard_name'];
                        $dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Grade']['grade_name'];
                        $dataArray[$index][++$k] = $order_items[$j]['Productcategory']['Size']['gdmm'].'X'.$order_items[$j]['length'];
                        $dataArray[$index][++$k] = $in_items[$i]['pcs'];
                        if($_GET['p'] != 2){
                          $dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mt']);
                        }
                        if($_GET['p'] != 3){
                          $dataArray[$index][++$k] = decimalQty($in_items[$i]['qty_mtr']);
                        }
                        if($_GET['p'] == 2 || $_GET['p'] == 3){
                          $dataArray[$index][++$k] = decimalQty($in_items[$i]['exempted']);
                        }
                        if($_GET['p'] == 2 || $_GET['p'] == 1){
                          $dataArray[$index][++$k] = $in_items[$i]['price'];
                          $dataArray[$index][++$k] = decimalPrice($in_items[$i]['netprice']);
                        }
                        if($_GET['p'] == 0 || $_GET['p'] == 3){
                          $dataArray[$index][++$k] = $in_items[$i]['netweight'];
                          $dataArray[$index][++$k] = $in_items[$i]['grossweight'];  
                        }                
                        ++$i; 
                        $k = 0;
                        $idx++;
                        $index++;                                 
                   } 
                   ++$j;
                }
            }
      $id = '24';
      $size = count($dataArray);
      foreach ($dataArray as $key => $value) {
        end($value);
        echo '<tr>';
        $keys = key($value);
        for($i=0,$j='A';$i<=$keys;$i++,$j++) {
            echo '<td class="numalign">'.$value[$i].'</td>';
        }
        echo '</tr>';
        $id++;
      }
      echo '<tr>';
      if($_GET['p'] == 0){
        echo '<td colspan="5"></td>';     
        echo '<td>Total</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalpcs'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqty'].'</td>';
        echo '<td class="numalign class="numalign"">'.$data['Invoice']['netweight'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['grossweight'].'</td>';
      }
      $amount_field  = getdelivery_type($data['Order']['delivery_type']);
      if($_GET['p'] == 1){
        echo '<td colspan="4"></td>';     
        echo '<td class="numalign">Total</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalpcs'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqty'].'</td>';
        echo '<td class="numalign">'.strtoupper($amount_field).' Value</td>';
        echo '<td class="numalign">'.$data['Invoice'][$amount_field].'</td>';
      }
      if($_GET['p'] == 2){
        echo '<td colspan="4"></td>';     
        echo '<td class="numalign">Total</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalpcs'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalexempted'].'</td>';
        echo '<td class="numalign">'.strtoupper($amount_field).' Value</td>';
        echo '<td class="numalign">'.$data['Invoice'][$amount_field].'</td>';
      }
      if($_GET['p'] == 3){
        echo '<td colspan="5"></td>';     
        echo '<td class="numalign">Total</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalpcs'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalqtymt'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['totalexempted'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['netweight'].'</td>';
        echo '<td class="numalign">'.$data['Invoice']['grossweight'].'</td>';
      }
      echo '</tr>';
      
      $amount_section = dependentAmountField($data['Invoice'],$data['Order']['delivery_type']);
      
      if($_GET['p'] == 1 || $_GET['p'] == 2){
      /* Start Print footer amount related section */
      foreach ($amount_section as $amount_sectionvalue) {
        //$index = ++$nxt_val;
        echo '<tr>';
        echo '<td colspan="8"></td>';
        echo '<td class="numalign">'.$amount_sectionvalue['label'].'</td>';
        echo '<td class="numalign">'.$amount_sectionvalue['value'].'</td>';
        echo '</tr>';
      }
      /* End Print footer amount related section */     
      echo '<tr>';
      $num = $this->requestAction('App/numtowords/'.$data['Invoice']['finalvalue'].'/'.$data['Order']['Price']['fullform']);
      echo '<td colspan="10">'.$num.'</td>';
      echo '</tr>';
    }
      ?>  


              </tbody>
           </table>
              </div>
        <div class="row metroform">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL PACKAGES<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                             <div class="input text"><?php echo $data['Invoice']['totalpackage']?></div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL NET WEIGHT<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                            <div class="input text"><?php echo $data['Invoice']['netweight']?></div>  <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3">TOTAL GROSS WEIGHT<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                             <div class="input text"><?php echo $data['Invoice']['grossweight']?></div> <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-3"><b>Grade Total</b></label>
                    <div class="col-md-9">
                        <?php echo showAllGradeWeight($data['Invoice']['gradebaseweight'],$data['Invoice']['customgradebaseweight']);?>
                         <span class="help-block"><br></span>
                    </div>
                </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                      <label class="control-label col-xs-3"> Date<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                            <div class="input text"><?php echo date('d/m/Y',strtotime($data['Invoice']['signdate']));?></div>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3"> Signature<span class="required" aria-required="true"></span></label>
                        <div class="col-xs-9">
                            <img src="<?php echo ROOT_PATH.'uploads/signature/'.$settings['Setting']['signature'];?>" width="200">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
          <div class="col-md-6">
                <!--<div class="form-group">
                    <label class="control-label col-md-3"><b>Custom Grade Total</b></label>
                    <div class="col-md-9">
                        <?php echo $data['Invoice']['customgradebaseweight'];?>
                         <span class="help-block"><br></span>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
</html>
