<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
      <?php echo $this->Session->flash();?>   
      <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
            </div>
            <div class="actions">
               <div class="btn-group">
               </div>
            </div>
        </div>
         
        <div class="portlet-body">
              <div class="table-container">
                  <div class="table-actions-wrapper">
                      <span> </span>
                      <select class="table-group-action-input form-control input-inline input-small input-sm" id ="sel_ct">
                          <option value="">Select...</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                          <option value="delete">Delete</option>
                      </select>
                      <button class="btn btn-sm green table-group-action-submit">
                          <i class="fa fa-check"></i> Submit</button>
                      <button class="btn btn-sm red table-group-action-delete">
                          <i class="fa fa-trash"></i> Delete</button>
                  </div>
                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                      <thead>
                          <tr role="row" class="heading">
                              <th width="2%">
                                  <input type="checkbox" class="group-checkable"> </th>
                              <th width="10%"> INVOICE No. </th>
                              <th width="10%"> Invoice Date </th> 
                              <th width="12%"> Order No. </th> 
                              <th width="8%"> Client Name </th>
                              <th width="8%"> Modified Date</th>
                              <th width="8%"> Modified By </th>
                             <th width="17%"> Actions </th>
                          </tr>
                          <tr role="row" class="filter">
                              <td> </td>
                              <td><input type="text" class="form-control form-filter input-sm" name="invoiceno"> </td>
                               <td>
                                  <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="invoicedate" placeholder="From">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>

                              </td>
                              <td><input type="text" class="form-control form-filter input-sm" name="orderno"> </td> 
                              <td><input type="text" class="form-control form-filter input-sm" name="ship_to"> </td>
                              <td>
                                  <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>
                                  <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                      <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                      <span class="input-group-btn">
                                          <button class="btn btn-sm default" type="button">
                                              <i class="fa fa-calendar"></i>
                                          </button>
                                      </span>
                                  </div>
                              </td> 
                              <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>
                             
                                                          
                              <td>
                                  <div class="margin-bottom-5">
                                      <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                      <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                  </div>
                              </td>
                          </tr>
                      </thead>
                      <tbody> </tbody>
                  </table>
              </div>
        </div>
      </div>
      <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->

<!--- Start Approve Proforma Invoice Dialog Modal -->
<div class="modal fade" id="shoppingform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Invoice',array('method'=>'POST','class'=>'shalcoform')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Shipping Info</h4>
      </div>
      <div class="modal-body">
        <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'exportid'));
          echo '<div class="clearfix"> <label for="foo">SB NO.<span class="required"> * </span></label>';
          echo $this->Form->input('sbno',array('type'=>'text','label'=>false,'placeholder'=>"SB No","class" => "form-control sbno",'id'=>'sbno','required'));           
          echo '</div><div class="clearfix"><br><label for="Extended Expiry Date">SB Date</label>
          <div class="input-group date date-picker" id="dp3" data-date-format="dd/mm/yyyy">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
          echo $this->Form->input('sbdate',array('type'=>'text','label'=>false,'placeholder'=>"SB date", "class" => "form-control input-group",'id'=>'ex_date','required'));
          echo '</div></div>'
        ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btnExport" class="btn btn-primary">Save</button>
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!--- End Approve Proforma Invoice Dialog Modal -->
<script>
    $(document).on('click', '#btn_shopp', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      $('#shoppingform').modal({
          show: true,
      });
        var licenceid = $(this).attr('data-lid');
        $('#exportid').val(licenceid);
        var sbdate = $(this).attr('data-sbdate');
        $('#ex_date').val(sbdate);
        var sbno = $(this).attr('data-sbno');
        $('#sbno').val(sbno);
     $(document).on('click', '#btnExport', function () {
        $("#spinner").show();
        e.preventDefault();
        e.stopImmediatePropagation();      
        var id = $('#exportid').val();
         var sbno = $('#sbno').val();
        var date =  $('#ex_date').val();
        $.ajax({
          url: '<?php echo WEBSITE_PATH;?>invoices/shoppinginfo/',
          type: 'POST', 
          data: 'id='+licenceid+'&sb_date='+date+'&sb_no='+sbno,
          cache: false,
          success: function (data) {
            $("#spinner").hide();
            $('.modal').modal('hide');
            $('.test'+id).hide();
            /*$('.test'+id).parent().parent('tr:first').find('td:eq(4)').text(sbno);
            $('.test'+id).parent().parent('tr:first').find('td:eq(5)').text(date);
            */
         }
        });         
      });
    
    });

  $(document).on('click', '#btn_verify', function () {
    var id = $(this).val();
    var ref = $(this);
    
    $.ajax({
      url: '<?php echo WEBSITE_PATH;?>proformainvoices/getVerified/'+id,
      type: 'POST',
      cache: false,
      success: function (data) { 
        ref.parent().html('<lable class="success"><i class="fa-lg fa fa-check">&nbsp;</i>Verified</lable>');
         ref.parent().parent().parent().prev('td').html('<button class="btn blue btn-outline" type="button" id="btn_accept" value="1" data-pid="">Accept</button>&nbsp;<button class="btn red btn-outline" type="button" id="btn_reject" value="2" data-pid="">Reject</button>');
       
        //location.reload();
      }
    });
  });
$(document).ready(function() {
 
  $(document).on('click', '#btn_accept', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var accept = $(this);
    var status_id = $(this).val();
    var performaid = $(this).attr('data-pid');
    $("#rejectid").val(performaid);
    $("#approveid").val(performaid);
  /*Active*/
      $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getStatus/'+performaid+'/'+status_id,
    type: 'POST', 
    cache: false,
    success: function (data) {       
      accept.parent().html('<span class="label label-sm label-success">Accepted</span>');
    }
  }); 
      $('#myModal2').modal({
        show: true,
      });
      $("#btnApprove").click(function(){ 
        e.preventDefault();
        e.stopImmediatePropagation();      
        var dispatch = $('select[id="dispatch_id"]').val();
        $.ajax({
        url: '<?php echo WEBSITE_PATH;?>proformainvoices/approveproformainvoice/',
        type: 'POST', 
        data: 'id='+$('#approveid').val()+'&dispatch='+dispatch+'&dispatchcode='+$("#approvecomment").val()+'&pocode='+$("#approvepo").val(),
        cache: false,
        success: function (data) {
        $('.modal').modal('hide');
          $('.modal-body').find('select').val('');
          $('.modal-body').find('textarea,input').val('');
         
        }
      }); 
    });

});
  
  /* 
   Reject Proformainvoice
  */
$(document).on('click', '#btn_reject', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var status_id = $(this).val();
    var performaid = $(this).attr('data-pid');
    var reject = $(this);
    $("#rejectid").val(performaid);
    $('#myModal1').modal({
      show: true,
    });

    $("#btnReject").click(function(e){ 
      e.preventDefault();
      e.stopImmediatePropagation();
      $.ajax({
          url: '<?php echo WEBSITE_PATH;?>proformainvoices/rejectproformainvoice/',
          type: 'POST', 
          data: 'id='+$('#rejectid').val()+'&comment='+$("#rejectcomment").val(),
          cache: false,
          success: function (data) {
          $('.modal').modal('hide');
            $('.modal-body').find('textarea,input').val('');

          }
      }); 
    });
        
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getStatus/'+performaid+'/'+status_id,
    type: 'POST', 
    cache: false,
    success: function (data) {       
      reject.parent().html('<span class="label label-sm label-danger test">Rejected</span>');
    }
  }); 
});
});


$(document).on('change', '.dispatch', function (e) {
     var location = $(this).val();
     var proforma_id = $("#approveid").val();     
     $.ajax({
      url: '<?php echo WEBSITE_PATH;?>proformainvoices/getDispatchcode/'+location+'/'+proforma_id,
      type: 'POST',
      cache: false,
       success: function (data) {
    $('#approvecomment').val(data);
  }

 });



     
});

</script>