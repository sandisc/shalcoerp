<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  foreach ($invoice as $invoices) {
    $records = array(); 
    $id = $invoices['Invoice']['id']; 
    if($invoices['Invoice']['isshipped'] != 0){
        $shop_info = '';
    }else{
           $shop_info = '<button class="btn blue btn-outline test'.$invoices['Invoice']['id'].'" type="button" id="btn_shopp" data-lid='.$invoices['Invoice']['id'].'>Shipping info</button>';
     
      }

    
      /*check record entered in next stage means used in another module*/
     /* $record_nextstage = $this->requestAction(
                    array('controller' => 'App', 'action' => 'check_nextstage'),
                    array('dependantmodel' => 'Certificate','conditional_parameter' => 'Certificate.refid = '.$id.' and Certificate.reftype = 0')
      );*/

    $records[] = '<input type="checkbox" name="id[]" value="'.$invoices['Invoice']['id'].'">';
    $records[] = '<a href="'.WEBSITE_PATH.'invoices/view/'.base64_encode($invoices['Invoice']['id']).'" title="View Invoice">'.$invoices['Invoice']['invoiceno'].'</a>';
    $records[] = $this->requestAction('App/date_ymd_to_dmy/'.strtotime($invoices['Invoice']['invoicedate']));
    $records[] = '<a href="'.WEBSITE_PATH.'orders/view/'.base64_encode($invoices['Order']['id']).'" title="View Order">'.$invoices['Order']['orderno'].'</a>';
    $records[] = $invoices['Order']['Client']['company_name'];
    $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($invoices['Invoice']['modified']));
    $records[] = $invoices['User']['first_name']." ".$invoices['User']['last_name'];
    
    $nextaction = '';
      /*Set Link of test certificate*/
        $tc_check = $this->requestAction('Certificates/checkCertiGenerate/0/'.$id);//check Test certificate generate or not
        if($tc_check == 0){
          $nextaction.= '<a href="'.WEBSITE_PATH.'certificates/generatetc/0/'.base64_encode($invoices['Invoice']['orderid']).'/'.base64_encode($invoices['Invoice']['id']).'" title="Generate Test Certificate" class="btn btn-success btn-double"><span class="glyphicon glyphicon-certificate"></span></a>';
        }
        else{
          $nextaction.= '<a href="'.WEBSITE_PATH.'certificates/viewtclist/0/'.base64_encode($id).'" title="View Test Certificate" class="btn btn-warning btn-double"><span class="glyphicon glyphicon-certificate"></span></a>';
        }
        $permis_sess = '';
      if(in_array('invoices/edit',$this->Session->read('accesscontrollers_actions'))){
        $permis_sess .= '<a href="'.WEBSITE_PATH.'invoices/edit/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>';
      }
      if(in_array('invoices/delete',$this->Session->read('accesscontrollers_actions'))){
        $permis_sess .= '<a href="'.WEBSITE_PATH.'invoices/delete/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this invoice?"><span class="glyphicon glyphicon-trash"></span></a>';
      }
            
    $printoption = '<a id="printrow" data-controllername="invoices" data-actionname="custominvoice" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Custom Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span>Custom Invoice</a>
      <a id="printrow" data-controllername="invoices" data-actionname="partyinvoice" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Party Invoice" targer="_blank"><span class="glyphicon glyphicon-print"></span>Party Invoice</a>
      <a id="printrow" data-controllername="invoices" data-actionname="custompacking" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Custom Packing" targer="_blank"><span class="glyphicon glyphicon-print"></span>Custom Packing</a>
      <a id="printrow" data-controllername="invoices" data-actionname="partypacking" data-id ="'.base64_encode($invoices['Invoice']['id']).'" class="btn blue margin-top-10 margin-bottom-10 margin-right-10 prints" title="Print Party Packing" targer="_blank"><span class="glyphicon glyphicon-print"></span>Party Packing</a>';

    $records[] = '<a href="'.WEBSITE_PATH.'invoices/view/'.base64_encode($invoices['Invoice']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>'.$permis_sess.$nextaction.$shop_info;

    $record['data'][] = $records;
  }

  $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
  // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>