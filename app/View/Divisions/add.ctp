<h3 class="page-title"> <?php echo $mainTitle;?></h3>
                  
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools">
            <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
     <?php echo $this->Form->create('Division',array('class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                      echo $this->Form->input('id', array('type'=>'hidden')); 
                      if(isset($this->params['named']['targetid'])){
                          echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                      }?> 
           <div class="form-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Division<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-8 input-icon right">
                                <?php echo $this->Form->input('division_name', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                <span class="help-block"><br></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Division Alias<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-8 input-icon right">
                                <?php echo $this->Form->input('division_alias', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                <span class="help-block"><br></span>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Excise Regn No<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-8 input-icon right">
                                <?php echo $this->Form->input('excise_regn_no', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                <span class="help-block"><br></span>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">                        
                                <button class="btn green" type="submit">Submit</button>
                                 <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'divisions'),array('class' => 'btn default')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
            <?php echo $this->Form->end();?>      
        <!-- END FORM-->
    </div>
</div>

<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>