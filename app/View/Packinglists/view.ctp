<?php echo $this->Html->link(__('View as PDF'), array('action' => 'pdf_index', 'ext' => 'pdf', $data[0]['Invoice']['id'])); ?>

<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<?php  $settings = $this->Session->read('setting_data');/*Read setting table's data*/?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <div class="portlet-body form">
        <div class="form-body"> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                 <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"><br></span>
                    </div>
                     <div class="col-lg-6">
                        <label class="control-label">Factory address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['office_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['factory_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Invoice No</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['invoiceno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">

                        <label class="control-label">Date</label>
                      <div class="input-group">
                      <div class="input text"><?php echo date('d-m-Y',strtotime($data[0]['Invoice']['invoicedate']));?></div>
                      </div>
                    </div>
                </div>`
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">P.O. No</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['Order']['clientpo']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">

                      <label class="control-label">Date</label>
                      <div class="input-group">
                        <div class="input text"><?php echo date('d-m-Y',strtotime($data[0]['Invoice']['invoicedate']));?></div>
                      </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Country Of Origin Goods</label>
                      <div class="input text"><?php echo $data[0]['Invoice']['fromcountry']?></div>      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Country Of final Destination</label>
                       <div class="input text"><?php echo $data[0]['Invoice']['fromcountry']?></div>
                       <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Consignee</label>
                       <div class="input text"><?php echo $data[0]['Invoice']['Order']['Client']['company_name'];?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Consignee Address</label>
                       <div class="input text"><?php echo $data[0]['Invoice']['Order']['Client']['address1'];?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Buyer's</label>
                       <div class="input text"><?php echo $data[0]['Invoice']['buyers']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                        <label class="control-label">IEC CODE NO.</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['ifscno']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIEGE BY</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['carriageby']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Place of Receipt By Pre-Carrier</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['receiptplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>

                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Other Reference</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['otherref']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
            <!--/row-->
            
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Vesse Flight no</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['flightno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Port of Loading</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['loadingport']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Delivery</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['top']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Port of Discharge</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['dischargeport']?></div>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Place of Delivery</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['deliveryplace']?></div>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Payment</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['top']?></div>
                            <span class="help-block"><br></span>
                    </div>
                </div>
            </div>
               <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Container no</label>
                        <div class="input text"><?php echo $data[0]['Invoice']['containerno']?></div>
                            <span class="help-block"><br></span>
                    </div>
                   
                </div>
                
                <div class="col-md-6">
                     
            </div>
        </div>  <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                <thead class="flip-content portlet box green">
                    <tr>
                        <th width="12%">No of Kind Packages</th>
                        <th width="15%">Product</th>
                        <th width="10%">STANDARD</th>
                        <th width="10%">MATERIAL<br>GRADE</th>
                        <th width="20%" colspan="1">GOOD DESCRIPTION</th>
                        <th width="4%">Pcs</th>
                        <th width="6%" id="qty_lbl"> QTY<br>MTR. <?php echo $data[0]['Invoice']['Order']['Price']['sign']; ?> </th>
                        <th width="10%" id="prc_lbl">Exempted<br>MTR <?php echo $data[0]['Invoice']['Order']['Price']['sign']; ?></th>
                        <th width="10%" id="amount_lbl">Net Amount <?php echo $data[0]['Invoice']['Order']['Price']['sign']; ?></th>
                          <th width="10%" id="amount_lbl">Gross Amount <?php echo $data[0]['Invoice']['Order']['Price']['sign']; ?><br></th>
                    </tr>                                          
                </thead>
                <tbody>
         <?php  
                        $packings_items = $data[0]['Invoice']['Invoiceitem'];
                         $packings = $data[0]['Invoice']['Order']['Orderitem'];
                       
                        $in_items = $data[0]['Packingitem'];
                        $i =0;
                       foreach($packings_items as $key => $v) {  ++$key;    if(isset($in_items[$i]['id'])){ $in_id = $in_items[$i]['id']; }else { $in_id = '';} ?> 
                    <tr class="addmore<?php echo $key;?>">
                     <?php echo $this->Form->input('id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'id'.$key,'name'=>'data[Packinglist][item]['.$key.'][id]','value'=>$in_id));?>
                         <?php echo $this->Form->input('invoiceitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id'],'name'=>'data[Packinglist][item]['.$key.'][invoiceitemid]'));?>
                        <td> 
                            <?php echo $this->Form->input('bundleno', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'bundleno'.$key,'value'=>$v['bundleno']));?>
                        </td>
                        <td> 
                            <?php echo $this->Form->input('productname', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['productname']));?>
                        </td>
                        <td> 
                            <?php echo $this->Form->input('standard_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['Standard']['standard_name']));?>
                        </td>
                        <td> 
                             <?php echo $this->Form->input('grade_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['Grade']['grade_name']));?>
                        </td>
                        <?php $total_length = $packings[$i]['length'] * 1000; ?>
                        <td> 
                        <?php echo $this->Form->input('size_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'value'=>$packings[$i]['Size']['gdmm'].' X '.$packings[$i]['length']));?>
                        </td>
                        <td style="white-space:nowrap;">
                          <?php echo $this->Form->input('pcs', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'length1','value'=>$v['pcs']));
                          ?>
                        </td>
                        <td> 
                          <?php echo $this->Form->input('qty_mtr', array('class'=>'form-control qty','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>round($v['qty_mtr'],2)));?>
                        </td>

            <td style="white-space:nowrap;">
              <?php echo $this->Form->input('exempted', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'heat1','value'=>round($v['exempted'],3)));
              ?>
            </td>
                        <td> <?php if(isset($in_items[$i]['netweight'])){ $weight = $in_items[$i]['netweight']; }else { $weight = '';} ?>
                          <?php echo $this->Form->input('netweight', array('class'=>'form-control prc','type'=>'text','label'=>false,'required'=>true,'id'=>'price1','name'=>'data[Packinglist][item]['.$key.'][netweight]','value'=>round($weight,3)));?>
                        </td>
                        <td> <?php if(isset($in_items[$i]['grossweight'])){ $gross = $in_items[$i]['grossweight']; }else { $gross = '';} ?>
                        <?php echo $this->Form->input('grossweight', array('class'=>'form-control netprice','label'=>false,'type'=>'text','required'=>true,'id'=>'amount1','name'=>'data[Packinglist][item]['.$key.'][grossweight]','value'=>round($gross,3)); ++$i;
                        ?>
                        </td>
                     
                    </tr>      
          <?php  } ?>
                    <tr id="1">    
                       
                       <td colspan="5"></td>
                       <td id="total_lbl"> Total </td>
                       <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>$data[0]['Invoice']['totalqty']));
                        ?></td>
                        <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>$data[0]['Invoice']['totalexempted']));
                        ?></td> 
                        <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly'));
            ?></td> 
                        <td><?php echo $this->Form->input('cif', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>''));
                        ?></td> 
                     </tr>
                   

                </tbody>
            </table>
              </div>
        <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Packages<span class="required" aria-required="true"></span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('totalpackage', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$data[0]['Invoice']['totalpackage'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Net Wieght<span class="required" aria-required="true"></span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('netweight', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$data[0]['Invoice']['netweight'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Gross Weight<span class="required" aria-required="true"></span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('grossnet', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$data[0]['Invoice']['grossweight'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"> Date<span class="required" aria-required="true"></span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('date', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"> Signature<span class="required" aria-required="true"></span></label>
                        <div class="col-md-9">
                            <img src="<?php echo ROOT_PATH.'uploads/signature/'.$settings['Setting']['signature'];?>" width="200">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
                            <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'packinglists'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->
