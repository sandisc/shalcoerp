<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $record = array();
 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Approval"),
    array("danger" => "Rejected"),
    array("danger" => "Pending")

  );
  //pr($Packinglist);
    foreach ($Packinglist as $invoices) {
        $records = array(); 
     
      $records[] = '<input type="checkbox" name="id[]" value="'.$invoices['Invoice']['id'].'">';

     $records[] = $invoices['Packinglist']['packingno'];
   // $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($invoices['Invoice']['invoicedate']));
     $records[] = $invoices['Invoice']['Order']['Client']['company_name'];
     $records[] = $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($invoices['Packinglist']['modified']));
     $records[] = $invoices['Invoice']['Order']['User']['first_name']." ".$invoices['Invoice']['Order']['User']['last_name'];
     $id = $invoices['Packinglist']['id'];
     //$records[] = '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>';
   
     $records[] = '<a href="'.WEBSITE_PATH.'packinglists/view/'.base64_encode($invoices['Packinglist']['id']).'" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>
       <a href="'.WEBSITE_PATH.'packinglists/edit/'.base64_encode($invoices['Packinglist']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'packinglists/delete/'.base64_encode($invoices['Packinglist']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this invoice?"><span class="glyphicon glyphicon-trash"></span></a>';
      $record['data'][] = $records;
    }
  //pr($record);exit;

  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
    
*/
    $record["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$record["customActionMessage"] = ""; // pass custom message(useful for getting status of group actions)
 // }

  $record["draw"] = $sEcho;
  $record["recordsTotal"] = $iTotalRecords;
  $record["recordsFiltered"] = $iTotalRecords;
  echo json_encode($record); exit;
?>
