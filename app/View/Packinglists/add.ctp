<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<?php $setting= $this->Session->read('setting_data'); /*Read setting table's data*/?>
<?php echo $this->Form->create('Packinglist',array('type' => 'file','class'=>'metroform')); 
    echo $this->Form->input('invoiceid',array('type'=> 'hidden','value'=>$id));?>				  
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
		<div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
	</div>
	<div class="portlet-body form">
		<div class="form-body">	
			    <!--/row-->
			    <div class="row separatordiv">
            <div class="col-md-6">
                <div class="col-lg-12">
                  <label class="control-label">Exporter</label>
                  <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                  <span class="help-block"><br></span>
                </div>
                <div class="col-lg-6">
                	<label class="control-label">Factory address</label>
                  <div class="input textarea"><?php echo $setting['Setting']['factory_address']; ?></div>
                  <span class="help-block"><br></span>
                </div>
                <div class="col-lg-6">
                	<label class="control-label">Office Address</label>
                  <div class="input textarea"><?php echo $setting['Setting']['office_address']; ?></div>
                  <span class="help-block"><br></span>
                </div>
            </div>

            <div class="col-md-6">
            	 <div class="col-lg-6">
                	<label class="control-label">Invoice NO.</label>
                	<?php echo $this->Form->input('packingno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'invoice_no','type'=>'text','value'=>$order_item[0]['Invoice']['invoiceno'],'readonly')); ?>
                       	<span class="help-block"><br></span>
                </div>
                <div class="col-lg-6 date date-picker" data-date-format="dd-mm-yyyy"">
                	<label class="control-label">Invoice Date</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <?php echo $this->Form->input('invoicedate', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'invoice_no','type'=>'text','value'=>date('d-m-Y',strtotime($order_item[0]['Invoice']['invoicedate'])))); ?>
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
            	 <div class="col-lg-6">
                	<label class="control-label">P.O NO.</label>
                	<?php echo $this->Form->input('pono', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'pono','type'=>'text','value'=>$order_item[0]['Order']['clientpo'],'readonly')); ?>
                       	<span class="help-block"><br></span>
                </div>
                <div class="col-lg-6 date date-picker" data-date-format="dd-mm-yyyy"">

                  <label class="control-label">P.O. Date</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <?php echo $this->Form->input('date', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'podate ','type'=>'text','value'=>date('d-m-Y',strtotime($order_item[0]['Invoice']['invoicedate'])))); ?>
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
            	 <div class="col-lg-6">
                	<label class="control-label">Country Of Origin Goods</label>
                	<?php echo $this->Form->input('fromcountry', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$order_item[0]['Invoice']['fromcountry'],'readonly')); ?>
                       	<span class="help-block"><br></span>
                </div>
                <div class="col-lg-6">
                	<label class="control-label">Country Of final Destination</label>
                    <?php echo $this->Form->input('tocountry', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$order_item[0]['Invoice']['tocountry'],'readonly')); ?>
                        	<span class="help-block"><br></span>
                </div>
            </div>
          </div>
			    <!--/row-->
			<div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                    	<label class="control-label">Consignee  (If Other Than Consignee)</label>
                    	<?php echo $this->Form->input('consignee', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'consigne','type'=>'text','value'=>$order_item[0]['Order']['Client']['company_name'],'readonly')); ?>
                           	<span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                    	<label class="control-label"> Consignee Address</label>
                        <?php echo $this->Form->input('address', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'address','type'=>'textarea','value'=>$order_item[0]['Order']['Client']['address1'],'rows'=>2,'cols'=>2,'readonly')); ?>
                            	<span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                	 <div class="col-lg-12">
                    	<label class="control-label">Buyer's (If Other Than Consignee)</label>
                    	<?php echo $this->Form->input('buyers', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'buyer','type'=>'text','value'=>$order_item[0]['Invoice']['buyers'],'readonly')); ?>
                           	<span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                    	<label class="control-label">IEC CODE NO.</label>
                        <?php echo $this->Form->input('ifscno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'ifcs','type'=>'text','value'=>$order_item[0]['Invoice']['ifscno'],'readonly')); ?>
                            	<span class="help-block"><br></span>
                    </div>
                </div>
      </div>
			<!--/row-->
			<div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                    	<label class="control-label">PRE CARRIEGE BY</label>
                    	<?php echo $this->Form->input('carriageby', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'precarriege','type'=>'text','value'=>$order_item[0]['Invoice']['carriageby'],'readonly')); ?>
                           	<span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                    	<label class="control-label"> Place of Receipt By Pre-Carrier</label>
                        <?php echo $this->Form->input('receiptplace', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'receiptplace','type'=>'text','value'=>$order_item[0]['Invoice']['receiptplace'],'readonly')); ?>
                            	<span class="help-block"><br></span>
                    </div>
                </div>

                <div class="col-md-6">
                	 <div class="col-lg-12">
                    	<label class="control-label">Other Reference</label>
                    	<?php echo $this->Form->input('otherref', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'otherref','type'=>'textarea','rows'=>2,'cols'=>2,'value'=>$order_item[0]['Invoice']['otherref'],'readonly')); ?>
                           	<span class="help-block"><br></span>
                    </div>
                </div>
      </div>
			<!--/row-->

      			<div class="row separatordiv">
              <div class="col-md-6">
                <div class="col-lg-6">
                  <label class="control-label">Vesse Flight no</label>
                  <?php echo $this->Form->input('flightno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'flightno','type'=>'text','value'=>$order_item[0]['Invoice']['flightno'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
                <div class="col-lg-6">
                  <label class="control-label"> Port of Loading</label>
                  <?php echo $this->Form->input('loadingport', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'loadingport','type'=>'text','value'=>$order_item[0]['Invoice']['loadingport'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
              </div>                
              <div class="col-md-6">
              	<div class="col-lg-12">
                  <label class="control-label">Terms of Delivery</label>
                  <?php echo $this->Form->input('tod', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'tod','type'=>'text','value'=>$order_item[0]['Invoice']['tod'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
              </div>
            </div>
            <!--/row-->      
			      <div class="row separatordiv">
              <div class="col-md-6">
                <div class="col-lg-6">
                	<label class="control-label">Port of Discharge</label>
                	<?php echo $this->Form->input('dischargeport', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'portofdischarge','type'=>'text','value'=>$order_item[0]['Invoice']['dischargeport'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
                <div class="col-lg-6">
                	<label class="control-label">Place of Delivery</label>
                  <?php echo $this->Form->input('deliveryplace', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$order_item[0]['Invoice']['deliveryplace'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
              </div>
              
              <div class="col-md-6">
              	<div class="col-lg-12">
                  <label class="control-label">Terms of Payment</label>
                  <?php echo $this->Form->input('top', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$order_item[0]['Invoice']['top'],'readonly')); ?>
                  <span class="help-block"><br></span>
                </div>
              </div>
            </div>
            <!--/row -->

            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                    	<label class="control-label">Container no</label>
                    	<?php echo $this->Form->input('containerno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'portofdischarge','type'=>'text','value'=>$order_item[0]['Invoice']['containerno'],'readonly')); ?>
                           	<span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                	 <div class="col-lg-12">
                    	<label class="control-label">HS Code</label>

                      <?php $selected = $order_item[0]['Invoice']['hscodeid'];?>
                    	<?php echo $this->Form->input('hscodeid', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'select','options'=>$hscodes,'selected'=>$selected,'empty' => 'Select HSCODE')); ?>
                           	<span class="help-block"><br></span>
                    </div>
            </div>
      </div>
            <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
				<thead class="flip-content portlet box green">
					<tr>
						<th width="12%">Marks & No</th>
						<th width="15%">No of Kind Packages</th>
						<th width="8%">STANDARD</th>
						<th width="12%">MATERIAL<br>GRADE</th>
						<th width="30%" colspan="1">GOOD DESCRIPTION</th>
						<th width="8%">Pcs</th>
            <th width="8%">Quantity</th>
						<th width="6%" id="qty_lbl"> Exempted</th>            
						<th width="15%" id="prc_lbl">Net Weight<br>/MTR <?php echo $order_item[0]['Order']['Price']['sign'];?></th>
						<th width="15%" id="amount_lbl">Gross Weight<br> <?php echo $order_item[0]['Order']['Price']['sign'];?></th>
					</tr>										   
                </thead>
				<tbody>
        <?php  
                        $invoices = $order_item[0]['Order']['Orderitem'];
                        $in_items = $order_item[0]['Invoiceitem'];
                      //  pr($in_items);
                        $i =0;
                       foreach($invoices as $key => $v) {  ++$key; ?>
					<tr class="addmore<?php echo $key;?>">
                         <?php echo $this->Form->input('proitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id']));?>
                           <?php echo $this->Form->input('invoiceitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'itemid'.$key,'value'=>$in_items[$i]['id'],'name'=>'data[Packinglist][item]['.$key.'][invoiceitemid]'));?>
                       
                        <td> 
                            <?php echo $this->Form->input('bundleno', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'bundleno'.$key,'value'=>$in_items[$i]['bundleno']));?>
                        </td>
						<td> 
                            <?php echo $this->Form->input('productname', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$v['productname']));?>
						</td>
						<td> 
						    <?php echo $this->Form->input('standard_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$v['Standard']['standard_name']));?>
						</td>
						<td> 
                             <?php echo $this->Form->input('grade_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$v['Grade']['grade_name']));?>
						</td>
                        <?php $total_length = $v['length'] * 1000; ?>
						<td> 
                        <?php echo $this->Form->input('size_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'value'=>$v['Size']['gdmm'].' X '.$v['length']));?>
						</td>
						<td style="white-space:nowrap;">
						  <?php echo $this->Form->input('pcs', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'length1','value'=>$in_items[$i]['pcs']));
						  ?>
						</td>
						<td> 
						  <?php echo $this->Form->input('qty_mtr', array('class'=>'form-control qty','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>round($in_items[$i]['qty_mtr'],3)));?>
						</td>

            <td style="white-space:nowrap;">
              <?php echo $this->Form->input('exempted', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'heat1','value'=>round($in_items[$i]['exempted'],3))); ++$i;
              ?>
            </td>
						<td> 
						  <?php echo $this->Form->input('netweight', array('class'=>'form-control netweight','type'=>'text','label'=>false,'required'=>true,'id'=>'netweight'.$key,'name'=>'data[Packinglist][item]['.$key.'][netweight]'));?>
						</td>
						<td>
						<?php echo $this->Form->input('grossweight', array('class'=>'form-control grossweight','label'=>false,'type'=>'text','required'=>true,'id'=>'grossweight'.$key,'name'=>'data[Packinglist][item]['.$key.'][grossweight]'));
						?>
						</td>
					 
					</tr>      
          <?php  } ?>
					<tr id="1">    
				       
     				   <td colspan="5"></td>
     				   <td id="total_lbl"> Total </td>
     				   <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>round($order_item[0]['Invoice']['totalqty'],3)));
						?></td>
						<td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>$order_item[0]['Invoice']['totalexempted']));
						?></td>	
						<td><?php echo $this->Form->input('totalnetweight', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'totalnet','readonly'));
            ?></td>	
						<td><?php echo $this->Form->input('totalgrossweight', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'totalgross','readonly'));
						?></td>	
				     </tr>
				   

				</tbody>
			</table>
		      </div>
		<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3">Total Packages<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('totalpackage', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Total Net Weight<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('netweight', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Total Gross Weight<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('grossnet', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3"> Date<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('date', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
							 <span class="help-block"><br></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3"> Signature<span class="required" aria-required="true"> * </span></label>
						<div class="col-md-9">
							<img src="<?php echo ROOT_PATH.'uploads/signature/'.$setting['Setting']['signature'];?>" width="200">
							 <span class="help-block"><br></span>
						</div>
					</div>
				</div>
            </div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
							<?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'packinglists'),array('class' => 'btn default')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6"> </div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->

<script type="text/javascript">
/* Use  : Get Price of Amount ex $,& */
$(document).ready(
  function() {
   $(document).on('change', '.price',
      function(){
        var newPrice = $('option:selected',this).text();
        if(newPrice != "Select Your Price") 
         {
       		$("#amount_lbl").html('AMOUNT '+newPrice);
       		$("#prc_lbl").html('PRICE/MTR '+newPrice);
        	$("#total_lbl").html('Total '+newPrice);
      	}
  });
     });


/*Use : Get grade name to related Standard via ajax*/
$(document).on('change', '.std', function () {
		 var id = $(this).val();
		 var id1 = $(this).attr('id');
		 var grade = id1.substr(id1.length - 1);
		 $("#gd_mm"+grade).val('');
		 $("#gd_mtr"+grade).val('');
		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getGrades/'+id,
			type: 'POST',
			cache: false,
		   success: function (data) {
		$('#grade_id'+grade).html(data);
	}

 });
});
$(document).on('change', '.procat', function () {
		 var pro_value = $(this).val();
		 var pro_id = $(this).attr('id');
		 var prod = pro_id.substr(pro_id.length - 1);

		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getStandards/'+pro_value,
			type: 'POST',
			cache: false,
		   success: function (data) {
		$('#standard_id'+prod).html(data);
	}

 });
});

 $(document).on('change', '.grd', function () {
		 var id = $(this).val();
		 var gd = $(this).attr('id');
		 var mtr = gd.substr(gd.length - 1);
	  
});

 /*Use : Get goods description MTR from related to goods description MM via Ajax*/
 $(document).on('change', '.size', function () {
		
		 var id = $(this).val();
		 var id1 = $(this).attr('id');
		 var size = id1.substr(id1.length - 1);
		 $("#gd_mtr"+size).val('');
		 $.ajax({
			url: '<?php echo WEBSITE_PATH;?>proformainvoices/getnb/'+id,
			type: 'POST',
			cache: false,
		   success: function (data) {
		$('#gd_mtr'+size).val(data);
	}

 });
});

 /*Use: Count final Total and amout from quantity and price.*/
$(document).on('keyup', '.netweight', function() {
	var gd = $(this).attr('id');
	var mtr = gd.substr(gd.length - 1);
	var netweight = $('#netweight'+mtr).val();
	var grs = parseFloat(netweight) + 5;
  var gross = grs.toFixed(3);
	 if(!isNaN(gross) && gross.length != 0) {
	 	var amount = $('#grossweight'+mtr).val(gross);
	 }
	
    var sum = 0;
   $('.netweight').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
				$('#totalnet').val(sum);
        }

    });
   var total = 0;
    $('.grossweight').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                total += parseFloat(this.value);
        $('#totalgross').val(total);
        }

    });
});   

/*Use : Add more Row Functionality.*/
$(document).on('click', '.addno', function () {
	  var no = $('#up').val();
	  console.log(no);
	  var nxt = parseInt(no) + 1;
	 

		   var upno = $('#up').val(nxt);
		   console.log(upno);	
		   var remmno = $('#remno').val();
		   var remnextno = parseInt(remmno) + 1;
		   var t = $('#remno').val(remnextno);

	   	   $(".comboselection").select2();
	  
});

 /* Use : Delete extra row from list. */
$(document).on('click', '.deleteno', function () {
		
		  var upno = $('#up').val();
		  var rm = $('#remno').val();

		    var value=$(this).closest('tr').children('td:first').data("id");
 //  alert(value);    
		  $('#expense_table tr:last-child td:first-child input').attr("name");		 
	  if(upno != '1')
	  {
		var myno = $(this).attr('mydelnum');
		$(this).parent().parent().remove();
		var remainingno = parseInt(upno) - 1;
		var upno = $('#up').val(remainingno);
		console.log(remainingno);
	  }


	/* do delete stuff here */
});

/*Start Fetch client name with autosuggestion*/
            $(".bill_to").select2({
                width: "off",
                ajax: {
                    url: "<?php echo WEBSITE_PATH;?>proformainvoices/getclients/",
                    dataType: 'json',
                    delay: 50,
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
            });
            /*End Fetch client name with autosuggestion*/
            /*Fetch address based on selected client*/
            $(document.body).on("change",".bill_to",function(){
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getclientdetail/',
                    data: 'id='+(this).value,
                    type: 'POST',
                    cache: false,
                    success: function (data) {
                        var datares = jQuery.parseJSON(data);                            
                        $('#bill_address').html(datares.client.address1);
                        /*If checkbox checked than set ship_to and its address*/
                        if($('#sameas').is(':checked')) {
                            $('#ship_to').val($('#bill_to option:selected').html());
                            $('#ship_address').val($('#bill_address').text());
                        }
                    }
                });    
            });
            /*If checkbox checked than set ship_to and its address*/
            $('#sameas').click(function () {
                if($('#sameas').is(':checked')) {
                    $('#ship_to').val($('#bill_to option:selected').html());
                    $('#ship_address').val($('#bill_address').text());
                }
            });
</script>