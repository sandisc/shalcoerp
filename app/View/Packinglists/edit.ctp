<h3 class="page-title"> <?php echo $mainTitle;?></h3>
<!-- BEGIN FORM-->
<?php echo $this->Form->create('Packinglist',array('type' => 'file')); 
    echo $this->Form->input('id',array('type'=> 'hidden'));
  echo $this->Form->input('invoiceid',array('type'=> 'hidden','value'=>$id));?>                 
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
        <div class="tools"><a class="collapse" href="javascript:;" data-original-title="" title=""> </a></div>
    </div>
    <?php  $settings = $this->Session->read('setting_data');/*Read setting table's data*/?>
    <div class="portlet-body form">
        <div class="form-body"> 
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                      <label class="control-label">Exporter</label>
                      <div class="input textarea">SHALCO INDUSTRIES PVT. LTD</div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Factory address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['factory_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Office Address</label>
                      <div class="input textarea"><?php echo $settings['Setting']['office_address']; ?></div>
                      <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Packing NO.</label>
                        <?php echo $this->Form->input('packingno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'invoice_no','type'=>'text','value'=>$packing_data[0]['Packinglist']['packingno'],'readonly')); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6 date date-picker" data-date-format="dd-mm-yyyy"">

                        <label class="control-label">Packing Date</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php echo $this->Form->input('invoicedate', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'invoice_no','type'=>'text','value'=>date('d-m-Y',strtotime($packing_data[0]['Invoice']['invoicedate'])))); ?>
                      </div>
                    </div>

                </div>
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">P.O NO.</label>
                        <?php echo $this->Form->input('clientpo', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'pono','type'=>'text','value'=>$packing_data[0]['Invoice']['Order']['clientpo'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6 date date-picker" data-date-format="dd-mm-yyyy"">

                      <label class="control-label">P.O. Date</label>
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <?php echo $this->Form->input('date', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'podate ','type'=>'text','value'=>date('d-m-Y',strtotime($packing_data[0]['Invoice']['invoicedate'])))); ?>
                      </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-6">
                        <label class="control-label">Country Of Origin Goods</label>
                        <?php echo $this->Form->input('fromcountry', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$packing_data[0]['Invoice']['fromcountry'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Country Of final Destination</label>
                        <?php echo $this->Form->input('tocountry', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$packing_data[0]['Invoice']['tocountry'])); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
      </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Consignee (If Other Than Consignee)</label>
                        <?php echo $this->Form->input('consignee', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'consigne','type'=>'text','value'=>$packing_data[0]['Invoice']['Order']['Client']['company_name'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Consignee Address</label>
                        <?php echo $this->Form->input('address', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'address','type'=>'textarea','value'=>$packing_data[0]['Invoice']['Order']['Client']['address1'],'rows'=>2,'cols'=>2,'readonly')); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Buyer's (If Other Than Consignee)</label>
                        <?php echo $this->Form->input('buyers', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'buyer','type'=>'text','value'=>$packing_data[0]['Invoice']['buyers'],'readonly')); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-12">
                        <label class="control-label">IES CODE NO.</label>
                        <?php echo $this->Form->input('ifscno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'ifcs','type'=>'text','value'=>$packing_data[0]['Invoice']['ifscno'])); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
      </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">PRE CARRIEGE BY</label>
                        <?php echo $this->Form->input('carriageby', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'precarriege','type'=>'text','value'=>$packing_data[0]['Invoice']['carriageby'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Place of Receipt By Pre-Carrier</label>
                        <?php echo $this->Form->input('receiptplace', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'receiptplace','type'=>'text','value'=>$packing_data[0]['Invoice']['receiptplace'])); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>

                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Other Reference</label>
                        <?php echo $this->Form->input('otherref', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'otherref','type'=>'textarea','rows'=>2,'cols'=>2,'value'=>$packing_data[0]['Invoice']['otherref'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                </div>
      </div>
            <!--/row-->
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Vesse Flight no</label>
                        <?php echo $this->Form->input('flightno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'flightno','type'=>'text','value'=>$packing_data[0]['Invoice']['flightno'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label"> Port of Loading</label>
                        <?php echo $this->Form->input('loadingport', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'loadingport','type'=>'text','value'=>$packing_data[0]['Invoice']['loadingport'])); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Delivery</label>
                        <?php echo $this->Form->input('tod', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'tod','type'=>'text','value'=>$packing_data[0]['Invoice']['Order']['tod'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                </div>
      </div>
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-6">
                        <label class="control-label">Port of Discharge</label>
                        <?php echo $this->Form->input('dischargeport', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'portofdischarge','type'=>'text','value'=>$packing_data[0]['Invoice']['dischargeport'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">Place of Delivery</label>
                        <?php echo $this->Form->input('deliveryplace', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$packing_data[0]['Invoice']['deliveryplace'])); ?>
                                <span class="help-block"><br></span>
                    </div>
                </div>
                
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">Terms of Payment</label>
                        <?php echo $this->Form->input('top', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'text','value'=>$packing_data[0]['Invoice']['Order']['top'])); ?>
                            <span class="help-block"><br></span>
                    </div>
            </div>
         </div>
            <div class="row separatordiv">
                <div class="col-md-6">
                    <div class="col-lg-12">
                        <label class="control-label">Container no</label>
                        <?php echo $this->Form->input('containerno', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'portofdischarge','type'=>'text','value'=>$packing_data[0]['Invoice']['containerno'])); ?>
                            <span class="help-block"><br></span>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="col-lg-12">
                        <label class="control-label">HS Code</label>

                      <?php $selected = $packing_data[0]['Invoice']['hscodeid'];?>
                        <?php echo $this->Form->input('hscodeid', array('class'=>'form-control','label'=>false,'required'=>true,'id'=>'performa_no','type'=>'select','options'=>$hscodes,'selected'=>$selected,'empty' => 'Select HSCODE')); ?>
                            <span class="help-block"><br></span>
                    </div>
            </div>
      </div>
            <!--/row-->    
           <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                <thead class="flip-content portlet box green">
                    <tr>
                        <th width="12%">Marks & No</th>
                        <th width="15%">No of Kind Packages</th>
                        <th width="8%">STANDARD</th>
                        <th width="12%">MATERIAL<br>GRADE</th>
                        <th width="30%" colspan="1">GOOD DESCRIPTION</th>
                        <th width="8%">Pcs</th>
                        <th width="8%">Quantity</th>
                        <th width="6%" id="qty_lbl"> Exempted</th>            
                        <th width="15%" id="prc_lbl">Net Weight<br>/MTR <?php echo $packing_data[0]['Invoice']['Order']['Price']['sign'];?></th>
                        <th width="15%" id="amount_lbl">Gross Weight<br> <?php echo $packing_data[0]['Invoice']['Order']['Price']['sign'];?></th>
                    </tr>                                          
                </thead>
                <tbody>
        <?php  
                        $packings_items = $packing_data[0]['Invoice']['Invoiceitem'];
                         $packings = $packing_data[0]['Invoice']['Order']['Orderitem'];
                       
                        $in_items = $packing_data[0]['Packingitem'];
                        $i =0;
                       foreach($packings_items as $key => $v) {  ++$key;    if(isset($in_items[$i]['id'])){ $in_id = $in_items[$i]['id']; }else { $in_id = '';} ?> 
                    <tr class="addmore<?php echo $key;?>">
                     <?php echo $this->Form->input('id', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'id'.$key,'name'=>'data[Packinglist][item]['.$key.'][id]','value'=>$in_id));?>
                         <?php echo $this->Form->input('invoiceitemid', array('class'=>'form-control ','type'=>'hidden','label'=>false,'id'=>'proitemid'.$key,'value'=>$v['id'],'name'=>'data[Packinglist][item]['.$key.'][invoiceitemid]'));?>
                        <td> 
                            <?php echo $this->Form->input('bundleno', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'bundleno'.$key,'value'=>$v['bundleno']));?>
                        </td>
                        <td> 
                            <?php echo $this->Form->input('productname', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['productname']));?>
                        </td>
                        <td> 
                            <?php echo $this->Form->input('standard_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['Standard']['standard_name']));?>
                        </td>
                        <td> 
                             <?php echo $this->Form->input('grade_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'id'=>'productname'.$key,'value'=>$packings[$i]['Grade']['grade_name']));?>
                        </td>
                        <?php $total_length = $packings[$i]['length'] * 1000; ?>
                        <td> 
                        <?php echo $this->Form->input('size_id', array('class'=>'form-control ','type'=>'text','label'=>false,'required'=>true,'value'=>$packings[$i]['Size']['gdmm'].' X '.$packings[$i]['length']$total_length));?>
                        </td>
                        <td style="white-space:nowrap;">
                          <?php echo $this->Form->input('pcs', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'length1','value'=>$v['pcs']));
                          ?>
                        </td>
                        <td> 
                          <?php echo $this->Form->input('qty_mtr', array('class'=>'form-control qty','type'=>'text','label'=>false,'required'=>true,'id'=>'qty'.$key,'value'=>round($v['qty_mtr'],3)));?>
                        </td>

            <td style="white-space:nowrap;">
              <?php echo $this->Form->input('exempted', array('class'=>'form-control','type'=>'text','label'=>false,'required'=>true,'id'=>'heat1','value'=>round($v['exempted'],3)));
              ?>
            </td>
                        <td> <?php if(isset($in_items[$i]['netweight'])){ $weight = $in_items[$i]['netweight']; }else { $weight = '';} ?>
                          <?php echo $this->Form->input('netweight', array('class'=>'form-control netweight','type'=>'text','label'=>false,'required'=>true,'id'=>'netweight'.$key,'name'=>'data[Packinglist][item]['.$key.'][netweight]','value'=>round($weight,3)));?>
                        </td>
                        <td> <?php if(isset($in_items[$i]['grossweight'])){ $gross = $in_items[$i]['grossweight']; }else { $gross = '';} ?>
                        <?php echo $this->Form->input('grossweight', array('class'=>'form-control grossweight','label'=>false,'type'=>'text','required'=>true,'id'=>'grossweight'.$key,'name'=>'data[Packinglist][item]['.$key.'][grossweight]','value'=>round($gross,3))); ++$i;
                        ?>
                        </td>
                     
                    </tr>      
          <?php  } ?>
                    <tr id="1">    
                       
                       <td colspan="5"></td>
                       <td id="total_lbl"> Total </td>
                       <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>$packing_data[0]['Invoice']['totalqty']));
                        ?></td>
                        <td><?php echo $this->Form->input('total', array('class'=>'form-control total','label'=>false,'type'=>'text','required'=>true,'id'=>'total','readonly','value'=>$packing_data[0]['Invoice']['totalexempted']));
                        ?></td> 
                        <td><?php echo $this->Form->input('totalnetweight', array('class'=>'form-control','label'=>false,'type'=>'text','required'=>true,'id'=>'totalnet','readonly','value'=>$packing_data[0]['Packinglist']['totalnetweight']));
            ?></td> 
                        <td><?php echo $this->Form->input('totalgrossweight', array('class'=>'form-control grossweight','label'=>false,'type'=>'text','required'=>true,'id'=>'totalgross'.$key,'readonly',
                            'value'=>$packing_data[0]['Packinglist']['totalgrossweight']));
                        ?></td> 
                     </tr>
                   

                </tbody>
            </table>
              </div>
        <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Packages<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('totalpackage', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$packing_data[0]['Invoice']['totalpackage'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Total Net Weight<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('netweight', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$packing_data[0]['Invoice']['netweight'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Gross Weight<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('grossnet', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control','id'=>'bank_id','value'=>$packing_data[0]['Invoice']['grossweight'])); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"> Date<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <?php echo $this->Form->input('date', array('label'=>false,'type'=>'text','required'=>true,'class'=>'form-control')); ?>
                             <span class="help-block"><br></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"> Signature<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-9">
                            <img src="<?php echo ROOT_PATH.'uploads/signature/'.$settings['Setting']['signature'];?>" width="200">
                             <span class="help-block"><br></span>
                        </div>
                    </div>
                </div>
            </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn green" type="submit">Submit</button>
                            <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'packinglists'),array('class' => 'btn default')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>      
<!-- END FORM-->

<script type="text/javascript">
/* Use  : Get Price of Amount ex $,& */
$(document).ready(
  function() {
   $(document).on('change', '.price',
      function(){
        var newPrice = $('option:selected',this).text();
        if(newPrice != "Select Your Price") 
         {
            $("#amount_lbl").html('AMOUNT '+newPrice);
            $("#prc_lbl").html('PRICE/MTR '+newPrice);
            $("#total_lbl").html('Total '+newPrice);
        }
  });
     });


 $(document).on('change', '.grd', function () {
         var id = $(this).val();
         var gd = $(this).attr('id');
         var mtr = gd.substr(gd.length - 1);
      
});

 /*Use : Get goods description MTR from related to goods description MM via Ajax*/
 $(document).on('change', '.size', function () {
        
         var id = $(this).val();
         var id1 = $(this).attr('id');
         var size = id1.substr(id1.length - 1);
         $("#gd_mtr"+size).val('');
         $.ajax({
            url: '<?php echo WEBSITE_PATH;?>proformainvoices/getnb/'+id,
            type: 'POST',
            cache: false,
           success: function (data) {
        $('#gd_mtr'+size).val(data);
    }

 });
});

 /*Use: Count final Total and amout from quantity and price.*/
$(document).on('keyup', '.netweight', function() {
    var gd = $(this).attr('id');
    var mtr = gd.substr(gd.length - 1);
    var netweight = $('#netweight'+mtr).val();
    var grs = parseFloat(netweight) + 5;
    var gross = grs.toFixed(3);
     if(!isNaN(gross) && gross.length != 0) {
        var amount = $('#grossweight'+mtr).val(gross);
     }
    
    var sum = 0;
   $('.netweight').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
                $('#totalnet').val(sum);
        }

    });
   var total = 0;
    $('.grossweight').each(function() {
       if(!isNaN(this.value) && this.value.length != 0) {
                total += parseFloat(this.value);
        $('#totalgross').val(total);
        }

    });
});   
/*Use : Add more Row Functionality.*/
$(document).on('click', '.addno', function () {
      var no = $('#up').val();
      console.log(no);
      var nxt = parseInt(no) + 1;
     

           var upno = $('#up').val(nxt);
           console.log(upno);   
           var remmno = $('#remno').val();
           var remnextno = parseInt(remmno) + 1;
           var t = $('#remno').val(remnextno);

           $(".comboselection").select2();
      
});

 /* Use : Delete extra row from list. */
$(document).on('click', '.deleteno', function () {
        
          var upno = $('#up').val();
          var rm = $('#remno').val();

            var value=$(this).closest('tr').children('td:first').data("id");
 //  alert(value);    
          $('#expense_table tr:last-child td:first-child input').attr("name");       
      if(upno != '1')
      {
        var myno = $(this).attr('mydelnum');
        $(this).parent().parent().remove();
        var remainingno = parseInt(upno) - 1;
        var upno = $('#up').val(remainingno);
        console.log(remainingno);
      }


    /* do delete stuff here */
});

/*Start Fetch client name with autosuggestion*/
            $(".bill_to").select2({
                width: "off",
                ajax: {
                    url: "<?php echo WEBSITE_PATH;?>proformainvoices/getclients/",
                    dataType: 'json',
                    delay: 50,
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
            });
            /*End Fetch client name with autosuggestion*/
            /*Fetch address based on selected client*/
            $(document.body).on("change",".bill_to",function(){
                $.ajax({
                    url: '<?php echo WEBSITE_PATH;?>proformainvoices/getclientdetail/',
                    data: 'id='+(this).value,
                    type: 'POST',
                    cache: false,
                    success: function (data) {
                        var datares = jQuery.parseJSON(data);                            
                        $('#bill_address').html(datares.client.address1);
                        /*If checkbox checked than set ship_to and its address*/
                        if($('#sameas').is(':checked')) {
                            $('#ship_to').val($('#bill_to option:selected').html());
                            $('#ship_address').val($('#bill_address').text());
                        }
                    }
                });    
            });
            /*If checkbox checked than set ship_to and its address*/
            $('#sameas').click(function () {
                if($('#sameas').is(':checked')) {
                    $('#ship_to').val($('#bill_to option:selected').html());
                    $('#ship_address').val($('#bill_address').text());
                }
            });
</script>