<?php
  /* 
   * Paging
   */

  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  
 
    foreach ($licence as $licences) {
      if($licences['Licence']['is_extended'] == 0){
        $ext_exp = '<button class="btn blue btn-outline test'.$licences['Licence']['id'].'" type="button" id="btn_ext" data-advace = '.$licences['Licence']['advancelicenceno'].' data-date='.$licences['Licence']['expiry_date'].' data-lid='.$licences['Licence']['id'].'>Extension</button>';
      }else {
        $ext_exp = '';
      }
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$licences['Licence']['id'].'">',
      $licences['Licence']['advancelicenceno'],
      $this->requestAction('App/date_ymd_to_dmy/'.strtotime($licences['Licence']['issue_date'])),
      $licences['Licence']['validity'],
      $this->requestAction('App/date_ymd_to_dmy/'.strtotime($licences['Licence']['expiry_date'])),
      $licences['Hscode']['hscode'],
      $this->requestAction('App/date_ymdhis_to_dmyhis/'.strtotime($licences['Licence']['modified'])),
      $licences['User']['first_name']." ".$licences['User']['last_name'],
       '<a href="'.WEBSITE_PATH.'licences/view/'.base64_encode($licences['Licence']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-eye-open" title="View"></span></a>
       <a href="'.WEBSITE_PATH.'licences/add/'.base64_encode($licences['Licence']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'licences/delete/'.base64_encode($licences['Licence']['id']).'" class="btn btn-danger" title="Delete" id="delete" data-confirm="Are you sure to delete this size?"><span class="glyphicon glyphicon-trash"></span></a>'.$ext_exp
      );
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>