 <?php
  $html = '<style>
        table {float: none;margin: 0 auto;width: 100%;border-spacing: 0;}
        .flip-scroll table, .table td .img-responsive {width: 100%;}
        .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border: 1px solid #e7ecf1
        }
        .table {margin-bottom: 20px;width: 100%; border: 1px solid #e7ecf1}
        .numalign{text-align:right; padding-right: 5px!important;margin-right:5px;}
        .itemdata{}
        .table th{font-size:12px;}
        .table td{font-size:10px;}
        </style>'; 
    $gradetagcount = sizeof($licencedata['Licenceitem']); /*number of gradetag associated with licence*/
    $totalcolumn = 5 + 2 * $gradetagcount;
    $gradetag_column = ''; /*grade tag associated with licence*/
    $allowedqty_column = ''; /*allowed quantity of grade tag associated with licence*/
    $blank_cell = ''; /*Blank cell for deign purpose*/
    $licenceitemindex = 1;
    $gradetag = array();
    /*Start Licence item associated grade tag and its allowed quantity*/
    foreach ($licencedata['Licenceitem'] as $itemvalue) {
      $gradetag_column .= '<th>'.$itemvalue['Gradetag']['gradetagname'].'</th>';
      $allowedqty_column .= '<th>'.$itemvalue['im_qty'].'</th>';
      $blank_cell .= '<th></th>';
      $gradetag[$licenceitemindex]['im_qty'] = $itemvalue['im_qty']; /*allowed quantity of particular grade*/
      $gradetag[$licenceitemindex]['licenceitemid'] = $itemvalue['id']; /*Licence item id,, which will be usefull to get same means this item id related data from import item array*/
      $gradetag[$licenceitemindex]['total_import_qty'] = 0.000; /*total import qty which is imported, by defaault we took it 0.00 for sum*/
      $gradetag[$licenceitemindex]['total_import_dollar'] = 0.000; /*total import dollar val which is imported, by defaault we took it 0.00 for sum*/
      $licenceitemindex++;
    }
    /*End Licence item associated grade tag and its allowed quantity*/

    /*search in array for to get licenceitem related record from licenceimport item array*/
    function searcharray($value, $key, $array) {
      foreach ($array as $k => $val) { 
        if ($val[$key] == $value) { 
          return $val;
        }
      }
      return null;
    }

    /*Start fetching licence import and its import item data*/
    $licenceimportdata = '';
    $licenceimport = '';

    foreach ($licencedata['Licenceimport'] as $importvalue) {
      $licenceimport = '<tr classs="itemdata">
        <td class="numalign">'.$importvalue['boeno'].'</td>
        <td class="numalign">'.$this->requestAction('App/date_ymd_to_dmy_licence/'.$importvalue['boedate']).'</td>
        <td class="numalign">'.$importvalue['invoiceid'].'</td>
        <td class="numalign">'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($importvalue['invoicedate'])).'</td>
        <td>'.$importvalue['Client']['company_name'].'</td>';
      $import_qty = ''; /*Import quantity data*/
      $dollar_val = ''; /*Dollar value data*/
      foreach ($gradetag as $key=>$gradetagvalue) {
        $results = searcharray($gradetagvalue['licenceitemid'],'licenceitemid', $importvalue['Licenceimportitem']);
        if(!empty($results)){
          $import_qty .= '<td class="numalign">'.$results['importqty'].'</td>';
          $dollar_val .= '<td class="numalign">'.$results['dollarvalue'].'</td>';
          $gradetag[$key]['total_import_qty'] += $results['importqty']; /*sum of each import qauntity (current row plus previous row)*/
          $gradetag[$key]['total_import_dollar'] += $results['dollarvalue'];
        }else{
          $import_qty .= '<td></td>';
          $dollar_val .= '<td></td>';
        }
      }
      $licenceimportdata .= $licenceimport.''.$import_qty.''.$dollar_val.'</tr>'; 
    }
    /*End fetching licence import and its import item data*/

      $final_import_qty = '';
      $final_import_dollar = '';
      $final_remaining_qty = '';
      $final_remaining_dollar = '';
      foreach ($gradetag as $key=>$gradetagvalue) {
          $rem_qty = $gradetagvalue['im_qty'] - $gradetagvalue['total_import_qty'];
          $rem_dollar = 0.000 - $gradetagvalue['total_import_dollar'];
          $final_import_qty .= '<td class="numalign">'.decimalQty($gradetagvalue['total_import_qty']).'</td>';
          $final_import_dollar .= '<td class="numalign">'.decimalPrice($gradetagvalue['total_import_dollar']).'</td>';
          $final_remaining_qty .= '<td class="numalign">'.decimalQty($rem_qty).'</td>';
          $final_remaining_dollar .= '<td class="numalign">'.decimalPrice($rem_dollar).'</td>';
      }
  $html .= '<html>
              <head><title>Licence Import</title></head>
              <body>
                <table width="100%" border="1" class="table table-bordered table-striped table-condensed flip-content view_invoice">
                <tbody>
                  <tr>
                    <td colspan="'.$totalcolumn.'"><b>ADVANCE LICENCE : '.$licencedata['Licence']['advancelicenceno'].'   &nbsp;&nbsp;&nbsp; ISSUE DATE : '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($licencedata['Licence']['issue_date'])).'</b></td>
                  </tr>';
                  if($licencedata['Licence']['is_extended'] == 1){
                    $html .= '<tr>
                        <td colspan="'.$totalcolumn.'">IMPORT LAST DATE : '.$licencedata['Licence']['ex_validity'].' MONTHS  '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($licencedata['Licence']['ex_expirydate'])).'</td>
                      </tr>';
                  }else{ 
                    $html .= '<tr>
                        <td colspan="'.$totalcolumn.'">IMPORT LAST DATE : '.$licencedata['Licence']['validity'].' MONTHS  '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($licencedata['Licence']['expiry_date'])).'</td>
                      </tr>';
                  }

        $html .= '<tr><td colspan="'.$totalcolumn.'">&nbsp;</td></tr>
                  <!--<tr>
                    <td colspan="5">IMPORT Qty : 304:236.250 MT,316 405.00000 MT</td>
                  </tr>-->
                  <tr>
                    <th>IMPORT</th>
                    <th colspan="2"> HS Code : '.$licencedata['Hscode']['hscode'].'</th>
                    <th colspan="2"> HS Code Value : '.$licencedata['Hscode']['rate'].'</th>   
                    <th colspan="'.$gradetagcount.'">'.$licencedata['Licence']['advancelicenceno'].' SMLS</th>
                    <th colspan="'.$gradetagcount.'">DOLLAR VALUE</th>
                  </tr>
                  <tr>
                    <th colspan="4"></th><th>GRADE</th>'.$gradetag_column.''.$gradetag_column.'
                  </tr>
                  <tr>
                    <th colspan="4"></th><th>QTY.</th>'.$allowedqty_column.''.$blank_cell.'
                  </tr>
                  <tr>
                    <th>BOE NO</th>
                    <th>BOE Date</th>
                    <th>INVOICE NO</th>
                    <th>INVOICE DATE</th>
                    <th>PARTY NAME</th>
                    '.$blank_cell.'
                    '.$blank_cell.'
                  </tr>';

        $html .= $licenceimportdata;
        $html .= '<tr><td colspan="'.$totalcolumn.'">&nbsp;</td></tr>
                  <tr><td colspan="'.$totalcolumn.'">&nbsp;</td></tr>';
        $html .= '<tr><th colspan="5"> Total Import </th>'.$final_import_qty.''.$final_import_dollar.'</tr>';
        $html .= '<tr><td colspan="'.$totalcolumn.'">&nbsp;</td></tr>';
        $html .= '<tr><th colspan="5"> Balance </th>'.$final_remaining_qty.''.$final_remaining_dollar.'</tr>';
        $html .= '</table>
      </body>
  </html>';              
  echo $html;
?>