<div class="pull-left"><h3 class="page-title"> <?php echo $mainTitle;?></h3></div>
<div class="pull-right">
    <?php echo $this->Html->link(
           '<span title="Edit" class="glyphicon glyphicon-pencil"></span> Edit',
            array(
                'controller'=>'licences',
                'action'=>'add',
                base64_encode($data['Licence']['id'])
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Edit',
                'title' => 'Edit',
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
          /*Set PDF Download link*/
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Licence Export PDF',
            array(
                'controller'=>'licences',
                'action'=>'licenceexportpdf',
                'ext' => 'pdf',
                $data['Licence']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Licence Export PDF',
                'title' => 'Licence Export PDF', 
                'download'=>'Licence Export -'.$data['Licence']['advancelicenceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );   
         /*Set PDF Download link*/
        echo $this->Html->link(
           '<i class="fa fa-file-pdf-o"></i> Licence Import PDF',
            array(
                'controller'=>'licences',
                'action'=>'licenceimportpdf',
                'ext' => 'pdf',
                $data['Licence']['id']
            ),
            array(
                'rel'=>'tooltip',
                'data-placement'=>'left',
                'data-original-title'=>'Licence Import PDF',
                'title' => 'Licence Import PDF', 
                'download'=>'Licence Import -'.$data['Licence']['advancelicenceno'],
                'class'=>'btn blue margin-top-10 margin-bottom-10 margin-right-10 dt-button buttons-print btn dark btn-outline ',
                'escape'=>false  //NOTICE THIS LINE ***************
            )
        );
  ?>
</div>
<div class="clearfix"></div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                    </div>
                </div>
                <div class="portlet-body form metroform">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal" role="form">
                        <div class="form-body">                                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <label class="control-label col-md-3 view-label">Advance Licence No.</label>
                                        <div class="col-md-9 view-data">
                                            <p class="form-control-static"><?php echo h($data['Licence']['advancelicenceno']); ?> </p>
                                        </div>
                                    </div>
                                </div>                                                            
                                <div class="col-md-6">
                                    <div class="">
                                        <label class="control-label col-md-3 view-label">Issue Date</label>
                                        <div class="col-md-3">
                                            <p class="form-control-static"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Licence']['issue_date'])); ?> </p>
                                        </div>
                                        <label class="control-label col-md-3 view-label">Validity</label>
                                        <div class="col-md-2">
                                            <p class="form-control-static"><?php echo h($data['Licence']['validity']); ?> Months</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <label class="control-label col-md-3 view-label">Hscode</label>
                                        <div class="col-md-9 view-data">
                                            <p class="form-control-static"><?php echo h($data['Hscode']['hscode']); ?> </p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="">
                                        <label class="control-label col-md-3 view-label">Expiry Date</label>
                                        <div class="col-md-9 view-data">
                                            <p class="form-control-static"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Licence']['expiry_date'])); ?></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!--/row-->
                            <?php if($data['Licence']['is_extended'] == 1){?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label col-md-3 view-label">Extension Expiry Date</label>
                                        <div class="col-md-9 view-data">
                                            <p class="form-control-static"><?php echo $this->requestAction('App/date_ymd_to_dmy/'.strtotime($data['Licence']['ex_expirydate'])); ?></p>
                                        </div>
                                </div>                                                            
                                <div class="col-md-6">
                                    <div class="">
                                      <label class="control-label col-md-3 view-label">Extension Validity </label>
                                        <div class="col-md-9 view-data">
                                            <p class="form-control-static"><?php echo h($data['Licence']['ex_validity']); ?> Months </p>
                                        </div>  
                                    </div>
                                </div>                                                            
                            </div>
                        <?php } ?>
                            <!--/row-->  
                    <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                        <thead class="flip-content portlet box green">
                          <!-- <tr>
                          <th colspan="2" style="color:red;"><b>Export <span class="glyphicon glyphicon-export" style="color:black;"></span></b></th>
                          <th colspan="1" style="color:red;"><b>Import <span class="glyphicon glyphicon-import" style="color:black;"></span></b></th>
                          </tr> -->
                          <tr>
                            <th width="20%">Grade</th>
                            <th width="20%">Qty / MT (Export) <span class="glyphicon glyphicon-export" style="color:red;"></span></th>
                            <th width="20%">Balance Qty (Export)</th>
                            <th width="20%">QTY / MT (Import) <span class="glyphicon glyphicon-import" style="color:red;"></span></th>
                            <th width="20%">Balance Qty (Import)</th>           
                          </tr>                    
                        </thead>
                        <tbody>
                        <?php 
                            foreach($data['Licenceitem'] as $key => $v) {   ++$key; ?>
                                  <tr class="addmore<?php echo $key;?>">
                                     <td class="numalign"><?php echo $v['Gradetag']['gradetagname'];?></td>
                                     <td class="numalign"><?php echo $v['ex_qty'];?></td>
                                     <td class="numalign"><?php echo $this->requestAction('Licenceexports/ExportGradetagbalanceQty/'.$v['licenceid'].'/'.$v['gradetagid']);?></td>
                                     <td class="numalign"><?php echo $v['im_qty'];?></td>
                                     <td class="numalign"><?php echo $this->requestAction('Licenceimports/ImportGradetagbalanceQty/'.$v['licenceid'].'/'.$v['gradetagid']);?></td>
                                  </tr>
                                <?php  
                            } ?>
                          <tr id="1" class="item_total"> 
                          </tr>
                        </tbody>
                      </table>

                        </div>                                             
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                        <?php echo $this->Html->link('Back',array('action' => 'index','controller' =>'licences'),array('class' => 'btn default')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>