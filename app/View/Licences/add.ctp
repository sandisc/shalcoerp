              <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
              <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                    </div>
                </div>
                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <?php echo $this->Form->create('Licence',array('class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                      echo $this->Form->input('id', array('type'=>'hidden')); 
                      if(isset($this->params['named']['targetid'])){
                          echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                      }?> 
                      <div class="form-body" style="width: 70%!important;">
                        <div class="col-md-12 row"><br/></div>
                        <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="control-label col-md-4"> Licence No<span class="required"> * </span></label>
                                  <div class="col-md-5">
                                    <?php echo $this->Form->input('advancelicenceno', array('class'=>'form-control advancelicenceno','label'=>false));?>
                                    <span class="help-block"><br></span>
                                  </div>
                              </div>
                          </div>                                                         
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="control-label col-md-4">Issue Date<span class="required"> * </span></label>
                                  <div class="col-md-5">
                                  <div class="input-group date date-picker" id="dp3" data-date-format="dd/mm/yyyy">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php 
                                    $issue_date = '';
                                    if(isset($this->request->data['Licence']['issue_date'])){
                                      $issue_date = date('d/m/Y',strtotime($this->request->data['Licence']['issue_date']));
                                    }
                                    echo $this->Form->input('issue_date', array('label'=>false,'class'=>'form-control issue_date','type'=>'text','value' => $issue_date));?>
                                  </div>
                                    <span class="help-block"><br></span>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label col-md-4">HS Code<span class="required" aria-required="true"> * </span></label>
                              <div class="col-md-5">
                                <select id='hscode_id' class='form-control hscodes' name='data[Licence][hscode_id]' >
                                  <option value="">Select Hscode</option>
                                  <?php if(!empty($this->request->data['Licence']['id'])){ 
                                    foreach($hscodes as $hscode){?>
                                      <option value='<?php echo $hscode['id'];?>' data-value='<?php echo $hscode['data-value'];?>' 
                                        <?php if($this->request->data['Licence']['hscode_id'] == $hscode['id']){ ?> selected="selected" <?php } ?>><?php echo $hscode['name'];?></option>
                                          <?php } 
                                      } else {  ?>
                                        <?php foreach($hscodes as $hscode){?>
                                          <option value='<?php echo $hscode['id'];?>' data-value='<?php echo $hscode['data-value'];?>'><?php echo $hscode['name'];?></option>
                                  <?php } } ?>
                                </select>
                                <span class="help-block"><br></span>
                              </div>
                              <div class="col-md-3 checkbox margin0px">
                                <a onclick="addHSCode()" data-target="#addHSCodeModal" data-toggle="modal">Add HS Code</a> 
                              </div>                                                
                            </div>
                          </div>                                                         
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="control-label col-md-4"> Validity<span class="required"> * </span></label>
                                  <div class="col-md-3">
                                    <?php echo $this->Form->input('validity', array('class'=>'form-control validity','label'=>false,'type'=>'text','maxlength'=>'2'));?>
                                    <span class="help-block"><br></span>
                                  </div>
                                  <div class="col-md-2">(Months)</div>
                              </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6"></div>
                          <div class="col-md-6">
                            <div class="form-group">
                                  <label class="control-label col-md-4">Expiry Date<span class="required"> * </span></label>
                                  <div class="col-md-5">
                                    <?php 
                                    $expiry_date = '';
                                    if(isset($this->request->data['Licence']['expiry_date'])){
                                      $expiry_date = date('d/m/Y',strtotime($this->request->data['Licence']['expiry_date']));
                                    }
                                    echo $this->Form->input('expiry_date', array('label'=>false,'class'=>'form-control expiry_date','type'=>'text','readonly','value' => $expiry_date));?>
                                  </div>
                                    <span class="help-block"><br></span>
                              </div>
                            </div>
                        </div>         
                      Note : Don't use same grade tag more than one time for single licence.
                        <div class="table-scrollable"> 
                          <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                            <thead class="flip-content portlet box green">
                              <tr>
                              <th colspan="4" style="color:red;"><b>Export <span class="glyphicon glyphicon-export" style="color:black;"></span></b></th>
                              <th colspan="2" style="color:red;"><b>Import <span class="glyphicon glyphicon-import" style="color:black;"></span></b></th>
                              </tr>
                              <tr>
                                <th width="5%">Sr. No.</th>
                                <th width="35%">Grade Tag</th>
                                <th width="15%">Qty / MT</th>
                                <th width="15%">Actions</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                              </tr>                    
                            </thead>
                            <tbody>
                            <?php 
                            if(!empty($this->request->data['Licence']['id'])){
                              $count = sizeof($licenitem);
                                 foreach($licenitem as $key => $v) {   ++$key; ?>
                              <tr class="addmore<?php echo $key;?>">
                                 <td><?php echo $this->Form->input('sr_no', array('class'=>'form-control sr_no nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no'.$key,'name'=>'data[Licence][item]['.$key.'][sr_no]','value'=>$key));?>
                                 </td>
                                 <td>
                                   <select id='procatid<?php echo $key;?>' class='form-control comboselection gradetag nameclass' name='data[Licence][item][<?php echo $key; ?>][gradetagid]' data-id=<?php echo $key; ?>>
                                        <?php foreach($gradetag as $pro_cat){?>
                                        <option value='<?php echo $pro_cat['Gradetag']['id'];?>' <?php if($pro_cat['Gradetag']['id'] == $v['Licenceitem']['gradetagid']){ ?> selected = 'selected' <?php } ?>><?php echo $pro_cat['Gradetag']['gradetagname'];?></option>
                                            <?php } ?>
                                    </select>
                                 </td>
                                 <td><?php echo $this->Form->input('ex_qty', array('class'=>'form-control exqty decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'procatid'.$key,'data-id'=>$key,'name'=>'data[Licence][item]['.$key.'][ex_qty]','value'=>$v['Licenceitem']['ex_qty']));?>
                                 </td>
                                  <td class="numeric"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'deleteno','value'=>'1','title'=>'Delete','data-id'=>$v['Licenceitem']['id'])); ?>&nbsp;&nbsp;
                                  <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addno','value'=>'1')); ?>
                                  </td>  
                                 <td class="selected_grade<?php echo $key;?>">  <?php foreach($gradetag as $pro_cat){ if($pro_cat['Gradetag']['id'] == $v['Licenceitem']['gradetagid']){ echo $pro_cat['Gradetag']['gradetagname']; } }  ?></td>
                                 <td><?php echo $this->Form->input('im_qty', array('class'=>'form-control decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'im_qty'.$key,'data-id'=>1,'name'=>'data[Licence][item]['.$key.'][im_qty]','value'=>$v['Licenceitem']['im_qty']));?></td>
                                  <?php echo $this->Form->input('id', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'item_id'.$key,'readonly','value'=>$v['Licenceitem']['id'],'name'=>'data[Licence][item]['.$key.'][id]'));
                                    ?>
                                     <?php echo $this->Form->input('licenceid', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'item_id'.$key,'readonly','value'=>$v['Licenceitem']['licenceid'],'name'=>'data[Licence][item]['.$key.'][licenceid]'));
                                    ?>
                              </tr>
                                 
                            <?php } }?>
                            <?php if(empty($this->request->data['Licence']['id'])){ ?>
                             <tr class="addmore1">
                                 <td><?php echo $this->Form->input('sr_no', array('class'=>'form-control sr_no nameclass','type'=>'text','label'=>false,'required'=>true,'id'=>'sr_no1','name'=>'data[Licence][item][1][sr_no]','value'=>1));?>
                                 </td>
                                 <td><?php echo $this->Form->input('gradetagid', array('class'=>'form-control comboselection gradetag','type'=>'select','label'=>false,'required'=>true,'data-id'=>1,'name'=>'data[Licence][item][1][gradetagid]','options'=>$gradetags,'empty'=>'Select Grade Tag'));?>
                                   
                                 </td>
                                 <td><?php echo $this->Form->input('ex_qty', array('class'=>'form-control exqty decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'procatid1','data-id'=>1,'name'=>'data[Licence][item][1][ex_qty]'));?>
                                 </td>
                                  <td class="numeric" align="center"><?php echo $this->Html->image('warning.png', array('alt' => 'delete','class'=>'deleteno','value'=>'1','title'=>'Delete')); ?>&nbsp;&nbsp;
                                  <?php echo $this->Html->image('add.png', array('alt' => 'Add','title'=>'Add','id'=>'add','class'=>'addno','value'=>'1')); ?>
                                  </td>  
                                 <td class="selected_grade1"></td>
                                 <td><?php echo $this->Form->input('im_qty', array('class'=>'form-control decimalallow','type'=>'text','label'=>false,'required'=>true,'id'=>'im_qty1','data-id'=>1,'name'=>'data[Licence][item][1][im_qty]'));?></td>
                                  <?php echo $this->Form->input('licenceid', array('class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'item_id','readonly','name'=>'data[Licence][item][1][licenceid]'));
                                    ?>
                              </tr>
                              <?php } ?>
                              <tr id="1" class="item_total"> 
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <input type="hidden" name="updateno" id="up" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" class="input" />
                        <input type="hidden" name="remno" id="remno" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" width="35px;"/>
                        <input type="hidden" name="removal_id" id="removal_id" value="" width="35px;"/>
                      </div>                                  
                      <div class="form-actions">
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="row">
                                      <div class="col-md-offset-3 col-md-8">
                                          <button class="btn green" type="submit">Submit</button>
                                           <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'licences'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6"> </div>
                          </div>
                      </div>
                   <?php echo $this->Form->end();?>                                        
                  <!-- END FORM-->
                </div>
              </div>
<script type="text/javascript">
$(document).ready(function(){
  
        /*Use : Add more Row Functionality.*/
  $(document).on('click', '.addno', function () {
    var no = $('#up').val();
    var nxt = parseInt(no) + 1;
    $(".item_total").before('<tr class="addmore'+nxt+'"><td><div class="input text"><input type="text" id="sr_no'+nxt+'" required="required" class="form-control sr_no nameclass" name="data[Licence][item]['+nxt+'][sr_no]" value="'+nxt+'"></div></td><td><div class="input select"><select class="form-control gradetag comboselection" name="data[Licence][item]['+nxt+'][gradetagid]" data-id='+nxt+'><option value="">Select Grade Tag</option><?php foreach($grades as $val){ ?><option value=<?php echo $val['Gradetag']['id'];?>><?php echo $val['Gradetag']['gradetagname'];?></option><?php } ?></select></div></td><td><div class="input text"><input type="text" data-id="'+nxt+'" id="ex_qty'+nxt+'" required="required" class="form-control exqty decimalallow" name="data[Licence][item]['+nxt+'][ex_qty]"></div></td><td class="numeric" align="center"><img title="Delete" value="1" class="deleteno" alt="delete" src="<?php echo $this->webroot;?>img/warning.png">&nbsp;&nbsp;<img value="1" class="addno" id="add" title="Add" alt="Add" src="<?php echo $this->webroot;?>img/add.png"></td><td class="selected_grade'+nxt+'">Grade</td><td><div class="input text"><input type="text" data-id="1" id="im_qty'+nxt+'" required="required" class="form-control decimalallow" name="data[Licence][item]['+nxt+'][im_qty]"></td></tr>');

      
    var upno = $('#up').val(nxt); 
      var remmno = $('#remno').val();
      var remnextno = parseInt(remmno) + 1;
      var t = $('#remno').val(remnextno);
       $(".comboselection").select2();
      //$("#procatid"+nxt+" option[value='Welded']").attr("selected", true);
      //alert("#procatid"+nxt);
      //alert(address[1]);
       
  });

  /* Use : Delete row from list. */
  $(document).on('click', '.deleteno', function () {
      
    var upno = $('#up').val();
    var id = $(this).attr('data-id');
    var rm = $('#remno').val();
    var gd = $(this).attr('mydelnum');
    
      if(upno != '1'){
          if(id == ''){
            $('#removal_id').val();
        }
        else{   
            $('#removal_id').val(function(i,val) { 
                return val + (!val ? '' : ',') + id;
            });
        }
      var myno = $(this).attr('mydelnum');
      $(this).parent().parent().remove();
      var remainingno = parseInt(upno) - 1;
      var upno = $('#up').val(remainingno); 
      var rem_amount = $(this).data("amount");
      var final_total = $('#total').val();
   }
  });
  /* Select dropdown on Import quantity */
    $(document).on('change', '.gradetag', function () {
                  var dataid = $(this).attr('data-id');
                 var grade_set = $('option:selected',this).text();
                 $('.selected_grade'+dataid).text(grade_set);
    });
    $(document).on('keyup', '.exqty', function () {
                  var data_id = $(this).attr('data-id');
                  var value_qty = $(this).val();
                  var hscode = $('#hscode_id').find(':selected').attr('data-value');
                  var import_value = parseFloat(value_qty) + (parseFloat(value_qty) * parseFloat(hscode)/100);
                  if(!isNaN(import_value) && import_value.length != 0) {
                  $('#im_qty'+data_id).val(import_value);
                }else {
                  $('#im_qty'+data_id).val('');
                }
    });
    $(document).on('keyup', '#LicenceValidity', function () {
                  var validity = $(this).val();
                  var issue_date = $('#LicenceIssueDate').val().split("/");
                  var CurrentDate = new Date(issue_date[2], issue_date[1] - 1, issue_date[0]);
                  var date = CurrentDate.getDate();
                  CurrentDate.setDate(1);
                  CurrentDate.setMonth(CurrentDate.getMonth()+ parseInt(validity));
                  CurrentDate.setDate(date);
                  var dd = CurrentDate.getDate();
                  var mm = CurrentDate.getMonth() + 1;
                  var y = CurrentDate.getFullYear();
                  var someFormattedDate = dd + '/'+ mm + '/'+ y;
                  if(!isNaN(validity) && validity.length != 0) {
                    $('#LicenceExpiryDate').val(someFormattedDate);
                  }
    });
$(document).on('change', '.issue_date', function () {
                  var issue = $(this).val().split("/");
                  var issue_date = $('#LicenceValidity').val();
                  var CurrentDate = new Date(issue[2], issue[1] - 1, issue[0]);
                  var date = CurrentDate.getDate();
                  CurrentDate.setDate(1);
                  CurrentDate.setMonth(CurrentDate.getMonth()+ parseInt(issue_date));
                  CurrentDate.setDate(date);
                  var dd = CurrentDate.getDate();
                  var mm = CurrentDate.getMonth() + 1;
                  var y = CurrentDate.getFullYear();
                  var samefordate = dd + '/'+ mm + '/'+ y;
                  if(!isNaN(issue_date) && issue_date.length != 0) {
                            $('#LicenceExpiryDate').val(samefordate);
                  }
    });
   });
               
</script>


<script type="text/javascript">
/*Add HS Code using modal dialog*/
 
function addHSCode() {
  $.ajax({
    url: '<?php echo WEBSITE_PATH;?>hscodes/add/targetid:hscode_id',
    type: 'POST',
    success: function(data) {
      $('#addHSCodeModal .previewText').html(data);
      $('#addHSCodeModal .page-title').remove();
      $('#addHSCodeModal #cancelbutton').remove();
    },
    error: function(e) {}
  });
}
</script>
<div class="panel-body">
  <div aria-hidden="true" aria-labelledby="addHSCodeModalLabel" role="dialog" tabindex="-1" id="addHSCodeModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>

<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>