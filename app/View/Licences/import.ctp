 <?php
  $html = '<style>
        table {float: none;margin: 0 auto;width: 100%;border-spacing: 0;}
        .flip-scroll table, .table td .img-responsive {width: 100%;}
        .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border: 1px solid #e7ecf1
        }
        .table {margin-bottom: 20px;width: 100%; border: 1px solid #e7ecf1}
        .numalign{text-align:right; padding-right: 10px!important;margin-right:10px;}
        </style>'; 
    $gradetagcount = sizeof($licencedata['Licenceitem']); /*number of gradetag associated with licence*/
    $totalcolumn = 5 + 2 * $gradetagcount;
    $gradetag_column = ''; /*grade tag associated with licence*/
    $allowedqty_column = ''; /*allowed quantity of grade tag associated with licence*/
    $blank_cell = ''; /*Blank cell for deign purpose*/
    $licenceitemindex = 1;
    
    /*Start Licence item associated grade tag and its allowed quantity*/
    foreach ($licencedata['Licenceitem'] as $itemvalue) {
      $gradetag_column .= '<th>'.$itemvalue['Gradetag']['gradetagname'].'</th>';
      $allowedqty_column .= '<th>'.$itemvalue['im_qty'].'</th>';
      $blank_cell .= '<th></th>';

      $gradetag[$licenceitemindex] = $itemvalue['id'];/*save licence item id, which will be usefull to get same means this item id related data from import item array*/
      $licenceitemindex++;
    }
    /*End Licence item associated grade tag and its allowed quantity*/

    /*search in array for to get licenceitem related record from licenceimport item array*/
    function searcharray($value, $key, $array) {
      foreach ($array as $k => $val) { 
        if ($val[$key] == $value) { 
          return $val;
        }
      }
      return null;
    }

    /*Start fetching licence import and its import item data*/
    $licenceimportdata = '';
    $licenceimport = '';
    foreach ($licencedata['Licenceimport'] as $importvalue) {
      $licenceimport = '<tr>
        <td>'.$importvalue['boeno'].'</td>
        <td>'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($importvalue['boedate'])).'</td>
        <td>'.$importvalue['invoiceid'].'</td>
        <td>'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($importvalue['invoicedate'])).'</td>
        <td>'.$importvalue['Client']['company_name'].'</td>';
      $import_qty = ''; /*Import quantity data*/
      $dollar_val = ''; /*Dollar value data*/
      foreach ($gradetag as $gradetagvalue) {
        $results = searcharray($gradetagvalue,'licenceitemid', $importvalue['Licenceimportitem']);
        if(!empty($results)){
          $import_qty .= '<td class="numalign">'.$results['importqty'].'</td>';
          $dollar_val .= '<td class="numalign">'.$results['dollarvalue'].'</td>';
        }else{
          $import_qty .= '<td></td>';
          $dollar_val .= '<td></td>';
        }
      }
      $licenceimportdata .= $licenceimport.''.$import_qty.''.$dollar_val.'</tr>'; 
    }
    /*End fetching licence import and its import item data*/

  $html .= '<html>
              <head><title>Licence</title></head>
              <body>
                <table width="100%" border="1" class="table table-bordered table-striped table-condensed flip-content view_invoice">
                <tbody>
                  <tr><td colspan="'.$totalcolumn.'" align="center"><b>Licence</b> </td>
                  <tr>
                    <td colspan="'.$totalcolumn.'"><b>ADVANCE LICENCE '.$licencedata['Licence']['advancelicenceno'].' ISSUE DATE '.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($licencedata['Licence']['issue_date'])).'</b></td>
                  </tr>
                  <tr>
                    <td colspan="5">IMPORT LAST DATE :'.$this->requestAction('App/date_ymd_to_dmy/'.strtotime($licencedata['Licence']['expiry_date'])).'</td>
                  </tr>
                  <tr>
                    <td colspan="5">IMPORT Qty : 304:236.250 MT,316 405.00000 MT</td>
                  </tr>
                  <tr>
                    <th colspan="5"></th>
                    <th colspan="'.$gradetagcount.'">'.$licencedata['Licence']['advancelicenceno'].' SMLS</th>
                    <th colspan="'.$gradetagcount.'">DOLLAR VALUE</th>
                  </tr>

                  <tr>
                    <th colspan="4"></th><th>GRADE</th>'.$gradetag_column.''.$gradetag_column.'
                  </tr>
                  <tr>
                    <th colspan="4"></th><th>QTY.</th>'.$allowedqty_column.''.$blank_cell.'
                  </tr>
                  <tr>
                    <th>BOE NO</th>
                    <th>BOE Date</th>
                    <th>INVOICE NO</th>
                    <th>INVOICE DATE</th>
                    <th>PARTY NAME</th>
                    '.$blank_cell.'
                    '.$blank_cell.'</tr>';

        $html .= $licenceimportdata;
    
        $html .= '</table>
      </body>
  </html>';              
  echo $html;
?>