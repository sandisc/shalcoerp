<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
     <?php echo $this->Session->flash();?>
      <div class="portlet light portlet-fit portlet-datatable bordered">
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
            </div>
            <div class="actions">
               <div class="btn-group btn-group-devided">
                  <?php echo $this->Html->link(' Add Dispatch ',array('action' => 'add','controller' =>'dispatches'),array('class' => 'fa fa-plus btn sbold green')); ?>
               </div>
               <div class="btn-group">
               </div>
            </div>
         </div>
         
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                             <button class="btn btn-sm red table-group-action-delete">
                                                <i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"><input type="checkbox" class="group-checkable"> </th>
                                                    <th width="20%"> Dispatch location name </th> 
                                                    <th width="10%"> Dispatch Code </th>
                                                    <th>No of Order </th>
                                                    <th width="10%"> Actions </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                  <td> </td>
                                                  <td><input type="text" class="form-control form-filter input-sm" name="location_name"> </td><td><input type="text" class="form-control form-filter input-sm" name="code"> </td>  
                                                  <td width="10%"></td>
                                                  <td>
                                                      <div class="margin-bottom-5">
                                                          <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                                          <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                                      </div>
                                                  </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
      </div>
         <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->