<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
     <?php echo $this->Session->flash();?>
      <div class="portlet light portlet-fit portlet-datatable bordered">
         <div class="portlet-title">
            <div class="caption">
               <i class="icon-settings font-dark"></i>
               <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle .'  ('.$dispatch_loc['Dispatch']['location'].')';?></span>
            </div>
            <div class="actions">
              
               <div class="btn-group">
               </div>
            </div>
         </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="20%"> Order No </th> 
                                                    <th width="10%"> Company Name </th>
                                                    <th width="10%"> Dispatch code </th>
                                                    <th width="15%"> Action </th>
                                                </tr>
                                                <tr role="row" class="filter">

                                                    <td><input type="text" class="form-control form-filter input-sm" name="proforma_no"> </td>
                                                     
                                                     <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>
                                                    <td>
                                                       <input type="text" class="form-control form-filter input-sm" name="dispatch_code">
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
      </div>
      <!-- End: life time stats -->
   </div>
</div>