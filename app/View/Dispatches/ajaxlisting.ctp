<?php
  $iTotalRecords = $count;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;

  $status_list = array(
    array("success" => "Active"),
    array("danger" => "Inactive")
  );
    foreach ($dispatch as $dispatch) {
    $link = $this->requestAction(
                  array('controller' => 'Orders', 'action' => 'countorder'),
                  array('conditional_parameter' => array('dispatch_id'=>$dispatch['Dispatch']['id']))
            );
    //$link = $this->requestAction('Orders/countorder/'.$dispatch['Dispatch']['id']);
      $records["data"][] = array('<input type="checkbox" name="id[]" value="'.$dispatch['Dispatch']['id'].'">',
      $dispatch['Dispatch']['location'],
      $dispatch['Dispatch']['code'],
      
      '<a href="'.WEBSITE_PATH.'dispatches/orderonlocation/'.base64_encode($dispatch['Dispatch']['id']).'" class="btn " title="Order On this Location">'.$link.'</a>',
      '<a href="'.WEBSITE_PATH.'dispatches/add/'.base64_encode($dispatch['Dispatch']['id']).'" class="btn btn-success"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>
      <a href="'.WEBSITE_PATH.'dispatches/delete/'.base64_encode($dispatch['Dispatch']['id']).'" class="btn btn-danger" title="Delete"><span class="glyphicon glyphicon-trash" id="delete" data-confirm="Are you sure to delete this dispatch location?"></span></a>
      <a href="'.WEBSITE_PATH.'dispatches/orderonlocation/'.base64_encode($dispatch['Dispatch']['id']).'" class="btn btn-info" title="Order On this Location"><span class="fa fa-location-arrow"></span></a>',);
    }
  /*if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            if($_REQUEST['customActionName'] == 'active'){
                $id = $_POST['id'];
                exit;
            }  
*/
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been updated successfully"; // pass custom message(useful for getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_delete") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Record has been deleted successfully"; // pass custom message(useful for getting status of group actions)
  }
   // }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records); exit;
?>