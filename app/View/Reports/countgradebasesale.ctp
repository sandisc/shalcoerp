                            <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
                            <div class="portlet box green">
                              <div class="portlet-title">
                                  <div class="caption">
                                      <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                  <div class="tools">
                                      <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                  </div>
                              </div>
                              <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?php echo $this->Form->create('Gradebasesale');
                                    echo $this->Form->input('id', array('type'=>'hidden')); 
                                    if(isset($this->params['named']['targetid'])){
                                        echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                    }?> 
                                    <div class="form-body">
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label col-md-4">Gradetag Name<span class="required" aria-required="true"> * </span></label>
                                                  <div class="col-md-8 checkbo_container"><?php
                                                    echo $this->Form->input('gradetag_id', array('multiple' => 'checkbox', 'options' => $gradetag,'label'=> false,'required'));
                                                  ?>
                                                  <span class="help-block"><br></span>
                                                  </div>
                                              </div>
                                            </div>
                                            <?php $options = array('16-17' => '16-17','15-16'=>'15-16', '14-15'=>'14-15'); ?>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Select Financial Year<span class="required" aria-required="true"> * </span></label>
                                                    <div class="col-md-8 checkbo_container">
                                                    <?php echo $this->Form->input('year', array('multiple' => 'checkbox', 'options' => $options,'label'=> false,));
                                                    ?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                  <label class="control-label col-md-4">Select Client<span class="required" aria-required="true"> * </span></label>           
                                                  <div class="col-md-8 checkbo_container">
                                                  <?php echo $this->Form->input('client', array('multiple' => 'checkbox', 'options' => $client,'label'=> false));
                                                    ?>
                                                  <span class="help-block"><br></span>
                                                  </div>
                                                </div>
                                            </div>                 
                                            <?php $type = array('1' => 'Invoice', '2'=>'Chalan'); ?>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label class="control-label col-md-4">Type<span class="required" aria-required="true"> * </span></label>
                                                  <div class="col-md-6">
                                                  <?php echo $this->Form->input('type', array('multiple' => 'checkbox', 'options' => $type,'label'=> false));
                                                    ?>
                                                    <span class="help-block"><br></span>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Report Name <span class="required" aria-required="true"> * </span></label>
                                                    <div class="col-md-8">
                                                       <?php echo $this->Form->input('reportname', array('type' => 'text','class'=>'form-control','label'=>false,'required'));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">You want to Save?</label>
                                                    <div class="col-md-8">
                                                    <?php echo $this->Form->checkbox('save'); ?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>                                                          
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-8">
                                                        <button class="btn green" type="submit">Generate</button>
                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'productcategories'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                 <?php echo $this->Form->end();?>                                        
                                <!-- END FORM-->
                                <div class="table-scrollable" style="display:none;"></div>
                                </div>
                            </div>
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>
<script>
$(document).ready(function() {      

    $("form").on("submit", function (e) {
        var form = $("form#GradebasesaleCountgradebasesaleForm").serializeArray();
       $.ajax({
           type: 'POST',
           url: '<?php echo WEBSITE_PATH;?>reports/gradebasesale/',
           data: form,
           success: function(data){
            $('.table-scrollable').css("display","block");
            $('.table-scrollable').html(data);
            var Gradesale = $('#GradebasesaleSave').val();
           // alert('Report Saved Successfully...');
            }
        });
          return false;
    
     //'event.preventDefault();
      //stop the actual form post !important!

    });

});
</script>