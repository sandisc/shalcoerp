                <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
                <div class="portlet box green">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                      <div class="tools">
                          <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                      </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Gradebasesalesdetails',array('novalidate' => true));
                        echo $this->Form->input('id', array('type'=>'hidden')); 
                        if(isset($this->params['named']['targetid'])){
                            echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                        }?> 
                        <div class="form-body">
                            <!--/row-->
                             <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label col-md-4">Bill Type<span class="required" aria-required="true"> * </span></label>
                                      <div class="col-md-8"><?php
                                         echo $this->Form->radio('bill_type', array(
                                            '1' => 'Invoice',
                                            '2' => 'Chalan',
                                        ), 
                                        array(
                                            'legend' => false,
                                            'value' => 1,    
                                        )
                                    );
                                      ?>
                                      <span class="help-block"><br></span>
                                      </div>
                                  </div>
                                </div>
                                 <div class="col-md-6">
                                   <div class="form-group">
                                      <label class="control-label col-md-4">Select Client<span class="required" aria-required="true"> * </span></label>           
                                      <div class="col-md-8 checkbo_container">
                                      <?php echo $this->Form->input('client', array('multiple' => 'checkbox', 'options' => $client,'label'=> false));
                                        ?>
                                      <span class="help-block"><br></span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                             <?php $options = array('16-17' => '16-17','15-16'=>'15-16', '14-15'=>'14-15'); ?>
                            <div class="row">
                                <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label col-md-4">Select Financial Year<span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-8 checkbo_container">
                                      <?php echo $this->Form->input('year', array('multiple' => 'checkbox', 'options' => $options,'label'=> false,));?>
                                        <span class="help-block"><br></span>
                                    </div>
                                  </div>                                 
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                       <label class="control-label col-md-3">Date Range</label>
                                          <div class="col-md-4">
                                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                              <?php echo $this->Form->input('from_date', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true)); ?>
                                              <span class="input-group-addon"> to </span>
                                              <?php echo $this->Form->input('to_date', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true)); ?>
                                            </div>
                                              <span class="help-block"> Select date range </span>
                                            </div>
                                          </div>
                                  </div>
                            </div>
                            <br>
                           <?php $options = array(); 
                            foreach($producttaxonomy as $value => $label) { 
                              $options[] = array( 
                                'name' => $label, 
                                'value' => $value, 
                                'class' => 'pro_text',
                                'onClick' => 'CheckForm(this)' 
                              ); 
                            } ?>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4">Select Product Taxonomy<span class="required" aria-required="true"> * </span></label>           
                                  <div class="col-md-8 checkbo_container">
                                  <?php echo $this->Form->input('producttaxonomy', array('multiple' => 'checkbox', 'options' => $options));
                                    ?>
                                  <span class="help-block"><br></span>
                                  </div>
                                </div>
                              </div>                 
                              <div class="col-md-6">                                   
                              </div>
                            </div>
                            <br>
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group new_gradetag">
                                    </div>
                                </div>               
                                <div class="col-md-6 size_row" style="display: none;">
                                     <div class="form-group">
                                        <label class="control-label col-md-4">Select Size(OD)</label>
                                      <div class="col-sm-4">                                                   
                                        <div class="input-group">
                                        <?php echo $this->Form->input('from_od', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'placeholder'=>'From')); ?>
                                        </div>
                                       <span class="help-block"><br></span>
                                     </div>       
                                     <div class="col-sm-4">
                                       <div class="input-group">
                                        <?php echo $this->Form->input('to_od', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'placeholder'=>'To')); ?>
                                        </div>
                                       <span class="help-block"><br></span>
                                     </div>

                                    </div>
                                     <div class="form-group">
                                        <label class="control-label col-md-4">Select Size(WT)</label>
                                      <div class="col-sm-4">                                                   
                                        <div class="input-group">
                                        <?php echo $this->Form->input('from_wt', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'placeholder'=>'From')); ?>
                                        </div>
                                       <span class="help-block"><br></span>
                                     </div>       
                                     <div class="col-sm-4">
                                       <div class="input-group">
                                        <?php echo $this->Form->input('to_wt', array('type' => 'text','class'=>'form-control','label'=>false,'required'=>true,'placeholder'=>'To')); ?>
                                        </div>
                                       <span class="help-block"><br></span>
                                     </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-8">
                                            <button class="btn green" type="submit">Generate</button>
                                             <?php echo $this->Html->link('Reset',array('action' => 'gradesizesalesdetails','controller' =>'reports'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                     <?php echo $this->Form->end();?>                                        
                    <!-- END FORM-->
                    <div class="table-scrollable" style="display:none;"></div>
                    </div>
                </div>
<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>
<script>
function CheckForm() { 
    var final = [];
    $('.pro_text:checked').each(function(){        
        var values = $(this).val();
        final.push(values);
    });
    final = final.join(',');
    $.ajax({
           type: 'POST',
           url: '<?php echo WEBSITE_PATH;?>reports/GetGrades/',
           data: { final : final},
           success: function(data){
              $('.new_gradetag').html(data);
              $('.size_row').css('display','block');
            }
        });
}
 $("form").on("submit", function (e) {
        var form = $("form#GradebasesalesdetailsGradesizesalesdetailsForm").serializeArray();
       $.ajax({
           type: 'POST',
           url: '<?php echo WEBSITE_PATH;?>reports/gradebasesales/',
           data: form,
           success: function(data){
            $('.table-scrollable').css("display","block");
            $('.table-scrollable').html(data);
            var Gradesale = $('#GradebasesaleSave').val();
           // alert('Report Saved Successfully...');
            }
        });
         return false;
       });
</script>