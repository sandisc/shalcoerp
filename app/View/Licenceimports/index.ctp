<h3 class="page-title"><?php echo $mainTitle;?></h3>
<div class="row">
   <div class="col-md-12">
        <?php echo $this->Session->flash();?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                   <i class="icon-settings font-dark"></i>
                   <span class="caption-subject font-dark sbold uppercase"><?php echo $pageTitle;?></span>
                </div>
                <div class="actions">
                   <div class="btn-group btn-group-devided">
                      <?php echo $this->Html->link(' Add Licence Impoort',array('action' => 'add','controller' =>'licenceimports'),array('class' => 'fa fa-plus btn sbold green')); ?>
                   </div>
                   <div class="btn-group">
                   </div>
                </div>
            </div>
         
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <span> </span>
                        <button class="btn btn-sm red table-group-action-delete">
                            <i class="fa fa-trash"></i> Delete</button>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%"><input type="checkbox" class="group-checkable"> </th>
                                <th width="10%"> Licence NO </th>
                                <th width="8%"> BOE No </th>
                                <th width="8%"> BOE Date </th>
                                <th width="12%"> Invoice NO </th>
                                <th width="8%"> Invoice Date </th>
                                <th width="15%"> Client Name </th>
                                <th width="12%"> Modified Date </th>
                                <th width="10%"> Modified By </th> 
                                <th width="15%"> Actions </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="advancelicenceno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="boeno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="boedate"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="invoiceno"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="invoicedate"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="company_name"> </td>                                
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="modified_from" placeholder="From">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="modified_to" placeholder="To">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>  
                                <td><input type="text" class="form-control form-filter input-sm" name="first_name"> </td>                                                  
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom" title="Click To Search"><i class="fa fa-search"></i> </button>
                                        <button class="btn btn-sm red btn-outline filter-cancel" title="Reset Search Criteria"><i class="fa fa-times"></i> </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: DIV portlet light portlet-fit portlet-datatable bordered -->
   </div>
</div><!--/row -->


<!--- Start Approve Proforma Invoice Dialog Modal -->
<div class="modal fade" id="extensionform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <?php echo $this->Form->create('Licence',array('method'=>'POST','class'=>'shalcoform')); ?>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Licence No :Extension</h4>
      </div>
      <div class="modal-body">
        <?php
          echo $this->Form->input('id',array('type'=>'hidden','id'=>'extensionid'));
          echo '<div class="clearfix"> <label for="foo">Extension Validity<span class="required"> * </span></label>';
          echo $this->Form->input('ex_validity',array('type'=>'text','label'=>false,'placeholder'=>"Extension Validity","class" => "form-control ex_validity",'id'=>'ex_validity'));           
          echo '</div><div class="clearfix"><br><label for="Extended Expiry Date">Extended Expiry Date</label>';
          echo $this->Form->input('ex_expirydate',array('type'=>'text','label'=>false,'placeholder'=>"Extension date", "class" => "form-control input-group",'id'=>'ex_date','readonly'));
          echo '</div>'
        ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btnExten" class="btn btn-primary">Save</button>
      </div>
       <?php echo $this->Form->end();?>
    </div>
  </div>
</div>
<!--- End Approve Proforma Invoice Dialog Modal -->
<script>

  $(document).ready(function() {
  
  /*Start Click Event of 'Accept' Button for accept Proforma Invoice*/   
    $(document).on('click', '#btn_ext', function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      $('#extensionform').modal({
          show: true,
      });
        var licenceid = $(this).attr('data-lid');
        $('#extensionid').val(licenceid);
        var ex_date = $(this).attr('data-date').split("-");
        $('#myModalLabel').text('Licence No :'+$(this).attr('data-advace')+' Extension');
        $(document).on('keyup', '#ex_validity', function () {
                
                var ex_validity = $('#ex_validity').val();
                var CurrentDate = new Date(ex_date[0], ex_date[1] - 1, ex_date[2]);
                var date = CurrentDate.getDate();
                CurrentDate.setDate(1);
                CurrentDate.setMonth(CurrentDate.getMonth()+ parseInt(ex_validity));
                CurrentDate.setDate(date);
                var dd = CurrentDate.getDate();
                var mm = CurrentDate.getMonth() + 1;
                var y = CurrentDate.getFullYear();
                var someFormattedDate = dd + '-'+ mm + '-'+ y;
                console.log(someFormattedDate);
                if(!isNaN(ex_validity) && ex_validity.length != 0) {
                       $('#ex_date').val(someFormattedDate);
                  }
                
         });
     $(document).on('click', '#btnExten', function () {
        $("#spinner").show();
        e.preventDefault();
        e.stopImmediatePropagation();      
        var id = $('#extensionid').val();
         var ex_validity = $('#ex_validity').val();
        var date =  $('#ex_date').val();
        $.ajax({
          url: '<?php echo WEBSITE_PATH;?>licences/extension/',
          type: 'POST', 
          data: 'id='+licenceid+'&ex_validity='+ex_validity+'&ex_date='+date,
          cache: false,
          success: function (data) {
            $("#spinner").hide();
            $('.modal').modal('hide');
            $('#btn_ext').hide();
            $('#ex_validity').val('');
            $('#ex_date').val('');
         }
        });         
      });
    
    });
});
</script>