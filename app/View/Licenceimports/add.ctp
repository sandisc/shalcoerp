                            <h3 class="page-title"> <?php echo $mainTitle;?></h3> 
                            <div class="portlet box green">
                                <div class="portlet-title">
                                  <div class="caption">
                                      <i class="fa fa-gift"></i><?php echo $pageTitle;?></div>
                                  <div class="tools">
                                      <a class="collapse" href="javascript:;" data-original-title="" title=""></a>
                                  </div>
                              </div>
                              <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?php echo $this->Form->create('Licenceimport',array('class'=>'shalcoajaxform','url'=> WEBSITE_PATH.''.$this->params['controller'].'/ajaxsubmit/'));
                                    echo $this->Form->input('id', array('type'=>'hidden')); 
                                    if(isset($this->params['named']['targetid'])){
                                        echo $this->Form->input('targetid', array('type'=>'hidden','value'=>$this->params['named']['targetid'])); 
                                    }?> 
                                    <div class="form-body">
                                      <div class="col-md-12 row"><br/></div>
                                      <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"> BOE No<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('boeno', array('class'=>'form-control advancelicenceno','label'=>false,'type'=>'text'));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>                                                         
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">BOE Date<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                    <div class="input-group date date-picker" id="dp3" data-date-format="dd-mm-yyyy">
                                                    <?php if(!empty($id)){?>
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      <?php echo $this->Form->input('boedate', array('label'=>false,'class'=>'form-control boedate','type'=>'text','value'=>$this->requestAction('App/date_ymd_to_dmy/'.strtotime($this->request->data['Licenceimport']['boedate'])))); } else { ?>
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      <?php echo $this->Form->input('boedate', array('label'=>false,'class'=>'form-control boedate','type'=>'text'))  ; } ?>
                                                    </div>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Licence No<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('licenceid', array('options'=>$licencelist,'label'=>false,'class'=>'form-control licenceno','type'=>'select','empty'=>'Select Licence no'));?>
                                                    </div>
                                                      <span class="help-block"><br></span>
                                                </div>
                                            </div>
                                      </div>
                                      <!-- 2ND row -->
                                      <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"> Invoice No<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('invoiceid', array('class'=>'form-control advancelicenceno','label'=>false,'type'=>'text'));?>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>                                                         
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Invoice Date<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                    <div class="input-group date date-picker" id="dp3" data-date-format="dd-mm-yyyy">
                                                    <?php if(!empty($id)){?>
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      <?php echo $this->Form->input('invoicedate', array('label'=>false,'class'=>'form-control boedate','type'=>'text','value'=>$this->requestAction('App/date_ymd_to_dmy/'.strtotime($this->request->data['Licenceimport']['invoicedate']))));?>
                                                      <?php } else { ?>
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      <?php echo $this->Form->input('invoicedate', array('label'=>false,'class'=>'form-control boedate','type'=>'text')); }?>
                                                    </div>
                                                      <span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Party Name<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                      <?php echo $this->Form->input('clientid', array('options'=>$client_list,'label'=>false,'class'=>'form-control licenceno','type'=>'select'));?>
                                                    </div>
                                                      <span class="help-block"><br></span>
                                                </div>
                                            </div>
                                      </div>
                                      <!-- New  field Price -->
                                       <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4"> Price<span class="required"> * </span></label>
                                                    <div class="col-md-8">
                                                     <div class="input select"><select required="required" id="price" class="form-control price" name="data[Licenceimport][priceid]" required="required">
							                        	<option value="0">Select Price Unit</option>
							                        	<?php foreach($price_set as $pr =>$v){?>
														<option value="<?php echo $pr;?>" <?php if($pr == '1'){ ?> selected="selected" <?php } ?>><?php echo $v;?></option><?php  }?>
														</select>
													 </div>
													<span class="help-block"><br></span>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                <div class="table-scrollable"> 
                                  <table class="table table-bordered table-striped table-condensed flip-content" id="yourtableid">
                                    <thead class="flip-content portlet box green">
                                      <tr>
                                        <th colspan="6" style="color:red;"><b>Import Grade Information<span class="glyphicon glyphicon-export" style="color:black;"></span></b></th>
                                      </tr>
                                      <tr>
                                        <th width="15%">Grade Tag</th>
                                        <th width="15%">Allow Qty</th>
                                        <th width="15%">Imported Qty</th>
                                        <th width="15%">Dollar Value</th>
                                        <th width="15%">Balance Qty</th>
                                      </tr>                    
                                    </thead>
                                  <?php if(empty($id)){ ?>
                                    <tbody class="custom_export">
                                   
                                    </tbody>

                                  <?php } else {?>
                                  <tbody class="custom_export">
                                  <?php 
                                      $i = 0;

                                      foreach($this->request->data['Licenceimportitem'] as $licence) { ?>
                                        <tr>
                                            <input type="hidden" value="<?php echo $licence['id'];?>" name="data[Licenceimport][item][<?php echo $i; ?>][id]">
                                            <td><?php echo $licence['Licenceitem']['Gradetag']['gradetagname'];?></td>
                                            <td><?php echo $licence['Licenceitem']['im_qty'];?></td>
                                            <td><div class="input text"><input type="text" name="data[Licenceimport][item][<?php echo $i; ?>][importqty]" class="form-control" value="<?php echo $licence['importqty'];?>"></div></td>
                                            <td><div class="input text"><input type="text" name="data[Licenceimport][item][<?php echo $i; ?>][dollarvalue]" class="form-control" value="<?php echo $licence['dollarvalue'];?>"></div></td>
                                            <td><div class="input text"><input type="text" name="data[Licenceimport][item][<?php echo $i; ?>][xyz]" class="form-control" value="<?php echo $this->requestAction('Licenceimports/ImportGradetagbalanceQty/'.$this->request->data['Licenceimport']['licenceid'].'/'.$licence['Licenceitem']['gradetagid']);?>"></div></td>
                                       </tr>
                                      <?php $i++;} } ?>
                                    </tbody>

                                    </table>
                                </div>
                                  <input type="hidden" name="updateno" id="up" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" class="input" />
                                  <input type="hidden" name="remno" id="remno" value="<?php if(!empty($count)){ echo $count;}else{ echo '1';} ?>" width="35px;"/>
                                  <input type="hidden" name="removal_id" id="removal_id" value="" width="35px;"/>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-8">
                                                        <button class="btn green" type="submit">Submit</button>
                                                         <?php echo $this->Html->link('Cancel',array('action' => 'index','controller' =>'licenceimports'),array('class' => 'btn default','id'=>'cancelbutton')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                 <?php echo $this->Form->end();?>                                        
                                <!-- END FORM-->
                                </div>
                            </div>
<div class="panel-body" style="display: none;">
  <div aria-hidden="true" aria-labelledby="addHSCodeModalLabel" role="dialog" tabindex="-1" id="addHSCodeModal" class="modal fade" style="display: none;">
    <div class="modal-dialog" style="width:1000px;">
      <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      </div>
      <div class="modal-body previewText"></div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
      </div>
      </div>
    </div>
  </div>
</div>

<?php 
/*If page open in popup than once again load custom.js*/
if(isset($this->params['named']['targetid'])){?>
<script src="<?php echo ROOT_PATH; ?>pages/scripts/custom.js" type="text/javascript"></script>
<?php } ?>
<script>
$(document).on('change', '#LicenceimportLicenceid', function() {  
  var id = $(this).val();
   $.ajax({
      url: '<?php echo WEBSITE_PATH;?>licenceimports/getGradetag/'+id,
      type: 'POST',
      cache: false,
       success: function (data) {
    $('.custom_export').html(data);
  }
 });
});
</script>