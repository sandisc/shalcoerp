-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2016 at 10:47 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shalcoerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sr_banks`
--

CREATE TABLE `sr_banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `swift_code` varchar(50) NOT NULL,
  `ad_code` varchar(50) NOT NULL,
  `ac_no` int(20) NOT NULL,
  `ac_name` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_banks`
--

INSERT INTO `sr_banks` (`id`, `bank_name`, `branch_name`, `swift_code`, `ad_code`, `ac_no`, `ac_name`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 'Axix Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'AXIXBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(3, 'HDFC Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'HDBCBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(4, 'SBI\r\n\r\n\r\n\r\n', 'Georg Road', 'SBI54BINBB', '0411102', 254564455, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(5, 'UCO Bank', 'Hill Road', 'UCOBINBB', '0469102', 28654825, 'Shalco industries', 8, 8, '2016-04-10 00:00:00', '2016-05-01 23:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `sr_chemicals`
--

CREATE TABLE `sr_chemicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `c_mn` float NOT NULL,
  `c_mx` float NOT NULL,
  `mn_mn` float NOT NULL,
  `mn_mx` float NOT NULL,
  `p_mn` float NOT NULL,
  `p_mx` float NOT NULL,
  `s_mn` float NOT NULL,
  `s_mx` float NOT NULL,
  `si_mn` float NOT NULL,
  `si_mx` float NOT NULL,
  `cr_mn` float NOT NULL,
  `cr_mx` float NOT NULL,
  `ni_mn` float NOT NULL,
  `ni_mx` float NOT NULL,
  `mo_mn` float NOT NULL,
  `mo_mx` float NOT NULL,
  `n_mx` float NOT NULL,
  `n_mn` float NOT NULL,
  `fe_mn` float NOT NULL,
  `fe_mx` float NOT NULL,
  `nb_mn` float NOT NULL,
  `nb_mx` float NOT NULL,
  `ti_mn` float NOT NULL,
  `ti_mx` float NOT NULL,
  `cu_mn` float NOT NULL,
  `cu_mx` float NOT NULL,
  `v_mn` float NOT NULL,
  `v_mx` float NOT NULL,
  `b_mn` float NOT NULL,
  `b_mx` float NOT NULL,
  `ai_mn` float NOT NULL,
  `ai_mx` float NOT NULL,
  `ce_mn` float NOT NULL,
  `ce_mx` float NOT NULL,
  `co_mn` float NOT NULL,
  `co_mx` float NOT NULL,
  `w_mn` float NOT NULL,
  `w_mx` float NOT NULL,
  `la_mn` float NOT NULL,
  `la_mx` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_chemicals`
--

INSERT INTO `sr_chemicals` (`id`, `grade_id`, `c_mn`, `c_mx`, `mn_mn`, `mn_mx`, `p_mn`, `p_mx`, `s_mn`, `s_mx`, `si_mn`, `si_mx`, `cr_mn`, `cr_mx`, `ni_mn`, `ni_mx`, `mo_mn`, `mo_mx`, `n_mx`, `n_mn`, `fe_mn`, `fe_mx`, `nb_mn`, `nb_mx`, `ti_mn`, `ti_mx`, `cu_mn`, `cu_mx`, `v_mn`, `v_mx`, `b_mn`, `b_mx`, `ai_mn`, `ai_mx`, `ce_mn`, `ce_mx`, `co_mn`, `co_mx`, `w_mn`, `w_mx`, `la_mn`, `la_mx`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 3, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '2016-03-24 20:04:24', '2016-03-24 20:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `sr_clients`
--

CREATE TABLE `sr_clients` (
  `id` int(11) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mob` varchar(20) NOT NULL,
  `contact_person` varchar(20) NOT NULL,
  `contact_num1` varchar(20) NOT NULL,
  `contact_num2` varchar(20) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `coordinator` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_clients`
--

INSERT INTO `sr_clients` (`id`, `company_name`, `email`, `mob`, `contact_person`, `contact_num1`, `contact_num2`, `fax`, `website`, `address1`, `address2`, `coordinator`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 'f', 'test@gmail.com', 'fd', 'dfsd', 'fdf', 'sfdg', 'dfs', 'https://www.google.co.in/search?', 'sd', 'fsd', 0, 1, 5, 5, '2016-03-17 19:54:36', '2016-04-09 13:41:56'),
(3, 'sad', 'fas', 'fsa', 'fsa', 'saf', 'fas', 'fas', 'vdfvds', 'f', 'fsa', 0, 1, 5, 5, '2016-03-18 18:26:08', '2016-03-18 18:26:20'),
(4, 'test', 'test3g@gmail.com', '1234567890', 'test', '1234567890', '65156', 'fds', 'rmsb.com', 'f', 'sda', 0, 1, 5, 5, '2016-03-18 19:15:26', '2016-03-18 19:21:23'),
(5, 'Ureka', 'mnj@gmail.com', '1234567890', 'Manoj Desai', '122343454', '34234554`', 'test', 'test.com', 'test', 'tewst', 7, 1, 7, 7, '2016-04-08 21:03:30', '2016-04-25 22:20:09'),
(6, 'Friends Steel', 'steel@gmail.com', '35435', '334342423', '1234324', '23222432', 'tewrt', 'freinds.com', 'Lamibto Road', 'Hmsailton', 0, 1, 5, 5, '2016-04-08 21:11:01', '2016-04-08 21:11:01'),
(7, 'Tata', 'tata@gmsail.com', '45435435', 'Ratan Tata', '43543545', '534543543', 'Tat', 'https://www.tata.com', 'Tata House,All India Road', 'Tata House All India Road 2', 5, 1, 7, 7, '2016-04-11 01:09:09', '2016-04-11 01:17:28');

-- --------------------------------------------------------

--
-- Table structure for table `sr_dispatches`
--

CREATE TABLE `sr_dispatches` (
  `id` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  `po` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_dispatches`
--

INSERT INTO `sr_dispatches` (`id`, `location`, `code`, `po`) VALUES
(1, 'Kalamboli', 'KB', ''),
(2, 'Panjrapole', 'PP', ''),
(3, 'Gulalwadi', 'GW', ''),
(4, 'Taloja', 'TJ', ''),
(5, 'Mahad', 'MD', ''),
(6, 'Kumbharwada', 'KW', ''),
(7, 'Kalamboli Godown 1', 'KBGD1', ''),
(8, 'Kalamboli Godown 2', 'KBGD2', ''),
(9, 'Boriwali', 'BW', '');

-- --------------------------------------------------------

--
-- Table structure for table `sr_grades`
--

CREATE TABLE `sr_grades` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_name` varchar(20) NOT NULL,
  `uns` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_grades`
--

INSERT INTO `sr_grades` (`id`, `standard_id`, `grade_name`, `uns`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'TP201', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(2, 1, 'TP202', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(3, 1, 'TP304/304L', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(4, 1, 'TP303/303L', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-01 01:57:31'),
(5, 1, 'TP304H', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(6, 2, 'TP201', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-01 01:09:51'),
(7, 2, 'TP201LN', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-01 02:01:21'),
(8, 2, 'TP304', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_hscodes`
--

CREATE TABLE `sr_hscodes` (
  `id` int(11) NOT NULL,
  `hscode` varchar(50) NOT NULL,
  `rate` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_hscodes`
--

INSERT INTO `sr_hscodes` (`id`, `hscode`, `rate`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'test', 0.3, 5, 5, '2016-05-29 17:29:28', '2016-05-29 17:30:32'),
(2, 'test2', 0.25, 5, 5, '2016-05-29 17:31:15', '2016-05-29 17:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `sr_invoiceitems`
--

CREATE TABLE `sr_invoiceitems` (
  `id` int(11) NOT NULL,
  `invoiceid` int(11) NOT NULL,
  `proitemid` int(11) NOT NULL,
  `bundleno` varchar(20) NOT NULL,
  `heatnumber` varchar(30) NOT NULL,
  `pcs` int(11) NOT NULL,
  `exempted` float NOT NULL,
  `unit_mt` float NOT NULL,
  `price` float NOT NULL,
  `netprice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_invoiceitems`
--

INSERT INTO `sr_invoiceitems` (`id`, `invoiceid`, `proitemid`, `bundleno`, `heatnumber`, `pcs`, `exempted`, `unit_mt`, `price`, `netprice`) VALUES
(1, 1, 3, 'bundle3', 'HT56', 9, 13.25, 53, 25, 15);

-- --------------------------------------------------------

--
-- Table structure for table `sr_invoices`
--

CREATE TABLE `sr_invoices` (
  `id` int(11) NOT NULL,
  `proformaid` int(11) NOT NULL,
  `invoiceno` varchar(30) NOT NULL COMMENT 'Invoice no',
  `invoicedate` date NOT NULL,
  `containerno` varchar(20) NOT NULL,
  `fromcountry` varchar(20) NOT NULL COMMENT 'Country Of Origin Goods',
  `tocountry` varchar(20) NOT NULL COMMENT 'Country of Final Destination',
  `buyers` varchar(50) NOT NULL,
  `ifscno` varchar(20) NOT NULL,
  `flightno` varchar(20) NOT NULL,
  `carriageby` varchar(20) NOT NULL COMMENT 'Pre Carraige By',
  `otherref` varchar(100) NOT NULL,
  `receiptplace` varchar(20) NOT NULL COMMENT 'P{lace of receipt by Pre-carrier',
  `dischargeport` varchar(20) NOT NULL COMMENT 'Port of discharge',
  `loadingport` varchar(20) NOT NULL COMMENT 'Port Of Loading',
  `deliveryplace` varchar(20) NOT NULL COMMENT 'Place Of Delievery',
  `tod` varchar(100) NOT NULL,
  `top` varchar(100) NOT NULL,
  `hscodeid` float NOT NULL,
  `cif` float NOT NULL,
  `fob` float NOT NULL,
  `frieght` float NOT NULL,
  `insurance` double NOT NULL,
  `discount` float NOT NULL,
  `finalcif` float NOT NULL,
  `totalpackage` int(11) NOT NULL,
  `netweight` float NOT NULL,
  `grossweight` float NOT NULL,
  `totalqty` float NOT NULL,
  `totalexempted` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_invoices`
--

INSERT INTO `sr_invoices` (`id`, `proformaid`, `invoiceno`, `invoicedate`, `containerno`, `fromcountry`, `tocountry`, `buyers`, `ifscno`, `flightno`, `carriageby`, `otherref`, `receiptplace`, `dischargeport`, `loadingport`, `deliveryplace`, `tod`, `top`, `hscodeid`, `cif`, `fob`, `frieght`, `insurance`, `discount`, `finalcif`, `totalpackage`, `netweight`, `grossweight`, `totalqty`, `totalexempted`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 2, 'SHI/8wfM1/2016', '2016-05-31', '345678', 'india', 'netharlands', 'no', 'IFCS67', 'FLIGHT987', 'sea', 'no', 'dubai port', 'no discharge', 'DUBAI', 'dubai', 'Test Of Paymernt', 'Test of Delivery', 0.25, 15, 0, 0, 0, 0, 0, 45, 456, 85, 53, 13.25, 8, 8, '2016-05-31 23:34:13', '2016-05-31 23:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `sr_items`
--

CREATE TABLE `sr_items` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `od_nb` float NOT NULL,
  `wt_nb` varchar(100) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `length` int(11) NOT NULL,
  `gdmm` varchar(100) NOT NULL,
  `gdnb` varchar(100) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_items`
--

INSERT INTO `sr_items` (`id`, `standard_id`, `grade_id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `length`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 4, 4, 1, '6', 17.4, 5, 1.5376, 6, '17.4 MM X 1.65 MM X 6000 MM', '1/4" X SCH10S X 6 MTR', 5, 5, '2016-03-20 13:52:52', '2016-03-20 19:50:29'),
(3, 4, 0, 1, 'SCH10', 17.6, 6, 1.72608, 10, '17.6 MM X 1.69 MM X 10000 MM', '1/4" X SCH10 X 10 MTR', 5, 5, '2016-03-20 13:57:20', '2016-03-24 21:21:08'),
(4, 2, 1, 5, '4', 2, 1, 0.0248, 6, '63 MM X 5 MM X 6000 MM', '5" X 4 X 6 MTR', 5, 5, '2016-03-20 14:26:12', '2016-03-20 19:51:23'),
(6, 3, 3, 3, 'SCH40S', 17.1, 2.31, 0.84729, 6, '17.1 MM X 2.31 MM X 6000 MM', '3/8" X SCH40S X 6 MTR', 5, 5, '2016-04-03 22:14:19', '2016-04-03 22:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `sr_mechanicals`
--

CREATE TABLE `sr_mechanicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `tensile_strength` float NOT NULL,
  `yield_strength` float NOT NULL,
  `elolgation` float NOT NULL,
  `hardness` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_mechanicals`
--

INSERT INTO `sr_mechanicals` (`id`, `grade_id`, `tensile_strength`, `yield_strength`, `elolgation`, `hardness`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 2.3, 1.5, 6, 7, 5, 0, '2016-03-24 20:09:45', '2016-03-24 20:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `sr_orderitems`
--

CREATE TABLE `sr_orderitems` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `proformaitemid` int(11) NOT NULL,
  `procatid` int(11) NOT NULL,
  `productname` varchar(30) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_orderitems`
--

INSERT INTO `sr_orderitems` (`id`, `orderid`, `proformaitemid`, `procatid`, `productname`, `standard_id`, `grade_id`, `size_id`, `length`, `qty`, `price`, `amount`, `total`) VALUES
(1, 2, 52, 1, 'Seamless Product', 2, 7, 1, 6, 6, 5, 30, 0),
(2, 2, 52, 1, 'Seamless Product', 2, 7, 1, 6, 6, 5, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_orders`
--

CREATE TABLE `sr_orders` (
  `id` int(11) NOT NULL,
  `proformaid` int(11) NOT NULL,
  `orderno` varchar(50) NOT NULL,
  `orderdate` date NOT NULL,
  `tod` varchar(100) NOT NULL COMMENT 'Terms of Deilivery',
  `top` varchar(100) NOT NULL COMMENT 'Terms of Payment',
  `bill_to` int(11) NOT NULL COMMENT 'Bill to Client',
  `ship_to` varchar(255) NOT NULL COMMENT 'Ship to Client',
  `ship_address` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total` float NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - Pending, 1 - Approval  2 - Rejected',
  `approval_date` datetime NOT NULL,
  `approvalby` int(11) NOT NULL,
  `isverified` tinyint(2) NOT NULL DEFAULT '0',
  `verified_date` datetime NOT NULL,
  `verifiedby` int(11) NOT NULL,
  `rejectcomment` varchar(100) NOT NULL,
  `dispatch_id` int(11) NOT NULL,
  `dispatchcode` varchar(20) NOT NULL,
  `po` varchar(20) NOT NULL,
  `ismanual` tinyint(2) NOT NULL DEFAULT '0',
  `local` tinyint(2) NOT NULL,
  `priceid` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_orders`
--

INSERT INTO `sr_orders` (`id`, `proformaid`, `orderno`, `orderdate`, `tod`, `top`, `bill_to`, `ship_to`, `ship_address`, `bank_id`, `total`, `status`, `approval_date`, `approvalby`, `isverified`, `verified_date`, `verifiedby`, `rejectcomment`, `dispatch_id`, `dispatchcode`, `po`, `ismanual`, `local`, `priceid`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 51, 'SHLALCO/NvTA/2016', '0000-00-00', 'dxgsdf', 'sdff', 6, 'Friends Steel', 'Lamibto Road', 2, 260, 1, '2016-06-01 01:59:15', 8, 1, '2016-06-01 01:03:39', 8, '', 0, '', '', 0, 1, '1', 8, 8, '2016-05-22 19:13:58', '2016-06-01 01:59:15'),
(2, 52, 'SHLALCO/Q7Q2/2016', '0000-00-00', 'sdfs', 'fsf', 7, 'Tata', 'Tata House,All India Road', 2, 30, 1, '2016-06-01 02:01:20', 8, 1, '2016-06-01 00:35:09', 8, '', 4, 'TJ-phw52', '', 0, 1, '1', 8, 8, '2016-05-22 19:20:37', '2016-06-01 02:01:20');

-- --------------------------------------------------------

--
-- Table structure for table `sr_physicals`
--

CREATE TABLE `sr_physicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `annealing_f` float NOT NULL,
  `annealing_c` float NOT NULL,
  `density` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_physicals`
--

INSERT INTO `sr_physicals` (`id`, `grade_id`, `annealing_f`, `annealing_c`, `density`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 6.3, 4.1, 4.9, 5, 5, '2016-03-24 20:05:26', '2016-03-24 20:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `sr_prices`
--

CREATE TABLE `sr_prices` (
  `id` int(11) NOT NULL,
  `sign` varchar(10) NOT NULL,
  `fullform` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_prices`
--

INSERT INTO `sr_prices` (`id`, `sign`, `fullform`) VALUES
(1, '$', 'Dollar'),
(2, '?', 'Rupees');

-- --------------------------------------------------------

--
-- Table structure for table `sr_productcategories`
--

CREATE TABLE `sr_productcategories` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_productcategories`
--

INSERT INTO `sr_productcategories` (`id`, `name`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'Seamless', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(2, 'Welded', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(3, 'Round Bar', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(4, 'Sheets & Plates', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(5, 'Pipe Fitting', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformainvoices`
--

CREATE TABLE `sr_proformainvoices` (
  `id` int(11) NOT NULL,
  `proforma_no` varchar(50) NOT NULL,
  `proforma_date` date NOT NULL,
  `tod` varchar(100) NOT NULL COMMENT 'Terms of Deilivery',
  `top` varchar(100) NOT NULL COMMENT 'Terms of Payment',
  `bill_to` int(11) NOT NULL COMMENT 'Bill to Client',
  `ship_to` varchar(255) NOT NULL COMMENT 'Ship to Client',
  `ship_address` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total` float NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - Pending, 1 - Approval  2 - Rejected',
  `approval_date` datetime NOT NULL,
  `approvalby` int(11) NOT NULL,
  `isverified` tinyint(2) NOT NULL DEFAULT '0',
  `verified_date` datetime NOT NULL,
  `verifiedby` int(11) NOT NULL,
  `rejectcomment` varchar(100) NOT NULL,
  `dispatch_id` int(11) NOT NULL,
  `dispatchcode` varchar(20) NOT NULL,
  `po` varchar(20) NOT NULL,
  `ismanual` tinyint(2) NOT NULL DEFAULT '0',
  `orderno` varchar(50) NOT NULL,
  `local` tinyint(2) NOT NULL,
  `priceid` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformainvoices`
--

INSERT INTO `sr_proformainvoices` (`id`, `proforma_no`, `proforma_date`, `tod`, `top`, `bill_to`, `ship_to`, `ship_address`, `bank_id`, `total`, `status`, `approval_date`, `approvalby`, `isverified`, `verified_date`, `verifiedby`, `rejectcomment`, `dispatch_id`, `dispatchcode`, `po`, `ismanual`, `orderno`, `local`, `priceid`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'SHL/g17E1/2016', '2016-04-30', 'Test of  Delivery', 'Test of Payment', 7, 'Tata', 'Tata House,All India Road', 1, 140, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 0, '1', 7, 7, '2016-04-30 23:57:24', '2016-04-30 23:57:24'),
(2, 'SHL/TjCA2/2016', '2016-05-01', 'Test Of Paymernt', 'Test of Delivery', 7, 'Tata', 'Tata House,All India Road', 3, 15, 0, '2016-05-01 19:32:36', 8, 1, '2016-05-01 02:21:54', 7, '', 2, 'PP-1', '', 0, '', 0, '1', 8, 8, '2016-05-01 00:17:16', '2016-05-01 19:32:36'),
(3, 'SHL/OL/4tOL1/2016', '2016-05-01', '120 DAYS FROM OPENING OF L/C', 'L/C AT SIGHT.', 7, '7', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 00:31:28', '2016-05-01 00:31:28'),
(4, 'SHL/KoZH3/2016', '2016-05-01', 'Delivery On time', 'Payment On Time.', 6, 'Friends Steel', 'Lamibto Road', 1, 364, 0, '2016-05-01 19:32:51', 8, 1, '2016-05-01 02:39:15', 7, '', 0, '', '', 0, '', 0, '1', 7, 7, '2016-05-01 00:37:38', '2016-05-01 19:32:51'),
(5, 'SHL/OL/Hyfv2/2016', '2016-05-01', '120 DAYS FROM OPENING OF L/C', 'L/C AT SIGHT.', 7, '7', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 00:38:07', '2016-05-01 00:38:07'),
(6, 'SHL/OL/FZ6v3/2016', '2016-05-01', '120 DAYS FROM OPENING OF L/C', 'L/C AT SIGHT.', 7, '7', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 01:49:36', '2016-05-01 01:49:36'),
(7, 'SHL/OL/IG734/2016', '2016-05-01', '120 DAYS FROM OPENING OF L/C', 'L/C AT SIGHT.', 7, '7', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 16274.4, 0, '2016-05-01 19:47:15', 8, 1, '2016-05-01 02:41:42', 7, 'test regect', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 02:10:35', '2016-05-01 19:47:21'),
(8, 'SHL/OL/pUj75/2016', '2016-05-01', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 16274.4, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 11:19:16', '2016-05-01 11:19:16'),
(9, 'SHL/OL/oVSq6/2016', '2016-05-01', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 16274.4, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-01 11:21:07', '2016-05-01 11:21:07'),
(11, 'SHL/OL/CYuk8/2016', '2016-05-01', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 16274.4, 0, '2016-05-02 01:24:00', 8, 1, '2016-05-01 20:00:21', 8, '', 3, 'GW-1', '', 1, '', 0, '1', 0, 0, '2016-05-01 18:43:34', '2016-05-02 01:24:06'),
(12, 'SHL/OL/mSfU9/2016', '2016-05-01', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 16274.4, 0, '2016-06-01 00:27:37', 8, 1, '2016-05-01 19:49:05', 8, '', 2, 'PP-3', '', 1, 'SHLALCO/rMC3/2016', 0, '1', 0, 0, '2016-05-01 18:57:04', '2016-06-01 00:27:37'),
(13, 'SHL/OL/lAd610/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 1, 31334.4, 0, '2016-05-05 00:54:59', 8, 1, '2016-05-05 00:54:51', 8, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-03 01:13:15', '2016-05-05 00:54:59'),
(14, 'SHL/OL/lnKt11/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 31334.4, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-03 01:41:23', '2016-05-03 01:41:23'),
(15, 'SHL/OL/Sl9V12/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 2, 31334.4, 0, '2016-06-01 00:29:52', 8, 1, '2016-06-01 00:29:05', 8, '', 0, '', '', 1, 'SHLALCO/7uGN/2016', 0, '1', 8, 8, '2016-05-03 01:50:41', '2016-06-01 00:29:53'),
(16, 'SHL/OL/GMAi13/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 31334.4, 0, '2016-05-18 01:24:12', 8, 1, '2016-05-05 01:20:12', 8, '', 2, 'PP-gCVg3', '69', 1, 'SHLALCO/ca36/2016', 0, '1', 0, 0, '2016-05-03 01:56:15', '2016-05-18 01:24:23'),
(17, 'SHL/OL/8SQs14/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 0, 31334.4, 0, '2016-05-18 00:48:07', 8, 1, '2016-05-05 01:15:22', 8, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-03 01:57:00', '2016-05-18 00:48:07'),
(18, 'SHL/OL/joAC15/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 1, 31334.4, 0, '2016-05-18 00:48:05', 8, 1, '2016-05-05 01:14:36', 8, '', 0, '', '', 1, '', 0, '1', 0, 0, '2016-05-03 02:11:15', '2016-05-18 00:48:05'),
(19, 'SHL/OL/PmgT16/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 1, 31334.4, 0, '2016-05-18 00:47:56', 8, 1, '2016-05-05 01:13:35', 8, '', 0, '9', '', 1, '', 0, '1', 0, 0, '2016-05-03 02:27:16', '2016-05-18 00:47:56'),
(20, 'SHL/OL/NwNo17/2016', '2016-05-03', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 1, 31334.4, 0, '2016-05-05 00:57:01', 8, 1, '2016-05-05 00:56:51', 8, '', 3, 'GW-G8SQ2', '', 1, '', 0, '1', 0, 0, '2016-05-03 02:28:21', '2016-05-05 00:57:06'),
(21, 'SHL/OL/Gpnt18/2016', '2016-05-04', 'After 10 days', 'After Delivery', 7, 'Tata', 'C/Solsona, 3 - Pol. Ind. La Florida  Mahjarast ,India ', 1, 31334.4, 0, '2016-05-04 00:24:42', 7, 1, '2016-05-05 00:56:27', 8, '', 9, 'BW/lk2f/3', '', 1, '', 0, '1', 0, 0, '2016-05-04 00:24:43', '2016-05-05 00:56:27'),
(22, 'SHL/KCLD4/2016', '2016-05-19', 'on time delivery', 'on time payment', 7, 'Tata', 'Tata House,All India Road', 2, 40, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-19 00:23:38', '2016-05-19 00:23:38'),
(23, 'SHL/QpBh5/2016', '2016-05-20', 'terms of paymenr', 'petms od patd', 7, 'Tata', 'Tata House,All India Road', 2, 25, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-20 23:53:08', '2016-05-20 23:53:08'),
(24, 'SHL/4pMy6/2016', '2016-05-20', 'fsdf', 'sdf', 7, 'Tata', 'Tata House,All India Road', 2, 24, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-20 23:56:15', '2016-05-20 23:56:15'),
(29, 'SHL/0Wp97/2016', '2016-05-21', 'DSF', 'FSD', 7, 'Tata', 'Tata House,All India Road', 2, 30, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-21 00:44:14', '2016-05-21 00:44:14'),
(31, 'SHL/xoDQ8/2016', '2016-05-21', 'sdf', 'fsd', 7, 'Tata', 'Tata House,All India Road', 2, 30, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 0, '1', 8, 8, '2016-05-21 00:48:18', '2016-05-21 00:48:18'),
(32, 'SHL/GbIC9/2016', '2016-05-21', 'sddas', 'sada', 7, 'Tata', 'Tata House,All India Road', 2, 48, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-21 01:04:48', '2016-05-21 01:04:48'),
(33, 'SHL/tqAB10/2016', '2016-05-21', 'sdafs', 'saf', 7, 'Tata', 'Tata House,All India Road', 2, 30, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-21 01:06:04', '2016-05-21 01:06:04'),
(34, 'SHL/gwTx11/2016', '2016-05-21', 'dvx', 'sad', 7, 'Tata', 'Tata House,All India Road', 2, 32, 0, '0000-00-00 00:00:00', 0, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, '', 1, '1', 8, 8, '2016-05-21 01:09:55', '2016-05-21 01:09:55'),
(39, 'SHL/aySj12/2016', '2016-05-22', 'terms of delivery', 'terms of payment', 6, 'Friends Steel', 'Lamibto Road', 2, 260, 1, '2016-06-01 01:51:01', 8, 1, '0000-00-00 00:00:00', 0, '', 0, '', '', 0, 'SHLALCO/3fge/2016', 1, '1', 8, 8, '2016-05-22 11:58:23', '2016-06-01 01:51:01'),
(40, 'SHL/G7Rv13/2016', '2016-05-22', 'rdgsdsdfsdf', 'fsd', 6, 'Friends Steel', 'Lamibto Road', 2, 324, 1, '2016-06-01 01:54:38', 8, 1, '0000-00-00 00:00:00', 0, '', 6, 'KW-tIbN1', '', 0, 'SHLALCO/vCEk/2016', 1, '1', 8, 8, '2016-05-22 12:01:08', '2016-06-01 01:54:45'),
(41, 'SHL/9a6h14/2016', '2016-05-22', 'dsfssd', 'fefs', 7, 'Tata', 'Tata House,All India Road', 2, 80, 1, '2016-06-01 01:57:01', 8, 1, '0000-00-00 00:00:00', 0, '', 8, 'KBGD2-pduh2', '', 0, 'SHLALCO/BhIX/2016', 1, '1', 8, 8, '2016-05-22 12:04:55', '2016-06-01 01:57:09'),
(42, 'SHL/M12g15/2016', '2016-05-22', 'sdefsdf', 'fgdsfds', 7, 'Tata', 'Tata House,All India Road', 2, 100, 1, '2016-06-01 01:57:27', 8, 1, '2016-06-01 01:12:34', 8, '', 7, 'KBGD1-3Wo21', '', 0, 'SHLALCO/lRSV/2016', 1, '1', 8, 8, '2016-05-22 12:06:19', '2016-06-01 01:57:34'),
(43, 'SHL/sTo416/2016', '2016-05-22', '15 day before t', 'on time Paymenrt', 7, 'Tata', 'Tata House,All India Road', 3, 1650, 0, '2016-05-22 16:19:45', 8, 1, '2016-05-22 16:19:36', 8, '', 8, 'KBGD2-dsNq1', 'PO250', 0, 'SHLALCO/Gn2j/2016', 1, '1', 8, 8, '2016-05-22 13:10:04', '2016-05-22 17:01:08'),
(44, 'SHL/KBdf17/2016', '2016-05-22', 'test', 'test', 7, 'Tata', 'Tata House,All India Road', 2, 490, 0, '2016-06-01 00:33:13', 8, 1, '2016-06-01 00:33:06', 8, 'no reason', 0, '', '', 0, 'SHLALCO/1U8I/2016', 1, '1', 8, 8, '2016-05-22 17:20:54', '2016-06-01 00:33:19'),
(51, 'SHL/yTES18/2016', '2016-05-22', 'dxgsdf', 'sdff', 6, 'Friends Steel', 'Lamibto Road', 2, 260, 1, '2016-06-01 01:59:15', 8, 1, '2016-06-01 01:03:39', 8, '', 0, '', '', 0, 'SHLALCO/NvTA/2016', 1, '1', 8, 8, '2016-05-22 19:13:58', '2016-06-01 01:59:15'),
(52, 'SHL/kKcY19/2016', '2016-05-22', 'sdfs', 'fsf', 7, 'Tata', 'Tata House,All India Road', 2, 30, 1, '2016-06-01 02:01:20', 8, 1, '2016-06-01 00:35:09', 8, '', 4, 'TJ-phw52', '', 0, 'SHLALCO/Q7Q2/2016', 1, '1', 8, 8, '2016-05-22 19:20:37', '2016-06-01 02:01:20'),
(53, 'SHL/aGb720/2016', '2016-05-22', 'hgfyjk', 'rdfhgdhdh', 7, 'Tata', 'Tata House,All India Road', 2, 40, 0, '2016-06-01 00:31:03', 8, 1, '2016-06-01 00:29:41', 8, '', 4, 'TJ-ZHPa1', 'test', 0, 'SHLALCO/Gvl1/2016', 0, '1', 8, 8, '2016-05-22 19:24:11', '2016-06-01 00:31:18');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformaitems`
--

CREATE TABLE `sr_proformaitems` (
  `id` int(11) NOT NULL,
  `proforma_id` int(11) NOT NULL,
  `procatid` int(11) NOT NULL,
  `productname` varchar(30) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformaitems`
--

INSERT INTO `sr_proformaitems` (`id`, `proforma_id`, `procatid`, `productname`, `standard_id`, `grade_id`, `size_id`, `length`, `qty`, `price`, `amount`, `total`) VALUES
(1, 1, 0, '', 1, 1, 1, 6, 8, 4, 32, 0),
(2, 1, 0, '', 2, 7, 2, 6, 9, 12, 108, 0),
(3, 2, 0, '', 2, 7, 2, 6, 3, 5, 15, 0),
(5, 3, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(6, 3, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(7, 3, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(8, 3, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(9, 4, 0, '', 2, 6, 1, 6, 9, 36, 324, 0),
(11, 5, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(12, 5, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(13, 5, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(14, 5, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(15, 5, 0, '', 3, 10, 9, 6, 4, 125, 500, 0),
(16, 5, 0, '', 3, 10, 1, 6, 5, 42, 210, 0),
(17, 5, 0, '', 1, 1, 2, 6, 9, 5, 45, 0),
(18, 4, 0, '', 2, 7, 2, 6, 8, 5, 40, 0),
(19, 6, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(20, 6, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(21, 6, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(22, 6, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(23, 7, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(24, 7, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(25, 7, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(26, 7, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(27, 8, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(28, 8, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(29, 8, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(30, 8, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(31, 9, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(32, 9, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(33, 9, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(34, 9, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(35, 10, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(36, 10, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(37, 10, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(38, 10, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(39, 11, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(40, 11, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(41, 11, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(42, 11, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(43, 12, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(44, 12, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(45, 12, 0, '', 1, 4, 9, 6, 1500, 3.02, 4530, 0),
(46, 12, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(47, 13, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(48, 13, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(49, 13, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(50, 13, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(51, 13, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(52, 13, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(53, 14, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(54, 14, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(55, 14, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(56, 14, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(57, 14, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(58, 14, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(59, 15, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(60, 15, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(61, 15, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(62, 15, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(63, 15, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(64, 15, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(65, 16, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(66, 16, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(67, 16, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(68, 16, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(69, 16, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(70, 16, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(71, 17, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(72, 17, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(73, 17, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(74, 17, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(75, 17, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(76, 17, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(77, 18, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(78, 18, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(79, 18, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(80, 18, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(81, 18, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(82, 18, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(83, 19, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(84, 19, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(85, 19, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(86, 19, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(87, 19, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(88, 19, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(89, 20, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(90, 20, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(91, 20, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(92, 20, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(93, 20, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(94, 20, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(95, 21, 0, '', 2, 3, 1, 7, 504, 2.35, 1184.4, 0),
(96, 21, 0, '', 2, 3, 2, 6, 1500, 2.02, 3030, 0),
(97, 21, 0, '', 1, 3, 9, 6, 1500, 3.02, 4530, 0),
(98, 21, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(99, 21, 0, '', 1, 4, 10, 6, 1500, 5.02, 7530, 0),
(100, 21, 0, '', 6, 4, 10, 6, 1500, 5.02, 7530, 0),
(101, 22, 3, 'Seamless Product', 1, 2, 2, 6, 5, 8, 40, 0),
(102, 23, 1, 'Seamless Product', 2, 6, 1, 6, 5, 5, 25, 0),
(103, 24, 2, 'welded', 2, 7, 1, 6, 3, 8, 24, 0),
(104, 25, 1, 'Seamless Product', 2, 6, 1, 9, 36, 6, 216, 0),
(105, 25, 1, 'Seamless Product', 2, 6, 1, 9, 36, 6, 216, 0),
(106, 25, 1, 'Seamless Product', 2, 6, 1, 9, 36, 6, 216, 0),
(107, 26, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(108, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(109, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(110, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(111, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(112, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(113, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(114, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(115, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(116, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(117, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(118, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(119, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(120, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(121, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(122, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(123, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(124, 27, 1, 'Seamless Product', 2, 6, 1, 6, 65, 6, 390, 0),
(125, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(126, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(127, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(128, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(129, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(130, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(131, 28, 1, 'Seamless Product', 2, 6, 1, 6, 6, 6, 36, 0),
(132, 29, 1, 'Seamless Product', 2, 6, 1, 9, 6, 5, 30, 0),
(133, 29, 1, 'Seamless Product', 2, 6, 1, 9, 6, 5, 30, 0),
(134, 30, 1, 'welded', 2, 6, 1, 3, 5, 8, 40, 0),
(135, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(136, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(137, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(138, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(139, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(140, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(141, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(142, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(143, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(149, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(150, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(151, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(152, 31, 1, 'welded', 2, 6, 1, 9, 6, 5, 30, 0),
(153, 32, 1, 'Seamless Product', 2, 6, 1, 6, 8, 6, 48, 0),
(154, 32, 1, 'Seamless Product', 2, 6, 1, 6, 8, 6, 48, 0),
(155, 32, 1, 'Seamless Product', 2, 6, 1, 6, 8, 6, 48, 0),
(156, 33, 1, 'Seamless Product', 2, 6, 1, 3, 6, 5, 30, 0),
(157, 33, 1, 'Seamless Product', 2, 6, 1, 3, 6, 5, 30, 0),
(158, 33, 1, 'Seamless Product', 2, 6, 1, 3, 6, 5, 30, 0),
(159, 34, 1, 'Seamless Product', 2, 6, 1, 6, 8, 4, 32, 0),
(160, 34, 1, 'Seamless Product', 2, 6, 1, 6, 8, 4, 32, 0),
(161, 34, 1, 'Seamless Product', 2, 6, 1, 6, 8, 4, 32, 0),
(162, 34, 1, 'Seamless Product', 2, 6, 1, 6, 8, 4, 32, 0),
(163, 34, 1, 'Seamless Product', 2, 6, 1, 6, 8, 4, 32, 0),
(164, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(165, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(166, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(167, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(168, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(169, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(170, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(171, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(172, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(173, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(174, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(175, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(176, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(177, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(178, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(179, 35, 1, 'Seamless Product', 2, 6, 1, 9, 64, 45, 2880, 0),
(180, 36, 1, 'saf', 2, 6, 1, 6, 5, 7, 35, 0),
(181, 36, 1, 'saf', 2, 6, 1, 6, 5, 7, 35, 0),
(182, 36, 1, 'saf', 2, 6, 1, 6, 5, 7, 35, 0),
(183, 36, 1, 'saf', 2, 6, 1, 6, 5, 7, 35, 0),
(184, 37, 1, 'Seamless Product', 2, 6, 1, 6, 5, 8, 40, 0),
(185, 37, 1, 'Seamless Product', 2, 6, 1, 6, 5, 8, 40, 0),
(186, 37, 1, 'Seamless Product', 2, 6, 1, 6, 5, 8, 40, 0),
(187, 37, 1, 'Seamless Product', 2, 6, 1, 6, 5, 8, 40, 0),
(188, 38, 1, 'Seamless Product', 2, 7, 1, 6, 5, 52, 260, 0),
(189, 39, 1, 'Seamless Product', 2, 7, 1, 6, 5, 52, 260, 0),
(190, 39, 1, 'Seamless Product', 2, 7, 1, 6, 5, 52, 260, 0),
(191, 40, 4, 'Seamless Product', 1, 4, 9, 6, 6, 54, 324, 0),
(192, 40, 4, 'Seamless Product', 1, 4, 9, 6, 6, 54, 324, 0),
(193, 41, 3, 'Seamless Product', 1, 4, 2, 6, 4, 20, 80, 0),
(194, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(195, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(196, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(197, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(198, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(199, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(200, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(201, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(202, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(203, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(204, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(205, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(206, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(207, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(208, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(209, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(210, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(211, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(212, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(213, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(214, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(215, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(216, 42, 4, 'welded', 1, 4, 9, 6, 5, 20, 100, 0),
(217, 43, 3, 'Seam', 1, 4, 2, 6, 5, 8, 40, 0),
(218, 43, 2, 'Seamless 2', 1, 3, 9, 6, 7, 230, 1610, 0),
(219, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(220, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(221, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(222, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(223, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(224, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(225, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(226, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(227, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(228, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(229, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(230, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(231, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(232, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(233, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(234, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(235, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(236, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(237, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(238, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(239, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(240, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(241, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(242, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(243, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(244, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(245, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(246, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(247, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(248, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(249, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(250, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(251, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(252, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(253, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(254, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(255, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(256, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(257, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(258, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(259, 44, 1, 'welded', 2, 7, 2, 6, 5, 98, 490, 0),
(260, 45, 4, 'test', 2, 7, 9, 6, 6, 6, 36, 0),
(261, 46, 2, 'welded', 1, 3, 9, 6, 8, 5, 40, 0),
(262, 47, 4, 'Seamless Product', 2, 7, 1, 6, 5, 6, 30, 0),
(263, 47, 1, 'Seamless 2', 2, 7, 9, 9, 5, 4, 20, 0),
(264, 47, 4, 'Seamless Product', 2, 7, 1, 6, 5, 6, 30, 0),
(265, 47, 1, 'Seamless 2', 2, 7, 9, 9, 5, 4, 20, 0),
(266, 48, 2, 'welded', 2, 8, 2, 8, 9, 5, 45, 0),
(267, 49, 1, 'Seam', 2, 6, 1, 6, 5, 6, 30, 0),
(268, 50, 1, 'Seamless Product', 2, 6, 1, 9, 10, 12, 120, 0),
(269, 50, 1, 'Seamless Product', 2, 6, 1, 9, 10, 12, 120, 0),
(270, 51, 1, 'Seamless Product', 2, 6, 1, 6, 52, 5, 260, 0),
(271, 52, 1, 'Seamless Product', 2, 7, 1, 6, 6, 5, 30, 0),
(272, 52, 1, 'Seamless Product', 2, 7, 1, 6, 6, 5, 30, 0),
(273, 53, 2, 'saf', 1, 2, 2, 6, 5, 8, 40, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_settings`
--

CREATE TABLE `sr_settings` (
  `id` int(11) NOT NULL,
  `office_address` varchar(255) NOT NULL,
  `factory_address` varchar(255) NOT NULL,
  `signature` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_settings`
--

INSERT INTO `sr_settings` (`id`, `office_address`, `factory_address`, `signature`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '24, 2nd FLR., 56, DAYABHAI BUILDING 	\r\nGULALWADI, MUMBAI-400004	\r\nMAHARASHTRA ( INDIA )  ', 'B-11, MIDC Mahad, Dist.Raigad,Pin code-402301,	\r\nMaharashtra, India	\r\n', 'Screenshot-2015-11-09-21.25.56.png', 5, 5, '0000-00-00 00:00:00', '2016-05-29 17:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `sr_sizes`
--

CREATE TABLE `sr_sizes` (
  `id` int(11) NOT NULL,
  `od_nb` varchar(20) NOT NULL,
  `wt_nb` varchar(20) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `gdmm` varchar(35) NOT NULL,
  `gdnb` varchar(35) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_sizes`
--

INSERT INTO `sr_sizes` (`id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '12', '25', 32, 6, 3.8688, '32 MM X 6 MM', '12 X 25', 5, 5, '2016-04-05 19:02:49', '2016-04-05 19:13:00'),
(2, '1/4', '25', 13.4, 1.67, 0.48581, '13.4 MM X 1.67 MM ', '1/4 X 25', 5, 5, '2016-04-05 20:15:35', '2016-04-05 20:15:35'),
(9, '1/4', '12', 91.2, 8.15, 16.7861, '91.2 MM X 8.15 MM', '1/4 X 12', 7, 0, '2016-05-01 00:31:28', '2016-05-01 00:31:28'),
(10, '1/4', '12', 40.2, 2.73, 2.53687, '40.2 MM X 2.73 MM', '1/4 X 12', 7, 0, '2016-05-01 00:31:28', '2016-05-01 00:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `sr_standards`
--

CREATE TABLE `sr_standards` (
  `id` int(11) NOT NULL,
  `procatid` varchar(20) NOT NULL,
  `standard_name` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_standards`
--

INSERT INTO `sr_standards` (`id`, `procatid`, `standard_name`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '2,3,4', 'A313', 1, 8, 8, '2016-04-04 00:00:00', '2016-06-01 01:57:31'),
(2, '1,2,4,5', 'A312', 1, 8, 8, '2016-04-04 00:00:00', '2016-06-01 02:01:21'),
(3, '1,2,3,4,5', 'A249', 1, 8, 8, '2016-04-04 00:00:00', '2016-05-18 22:56:44'),
(4, '1,2,3,4', 'A268', 1, 8, 8, '2016-04-04 00:00:00', '2016-05-18 22:57:02'),
(5, '0', 'A269', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(6, '1', 'a123', 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '1,3,4', 'A345', 1, 8, 8, '2016-04-30 01:20:38', '2016-05-18 22:55:41'),
(9, '1,2,3', 'A 123', 1, 8, 8, '2016-05-17 23:33:07', '2016-05-18 23:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `sr_users`
--

CREATE TABLE `sr_users` (
  `id` int(11) NOT NULL COMMENT 'User id ',
  `usertype_id` int(11) NOT NULL COMMENT '1 = admin, 2 = HR, 3 = Supervisor, 4 = Employee, 5 = Secondary Supervisor',
  `first_name` varchar(30) NOT NULL COMMENT 'User First Name',
  `middle_name` varchar(30) NOT NULL COMMENT 'User Middle Name',
  `last_name` varchar(30) NOT NULL COMMENT 'User Last Name',
  `preferred_name` varchar(50) NOT NULL COMMENT 'User Preferred Name',
  `username` varchar(50) NOT NULL COMMENT 'User Name',
  `email` varchar(100) NOT NULL COMMENT 'User Email',
  `mobile` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT 'User Password',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 = Male, 1 = Female',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `supervisor` int(11) DEFAULT NULL COMMENT 'Supervisor ID',
  `signature` varchar(100) NOT NULL COMMENT 'User Signature',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active',
  `token` varchar(50) NOT NULL COMMENT 'User Information token',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_users`
--

INSERT INTO `sr_users` (`id`, `usertype_id`, `first_name`, `middle_name`, `last_name`, `preferred_name`, `username`, `email`, `mobile`, `password`, `gender`, `dob`, `supervisor`, `signature`, `status`, `token`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'Hemendra', '', 'Bothra', '', 'hemendra', 'hemendra@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(2, 1, 'Vikas', '', 'Bothra', '', 'vikas', 'vikas@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '1991-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(3, 1, 'Udit', '', 'bhanshali', '', 'udit@gmail.com', 'udit@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(4, 1, 'Ramesh', '', 'Bothra', '', 'ramesh@gmail.com', 'ramesh@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(5, 1, 'jay', '', 'shah', '', 'jay@gmail.com', 'jay@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(6, 1, 'admin', '', 'admin', '', 'admin@admin.com', 'admin@admin.com', '453543545', '75874572ba20024ef8283f6d7ad6df2f82fe998f', 0, '2016-04-06', NULL, '', 1, '', 0, 0, '2016-04-08 21:15:40', '2016-04-09 13:43:35'),
(7, 1, 'chirag', '', 'shah', '', 'chirag@gmail.com', 'chirag@gmail.com', '7878473171', 'b811596cb8f96e7142839b8865cc420421863140', 0, '2011-07-22', NULL, '', 1, '', 0, 0, '2016-04-08 22:05:30', '2016-04-30 22:58:49'),
(8, 1, 'Kaushal', '', 'Shah', '', 'kaushal@gmail.com', 'kaushal@gmail.com', '7878473172', '1391fae89de2306617b69b6aab9d1933896a6e34', 0, '2005-03-10', NULL, '', 1, '', 0, 0, '2016-04-29 22:49:53', '2016-05-02 23:53:06'),
(9, 1, 'jainam', '', 'shah', '', 'jainam@gmail.com', 'jainam@gmail.com', '9898989898', 'b811596cb8f96e7142839b8865cc420421863140', 0, '2000-02-09', NULL, '', 1, '', 0, 0, '2016-04-30 00:03:11', '2016-04-30 00:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `sr_usertypes`
--

CREATE TABLE `sr_usertypes` (
  `id` int(11) NOT NULL,
  `usertype_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_usertypes`
--

INSERT INTO `sr_usertypes` (`id`, `usertype_name`) VALUES
(1, 'Admin'),
(2, 'HR'),
(3, 'Supervisor'),
(4, 'Employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sr_banks`
--
ALTER TABLE `sr_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_clients`
--
ALTER TABLE `sr_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_dispatches`
--
ALTER TABLE `sr_dispatches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_grades`
--
ALTER TABLE `sr_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `standard_id` (`standard_id`);

--
-- Indexes for table `sr_hscodes`
--
ALTER TABLE `sr_hscodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_invoiceitems`
--
ALTER TABLE `sr_invoiceitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_invoices`
--
ALTER TABLE `sr_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_items`
--
ALTER TABLE `sr_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_orderitems`
--
ALTER TABLE `sr_orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_orders`
--
ALTER TABLE `sr_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_prices`
--
ALTER TABLE `sr_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_productcategories`
--
ALTER TABLE `sr_productcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_settings`
--
ALTER TABLE `sr_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_standards`
--
ALTER TABLE `sr_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_users`
--
ALTER TABLE `sr_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type` (`usertype_id`);

--
-- Indexes for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sr_banks`
--
ALTER TABLE `sr_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_clients`
--
ALTER TABLE `sr_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sr_dispatches`
--
ALTER TABLE `sr_dispatches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_grades`
--
ALTER TABLE `sr_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sr_hscodes`
--
ALTER TABLE `sr_hscodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_invoiceitems`
--
ALTER TABLE `sr_invoiceitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_invoices`
--
ALTER TABLE `sr_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_items`
--
ALTER TABLE `sr_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_orderitems`
--
ALTER TABLE `sr_orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_orders`
--
ALTER TABLE `sr_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_prices`
--
ALTER TABLE `sr_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_productcategories`
--
ALTER TABLE `sr_productcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;
--
-- AUTO_INCREMENT for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sr_standards`
--
ALTER TABLE `sr_standards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_users`
--
ALTER TABLE `sr_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id ', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
