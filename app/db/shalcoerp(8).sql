-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2016 at 07:05 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shalcoerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sr_banks`
--

CREATE TABLE `sr_banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `swift_code` varchar(50) NOT NULL,
  `ad_code` varchar(50) NOT NULL,
  `ac_no` int(20) NOT NULL,
  `ac_name` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_banks`
--

INSERT INTO `sr_banks` (`id`, `bank_name`, `branch_name`, `swift_code`, `ad_code`, `ac_no`, `ac_name`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 'Axix Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'AXIXBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(3, 'HDFC Bank\r\n\r\n\r\n\r\n', 'Hamilton Road', 'HDBCBINBB', '0469102', 28654825, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(4, 'SBI\r\n\r\n\r\n\r\n', 'Georg Road', 'SBI54BINBB', '0411102', 254564455, 'Shalco industries', 3, 3, '2016-04-10 00:00:00', '2016-04-10 00:00:00'),
(5, 'UCO Bank', 'Hill Road', 'UCOBINBB', '0469102', 28654825, 'Shalco industries', 8, 8, '2016-04-10 00:00:00', '2016-05-01 23:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `sr_chemicals`
--

CREATE TABLE `sr_chemicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `c_mn` float NOT NULL,
  `c_mx` float NOT NULL,
  `mn_mn` float NOT NULL,
  `mn_mx` float NOT NULL,
  `p_mn` float NOT NULL,
  `p_mx` float NOT NULL,
  `s_mn` float NOT NULL,
  `s_mx` float NOT NULL,
  `si_mn` float NOT NULL,
  `si_mx` float NOT NULL,
  `cr_mn` float NOT NULL,
  `cr_mx` float NOT NULL,
  `ni_mn` float NOT NULL,
  `ni_mx` float NOT NULL,
  `mo_mn` float NOT NULL,
  `mo_mx` float NOT NULL,
  `n_mx` float NOT NULL,
  `n_mn` float NOT NULL,
  `fe_mn` float NOT NULL,
  `fe_mx` float NOT NULL,
  `nb_mn` float NOT NULL,
  `nb_mx` float NOT NULL,
  `ti_mn` float NOT NULL,
  `ti_mx` float NOT NULL,
  `cu_mn` float NOT NULL,
  `cu_mx` float NOT NULL,
  `v_mn` float NOT NULL,
  `v_mx` float NOT NULL,
  `b_mn` float NOT NULL,
  `b_mx` float NOT NULL,
  `ai_mn` float NOT NULL,
  `ai_mx` float NOT NULL,
  `ce_mn` float NOT NULL,
  `ce_mx` float NOT NULL,
  `co_mn` float NOT NULL,
  `co_mx` float NOT NULL,
  `w_mn` float NOT NULL,
  `w_mx` float NOT NULL,
  `la_mn` float NOT NULL,
  `la_mx` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_chemicals`
--

INSERT INTO `sr_chemicals` (`id`, `grade_id`, `c_mn`, `c_mx`, `mn_mn`, `mn_mx`, `p_mn`, `p_mx`, `s_mn`, `s_mx`, `si_mn`, `si_mx`, `cr_mn`, `cr_mx`, `ni_mn`, `ni_mx`, `mo_mn`, `mo_mx`, `n_mx`, `n_mn`, `fe_mn`, `fe_mx`, `nb_mn`, `nb_mx`, `ti_mn`, `ti_mx`, `cu_mn`, `cu_mx`, `v_mn`, `v_mx`, `b_mn`, `b_mx`, `ai_mn`, `ai_mx`, `ce_mn`, `ce_mx`, `co_mn`, `co_mx`, `w_mn`, `w_mx`, `la_mn`, `la_mx`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 3, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '2016-03-24 20:04:24', '2016-03-24 20:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `sr_clients`
--

CREATE TABLE `sr_clients` (
  `id` int(11) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mob` varchar(20) NOT NULL,
  `contact_person` varchar(20) NOT NULL,
  `contact_num1` varchar(20) NOT NULL,
  `contact_num2` varchar(20) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `coordinator` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_clients`
--

INSERT INTO `sr_clients` (`id`, `company_name`, `email`, `mob`, `contact_person`, `contact_num1`, `contact_num2`, `fax`, `website`, `address1`, `address2`, `coordinator`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 'f', 'test@gmail.com', 'fd', 'dfsd', 'fdf', 'sfdg', 'dfs', 'https://www.google.co.in/search?', 'sd', 'fsd', 0, 1, 5, 5, '2016-03-17 19:54:36', '2016-04-09 13:41:56'),
(3, 'sad', 'fas', 'fsa', 'fsa', 'saf', 'fas', 'fas', 'vdfvds', 'f', 'fsa', 0, 1, 5, 5, '2016-03-18 18:26:08', '2016-03-18 18:26:20'),
(4, 'test', 'test3g@gmail.com', '1234567890', 'test', '1234567890', '65156', 'fds', 'rmsb.com', 'f', 'sda', 0, 1, 5, 5, '2016-03-18 19:15:26', '2016-03-18 19:21:23'),
(5, 'Ureka', 'mnj@gmail.com', '1234567890', 'Manoj Desai', '122343454', '34234554`', 'test', 'test.com', 'test', 'tewst', 7, 1, 7, 7, '2016-04-08 21:03:30', '2016-04-25 22:20:09'),
(6, 'Friends Steel', 'steel@gmail.com', '35435', '334342423', '1234324', '23222432', 'tewrt', 'freinds.com', 'Lamibto Road', 'Hmsailton', 0, 1, 5, 5, '2016-04-08 21:11:01', '2016-04-08 21:11:01'),
(7, 'Tata', 'tata@gmsail.com', '45435435', 'Ratan Tata', '43543545', '534543543', 'Tat', 'https://www.tata.com', 'Tata House,All India Road', 'Tata House All India Road 2', 5, 1, 7, 7, '2016-04-11 01:09:09', '2016-04-11 01:17:28'),
(8, 'Paul Meijering', 'paul@gmail.com', '9865321254', 'Paul', '9865321254', '9865321254', 'paul-fax', 'paul.com', 'Van voodenpark 10, 5301 KP Zaltbommel,The Netharlands', 'Van voodenpark 10, 5301 KP Zaltbommel,The Netharlands', 1, 1, 8, 8, '2016-06-05 11:36:59', '2016-06-05 11:36:59');

-- --------------------------------------------------------

--
-- Table structure for table `sr_dispatches`
--

CREATE TABLE `sr_dispatches` (
  `id` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  `clientpo` varchar(20) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_dispatches`
--

INSERT INTO `sr_dispatches` (`id`, `location`, `code`, `clientpo`, `comment`) VALUES
(1, 'Kalamboli', 'KB', '', ''),
(2, 'Panjrapole', 'PP', '', ''),
(3, 'Gulalwadi', 'GW', '', ''),
(4, 'Taloja', 'TJ', '', ''),
(5, 'Mahad', 'MD', '', ''),
(6, 'Kumbharwada', 'KW', '', ''),
(7, 'Kalamboli Godown 1', 'KBGD1', '', ''),
(8, 'Kalamboli Godown 2', 'KBGD2', '', ''),
(9, 'Boriwali', 'BW', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sr_grades`
--

CREATE TABLE `sr_grades` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_name` varchar(20) NOT NULL,
  `uns` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_grades`
--

INSERT INTO `sr_grades` (`id`, `standard_id`, `grade_name`, `uns`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'TP201', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(2, 1, 'TP202', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(3, 1, 'TP304/304L', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-05 11:43:49'),
(4, 1, 'TP303/303L', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-05 11:43:49'),
(5, 1, 'TP304H', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(6, 2, 'TP201', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-01 01:09:51'),
(7, 2, 'TP201LN', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-06-05 11:43:49'),
(8, 2, 'TP304', '10', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_hscodes`
--

CREATE TABLE `sr_hscodes` (
  `id` int(11) NOT NULL,
  `hscode` varchar(50) NOT NULL,
  `rate` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_hscodes`
--

INSERT INTO `sr_hscodes` (`id`, `hscode`, `rate`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'test', 0.3, 5, 5, '2016-05-29 17:29:28', '2016-05-29 17:30:32'),
(2, 'test2', 0.25, 5, 5, '2016-05-29 17:31:15', '2016-05-29 17:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `sr_invoiceitems`
--

CREATE TABLE `sr_invoiceitems` (
  `id` int(11) NOT NULL,
  `invoiceid` int(11) NOT NULL,
  `orderitemid` int(11) NOT NULL,
  `bundleno` varchar(20) NOT NULL,
  `heatnumber` varchar(30) NOT NULL,
  `pcs` int(11) NOT NULL,
  `exempted` float NOT NULL,
  `unit_mt` float NOT NULL,
  `price` float NOT NULL,
  `netprice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_invoiceitems`
--

INSERT INTO `sr_invoiceitems` (`id`, `invoiceid`, `orderitemid`, `bundleno`, `heatnumber`, `pcs`, `exempted`, `unit_mt`, `price`, `netprice`) VALUES
(1, 1, 1, 'Bundle no.-1,2,55', '63', 104, 3, 12, 60.5, 726),
(2, 1, 2, 'bundle No.-3,4,5', '63', 70, 4, 16, 75.4, 1206.4),
(3, 1, 3, 'Bundle No.-10 To 15', '63', 128, 4.5, 18, 80.5, 1449),
(4, 1, 4, 'Bundle No.- 6,3,5', '63', 2, 5, 20, 85.4, 1708),
(5, 1, 5, 'Bunde No.- 1,2,3', '63', 9, 5.75, 23, 63.3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_invoices`
--

CREATE TABLE `sr_invoices` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `invoiceno` varchar(30) NOT NULL COMMENT 'Invoice no',
  `invoicedate` date NOT NULL,
  `containerno` varchar(20) NOT NULL,
  `fromcountry` varchar(20) NOT NULL COMMENT 'Country Of Origin Goods',
  `tocountry` varchar(20) NOT NULL COMMENT 'Country of Final Destination',
  `buyers` varchar(50) NOT NULL,
  `ifscno` varchar(20) NOT NULL,
  `flightno` varchar(20) NOT NULL,
  `carriageby` varchar(20) NOT NULL COMMENT 'Pre Carraige By',
  `otherref` varchar(100) NOT NULL,
  `receiptplace` varchar(20) NOT NULL COMMENT 'P{lace of receipt by Pre-carrier',
  `dischargeport` varchar(20) NOT NULL COMMENT 'Port of discharge',
  `loadingport` varchar(20) NOT NULL COMMENT 'Port Of Loading',
  `deliveryplace` varchar(20) NOT NULL COMMENT 'Place Of Delievery',
  `tod` varchar(100) NOT NULL,
  `top` varchar(100) NOT NULL,
  `hscodeid` float NOT NULL,
  `cif` float NOT NULL,
  `fob` float NOT NULL,
  `frieght` float NOT NULL,
  `insurance` double NOT NULL,
  `discount` float NOT NULL,
  `finalcif` float NOT NULL,
  `totalpackage` float NOT NULL,
  `netweight` float NOT NULL,
  `grossweight` float NOT NULL,
  `totalqty` float NOT NULL,
  `totalexempted` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_invoices`
--

INSERT INTO `sr_invoices` (`id`, `orderid`, `invoiceno`, `invoicedate`, `containerno`, `fromcountry`, `tocountry`, `buyers`, `ifscno`, `flightno`, `carriageby`, `otherref`, `receiptplace`, `dischargeport`, `loadingport`, `deliveryplace`, `tod`, `top`, `hscodeid`, `cif`, `fob`, `frieght`, `insurance`, `discount`, `finalcif`, `totalpackage`, `netweight`, `grossweight`, `totalqty`, `totalexempted`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'SHI/9eVm1/2016', '2016-06-05', 'SEGU 7313644', 'INDIA', 'NETHARLANDS', 'no', '0302042369', 'First Available', 'SEA', 'no', 'Mahad,Dist.-RAIGAD', 'ROTTEDAM', 'JNPT', 'The Netharlands', 'CIF', '30 days after B/L', 0.25, 6526, 0, 0, 0, 0, 0, 45.365, 45.137, 34, 66, 16.5, 8, 8, '2016-06-05 16:01:59', '2016-06-05 17:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `sr_items`
--

CREATE TABLE `sr_items` (
  `id` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `od_nb` float NOT NULL,
  `wt_nb` varchar(100) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `length` int(11) NOT NULL,
  `gdmm` varchar(100) NOT NULL,
  `gdnb` varchar(100) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_items`
--

INSERT INTO `sr_items` (`id`, `standard_id`, `grade_id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `length`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(2, 4, 4, 1, '6', 17.4, 5, 1.5376, 6, '17.4 MM X 1.65 MM X 6000 MM', '1/4" X SCH10S X 6 MTR', 5, 5, '2016-03-20 13:52:52', '2016-03-20 19:50:29'),
(3, 4, 0, 1, 'SCH10', 17.6, 6, 1.72608, 10, '17.6 MM X 1.69 MM X 10000 MM', '1/4" X SCH10 X 10 MTR', 5, 5, '2016-03-20 13:57:20', '2016-03-24 21:21:08'),
(4, 2, 1, 5, '4', 2, 1, 0.0248, 6, '63 MM X 5 MM X 6000 MM', '5" X 4 X 6 MTR', 5, 5, '2016-03-20 14:26:12', '2016-03-20 19:51:23'),
(6, 3, 3, 3, 'SCH40S', 17.1, 2.31, 0.84729, 6, '17.1 MM X 2.31 MM X 6000 MM', '3/8" X SCH40S X 6 MTR', 5, 5, '2016-04-03 22:14:19', '2016-04-03 22:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `sr_mechanicals`
--

CREATE TABLE `sr_mechanicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `tensile_strength` float NOT NULL,
  `yield_strength` float NOT NULL,
  `elolgation` float NOT NULL,
  `hardness` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_mechanicals`
--

INSERT INTO `sr_mechanicals` (`id`, `grade_id`, `tensile_strength`, `yield_strength`, `elolgation`, `hardness`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 2.3, 1.5, 6, 7, 5, 0, '2016-03-24 20:09:45', '2016-03-24 20:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `sr_orderitems`
--

CREATE TABLE `sr_orderitems` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `proformaitemid` int(11) NOT NULL,
  `procatid` int(11) NOT NULL,
  `productname` varchar(30) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_orderitems`
--

INSERT INTO `sr_orderitems` (`id`, `orderid`, `proformaitemid`, `procatid`, `productname`, `standard_id`, `grade_id`, `size_id`, `length`, `qty`, `price`, `amount`, `total`) VALUES
(1, 1, 1, 1, 'Stainless stell', 2, 7, 1, 6, 12, 60.5, 726, 0),
(2, 1, 1, 2, 'Stainless stell', 1, 3, 2, 6, 16, 75.4, 1206.4, 0),
(3, 1, 1, 3, 'Stainless stell', 1, 4, 9, 6, 18, 80.5, 1449, 0),
(4, 1, 1, 4, 'Sheets', 1, 2, 10, 6, 20, 85.4, 1708, 0),
(5, 1, 0, 5, 'Pipe fiting', 2, 6, 2, 6, 22, 65.3, 1436.6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_orders`
--

CREATE TABLE `sr_orders` (
  `id` int(11) NOT NULL,
  `proformaid` int(11) NOT NULL,
  `orderno` varchar(50) NOT NULL,
  `orderdate` datetime NOT NULL,
  `tod` varchar(100) NOT NULL COMMENT 'Terms of Deilivery',
  `top` varchar(100) NOT NULL COMMENT 'Terms of Payment',
  `bill_to` int(11) NOT NULL COMMENT 'Bill to Client',
  `ship_to` varchar(255) NOT NULL COMMENT 'Ship to Client',
  `ship_address` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total` float NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - Pending, 1 - Approval  2 - Rejected',
  `approval_date` datetime NOT NULL,
  `approvalby` int(11) NOT NULL,
  `isverified` tinyint(2) NOT NULL DEFAULT '0',
  `verified_date` datetime NOT NULL,
  `verifiedby` int(11) NOT NULL,
  `rejectcomment` varchar(100) NOT NULL,
  `dispatch_id` int(11) NOT NULL,
  `dispatchcode` varchar(20) NOT NULL,
  `clientpo` varchar(20) NOT NULL,
  `ismanual` tinyint(2) NOT NULL DEFAULT '0',
  `local` tinyint(2) NOT NULL,
  `priceid` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_orders`
--

INSERT INTO `sr_orders` (`id`, `proformaid`, `orderno`, `orderdate`, `tod`, `top`, `bill_to`, `ship_to`, `ship_address`, `bank_id`, `total`, `status`, `approval_date`, `approvalby`, `isverified`, `verified_date`, `verifiedby`, `rejectcomment`, `dispatch_id`, `dispatchcode`, `clientpo`, `ismanual`, `local`, `priceid`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'SHI/1Iip1/2016', '2015-05-31 00:00:00', 'CIF', '30 days after B/L', 8, 'Paul Meijering', 'Van voodenpark 10, 5301 KP Zaltbommel,The Netharlands,Van voodenpark 10, 5301 KP Zaltbommel,The Netharlands', 4, 6526, 1, '2016-06-05 11:43:48', 8, 1, '2016-06-05 11:43:24', 8, '', 0, '', '', 0, 0, '1', 8, 8, '2016-06-05 11:39:22', '2016-06-05 18:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `sr_packingitems`
--

CREATE TABLE `sr_packingitems` (
  `id` int(11) NOT NULL,
  `packingid` int(11) NOT NULL,
  `invoiceitemid` int(11) NOT NULL,
  `grossweight` float NOT NULL,
  `netweight` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_packingitems`
--

INSERT INTO `sr_packingitems` (`id`, `packingid`, `invoiceitemid`, `grossweight`, `netweight`) VALUES
(1, 1, 0, 756, 63),
(2, 1, 0, 362, 25),
(3, 1, 0, 2, 85),
(4, 1, 0, 2, 20),
(5, 1, 0, 22, 42);

-- --------------------------------------------------------

--
-- Table structure for table `sr_packinglists`
--

CREATE TABLE `sr_packinglists` (
  `id` int(11) NOT NULL,
  `invoiceid` int(11) NOT NULL,
  `packingno` varchar(20) NOT NULL,
  `totalgrossweight` float NOT NULL,
  `totalnetwieght` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_packinglists`
--

INSERT INTO `sr_packinglists` (`id`, `invoiceid`, `packingno`, `totalgrossweight`, `totalnetwieght`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'SHI/9eVm1/2016', 0, 0, 8, 8, '2016-06-05 19:30:26', '2016-06-05 19:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `sr_physicals`
--

CREATE TABLE `sr_physicals` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `annealing_f` float NOT NULL,
  `annealing_c` float NOT NULL,
  `density` float NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_physicals`
--

INSERT INTO `sr_physicals` (`id`, `grade_id`, `annealing_f`, `annealing_c`, `density`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 6.3, 4.1, 4.9, 5, 5, '2016-03-24 20:05:26', '2016-03-24 20:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `sr_prices`
--

CREATE TABLE `sr_prices` (
  `id` int(11) NOT NULL,
  `sign` char(10) NOT NULL,
  `fullform` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_prices`
--

INSERT INTO `sr_prices` (`id`, `sign`, `fullform`) VALUES
(1, '$', 'Dollar'),
(2, '''?''', 'Rupees');

-- --------------------------------------------------------

--
-- Table structure for table `sr_productcategories`
--

CREATE TABLE `sr_productcategories` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_productcategories`
--

INSERT INTO `sr_productcategories` (`id`, `name`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'Seamless', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(2, 'Welded', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(3, 'Round Bar', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(4, 'Sheets & Plates', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00'),
(5, 'Pipe Fitting', 1, 1, '2016-05-17 15:00:00', '2016-05-17 15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformainvoices`
--

CREATE TABLE `sr_proformainvoices` (
  `id` int(11) NOT NULL,
  `proforma_no` varchar(50) NOT NULL,
  `proforma_date` date NOT NULL,
  `tod` varchar(100) NOT NULL COMMENT 'Terms of Deilivery',
  `top` varchar(100) NOT NULL COMMENT 'Terms of Payment',
  `bill_to` int(11) NOT NULL COMMENT 'Bill to Client',
  `ship_to` varchar(255) NOT NULL COMMENT 'Ship to Client',
  `ship_address` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `total` float NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - Pending, 1 - Approval  2 - Rejected',
  `approval_date` datetime NOT NULL,
  `approvalby` int(11) NOT NULL,
  `isverified` tinyint(2) NOT NULL DEFAULT '0',
  `verified_date` datetime NOT NULL,
  `verifiedby` int(11) NOT NULL,
  `rejectcomment` varchar(100) NOT NULL,
  `dispatch_id` int(11) NOT NULL,
  `dispatchcode` varchar(20) NOT NULL,
  `clientpo` varchar(50) NOT NULL,
  `ismanual` tinyint(2) NOT NULL DEFAULT '0',
  `orderno` varchar(50) NOT NULL,
  `local` tinyint(2) NOT NULL,
  `priceid` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformainvoices`
--

INSERT INTO `sr_proformainvoices` (`id`, `proforma_no`, `proforma_date`, `tod`, `top`, `bill_to`, `ship_to`, `ship_address`, `bank_id`, `total`, `status`, `approval_date`, `approvalby`, `isverified`, `verified_date`, `verifiedby`, `rejectcomment`, `dispatch_id`, `dispatchcode`, `clientpo`, `ismanual`, `orderno`, `local`, `priceid`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 'SHL/END11/2016', '2016-06-05', 'CIF', '30 days after B/L', 8, 'Paul Meijering', 'Van voodenpark 10, 5301 KP Zaltbommel,The Netharlands', 4, 3381.4, 1, '2016-06-05 11:43:48', 8, 1, '2016-06-05 11:43:24', 8, '', 7, 'KBGD1-BgUd1', 'KB-234', 0, 'SHI/1Iip1/2016', 0, '1', 8, 8, '2016-06-05 11:39:22', '2016-06-05 11:44:07');

-- --------------------------------------------------------

--
-- Table structure for table `sr_proformaitems`
--

CREATE TABLE `sr_proformaitems` (
  `id` int(11) NOT NULL,
  `proforma_id` int(11) NOT NULL,
  `procatid` int(11) NOT NULL,
  `productname` varchar(30) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_proformaitems`
--

INSERT INTO `sr_proformaitems` (`id`, `proforma_id`, `procatid`, `productname`, `standard_id`, `grade_id`, `size_id`, `length`, `qty`, `price`, `amount`, `total`) VALUES
(1, 1, 1, 'Stainless stell', 2, 7, 1, 6, 12, 60.5, 726, 0),
(2, 1, 2, 'Stainless stell', 1, 3, 2, 6, 16, 75.4, 1206.4, 0),
(3, 1, 3, 'Stainless stell', 1, 4, 9, 6, 18, 80.5, 1449, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sr_settings`
--

CREATE TABLE `sr_settings` (
  `id` int(11) NOT NULL,
  `office_address` varchar(255) NOT NULL,
  `factory_address` varchar(255) NOT NULL,
  `signature` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_settings`
--

INSERT INTO `sr_settings` (`id`, `office_address`, `factory_address`, `signature`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '24, 2nd FLR., 56, DAYABHAI BUILDING 	\r\nGULALWADI, MUMBAI-400004	\r\nMAHARASHTRA ( INDIA )  ', 'B-11, MIDC Mahad, Dist.Raigad,Pin code-402301,	\r\nMaharashtra, India	\r\n', 'Screenshot-2015-11-09-21.25.56.png', 5, 5, '0000-00-00 00:00:00', '2016-05-29 17:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `sr_sizes`
--

CREATE TABLE `sr_sizes` (
  `id` int(11) NOT NULL,
  `od_nb` varchar(20) NOT NULL,
  `wt_nb` varchar(20) NOT NULL,
  `od_mm` float NOT NULL,
  `wt_mm` float NOT NULL,
  `calc_ratio` float NOT NULL,
  `gdmm` varchar(35) NOT NULL,
  `gdnb` varchar(35) NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_sizes`
--

INSERT INTO `sr_sizes` (`id`, `od_nb`, `wt_nb`, `od_mm`, `wt_mm`, `calc_ratio`, `gdmm`, `gdnb`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '12', '25', 32, 6, 3.8688, '32 MM X 6 MM', '12 X 25', 5, 5, '2016-04-05 19:02:49', '2016-04-05 19:13:00'),
(2, '1/4', '25', 13.4, 1.67, 0.48581, '13.4 MM X 1.67 MM ', '1/4 X 25', 5, 5, '2016-04-05 20:15:35', '2016-04-05 20:15:35'),
(9, '1/4', '12', 91.2, 8.15, 16.7861, '91.2 MM X 8.15 MM', '1/4 X 12', 7, 0, '2016-05-01 00:31:28', '2016-05-01 00:31:28'),
(10, '1/4', '12', 40.2, 2.73, 2.53687, '40.2 MM X 2.73 MM', '1/4 X 12', 7, 0, '2016-05-01 00:31:28', '2016-05-01 00:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `sr_standards`
--

CREATE TABLE `sr_standards` (
  `id` int(11) NOT NULL,
  `procatid` varchar(20) NOT NULL,
  `standard_name` varchar(20) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_standards`
--

INSERT INTO `sr_standards` (`id`, `procatid`, `standard_name`, `status`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, '2,3,4', 'A313', 1, 8, 8, '2016-04-04 00:00:00', '2016-06-05 11:43:49'),
(2, '1,2,4,5', 'A312', 1, 8, 8, '2016-04-04 00:00:00', '2016-06-05 11:43:49'),
(3, '1,2,3,4,5', 'A249', 1, 8, 8, '2016-04-04 00:00:00', '2016-05-18 22:56:44'),
(4, '1,2,3,4', 'A268', 1, 8, 8, '2016-04-04 00:00:00', '2016-05-18 22:57:02'),
(5, '0', 'A269', 1, 5, 5, '2016-04-04 00:00:00', '2016-04-04 00:00:00'),
(6, '1', 'a123', 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '1,3,4', 'A345', 1, 8, 8, '2016-04-30 01:20:38', '2016-05-18 22:55:41'),
(9, '1,2,3', 'A 123', 1, 8, 8, '2016-05-17 23:33:07', '2016-05-18 23:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `sr_users`
--

CREATE TABLE `sr_users` (
  `id` int(11) NOT NULL COMMENT 'User id ',
  `usertype_id` int(11) NOT NULL COMMENT '1 = admin, 2 = HR, 3 = Supervisor, 4 = Employee, 5 = Secondary Supervisor',
  `first_name` varchar(30) NOT NULL COMMENT 'User First Name',
  `middle_name` varchar(30) NOT NULL COMMENT 'User Middle Name',
  `last_name` varchar(30) NOT NULL COMMENT 'User Last Name',
  `preferred_name` varchar(50) NOT NULL COMMENT 'User Preferred Name',
  `username` varchar(50) NOT NULL COMMENT 'User Name',
  `email` varchar(100) NOT NULL COMMENT 'User Email',
  `mobile` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT 'User Password',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 = Male, 1 = Female',
  `dob` date DEFAULT NULL COMMENT 'Date of Birth',
  `supervisor` int(11) DEFAULT NULL COMMENT 'Supervisor ID',
  `signature` varchar(100) NOT NULL COMMENT 'User Signature',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active',
  `token` varchar(50) NOT NULL COMMENT 'User Information token',
  `createdby` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_users`
--

INSERT INTO `sr_users` (`id`, `usertype_id`, `first_name`, `middle_name`, `last_name`, `preferred_name`, `username`, `email`, `mobile`, `password`, `gender`, `dob`, `supervisor`, `signature`, `status`, `token`, `createdby`, `modifiedby`, `created`, `modified`) VALUES
(1, 1, 'Hemendra', '', 'Bothra', '', 'hemendra', 'hemendra@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(2, 1, 'Vikas', '', 'Bothra', '', 'vikas', 'vikas@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '1991-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(3, 1, 'Udit', '', 'bhanshali', '', 'udit@gmail.com', 'udit@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(4, 1, 'Ramesh', '', 'Bothra', '', 'ramesh@gmail.com', 'ramesh@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(5, 1, 'jay', '', 'shah', '', 'jay@gmail.com', 'jay@gmail.com', '1234567890', '82fa8827a403d173449e360742456440b2a9c6f6', 0, '2016-03-07', NULL, '', 1, '', 5, 5, '2016-03-13 19:23:22', '2016-03-13 19:23:22'),
(6, 1, 'admin', '', 'admin', '', 'admin@admin.com', 'admin@admin.com', '453543545', '75874572ba20024ef8283f6d7ad6df2f82fe998f', 0, '2016-04-06', NULL, '', 1, '', 0, 0, '2016-04-08 21:15:40', '2016-04-09 13:43:35'),
(7, 1, 'chirag', '', 'shah', '', 'chirag@gmail.com', 'chirag@gmail.com', '7878473171', 'b811596cb8f96e7142839b8865cc420421863140', 0, '2011-07-22', NULL, '', 1, '', 0, 0, '2016-04-08 22:05:30', '2016-04-30 22:58:49'),
(8, 1, 'Kaushal', '', 'Shah', '', 'kaushal@gmail.com', 'kaushal@gmail.com', '7878473172', '1391fae89de2306617b69b6aab9d1933896a6e34', 0, '2005-03-10', NULL, '', 1, '', 0, 0, '2016-04-29 22:49:53', '2016-05-02 23:53:06'),
(9, 1, 'jainam', '', 'shah', '', 'jainam@gmail.com', 'jainam@gmail.com', '9898989898', 'b811596cb8f96e7142839b8865cc420421863140', 0, '2000-02-09', NULL, '', 1, '', 0, 0, '2016-04-30 00:03:11', '2016-04-30 00:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `sr_usertypes`
--

CREATE TABLE `sr_usertypes` (
  `id` int(11) NOT NULL,
  `usertype_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sr_usertypes`
--

INSERT INTO `sr_usertypes` (`id`, `usertype_name`) VALUES
(1, 'Admin'),
(2, 'HR'),
(3, 'Supervisor'),
(4, 'Employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sr_banks`
--
ALTER TABLE `sr_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_clients`
--
ALTER TABLE `sr_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_dispatches`
--
ALTER TABLE `sr_dispatches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_grades`
--
ALTER TABLE `sr_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `standard_id` (`standard_id`);

--
-- Indexes for table `sr_hscodes`
--
ALTER TABLE `sr_hscodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_invoiceitems`
--
ALTER TABLE `sr_invoiceitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_invoices`
--
ALTER TABLE `sr_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_items`
--
ALTER TABLE `sr_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_orderitems`
--
ALTER TABLE `sr_orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_orders`
--
ALTER TABLE `sr_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_packingitems`
--
ALTER TABLE `sr_packingitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_packinglists`
--
ALTER TABLE `sr_packinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_prices`
--
ALTER TABLE `sr_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_productcategories`
--
ALTER TABLE `sr_productcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_settings`
--
ALTER TABLE `sr_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_standards`
--
ALTER TABLE `sr_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sr_users`
--
ALTER TABLE `sr_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type` (`usertype_id`);

--
-- Indexes for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sr_banks`
--
ALTER TABLE `sr_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_chemicals`
--
ALTER TABLE `sr_chemicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_clients`
--
ALTER TABLE `sr_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sr_dispatches`
--
ALTER TABLE `sr_dispatches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_grades`
--
ALTER TABLE `sr_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sr_hscodes`
--
ALTER TABLE `sr_hscodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_invoiceitems`
--
ALTER TABLE `sr_invoiceitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_invoices`
--
ALTER TABLE `sr_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_items`
--
ALTER TABLE `sr_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sr_mechanicals`
--
ALTER TABLE `sr_mechanicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_orderitems`
--
ALTER TABLE `sr_orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_orders`
--
ALTER TABLE `sr_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_packingitems`
--
ALTER TABLE `sr_packingitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_packinglists`
--
ALTER TABLE `sr_packinglists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_physicals`
--
ALTER TABLE `sr_physicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_prices`
--
ALTER TABLE `sr_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sr_productcategories`
--
ALTER TABLE `sr_productcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sr_proformainvoices`
--
ALTER TABLE `sr_proformainvoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sr_proformaitems`
--
ALTER TABLE `sr_proformaitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sr_sizes`
--
ALTER TABLE `sr_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sr_standards`
--
ALTER TABLE `sr_standards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_users`
--
ALTER TABLE `sr_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User id ', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sr_usertypes`
--
ALTER TABLE `sr_usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
