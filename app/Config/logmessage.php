<?php
	/**
	 * {loingName} = login user name;
	 * {empName} = login user can tack action any user;
	 * {group} = Group name
	 * {email} = cantidate of out side of portal email
	 * {benefitName} = Benefit name
	 * {agreementName} = Agreement name
	 * {courseName} = Course name
	 * {contractName} = Contract name
	 * {documenName} = Document Name
	 * {eventName} = Event Processce name Name
	 * {action} = Active/Inactive etc...
	 * {hiringstatus} = Name of hiring status
	 * {holidayName} = Holiday Name
	 * {instructorName} = Instructor Name
	 * {jobopeningName} = Job opening Name
	 * {leavetype} = Leave Type Name
	 * {locationName} = Location Name
	 * {questionName} and {categoryName} = Question and Question Category Name
	 * {reportName} and {reportModule} = Report title and Report Module Name
	 * {residencyName} =  Residency Category Name
	 * {trainingName} = Training Name
	 * {emailTemplate} = Email template title
	 */
	$logMessage = array(
		/* 1) UsersControllers */
		'loginMsg' => '{loingName} has logged in',
		'logoutMsg' => '{loingName} has logged out',
		'personalinfo' => '{loingName} has changed own personal information',
		'professionalinfodelete' => '{loingName} has removed own personal professional',
		'professionalinfo' => '{loingName} has added own professional information',
		'bankinfo' => '{loingName} has changed own bank information',
		'deleteemergencycontact' => '{loingName} has removed own emergency contact',
		'emergencycontact' => '{loingName} has changed own emergency contact',
		'changepass' => '{loingName} has changed own password',
		'forgotpass' => '{loingName} has sent forgot password request',
		'shiftAllGroup' => '{loingName} has change {group} shift schedule',
		'shiftEmp' => '{loingName} has change {empName} shift schedule',
		'shift' => '{loingName} has change portal shift schedule',
		'createletter' => '{loingName} has created {empName} job offer letter',
		'createemployee' => '{loingName} has created {empName}',
		'sendtocandidate' => '{loingName} has sent job offer letter to {empName}',
		'replacepass' => '{loingName} has changed own password',
		'edit' => '{loingName} has change {empName} information',
		'changesupervisor' => '{loingName} has changed supervisor for {empName}',
		'changejobtitle' => '{loingName} has changed job title for {empName}',
		'changesalary' => '{loingName} has changed salary for {empName}',
		'adminchangepass' => '{loingName} has changed {empName} password',
		'import' => '{loingName} has import employee',

		/* 2) CandidatesController */
		'acceptjobofferletter' => '{loingName} has accepted job offer letter',
		'adminacceptjobofferletter' => '{loingName} has accepted {empName} job offer letter',
		'employeeinfo' => '{loingName} has filled up own information',
		'hremployeeinfo' => '{loingName} has filled up {empName} information',
		
		/* 3) BostonsController */
		"addbostonemployee" => '{loingName} has added {empName} bostone employee',
		"updatebostonemployee" => '{loingName} has updated {empName} bostone employee',
		"deletebostonemployee" => '{loingName} has deleted {empName} bostone employee',

		/* 4) BenefitsController */
		"addbenefit" => '{loingName} has added new {benefitName} benefit',
		"updatebenefit" => '{loingName} has updated {benefitName} benefit',
		"deletebenefit" => '{loingName} has deleted {benefitName} benefit',
		"addrequestbenefit" => '{loingName} has sent request {benefitName} benefit',
		"deleterequestbenefit" => '{loingName} has deleted {benefitName} benefit request',
		"cancelrequestbenefit" => '{loingName} has canceled {benefitName} benefit request',
		"resendrequestbenefit" => '{loingName} has resend {benefitName} benefit request',
		"approvebenefitrequest" => '{loingName} has approved {benefitName} benefit request for {empName}',
		"rejectbenefitrequest" => '{loingName} has rejected {benefitName} benefit request for {empName}',

		/* 5) CompanysController */
		"companysindex" => '{loingName} has changed company information',
		"addemailtemplate" => '{loingName} has added {emailTemplate} email template',
		"editemailtemplate" => '{loingName} has updated {emailTemplate} email template',

		/* 6) ContentsController */
		"addcontent" => '{loingName} has added contents',
		"updatecontent" => '{loingName} has updated contents',
		"deletecontent" => '{loingName} has deleted contents',

		/* 7) ContractorsController */
		"addcontractor" => '{loingName} has hired {empName} for contractor',
		"updatecontractor" => '{loingName} has updated {empName} contractor information',
		"deletecontractor" => '{loingName} has deleted {empName} contractor and him agreements',
		"deleteagreements" => '{loingName} has deleted {agreementName} agreement in {empName} contractor',
		'createagreement' => '{loingName} has created {agreementName} agreement in {empName} contractor',
		'reqsignature' => '{loingName} has signed {agreementName} agreement with {empName} contractor',
		'agreementsign' => '{loingName} has signed {agreementName} agreement',
		'updateinfo' => '{loingName} has updated own information',

		/* 8) ContractsController */
		"addcontract" => '{loingName} has added {contractName} contract type',
		"updatecontract" => '{loingName} has updated {contractName} contract type',
		"deletecontract" => '{loingName} has deleted {contractName} contract type',

		/* 9) CoursesController */
		"addcourse" => '{loingName} has added {courseName} course',
		"updatecourse" => '{loingName} has updated {courseName} course',
		"deletecourse" => '{loingName} has deleted {courseName} course',
		"approverequest" => '{loingName} has approved {empName} {courseName} course request',
		"rejectrequest" => '{loingName} has rejected {empName} {courseName} course request',
		"courserequest" => '{loingName} has sent {courseName} course request',
		"empsignature" => '{loingName} has changed employee for signature in {courseName} course',
		"coursereqsignature" => '{loingName} has signed for course request',

		/* 10) DocumentsController */
		"adddocument" => '{loingName} has added {documenName} document',
		"updatedocument" => '{loingName} has updated {documenName} document',
		"deletedocument" => '{loingName} has deleted {documenName} document',
		"adddocumentcategory" => '{loingName} has added {documenName} document category',
		"updatedocumentcategory" => '{loingName} has updated {documenName} document category',
		"deletecategory" => '{loingName} has deleted {documenName} document category',
		"addnewdocument" => '{loingName} has added document',
		"deletenewdocument" => '{loingName} has deleted document',
		"addtemplate" => '{loingName} has added {documenName} template',
		"updatetemplate" => '{loingName} has updated {documenName} template',
		"deletetemplate" => '{loingName} has deleted {documenName} template',

		/* 11) EventprocessController */
		"updateeventprocess" => '{loingName} has updated {eventName} event process',
		"updateevent" => '{loingName} has updated {eventName} event',
		"changeeventstatus" => '{loingName} has change {eventName} event to {action}',

		/* 12) GroupsController */
		"addgroup" => '{loingName} has added {group} group',
		"updategroup" => '{loingName} has updated {group} group',
		"deletegroup" => '{loingName} has deleted {group} group',
		"assignemployeegroup" => '{loingName} has changed employee in {group} group',
		
		/* 13) HiringstatussController */
		"addhiringstatus" => '{loingName} has added {hiringstatus} hiring status',
		"updatehiringstatus" => '{loingName} has updated {hiringstatus} hiring status',
		"deletehiringstatus" => '{loingName} has deleted {hiringstatus} hiring status',

		/* 14) HolidaysController */
		"addholiday" => '{loingName} has added {holidayName} holiday',
		"updateholiday" => '{loingName} has updated {holidayName} holiday',
		"deleteholiday" => '{loingName} has deleted {holidayName} holiday',

		/* 15) InstructorsController */
		"addinstructor" => '{loingName} has added {instructorName} instructor',
		"updateinstructor" => '{loingName} has updated {instructorName} instructor',
		"deleteinstructor" => '{loingName} has deleted {instructorName} instructor',

		/* 16) JobopeningsController */
		"addjobopening" => '{loingName} has added {jobopeningName} job opening',
		"updatejobopening" => '{loingName} has updated {jobopeningName} job opening',
		"deletejobopening" => '{loingName} has deleted {jobopeningName} job opening',

		/* 17) LeavetypesController */
		"addleavetype" => '{loingName} has added {leavetypeName} leave type',
		"updateleavetype" => '{loingName} has updated {leavetypeName} leave type',
		"deleteleavetype" => '{loingName} has deleted {leavetypeName} leave type',

		/* 18) LocationsController */
		"addlocation" => '{loingName} has added {locationName} location',
		"updatelocation" => '{loingName} has updated {locationName} location',

		/* 19) PerformancesController */

		/* 20) PermissionsController */
		"addgroupspermission" => '{loingName} has added {group} groups permission',
		"updategroupspermission" => '{loingName} has changed {group} groups permission',
		"adduserpermission" => '{loingName} has added {empName} permission',
		"updateuserpermission" => '{loingName} has changed {empName} permission',

		/* 21) QuestioncategoriesController */
		"addcategory" => '{loingName} has added {categoryName} category',
		"updatecategory" => '{loingName} has updated {categoryName} category',
		"deletecategory" => '{loingName} has deleted {categoryName} category',

		/* 22) QuestionsController */
		"addquestion" => '{loingName} has added {questionName} question in {categoryName} category',
		"updatequestion" => '{loingName} has updated {questionName} question  in {categoryName} category',
		"deletequestion" => '{loingName} has deleted {questionName} question in {categoryName} category',

		/* 23) ReportsController */
		"createreport" => '{loingName} has created {reportName} for {reportModule}',
		"downloadreport" => '{loingName} has downloaded {reportName}',

		/* 24) ResidencysController */
		"addresidency" => '{loingName} has added {residencyName} residency category',
		"updateresidency" => '{loingName} has updated {residencyName} residency category',
		"deleteresidency" => '{loingName} has deleted {residencyName} residency category',

		/* 25) SettingsController */
		"changesignature" => '{loingName} has changed signature',
		"changesetting" => '{loingName} has changed system settings',

		/* 26) ThirdpartyusersController */
		"addthirdpartyusers" => '{loingName} has added {empName} in third party user',
		"updatethirdpartyusers" => '{loingName} has updated {empName} in third party user',
		"deletethirdpartyusers" => '{loingName} has deleted {empName} in third party user',
		"addusernotify" => '{loingName} has added template for third party user notification',
		"updateusernotify" => '{loingName} has updated template for third party user notification',
		"deleteusernotify" => '{loingName} has deleted template for third party user notification',

		/* 27) TrainingsController */
		"addtraining" => '{loingName} has added {trainingName} training',
		"updatetraining" => '{loingName} has updated {trainingName} training',
		"deletetraining" => '{loingName} has deleted {trainingName} training',

		/* 28) AttendancesController */
		'changestatus' => '{loingName} has {action} {empName} {leavetype} form {start_date} to {end_date}',
		'sendcomprequest' => '{loingName} has send compensation request for {empName} form {start_date} to {end_date}',
		'deletecomprequestforadmin' => '{loingName} has deleted {empName} compensation request form {start_date} to {end_date}',
		'empdeleterequest' => '{loingName} has deleted {leavetype} form {start_date} to {end_date}',
		"resendrequestattendance" => '{loingName} has resend {leavetype} form {start_date} to {end_date}',
		"resendcomrequest" => '{loingName} has resend compensation request for {empName} form {start_date} to {end_date}',
		'editemployeereq' => '{loingName} has updated {leavetype} for {empName} form {start_date} to {end_date}',
		'addleavereq' => '{loingName} has sent {leavetype} form {start_date} to {end_date}',
		'editleavereq' => '{loingName} has updated {leavetype} form {start_date} to {end_date}',

		);
	Configure :: write('logMessage',$logMessage);

	/*
   	 * Name: getlogmessage
   	 * Use: this function is use to get log message data.
   	 * @Param : key.
	 */
	function getlogmessage($key = '') {
		$message = Configure :: read('logMessage');
		return $message[$key];
	}

/** 
 *	Done Controller
 *	1) UsersControllers
 *	2) CandidatesController
 *	3) BostonsController
 *	4) BenefitsController
 *	5) CompanysController
 *	6) ContentsController
 *	7) ContractorsController
 *  8) ContractsController
 *  9) CoursesController
 *  10) DocumentsController
 *  11) EventprocessController
 *  12) GroupsController
 *  13) HiringstatussController
 *  14) HolidaysController
 *  15) InstructorsController
 *  16) JobopeningsController
 *  17) LeavetypesController
 *  18) LocationsController
 *  19) 
 *  20) PermissionsController
 *  21) QuestioncategoriesController
 *  22) QuestionsController
 *  23) ReportsController
 *  24) ResidencysController
 *  25) SettingsController
 *  26) ThirdpartyusersController
 *  27) TrainingsController
 *  28) AttendancesController
*/
?>