<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); // Loads a single plugin named DebugKit
 */

/**
 * To prefer app translation over plugin translation, you can set
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

	/**
   	 * Name: proformaitemTableColumn
   	 * Use: item solumn name of proforma add/edit form
     * @param int $price_sign id of record
     * @return column names
	*/
	function proformaitemTableColumn($price_sign=null){
		$sign=''; 
		if(isset($price_sign)) {$sign = '<br>'.$price_sign;}
		$row = '<tr>
			<th width="5%">#</th>
			<th width="16%">Product Category</th>
			<th width="12%">Standard</th>
			<th width="12%">Grade</th>
			<th width="16%">Good Description</th>
			<th width="6%"> Length</th>		
			<th width="9%" id="qty_lbl"> Qty</th>
			<th width="6%"> Unit</th>
			<th width="10%" id="prc_lbl">Unit Price'.$sign.'</th>
			<th width="12%" id="amount_lbl">Amount'.$sign.'</th>
			<th width="10%"> Action </th>
		</tr>';	
		return $row;							
	}

	/**
   	 * Name: invoiceitemTableColumn
   	 * Use: item solumn name of invoice add/edit form
     * @param int $price_sign id of record
     * @return column names
	*/
	function invoiceitemTableColumn($price_sign=null){
		$sign=''; 
		if(isset($price_sign)) {$sign = $price_sign;}
		$row = '<tr>
	              	<th width="5%">#</th>
	                <th width="10%">Marks & Bundle</th>
	                <th width="6%">Party <br>Invoice No</th>
	                <th width="22%">Licence No</th>
	                <th width="10%">PCS</th>
	                <th width="7%" id="qty_lbl"> QTY</th>	                
	                <th width="10%" id="qty_lbl"> QTY/MT</th>
	                <th width="10%" id="amount_lbl">Net Weight<br>(KG)</th>
	                <th width="10%" id="amount_lbl">Gross Weight<br>(KG)</th>
	                <th width="10%" id="prc_lbl">Unit Price'.$sign.'<br/></th>
	                <th width="10%" id="amount_lbl">Net Amount <br>'.$sign.'</th>
	                <th width="6%">Action</th>
	            </tr>  ';	
		return $row;					
	}

	/**
   	 * Name: chalanitemTableColumn
   	 * Use: item solumn name of chalan add/edit form
     * @param int $price_sign id of record
     * @return column names
	*/
	function chalanitemTableColumn($price_sign=null){
		$sign=''; 
		if(isset($price_sign)) {$sign = $price_sign;}
		$row = '<tr>
					<th width="5%">#</th>
					<th width="10%">Party <br>Chalan No</th>
            		<th width="25%">Product Name</th>

					<th width="10%">Pcs</th>
					<th width="10%" id="qty_lbl"> QTY </th>
					<th width="10%" id="qty_lbl"> QTY/MT</th>
					<th width="10%" id="prc_lbl">Unit Price'.$sign.'</th>
					<th width="15%" id="amount_lbl">Net Amount<br>'.$sign.'</th>
					<th width="5%">Action</th>
				</tr>';
		return $row;					
	}
   /**
	* Name : getdelivery_type
	* Use : fetch field name from invoice table associated with delievery type
    * @param int $id id of delivery_type
    * @return delivery type name
	*/
	function getdelivery_type($id){
		$variable ='';
        if ($id == 1) { 
          $variable = 'cif';
        } elseif ($id == 2) { 
          $variable = 'fob';
        }elseif ($id == 3) {
          $variable = 'exwork';
        }elseif ($id == 4) {
          $variable = 'cnf';
        }
        else{	        	
        }
	    return $variable;    
	}
   /**
	* Name : dependentAmountField
	* Use : fetch dependant field related to amount based on delivery
    * @param int $id id of delivery_type
    * @return field name and its of id	
	*/
	function dependentAmountField($invoice_array = null,$delivery_type = null) {

		$amount_part = array();
		$variable = '';

		if ($delivery_type == 1) {
			if($invoice_array['fob'] != 0){
				$amount_part[] = array('label'=>'FOB Value','value'=>decimalPrice($invoice_array['fob']));
			}
			if($invoice_array['frieght'] != 0){
				$amount_part[] = array('label'=>'Frieght','value'=>decimalPrice($invoice_array['frieght']));
			}
			if($invoice_array['insurance'] != 0){
				$amount_part[] = array('label'=>'Insurance','value'=>decimalPrice($invoice_array['insurance']));
			}
			if($invoice_array['discount'] != 0){
				$amount_part[] = array('label'=>'Discount','value'=>decimalPrice($invoice_array['discount']));
			}									
		}
		elseif ($delivery_type == 2) {
			if($invoice_array['discount'] != 0){
				$amount_part[] = array('label'=>'Discount','value'=>decimalPrice($invoice_array['discount']));
			}
		}
		elseif ($delivery_type == 3) {
			if($invoice_array['packing'] != 0){
				$amount_part[] = array('label'=>'P&F','value'=>decimalPrice($invoice_array['packing']));
			}			
		}
		elseif ($delivery_type == 4) {
			if($invoice_array['fob'] != 0){
				$amount_part[] = array('label'=>'FOB Value','value'=>decimalPrice($invoice_array['fob']));
			}
			if($invoice_array['frieght'] != 0){
				$amount_part[] = array('label'=>'Frieght','value'=>decimalPrice($invoice_array['frieght']));
			}
			if($invoice_array['discount'] != 0){
				$amount_part[] = array('label'=>'Discount','value'=>decimalPrice($invoice_array['discount']));
			}			
		}
		else {
			return true;
		}

		$variable = getdelivery_type($delivery_type);

		$amount_part[] = array('label'=>'Final '.strtoupper($variable).' Value','value'=>decimalPrice($invoice_array['finalvalue']));
		if($invoice_array['advancereceive'] != 0){
			$amount_part[] = array('label'=>'Advance Received','value'=>decimalPrice($invoice_array['advancereceive']));
		}
		if($invoice_array['balancevalue'] != 0){
			$amount_part[] = array('label'=>'Balance Value','value'=>decimalPrice($invoice_array['balancevalue']));
		}		
		return $amount_part;
	}

	/* 
	* Name :showAllGradeWeight
	* Use : display all grade weight in invoice footer with formatting.
	*/
	function showAllGradeWeight($partygradebaseweight,$customgradebaseweight,$p){
	    if($p == '0' || $p == '1'){/*Party Invoice Or Party Packing List*/
	      $gradebaseweight = $partygradebaseweight;
	    }
	    if($p == '2' || $p == '3'){/*Custom Invoice Or Custom Packing List*/
	      $gradebaseweight = $customgradebaseweight;
	    }
/*		if(!empty($customgradebaseweight)){
			$gradebaseweight = $customgradebaseweight;
		}
		else{
			$gradebaseweight = $partygradebaseweight;
		}*/
		$cell = str_replace(array('<br/>','</br>'),"\n ",$gradebaseweight);
		return $cell;
	}

	/* 
	* Name :replacebreak
	* Use : display address with formatting.
	*/
	function replacebreak($dataval){
		//$break = str_replace(array('<br/>','</br>'),"\n ",$address);
		//	$val = nl2br($dataval);
		$break = preg_replace("/\r\n|\r|\n/",'<br/>',$dataval);
		return $break;
	}

	/**
   	 * Name: decimalQty
   	 * Use: set decimal 3 point after value
     * @param decimal $qty quantity of record 
     * @return decimal quantity
	*/
	function decimalQty($qty = null){
		$decimal_value = number_format((float)$qty,3, '.', '');
		return $decimal_value;
	}

	/**
   	 * Name: decimalPrice
   	 * Use: set decimal 2 point after value
     * @param decimal $price price of record 
     * @return decimal price
	*/
	function decimalPrice($price = null){
		$price_value = number_format((float)$price,2, '.', '');
		return $price_value;
	}

	/**
   	 * Name: decimalWeight
   	 * Use: set decimal 3 point after value
     * @param decimal $weight weight of record 
     * @return decimal weight
	*/
	function decimalWeight($weight = null){
		$decimal_value = number_format((float)$weight,3, '.', '');
		return $decimal_value;
	}
	/**
   	 * Name: setGDnbformat
   	 * Use: get gd nb
     * @param varchar $gdnb gdnb of record
     * @param varchar $length length of record     
     * @return final gd nb
	*/
	function setGDnbformat($gdnb=null,$length=null){
		$datagdnb = '';
		if(!empty($gdnb)){ $datagdnb.= $gdnb.' X ';} $datagdnb.= $length; 
		return $datagdnb;
	}
	
	/**
   	 * Name: setGDmmformat
   	 * Use: get gd nb
     * @param varchar $gdmm gdmm of record
     * @param varchar $length length of record     
     * @return final gd mm
	*/		
	function setGDmmformat($gdmm=null,$length=null){
		$datagdmm = '';
		if(!empty($gdmm)){ $datagdmm.= $gdmm.' X ';} $datagdmm.= $length;
		return $datagdmm;
	}

	/**
   	 * Name: searchFromAddress
   	 * Use: fetch particular one address from all address of client.
     * @param int $addressid address id  of record
     * @param array $addressArray address array of particular client
     * @return single address
	*/
	function searchFromAddress($addressid, $addressArray) {
	    foreach ($addressArray as $key => $val) {
	        if ($val['id'] === $addressid) {
	           return $val['streetaddress'].'<br/>'.$val['city'].'<br/>'.$val['state'].'<br/>'.$val['country'];
	        }
	    }
	    return null;
	}

	App::uses('CakeSession', 'Model/Datasource');
	CakeSession::read('login_usertype_id');
$access_controller = array();
array_push($access_controller,'dashboards/index','users/login','users/logout','proformainvoices/ajaxlisting','proformainvoices/view','proformainvoices/getDispatchID','proformainvoices/getDispatchcode','proformainvoices/getUserID','proformainvoices/getGrades','proformainvoices/getnb','proformainvoices/getStandards','proformainvoices/getclientdetail','proformainvoices/getclients','proformainvoices/sendEmailCoordinator','proformainvoices/rejectproformainvoice','proformainvoices/approveproformainvoice','proformainvoices/sendEmailClient','proformainvoices/getVerified','proformainvoices/getProformaNO','proformainvoices/getManualID','proformainvoices/order','proformainvoices/getClientID','proformainvoices/getBankID','proformainvoices/getStandardID','proformainvoices/getGradeID','proformainvoices/getSizeID','orders/ajaxlisting','orders/view','orders/getManualID','orders/getClientID','orders/getStandardID','orders/getGradeID','orders/getBankID','orders/getSizeID','orders/getUserID','orders/getDispatchID','orders/getProductCat','orders/getPriceId','orders/changeitemstatus','orders/changeorderstatus','orders/checkorderstatus','orders/export','invoices/ajaxlisting','invoices/view','invoices/countdeliveredqty','invoices/getInvoiceNO','invoices/invoiceoforder','chalans/ajaxlisting','chalans/view','chalans/getChalanNO','chalans/countdeliveredqty','chalans/chalanoforder','chalans/chalanlisting','certificates/ajaxlisting','certificates/generatetc','certificates/_group_by','certificates/_group_ty','certificates/getCertificateNO','certificates/getHeatno','certificates/checkCertiGenerate','certificates/viewtclist','users/personalinfo','users/changepass','users/logout','users/getUsername','invoices/index','proformainvoices/prints','users/forgotpass','users/resetpass','users/myprofile','proformainvoices/generatepdf','orders/prints','orders/generatepdf','invoices/custominvoice','invoices/partyinvoice','invoices/custompacking','invoices/partypacking','invoices/generatepdf','chalans/prints');
CakeSession::write('access_controller',$access_controller);
//