jQuery(document).ready(function() {   
  $(".comboselection").select2();

  $(".shalcoform").submit(function(e){   
    var formObj = $(this);
    var formURL = formObj.attr("action"); 
    //var formData = new FormData(this);    
    $("#spinner").show();
        $.ajax({
          url: formURL,
          type: 'POST',
          data:  formData,
          mimeType:"multipart/form-data",
          dataType : 'json',
          contentType: false,
          cache: false,
          processData:false, 
          success: function(data, textStatus, jqXHR){ 
            $("#spinner").hide();
          },
          error: function(jqXHR, textStatus, errorThrown){ 
            $("#spinner").hide();
          }           
       });
      e.preventDefault();
  });
  /*form submit end*/

  /*form submit using ajax start*/
  $(".shalcoajaxform").submit(function(e){ 
    e.preventDefault();
    var formObj = $(this);
    var formURL = formObj.attr("action");    
    var formData = new FormData(this);  
    //var fid=$('form').attr('id');
    //var formData = $('#'+fid).serialize();  

    $("#spinner").show();
        $.ajax({
          url: formURL,
          type: 'POST',
          data:  formData,
          mimeType:"multipart/form-data",
          dataType : 'json',
          contentType: false,
          cache: false,
          processData:false, 
          beforeSend: function(){
            $('.green').attr('disabled', true);
            $(".error-message").remove();    
            $(".form-control").removeClass('invalid_field');    
                   
          },
          success: function(response){             
              var response1=jQuery.parseJSON(JSON.stringify(response));
              $("#spinner").hide();
              $('.green').attr('disabled', false);
              $(".msg").text(response1.message);              
              /* Success response than */
              if(response1.status=='success'){/*check redirect url set or not,Its not set for popup(modal dialog) section*/
                if(response1.redirect == ''){
                  /*If modal dialog is client than set its value in parent page,client dropdown element*/
                  if(response1.topic == 'client'){
                    $("#addClientModal .close").click();
                    $('.bill_to').append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $(".bill_to option[value='"+response1.idvalue+"']").attr("selected", true);
                    $("#select2-"+response1.targetid+"-container").text(response1.text);
                    $('.bill_to').trigger('change');
                  }
                  /*If modal dialog is size than set its value in parent page,size dropdown element*/
                  if(response1.topic == 'size'){
                    $("#addSizeModal .close").click();
                    $('#'+response1.targetid).append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $("#"+response1.targetid+" option[value='"+response1.idvalue+"']").attr("selected", true);
                    $("#select2-"+response1.targetid+"-container").text(response1.text);
                    $('#'+response1.targetid).trigger('change');
                    //$('.size').trigger('change');
                  }                    
                  /*If modal dialog is HS code than set its value in parent page,hscode dropdown element*/
                  if(response1.topic == 'hscode'){
                    $("#addHSCodeModal .close").click();
                    $('#'+response1.targetid).append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $("#"+response1.targetid+" option[value='"+response1.idvalue+"']").attr("selected", true);
                    //$("#select2-"+response1.targetid+"-container").text(response1.text);                   
                  }                  
                }else{
                  window.setTimeout(function() {
                    $(".message").fadeOut('slow');
                      window.location = response1.redirect;
                  }, 5);
                }                
              }
              /* Error response than */
              else if(response1.status=='error'){
                var firsterrorAt = ''; /*save first element id where error generate*/
                $.each(response1.data, function(model, errors) {
                  for (fieldName in this) {
                    var element = $("#" + camelcase(model + '_' + fieldName));
                    if(firsterrorAt === ''){                        
                        firsterrorAt = "#"+camelcase(model + '_' + fieldName);
                    }
                    /* add style to element which has error */    
                    $("#"+camelcase(model + '_' + fieldName)).addClass('invalid_field');                      
                    var create = $(document.createElement('div')).insertAfter(element);
                    create.addClass('error-message').text(this[fieldName][0])
                  }
                });
                /*scroll first position where error occur*/
                $("body, html").animate({ 
                  scrollTop: ($(firsterrorAt).offset().top - 20)
                }, 600);                                  
              }              
          },
          error: function(jqXHR, textStatus, errorThrown){ 
            $("#spinner").hide();
          }           
       });
      e.preventDefault();
  });
  /*form submit using ajax end*/

    /*concatenation of element and capitalization of form element first character */
    function camelcase(inputstring) {
      var a = inputstring.split('_'), i;
      s = [];
      for (i=0; i<a.length; i++){
          s.push(a[i].charAt(0).toUpperCase() + a[i].substring(1));
      }
      s = s.join('');
      return s;
    }    
});

$(document).ready(function() {

 $(document).on('click', '#delete', function (e) {   // e.preventDefault();
 //  alert('dsfsdf');
    var choice = confirm($(this).attr('data-confirm'));
   // alert(choice);
    if (choice) {
        window.location.href = $(this).attr('href');
    }else{
        return false;
    }
});


 $(document).on('click', '.prints', function(e) {
    e.preventDefault();
    var printid = $(this).attr("data-id");        
    var controllername = $(this).attr("data-controllername"); 
    var actionname = $(this).attr("data-actionname");        
    $.ajax({
      //url: "http://localhost/shalcoerp/"+controllername+"/"+actionname+"/"+printid,
      url: WEBSITE_PATH+"/"+controllername+"/"+actionname+"/"+printid,
      //data: data,
      //type: 'POST',
      success : function (response) {            
          $('body').html(response);
          window.print();
      },
      error : function () {
        alert('Could not fetch EXPLAIN for query.');
      }
    });
});
 /* only allow numeric word */

/*restict to press keyboard key  */
$(document).on('keypress', '.qty_mtr,.prc,.validity,.ex_validity,.decimalallow', function(evt) {
    //alert('hello');
 evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 37 && charCode != 9 && charCode != 8 && charCode != 39 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
});


  // $(document).on('click', '.print_proforma', function (e) {

  //  		var printid = $(this).attr("data-id");
  //       $.get("http://localhost/shalcoerp/proformainvoices/view/"+printid, function(data, status,xhr){
  //       alert("Data: " + data + "\nStatus: " + status);
  //       var divToprint = document.getElementById('tab_3');
  //       console.log(divToprint);
  //       	var divToPrint = document.getElementById('tab_3');
  //         //window.open('','_blank', 'width=700,height=500,left=200');
  //         document.open();
  //         document.write('<html><head><link href="http://localhost/shalcoerp/app/webroot/css/custom.css" rel="stylesheet" type="text/css" /><link href="http://localhost/shalcoerp/app/webroot/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><style>.btn.default{display: none;}.print_none{display:none;}.portlet > .portlet-title > .caption {display: inline-block;float: left;font-size: 18px;line-height: 25px;text-transform: uppercase;padding : 0 0 0 40%}</style></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
  //           document.close();

  //       });
        
  //   });
});