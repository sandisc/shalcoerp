jQuery(document).ready(function() {   
  $(".comboselection").select2();


  /*form submit inside popup start*/
  $(".shalcopopupajaxform").submit(function(e){ 
    e.preventDefault();
    var formObj = $(this);
    var formURL = formObj.attr("action");    
    var formData = new FormData(this);  
    //var fid=$('form').attr('id');
    //var formData = $('#'+fid).serialize();  

    $("#spinner").show();
        $.ajax({
          url: formURL,
          type: 'POST',
          data:  formData,
          mimeType:"multipart/form-data",
          dataType : 'json',
          contentType: false,
          cache: false,
          processData:false, 
          beforeSend: function(){
            $('.green').attr('disabled', true);
            $(".error-message").remove();    
            $(".form-control").removeClass('invalid_field');    
                   
          },
          success: function(response){             
              var response1=jQuery.parseJSON(JSON.stringify(response));
              $("#spinner").hide();
              $('.green').attr('disabled', false);
              $(".msg").text(response1.message);              
              /* Success response than */
              if(response1.status=='success'){/*check redirect url set or not,Its not set for popup(modal dialog) section*/
                if(response1.redirect == ''){
                  /*If modal dialog is client than set its value in parent page,client dropdown element*/
                  if(response1.topic == 'client'){
                    $("#addClientModal .close").click();
                    $('.bill_to').append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $(".bill_to option[value='"+response1.idvalue+"']").attr("selected", true);
                    $("#select2-"+response1.targetid+"-container").text(response1.text);
                    $('.bill_to').trigger('change');
                  }
                  /*If modal dialog is size than set its value in parent page,size dropdown element*/
                  if(response1.topic == 'size'){
                    $("#addSizeModal .close").click();
                    $('#'+response1.targetid).append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $("#"+response1.targetid+" option[value='"+response1.idvalue+"']").attr("selected", true);
                    $("#select2-"+response1.targetid+"-container").text(response1.text);
                    $('#'+response1.targetid).trigger('change');
                    //$('.size').trigger('change');
                  }                    
                  /*If modal dialog is HS code than set its value in parent page,hscode dropdown element*/
                  if(response1.topic == 'hscode'){
                    $("#addHSCodeModal .close").click();
                    $('#'+response1.targetid).append($('<option>', {
                        value: response1.idvalue,
                        text: response1.text
                    }));
                    $("#"+response1.targetid+" option[value='"+response1.idvalue+"']").attr("selected", true);
                    //$("#select2-"+response1.targetid+"-container").text(response1.text);                   
                  }                  
                }else{
                  window.setTimeout(function() {
                    $(".message").fadeOut('slow');
                      window.location = response1.redirect;
                  }, 5);
                }                
              }
              /* Error response than */
              else if(response1.status=='error'){
                var firsterrorAt = ''; /*save first element id where error generate*/
                $.each(response1.data, function(model, errors) {
                  for (fieldName in this) {
                    var element = $("#" + camelcase(model + '_' + fieldName));
                    if(firsterrorAt === ''){                        
                        firsterrorAt = "#"+camelcase(model + '_' + fieldName);
                    }
                    /* add style to element which has error */    
                    $("#"+camelcase(model + '_' + fieldName)).addClass('invalid_field');                      
                    var create = $(document.createElement('div')).insertAfter(element);
                    create.addClass('error-message').text(this[fieldName][0])
                  }
                });
                /*scroll first position where error occur*/
                $("body, html").animate({ 
                  scrollTop: ($(firsterrorAt).offset().top - 20)
                }, 600);                                  
              }              
          },
          error: function(jqXHR, textStatus, errorThrown){ 
            $("#spinner").hide();
          }           
       });
      e.preventDefault();
  });
  /*form submit inside popup end*/
    /*concatenation of element and capitalization of form element first character */
    function camelcase(inputstring) {
      var a = inputstring.split('_'), i;
      s = [];
      for (i=0; i<a.length; i++){
          s.push(a[i].charAt(0).toUpperCase() + a[i].substring(1));
      }
      s = s.join('');
      return s;
    } 

});